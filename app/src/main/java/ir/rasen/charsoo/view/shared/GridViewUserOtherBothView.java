package ir.rasen.charsoo.view.shared;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;

import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.FriendshipRelation;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.model.friend.RequestFriendship;
import ir.rasen.charsoo.view.activity.user.ActivityUserFollowingBusinesses;
import ir.rasen.charsoo.view.activity.user.ActivityUserFriends;
import ir.rasen.charsoo.view.activity.user.ActivityUserReviews;
import ir.rasen.charsoo.view.adapter.AdapterPostGridAndList;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.dialog.PopupWindowCancelFriendship;
import ir.rasen.charsoo.view.interface_m.ICancelFriendship;
import ir.rasen.charsoo.view.interface_m.IPullToRefresh;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.WaitDialog;
import ir.rasen.charsoo.view.widgets.buttons.ButtonFont;
import ir.rasen.charsoo.view.widgets.buttons.FloatButton;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.HFGridView;

/**
 * Created by android on 3/14/2015.
 */
public class GridViewUserOtherBothView implements IWebservice, ICancelFriendship {
    public static final String TAG = "GridViewUserOtherBothView";

    public static final int REQUEST_FRIENDSHIP_INT_CODE = 14, REQUEST_CANCEL_FRIENDSHIP_INT_CODE = 19;

    public HFGridView gridViewHeader;
    //    AdapterPostGrid adapterPostrid;
//    AdapterPost adapterPost;
    public AdapterPostGridAndList adapterPostGridAndList;

    public boolean isThreeColumn;

    FloatButton imageViewFriends, imageViewReviews, imageViewFollowingBusinesses;
    ImageView imageViewCover;
    View switchGrid, switchList;
    //TextViewFont textViewFriends, textViewBusinesses, textViewReviews,
    TextViewFont textViewIdentifier, textViewName;
    ButtonFont buttonFriendStatus;
    View viewHeader;
    Activity context;
    View listFooterView;
    boolean isLoadingMore;
    ArrayList<Post> posts;
    User user;
    boolean headerInitialized;
    ICancelFriendship iCancelFriendshipl;
    WaitDialog progressDialog;
    DialogMessage dialogMessage;
    View viewNothing;
    ImageView imgNothing;
    TextViewFont txtNothing;
    IPullToRefresh iPullToRefresh;

    public static String parentTag;

    public GridViewUserOtherBothView
            (final Activity context, final User displayedUser, HFGridView gViewHeader,
             DialogMessage dialogMessage,String parentTag,IPullToRefresh iPullToRefresh) {
        this.context = context;
        this.user = displayedUser;
        this.gridViewHeader = gViewHeader;
        this.iCancelFriendshipl = this;
        progressDialog = new WaitDialog(context, context.getResources().getString(R.string.please_wait));
        this.dialogMessage = dialogMessage;
        headerInitialized=false;
        isLoadingMore=false;
        isThreeColumn=true;

        this.parentTag = parentTag;
        this.iPullToRefresh=iPullToRefresh;
    }

    public void InitialHeaderNoDataAvailable() {

    }

    public void InitialGridViewUser(ArrayList<Post> postList, boolean beThreeColumn){
        this.isThreeColumn = beThreeColumn;
        posts=(ArrayList<Post>)postList.clone();

//        adapterPostGrid = new AdapterPostGrid(context, searchItemPosts, 0, Post.GetPostType.SHARE);
//
//        adapterPost = new AdapterPost(GridViewUserOtherBothView.this, context, posts);

        adapterPostGridAndList =
                new AdapterPostGridAndList(
                        context,"0",
                        Post.GetPostType.SHARE,GridViewUserOtherBothView.this,
                        context,posts,isThreeColumn, parentTag);

        if (!headerInitialized) {
            viewHeader =
                    ((Activity) context).getLayoutInflater().
                            inflate(R.layout.layout_header_user_another, null);


            imageViewCover = (ImageView) viewHeader.findViewById(R.id.imageView_cover);

            imageViewFriends = (FloatButton) viewHeader.findViewById(R.id.imageView_friends);
            imageViewReviews = (FloatButton) viewHeader.findViewById(R.id.imageView_reviews);
            imageViewFollowingBusinesses = (FloatButton) viewHeader.findViewById(R.id.imageView_businesses);

            switchGrid = viewHeader.findViewById(R.id.btn_switch_grid);
            switchList = viewHeader.findViewById(R.id.btn_switch_list);
            //textViewBusinesses = (TextViewFont) viewHeader.findViewById(R.uniqueId.textView_businesses);
            //textViewFriends = (TextViewFont) viewHeader.findViewById(R.uniqueId.textView_friends);
            //textViewReviews = (TextViewFont) viewHeader.findViewById(R.uniqueId.textView_reviews);
            textViewIdentifier = (TextViewFont) viewHeader.findViewById(R.id.textView_user_identifier);
            textViewName = (TextViewFont) viewHeader.findViewById(R.id.textView_user_name);
            buttonFriendStatus = (ButtonFont) viewHeader.findViewById(R.id.btn_friend_satus);

            viewNothing = viewHeader.findViewById(R.id.view_nothing);
            txtNothing = (TextViewFont) viewHeader.findViewById(R.id.txt_nothing);
            imgNothing = (ImageView) viewHeader.findViewById(R.id.img_nothing);
            viewNothing.setVisibility(View.GONE);

            //TODO change user.name to user.userIdentifier


            textViewIdentifier.setText(user.userIdentifier);
            textViewName.setText(user.name);

            String text = buttonFriendStatus.getText().toString();
            switch (user.friendshipRelationStatus) {
                case FRIEND:
                    buttonFriendStatus.setBackgroundResource(R.drawable.selector_button_shape_green);
                    buttonFriendStatus.setCompoundDrawablesWithIntrinsicBounds(null, null, context.getResources().getDrawable(R.drawable.ic_check_white_24dp), null);
                    buttonFriendStatus.setText(context.getString(R.string.friend));
                    break;
                case NOT_FRIEND:
                    buttonFriendStatus.setBackgroundResource(R.drawable.selector_button_shape_blue);
                    buttonFriendStatus.setText(context.getString(R.string.friendy));
                    break;
                case REQUEST_REJECTED:
                    buttonFriendStatus.setBackgroundResource(R.drawable.selector_button_shape_blue);
                    buttonFriendStatus.setText(context.getString(R.string.friendy));
                    break;
                case REQUEST_SENT:
                    buttonFriendStatus.setEnabled(false);
                    buttonFriendStatus.setBackgroundResource(R.drawable.shape_button_gray);
                    buttonFriendStatus.setText(context.getString(R.string.wating_for_comfirm));
                    break;
            }


            text = buttonFriendStatus.getText().toString();
            buttonFriendStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (user.friendshipRelationStatus) {
                        case FRIEND:
                            //cancel friendship
//                            new RequestCancelFriendship(context, LoginInfo.getUserId(context), user.uniqueId, NOT_USED_GridViewUserOther.this).executeWithNewSolution();;
                            new PopupWindowCancelFriendship(context, user.uniqueId, GridViewUserOtherBothView.this).showAsDropDown(view);
                            break;
                        case NOT_FRIEND:
                            //send friendship request
                            //   progressDialog.show();
                            new RequestFriendship(context,LoginInfo.getInstance().getUserUniqueId(), user.uniqueId, GridViewUserOtherBothView.this, REQUEST_FRIENDSHIP_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                            break;
                        case REQUEST_REJECTED:
                            //send request again
                            new RequestFriendship(context,LoginInfo.getInstance().getUserUniqueId(), user.uniqueId, GridViewUserOtherBothView.this, REQUEST_FRIENDSHIP_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                            break;
                        case REQUEST_SENT:
                            //do nothing
                            break;
                    }
                }
            });

            SimpleLoader simpleLoader = new SimpleLoader(context);
            simpleLoader.loadImage(user.profilePictureUniqueId, Image_M.LARGE, Image_M.ImageType.USER, imageViewCover);

            imageViewFollowingBusinesses.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (user.friendshipRelationStatus != FriendshipRelation.Status.FRIEND)
                        return;
                    Intent intent1 = new Intent(context, ActivityUserFollowingBusinesses.class);
                    intent1.putExtra(Params.VISITED_USER_UNIQUE_ID, user.uniqueId);
                    context.startActivity(intent1);
                }
            });
            imageViewFriends.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (user.friendshipRelationStatus != FriendshipRelation.Status.FRIEND)
                        return;
                        Intent intent1 = new Intent(context, ActivityUserFriends.class);
                        intent1.putExtra(Params.VISITED_USER_UNIQUE_ID, user.uniqueId);
                        context.startActivity(intent1);
                }
            });

            imageViewReviews.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (user.friendshipRelationStatus != FriendshipRelation.Status.FRIEND)
                        return;
                        Intent intent1 = new Intent(context, ActivityUserReviews.class);
                        intent1.putExtra(Params.VISITED_USER_UNIQUE_ID, user.uniqueId);
                        context.startActivity(intent1);
                }
            });

            switchList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gridViewHeader.setNumColumns(1);
                    gridViewHeader.setColumnWidth(context.getWindowManager().getDefaultDisplay().getWidth());
//                    gridViewHeader.setVerticalSpacing(3);
//                    gridViewHeader.setHorizontalSpacing(9);
//                    gridViewHeader.setViewWidthIfItsZero(activity.getWindowManager().getDefaultDisplay().getWidth());
//                    gridViewHeader.setAdapter(adapterPostGridAndList);

                    //now it has one column
                    isThreeColumn = false;
                    adapterPostGridAndList.setViewType(isThreeColumn);

                    switchList.setBackgroundColor(context.getResources().getColor(R.color.lightPrimaryColor));
                    switchGrid.setBackgroundColor(context.getResources().getColor(R.color.material_gray_light));
                }
            });
            switchGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    prepareGridThreeColumn(gridViewHeader);
                    gridViewHeader.setNumColumns(3);
                    isThreeColumn = true;
                    adapterPostGridAndList.setViewType(isThreeColumn);

                    // now it has three column
                    switchGrid.setBackgroundColor(context.getResources().getColor(R.color.lightPrimaryColor));
                    switchList.setBackgroundColor(context.getResources().getColor(R.color.material_gray_light));
                }
            });

            gridViewHeader.addHeaderView(viewHeader);
            listFooterView =
                    ((LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE)).
                            inflate(R.layout.layout_loading_more, null, false);
            gridViewHeader.addFooterView(listFooterView);

            headerInitialized = true;
        } else {
            listFooterView.setVisibility(View.GONE);
        }

        gridViewHeader.setBackgroundColor(Color.parseColor("#ffffff"));
        gridViewHeader.setOnScrollListener(new AbsListView.OnScrollListener() {
            int currentFirstVisibleItem,
                    currentVisibleItemCount,
                    currentScrollState;

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
            }

            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            private void isScrollCompleted() {
                if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
                    if (!isLoadingMore
                            && posts.size() > 0 && posts.size() % context.getResources().getInteger(R.integer.lazy_load_limitation) == 0) {
                        loadMoreData();
                    }
                }
            }
        });


        //if gridview is displaying the post or user has not any posts
        if (postList.size() != 0 || (headerInitialized && postList.size() == 0))
            listFooterView.setVisibility(View.GONE);

        if (isThreeColumn) {
//            gridViewHeader.setNumColumns(3);
//            gridViewHeader.setVerticalSpacing(3);
//            gridViewHeader.setHorizontalSpacing(9);
            //        gridViewHeader.setAdapter(adapterPostShared);
            //now it has one column
            //        isThreeColumn = false;
            //        switchList.setBackgroundColor(activity.getResources().getColor(R.color.material_blue_light));
            //        switchGrid.setBackgroundColor(activity.getResources().getColor(R.color.material_gray_light));
//            gridViewHeader.setNumColumns(3);
//            gridViewHeader.setVerticalSpacing(3);
//            gridViewHeader.setHorizontalSpacing(9);
            prepareGridThreeColumn(gridViewHeader);
            gridViewHeader.setAdapter(adapterPostGridAndList);
            // now it has three column
            switchGrid.setBackgroundColor(context.getResources().getColor(R.color.lightPrimaryColor));
            switchList.setBackgroundColor(context.getResources().getColor(R.color.material_gray_light));
        } else {
            gridViewHeader.setNumColumns(1);
            gridViewHeader.setAdapter(adapterPostGridAndList);
            switchList.setBackgroundColor(context.getResources().getColor(R.color.lightPrimaryColor));
            switchGrid.setBackgroundColor(context.getResources().getColor(R.color.material_gray_light));
        }
        noPosts();
    }

    // LOAD MORE DATA
    public void loadMoreData() {
        // LOAD MORE DATA HERE...
        isLoadingMore = true;
        listFooterView.setVisibility(View.VISIBLE);
        iPullToRefresh.notifyLoadMore();
    }

    public void addMorePosts(ArrayList<Post> newPosts){
        posts.addAll(newPosts);
        noPosts();
        listFooterView.setVisibility(View.GONE);
        adapterPostGridAndList.loadMoreItems(newPosts);
        isLoadingMore = false;
    }

    public void resetPostItems(ArrayList<Post> newPosts){
        posts=new ArrayList<>(newPosts);
        adapterPostGridAndList.resetItems(posts);
    }


    private void prepareGridThreeColumn(HFGridView gridViewHeader) {
        gridViewHeader.setNumColumns(3);
        gridViewHeader.setVerticalSpacing(3);
        gridViewHeader.setHorizontalSpacing(9);
        gridViewHeader.setViewWidthIfItsZero(context.getWindowManager().getDefaultDisplay().getWidth());
        isThreeColumn = true;
    }


    //    public void cancelFriendship(){
//        buttonFriendStatus.setBackgroundResource(R.drawable.selector_button_shape_blue);
//        buttonFriendStatus.setEnabled(true);
//        buttonFriendStatus.setCompoundDrawablesWithIntrinsicBounds(null, null,null, null);
//        buttonFriendStatus.setText(context.getString(R.string.friendy));
//        user.friendshipRelationStatus = FriendshipRelation.Status.NOT_FRIEND;
//    }
    @Override
    public void notifyDeleteFriend(String userId) {
        buttonFriendStatus.setBackgroundResource(R.drawable.selector_button_shape_blue);
        buttonFriendStatus.setEnabled(true);
        buttonFriendStatus.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        buttonFriendStatus.setText(context.getString(R.string.friendy));
        user.friendshipRelationStatus = FriendshipRelation.Status.NOT_FRIEND;
    }

    @Override
    public void notifyDeleteFriendFailed(String failureMessage) {
        if (!dialogMessage.isShowing()) {
            dialogMessage.show();
            dialogMessage.setMessage(failureMessage);
        }
//
    }

    public void refreshUserData(final User displayedUser) {
        if (this.user.profilePictureUniqueId != displayedUser.profilePictureUniqueId) {
            this.user = displayedUser;
            SimpleLoader simpleLoader = new SimpleLoader(context);
            simpleLoader.loadImage(user.profilePictureUniqueId, Image_M.LARGE, Image_M.ImageType.USER, imageViewCover);
        } else
            this.user = displayedUser;

        textViewIdentifier.setText(user.userIdentifier);
        textViewName.setText(user.name);

        switch (user.friendshipRelationStatus) {
            case FRIEND:
                buttonFriendStatus.setBackgroundResource(R.drawable.selector_button_shape_green);
                buttonFriendStatus.setCompoundDrawablesWithIntrinsicBounds(
                        null, null, context.getResources().getDrawable(R.drawable.ic_check_white_24dp),
                        null);

                buttonFriendStatus.setText(context.getString(R.string.friend));
                break;
            case NOT_FRIEND:
                buttonFriendStatus.setBackgroundResource(R.drawable.selector_button_shape_blue);
                buttonFriendStatus.setText(context.getString(R.string.friendy));
                buttonFriendStatus.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                break;
            case REQUEST_REJECTED:
                buttonFriendStatus.setBackgroundResource(R.drawable.selector_button_shape_blue);
                buttonFriendStatus.setText(context.getString(R.string.friendy));
                buttonFriendStatus.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                break;
            case REQUEST_SENT:
                buttonFriendStatus.setEnabled(false);
                buttonFriendStatus.setBackgroundResource(R.drawable.shape_button_gray);
                buttonFriendStatus.setText(context.getString(R.string.wating_for_comfirm));
                buttonFriendStatus.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                break;
        }
//        buttonFriendStatus.setEnabled(user.friendshipRelationStatus== FriendshipRelation.Status.NOT_FRIEND
//                                    || user.friendshipRelationStatus== FriendshipRelation.Status.REQUEST_REJECTED);
        buttonFriendStatus.setEnabled(user.friendshipRelationStatus!= FriendshipRelation.Status.REQUEST_SENT);
    }

    @Override
    public void getResult(int request, Object result) {
        switch (request) {
            case REQUEST_FRIENDSHIP_INT_CODE:
                //RequestFriendship's result
                buttonFriendStatus.setEnabled(false);
                buttonFriendStatus.setBackgroundResource(R.drawable.shape_button_gray);
                buttonFriendStatus.setText(context.getString(R.string.wating_for_comfirm));
                user.friendshipRelationStatus = FriendshipRelation.Status.REQUEST_SENT;
                break;
//            case REQUEST_CANCEL_FRIENDSHIP_INT_CODE:
//                break;
        }
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
        progressDialog.dismiss();
        if (!dialogMessage.isShowing()) {
            dialogMessage.show();
            dialogMessage.setMessage(ServerAnswer.getError(context, errorCode, callerStringID + ">" + TAG));
        }
    }

    public void noPosts() {
        if (posts.size() == 0) {
            if (user.friendshipRelationStatus == FriendshipRelation.Status.FRIEND) {
                txtNothing.setText(R.string.no_posts);
                imgNothing.setImageResource(R.mipmap.ic_ops);
            } else {
                txtNothing.setText(R.string.only_friends);
                imgNothing.setImageResource(R.mipmap.ic_locked);
            }
            viewNothing.setVisibility(View.VISIBLE);
        } else {
            viewNothing.setVisibility(View.GONE);
        }
    }
}
