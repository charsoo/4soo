package ir.rasen.charsoo.view.interface_m;

/**
 * Created by android on 5/4/2015.
 */
public interface ISelectCategory {
    void notifySelectCategory(String categoryListPosition);
    void notifySelectSubcategory(String subcategoryListPosition);
}
