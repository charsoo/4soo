package ir.rasen.charsoo.view.activity.user;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;

import java.io.File;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.PersianDate;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.Sex;
import ir.rasen.charsoo.controller.helper.Validation;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.model.user.GetUserProfileInfo;
import ir.rasen.charsoo.model.user.UpdateUserProfile;
import ir.rasen.charsoo.view.activity.ActivityCamera;
import ir.rasen.charsoo.view.activity.ActivityGallery;
import ir.rasen.charsoo.view.activity.ActivityMain;
import ir.rasen.charsoo.view.dialog.DialogClearSearchHistoryConfirmation;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.dialog.DialogSaveResult;
import ir.rasen.charsoo.view.dialog.PopupSelectCameraGallery;
import ir.rasen.charsoo.view.fragment.FragmentUser;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.widgets.PersianDatePicker;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.edittexts.EditTextFont;

public class ActivityUserProfile extends CharsooActivity implements View.OnClickListener, IWebservice,TryAgainListener {

    private static final int GET_USER_PROFILE_INFO_INT_CODE=11,UPDATE_USER_PROFILE_INT_CODE=14
            , CHANGE_PASSWORD_ACTIVITY = 22;


    ImageView imageViewCover;
    EditTextFont editTextAboutMe, editTextEmail, editTextName, editTextIdentifier;
    PersianDatePicker persianDatePicker;
    Spinner spinnerSex;
    String filePath, userPictureString;
    User user, originalUser;
    DialogMessage dialogMessage;
    RelativeLayout networkFailedLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);
  
        setContentView(R.layout.activity_user_profile);

        setTitle(getString(R.string.user_profile));
        // bataye set kardane scroll dar makane avalesh


        networkFailedLayout = MyApplication.initNetworkErrorLayout(getWindow().getDecorView(),
                ActivityUserProfile.this,ActivityUserProfile.this);


        dialogMessage=new DialogMessage(this,"");

        (findViewById(R.id.btn_change_password)).setOnClickListener(this);
        (findViewById(R.id.btn_clear_search_history)).setOnClickListener(this);
        (findViewById(R.id.imageView_back)).setOnClickListener(this);
        (findViewById(R.id.btn_camera)).setOnClickListener(this);
        (findViewById(R.id.btn_user_profile_submit)).setOnClickListener(this);

        imageViewCover = (ImageView) findViewById(R.id.imageView_cover);

        editTextAboutMe = (EditTextFont) findViewById(R.id.editText_about_me);
        editTextEmail = (EditTextFont) findViewById(R.id.edt_email);


        editTextIdentifier = (EditTextFont) findViewById(R.id.editText_identifier);
        editTextName = (EditTextFont) findViewById(R.id.editText_name);

        spinnerSex = (Spinner) findViewById(R.id.spinner_sex);

        showWaitDialog();
        new GetUserProfileInfo(this,LoginInfo.getInstance().getUserUniqueId(), ActivityUserProfile.this,GET_USER_PROFILE_INFO_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }



    @Override
    public void getResult(int request, Object result) {
        hideWaitDialog();
        switch (request){
            case GET_USER_PROFILE_INFO_INT_CODE:
                //GetUserProfileInfo result

                user = (User) result;
                originalUser = (User) result;
                SimpleLoader simpleLoader = new SimpleLoader(this);
                simpleLoader.loadImage(user.profilePictureUniqueId, Image_M.LARGE, Image_M.ImageType.USER, imageViewCover);

                //initial view

                editTextAboutMe.setText(user.aboutMe);
                spinnerSex.setSelection(Sex.getInt(user.sex));

                editTextName.setSelection(editTextName.getText().length());
                editTextEmail.setText(user.email);
                persianDatePicker = (PersianDatePicker) findViewById(R.id.persianDatePicker);
                editTextIdentifier.setText(user.userIdentifier);
                editTextName.setText(user.name);

                if (!user.birthDate.equals("") && !user.birthDate.equals("null")) {
                    if (!PersianDate.getDayFromBirthDateString(user.birthDate).equals("null"))
                        persianDatePicker.setDay(Integer.parseInt(PersianDate.getDayFromBirthDateString(user.birthDate)));
                    if (!PersianDate.getMonthFromBirthDateString(user.birthDate).equals("null"))
                        persianDatePicker.setMonth(Integer.parseInt(PersianDate.getMonthFromBirthDateString(user.birthDate)));
                    if (!PersianDate.getYearFromBirthDateString(user.birthDate).equals("null"))
                        persianDatePicker.setYear(Integer.parseInt(PersianDate.getYearFromBirthDateString(user.birthDate)));
                }
                break;
            case UPDATE_USER_PROFILE_INT_CODE:
//                new DialogMessage(ActivityUserProfile.this, R.string.profile_edit, getString(R.string.dialog_success)).show();
                Toast.makeText(ActivityUserProfile.this,R.string.saved,Toast.LENGTH_LONG).show();
                try{
                    ActivityMain.iUpdateUserProfile.doUpdateUserProfileData();
                }
                catch (Exception e ) {}
                try {
                    FragmentUser.iUpdateUserProfile.doUpdateUserProfileData();
                } catch (Exception e) {}
                finish();
                break;
        }


        networkFailedLayout.setVisibility(View.GONE);
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
//        progressDialog.dismiss();
        hideWaitDialog();
        if (errorCode==ServerAnswer.NETWORK_CONNECTION_ERROR){
            networkFailedLayout.setVisibility(View.VISIBLE);


        }
        else {
            dialogMessage.show();
            dialogMessage.setMessage(ServerAnswer.getError(ActivityUserProfile.this, errorCode, callerStringID + ">" + this.getLocalClassName()));
        }

    }


    @Override
    public void onBackPressed() {
        boolean isUserDirty=false;
        if (originalUser!=null){
            if (userPictureString!=null && !userPictureString.equals(""))
                isUserDirty=true;
//            if (!isUserDirty && !originalUser.userIdentifier.equals(editTextIdentifier.getText().toString()))
//                isUserDirty=true;
            if (!isUserDirty &&
                    originalUser.aboutMe!=null &&
                    !originalUser.aboutMe.equals(editTextAboutMe.getText().toString()))
                isUserDirty=true;
            if (!isUserDirty && originalUser.sex==Sex.MALE && spinnerSex.getSelectedItemPosition()==2)
                isUserDirty=true;
            if (!isUserDirty && originalUser.sex==Sex.FEMALE && spinnerSex.getSelectedItemPosition()==1)
                isUserDirty=true;
            if (!isUserDirty && originalUser.sex==Sex.NONE && spinnerSex.getSelectedItemPosition()>0)
                isUserDirty=true;
            if (!isUserDirty && !originalUser.email.equals(editTextEmail.getText().toString()))
                isUserDirty=true;
            if (!isUserDirty && !originalUser.name.equals(editTextName.getText().toString()))
                isUserDirty=true;
            if (!isUserDirty){
                if (persianDatePicker.getDay() !=Integer.parseInt(PersianDate.getDayFromBirthDateString(originalUser.birthDate)))
                    isUserDirty=true;
                if (persianDatePicker.getMonth()!=Integer.parseInt(PersianDate.getMonthFromBirthDateString(originalUser.birthDate)))
                    isUserDirty=true;
                if (persianDatePicker.getYear()!=Integer.parseInt(PersianDate.getYearFromBirthDateString(originalUser.birthDate)))
                    isUserDirty=true;
            }

        }
        else {

        }
        if (isUserDirty){
            new DialogSaveResult(ActivityUserProfile.this).show();
        }
        else{
            finish();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageView_back:
                onBackPressed();
                break;
            case R.id.btn_camera:
                new PopupSelectCameraGallery(ActivityUserProfile.this).show();
                break;
            case R.id.btn_change_password:
                //new UNUSED_DialogChangePassword(ActivityUserProfile.this, ActivityUserProfile.this).show();
                Intent intent = new Intent(ActivityUserProfile.this, ActivityUserChangePassword.class);
                Globals.getInstance().getValue().userOld = originalUser;
                startActivityForResult(intent, CHANGE_PASSWORD_ACTIVITY);
                break;
            case R.id.btn_clear_search_history:
                new DialogClearSearchHistoryConfirmation(ActivityUserProfile.this).show();
                break;
            case R.id.btn_user_profile_submit:
                submit();
                break;
        }
    }

    public void submit(){
        if (getUserInfo()) {
//            progressDialog.show();
            showWaitDialog();
            new UpdateUserProfile(ActivityUserProfile.this, user, ActivityUserProfile.this,UPDATE_USER_PROFILE_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;

        }
    }

    private boolean getUserInfo() {
        if (!Validation.validateName(ActivityUserProfile.this, editTextName.getText().toString()).isValid()) {
            editTextName.setError(Validation.getErrorMessage());
            return false;
        }
        if ((!editTextAboutMe.getText().toString().isEmpty()) && (!Validation.validateAboutMe(ActivityUserProfile.this, editTextAboutMe.getText().toString()).isValid())) {
            editTextAboutMe.setError(Validation.getErrorMessage());
            return false;
        }
        if (!Validation.validateEmail(ActivityUserProfile.this, editTextEmail.getText().toString()).isValid()) {
            editTextEmail.setError(Validation.getErrorMessage());
            return false;
        }
        user.name = editTextName.getText().toString();
        user.aboutMe = editTextAboutMe.getText().toString();
        switch (spinnerSex.getSelectedItemPosition()) {
            case 1:
                user.sex = Sex.MALE;
                break;
            case 2:
                user.sex = Sex.FEMALE;
                break;
        }
        user.email = editTextEmail.getText().toString();


        user.birthDate = PersianDate.getBirthDateString(persianDatePicker.getDay()+"", persianDatePicker.getMonth()+"", persianDatePicker.getYear()+"");
        if (userPictureString != null)
            user.profilePicture = userPictureString;
        else
            user.profilePicture = "";

        return true;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == ActivityCamera.CAPTURE_PHOTO) {
                filePath = data.getStringExtra(ActivityCamera.FILE_PATH);
                displayCropedImage(filePath);
            } else if (requestCode == ActivityGallery.CAPTURE_GALLERY) {
                filePath = data.getStringExtra(ActivityGallery.FILE_PATH);
                displayCropedImage(filePath);
            } else if (requestCode == CHANGE_PASSWORD_ACTIVITY) {
                user.password = data.getStringExtra(Params.PASSWORD_NEW);
            }
        }

    }

    private void displayCropedImage(String filePath) {
        File file = new File(filePath);
        if (file.exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            //crop middle part of picture
            int width = getResources().getDisplayMetrics().widthPixels;
            //crop middle part of picture
            int bitmapWidth = myBitmap.getWidth();
            if (width > bitmapWidth)
                width = bitmapWidth;
            try {
                Bitmap croppedBitmap = Bitmap.createBitmap(myBitmap, 0, width / 6, width, (width / 3) * 2);
                imageViewCover.setImageBitmap(croppedBitmap);
                userPictureString = Image_M.getBase64String(filePath);
            } catch (Exception e) {
                String s = e.getMessage();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    public void back(View view) {
        onBackPressed();
    }


    @Override
    public void doTryAgainDialog() {
//        progressDialog.show();
        showWaitDialog();
        new GetUserProfileInfo(this,LoginInfo.getInstance().getUserUniqueId(), ActivityUserProfile.this,GET_USER_PROFILE_INFO_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }


    @Override
    public void doTryAgain() {
//        progressDialog.show();
        showWaitDialog();
        new GetUserProfileInfo(this,LoginInfo.getInstance().getUserUniqueId(), ActivityUserProfile.this,GET_USER_PROFILE_INFO_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

}
