package ir.rasen.charsoo.view.activity.user;

import android.net.Uri;

import java.io.File;

/**
 * Created by android on 8/25/2015.
 */
public class Utils {

    public static Uri getImageUri(String path) {
        return Uri.fromFile(new File(path));
    }
}
