package ir.rasen.charsoo.view.interface_m;

/**
 * Created by android on 3/10/2015.
 */
public interface IUnfollowBusiness {
    void notifyUnfollowBusiness(String businessId) throws Exception;
    void notifyUnfollowBusinessFailed(Integer errorCode,String callerStringID) throws Exception;
}
