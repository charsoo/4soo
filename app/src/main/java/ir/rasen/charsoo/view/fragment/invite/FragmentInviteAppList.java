package ir.rasen.charsoo.view.fragment.invite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.object.ContactEntry;
import ir.rasen.charsoo.controller.object.PackageInfoCustom;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.model.user.GetUserHomeInfo;
import ir.rasen.charsoo.model.user.GetUserProfileInfo;
import ir.rasen.charsoo.view.adapter.AdapterInviteFriendsByApp;
import ir.rasen.charsoo.view.adapter.AdapterInviteContacts;
import ir.rasen.charsoo.view.interface_m.IFragInviteSelectionListener;
import ir.rasen.charsoo.view.interface_m.IInviteFriendByAppListener;
import ir.rasen.charsoo.view.widgets.buttons.ButtonFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.imageviews.RoundedSquareImageView;

/**
 * Created by hossein-pc on 6/9/2015.
 */
public class FragmentInviteAppList
        extends Fragment
        implements IInviteFriendByAppListener, IFragInviteSelectionListener {

    public static final String TAG = "FragmentInviteAppList";
    public static Context context;
    Activity parentactivity;
    public String BODY, body1, body2, body3, body4, body5, body6, body7, body8, Userid, Businessid, Businessname;
    public String subject;


    //        TextViewFont persianLicenseTextView,englishLicenseTextView;
    ArrayList<PackageInfoCustom> applicationList;
    Hashtable<String, PackageInfoCustom> applicationsHashtable;
    ListView listViewApplication, listViewEmailContacts;
    AdapterInviteFriendsByApp listViewAdapter;
    LinearLayout selectedContactsContainer;
    ArrayList<ContactEntry> noneCharsooContactList;
    AdapterInviteContacts noneCharsooEmailContactsAdapter;
    ButtonFont sendEmailButton;
    Hashtable<Integer, ContactEntry> selectedContactsToInvite;
    Hashtable<Integer, Integer> positionMapForSelectedContacts;
    HorizontalScrollView selectedContactsScrollView;
    int selectedItemHeight, selectedItemMargin;
    LinearLayout.LayoutParams params;
    public CharsooActivity charsooActivityContext;
    public static String x;

    View view;


    boolean isHidingContactView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CustomActivityOnCrash.install(getActivity());

        view = inflater.inflate(R.layout.fragment_register_user_offer_friends_invite,
                container, false);
        context = parentactivity = getActivity();
        charsooActivityContext=(CharsooActivity)getActivity();
        charsooActivityContext.showWaitDialog();
//        x = GetUserHomeInfo.name;
        listViewApplication = (ListView) view.findViewById(R.id.applicationsListView);
        listViewEmailContacts = (ListView) view.findViewById(R.id.allContactsListView);
        sendEmailButton = (ButtonFont) view.findViewById(R.id.btn_SendEmail);
        sendEmailButton.setBackgroundColor(getActivity().getResources().getColor(R.color.primaryColor));

        selectedContactsScrollView = (HorizontalScrollView) view.findViewById(R.id.selectedContactsScrollView);
        selectedContactsContainer = (LinearLayout) view.findViewById(R.id.ll_SelectedContactsContainer);
        sendEmailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail();
            }
        });

        if (selectedContactsToInvite == null)
            selectedContactsToInvite = new Hashtable<>();
        if (positionMapForSelectedContacts == null)
            positionMapForSelectedContacts = new Hashtable<>();

        if ((applicationList != null) && (noneCharsooContactList != null)) {
            listViewAdapter = new AdapterInviteFriendsByApp(getActivity(), applicationList, noneCharsooContactList, FragmentInviteAppList.this);
            listViewApplication.setAdapter(listViewAdapter);
        }
        selectedItemHeight = getSizeInPixelFromDp(38);
        selectedItemMargin = getSizeInPixelFromDp(16);
        params = new LinearLayout.LayoutParams(selectedItemHeight, selectedItemHeight);
        params.setMargins(0, 0, selectedItemMargin, 0);


        isHidingContactView = false;

        if (!FragmentInvite.isBusinessPromotion) {
            // matne aslie email

            // matne invite
            Userid = LoginInfo.getInstance().userUniqueName + "@";
            x = LoginInfo.getInstance().username;
            body1 = getActivity().getString(R.string.EMAIL_BODY_1);
            body2 = getActivity().getString(R.string.EMAIL_BODY_2);
            body3 = getActivity().getString(R.string.EMAIL_BODY_3);
            body4 = getActivity().getString(R.string.EMAIL_BODY_4);
            body5 = "با جستجو " + Userid + " دوست من شوید.";
            body6 = getActivity().getString(R.string.EMAIL_BODY_6);
            body7 = getActivity().getString(R.string.EMAIL_BODY_7);
            body8 = getActivity().getString(R.string.EMAIL_BODY_8);

            BODY = body1 + '\n' + body2 + '\n' + body3 + '\n' + body4 + '\n' + body5 + '\n' + body6 + '\n' + body7 + '\n' + body8;
            subject =x + '\n' + " شما را به چارسو دعوت کرده است";

        }
        else {
            // todo: business uniqueId  & name   & sms
            // matne email = matne 2vom ( kasbo kar )
//            Businessid=String.valueOf(business.businessIdentifier)+"@";
            Businessid = FragmentInvite.Business_id + "@";

            body1 = getActivity().getString(R.string.EMAIL_BODY_1);
            body2 = getActivity().getString(R.string.EMAIL_FORMAT2_BODY2);
            body3 = getActivity().getString(R.string.EMAIL_FORMAT2_BODY3);
            body4 = "با جستجو " + Businessid + " کسب و کار من را دنبال کنید.";
            body5 = getActivity().getString(R.string.EMAIL_BODY_7);
            body6 = getActivity().getString(R.string.EMAIL_BODY_8);
            BODY = body1 + '\n' + body2 + '\n' + body3 + '\n' + body4 + '\n' + body5 + '\n' + body6;

//            Businessname="@"+String.valueOf(business.name);
            Businessname = FragmentInvite.Business_name;
            subject = Businessname + '\n' + " شعبه جدید ما در چارسو افتتاح شد.";

        }

        return view;

    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        charsooActivityContext.hideWaitDialog();
        if (!selectedContactsToInvite.isEmpty()) {
            selectedContactsScrollView.setVisibility(View.VISIBLE);
            positionMapForSelectedContacts = new Hashtable<>();
            ArrayList<Integer> keys = new ArrayList<>(selectedContactsToInvite.keySet());
            for (Integer i = 0; i < keys.size(); i++) {
                positionMapForSelectedContacts.put(keys.get(i), i);
                RoundedSquareImageView r = new RoundedSquareImageView(getActivity());

                r.setLayoutParams(params);
                final int position = keys.get(i);
                r.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        noneCharsooEmailContactsAdapter.setUncheckedViewAt(position);
                        removeSelectedContact(position);
                    }
                });
                if (selectedContactsToInvite.get(keys.get(i)).contactPhoto != null) {
                    r.setImageBitmap(selectedContactsToInvite.get(keys.get(i)).contactPhoto);
                }
                selectedContactsContainer.addView(r);
            }

        }
    }

    @Override
    public void onItemClicked(String itemTag) {
        Intent myIntent;
        switch (itemTag) {
            case Params.EMAIL_APP:
                (view.findViewById(R.id.ll_EmailListContainer)).setVisibility(View.VISIBLE);
                if (noneCharsooEmailContactsAdapter == null) {
                    noneCharsooEmailContactsAdapter = new AdapterInviteContacts(getActivity(), noneCharsooContactList, FragmentInviteAppList.this);
                    listViewEmailContacts.setAdapter(noneCharsooEmailContactsAdapter);
                }

                break;
            case Params.SHARE_APP:
                myIntent = new Intent(Intent.ACTION_SEND);
                myIntent.setType("text/html");
                myIntent.putExtra(Intent.EXTRA_TEXT, BODY);//
                getActivity().startActivity(Intent.createChooser(myIntent, "Share with"));
                break;
            default:
                myIntent = new Intent(Intent.ACTION_SEND);
                myIntent.setType("text/plain");
                String s2 = applicationsHashtable.get(itemTag).pname;
                myIntent.setPackage(s2);
                myIntent.putExtra(Intent.EXTRA_TEXT, BODY);//
                getActivity().startActivity(Intent.createChooser(myIntent, "Share with"));
                break;
        }
    }

    public static Intent createEmailIntent(final String[] toEmail,
                                           final String subject,
                                           final String message) {
        String emailsString = "";
        if (toEmail.length > 0)
            emailsString = toEmail[0];

        for (int i = 1; i < toEmail.length; i++) {
            emailsString += ",";
            emailsString += toEmail[i];
        }
        Intent sendTo = new Intent(Intent.ACTION_SENDTO);
        String uriText = "mailto:" + Uri.encode(emailsString) +
                "?subject=" + Uri.encode(subject) +
                "&body=" + Uri.encode(message);
        Uri uri = Uri.parse(uriText);
        sendTo.setData(uri);
        sendTo.putExtra("exit_on_sent", true);
        List<ResolveInfo> resolveInfos = context.getPackageManager().queryIntentActivities(sendTo, 0);

        // Emulators may not like this check...
        if (!resolveInfos.isEmpty()) {
            return Intent.createChooser(sendTo, "Send Email");
        }

        // Nothing resolves send to, so fallback to send...
        Intent send = new Intent(Intent.ACTION_SEND);

        send.setType("message/rfc822");
        send.putExtra(Intent.EXTRA_EMAIL, toEmail);
        send.putExtra(Intent.EXTRA_SUBJECT, subject);
        send.putExtra(Intent.EXTRA_TEXT, message);

        return Intent.createChooser(send, "Send Email");
    }


    private void doOnApplicationListReady() {
        if (listViewAdapter != null)
            listViewAdapter.resetItems(applicationList);

        if (view != null) {
            if ((applicationList != null) && (noneCharsooContactList != null)) {
                listViewAdapter = new AdapterInviteFriendsByApp(getActivity(), applicationList, noneCharsooContactList, FragmentInviteAppList.this);
                listViewApplication.setAdapter(listViewAdapter);
            }
        }
    }

    public void setApplicationList(ArrayList<PackageInfoCustom> appList) {
        if (applicationList == null) {
            applicationList = appList;
            applicationsHashtable = new Hashtable<>();
            for (int i = 0; i < applicationList.size(); i++) {
                applicationsHashtable.put(applicationList.get(i).appname, applicationList.get(i));
            }
            doOnApplicationListReady();
        }
    }

    public void setNoneCharsooContacts(ArrayList<ContactEntry> noneCharsooContacts) {
        noneCharsooContactList = new ArrayList<>(noneCharsooContacts);
        if (view != null) {
            if ((applicationList != null) && (noneCharsooContactList != null)) {
                listViewAdapter = new AdapterInviteFriendsByApp(getActivity(), applicationList, noneCharsooContactList, FragmentInviteAppList.this);
                listViewApplication.setAdapter(listViewAdapter);
            }
        }
    }

    private void removeSelectedContact(int itemPosition) {
        selectedContactsToInvite.remove(itemPosition);
        int removePosition = positionMapForSelectedContacts.get(itemPosition);
        selectedContactsContainer.removeViewAt(removePosition);
        for (int i : positionMapForSelectedContacts.keySet()) {
            int tempInt = positionMapForSelectedContacts.get(i);
            if (tempInt > removePosition) {
                positionMapForSelectedContacts.put(i, tempInt - 1);
            }
        }
        positionMapForSelectedContacts.remove(itemPosition);

        checkSelectedViewVisibility();
    }

    @Override
    public void onItemCheckBoxClicked(final int position) {
        if (selectedContactsToInvite.containsKey(position)) {
            removeSelectedContact(position);
        } else {
            selectedContactsToInvite.put(position, noneCharsooContactList.get(position));
            positionMapForSelectedContacts.put(position, selectedContactsContainer.getChildCount());
            RoundedSquareImageView r = new RoundedSquareImageView(getActivity());

            r.setLayoutParams(params);
            r.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    noneCharsooEmailContactsAdapter.setUncheckedViewAt(position);
                    removeSelectedContact(position);
                }
            });
            if (selectedContactsToInvite.get(position).contactPhoto != null) {
                r.setImageBitmap(selectedContactsToInvite.get(position).contactPhoto);
            } else {
//                r.setImageDrawable(selectedContactsToInvite.get(position).contactPhotoDrawable);

            }
            selectedContactsContainer.addView(r);

            selectedContactsContainer.post(new Runnable() {
                @Override
                public void run() {
                    selectedContactsScrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                }
            });

        }
        checkSelectedViewVisibility();
    }

    private void checkSelectedViewVisibility() {
        if (selectedContactsToInvite.isEmpty()) {
            if (selectedContactsScrollView.getVisibility() == View.VISIBLE) {
                YoYo.with(Techniques.FadeOutDown).withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        isHidingContactView = true;
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (isHidingContactView) {
                            selectedContactsScrollView.setVisibility(View.GONE);
                            isHidingContactView = false;
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).playOn(selectedContactsScrollView);
            }
        } else {
            if (selectedContactsScrollView.getVisibility() == View.GONE || isHidingContactView) {
                isHidingContactView = false;
                YoYo.with(Techniques.FadeInUp).playOn(selectedContactsScrollView);
                selectedContactsScrollView.setVisibility(View.VISIBLE);
            }
        }
    }


    private void sendEmail() {
        // create EMAIL SUBJECT & BODY  /  Bodu dar oncreate view sakhte shode

        int size = selectedContactsToInvite.size();
        String[] emailsStringArray = new String[size];
        ArrayList<Integer> keys = new ArrayList<>(selectedContactsToInvite.keySet());
        for (int i = 0; i < size; i++) {
            emailsStringArray[i] = selectedContactsToInvite.get(keys.get(i)).contactData;
        }
        startActivityForResult(createEmailIntent(emailsStringArray, subject, BODY), 12);
    }

    public int getSizeInPixelFromDp(int dpToConvert) {
        Resources r = getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpToConvert, r.getDisplayMetrics());
        return (int) px;
    }


    public boolean ifHideEmailList() {
        if (view != null) {
            if ((view.findViewById(R.id.ll_EmailListContainer)).getVisibility() == View.VISIBLE) {
                (view.findViewById(R.id.ll_EmailListContainer)).setVisibility(View.GONE);
                return true;
            } else
                return false;
        } else
            return false;

    }
    public String getname(){

        return x;
    }

}
