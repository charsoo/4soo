package ir.rasen.charsoo.view.activity.user;

import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;

import com.flurry.android.FlurryAgent;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.view.fragment.FragmentUser;
import ir.rasen.charsoo.view.fragment.FragmentUserOther;
import ir.rasen.charsoo.view.shared.PostInitializerBothView;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;

public class ActivityUserOther extends CharsooActivity /*implements INetworkStatusListener*/ {

    String visitedUserIntId,visitorIntId;
    FragmentUserOther fragOther;
    FragmentUser fragUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.activity_user_other_new);

        visitedUserIntId = getIntent().getExtras().getString(Params.VISITED_USER_UNIQUE_ID);
        visitorIntId=LoginInfo.getInstance().getUserUniqueId();


        showWaitDialog();
        if (visitorIntId.equals(visitedUserIntId)){
            fragUser=new FragmentUser();
//            fragUser.setiNetworkStatusListener(ActivityUserOther.this);
            FragmentTransaction ft= getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragmentContainer,fragUser);
            ft.commit();
        }
        else
        {
            fragOther=new FragmentUserOther();
//            fragOther.setNetworkStatusListener(ActivityUserOther.this);
            FragmentTransaction ft= getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragmentContainer,fragOther);
            ft.commit();
        }
    }

    @Override
    public void onBackPressed() {
        if(PostInitializerBothView.dismissAllPopupsIfAnyShowing())
            return;

        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    //    @Override
//    public void onNetworkStatusChanged(boolean isNetworkConnected) {
//        if (!isNetworkConnected) {
//            (findViewById(R.uniqueId.networkErrorLayout)).setVisibility(View.VISIBLE);
//            (findViewById(R.uniqueId.netErrorLayout_btn_back)).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    finish();
//                }
//            });
//        }
//        else{
//            (findViewById(R.uniqueId.networkErrorLayout)).setVisibility(View.GONE);
//        }
//    }
}
