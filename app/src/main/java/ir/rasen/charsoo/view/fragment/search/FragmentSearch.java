package ir.rasen.charsoo.view.fragment.search;

/**
 * Created by Sina KH on 6/23/2015.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.object.Category;
import ir.rasen.charsoo.controller.object.ParentCategory;
import ir.rasen.charsoo.controller.object.SubCategory;
import ir.rasen.charsoo.model.business.GetBusinessCategories;
import ir.rasen.charsoo.model.business.GetBusinessSubcategories;
import ir.rasen.charsoo.model.search.SearchBusinessesLocation;
import ir.rasen.charsoo.model.search.SearchPost;
import ir.rasen.charsoo.model.search.SearchUser;
import ir.rasen.charsoo.view.activity.ActivityMain;
import ir.rasen.charsoo.view.activity.ActivityMapChoose;
import ir.rasen.charsoo.view.adapter.AdapterCategories;
import ir.rasen.charsoo.view.adapter.AdapterSearchTabs;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.PagerSlidingTabStrip;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.material_library.widgets.Dialog;

public class FragmentSearch extends Fragment implements IWebservice {
    public static final String TAG = "FragmentSearch";

    private static final int REQUEST_CATEGORIES = 1, REQUEST_SUBCATEGORIES = 2;
    private static final int SEARCH_BUSINESS_LOCATION_INT_CODE=11,SEARCH_POST_INT_CODE=14,SEARCH_USER_INT_CODE=19;

    private static final int TWO_MINUTES = 1000 * 60 * 2;

    private static final int SEARCH_MODE_NORMAL = 0, SEARCH_MODE_AUTO = 1;

    public enum SearchType {BUSINESSES, PRODUCTS, USERS}

    private SearchType searchType;

    private PagerSlidingTabStrip tabs;
    private ViewPager viewPager;
    private AdapterSearchTabs adapter;
    private AdapterCategories adapterCategories;
    private EditText search;
    private EditText place;
    private View placeView;
    private View headers;
    private TextViewFont status;
    private RelativeLayout waitView;
    public CharsooActivity charsoocontext;
    private String subcatId;
    int loadingIndex=-1;

    private ActivityMain activityMain;
    private boolean searching = true;
    private LocationManager locManager;
    private Location location;
    private GoogleMap googleMap;
    private MapView mapView;
    private MarkerOptions marker;
    private ImageView cleanSearch;
    private Bundle savedInstanceState;
    private boolean headersVisibility = true;
    String latitude="",longitude="";

    private DrawerLayout drawerLayout;
    private ListView rvCategories;

    private ArrayList<ParentCategory> parentCategories;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CustomActivityOnCrash.install(getActivity());

        View view = inflater.inflate(R.layout.fragment_search,
                container, false);
        charsoocontext=(CharsooActivity) getActivity();

        activityMain = (ActivityMain) getActivity();
        activityMain.lockDrawer();
        init(view);
        this.savedInstanceState = savedInstanceState;
        initFilters();

        return view;
    }

    private void init(View view) {
        tabs = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        viewPager.setOffscreenPageLimit(3);
        search = (EditText) view.findViewById(R.id.edt_search);
        place = (EditText) view.findViewById(R.id.edt_place);
        placeView = view.findViewById(R.id.view_place);
        waitView =(RelativeLayout) view.findViewById(R.id.waitView);
        status = (TextViewFont) waitView.findViewById(R.id.waitView_status);
        headers = view.findViewById(R.id.view_header);
        waitView.setVisibility(View.GONE);
        adapter = new AdapterSearchTabs(getChildFragmentManager(), getActivity());
        drawerLayout = (DrawerLayout) view.findViewById(R.id.drawer_layout);
        cleanSearch = (ImageView) view.findViewById(R.id.cleanSearch);
        mapView = (MapView) view.findViewById(R.id.mapView);
        parentCategories = new ArrayList<>();
        rvCategories = (ListView) drawerLayout.findViewById(R.id.categories);

        viewPager.setAdapter(adapter);
        tabs.setViewPager(viewPager);
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        hideFilters();
                        searchType = SearchType.PRODUCTS;
                        FragmentSearchProduct fragmentSearchProduct =
                                ((FragmentSearchProduct) adapter.getFragment(searchType));
                        if (fragmentSearchProduct != null)
                            if (!fragmentSearchProduct.getCurrentResultIsFor().equals(search.getText().toString()))
                                searchNow(SEARCH_MODE_AUTO);
                        break;
                    case 1:
                        searchType = SearchType.BUSINESSES;
                        FragmentSearchBusiness fragmentSearchBusiness =
                                ((FragmentSearchBusiness) adapter.getFragment(searchType));
                        if (fragmentSearchBusiness != null)
                            if (!fragmentSearchBusiness.getCurrentResultIsFor().equals(search.getText().toString()))
                                searchNow(SEARCH_MODE_AUTO);
                        if (search.getText().length() > 0)
                            showFilters();
                        break;
                    case 2:
                        hideFilters();
                        searchType = SearchType.USERS;
                        FragmentSearchUser fragmentSearchUser =
                                ((FragmentSearchUser) adapter.getFragment(searchType));
                        if (fragmentSearchUser != null)
                            if (!fragmentSearchUser.getCurrentResultIsFor().equals(search.getText().toString()))
                                searchNow(SEARCH_MODE_AUTO);
                        break;
                }
                showFloatingViews();
                search.setHint(getHint(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchNow();
                }
                return false;
            }
        });
        search.addTextChangedListener(new TextWatcher() {
            private boolean notZero = false;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                notZero = s.length() > 0;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (notZero != s.length() > 0) {
                    if (search.getText().length() == 0)
                        cleanSearch.setImageResource(R.mipmap.ic_back_drw);
                    else
                        cleanSearch.setImageResource(R.mipmap.ic_clean_drw);
                    notZero = s.length() > 0;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        search.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(searchType!=SearchType.BUSINESSES)
                    return false;
                showFilters();
                if (mapView != null && mapView.getVisibility() == View.VISIBLE) {
                    mapView.setVisibility(View.GONE);
                    return true;
                }
                return false;
            }
        });
        view.findViewById(R.id.searchNow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchNow();
            }
        });
        view.findViewById(R.id.cleanSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (search.getText().length() == 0)
                    getActivity().onBackPressed();
                else
                    search.setText("");
            }
        });
        view.findViewById(R.id.showFilters).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });
        view.findViewById(R.id.showMap).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mapView != null && mapView.getVisibility() == View.VISIBLE)
                    mapView.setVisibility(View.GONE);
                else
                    initMapView();
            }
        });

        viewPager.setCurrentItem(1);
        placeView.setVisibility(View.GONE);
        tabs.setVisibility(View.VISIBLE);
        setUpGeneralFeatures(view);
        setUpLocationServices();
    }

    private void setUpGeneralFeatures(View view) {
        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    activityMain.hideSoftKeyboard();
                    return false;
                }

            });
        }
        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setUpGeneralFeatures(innerView);
            }
        }
    }

    private void searchNow() {
        searchNow(SEARCH_MODE_NORMAL);
    }

    private void searchNow(int status) {
        activityMain.hideSoftKeyboard();

        String searchKey = search.getText().toString().trim();
        search.setText(searchKey);
        String placeStr = place.getText().toString().trim();
        place.setHint(R.string.selected_location);
        place.setText("");

        if (searchKey.length() == 0) {
            if (status == SEARCH_MODE_NORMAL)
                search.setError(getString(R.string.err_fill_search_box));
//            new DialogMessage(getActivity(), R.string.search, R.string.err_fill_search_box).show();
            return;
        }

        switch (searchType) {
            case BUSINESSES:

                if(searchKey.length() !=0)
                    if(placeStr.length() !=0)
                        new SearchBusinessesLocation(getActivity(), searchKey,"0", ActivityMapChoose.templat + "", ActivityMapChoose.templong + "", 0, getResources().getInteger(R.integer.lazy_load_limitation), FragmentSearch.this,SEARCH_BUSINESS_LOCATION_INT_CODE, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                 else if (placeStr.length() == 0) {
                        if (location == null) {
                            if (status == 0) locationIsNull();
                            return;
                        }
                        new SearchBusinessesLocation(getActivity(), searchKey,
                                subcatId, location.getLatitude() + "", location.getLongitude() + "", 0,
                                getResources().getInteger(R.integer.lazy_load_limitation),
                                FragmentSearch.this, SEARCH_BUSINESS_LOCATION_INT_CODE, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
                    }



                ((FragmentSearchBusiness) adapter.getFragment(searchType)).setCurrentResultIsFor(searchKey);
                ((FragmentSearchBusiness) adapter.getFragment(searchType)).reset();
                search.setText("");
                break;
            case PRODUCTS:
                new SearchPost(getActivity(), searchKey, 0, getResources().getInteger(R.integer.lazy_load_limitation), FragmentSearch.this,SEARCH_POST_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
                ((FragmentSearchProduct) adapter.getFragment(searchType)).setCurrentResultIsFor(searchKey);
                ((FragmentSearchProduct) adapter.getFragment(searchType)).reset();
                break;
            case USERS:
                new SearchUser(getActivity(), searchKey, 0, getResources().getInteger(R.integer.lazy_load_limitation), FragmentSearch.this,SEARCH_USER_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
                ((FragmentSearchUser) adapter.getFragment(searchType)).setCurrentResultIsFor(searchKey);
                ((FragmentSearchUser) adapter.getFragment(searchType)).reset();
                break;
        }

        searchStarted();

    }


    @Override
    public void getResult(int request, Object result) {

        searchFinished();
        switch (request) {
            case REQUEST_CATEGORIES:

                ArrayList<ParentCategory> tmp = ParentCategory.fromArrayCategories((ArrayList<Category>) result);
                parentCategories.clear();
                parentCategories.add(new ParentCategory("0", getString(R.string.filter_all), true));
                parentCategories.addAll(tmp);
                adapterCategories = new AdapterCategories(getActivity(), parentCategories, FragmentSearch.this);
                rvCategories.setAdapter(adapterCategories);
                break;
            case REQUEST_SUBCATEGORIES:
//                pbCategories.setVisibility(View.GONE);
                ArrayList<ParentCategory> tmpSub = ParentCategory.fromArraySubCategories((ArrayList<SubCategory>) result);
                parentCategories.addAll(loadingIndex + 1, tmpSub);
                adapterCategories.notifyDataSetChanged();
                loadingIndex = -1;
                break;
            case SEARCH_BUSINESS_LOCATION_INT_CODE:
                searchFinished();
                ((FragmentSearchBusiness) adapter.getFragment(searchType))
                        .getResult(FragmentSearchBusiness.SEARCH_BUSINESS_LOCATION_REQUEST,result);
                search.setText("");
                break;
            case SEARCH_POST_INT_CODE:
                searchFinished();
                ((FragmentSearchProduct) adapter.getFragment(searchType))
                        .getResult(FragmentSearchProduct.SEARCH_POST,result);
                break;
            case SEARCH_USER_INT_CODE:
                searchFinished();
                ((FragmentSearchUser) adapter.getFragment(searchType))
                        .getResult(FragmentSearchUser.SEARCH_USER_REQUEST,result);
                break;
        }
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
        searchFinished();
//        getError(errorCode, callerStringID);
    }

//    @Override
//    public void getError(Integer errorCode, String callerStringID) {
//        try {
//            searchFinished();
//            //progressDialog.dismiss();
//            new DialogMessage(getActivity(), ServerAnswer.getError(getActivity(), errorCode, callerStringID + ">")).show();
//        } catch (Exception e) {
//
//        }
//    }

    private String getHint(int position) {
        switch (position) {
            case 0:
                return getString(R.string.search_products);
            case 1:
                return getString(R.string.search_businesses);
            case 2:
                return getString(R.string.search_users);
            default:
                return null;
        }
    }

    public void searchStarted() {
        searching = true;
        status.setText(getString(R.string.doing) + " " + getHint(viewPager.getCurrentItem()));
        waitView.setVisibility(View.VISIBLE);
    }

    public void searchFinished() {
        searching = false;
        status.setVisibility(View.GONE);
        waitView.setVisibility(View.GONE);
    }

    public boolean isSearching() {
        return searching;
    }

    // set place edit text view visibility
    private void hideFilters() {
        if(tabs.getVisibility()==View.GONE) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            YoYo.with(Techniques.FadeOutDown).duration(200).playOn(placeView);
            YoYo.with(Techniques.FadeInUp).duration(200).playOn(tabs);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    placeView.setVisibility(View.GONE);
                    place.setText("");
                    tabs.setVisibility(View.VISIBLE);
                }
            }, 200);
        }
    }

    private void setUpLocationServices() {
        locManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        location = locManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null)
            location = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000L, 500.0f, locationListener);
    }

    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location newLocation) {
            if (isBetterLocation(newLocation, location)) {
                location = newLocation;
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    @Override
    public void onDestroy() {
        // handler.removeCallbacks(sendUpdatesToUI);
        super.onDestroy();
        locManager.removeUpdates(locationListener);
    }

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    private void locationIsNull() {
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            Dialog dialog = new Dialog(getActivity(), getString(R.string.search_businesses), getString(R.string.activate_location_services));
            dialog.addCancelButton(getString(R.string.cancel));
            dialog.setOnAcceptButtonClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    getActivity().startActivity(myIntent);
                }
            });
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    chooseLocationManualy();
                }
            });
            dialog.show();
        } else if (location == null) {
            chooseLocationManualy();
        }
    }

    private void chooseLocationManualy() {
        Dialog dialog = new Dialog(getActivity(), getString(R.string.search_businesses), getString(R.string.no_location_found));
        dialog.addCancelButton(getString(R.string.cancel));
        dialog.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initMapView();
            }
        });
        dialog.show();
    }

    private void initMapView() {


        Intent intent = new Intent(getActivity(), ActivityMapChoose.class);
        intent.putExtra(Params.IS_EDITTING, false);
        startActivityForResult(intent, Params.ACTION_CHOOSE_LOCATION);

//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                place.setText(ActivityMapChoose.templat + "," + ActivityMapChoose.templong);
//            }
//        }, 1000);
        //Map section
//        MapsInitializer.initialize(this.getActivity());
//        mapView.onCreate(savedInstanceState);
//        mapView.setVisibility(View.VISIBLE);
//
//        if (!LocationManagerTracker.isGooglePlayServicesAvailable(getActivity())) {
//            Dialog dialog = new Dialog(getActivity(), getString(R.string.err), getString(R.string.err_map_loading_description));
//            dialog.addCancelButton(getString(R.string.ok));
//            dialog.setAcceptText(getString(R.string.install_now));
//            dialog.setOnAcceptButtonClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    final String appPackageName = "com.google.android.gms";
//                    try {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?uniqueId=" + appPackageName)));
//                    } catch (android.content.ActivityNotFoundException anfe) {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
//                    }
//                }
//            });
//            dialog.show();
//            mapView.setVisibility(View.GONE);
//        } else if (mapView != null) {
//            googleMap = mapView.getMap();
//            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
//            googleMap.setMyLocationEnabled(true);
//            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
//                    new LatLng(Double.valueOf(getString(R.string.sample_latitude)), Double.valueOf(getString(R.string.sample_longitude))), 5));
//            // Zoom in, animating the camera.
//            googleMap.animateCamera(CameraUpdateFactory.zoomTo(5), 2000,
//                    null);
//            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//                @Override
//                public void onMapClick(LatLng latLng) {
//                    if (marker == null) {
//                        marker = new MarkerOptions().position(
//                                latLng);
//                        googleMap.addMarker(marker);
//                        marker.draggable(true);
//                    } else {
//                        marker.position(latLng);
//                    }
////                    mapView.setVisibility(View.GONE);
//                    place.setText(marker.getPosition().latitude + "," + marker.getPosition().longitude);
//                }
//            });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode ==  Params.ACTION_CHOOSE_LOCATION) {

                place.setText(ActivityMapChoose.templat + "," + ActivityMapChoose.templong);
                latitude = ActivityMapChoose.templat;
                longitude = ActivityMapChoose.templong;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    ;
    private int mLastFirstVisibleItem;

    public void onScroll(int firstVisibleItem) {
        if (mLastFirstVisibleItem != -1) {
            if (mLastFirstVisibleItem < firstVisibleItem && headersVisibility) {
                YoYo.with(Techniques.FadeOutUp).duration(200).playOn(headers);
                headersVisibility = false;
            }
            if (mLastFirstVisibleItem > firstVisibleItem && !headersVisibility) {
                showFloatingViews();
            }
        }
        mLastFirstVisibleItem = firstVisibleItem;
    }

    private void showFloatingViews() {
        if (!headersVisibility) {
            YoYo.with(Techniques.FadeInDown).duration(200).playOn(headers);
            headersVisibility = true;
        }
    }

    private void showFilters() {
        if (placeView.getVisibility() == View.GONE) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            placeView.setVisibility(View.VISIBLE);
            tabs.setVisibility(View.GONE);
            YoYo.with(Techniques.FadeInDown).duration(200).playOn(placeView);
            YoYo.with(Techniques.FadeOutDown).duration(200).playOn(tabs);
        }
    }

    private void initFilters() {
        //    pbCategories.setVisibility(View.VISIBLE);
        drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                ((ActivityMain) getActivity()).lockDrawer();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                ((ActivityMain) getActivity()).unlockDrawer();
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
        new GetBusinessCategories(getActivity(), FragmentSearch.this, REQUEST_CATEGORIES, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
    }

    public boolean back() {
        if (mapView != null && mapView.getVisibility() == View.VISIBLE) {
            mapView.setVisibility(View.GONE);
            return true;
        }
        if(tabs.getVisibility()==View.GONE) {
            hideFilters();
            return true;
        }
        return false;
    }

    public void catSelected(String id, int index) {
        if (loadingIndex == -1) {
            loadingIndex = index;
//            pbCategories.setVisibility(View.VISIBLE);
            new GetBusinessSubcategories(getActivity(), id, FragmentSearch.this,
                    REQUEST_SUBCATEGORIES,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
        }
    }

    public void subcatSelected(String id) {
        if (searchType == SearchType.BUSINESSES)
            searchNow(SEARCH_MODE_AUTO);
        subcatId = id;
        drawerLayout.closeDrawer(Gravity.LEFT);
    }

    public void catUnselected(int position) {
        int i = position + 1;
        while (i < parentCategories.size() && parentCategories.get(i).isSub) {
            parentCategories.remove(i);
        }
        adapterCategories.notifyDataSetChanged();
    }

}