package ir.rasen.charsoo.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.FriendshipRelation;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.PullToRefreshGrid;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.post.GetPost;
import ir.rasen.charsoo.model.post.GetSharedPosts;
import ir.rasen.charsoo.model.user.GetUserHomeInfo;
import ir.rasen.charsoo.view.activity.ActivityMain;
import ir.rasen.charsoo.view.activity.user.ActivityUserOther;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.INetworkStatusListener;
import ir.rasen.charsoo.view.interface_m.IPostUpdateListener;
import ir.rasen.charsoo.view.interface_m.IPullToRefresh;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.shared.GridViewUserOtherBothView;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.HFGridView;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.PullToRefreshGridViewWithHeaderAndFooter;

public class FragmentUserOther extends Fragment implements IWebservice, IPullToRefresh,NetworkStateChangeListener,
        TryAgainListener,IPostUpdateListener{

    public static final String TAG="FragmentUserOther";

    public static final int GET_USER_HOME_INFO_INT_CODE=11,GET_SHARED_POSTS_INT_CODE=14,GET_SINGLE_POST_INT_CODE=18;
    private int lastFailedWebServiceCall;

    private enum Status {FIRST_TIME, LOADING_MORE, REFRESHING, NONE}
    private Status status;

    private HFGridView gridView;
    private String visitedUserId;
    private User user;
    GridViewUserOtherBothView gridViewUserOtherBothView;
    ArrayList<Post> posts = new ArrayList<>();
   // ActivityUserOther parentActivity;
    DialogMessage dialogMessage;
    INetworkStatusListener networkStatusListener;
    RelativeLayout networkErrorLayout;
    public static IPostUpdateListener iPostUpdateListener;
    Activity parentActivity;

    CharsooActivity mCharsooActivity;


    RelativeLayout actionBarLayout;
    ImageView imageViewBack,imageViewSearch;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CustomActivityOnCrash.install(getActivity());

        View view = inflater.inflate(R.layout.fragment_user_other,
                container, false);
        parentActivity = getActivity();
        iPostUpdateListener = FragmentUserOther.this;
        networkErrorLayout = MyApplication.initNetworkErrorLayout(view, getActivity(),FragmentUserOther.this);

        imageViewBack = (ImageView) view.findViewById(R.id.imageView_back);
        imageViewSearch = (ImageView) view.findViewById(R.id.imageView_search);

        actionBarLayout = (RelativeLayout) view.findViewById(R.id.ll_action_bar);
        actionBarLayout.setOnClickListener(null);

        if (posts == null)
            posts = new ArrayList<>();

        parentActivity = (ActivityUserOther) getActivity();
        visitedUserId = parentActivity.getIntent().getExtras().getString(Params.VISITED_USER_UNIQUE_ID);

        //set progress dialog
        mCharsooActivity = ((CharsooActivity)getActivity());
        mCharsooActivity.showWaitDialog();

        dialogMessage = new DialogMessage(parentActivity, "");


        pullToRefreshGridView = new PullToRefreshGrid(parentActivity, (PullToRefreshGridViewWithHeaderAndFooter) view.findViewById(R.id.gridView_HF), FragmentUserOther.this);
        gridView = pullToRefreshGridView.getGridViewHeaderFooter();

        status=Status.FIRST_TIME;
        callWebservice(GET_USER_HOME_INFO_INT_CODE, true);

        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        imageViewSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityMain.openThisActivityAndGotoFragmentSearch(getActivity());
            }
        });

        return view;

    }

    @Override
    public void notifyRefresh() {
        if (posts != null) {
            status = Status.REFRESHING;
            callWebservice(GET_USER_HOME_INFO_INT_CODE,false);
        }
        else
            pullToRefreshGridView.onRefreshComplete();
    }

    @Override
    public void notifyLoadMore() {
        status = Status.LOADING_MORE;
        callWebservice(GET_SHARED_POSTS_INT_CODE,false);
    }


    @Override
    public void getResult(int request, Object result) {
        mCharsooActivity.hideWaitDialog();
        switch (request){
            case GET_USER_HOME_INFO_INT_CODE:
                mCharsooActivity.hideWaitDialog();


                user = (User) result;
                gridView.setVisibility(View.VISIBLE);

                boolean beThreeColumn = gridViewUserOtherBothView == null || gridViewUserOtherBothView.isThreeColumn;
                if (gridViewUserOtherBothView == null){
                    gridViewUserOtherBothView = new GridViewUserOtherBothView(parentActivity, user, gridView,dialogMessage,FragmentUserOther.TAG,FragmentUserOther.this);
                    gridViewUserOtherBothView.InitialGridViewUser(new ArrayList<Post>(), beThreeColumn);
                }
                else
                    gridViewUserOtherBothView.refreshUserData(user);
//            if (pullToRefreshGridView.isRefreshing())
//                gridView.removeHeaderView(gridView.getHeaderView());
                if (user.friendshipRelationStatus == FriendshipRelation.Status.FRIEND)
                    callWebservice(GET_SHARED_POSTS_INT_CODE,false);
                else {
                    if (pullToRefreshGridView.isRefreshing())
                        pullToRefreshGridView.onRefreshComplete();
                    gridViewUserOtherBothView.noPosts();
                }
                break;
            case GET_SHARED_POSTS_INT_CODE:
                if (status==Status.FIRST_TIME || status==Status.REFRESHING){
                    if(pullToRefreshGridView.isRefreshing())
                        pullToRefreshGridView.onRefreshComplete();
                    posts =new ArrayList<>((ArrayList<Post>) result);
                    pullToRefreshGridView.setResultSize(posts.size());
                    gridViewUserOtherBothView.InitialGridViewUser(posts, gridViewUserOtherBothView.isThreeColumn);
                }
                else if(status==Status.LOADING_MORE){
                    posts.addAll((ArrayList<Post>) result);
                    gridViewUserOtherBothView.addMorePosts((ArrayList<Post>) result);
                }
                status=Status.NONE;
                //
                break;
            case GET_SINGLE_POST_INT_CODE :
                Post newPost = (Post) result;
                for (int i = 0; i < posts.size() ; i++) {
                    if (posts.get(i).uniqueId.equals(newPost.uniqueId)){
                        posts.set(i,MyApplication.mergeTwoPost(posts.get(i),newPost));
                        gridViewUserOtherBothView.resetPostItems(posts);
                    }
                }
//                gridViewUser.adapterPostGridAndList.updateOnePost(newPost);
//                Post newPost = (Post) result;
//                gridViewUserOtherBothView.adapterPostGridAndList.updateOnePost(newPost);
                //
                break;
        }
//        networkStatusListener.onNetworkStatusChanged(true);
        networkErrorLayout.setVisibility(View.GONE);
        NetworkConnectivityReciever.removeIfHaveListener(TAG);
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {

        mCharsooActivity.hideWaitDialog();
        pullToRefreshGridView.onRefreshComplete();
        if ((errorCode == ServerAnswer.NETWORK_CONNECTION_ERROR)){
//            networkStatusListener.onNetworkStatusChanged(false);
            networkErrorLayout.setVisibility(View.VISIBLE);
            lastFailedWebServiceCall=GET_USER_HOME_INFO_INT_CODE;
            NetworkConnectivityReciever.setNetworkStateListener(TAG, FragmentUserOther.this);
        }
        else if (!dialogMessage.isShowing()) {
            dialogMessage.show();
            dialogMessage.setMessage(ServerAnswer.getError(parentActivity, errorCode,callerStringID+">"+parentActivity.getLocalClassName()));
        }
    }

    @Override
    public void doOnNetworkConnected() {
        callWebservice(lastFailedWebServiceCall,true);
    }

    @Override
    public void doTryAgain() {
        callWebservice(lastFailedWebServiceCall,true);
    }

    private void callWebservice(int reqCode,boolean showProgressDialog){
        if (showProgressDialog)
            mCharsooActivity.showWaitDialog();
        switch (reqCode) {
            case GET_USER_HOME_INFO_INT_CODE:
                new GetUserHomeInfo(parentActivity, visitedUserId,LoginInfo.getInstance().getUserUniqueId(),
                        this, GET_USER_HOME_INFO_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                break;
            case GET_SHARED_POSTS_INT_CODE:
                if (status!=Status.LOADING_MORE){
                    new GetSharedPosts(parentActivity, visitedUserId, 0, getResources().getInteger(R.integer.lazy_load_limitation), FragmentUserOther.this,GET_SHARED_POSTS_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                }
                else
                    new GetSharedPosts(parentActivity, visitedUserId,gridViewUserOtherBothView.adapterPostGridAndList.getCount(), getResources().getInteger(R.integer.lazy_load_limitation), FragmentUserOther.this,GET_SHARED_POSTS_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
                break;
        }
    }


    //pull_to_refresh_lib
    PullToRefreshGrid pullToRefreshGridView;

    //    @Override
//    public void doOnPostModified(int postIntId) {
//        try {
//            if (posts!=null)
//                posts.clear();
//            notifyRefresh();
//        }catch (Exception e){}
//    }

    @Override
    public void onPostDetailsChange(String dirtyPostId) {
        for (int i = 0; i < posts.size(); i++) {
            if (posts.get(i).uniqueId.equals(dirtyPostId)){
                new GetPost(parentActivity,LoginInfo.getInstance().getUserUniqueId(),
                        posts.get(i).businessUniqueId,dirtyPostId, Post.GetPostType.SHARE,
                        FragmentUserOther.this,GET_SINGLE_POST_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
                break;
            }
        }
    }

}
