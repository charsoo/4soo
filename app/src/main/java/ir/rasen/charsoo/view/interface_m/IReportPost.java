package ir.rasen.charsoo.view.interface_m;

import android.widget.ImageView;

/**
 * Created by android on 3/11/2015.
 */
public interface IReportPost {
    public void notifyReportPost(String id,ImageView imageViewMore);
}

