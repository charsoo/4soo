package ir.rasen.charsoo.view.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.model.post.Report;
import ir.rasen.charsoo.view.interface_m.IReportPost;
import ir.rasen.charsoo.view.interface_m.IUpdateTimeLine;
import ir.rasen.charsoo.view.interface_m.IWebservice;


public class PopupWindowReportCancelSharePost extends PopupWindow implements IWebservice {

    private IReportPost iReportPost;
    private ImageView imageViewMore;
    private String postId;

    public static final int REPORT_REQUEST = 100;

    @SuppressLint("NewApi")
    public PopupWindowReportCancelSharePost(final Context context, final String userId, final String postId, ImageView imageViewMore, final IUpdateTimeLine iUpdateTimeLine, final IReportPost iReportPost) {
        super(context);

        this.iReportPost = iReportPost;
        this.imageViewMore = imageViewMore;
        this.postId = postId;

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_post_report_cancel_share_more, null, false);
        setContentView(view);

        View textViewCancelShare = view.findViewById(R.id.btn_post_more_cancel_share);
        textViewCancelShare.setVisibility(View.GONE);

        View textViewReport = view.findViewById(R.id.btn_post_more_report);


//
//        textViewCancelShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new CancelShare(context, LoginInfo.getUserId(context), postId, iUpdateTimeLine).executeWithNewSolution();;
//                dismiss();
//            }
//        });
        textViewReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Report(context,LoginInfo.getInstance().getUserUniqueId(),postId,PopupWindowReportCancelSharePost.this,REPORT_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
                Toast.makeText(context, R.string.TAKHALOF, Toast.LENGTH_SHORT).show();
                PopupWindowReportCancelSharePost.this.dismiss();

            }
        });

        setBackgroundDrawable(new BitmapDrawable());
        setOutsideTouchable(true);
        setWindowLayoutMode(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

    }


    @Override
    public void getResult(int reqCode,Object result) {
        iReportPost.notifyReportPost(postId,imageViewMore);
    }

    @Override
    public void getError(int reqCode,Integer errorCode,String callerStringID) {

    }
}
