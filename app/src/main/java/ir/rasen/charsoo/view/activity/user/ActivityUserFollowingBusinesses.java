package ir.rasen.charsoo.view.activity.user;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.BaseAdapterItem;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.PullToRefreshList;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.user.GetFollowingBusinesses;
import ir.rasen.charsoo.view.adapter.AdapterUserFollowingBusinesses;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IOnFollowStatudChangedListener;
import ir.rasen.charsoo.view.interface_m.IPullToRefresh;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.PullToRefreshListView;


public class ActivityUserFollowingBusinesses
        extends CharsooActivity
        implements IWebservice,IPullToRefresh,NetworkStateChangeListener,
        TryAgainListener,IOnFollowStatudChangedListener {

    public static final String TAG = "ActivityUserFollowingBusinesses";
    String visitedUserId;
    AdapterUserFollowingBusinesses adapterFollowingBusinesses;
    ListView listView;
    ArrayList<BaseAdapterItem> businesses;
    private View listFooterView;

    DialogMessage dialogMessage;
    //pull_to_refresh_lib
    PullToRefreshList pullToRefreshListView;

    TextViewFont tvNoBusinessExists;

    RelativeLayout networkFailLayout;

    public static final int GET_FOLLOWING_BUSINESSES_REQUEST = 100;


    private enum Status {FIRST_TIME, LOADING_MORE, REFRESHING, NONE}

    private Status status;


    boolean isOwnerUser;


    public static IOnFollowStatudChangedListener iOnFollowStatudChangedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.activity_user_following_businesses);
        setTitle(getString(R.string.businesses));


        iOnFollowStatudChangedListener = this;


        dialogMessage = new DialogMessage(ActivityUserFollowingBusinesses.this, "");


        visitedUserId = getIntent().getExtras().getString(Params.VISITED_USER_UNIQUE_ID);

        businesses = new ArrayList<>();
        adapterFollowingBusinesses = new AdapterUserFollowingBusinesses(ActivityUserFollowingBusinesses.this, visitedUserId, businesses);
        status = Status.FIRST_TIME;


        pullToRefreshListView = new PullToRefreshList(this, (PullToRefreshListView) findViewById(R.id.pull_refresh_list), ActivityUserFollowingBusinesses.this);
        listView = pullToRefreshListView.getListView();
        listView.setAdapter(adapterFollowingBusinesses);
//        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
//            int currentFirstVisibleItem
//                    ,
//                    currentVisibleItemCount
//                    ,
//                    currentScrollState;
//
//            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                this.currentFirstVisibleItem = firstVisibleItem;
//                this.currentVisibleItemCount = visibleItemCount;
//            }
//
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//                this.currentScrollState = scrollState;
//                this.isScrollCompleted();
//            }
//
//            private void isScrollCompleted() {
//                if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
//                    if (status != Status.LOADING_MORE
//                            && businesses.size() > 0 && businesses.size() % getResources().getInteger(R.integer.lazy_load_limitation) == 0) {
//                        //loadMoreData();
//                    }
//                }
//            }
//        });

        listFooterView = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_loading_more, null, false);
        listFooterView.setVisibility(View.GONE);
        listView.addFooterView(listFooterView, null, false);


        showWaitDialog();
        new GetFollowingBusinesses(
                ActivityUserFollowingBusinesses.this, visitedUserId,
                ActivityUserFollowingBusinesses.this, GET_FOLLOWING_BUSINESSES_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;


        tvNoBusinessExists = (TextViewFont) findViewById(R.id.tv_no_data_exist);

        networkFailLayout =
                MyApplication.initNetworkErrorLayout(getWindow().getDecorView(),this,this);

        isOwnerUser = visitedUserId.equals(LoginInfo.getInstance().getUserUniqueId()) ? true : false;
    }


    // LOAD MORE DATA
    public void loadMoreData() {
        // LOAD MORE DATA HERE...
        status = Status.LOADING_MORE;

        pullToRefreshListView.setFooterVisibility(View.VISIBLE);
        new GetFollowingBusinesses(
                ActivityUserFollowingBusinesses.this, visitedUserId,
                ActivityUserFollowingBusinesses.this, GET_FOLLOWING_BUSINESSES_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        /*MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_next_button, menu);*/
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }


    @Override
    public void getResult(int reqCode, Object result) {
        hideWaitDialog();

        switch (reqCode) {
            case GET_FOLLOWING_BUSINESSES_REQUEST:
                if (result instanceof ArrayList) {
                    ArrayList<BaseAdapterItem> temp = (ArrayList<BaseAdapterItem>) result;
                    businesses.addAll(temp);


                    if (status == Status.FIRST_TIME) {
                        adapterFollowingBusinesses = new AdapterUserFollowingBusinesses(ActivityUserFollowingBusinesses.this, visitedUserId, businesses);
                        listView.setAdapter(adapterFollowingBusinesses);
                    } else if (status == Status.REFRESHING) {
                        businesses.clear();
                        adapterFollowingBusinesses.notifyDataSetChanged();
                        businesses.addAll(temp);
                        adapterFollowingBusinesses.notifyDataSetChanged();
                        if (pullToRefreshListView.isRefreshing())
                            pullToRefreshListView.onRefreshComplete();
                    } else {
                        //it is loading more
                        pullToRefreshListView.setFooterVisibility(View.GONE);
//                listFooterView.setVisibility(View.GONE);
                        adapterFollowingBusinesses.loadMore(temp);
                    }
                    status = Status.NONE;

                    pullToRefreshListView.setResultSize(businesses.size());

                }
                NetworkConnectivityReciever.removeIfHaveListener(TAG);

                break;
        }

        networkFailLayout.setVisibility(View.GONE);
        handleTvNoReviewExist();
    }

    @Override
    public void getError(int reqCode, Integer errorCode, String callerStringID) {
        hideWaitDialog();

        switch (reqCode) {
            case GET_FOLLOWING_BUSINESSES_REQUEST:
                pullToRefreshListView.onRefreshComplete();

                if (errorCode == ServerAnswer.NETWORK_CONNECTION_ERROR) {
                    NetworkConnectivityReciever.setNetworkStateListener(TAG, ActivityUserFollowingBusinesses.this);
                }
                if(ServerAnswer.canShowErrorLayout(errorCode) && status!=Status.REFRESHING){
                    networkFailLayout.setVisibility(View.VISIBLE);
                }else if(networkFailLayout.getVisibility()!=View.VISIBLE){
                    dialogMessage.show();
                    dialogMessage.setMessage(
                            ServerAnswer.getError(
                                    ActivityUserFollowingBusinesses.this, errorCode,
                                    callerStringID + ">" + this.getLocalClassName()));
                }

                break;
        }

        handleTvNoReviewExist();
    }

    @Override
    public void doOnNetworkConnected() {
        tryAgain();
    }


    @Override
    public void notifyRefresh() {
        status = Status.REFRESHING;
        new GetFollowingBusinesses(
                ActivityUserFollowingBusinesses.this, visitedUserId,
                ActivityUserFollowingBusinesses.this, GET_FOLLOWING_BUSINESSES_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }

    @Override
    public void notifyLoadMore() {
        loadMoreData();
    }

    void handleTvNoReviewExist() {
        if (businesses.size() <= 0) {
            tvNoBusinessExists.setVisibility(View.VISIBLE);
            if(isOwnerUser)
                tvNoBusinessExists.setText(getString(R.string.txt_not_exists_user_followed_business));
            else
                tvNoBusinessExists.setText(getString(R.string.txt_not_exists_use_other_followed_business));
        } else
            tvNoBusinessExists.setVisibility(View.GONE);
    }

    @Override
    public void doTryAgain() {
        tryAgain();
    }


    public void tryAgain(){
        if(isShowingWaitDialog())
            return;

        showWaitDialog();
        status = Status.REFRESHING;
        new GetFollowingBusinesses(
                ActivityUserFollowingBusinesses.this, visitedUserId,
                ActivityUserFollowingBusinesses.this, GET_FOLLOWING_BUSINESSES_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }


    @Override
    public void onFollowStatusChanged(String targetBusinessId) {
        status = Status.REFRESHING;
        new GetFollowingBusinesses(
                ActivityUserFollowingBusinesses.this, visitedUserId,
                ActivityUserFollowingBusinesses.this, GET_FOLLOWING_BUSINESSES_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }

}