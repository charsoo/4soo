package ir.rasen.charsoo.view.adapter;

/**
 * Created by Sina KH on 6/23/2015.
 */

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.view.shared.ScreenUtils;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.view.widgets.MaterialProgressBarCircular;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.imageviews.ImageViewCircle;

public class AdapterUsers extends BaseAdapter {
    private Activity activity;
    ArrayList<User> items;
    //private int screedWidth;
    SimpleLoader simpleLoader;
    private int prevPosition = 0;

    // Constructor
    public AdapterUsers(Activity activity, ArrayList<User> useres) {
        this.activity = activity;
        items = useres;
        //screedWidth = activity.getResources().getDisplayMetrics().widthPixels;
        simpleLoader = new SimpleLoader(activity);
    }

    public void loadMore(ArrayList<User> newItems) {
        this.items.addAll(newItems);
        notifyDataSetChanged();
    }

    public int getCount() {
        return items.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View view, ViewGroup parent) {
        final Holder holder;
        if (view == null) {
            holder = new Holder();
            view = LayoutInflater.from(activity).inflate(R.layout.item_user, parent, false);
            holder.imageView = (ImageViewCircle) view.findViewById(R.id.user_img);
            holder.name = (TextViewFont) view.findViewById(R.id.user_name);
            holder.id = (TextViewFont) view.findViewById(R.id.user_id);
            holder.progressBar = (MaterialProgressBarCircular) view.findViewById(R.id.user_pb);

        } else
            holder = (Holder) view.getTag();

        if(holder!=null) {
            User user = items.get(position);
            if (user.coverPictureUniqueId.equals("0") && user.coverPicture != null && !user.coverPicture.equals(""))
                holder.imageView.setImageBitmap(Image_M.getBitmapFromString(user.coverPicture));
            else
                simpleLoader.loadImage(user.coverPictureUniqueId, Image_M.MEDIUM, Image_M.ImageType.POST, holder.imageView
                        , ScreenUtils.convertDpToPx(activity, activity.getResources().getDimension(R.dimen.profile_pic_large))
                        , ScreenUtils.convertDpToPx(activity, activity.getResources().getDimension(R.dimen.profile_pic_large))
                        , holder.progressBar, user.userIdentifier);
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    User.goUserHomeInfoPage(activity, items.get(position).uniqueId);
                }
            });
            holder.name.setText(
                    user.name != null ? user.name : ""
            );
            holder.id.setText(
                    user.userIdentifier != null ? user.userIdentifier : ""
            );
            if (position>prevPosition){
                prevPosition=position;
                Animation animation = AnimationUtils.loadAnimation(activity, R.anim.slide_in_from_bottom);
                view.startAnimation(animation);
                }
        }

        return view;
    }

    private class Holder {
        ImageViewCircle imageView;
        MaterialProgressBarCircular progressBar;
        TextViewFont name, id;
    }

}
