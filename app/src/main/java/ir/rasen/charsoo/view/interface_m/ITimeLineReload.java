package ir.rasen.charsoo.view.interface_m;

/**
 * Created by Rasen_iman on 8/6/2015.
 */
public interface ITimeLineReload {
    void onTimeLineReload();
}
