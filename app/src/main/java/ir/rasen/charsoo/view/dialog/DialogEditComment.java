package ir.rasen.charsoo.view.dialog;

import android.annotation.SuppressLint;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.object.Comment;
import ir.rasen.charsoo.model.comment.UpdateComment;
import ir.rasen.charsoo.view.interface_m.ICommentChange;
import ir.rasen.charsoo.view.widgets.buttons.ButtonFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.edittexts.EditTextFont;


public class DialogEditComment extends MyDialogOkCancel {



    @SuppressLint("NewApi")
    public DialogEditComment(final CharsooActivity charsooActivity, final Comment comment,final ICommentChange iCommentChange) {
        super(charsooActivity, charsooActivity.getResources().getString(R.string.edit_comment),
                charsooActivity.getResources().getString(R.string.cancel),
                charsooActivity.getResources().getString(R.string.edit));

        //creating editText
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final EditTextFont et_comment = new EditTextFont(charsooActivity);
        params.setMargins(10, getRowHeight(), 10, getRowHeight());
        et_comment.setLayoutParams(params);
        et_comment.setText(comment.text);
        et_comment.setBackgroundResource(R.drawable.shape_edit_text_back);
        et_comment.setGravity(Gravity.RIGHT| Gravity.CENTER_VERTICAL);
        int padding = charsooActivity.getResources().getInteger(R.integer.edit_text_padding);
        et_comment.setPadding(padding, padding, padding, padding);

        et_comment.setSingleLine();
        //add editText to the body
        LinearLayout ll_body = getBody();
        ll_body.addView(et_comment);

        //set onClickListener for the cancel button
        ButtonFont textViewOk = getOkButtonTextView();
        ButtonFont textViewCancel = getCancelButtonTextView();

        textViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        //set onClickListener for the ok button
        textViewOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_comment.length() < Params.COMMENT_TEXT_MIN_LENGTH) {
                    et_comment.setError(charsooActivity.getString(R.string.comment_is_too_short));
                    return;
                }
                if (et_comment.length() > Params.COMMENT_TEXT_MAX_LENGTH) {
                    et_comment.setError(charsooActivity.getString(R.string.enter_is_too_long));
                    return;
                }
//                comment.text = et_comment.getText().toString();

                Comment newComment = new Comment();
                newComment.ownerUniqueId = comment.ownerUniqueId;
                newComment.uniqueId = comment.uniqueId;
                newComment.text = et_comment.getText().toString();
                newComment.isUserSendedComment = comment.isUserSendedComment;

//                progressDialog.show();
                charsooActivity.showWaitDialog();
                //update the comment
                new UpdateComment(charsooActivity,newComment,iCommentChange, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                dismiss();
            }
        });

    }

}
