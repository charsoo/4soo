package ir.rasen.charsoo.view.activity.business;


import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.PullToRefreshList;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.controller.object.Review;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.review.GetBusinessReviews;
import ir.rasen.charsoo.view.activity.ActivityBusinessImage;
import ir.rasen.charsoo.view.activity.ActivityInvite;
import ir.rasen.charsoo.view.adapter.AdapterBusinessReview;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IAddReview;
import ir.rasen.charsoo.view.interface_m.IPullToRefresh;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.PullToRefreshListView;


public class ActivityBusinessReviews
        extends CharsooActivity
        implements IWebservice, IAddReview, IPullToRefresh,NetworkStateChangeListener,TryAgainListener {

    public static final String TAG="ActivityBusinessReviews";
//    WaitDialog progressDialog;
    private String businessId;
    AdapterBusinessReview adapterBusinessReview;
    ListView listView;
    ArrayList<Review> results;
    String visitorIntId;


    public static final int GET_BUSINESS_REVIEWS_REQUSET = 100;

    //pull_to_refresh_lib
    PullToRefreshList pullToRefreshListView;

    @Override
    public void notifyRefresh() {
        status = Status.REFRESHING;
        new GetBusinessReviews(ActivityBusinessReviews.this, visitorIntId,businessId, 0, getResources().getInteger(R.integer.lazy_load_limitation), ActivityBusinessReviews.this,GET_BUSINESS_REVIEWS_REQUSET,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }

    @Override
    public void notifyLoadMore() {
        loadMoreData();
    }

    @Override
    public void doOnNetworkConnected() {
        doTryAgain();
    }

    private enum Status {FIRST_TIME, LOADING_MORE, REFRESHING, NONE}

    private Status status;

    private View addReview,showGallery;

    RelativeLayout networkFailLayout;


    TextViewFont tvNoReviewExists;

    boolean isOwnerBusiness;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.activity_business_reviews);
        setTitle(getResources().getString(R.string.reviews));

        networkFailLayout =
                MyApplication.initNetworkErrorLayout(
                        getWindow().getDecorView(),ActivityBusinessReviews.this,
                        ActivityBusinessReviews.this);
        networkFailLayout.setVisibility(View.GONE);

        addReview = findViewById(R.id.btn_new_review);
        showGallery = findViewById(R.id.btn_galley);
        try {
        } catch (Exception e) {

        }

        visitorIntId = LoginInfo.getInstance().getUserUniqueId();
        businessId = getIntent().getExtras().getString(Params.BUSINESS_ID_STRING);
        
        isOwnerBusiness = getIntent().getExtras().getBoolean(Params.BUSINESS_OWNER);
        if (isOwnerBusiness)
            addReview.setVisibility(View.GONE);



        results = new ArrayList<>();
        status = Status.FIRST_TIME;

        pullToRefreshListView = new PullToRefreshList(this, (PullToRefreshListView) findViewById(R.id.pull_refresh_list), ActivityBusinessReviews.this);
        listView = pullToRefreshListView.getListView();

        adapterBusinessReview = new AdapterBusinessReview(ActivityBusinessReviews.this, results);
        listView.setAdapter(adapterBusinessReview);

        showWaitDialog();
        new GetBusinessReviews(
                ActivityBusinessReviews.this,visitorIntId, businessId, 0,
                getResources().getInteger(R.integer.lazy_load_limitation), ActivityBusinessReviews.this,GET_BUSINESS_REVIEWS_REQUSET,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;


        addReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myintent=
                        new Intent(ActivityBusinessReviews.this,ActivityAddUpdateReview.class);
                startActivity(myintent);
            }
        });

      showGallery.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              openGalleryActivity(businessId);
          }
      });

        tvNoReviewExists = (TextViewFont) findViewById(R.id.tv_no_data_exist);
    }


    // LOAD MORE DATA
    public void loadMoreData() {
        // LOAD MORE DATA HERE...
        status = Status.LOADING_MORE;
        pullToRefreshListView.setFooterVisibility(View.VISIBLE);
        new GetBusinessReviews(
                ActivityBusinessReviews.this,visitorIntId, businessId,
                adapterBusinessReview.getCount(), getResources().getInteger(R.integer.lazy_load_limitation), ActivityBusinessReviews.this,
                GET_BUSINESS_REVIEWS_REQUSET,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        /*MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_next_button, menu);*/
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    @Override
      public void getResult(int reqCode,Object result) {
        networkFailLayout.setVisibility(View.GONE);
        hideWaitDialog();
        switch (reqCode){
            case GET_BUSINESS_REVIEWS_REQUSET :
                hideWaitDialog();
                if (result instanceof ArrayList) {
                    ArrayList<Review> temp = (ArrayList<Review>) result;
                    results.addAll(temp);

                    if (Review.submitBefore(ActivityBusinessReviews.this, temp))
                        addReview.setVisibility(View.GONE);

                    pullToRefreshListView.setResultSize(results.size());

                    if (status == Status.FIRST_TIME) {
                        adapterBusinessReview = new AdapterBusinessReview(ActivityBusinessReviews.this, results);
                        listView.setAdapter(adapterBusinessReview);
                    } else if (status == Status.REFRESHING) {
                        results.clear();
                        adapterBusinessReview.notifyDataSetChanged();
                        results.addAll(temp);
                        adapterBusinessReview.notifyDataSetChanged();
                        pullToRefreshListView.onRefreshComplete();
                    } else {
                        //it is loading more
                        pullToRefreshListView.setFooterVisibility(View.GONE);
                        adapterBusinessReview.loadMore(temp);
                    }
                    status = Status.NONE;
                }
                NetworkConnectivityReciever.removeIfHaveListener(TAG);


                break;
        }

        handleTvNoReviewExist();
    }

    @Override
    public void getError(int reqCode,Integer errorCode,String callerStringID) {
        switch(reqCode){
            case GET_BUSINESS_REVIEWS_REQUSET :
                hideWaitDialog();
                pullToRefreshListView.onRefreshComplete();

                if (errorCode==ServerAnswer.NETWORK_CONNECTION_ERROR) {
                    NetworkConnectivityReciever.setNetworkStateListener(TAG, ActivityBusinessReviews.this);
                }

                if(ServerAnswer.canShowErrorLayout(errorCode) && status!=Status.REFRESHING){
                    networkFailLayout.setVisibility(View.VISIBLE);
                }
                if(errorCode==ServerAnswer.NO_REVIEW_ERROR)
                    networkFailLayout.setVisibility(View.GONE);

                else if (!ServerAnswer.canShowErrorLayout(reqCode) && errorCode != ServerAnswer.NO_REVIEW_ERROR) {
                    new DialogMessage(
                            ActivityBusinessReviews.this, ServerAnswer.getError(ActivityBusinessReviews.this,
                            errorCode, callerStringID + ">" + this.getLocalClassName())).show();
                    addReview.setVisibility(View.GONE);
                }



                break;
        }

        handleTvNoReviewExist();
    }

    @Override
    public void successAddReview(RequestObject.ReviewItem reviewItem) {
//        progressDialog.dismiss();
        hideWaitDialog();
//        results.add(0, new Review(
//                LoginInfo.getAccessUserIdentifier(ActivityBusinessReviews.this),
//                LoginInfo.getUserProfilePictureId(ActivityBusinessReviews.this),
//                reviewRate, reviewText));
//        adapterBusinessReview.notifyDataSetChanged();
        addReview.setVisibility(View.GONE);
        status=Status.REFRESHING;
        new GetBusinessReviews(
                ActivityBusinessReviews.this,visitorIntId, businessId, 0,
                getResources().getInteger(R.integer.lazy_load_limitation),
                ActivityBusinessReviews.this,GET_BUSINESS_REVIEWS_REQUSET,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;

        handleTvNoReviewExist();
    }

    @Override
    public void failAddReview(int errorcode, String error) {
        hideWaitDialog();
        new DialogMessage(ActivityBusinessReviews.this, ServerAnswer.getError(ActivityBusinessReviews.this, errorcode,error+">"+this.getLocalClassName())).show();

        handleTvNoReviewExist();
    }



    @Override
    public void doTryAgain() {
        if(isShowingWaitDialog())
            return;

        showWaitDialog();
        status = Status.REFRESHING;
        new GetBusinessReviews(
                ActivityBusinessReviews.this,visitorIntId, businessId, 0,
                getResources().getInteger(R.integer.lazy_load_limitation),
                ActivityBusinessReviews.this,GET_BUSINESS_REVIEWS_REQUSET,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }


    void handleTvNoReviewExist(){
        if(results.size() <= 0) {
            tvNoReviewExists.setVisibility(View.VISIBLE);
            if (isOwnerBusiness)
                tvNoReviewExists.setText(getString(R.string.txt_not_exists_review_for_business));
            else
                tvNoReviewExists.setText(getString(R.string.txt_not_exists_review_for_business_other));
        }
        else
            tvNoReviewExists.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        NetworkConnectivityReciever.removeIfHaveListener(TAG);
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this,"2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
