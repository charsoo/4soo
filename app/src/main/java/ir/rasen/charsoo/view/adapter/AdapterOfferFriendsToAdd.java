package ir.rasen.charsoo.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.Hashtable;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.ContactEntry;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.buttons.ButtonFont;
import ir.rasen.charsoo.view.widgets.imageviews.ImageViewCircle;


public class AdapterOfferFriendsToAdd extends BaseAdapter  {

    private ArrayList<ContactEntry> items;
    private Context context;
    SimpleLoader simpleLoader;
    private Hashtable<Integer,Boolean> isItemChecked;
    IWebservice delegate;
    int prevProsition=0;
    String userIntId;
    int friendshipRequestIntCode;
    Holder holder;
    View v;


    public AdapterOfferFriendsToAdd(Context context,String userIntId, ArrayList<ContactEntry> items, IWebservice delegate,
                                    int friendshipRequestIntCode) {
        this.delegate = delegate;
        this.context = context;
        this.items = items;
        simpleLoader = new SimpleLoader(context);
        isItemChecked = new Hashtable<>();
        this.userIntId = userIntId;
        this.friendshipRequestIntCode = friendshipRequestIntCode;
    }

    public void loadMore(ArrayList<ContactEntry> newItem) {
        this.items.addAll(newItem);
        notifyDataSetChanged();
    }

    public void resetItems(ArrayList<ContactEntry> newItem){
        this.items=(ArrayList<ContactEntry>)newItem.clone();
        notifyDataSetChanged();
    }

    public void setUncheckedViewAt(int position){
        try{
            isItemChecked.put(position, false);
            items.remove(position);
            notifyDataSetChanged();
        }catch(Exception ed){

        }
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }



    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {

        holder = new Holder();
        v = view;

        if (v == null){
            v = LayoutInflater.from(context).inflate(R.layout.item_contact, viewGroup, false);
            holder.imageViewProfilePicture = (ImageViewCircle) v.findViewById(R.id.contact_picture);
            holder.textViewFontCharsooFullName = (TextViewFont) v.findViewById(R.id.contact_fullName);
            holder.textViewFontCharsooUserId = (TextViewFont) v.findViewById(R.id.contact_userName);
            v.setTag(holder);
        }
        else
            holder = (Holder) v.getTag();

        if (!items.get(position).isSeenBefore)
            v.setBackgroundColor(context.getResources().getColor(R.color.gray_light));
        holder.textViewFontCharsooFullName.setText(items.get(position).charsooFullName);
        holder.textViewFontCharsooUserId.setText(items.get(position).charsooUniqueId);
        simpleLoader.loadImage(items.get(position).charsooPictureUniqueId, Image_M.SMALL,
                Image_M.ImageType.USER, holder.imageViewProfilePicture);
        holder.imageViewProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User.goUserHomeInfoPage(context,items.get(position).charsooStringId);
            }
        });
        holder.textViewFontCharsooFullName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User.goUserHomeInfoPage(context,items.get(position).charsooStringId);
            }
        });
        holder.textViewFontCharsooUserId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User.goUserHomeInfoPage(context,items.get(position).charsooStringId);
            }
        });

        if(position>prevProsition) {
            prevProsition = position;
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_in_from_bottom);
            v.startAnimation(animation);
        }
        return v;
    }

    private class Holder {

        ButtonFont btnRequestFriendship;
        TextViewFont textViewFontCharsooFullName;
        TextViewFont textViewFontCharsooUserId;
        ImageViewCircle imageViewProfilePicture;

    }
}
