package ir.rasen.charsoo.view.adapter;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.view.fragment.search.FragmentSearch;
import ir.rasen.charsoo.view.fragment.timeline.FragmentTimelineAllPosts;
import ir.rasen.charsoo.view.fragment.timeline.FragmentTimelineFollowingBusinessPosts;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;

/**
 * Created by Rasen_iman on 8/12/2015.
 */
public class AdapterTimeLineTabs extends FragmentPagerAdapter{

    FragmentTimelineAllPosts mFragmentTimelineAllPosts;
    FragmentTimelineFollowingBusinessPosts mFragmentTimelineFollowingBusinessPosts;

    CharsooActivity mCharsooActivityContext;

    private static final int NUM_ITEMS = 2;

    public AdapterTimeLineTabs(FragmentManager fm,Activity activity) {
        super(fm);
        mCharsooActivityContext = (CharsooActivity) activity;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 :
                mFragmentTimelineAllPosts = FragmentTimelineAllPosts.newInstance();
                return mFragmentTimelineAllPosts;

            case 1 :
                mFragmentTimelineFollowingBusinessPosts =
                        FragmentTimelineFollowingBusinessPosts.newInstance();

                return mFragmentTimelineFollowingBusinessPosts;
        }
        return null;
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mCharsooActivityContext.getString(R.string.timeline_all_posts);
            case 1:
                return mCharsooActivityContext.getString(R.string.timeline_followed_business_posts);
            default:
                return null;
        }
    }

}
