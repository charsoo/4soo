package ir.rasen.charsoo.view.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.flurry.android.FlurryAgent;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.WebservicesHandler;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.ServerAppVersion;
import ir.rasen.charsoo.model.app.GetVersion;
import ir.rasen.charsoo.view.activity.user.ActivityUserRegister;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;


public class ActivitySplash extends Activity implements IWebservice {
    public static final int GET_VERSION_REQUEST = 200;
    MyApplication myApplication;
    DialogMessage dialogMessage;
    boolean isContinuAfterUpdateDialog = true;
    public CharsooActivity charsooActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_activity_splash);
        charsooActivity=new CharsooActivity();
        myApplication = (MyApplication) getApplication();
        myApplication.setCurrentWebservice(WebservicesHandler.Webservices.NONE);
        dialogMessage=new DialogMessage(this,"");
        // i am setting 2 drawables to be shown at first as default drawables
        ArrayList<Drawable> drawables = new ArrayList<>();
        drawables.add(ContextCompat.getDrawable(this, R.drawable.splash_1));
        drawables.add(ContextCompat.getDrawable(this, R.drawable.splash_2));

        AutoImageFlipper autoImageFlipper = (AutoImageFlipper) findViewById(R.id.autoImageFlipper);
        autoImageFlipper.setDrawables(drawables);
//        autoImageFlipper.setAspectRatio(0.75);
        // then, i'm going to add a new drawable to flipping list
        // this drawable can be something received from the internet
        drawables.add(ContextCompat.getDrawable(this, R.drawable.splash_3));
        // OR : autoImageFlipper.addDrawable(ContextCompat.getDrawable(this, R.drawable.sample3));
        new GetVersion(ActivitySplash.this, ActivitySplash.this, GET_VERSION_REQUEST).exectueWithNewSolution();
    }
    public void goto_login (View view){
        Intent intent =new Intent(ActivitySplash.this,ActivityLogin.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void goto_signup (View view){
        Intent intent = new Intent(ActivitySplash.this, ActivityUserRegister.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }


    @Override
    public void getResult(int request, Object result) throws Exception {
        charsooActivity.hideWaitDialog();
        ServerAppVersion serverAppVersion = (ServerAppVersion) result;
        int currentAppVersionCode = myApplication.getApplicationVersionCode();

        if(currentAppVersionCode<0)
            showNonDefineErrorForGettingVersionCode();
        else if(currentAppVersionCode<serverAppVersion.minVersion)
            showForceUpdateDialog();
        else if(currentAppVersionCode<serverAppVersion.maxVersion)
            showNotifyUpdateDialog();
        else
            continueAfterSuccessingVersionCode();
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) throws Exception {
        charsooActivity.hideWaitDialog();
        showTryAgainDialog();
    }

    private void showNonDefineErrorForGettingVersionCode() {
        dialogMessage = new DialogMessage(ActivitySplash.this,"Non Defined Error");
        dialogMessage.setAcceptText("exit");
        dialogMessage.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        dialogMessage.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
            }
        });
        dialogMessage.show();
    }

    private void showForceUpdateDialog() {
        dialogMessage =
                new DialogMessage(ActivitySplash.this,
                        getString(R.string.force_update_dlg_message));

        dialogMessage.setAcceptText(getString(R.string.force_update_dlg_accept_btn_text));
        dialogMessage.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openHtmlLink("http://www.icharsoo.com");
                finish();
            }
        });

        dialogMessage.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
            }
        });
        dialogMessage.show();
    }
    public void openHtmlLink(String rawLink){
        try {
            Intent intentViewHTMLLink = new Intent(Intent.ACTION_VIEW, Uri.parse(rawLink));
            startActivity(intentViewHTMLLink);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void showTryAgainDialog() {

        dialogMessage.setAcceptText(getString(R.string.try_again_dlg_accept_btn_text));
        dialogMessage.addCancelButton(getString(R.string.try_again_dlg_Cancel_btn_text), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        dialogMessage.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogMessage.dismiss();
                TryAgainGetVersionCode();
            }
        });
        dialogMessage.show();
        dialogMessage.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                dialogMessage.dismiss();
                TryAgainGetVersionCode();
            }
        });

        dialogMessage.setMessage(getString(R.string.try_again_dlg_message));

    }
    private void showNotifyUpdateDialog() {
        isContinuAfterUpdateDialog = true;
        dialogMessage =
                new DialogMessage(ActivitySplash.this,
                        getString(R.string.notify_update_dlg_message));

        dialogMessage.setAcceptText(getString(R.string.notify_update_dlg_accept_btn_text));
        dialogMessage.addCancelButton(getString(R.string.notify_update_dlg_Cancel_btn_text), new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogMessage.dismiss();
            }
        });
        dialogMessage.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isContinuAfterUpdateDialog = false;
                openHtmlLink("http://www.icharsoo.com");
                finish();
            }
        });
        dialogMessage.show();
        dialogMessage.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (isContinuAfterUpdateDialog)
                    continueAfterSuccessingVersionCode();
            }
        });
    }
    void continueAfterSuccessingVersionCode(){
        // noting
    }
    private void TryAgainGetVersionCode() {
        charsooActivity.showWaitDialog();
        new GetVersion(ActivitySplash.this,ActivitySplash.this,GET_VERSION_REQUEST).exectueWithNewSolution();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}




