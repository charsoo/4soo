package ir.rasen.charsoo.view.dialog;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.model.post.CancelShare;
import ir.rasen.charsoo.view.activity.ActivityPostAddEdit;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessEdit;
import ir.rasen.charsoo.view.activity.user.ActivityUserProfile;
import ir.rasen.charsoo.view.interface_m.IShareCancelShareListener;
import ir.rasen.charsoo.view.widgets.material_library.widgets.Dialog;


public class DialogSaveResult extends Dialog {
    IShareCancelShareListener delegate;
//    Activity activity;

    public DialogSaveResult(final Activity activity) {
        super(activity, activity.getString(R.string.dialogTitle_SaveChanges), activity.getString(R.string.dialogMessage_SaveChanges));

        this.delegate=delegate;

        addCancelButton(R.string.cancel);
        setAcceptText(R.string.yes);

        setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (activity instanceof ActivityPostAddEdit) {
                    ((ActivityPostAddEdit) activity).submit();
                } else if (activity instanceof ActivityUserProfile) {
                    ((ActivityUserProfile) activity).submit();
                }
            }
        });

        setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        });

    }

}
