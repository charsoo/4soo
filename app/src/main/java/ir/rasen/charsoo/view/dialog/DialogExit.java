package ir.rasen.charsoo.view.dialog;

import android.app.Activity;
import android.view.View;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.model.user.Logout;
import ir.rasen.charsoo.view.interface_m.ILogout;
import ir.rasen.charsoo.view.widgets.material_library.widgets.Dialog;


public class DialogExit extends Dialog {

    public DialogExit(final Activity activity,final ILogout iLogout) {
        super(activity, activity.getResources().getString(R.string.popup_warning),
                activity.getResources().getString(R.string.confirmation_exit));

        addCancelButton(R.string.cancel);
        setAcceptText(R.string.exit);

        //set onClickListener for the ok button
        setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Logout(activity,LoginInfo.getInstance().getUserUniqueId()
                        ,LoginInfo.getInstance().userAccessToken,iLogout).exectueWithNewSolution();;
                dismiss();
            }
        });

    }

}
