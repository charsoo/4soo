package ir.rasen.charsoo.view.widgets.buttons;

import android.content.Context;
import android.util.AttributeSet;

public class FlatButton extends ButtonFont {

    public FlatButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}