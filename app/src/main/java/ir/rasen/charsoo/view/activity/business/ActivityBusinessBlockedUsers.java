package ir.rasen.charsoo.view.activity.business;


import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import com.flurry.android.FlurryAgent;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.BaseAdapterItem;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.PullToRefreshList;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.business.GetBlockedUsers;
import ir.rasen.charsoo.view.adapter.AdapterBlockedUsers;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IPullToRefresh;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.Footer;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.PullToRefreshListView;


public class ActivityBusinessBlockedUsers
        extends CharsooActivity implements
        IWebservice,NetworkStateChangeListener,IPullToRefresh {
    public static final String TAG="ActivityBusinessBlockedUsers";

    public static final int GET_BLOCKED_USERS_REQUEST = 10;

//    WaitDialog progressDialog;
String businessId;
    AdapterBlockedUsers adapterBlockedUsers;
    ArrayList<BaseAdapterItem> blockedUsers;
    DialogMessage dialogMessage;


    private Footer footer;

    private enum Status {FIRST_TIME, LOADING_MORE, NONE,REFRESHING}

    private Status status;

    PullToRefreshList pullToRefreshList;
    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.layout_business_user_blocked);

        setTitle(getResources().getString(R.string.blocked));

        businessId = getIntent().getExtras().getString(Params.BUSINESS_ID_STRING);
        blockedUsers = new ArrayList<>();
        status = Status.FIRST_TIME;

//        progressDialog = new WaitDialog(this);
//        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        dialogMessage=new DialogMessage(this,"");

        pullToRefreshList =
                new PullToRefreshList(ActivityBusinessBlockedUsers.this,
                        ((PullToRefreshListView)findViewById(R.id.pull_refresh_list)),ActivityBusinessBlockedUsers.this);

        listView = pullToRefreshList.getListView();
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            int currentFirstVisibleItem
                    ,
                    currentVisibleItemCount
                    ,
                    currentScrollState;

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
            }

            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            private void isScrollCompleted() {
                if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
                    if (status != Status.LOADING_MORE
                            && blockedUsers.size() > 0 && blockedUsers.size() % getResources().getInteger(R.integer.lazy_load_limitation) == 0) {
                        //loadMoreData();
                    }
                }
            }
        });

        adapterBlockedUsers = new AdapterBlockedUsers(ActivityBusinessBlockedUsers.this, businessId, blockedUsers);
        listView.setAdapter(adapterBlockedUsers);

        footer = new Footer(this);
        listView.addFooterView(footer.getFooterView(), null, false);

//        progressDialog.show();
        showWaitDialog();
        new GetBlockedUsers(ActivityBusinessBlockedUsers.this, businessId,0,getResources().getInteger(R.integer.lazy_load_limitation),
                ActivityBusinessBlockedUsers.this,GET_BLOCKED_USERS_REQUEST, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;


    }


    // LOAD MORE DATA
    public void loadMoreData() {
        // LOAD MORE DATA HERE...
        status = Status.LOADING_MORE;
        footer.setVisibility(View.VISIBLE);
        new GetBlockedUsers(ActivityBusinessBlockedUsers.this, businessId,adapterBlockedUsers.getCount()
                ,getResources().getInteger(R.integer.lazy_load_limitation)
                , ActivityBusinessBlockedUsers.this,GET_BLOCKED_USERS_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        /*MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_next_button, menu);*/
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }


    @Override
    public void getResult(int reqCode,Object result) {
        switch(reqCode){
            case GET_BLOCKED_USERS_REQUEST :
                  hideWaitDialog();

                if (result instanceof ArrayList) {
                    ArrayList<BaseAdapterItem> temp = (ArrayList<BaseAdapterItem>) result;

                    if (status == Status.FIRST_TIME || status == Status.REFRESHING) {
                        adapterBlockedUsers.resetItems(temp);
                        pullToRefreshList.onRefreshComplete();
                    } else {
                        //it is loading more
                        footer.setVisibility(View.GONE);
                        adapterBlockedUsers.loadMore(temp);
                    }
                    status = Status.NONE;
                }
                NetworkConnectivityReciever.removeIfHaveListener(TAG);
                break;
        }
    }

    @Override
    public void getError(int reqCode,Integer errorCode,String callerStringID) {
        switch (reqCode){
            case GET_BLOCKED_USERS_REQUEST:
//                progressDialog.dismiss();
                  hideWaitDialog();
                if (!dialogMessage.isShowing()){
                    dialogMessage.show();
                    dialogMessage.setMessage(ServerAnswer.getError(
                            ActivityBusinessBlockedUsers.this,
                            errorCode,callerStringID+">"+this.getLocalClassName()));
                }
                if (errorCode==ServerAnswer.NETWORK_CONNECTION_ERROR)
                    NetworkConnectivityReciever.setNetworkStateListener(TAG,ActivityBusinessBlockedUsers.this);

                break;
        }
    }

    @Override
    public void doOnNetworkConnected() {
//        progressDialog.show();
        showWaitDialog();
        new GetBlockedUsers(ActivityBusinessBlockedUsers.this, businessId,0,getResources().getInteger(R.integer.lazy_load_limitation), ActivityBusinessBlockedUsers.this,GET_BLOCKED_USERS_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;

    }





    @Override
    public void notifyRefresh() {
        status = Status.REFRESHING;
        new GetBlockedUsers(ActivityBusinessBlockedUsers.this, businessId,
                0,getResources().getInteger(R.integer.lazy_load_limitation),
                ActivityBusinessBlockedUsers.this,GET_BLOCKED_USERS_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }

    @Override
    public void notifyLoadMore() {

    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
