package ir.rasen.charsoo.view.dialog;

import android.content.Context;
import android.view.View;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.model.business.DeleteBusiness;
import ir.rasen.charsoo.view.widgets.material_library.widgets.Dialog;


public class DialogDeleteBusinessConfirmation extends Dialog {
    Context context;

    public DialogDeleteBusinessConfirmation(final Context context, final String businessId) {
        super(context, context.getResources().getString(R.string.delete),
                context.getResources().getString(R.string.confirmation_delete_business));

        addCancelButton(R.string.cancel);
        setAcceptText(R.string.yes);

        //set onClickListener for the ok button
        setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DeleteBusiness(context,LoginInfo.getInstance().getUserUniqueId(), businessId).exectueWithNewSolution();
                dismiss();
            }
        });

    }

}
