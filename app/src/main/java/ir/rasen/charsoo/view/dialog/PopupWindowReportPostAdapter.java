package ir.rasen.charsoo.view.dialog;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.model.post.Report;
import ir.rasen.charsoo.view.interface_m.IReportPost;
import ir.rasen.charsoo.view.interface_m.IWebservice;


public class PopupWindowReportPostAdapter extends PopupWindow implements IWebservice {
    private Context context;
    private IReportPost iReportPost;
    private ImageView reportedItemImageViewMore;
    private String postId;

    public static final int REPORT_REQUEST = 100;

    public PopupWindowReportPostAdapter(final Context context, final String userId, final String postId, ImageView reportedItemImageViewMore, IReportPost iReportPost) {
        super(context);

        this.context = context;
        this.iReportPost = iReportPost;
        this.postId = postId;
        this.reportedItemImageViewMore = reportedItemImageViewMore;

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_post_report_cancel_share_more, null, false);
        setContentView(view);

        view.findViewById(R.id.btn_post_more_cancel_share).setVisibility(View.GONE);
        View textViewReport = view.findViewById(R.id.btn_post_more_report);

        textViewReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Report(context,userId,postId,PopupWindowReportPostAdapter.this,REPORT_REQUEST, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
                Toast.makeText(context,R.string.TAKHALOF,Toast.LENGTH_SHORT).show();
                PopupWindowReportPostAdapter.this.dismiss();

            }
        });

        setBackgroundDrawable(new BitmapDrawable());
        setOutsideTouchable(true);
        setWindowLayoutMode(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

    }


    @Override
    public void getResult(int reqCode,Object result) {
        iReportPost.notifyReportPost(postId,reportedItemImageViewMore);
    }

    @Override
    public void getError(int reqCode,Integer errorCode,String callerStringID) {

    }
}
