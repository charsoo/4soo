package ir.rasen.charsoo.view.widgets.buttons;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.view.widgets.RippleView;

public class ButtonFont extends RippleView {

    private static String ANDROIDXML = "http://schemas.android.com/apk/res/android";
    private static int RIPPLE_DURATION = 200;

    private Button btn;

    public ButtonFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ButtonFont(Context context) {
        super(context, null);
        init(null);
    }

    private void init(AttributeSet attrs) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_button, this, false);
        addView(view);

        btn = (Button) view.findViewById(R.id.btn);

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/b_yekan.ttf");
        btn.setTypeface(tf);

        if (attrs != null) {
            int text = attrs.getAttributeResourceValue(ANDROIDXML, "text", -1);
            if (text != -1)
                btn.setText(text);
            int textColor = attrs.getAttributeResourceValue(ANDROIDXML, "textColor", -1);
            if (textColor != -1)
                btn.setTextColor(getResources().getColor(textColor));
            int textSize = attrs.getAttributeResourceValue(ANDROIDXML, "textSize", -1);
            if (textSize != -1)
                btn.setTextSize(getResources().getDimensionPixelSize(textSize));
            int background = attrs.getAttributeResourceValue(ANDROIDXML, "background", -1);
            if (background==-1 || background==android.R.color.white
                    || background==android.R.drawable.editbox_background_normal) {
                setRippleColor(R.color.gray_light);
            }
        }
        btn.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        setRippleDuration(RIPPLE_DURATION);

    }

    public CharSequence getText() {
        return this.btn.getText();
    }

    public void setText(CharSequence text) {
        this.btn.setText(text);
    }

    public void setCompoundDrawablesWithIntrinsicBounds(Drawable d1, Drawable d2, Drawable d3, Drawable d4) {
        this.btn.setCompoundDrawablesWithIntrinsicBounds(d1, d2, d3, d4);
    }

    public void setTextColor(int textColor) {
        this.btn.setTextColor(textColor);
    }

    public void setText(int id) {
        setText(getContext().getString(id));
    }

}