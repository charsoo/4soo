package ir.rasen.charsoo.view.fragment.business_register;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.TextProcessor;
import ir.rasen.charsoo.controller.object.Category;
import ir.rasen.charsoo.controller.object.SubCategory;
import ir.rasen.charsoo.model.business.GetBusinessCategories;
import ir.rasen.charsoo.model.business.GetBusinessSubcategories;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.edittexts.EditTextFont;

/**
 * Created by hossein-pc on 6/14/2015.
 */
public class FragmentBusinessRegisterPageTwo extends Fragment implements IWebservice {
    public static final String TAG = "FragmentBusinessRegisterPageTwo";

    private static final int REQUEST_CATEGORIES = 1, REQUEST_SUBCATEGORIES = 2;



    private EditTextFont editTextDescription, editTextHashtags;
    private Spinner spinnerCategory, spinnerSubcategory;

    public static Category selectedCategory;
    public static SubCategory selectedSubcategory;

    boolean areHashtagsValid = true;
    public CharsooActivity charsooActivitycontext;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CustomActivityOnCrash.install(getActivity());

        View view = inflater.inflate(R.layout.fragment_register_business_page_two,
                container, false);


        charsooActivitycontext=(CharsooActivity) getActivity();
        spinnerCategory = (Spinner) view.findViewById(R.id.spinner_Category);
        spinnerSubcategory = (Spinner) view.findViewById(R.id.spinner_Subcategory);

        if(Globals.getInstance().getValue().loadedCategories == null ||
                Globals.getInstance().getValue().loadedCategories.size()==0){
            charsooActivitycontext.showWaitDialog();
            new GetBusinessCategories(getActivity(), FragmentBusinessRegisterPageTwo.this
                    , REQUEST_CATEGORIES, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
        }else{
            charsooActivitycontext.hideWaitDialog();
            ArrayList<String> categoriesName =
                    Category.getCategoriesName(Globals.getInstance().getValue().loadedCategories);

            setCategorySpinnerAdapter(
                    categoriesName,
                    getActivity());

            if(
                    selectedCategory!=null &&
                            selectedCategory.categoryName!=null &&
                            !selectedCategory.categoryName.equals("") &&
                            !selectedCategory.categoryName.equals("null")) {

                spinnerCategory.setSelection(categoriesName.indexOf(selectedCategory.categoryName));
            }
        }


        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==0){
                    spinnerSubcategory.setVisibility(View.GONE);
                }
                else if(i>0){
                    spinnerSubcategory.setVisibility(View.VISIBLE);
                    selectedCategory = Globals.getInstance().getValue().loadedCategories.get(i);
                    charsooActivitycontext.showWaitDialog();

                    String selectedCategoryId = Globals.getInstance().getValue().loadedCategories.get(i).categoryUniqueId;

                    new GetBusinessSubcategories(
                            getActivity(),selectedCategoryId,FragmentBusinessRegisterPageTwo.this,
                            REQUEST_SUBCATEGORIES,LoginInfo.getInstance().userAccessToken).execute();

                    selectedCategory.categoryName= Globals.getInstance().getValue().loadedCategories.get(i).categoryName;
                    selectedCategory.categoryUniqueId = selectedCategoryId;

                    Globals.getInstance().getValue().businessRegistering.categoryUniqueId = selectedCategoryId;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerSubcategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i>0) {
                    if(selectedSubcategory == null)
                        selectedSubcategory = new SubCategory();
                    selectedSubcategory.subCategoryName = Globals.getInstance().getValue().loadedSubCategories.get(i).subCategoryName;
                    selectedSubcategory.subCaregoryUniqueId = Globals.getInstance().getValue().loadedSubCategories.get(i).subCaregoryUniqueId;

                    Globals.getInstance().getValue().businessRegistering.subCategoryUniqueId = selectedSubcategory.subCaregoryUniqueId;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        editTextDescription = (EditTextFont) view.findViewById(R.id.edt_description);
        editTextHashtags = (EditTextFont) view.findViewById(R.id.edt_hashtags);
        editTextHashtags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editTextHashtags.getText()==null ||editTextHashtags.getText().toString().equals("") )
                    editTextHashtags.setText("#");
                editTextHashtags.setSelection(editTextHashtags.getText().length());
            }
        });

        //process hashtags
        editTextHashtags.addTextChangedListener(new TextWatcher() {
            String oldText;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                oldText = charSequence.toString();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (charSequence.toString().equals(oldText))
                    return;
//                TextProcessor.processEdtHashtags(editTextHashtags.getText().toString(), editTextHashtags, getActivity());
                areHashtagsValid = TextProcessor.parsTextForHashtag(editTextHashtags
                        , getResources().getColor(R.color.hashtagColor)
                        , getResources().getColor(R.color.accentColor)
                        , getResources().getColor(R.color.hashtagBackgroundColor));
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

//        myApplication = (MyApplication) getActivity().getApplication();
//        myApplication.setCurrentWebservice(WebservicesHandler.Webservices.GET_BUSINESS_CATEGORY);


        return view;
    }

    @Override
    public void getResult(int request, Object result) {
        charsooActivitycontext.hideWaitDialog();

        switch (request) {
            case REQUEST_CATEGORIES:
                Globals.getInstance().getValue().loadedCategories =
                        Category.addDefaultCategoryToStartOfList(getActivity(), (ArrayList<Category>) result);

                ArrayList<String> categoriesName =
                        Category.getCategoriesName(Globals.getInstance().getValue().loadedCategories);

                setCategorySpinnerAdapter(
                        categoriesName,
                        getActivity()
                );

                if(selectedCategory!=null &&
                        selectedCategory.categoryName!=null &&
                        !selectedCategory.categoryName.equals("") &&
                        !selectedCategory.categoryName.equals("null"))
                    spinnerCategory.setSelection(categoriesName.indexOf(selectedCategory.categoryName));

                break;
//                getSubcategoryList();
            case REQUEST_SUBCATEGORIES:
                Globals.getInstance().getValue().loadedSubCategories =
                        SubCategory.addDefaultSubCategoryToStartOfList(getActivity(),(ArrayList<SubCategory>)result);

                ArrayList<String> subCategoriesName =
                        SubCategory.getSubCategoriesName(Globals.getInstance().getValue().loadedSubCategories);

                setSubcategorySpinnerAdapter(
                        subCategoriesName,
                        getActivity());

                int selectedPos = subCategoriesName.indexOf(selectedSubcategory.subCategoryName);
                if(selectedSubcategory!=null &&
                        selectedSubcategory.subCategoryName!=null &&
                        !selectedSubcategory.subCategoryName.equals("") &&
                        !selectedSubcategory.subCategoryName.equals("null") &&
                        selectedPos != -1)
                    spinnerSubcategory.setSelection(selectedPos);

                break;
        }
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
        charsooActivitycontext.hideWaitDialog();
        new DialogMessage(getActivity(), ServerAnswer.getError(getActivity(), errorCode, getActivity().getString(R.string.err_network_connection))).show();
    }

    public boolean isVerified() {

        if (spinnerCategory.getSelectedItemPosition() == 0) {
            new DialogMessage(getActivity(), getActivity().getString(R.string.txt_PleaseSelectCategory)).show();
            return false;
        }

        if (spinnerSubcategory.getSelectedItemPosition() == 0) {
            new DialogMessage(getActivity(), getActivity().getString(R.string.txt_PleaseSelectSubcategory)).show();
            return false;
        }

        if(editTextHashtags.getText().length()!=0){
            if(!editTextHashtags.getText().toString().startsWith("#") ||  editTextHashtags.getText().toString().length()<=2){
                new DialogMessage(getActivity(), getActivity().getString(R.string.HASHTAG_TEXT)).show();
                return  false;
            }}

        return true;
    }


    public ArrayList<String> getHashtagList() {
        return TextProcessor.getHashtags(editTextHashtags.getText().toString());
    }

    public String getInputDescription() {
        return editTextDescription.getText().toString();
    }

    private void setCategorySpinnerAdapter(ArrayList<String> categoriesName, Context context) {
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item,categoriesName);

        spinnerCategory.setAdapter(adapter);
    }

    private void setSubcategorySpinnerAdapter(ArrayList<String> subCategoriesName, Context context) {
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item,subCategoriesName);

        spinnerSubcategory.setAdapter(adapter);
    }

    public void setSpinners(Context c) {

        /*setCategorySpinnerAdapter(categories, c);
        setSubcategorySpinnerAdapter(subCategories,c);
        spinnerCategory.setSelection(selectedCategoryPosition);
        spinnerSubcategory.setSelection(selectedSubcategoryPosition);*/
    }

}
