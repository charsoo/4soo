package ir.rasen.charsoo.view.widgets.imageviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class RoundedTopSquareImageView extends ImageView {

    public RoundedTopSquareImageView(Context context) {
        super(context);
        init();
    }

    public RoundedTopSquareImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RoundedTopSquareImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

}