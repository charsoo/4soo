package ir.rasen.charsoo.view.activity.business;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.business.DeleteImageFromGallery;
import ir.rasen.charsoo.model.business.GetImageFromGallery;
import ir.rasen.charsoo.model.business.Report;
import ir.rasen.charsoo.view.activity.ActivityBusinessImage;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IWebServiceDeleteItem;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.shared.ScreenUtils;
import ir.rasen.charsoo.view.widgets.MyPopUpMenu;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.imageviews.ImageViewCircular;


public class ActivityBusinessGallery extends Activity implements IWebServiceDeleteItem,TryAgainListener {

    public static String TAG = "ActivityBusinessGallery";
    public TextViewFont username, date;
    public ImageView imageView;
    DialogMessage dialogMessage;
    Business business;
    private static final int GET_IMAGE_FROM_GALLER_REQUEST = 18;
    String userid = LoginInfo.getInstance().getUserUniqueId();
    String galleryid = "";
    ImageViewCircular imageViewCircular;
    public int w;
    MyPopUpMenu mPopUp;
    int req_report = 100,req_deleteimagefromgallery = 200 ;
    Boolean isowner;
    ActivityBusinessImage activityBusinessImage = new ActivityBusinessImage();
    BroadcastReceiver cancelShareReceiver;
    public static Boolean update = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_gallery);


        TextViewFont title=(TextViewFont)findViewById(R.id.textView_business_identifier_header);
        title.setText(Globals.getInstance().getValue().businessVisiting.name);
        galleryid = getIntent().getExtras().getString(Params.Gallery_Id);
        update = false;

        new GetImageFromGallery(ActivityBusinessGallery.this, galleryid,
                ActivityBusinessGallery.this, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

        dialogMessage = new DialogMessage(this, "");
        w = ScreenUtils.convertDpToPx(this, this.getResources().getDimension(R.dimen.profile_pic));

        username = (TextViewFont) findViewById(R.id.user_name);
        date = (TextViewFont) findViewById(R.id.date);
        imageView = (ImageView) findViewById(R.id.imageView);
        ImageView imageView_back = (ImageView) findViewById(R.id.imageView_back);
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ImageView more = (ImageView) findViewById(R.id.more);
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isowner)
                    mPopUp = new MyPopUpMenu(ActivityBusinessGallery.this,v,R.menu.poput_menu_report_delete);
                else
                    mPopUp = new MyPopUpMenu(ActivityBusinessGallery.this,v,R.menu.poput_menu_report);
                mPopUp = new MyPopUpMenu(ActivityBusinessGallery.this,v,R.menu.poput_menu_report_delete);

                mPopUp.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.popup_report:
                                new Report(ActivityBusinessGallery.this,galleryid
                                        ,ActivityBusinessGallery.this,req_report,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                                break;

                            case R.id.popup_delete:
                                new DeleteImageFromGallery(ActivityBusinessGallery.this, galleryid, ActivityBusinessGallery.this
                                        , req_deleteimagefromgallery, LoginInfo.getInstance().userAccessToken,0).exectueWithNewSolution();
                                break
                            ;
                        }
                        return false;
                    }
                });
                mPopUp.show();
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_business_gallery, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
            return  true;
        }


        if (item.getItemId() == R.id.action_report) {


            new Report(ActivityBusinessGallery.this,galleryid
                    ,ActivityBusinessGallery.this,req_report,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
            return true;
        }if (item.getItemId() == R.id.action_delete) {
            if(isowner)
                 new DeleteImageFromGallery(ActivityBusinessGallery.this,galleryid,ActivityBusinessGallery.this
                    ,req_deleteimagefromgallery,LoginInfo.getInstance().userAccessToken,0).exectueWithNewSolution();
            else
                Toast.makeText(ActivityBusinessGallery.this,"",Toast.LENGTH_LONG).show();

            return true;
        }
        else {
            return super.onOptionsItemSelected(item);
        }

    }




    public void init() {
        userid = LoginInfo.getInstance().getUserUniqueId();

        if (isNetworkAvailable()) {
        }
    }

    @Override
    public void onBackPressed() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        finish();
    }




    @Override
    public void getResult(int request, Object result,int positon) throws Exception {

        try {
            business = (Business) result;
            username.append(" "+business.name);
            isowner = business.isowner;
            date.append(business.LastUpdate);
            SimpleLoader simpleLoader = new SimpleLoader(this);
            simpleLoader.loadImage(business.profilePictureUniqueId, Image_M.LARGE, Image_M.ImageType.USER, imageView);
            simpleLoader.loadImage(business.UserImageId, Image_M.LARGE, Image_M.ImageType.USER
                    , (ImageViewCircular) findViewById(R.id.drawer_user_pic), w, w, null, business.PublisherUserId);
        }

        catch (Exception e){
            e.printStackTrace();
        }
        if(request == req_report)
            Toast.makeText(ActivityBusinessGallery.this,"ثبت شد",Toast.LENGTH_LONG).show();
        else
            if(request == req_deleteimagefromgallery){
                Toast.makeText(ActivityBusinessGallery.this,getResources().getString(R.string.Delete_Image),Toast.LENGTH_LONG).show();
                try {
                    ActivityBusinessImage.onImageDeletedListener.onImageDeleted();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();
            }


    }

//    private void callbroadcast() {
//        cancelShareReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//
////                String postId = intent.getExtras().getString(Params.POST_ID_INT, "0");
////
////                //remove canceled post
////                cancelShare(postId);
////
////                //update time line
////                Intent intentUpdateTimeLine = new Intent(Params.UPATE_TIME_LINE);
////                intentUpdateTimeLine.putExtra(Params.UPDATE_TIME_LINE_TYPE, Params.UPATE_TIME_LINE_TYPE_CANCEL_SHARE);
////                intentUpdateTimeLine.putExtra(Params.POST_ID_INT, postId);
////                LocalBroadcastManager.getInstance(parentActivity).sendBroadcast(intentUpdateTimeLine);
////            }
////        };
////        LocalBroadcastManager.getInstance(parentActivity).registerReceiver(cancelShareReceiver, new IntentFilter(Params.CANCEL_USER_SHARE_POST));
//            }}}

    @Override
    public void getError(int request, Integer errorCode, String callerStringID,int posion) throws Exception {
            dialogMessage.show();
            dialogMessage.setMessage(ServerAnswer.getError(ActivityBusinessGallery.this, errorCode, callerStringID + ">" + this.getLocalClassName()));
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(ActivityBusinessGallery.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void doTryAgain() {
//        new GetImageFromGallery(this, userid, galleryid, this, GET_IMAGE_FROM_GALLER_REQUEST, LoginInfo.getInstance().userAccessToken);
    }
}
