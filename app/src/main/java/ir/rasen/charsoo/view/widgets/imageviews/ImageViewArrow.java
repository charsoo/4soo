package ir.rasen.charsoo.view.widgets.imageviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import ir.rasen.charsoo.R;

public class ImageViewArrow extends ImageView {

    boolean isExpanded = false;

    public ImageViewArrow(Context context) {
        super(context);
        init();
    }

    public ImageViewArrow(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ImageViewArrow(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setExpanded(isExpanded);
    }

    public void setExpanded(boolean isExpanded) {
        this.isExpanded = isExpanded;
        if(isExpanded)
            setImageResource(R.mipmap.ic_up);
        else
            setImageResource(R.mipmap.ic_down);
    }
}