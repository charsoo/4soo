package ir.rasen.charsoo.view.dialog;

import android.view.View;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.model.business.UnblockUser;
import ir.rasen.charsoo.view.interface_m.IUnblockUserListener;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.material_library.widgets.Dialog;


public class DialogUnBlockUserConfirmation extends Dialog {
    public DialogUnBlockUserConfirmation
            (final CharsooActivity charsooActivity, final String ownerBusinessId,
             final String userBlockedId ,
             final IUnblockUserListener iUnblockUserListener) {

        super(charsooActivity, charsooActivity.getString(R.string.txt_dialog_unblock_user),
                charsooActivity.getString(R.string.txt_dialog_block_user_confrim));

        addCancelButton(charsooActivity.getResources().getString(R.string.cancel));
        setAcceptText(R.string.yes);

        setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        //set onClickListener for the ok button
        setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                charsooActivity.showWaitDialog();
                new UnblockUser(charsooActivity,ownerBusinessId
                        ,userBlockedId,iUnblockUserListener, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                dismiss();
            }
        });

    }

}
