package ir.rasen.charsoo.view.interface_m;

import ir.rasen.charsoo.controller.object.Comment;

/**
 * Created by android on 3/9/2015.
 */
public interface ICommentChange {
    void notifyDeleteComment(String commentId) throws Exception;
    void notifyDeleteCommentFailed(String commentId,String errorMessag) throws Exception;
    void notifyUpdateComment(Comment comment) throws Exception;
    void notifyUpdateCommentFailed(String commentId,String errorMessag) throws Exception;
}
