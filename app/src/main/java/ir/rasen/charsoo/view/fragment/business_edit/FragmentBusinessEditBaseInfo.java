package ir.rasen.charsoo.view.fragment.business_edit;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.ImageHelper;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.TextProcessor;
import ir.rasen.charsoo.controller.helper.Validation;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.Category;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.SubCategory;
import ir.rasen.charsoo.model.business.GetBusinessCategories;
import ir.rasen.charsoo.model.business.GetBusinessSubcategories;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessEdit;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IGetCallForTakePicture;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.buttons.FloatButton;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.edittexts.EditTextFont;
import ir.rasen.charsoo.view.widgets.imageviews.ExpandableImageView;

public class FragmentBusinessEditBaseInfo extends Fragment implements IWebservice /*ISelectCategory*/ {
    public static final String TAG = "FragmentBusinessEditBaseInfo";

    private static final int REQUEST_CATEGORIES = 1, REQUEST_SUBCATEGORIES = 2;

//    private WaitDialog progressDialog;
    private EditTextFont editTextName, editTextIdentifier, editTextDescription, editTextHashtags ,editTextInstagram;
    private MyApplication myApplication;
    DialogMessage dialogMessage;

    private ExpandableImageView imageViewPicture;
    private IGetCallForTakePicture iGetCallForTakePicture;
    private boolean isEditing=false;
    private Business editingBusiness;

    //    ISelectCategory iSelectCategory;
    Spinner spinnerCategory, spinnerSubcategory;
    //    ISelectCategory iSelectCategory;

    public static Category selectedCategory;
    public static SubCategory selectedSubcategory;

    CharsooActivity charsooactivity;

    public void setPicture(Bitmap bitmap) {
        imageViewPicture.setImageBitmap(ImageHelper.getRoundedCornerBitmap(bitmap, 20));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            iGetCallForTakePicture = (IGetCallForTakePicture) activity;
        } catch (ClassCastException castException) {
            /** The activity does not implement the listener. */
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CustomActivityOnCrash.install(getActivity());

        View view = inflater.inflate(R.layout.fragment_register_business_base_info,
                container, false);
        isEditing = getArguments().getBoolean(Params.IS_EDITTING);
        charsooactivity = (CharsooActivity) getActivity();
//        progressDialog = new WaitDialog(getActivity());
//        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        dialogMessage = new DialogMessage(getActivity(), "");

        spinnerCategory = (Spinner) view.findViewById(R.id.spinner_Category);
        spinnerSubcategory = (Spinner) view.findViewById(R.id.spinner_Subcategory);


        if(selectedCategory == null || selectedSubcategory == null){
            selectedCategory = new Category("-1",Globals.getInstance().getValue().businessEditing.categoryName);
            selectedSubcategory = new SubCategory("-1",Globals.getInstance().getValue().businessEditing.subcategoryName);
        }

        if(Globals.getInstance().getValue().loadedCategories == null ||
                Globals.getInstance().getValue().loadedCategories.size()==0){
            charsooactivity.showWaitDialog();
            new GetBusinessCategories(getActivity(), FragmentBusinessEditBaseInfo.this,
                    REQUEST_CATEGORIES, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
        }else{
            charsooactivity.hideWaitDialog();
            ArrayList<String> categoriesName =
                    Category.getCategoriesName(Globals.getInstance().getValue().loadedCategories);

            setCategorySpinnerAdapter(
                    categoriesName,
                    getActivity());

            if(
                    selectedCategory!=null &&
                            selectedCategory.categoryName!=null &&
                            !selectedCategory.categoryName.equals("") &&
                            !selectedCategory.categoryName.equals("null")) {

                spinnerCategory.setSelection(categoriesName.indexOf(selectedCategory.categoryName));
            }
        }



        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==0){
                    spinnerSubcategory.setVisibility(View.INVISIBLE);
                }
                else if(i>0) {
                    spinnerSubcategory.setVisibility(View.VISIBLE);

                    selectedCategory = Globals.getInstance().getValue().loadedCategories.get(i);
                    charsooactivity.showWaitDialog();

                    String selectedCategoryId = Globals.getInstance().getValue().loadedCategories.get(i).categoryUniqueId;

                    new GetBusinessSubcategories(
                            getActivity(),selectedCategoryId,FragmentBusinessEditBaseInfo.this,
                            REQUEST_SUBCATEGORIES,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

                    selectedCategory.categoryName = Globals.getInstance().getValue().loadedCategories.get(i).categoryName;
                    selectedCategory.categoryUniqueId = selectedCategoryId;

                    Globals.getInstance().getValue().businessEditing.categoryUniqueId = selectedCategoryId;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerSubcategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i>0) {
                    if(selectedSubcategory == null)
                        selectedSubcategory = new SubCategory();
                    selectedSubcategory.subCategoryName =
                            Globals.getInstance().getValue().loadedSubCategories.get(i).subCategoryName;

                    selectedSubcategory.subCaregoryUniqueId =
                            Globals.getInstance().getValue().loadedSubCategories.get(i).subCaregoryUniqueId;

                    Globals.getInstance().getValue().businessEditing.subCategoryUniqueId = selectedSubcategory.subCaregoryUniqueId;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        editTextDescription = (EditTextFont) view.findViewById(R.id.edt_description);
        editTextHashtags = (EditTextFont) view.findViewById(R.id.edt_hashtags);
        editTextIdentifier = (EditTextFont) view.findViewById(R.id.edt_identifier);
        editTextName = (EditTextFont) view.findViewById(R.id.edt_name);
        editTextInstagram = (EditTextFont) view.findViewById(R.id.edt_instagram);


        imageViewPicture = (ExpandableImageView) view.findViewById(R.id.imageView_cover);
        FloatButton imageViewCamera = (FloatButton) view.findViewById(R.id.btn_camera);
        imageViewCamera.setDrawableIcon(R.drawable.ic_camera); // for some reasons!! it's needed
        imageViewCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iGetCallForTakePicture.notifyCallForTakePicture();
            }
        });


        myApplication = (MyApplication) getActivity().getApplication();


        //if the user is editing the business
        if (isEditing) {
            editingBusiness = Globals.getInstance().getValue().businessEditing;

            if(ActivityBusinessEdit.myBitmap!=null)
                setPicture(ActivityBusinessEdit.myBitmap);
            else {
                SimpleLoader simpleLoader = new SimpleLoader(getActivity());
                simpleLoader.loadImage(editingBusiness.profilePictureUniqueId, Image_M.LARGE, Image_M.ImageType.BUSINESS, imageViewPicture);
            }
            editTextName.setText(editingBusiness.name);
            editTextIdentifier.setText(editingBusiness.businessIdentifier);
            editTextIdentifier.setEnabled(false);
            editTextDescription.setText(editingBusiness.description);
            editTextInstagram.setText(editingBusiness.IdInstagram);
            String hashtags = "";
            for (String hashtag : editingBusiness.hashtagList){
                hashtags += "#" + hashtag;
            }

            editTextHashtags.setText(hashtags);
            TextProcessor.parsTextForHashtag(editTextHashtags
                    , getResources().getColor(R.color.hashtagColor)
                    , getResources().getColor(R.color.accentColor)
                    , getResources().getColor(R.color.hashtagBackgroundColor));

            //process hashtags
            editTextHashtags.addTextChangedListener(new TextWatcher() {
                String oldText;

                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                    oldText = charSequence.toString();
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                    if (charSequence.toString().equals(oldText))
                        return;
//                TextProcessor.processEdtHashtags(editTextHashtags.getText().toString(), editTextHashtags, getActivity());
                    TextProcessor.parsTextForHashtag(editTextHashtags
                            , getResources().getColor(R.color.hashtagColor)
                            , getResources().getColor(R.color.accentColor)
                            , getResources().getColor(R.color.hashtagBackgroundColor));
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });

            editTextName.requestFocus();
        }
        imageViewPicture.post(new Runnable() {
            @Override
            public void run() {
                imageViewPicture.requestFocus();
            }
        });
        return view;
    }

    @Override
    public void getResult(int request, Object result) {
//        progressDialog.dismiss();
        charsooactivity.hideWaitDialog();
        switch (request) {
            case REQUEST_CATEGORIES:
                Globals.getInstance().getValue().loadedCategories =
                        Category.addDefaultCategoryToStartOfList(getActivity(),(ArrayList<Category>)result);

                ArrayList<String> categoriesName =
                        Category.getCategoriesName(Globals.getInstance().getValue().loadedCategories);

                setCategorySpinnerAdapter(categoriesName,getActivity());

                if(selectedCategory!=null &&
                        selectedCategory.categoryName!=null &&
                        !selectedCategory.categoryName.equals("") &&
                        !selectedCategory.categoryName.equals("null"))
                    spinnerCategory.setSelection(categoriesName.indexOf(selectedCategory.categoryName));

                break;
//                getSubcategoryList();
            case REQUEST_SUBCATEGORIES:
                Globals.getInstance().getValue().loadedSubCategories =
                        SubCategory.addDefaultSubCategoryToStartOfList(getActivity(),(ArrayList<SubCategory>)result);

                ArrayList<String> subCategoriesName =
                        SubCategory.getSubCategoriesName(Globals.getInstance().getValue().loadedSubCategories);

                setSubcategorySpinnerAdapter(
                        subCategoriesName,
                        getActivity());

                int selectedPos = subCategoriesName.indexOf(selectedSubcategory.subCategoryName);
                if(selectedSubcategory!=null &&
                        selectedSubcategory.subCategoryName!=null &&
                        !selectedSubcategory.subCategoryName.equals("") &&
                        !selectedSubcategory.subCategoryName.equals("null") &&
                        selectedPos != -1)
                    spinnerSubcategory.setSelection(selectedPos);
                break;
        }
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
//        progressDialog.dismiss();
        charsooactivity.hideWaitDialog();
        if (!dialogMessage.isShowing()) {
            dialogMessage.show();
            dialogMessage.setMessage(ServerAnswer.getError(getActivity(), errorCode, callerStringID + ">" + TAG));
//            new DialogMessage(getActivity(), ServerAnswer.getError(getActivity(), errorCode, callerStringID + ">" + TAG)).show();
        }
    }

    public boolean isVerified() {

        if (!Validation.validateName(getActivity(), editTextName.getText().toString()).isValid()) {
            editTextName.setError(Validation.getErrorMessage());
            return false;
        }
        if (!Validation.validateBusinessIdentifier(getActivity(), editTextIdentifier.getText().toString()).isValid()) {
            editTextIdentifier.setError(Validation.getErrorMessage());
            return false;
        }
        if (spinnerCategory.getSelectedItemPosition() == 0) {
            new DialogMessage(getActivity(), getActivity().getString(R.string.txt_PleaseSelectCategory)).show();
            return false;
        }

        if (spinnerSubcategory.getSelectedItemPosition() == 0) {
            new DialogMessage(getActivity(), getActivity().getString(R.string.txt_PleaseSelectSubcategory)).show();
            return false;
        }
        if (!editTextDescription.getText().toString().equals("") &&
                !Validation.validateAboutMe(getActivity(), editTextDescription.getText().toString()).isValid()) {
            editTextDescription.setError(Validation.getErrorMessage());
            return false;
        }
//        if (!editTextHashtags.getText().toString().equals("") &&
//                !Validation.va(getActivity(),editTextDescription.getText().toString()).isValid()) {
//            editTextDescription.setError(Validation.getErrorMessage());
//            return false;
//        }

        //initial MyApplication.business to pass the data
        Business business = Globals.getInstance().getValue().businessEditing;
        business.name = editTextName.getText().toString();
        business.businessIdentifier = editTextIdentifier.getText().toString();
        business.categoryUniqueId = selectedCategory.categoryUniqueId;
        business.subCategoryUniqueId = selectedSubcategory.subCaregoryUniqueId;
        business.description = editTextDescription.getText().toString();
        business.hashtagList = TextProcessor.getHashtags(editTextHashtags.getText().toString());
        business.IdInstagram = editTextInstagram.getText().toString();
        //profile Pictuer is setted on ActivityBusinessEdit.displayCropedImage(String filePath);
        Globals.getInstance().getValue().businessEditing = business;

        return true;
    }

    private void setCategorySpinnerAdapter(ArrayList<String> categoriesName, Context context) {

        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item,
                        categoriesName);

        spinnerCategory.setAdapter(adapter);
    }

    private void setSubcategorySpinnerAdapter(ArrayList<String> subCategoriesName, Context context) {
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item,
                        subCategoriesName);
        spinnerSubcategory.setAdapter(adapter);
    }


    public boolean isBusinessDirty(Business originalBusiness) {
        boolean isDirty = false;
        if (!isDirty && !originalBusiness.name.equals(editTextName.getText().toString()))
            isDirty = true;
        if (!isDirty && !originalBusiness.businessIdentifier.equals(editTextIdentifier.getText().toString()))
            isDirty = true;
        if (!isDirty && !originalBusiness.categoryName.equals(spinnerCategory.getSelectedItem().toString()))
            isDirty = true;
        String originalCat = originalBusiness.categoryName;
        String selectedCat = spinnerCategory.getSelectedItem().toString();


        if (!isDirty && !originalBusiness.subcategoryName.equals(spinnerSubcategory.getSelectedItem()))
            isDirty = true;
        if (!isDirty && !originalBusiness.description.equals(editTextDescription.getText().toString()))
            isDirty = true;
        if (!isDirty && !originalBusiness.hashtagList.equals(TextProcessor.getHashtags(editTextHashtags.getText().toString())))
            isDirty = true;
        if (!isDirty && !originalBusiness.IdInstagram.equals(editTextInstagram.getText().toString()))
            isDirty = true;
        return isDirty;
    }
}
