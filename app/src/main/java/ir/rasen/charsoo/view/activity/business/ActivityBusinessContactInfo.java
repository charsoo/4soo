package ir.rasen.charsoo.view.activity.business;


import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.flurry.android.FlurryAgent;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.WorkDays;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.business.GetBusinessProfileInfo;
import ir.rasen.charsoo.view.activity.ActivityMapDisplay;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IEditContactInfo;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;


public class ActivityBusinessContactInfo extends CharsooActivity implements IWebservice,
        NetworkStateChangeListener,IEditContactInfo {

    public static final String TAG="ActivityBusinessContactInfo";

    public static final int GET_BUSINESS_PROFILE_INFO_REQ_CODE = 100;

    public   int count;

    //    WaitDialog progressDialog;
    Business business;
    public String workTimeStr;
    public static IEditContactInfo iEditContactInfo;

//    TextViewFont tvWorkTimeDays;

    TextViewFont weekday0,weekday1,weekday2,weekday3,weekday4,weekday5,weekday6;
    TextViewFont worktime0,worktime1,worktime2,worktime3,worktime4,worktime5,worktime6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.activity_business_contact_info);
        setTitle(getResources().getString(R.string.call_info));

        iEditContactInfo = ActivityBusinessContactInfo.this;

        showWaitDialog();
        try {
            business= Globals.getInstance().getValue().businessVisiting.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        new GetBusinessProfileInfo(this, business.uniqueId, this,
                GET_BUSINESS_PROFILE_INFO_REQ_CODE, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

        if(getIntent().getExtras()!=null && getIntent().getBooleanExtra(Params.BUSINESS_OWNER, false)) {
            findViewById(R.id.imageView_edit).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.imageView_edit).setVisibility(View.GONE);
        }
        findViewById(R.id.info_view).setVisibility(View.GONE);

        SimpleLoader simpleLoader = new SimpleLoader(this);
        simpleLoader.loadImage(business.profilePictureUniqueId, Image_M.LARGE, Image_M.ImageType.BUSINESS, (ImageView) findViewById(R.id.imageView_cover));
//        businessProfilePictureUniqueId=Integer.valueOf(myApplication.business.profilePictureUniqueId);
//        ((TextViewFont) findViewById(R.uniqueId.editText_name)).setText(myApplication.business.name);
//        ((TextViewFont) findViewById(R.uniqueId.editText_identifier)).setText(myApplication.business.businessIdentifier);

//        tvWorkTimeDays = (TextViewFont) findViewById(R.id.info_workTime_days_tv);
//        tvWorkTimeDays.setText("");

        weekday0=(TextViewFont)findViewById(R.id.info_workTime_days_tv0);
        weekday1=(TextViewFont)findViewById(R.id.info_workTime_days_tv1);
        weekday2=(TextViewFont)findViewById(R.id.info_workTime_days_tv2);
        weekday3=(TextViewFont)findViewById(R.id.info_workTime_days_tv3);
        weekday4=(TextViewFont)findViewById(R.id.info_workTime_days_tv4);
        weekday5=(TextViewFont)findViewById(R.id.info_workTime_days_tv5);
        weekday6=(TextViewFont)findViewById(R.id.info_workTime_days_tv6);

        worktime0=(TextViewFont)findViewById(R.id.clock0);
        worktime1=(TextViewFont)findViewById(R.id.clock1);
        worktime2=(TextViewFont)findViewById(R.id.clock2);
        worktime3=(TextViewFont)findViewById(R.id.clock3);
        worktime4=(TextViewFont)findViewById(R.id.clock4);
        worktime5=(TextViewFont)findViewById(R.id.clock5);
        worktime6=(TextViewFont)findViewById(R.id.clock6);

        weekday0.setText("");
        weekday1.setText("");
        weekday2.setText("");
        weekday3.setText("");
        weekday4.setText("");
        weekday5.setText("");
        weekday6.setText("");

        worktime0.setText("");
        worktime1.setText("");
        worktime2.setText("");
        worktime3.setText("");
        worktime4.setText("");
        worktime5.setText("");
        worktime6.setText("");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }


    private void init(){

        findViewById(R.id.info_view).setVisibility(View.VISIBLE);
        //((TextViewFont) findViewById(R.uniqueId.edt_description)).setText(business.description);
        if(business.phone!=null && business.phone.length()>0) {
            findViewById(R.id.info_phoneView).setVisibility(View.VISIBLE);
            findViewById(R.id.info_phoneViewTelephone).setVisibility(View.VISIBLE);
            ((TextViewFont) findViewById(R.id.info_phone)).setText(business.phone);
        } else if (getIntent().getBooleanExtra(Params.BUSINESS_OWNER, false)){
            findViewById(R.id.info_phoneView).setVisibility(View.VISIBLE);
            findViewById(R.id.info_phoneViewTelephone).setVisibility(View.VISIBLE);
            ((TextViewFont) findViewById(R.id.info_phone)).setText(R.string.WRITE_PHONE);
        }else {

            findViewById(R.id.info_phoneView).setVisibility(View.GONE);
            findViewById(R.id.info_phoneViewTelephone).setVisibility(View.GONE);
        }
        if(business.mobile!=null && business.mobile.length()>0) {
            findViewById(R.id.info_phoneView).setVisibility(View.VISIBLE);
            findViewById(R.id.info_phoneViewMobile).setVisibility(View.VISIBLE);
            ((TextViewFont) findViewById(R.id.info_mobile)).setText(business.mobile);
        } else if(getIntent().getBooleanExtra(Params.BUSINESS_OWNER,false)){
            findViewById(R.id.info_phoneView).setVisibility(View.VISIBLE);
            findViewById(R.id.info_phoneViewMobile).setVisibility(View.VISIBLE);
            ((TextViewFont) findViewById(R.id.info_mobile)).setText(R.string.WRITE_TEL);
        }else
            findViewById(R.id.info_phoneViewMobile).setVisibility(View.GONE);


        if(business.workDays.getDayTimes().size()!=0){


            ArrayList<WorkDays.WorkDaysGroup> ret = convertToWorkDaysGroup(business.workDays);

            count= ret.size();

            if(ret.size()>0){
                if(ret.get(0).mDayTimes.get(0).isSet()){
                    weekday0.setText(GetDayName(ret.get(0).mDayTimes.get(0).mCurrentDay));
                    String clock_first = String.valueOf(ret.get(0).mDayTimes.get(0).mOpenTime.mHourTime) + ":" + String.valueOf(ret.get(0).mDayTimes.get(0).mOpenTime.mMinuteTime);
                    String clock_second = String.valueOf(ret.get(0).mDayTimes.get(0).mCloseTime.mHourTime) + ":" + String.valueOf(ret.get(0).mDayTimes.get(0).mCloseTime.mMinuteTime);
                    worktime0.setText(clock_first +" "+ getResources().getString(R.string.ta)+" " + clock_second);
                    worktime0.setVisibility(View.VISIBLE);
                    weekday0.setVisibility(View.VISIBLE);
                    count--;
                }

                if( count>0 && ret.get(1).mDayTimes.get(0).isSet()){
                    weekday1.setText(GetDayName(ret.get(1).mDayTimes.get(0).mCurrentDay));
                    String clock_first1 = String.valueOf(ret.get(1).mDayTimes.get(0).mOpenTime.mHourTime) + ":" + String.valueOf(ret.get(1).mDayTimes.get(0).mOpenTime.mMinuteTime);
                    String clock_second1 = String.valueOf(ret.get(1).mDayTimes.get(0).mCloseTime.mHourTime) + ":" + String.valueOf(ret.get(1).mDayTimes.get(0).mCloseTime.mMinuteTime);
                    worktime1.setText(clock_first1 + " " + getResources().getString(R.string.ta)+" " + clock_second1);
                    worktime1.setVisibility(View.VISIBLE);
                    weekday1.setVisibility(View.VISIBLE);
                    count--;
                }

                if(count>0 && ret.get(2).mDayTimes.get(0).isSet()){

                    weekday2.setText(GetDayName(ret.get(2).mDayTimes.get(0).mCurrentDay));
                    String clock_first2 = String.valueOf(ret.get(2).mDayTimes.get(0).mOpenTime.mHourTime) + ":" + String.valueOf(ret.get(2).mDayTimes.get(0).mOpenTime.mMinuteTime);
                    String clock_second2 = String.valueOf(ret.get(2).mDayTimes.get(0).mCloseTime.mHourTime) + ":" + String.valueOf(ret.get(2).mDayTimes.get(0).mCloseTime.mMinuteTime);
                    worktime2.setText(clock_first2 + " " + getResources().getString(R.string.ta)+" "+ clock_second2);
                    worktime2.setVisibility(View.VISIBLE);
                    weekday2.setVisibility(View.VISIBLE);
                    count--;
                }
                if(count>0 && ret.get(3).mDayTimes.get(0).isSet()){
                    weekday3.setText(GetDayName(ret.get(3).mDayTimes.get(0).mCurrentDay));
                    String clock_first3 = String.valueOf(ret.get(3).mDayTimes.get(0).mOpenTime.mHourTime) + ":" + String.valueOf(ret.get(3).mDayTimes.get(0).mOpenTime.mMinuteTime);
                    String clock_second3 = String.valueOf(ret.get(3).mDayTimes.get(0).mCloseTime.mHourTime) + ":" + String.valueOf(ret.get(3).mDayTimes.get(0).mCloseTime.mMinuteTime);
                    worktime3.setText(clock_first3 +  " " + getResources().getString(R.string.ta)+" " + clock_second3);
                    worktime3.setVisibility(View.VISIBLE);
                    weekday3.setVisibility(View.VISIBLE);
                    count--;

                }
                if(count>0 && ret.get(4).mDayTimes.get(0).isSet()){

                    weekday4.setText(GetDayName(ret.get(4).mDayTimes.get(0).mCurrentDay));
                    String clock_first4 = String.valueOf(ret.get(4).mDayTimes.get(0).mOpenTime.mHourTime) + ":" + String.valueOf(ret.get(4).mDayTimes.get(0).mOpenTime.mMinuteTime);
                    String clock_second4 = String.valueOf(ret.get(4).mDayTimes.get(0).mCloseTime.mHourTime) + ":" + String.valueOf(ret.get(4).mDayTimes.get(0).mCloseTime.mMinuteTime);
                    worktime4.setText(clock_first4 +  " " + getResources().getString(R.string.ta)+" " + clock_second4);
                    worktime4.setVisibility(View.VISIBLE);
                    weekday4.setVisibility(View.VISIBLE);
                    count--;
                }
                if(count>0 && ret.get(5).mDayTimes.get(0).isSet()){

                    weekday5.setText(GetDayName(ret.get(5).mDayTimes.get(0).mCurrentDay));
                    String clock_first5 = String.valueOf(ret.get(5).mDayTimes.get(0).mOpenTime.mHourTime) + ":" + String.valueOf(ret.get(5).mDayTimes.get(0).mOpenTime.mMinuteTime);
                    String clock_second5 = String.valueOf(ret.get(5).mDayTimes.get(0).mCloseTime.mHourTime) + ":" + String.valueOf(ret.get(5).mDayTimes.get(0).mCloseTime.mMinuteTime);
                    worktime5.setText(clock_first5 +  " " + getResources().getString(R.string.ta)+" " + clock_second5);
                    worktime5.setVisibility(View.VISIBLE);
                    weekday5.setVisibility(View.VISIBLE);
                    count--;
                }
                if(count>0 && ret.get(6).mDayTimes.get(0).isSet()){

                    weekday6.setText(GetDayName(ret.get(6).mDayTimes.get(0).mCurrentDay));
                    String clock_first6 = String.valueOf(ret.get(6).mDayTimes.get(0).mOpenTime.mHourTime) + ":" + String.valueOf(ret.get(6).mDayTimes.get(0).mOpenTime.mMinuteTime);
                    String clock_second6 = String.valueOf(ret.get(6).mDayTimes.get(0).mCloseTime.mHourTime) + ":" + String.valueOf(ret.get(6).mDayTimes.get(0).mCloseTime.mMinuteTime);
                    worktime6.setText(clock_first6 +  " " + getResources().getString(R.string.ta)+" " + clock_second6);
                    worktime6.setVisibility(View.VISIBLE);
                    weekday6.setVisibility(View.VISIBLE);
                    count--;
                }

            }}
//            workTimeStr = WorkDays.createWorkDaysStr(this, business.workDays.getDayTimes());
        if(count==0){
            workTimeStr = getResources().getString(R.string.nothing_day_selected);
            weekday0.setVisibility(View.VISIBLE);
            weekday0.setText(workTimeStr);
        }


        ((TextViewFont) findViewById(R.id.info_location)).setText((business.stateName != null && business.stateName.length() > 0 ? business.stateName + "," + "\n" : "")
                + (business.cityName != null && business.cityName.length() > 0 ? business.cityName + "," + "\n" : "")
                + (business.address != null && business.address.length() > 0 ? business.address : ""));
        if(((TextViewFont) findViewById(R.id.info_location)).getText().toString().length()>0) {
            findViewById(R.id.info_location).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.info_location).setVisibility(View.GONE);
        }

//        String open = business.workDays.getTimeWorkOpenWebservice();
//        String close = business.workDays.getTimeWorkCloseWebservice();
//        String workTime = getString(R.string.from) + " " + open + " " + getString(R.string.until) + " " + close;
//        if(open.equals("") && close.equals("") && getIntent().getBooleanExtra(Params.BUSINESS_OWNER,false))
//            ((TextViewFont) findViewById(R.id.info_workTime)).setText(R.string.WORKTIME);
//        else if(!open.equals("")&& !close.equals(""))
//            ((TextViewFont) findViewById(R.id.info_workTime)).setText(workTime);
//        else
//            findViewById(R.id.info_workTimeView).setVisibility(View.GONE);

        if(business.webSite!=null && business.webSite.length()>0) {
            ((TextViewFont) findViewById(R.id.info_website)).setText(business.webSite);
            findViewById(R.id.info_websiteView).setVisibility(View.VISIBLE);
        } else if (getIntent().getBooleanExtra(Params.BUSINESS_OWNER,false)){
            findViewById(R.id.info_websiteView).setVisibility(View.VISIBLE);
            ((TextViewFont) findViewById(R.id.info_website)).setText(R.string.WEBSITE);
        }else
            findViewById(R.id.info_websiteView).setVisibility(View.GONE);

        if(business.email!=null && business.email.length()>0) {
            ((TextViewFont) findViewById(R.id.info_email)).setText(business.email);
            findViewById(R.id.info_emailView).setVisibility(View.VISIBLE);
        } else if (getIntent().getBooleanExtra(Params.BUSINESS_OWNER,false)){
            findViewById(R.id.info_emailView).setVisibility(View.VISIBLE);
            ((TextViewFont) findViewById(R.id.info_email)).setText(R.string.EMAIL);
        }else
            findViewById(R.id.info_emailView).setVisibility(View.GONE);

        if(business.IdInstagram!=null && business.IdInstagram.length()>0){
            ((TextViewFont) findViewById(R.id.info_Instagram)).setText(business.IdInstagram);
            findViewById(R.id.info_instagramView).setVisibility(View.VISIBLE);
        }
        else if(getIntent().getBooleanExtra(Params.BUSINESS_OWNER,false)){
            findViewById(R.id.info_instagramView).setVisibility(View.VISIBLE);
            ((TextViewFont) findViewById(R.id.info_Instagram)).setText(R.string.Id_InsertInstagram);
        }
        else
            findViewById(R.id.info_instagramView).setVisibility(View.GONE);}


    public String GetDayName(int dayVal){
        String ret="Shanbe";
        switch (dayVal){
            case 0:
                ret = getResources().getString(R.string.SHANBE);
                break;
            case 1:
                ret =getResources().getString(R.string.YEK_SHANBE);
                break;
            case 2:
                ret =getResources().getString(R.string.DO_SHANBE);
                break;
            case 3:
                ret =getResources().getString(R.string.SE_SHANBE);
                break;
            case 4:
                ret =getResources().getString(R.string.CHAHAR_SHANBE);
                break;
            case 5:
                ret =getResources().getString(R.string.PANJ_SHANBE);
                break;
            case 6:
                ret =getResources().getString(R.string.JOME);
                break;
        }
        return ret;
    }
    public static ArrayList<WorkDays.WorkDaysGroup> convertToWorkDaysGroup(WorkDays workDays){
        ArrayList<WorkDays.WorkDaysGroup> wdgs = new ArrayList<>();
        ArrayList<WorkDays.DayTime> dayTimes = (ArrayList<WorkDays.DayTime>) workDays.mDayTimes.clone();

        for(int i=0;i<dayTimes.size();i++){
            if(!dayTimes.get(i).isSet())
                continue;
            WorkDays.WorkDaysGroup workDaysGroup = new WorkDays.WorkDaysGroup();
            workDaysGroup.mDayTimes.add(dayTimes.get(i));
            wdgs.add(workDaysGroup);
        }

        return wdgs;
    }

    @Override
    public void getResult(int reqCode,Object result) {
        switch(reqCode){
            case GET_BUSINESS_PROFILE_INFO_REQ_CODE:
                hideWaitDialog();
                if (!(result instanceof Business))
                    return;
                String businessProfilePictureId=business.profilePictureUniqueId;
                String businessIntId=business.uniqueId;
                String businessStrId=business.businessIdentifier;
                try {
                    business = ((Business) result).clone();
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
                business.profilePictureUniqueId =businessProfilePictureId;
                business.uniqueId =businessIntId;
                business.businessIdentifier=businessStrId;
                init();

                NetworkConnectivityReciever.removeIfHaveListener(TAG);

                break;
        }
    }

    @Override
    public void getError(int reqCode,Integer errorCode,String callerStringID) {
        switch(reqCode){
            case GET_BUSINESS_PROFILE_INFO_REQ_CODE:
//                progressDialog.dismiss();
                hideWaitDialog();
                new DialogMessage(ActivityBusinessContactInfo.this, ServerAnswer.getError(ActivityBusinessContactInfo.this, errorCode,callerStringID+">" +this.getLocalClassName())).show();
                if (errorCode==ServerAnswer.NETWORK_CONNECTION_ERROR){
                    NetworkConnectivityReciever.setNetworkStateListener(TAG,ActivityBusinessContactInfo.this);
                }

                break;
        }
    }

    @Override
    public void doOnNetworkConnected() {
//        progressDialog.show();
        showWaitDialog();
        new GetBusinessProfileInfo(this, Globals.getInstance().getValue().businessVisiting.uniqueId, this
                , GET_BUSINESS_PROFILE_INFO_REQ_CODE, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
    }

    public void showOnMap(View view) {
        Intent intent = new Intent(ActivityBusinessContactInfo.this, ActivityMapDisplay.class);
        intent.putExtra(Params.LATITUDE, Double.valueOf(business.location_m.getLatitude()));
        intent.putExtra(Params.LONGITUDE, Double.valueOf(business.location_m.getLongitude()));
        startActivity(intent);
    }

    public void back(View view) {
        onBackPressed();
    }

    public void edit(View view) {
        Intent intent = new Intent(this, ActivityBusinessEdit.class);
        intent.putExtra(Params.BUSINESS_PICUTE_ID,business.profilePictureUniqueId);
        intent.putExtra(Params.BUSINESS_ID_STRING, business.uniqueId);
        intent.putExtra(Params.BUSINESS_IDENTIFIER, business.businessIdentifier);
        startActivityForResult(intent, Params.ACTION_EDIT_BUSINESS);
    }

    @Override
    public void onContactEdited() {
        showWaitDialog();
        new GetBusinessProfileInfo(this, business.uniqueId, this,
                GET_BUSINESS_PROFILE_INFO_REQ_CODE, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
