package ir.rasen.charsoo.view.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Timer;
import java.util.TimerTask;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.view.fragment.search.FragmentSearch;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;

public class ActivityMapChoose extends CharsooActivity{

    // Google Map
    private GoogleMap googleMap;
    MapView mapView;
    //LatLng latLng;
    LatLng choosedLatLng;
    Marker marker;
    MenuItem menuItemTik;
    boolean isLocationInitialized = false;
    LocationManager locationManager;
    private static final int locationCeckIntervalTime=5000;
    String gpsProvider,networkProvider;
    boolean gps_enabled=false,network_enabled=false;
    LatLng loc;
    Timer timer;

    public static String templat;
    public static String templong;


    //boolean isFirstTime = true;

//    private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
//        @Override
//        public void onMyLocationChange(Location location) {
//            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
//
//            if (googleMap != null && !isLocationInitialized) {
//                marker = googleMap.addMarker(new MarkerOptions().position(loc));
//                menuItemTik.setVisible(true);
//                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 14.0f));
//                choosedLatLng = loc;
//                isLocationInitialized = true;
//
//            }
//        }
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setTitle(getString(R.string.choose_location));
        setContentView(R.layout.activity_map);

        templat = "";
        templong = "";


        locationManager=(LocationManager)getSystemService(Context.LOCATION_SERVICE);
        networkProvider=LocationManager.NETWORK_PROVIDER;
        gpsProvider=LocationManager.GPS_PROVIDER;
        checkIfLocationEnabled();
        timer=new Timer();
        try {
            MapsInitializer.initialize(ActivityMapChoose.this);
            mapView = (MapView) findViewById(R.id.map);
            mapView.onCreate(savedInstanceState);

            if (mapView != null) {
                googleMap = mapView.getMap();
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                googleMap.setMyLocationEnabled(true);
                googleMap.getUiSettings().setZoomControlsEnabled(true);
//                googleMap.setLocationSource(ActivityMapChoose.this);
//                googleMap.setOnMyLocationChangeListener(myLocationChangeListener);
                googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                    @Override
                    public boolean onMyLocationButtonClick() {
                        if (!isLocationInitialized)
                            return navigateToLocation(null);
                        else
                            return false;
                    }
                });


            }
            if (getIntent().getExtras().getBoolean(Params.IS_EDITTING)) {
                double lat,lng;
                try {
                    lat=Double.valueOf(getIntent().getStringExtra(Params.LATITUDE));
                    lng=Double.valueOf(getIntent().getStringExtra(Params.LONGITUDE));
                    if (lat!=0 || lng!=0) {
                        LatLng loc = new LatLng(lat, lng);
                        isLocationInitialized = false;
                        doOnLocationChanged(loc);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    isLocationInitialized=false;
                    doOnLocationChanged(latLng);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onBackPressed() {
        mapView.onPause();
        finish();
    }

    @Override
    public void onResume() {
        mapView.onResume();
        (new Handler()).post(new Runnable() {
            @Override
            public void run() {
                if (!isLocationInitialized) {
                    if (navigateToLocation(null)) {
                        replaceMarker(loc,getString(R.string.business_location));
                        if (menuItemTik!=null)
                            menuItemTik.setVisible(true);
                        choosedLatLng = loc;
                        isLocationInitialized = true;
                    }
                    else{
                        if (gps_enabled)
                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
                                    gpsLocationListener);
                        if (network_enabled)
                            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0,
                                    networkLocationListener);
                    }
                }
            }
        });
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else if (item.getItemId() == R.id.action_tik) {
            Intent i = getIntent();
//            Bundle bundle = new Bundle();
            templat = String.valueOf(choosedLatLng.latitude);
            templong = String.valueOf(choosedLatLng.longitude);
//            bundle.putString("latitude", String.valueOf(choosedLatLng.latitude));
//            bundle.putString("longitude", String.valueOf(choosedLatLng.longitude));
// set Fragmentclass Arguments
//            FragmentSearch fragmentsearch = new FragmentSearch();
//            fragmentsearch.onCreate(bundle);
//            fragmentsearch.setArguments(bundle);
            i.putExtra(Params.LATITUDE, String.valueOf(choosedLatLng.latitude));
            i.putExtra(Params.LONGITUDE, String.valueOf(choosedLatLng.longitude));
            setResult(RESULT_OK, i);
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_tik_button, menu);
        menuItemTik = menu.findItem(R.id.action_tik);
        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isLocationInitialized)
                    menuItemTik.setVisible(false);
                else
                    menuItemTik.setVisible(true);
            }
        },1000);

        return true;
    }


    private void doOnLocationChanged(LatLng loc){
        if (googleMap != null && !isLocationInitialized) {
            navigateToLocation(loc);
            replaceMarker(loc,getString(R.string.business_location));
            if (menuItemTik!=null)
                menuItemTik.setVisible(true);
            choosedLatLng = loc;
            isLocationInitialized = true;
        }
    }

    private void replaceMarker(LatLng loc,String title){
        if (marker != null)
            marker.remove();
        marker = googleMap.addMarker(new MarkerOptions().position(loc)
                .title(title));
    }

    private boolean navigateToLocation(LatLng loc){
        Location location=getMyLocation();
//        if (loc==null)
//            location=getMyLocation();
        if (location != null || loc!=null)
        {
//            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
//                    new LatLng(location.getLatitude(), location.getLongitude()), 17));
            if (loc == null)
                loc = new LatLng(location.getLatitude(), location.getLongitude());
            ActivityMapChoose.this.loc=loc;
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(loc)      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .bearing(0)                // Sets the orientation of the camera to east
                    .tilt(10)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            return true;
        }
        else{
            timer.schedule(new GetLastLocation(), locationCeckIntervalTime);
            return false;
        }

    }


    private boolean checkIfLocationEnabled() {
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled)
            return false;
        else
            return true;
    }


    private Location getMyLocation() {
        // Get location from GPS if it's available

        Location myLocation=null;
        checkIfLocationEnabled();
        if (!gps_enabled && !network_enabled){
            showLocationFailed();
        }
        else{
            if (network_enabled){
                myLocation=locationManager.getLastKnownLocation(networkProvider);
                if (myLocation==null)
                    locationManager.requestLocationUpdates(networkProvider,0,0,networkLocationListener);
            }
            if (gps_enabled){
                myLocation=locationManager.getLastKnownLocation(gpsProvider);
                if (myLocation==null)
                    locationManager.requestLocationUpdates(gpsProvider,0,0,gpsLocationListener);
            }
        }
        return myLocation;
    }


    private void showLocationFailed(){
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(R.string.ENABLE_MYLOCATION);
        dialog.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
                Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);

                //get gps
            }
        });
        dialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub

            }
        });
        dialog.show();
    }


    class GetLastLocation extends TimerTask {
        @Override
        public void run() {
            Location net_loc = null, gps_loc = null;
            if (gps_enabled)
                gps_loc = locationManager.getLastKnownLocation(gpsProvider);
            if (network_enabled)
                net_loc = locationManager.getLastKnownLocation(networkProvider);

            //if there are both values use the latest one
            if (gps_loc != null && net_loc != null) {
                if (gps_loc.getTime() > net_loc.getTime()) {
                    loc = new LatLng(gps_loc.getLatitude(), gps_loc.getLongitude());
                } else {
                    loc = new LatLng(net_loc.getLatitude(), net_loc.getLongitude());
                }

            } else if (gps_loc != null) {
                loc = new LatLng(gps_loc.getLatitude(), gps_loc.getLongitude());
            } else if (net_loc != null) {
                loc = new LatLng(net_loc.getLatitude(), net_loc.getLongitude());
            }

            if (gps_loc!=null || net_loc!=null){
                locationManager.removeUpdates(networkLocationListener);
                locationManager.removeUpdates(gpsLocationListener);
            }
        }
    }


    LocationListener networkLocationListener=new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            doOnLocationChanged(new LatLng(location.getLatitude(),location.getLongitude()));
            locationManager.removeUpdates(networkLocationListener);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }
        @Override
        public void onProviderEnabled(String provider) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0,
                        networkLocationListener);
        }
        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    LocationListener gpsLocationListener=new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            doOnLocationChanged(new LatLng(location.getLatitude(),location.getLongitude()));
            locationManager.removeUpdates(gpsLocationListener);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }
        @Override
        public void onProviderEnabled(String provider) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
                    gpsLocationListener);
        }
        @Override
        public void onProviderDisabled(String provider) {

        }
    };


    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this,"2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
