package ir.rasen.charsoo.view.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.model.user.UnFollowBusiness;
import ir.rasen.charsoo.view.interface_m.IUnfollowBusiness;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.material_library.widgets.Dialog;


public class DialogUnfollowBusinessConfirmation extends Dialog {
    Context context;


    @SuppressLint("NewApi")
    public DialogUnfollowBusinessConfirmation(final Context context, final String buisnessId, final IUnfollowBusiness iUnfollowBusiness ) {
        super(context, context.getResources().getString(R.string.unfollow),
                context.getResources().getString(R.string.confirmation_unfollow));

        //set onClickListener for the ok button
        setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((CharsooActivity)context).showWaitDialog();
                //progressDialog will be closed in getResult or getError in calling class
                new UnFollowBusiness(context,LoginInfo.getInstance().getUserUniqueId(),buisnessId,iUnfollowBusiness,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;

                dismiss();
            }
        });

    }

}
