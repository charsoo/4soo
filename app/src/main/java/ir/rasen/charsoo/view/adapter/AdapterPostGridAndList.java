package ir.rasen.charsoo.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.SearchItemPost;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.view.activity.ActivityPost;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.fragment.timeline.FragmentTimelineFollowingBusinessPosts;
import ir.rasen.charsoo.view.interface_m.ILikeDislikeListener;
import ir.rasen.charsoo.view.interface_m.IReportPost;
import ir.rasen.charsoo.view.interface_m.IShareCancelShareListener;
import ir.rasen.charsoo.view.interface_m.IUpdateTimeLine;
import ir.rasen.charsoo.view.shared.GridViewBusinessBothView;
import ir.rasen.charsoo.view.shared.GridViewBusinessOtherBothView;
import ir.rasen.charsoo.view.shared.GridViewUserBothView;
import ir.rasen.charsoo.view.shared.GridViewUserOtherBothView;
import ir.rasen.charsoo.view.shared.PostInitializerBothView;
import ir.rasen.charsoo.view.shared.ScreenUtils;
import ir.rasen.charsoo.view.widgets.MaterialProgressBarCircular;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.imageviews.RoundedImageView;
import ir.rasen.charsoo.view.widgets.imageviews.SquareImageView;
import ir.rasen.charsoo.view.widgets.material_library.views.CustomView;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.HFGridView;

/**
 * Created by android on 3/7/2015.
 * EDIT:: MHosseinFathi & SinaKH 4/7/2015 & 5/7/2015
 */
public class AdapterPostGridAndList
        extends BaseAdapter
        implements IUpdateTimeLine, IReportPost,
        ILikeDislikeListener, IShareCancelShareListener {
    public static final String TAG = "AdapterPostGridAndList";
    private Activity activity;
    private ArrayList<Post> items;
    ArrayList<SearchItemPost> searchItems;
    private Context context;
    SimpleLoader simpleLoader;
    String businessIdForBusinessPosts;
    Post.GetPostType getPostType;
    private IReportPost iReportPost;
    private int prevPosition = 0;
    DialogMessage dialogMessage;
    ViewGroup viewGroup;

    public enum OpenedType{
        OPENED_TYPE_BUSINESS,OPENED_TYPE_BUSINESS_OTHER,
        OPENED_TYPE_USER,OPENED_TYPE_USER_OTHER}


    boolean isViewGrid;


    // be surat haye mokhtalef, dar class haye mokhtalef, post ha be namayesh dar mi ayand
    private GridViewUserBothView gridViewUserBothView;
    private GridViewUserOtherBothView gridViewUserOtherBothView;
    private GridViewBusinessOtherBothView gridViewBusinessOtherBothView;
    private GridViewBusinessBothView gridViewBusinessBothView;
    private ActivityPost activityPost;

    public static String parentTag;

    public OpenedType openedType;

    boolean isOwnerTask;

    private CharsooActivity mCharsooActivity;

    public AdapterPostGridAndList(Activity activity,
                                  String businessIdForBusinessPosts, Post.GetPostType getPostType,
                                  GridViewUserBothView gridViewUserBothView, Context context,
                                  ArrayList<Post> items, boolean isGrid,String parentTag) {
        this.activity = activity;
        this.businessIdForBusinessPosts = businessIdForBusinessPosts;
        this.getPostType = getPostType;

        this.gridViewUserBothView = gridViewUserBothView;
        this.context = context;
        this.isViewGrid = isGrid;
        resetItems(items);

        this.parentTag = parentTag;
        openedType = OpenedType.OPENED_TYPE_USER;

        mCharsooActivity = (CharsooActivity) activity;

        init();
    }

    public AdapterPostGridAndList(Activity activity,
                                  String businessIdForBusinessPosts, Post.GetPostType getPostType,
                                  GridViewUserOtherBothView gridViewUserOtherBothView, Context context,
                                  ArrayList<Post> items, boolean isGrid,String parentTag) {
        this.activity = activity;
        this.businessIdForBusinessPosts = businessIdForBusinessPosts;
        this.getPostType = getPostType;

        this.gridViewUserOtherBothView = gridViewUserOtherBothView;
        this.context = context;
        this.isViewGrid = isGrid;

        this.parentTag = parentTag;
        openedType = OpenedType.OPENED_TYPE_USER_OTHER;

        mCharsooActivity = (CharsooActivity) activity;

        resetItems(items);
        init();

    }

    public AdapterPostGridAndList(Activity activity,
                                  String businessIdForBusinessPosts, Post.GetPostType getPostType,
                                  GridViewBusinessBothView gridViewBusinessBothView, Context context,
                                  ArrayList<Post> items, boolean isGrid,String parentTag) {
        this.activity = activity;
        this.businessIdForBusinessPosts = businessIdForBusinessPosts;
        this.getPostType = getPostType;

        this.gridViewBusinessBothView = gridViewBusinessBothView;
        this.context = context;
        this.isViewGrid = isGrid;

        this.parentTag = parentTag;
        openedType = OpenedType.OPENED_TYPE_BUSINESS;


        mCharsooActivity = (CharsooActivity) activity;

        resetItems(items);
        init();

    }

    public AdapterPostGridAndList(Activity activity,
                                  String businessIdForBusinessPosts, Post.GetPostType getPostType,
                                  GridViewBusinessOtherBothView gridViewBusinessOtherBothView, Context context,
                                  ArrayList<Post> items, boolean isGrid,String parentTag) {
        this.activity = activity;
        this.businessIdForBusinessPosts = businessIdForBusinessPosts;
        this.getPostType = getPostType;

        this.gridViewBusinessOtherBothView = gridViewBusinessOtherBothView;
        this.context = context;
        this.isViewGrid = isGrid;

        this.parentTag = parentTag;
        openedType = OpenedType.OPENED_TYPE_BUSINESS_OTHER;


        mCharsooActivity = (CharsooActivity) activity;

        resetItems(items);
        init();

    }



    private void init() {

        isOwnerTask = (openedType==OpenedType.OPENED_TYPE_BUSINESS)?true:false;

        simpleLoader = new SimpleLoader(context);
        iReportPost = this;
        dialogMessage = new DialogMessage(context, "");
    }



    public void loadMoreItems(ArrayList<Post> newItems) {
        this.items.addAll(newItems);
        this.searchItems.addAll(SearchItemPost.getItems(newItems));
        notifyDataSetChanged();
    }


    public void resetItems(ArrayList<Post> newItems) {
        if(newItems==null)
            newItems = new ArrayList<>();
        items = new ArrayList<>();
        searchItems = new ArrayList<>();
        for (Post post : newItems) {
            items.add(post);
            searchItems.add(new SearchItemPost(post));
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        if(isViewGrid)
            return getViewGrid(position,view,viewGroup);
        else
            return getViewList(position,view,viewGroup);
    }

    private View getViewGrid(final int position, View view, ViewGroup viewGroup){
        final HolderGrid holder;
        if (view == null) {
            holder = new HolderGrid();
            view = LayoutInflater.from(activity).inflate(R.layout.item_post_grid, viewGroup, false);
            holder.imageView = (SquareImageView) view.findViewById(R.id.img_grid_post);
            holder.progressBar = (MaterialProgressBarCircular) view.findViewById(R.id.pb_grid_post);
        } else
            holder = (HolderGrid) view.getTag();

        if(holder!=null) {
            holder.progressBar.setVisibility(View.VISIBLE);
            simpleLoader.loadImage(searchItems.get(position).postPictureId, Image_M.MEDIUM,
                    Image_M.ImageType.POST, holder.imageView, holder.progressBar);
            simpleLoader.loadImage(searchItems.get(position).postPictureId, Image_M.MEDIUM, Image_M.ImageType.POST, holder.imageView, holder.progressBar);
            if (holder.imageView != null)
                holder.imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Post.goPostPage(
                                activity, searchItems.get(position).postId,
                                "0", getPostType,isOwnerTask);
                    }
                });
        }

        return view;
    }

    private View getViewList(final int position, View view, ViewGroup viewGroup){
        this.viewGroup=viewGroup;
        HolderList holder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_post, viewGroup, false);
            holder = new HolderList();
            initialHolder(view, holder);
            view.setTag(holder);
        } else if (view.getTag() instanceof HolderList)
            holder = (HolderList) view.getTag();
        else {
            view = LayoutInflater.from(context).inflate(R.layout.item_post, viewGroup, false);
            holder = new HolderList();
            initialHolder(view, holder);
            view.setTag(holder);
        }

        items.get(position).type = Post.Type.CompleteBusiness;

        initCompleteBusinessPost(view, position, holder,isOwnerTask);


        if (position > prevPosition) {
            prevPosition = position;
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.abc_grow_fade_in_from_bottom);
            view.startAnimation(animation);
        }

        return view;
    }

    @Override
    public void notifyReportPost(String id, ImageView imageViewMore) {
        int position = -1;
        for (int i = 0; i < items.size(); i++)
            if (items.get(i).uniqueId.equals(id)) {
                position = i;
                break;
            }
        if (position == -1)
            return;
        items.get(position).isReported = true;
        imageViewMore.setVisibility(View.GONE);
    }

    @Override
    public void onDislikeSuccessful(String dislikedPostIntId) {
        MyApplication.broadCastUpdateDirtyPost(parentTag,dislikedPostIntId);
    }

    @Override
    public void onDislikeFailed(String dislikePostIntId, String failureMessage) {
        mCharsooActivity.showNoConnectionToast();

        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).uniqueId.equals(dislikePostIntId)) {
//                View v=getView(i,null,ViewGroup)
                items.get(i).isLiked = true;
                items.get(i).likeNumber++;
//                updatePost(items.get(i));
//                notifyDataSetChanged();
                try
                {
                    int firstVisibleItemPosition=((HFGridView)viewGroup).getFirstVisiblePosition()-1;// -1 be khatere header dashtane viewGroup ast
                    int lastVisibleItemPosition=((HFGridView)viewGroup).getLastVisiblePosition()-1;
                    if (i>=firstVisibleItemPosition && i<=lastVisibleItemPosition) {
                        View v = viewGroup.getChildAt(i - firstVisibleItemPosition);
                        ((ImageView) v.findViewById(R.id.cb_imageView_like)).setImageResource(R.mipmap.ic_liked);
                        ((TextViewFont) v.findViewById(R.id.cb_textView_like_number)).setText(String.valueOf(items.get(i).likeNumber));
                    }
                }catch (Exception ee){}
            }
        }

    }

    @Override
    public void onLikeSuccessful(String dislikedPostIntId) {
        MyApplication.broadCastUpdateDirtyPost(parentTag,dislikedPostIntId);
    }

    @Override
    public void onLikeFailed(String dislikePostIntId, String failureMessage) {
        mCharsooActivity.showNoConnectionToast();
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).uniqueId.equals(dislikePostIntId)) {
                items.get(i).isLiked = false;
                items.get(i).likeNumber--;
//                updatePost(items.get(i));
//                notifyDataSetChanged();
                try
                {
                    int firstVisibleItemPosition=((HFGridView)viewGroup).getFirstVisiblePosition()-1;// -1 be khatere header dashtane viewGroup ast
                    int lastVisibleItemPosition=((HFGridView)viewGroup).getLastVisiblePosition()-1;
                    if (i>=firstVisibleItemPosition && i<=lastVisibleItemPosition) {
                        View v = viewGroup.getChildAt(i - firstVisibleItemPosition);
                        ((ImageView) v.findViewById(R.id.cb_imageView_like)).setImageResource(R.mipmap.ic_like);
                        ((TextViewFont) v.findViewById(R.id.cb_textView_like_number)).setText(String.valueOf(items.get(i).likeNumber));
                    }
                }catch (Exception ee){}
            }
        }
    }

    public void updatePost(Post post) {
        if (gridViewBusinessOtherBothView != null)
            gridViewBusinessOtherBothView.updatePost(post);
    }

    @Override
    public void notifyUpdateTimeLineShare(String postId) {
        Intent intent = new Intent(Params.UPATE_TIME_LINE);
        intent.putExtra(Params.UPDATE_TIME_LINE_TYPE, Params.UPATE_TIME_LINE_TYPE_SHARE);
        intent.putExtra(Params.POST_ID_INT, postId);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @Override
    public void notifyUpdateTimeLineCancelShare(String postId) {
        Intent intent = new Intent(Params.UPATE_TIME_LINE);
        intent.putExtra(Params.UPDATE_TIME_LINE_TYPE, Params.UPATE_TIME_LINE_TYPE_CANCEL_SHARE);
        intent.putExtra(Params.POST_ID_INT, postId);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

        removePostByIntID(postId);
        if (gridViewUserBothView != null)
            gridViewUserBothView.notifyOnShareCanceled(postId);
    }

    @Override
    public void onShareResult(boolean isSuccessful, String postIntId, String failureMessage) {
        if(isSuccessful){
            MyApplication.broadCastUpdateDirtyPost(parentTag,postIntId);
        }
        else{
            mCharsooActivity.showNoConnectionToast();
            for (int i = 0; i < items.size(); i++) {
                if (postIntId.equals(items.get(i).uniqueId)) {
                    items.get(i).isShared = false;
                    items.get(i).shareNumber--;
                    try
                    {
                        int firstVisibleItemPosition=((HFGridView)viewGroup).getFirstVisiblePosition()-1;// -1 be khatere header dashtane viewGroup ast
                        int lastVisibleItemPosition=((HFGridView)viewGroup).getLastVisiblePosition()-1;
                        if (i>=firstVisibleItemPosition && i<=lastVisibleItemPosition) {
                            View v = viewGroup.getChildAt(i - firstVisibleItemPosition);
                            ((ImageView) v.findViewById(R.id.cb_imageView_share)).setImageResource(R.mipmap.ic_share);
                            ((TextViewFont) v.findViewById(R.id.cb_textView_share_number)).setText(String.valueOf(items.get(i).shareNumber));
                        }
                    }catch (Exception ee){}
                }
            }
        }

    }

    @Override
    public void onCancelShareResult(boolean isSuccessful, String postIntId, String failureMessage) {
        if(isSuccessful){
            MyApplication.broadCastUpdateDirtyPost(parentTag,postIntId);
        }
        else{
            mCharsooActivity.showNoConnectionToast();
            for (int i = 0; i < items.size(); i++) {
                if (postIntId.equals(items.get(i).uniqueId)) {
                    items.get(i).isShared = true;
                    items.get(i).shareNumber++;
                    try
                    {
                        int firstVisibleItemPosition=((HFGridView)viewGroup).getFirstVisiblePosition()-1;// -1 be khatere header dashtane viewGroup ast
                        int lastVisibleItemPosition=((HFGridView)viewGroup).getLastVisiblePosition()-1;
                        if (i>=firstVisibleItemPosition && i<=lastVisibleItemPosition) {
                            View v = viewGroup.getChildAt(i - firstVisibleItemPosition);
                            ((ImageView) v.findViewById(R.id.cb_imageView_share)).setImageResource(R.mipmap.ic_shared);
                            ((TextViewFont) v.findViewById(R.id.cb_textView_share_number)).setText(String.valueOf(items.get(i).shareNumber));
                        }
                    }catch (Exception ee){}
                }
            }
        }

    }

    private class HolderList {


        // Complete business post
        LinearLayout cb_llCompleteSection;
        RoundedImageView cb_imageViewProfileImage;
        TextViewFont cb_textViewBusinessIdentifier;
        TextViewFont cb_textViewDate;
        ImageView cb_imageViewPost;
        CustomView cb_pbPost;
        ImageView cb_imageViewPostLike;
        TextViewFont cb_textViewLikeNumber;
        TextViewFont cb_textViewCommentNumber;
        TextViewFont cb_textViewShareNumber;
        TextViewFont cb_textViewDescription;
        TextViewFont cb_textViewComment1UserIdentifier, cb_textViewComment2UserIdentifier, cb_textViewComment3UserIdentifier;
        TextViewFont cb_textViewComment1, cb_textViewComment2, cb_textViewComment3;
        View cb_comment1, cb_comment2, cb_comment3;
        ImageView cb_imgComment1, cb_imgComment2, cb_imgComment3;
        TextViewFont cb_textViewPrice, cb_textViewTitle;
        ImageView cb_imageViewLike, cb_imageViewComment, cb_imageViewShare, cb_imageViewMore;
        TextViewFont cb_textViewCode;
        LinearLayout cb_llPriceSection, cb_llCodeSection;
        RelativeLayout cb_rlSections;


        // follow announcement
        LinearLayout af_llAnnouncementSection;
        RoundedImageView af_imageViewProfileImageShared;
        TextViewFont af_textViewAnnouncementUserIdentifier, af_textViewAnnouncementBusiness;
        View af_btnView;


        // review announcement
        LinearLayout ar_llAnnouncementSection;
        RoundedImageView ar_imageViewProfileImageShared;
        TextViewFont ar_textViewAnnouncementUserIdentifier, ar_textViewAnnouncementBusiness;
        View ar_btnView;
    }

    private class HolderGrid {
        ImageView imageView;
        MaterialProgressBarCircular progressBar;
    }


    public void initialHolder(View view, HolderList holder) {
        // complete business post
        holder.cb_llCompleteSection = (LinearLayout) view.findViewById(R.id.completeBusinessPostContainer);
        holder.cb_imageViewProfileImage = (RoundedImageView) view.findViewById(R.id.cb_imageView_profile_picture);
        holder.cb_textViewBusinessIdentifier = (TextViewFont) view.findViewById(R.id.cb_textView_business_identifier);
        holder.cb_textViewDate = (TextViewFont) view.findViewById(R.id.cb_textView_date);
        holder.cb_imageViewPost = (ImageView) view.findViewById(R.id.cb_imageView_post);
        holder.cb_pbPost = (CustomView) view.findViewById(R.id.cb_progressBarIndeterminate);
        holder.cb_imageViewPostLike = (ImageView) view.findViewById(R.id.cb_imageView_post_like);
        holder.cb_textViewLikeNumber = (TextViewFont) view.findViewById(R.id.cb_textView_like_number);
        holder.cb_textViewCommentNumber = (TextViewFont) view.findViewById(R.id.cb_textView_comment_number);
        holder.cb_textViewShareNumber = (TextViewFont) view.findViewById(R.id.cb_textView_share_number);
        holder.cb_textViewDescription = (TextViewFont) view.findViewById(R.id.cb_textView_description);
        holder.cb_textViewComment1UserIdentifier = (TextViewFont) view.findViewById(R.id.cb_textView_comment1_user_identifier);
        holder.cb_textViewComment2UserIdentifier = (TextViewFont) view.findViewById(R.id.cb_textView_comment2_user_identifier);
        holder.cb_textViewComment3UserIdentifier = (TextViewFont) view.findViewById(R.id.cb_textView_comment3_user_identifier);
        holder.cb_textViewComment1 = (TextViewFont) view.findViewById(R.id.cb_textView_comment1);
        holder.cb_textViewComment2 = (TextViewFont) view.findViewById(R.id.cb_textView_comment2);
        holder.cb_textViewComment3 = (TextViewFont) view.findViewById(R.id.cb_textView_comment3);
        holder.cb_imgComment1 = (ImageView) view.findViewById(R.id.cb_comment1_img);
        holder.cb_imgComment2 = (ImageView) view.findViewById(R.id.cb_comment2_img);
        holder.cb_imgComment3 = (ImageView) view.findViewById(R.id.cb_comment3_img);
        holder.cb_comment1 = view.findViewById(R.id.cb_comment1);
        holder.cb_comment2 = view.findViewById(R.id.cb_comment2);
        holder.cb_comment3 = view.findViewById(R.id.cb_comment3);
        //holder.cb_comment_text = (EditTextFont) view.findViewById(R.uniqueId.cb_comment_text);
        //holder.cb_comment_send = view.findViewById(R.uniqueId.cb_comment_send);
        holder.cb_textViewPrice = (TextViewFont) view.findViewById(R.id.cb_textView_price);
        holder.cb_textViewTitle = (TextViewFont) view.findViewById(R.id.cb_textView_title);
        holder.cb_imageViewLike = (ImageView) view.findViewById(R.id.cb_imageView_like);
        holder.cb_imageViewComment = (ImageView) view.findViewById(R.id.cb_imageView_comment);
        holder.cb_imageViewShare = (ImageView) view.findViewById(R.id.cb_imageView_share);
        holder.cb_imageViewMore = (ImageView) view.findViewById(R.id.cb_imageView_more);
        holder.cb_textViewCode = (TextViewFont) view.findViewById(R.id.cb_textView_code);
        holder.cb_llPriceSection = (LinearLayout) view.findViewById(R.id.cb_ll_price_section);
        holder.cb_llCodeSection = (LinearLayout) view.findViewById(R.id.cb_ll_code_section);
        holder.cb_rlSections = (RelativeLayout) view.findViewById(R.id.cb_rl_sections);

        // follow announcement post
        holder.af_llAnnouncementSection = (LinearLayout) view.findViewById(R.id.announcementFriendFollowContainer);
        holder.af_imageViewProfileImageShared = (RoundedImageView) view.findViewById(R.id.af_imageView_profile_picture_shared);
        holder.af_textViewAnnouncementUserIdentifier = (TextViewFont) view.findViewById(R.id.af_textView_announcement_userIdentifier_title);
        holder.af_textViewAnnouncementBusiness = (TextViewFont) view.findViewById(R.id.af_textView_announcement_business);
        holder.af_btnView = view.findViewById(R.id.af_btn_announcement_view);


        // review announcement post
        holder.ar_llAnnouncementSection = (LinearLayout) view.findViewById(R.id.announcementFriendReviewContainer);
        holder.ar_imageViewProfileImageShared = (RoundedImageView) view.findViewById(R.id.ar_imageView_profile_picture_shared);
        holder.ar_textViewAnnouncementUserIdentifier = (TextViewFont) view.findViewById(R.id.ar_textView_announcement_userIdentifier_title);
        holder.ar_textViewAnnouncementBusiness = (TextViewFont) view.findViewById(R.id.ar_textView_announcement_business);
        holder.ar_btnView = view.findViewById(R.id.ar_btn_announcement_view);

    }

    private void initCompleteBusinessPost(View view, final int position, final HolderList holder,boolean isOwnerTask) {

        // be komake classe post initializer
        //      , halat haye mokhtalefe namayesh ra ijad va handle mikonim

        holder.af_llAnnouncementSection.setVisibility(View.GONE);
        holder.ar_llAnnouncementSection.setVisibility(View.GONE);
        holder.cb_llCompleteSection.setVisibility(View.VISIBLE);

        PostInitializerBothView postInitializer = new PostInitializerBothView(AdapterPostGridAndList.this, context, simpleLoader
                , AdapterPostGridAndList.this, items.get(position), AdapterPostGridAndList.this);

        postInitializer.initPostDetails(holder.cb_imageViewProfileImage, holder.cb_textViewBusinessIdentifier, holder.cb_textViewDate
                , holder.cb_imageViewPost, holder.cb_pbPost, holder.cb_textViewTitle, holder.cb_textViewDescription
                , holder.cb_llPriceSection, holder.cb_textViewPrice, holder.cb_llCodeSection, holder.cb_textViewCode, holder.cb_rlSections);
        postInitializer.initLike(holder.cb_imageViewPost, holder.cb_imageViewLike, holder.cb_textViewLikeNumber, holder.cb_imageViewPostLike);
        postInitializer.initComment(holder.cb_imageViewComment, holder.cb_textViewCommentNumber,isOwnerTask);
        postInitializer.initShare(holder.cb_imageViewShare, holder.cb_textViewShareNumber);
        if (gridViewBusinessOtherBothView != null ||
                gridViewUserOtherBothView != null ||
                gridViewUserBothView != null)
            postInitializer.initMenu(holder.cb_imageViewMore, iReportPost);
        if (gridViewUserBothView != null || gridViewUserOtherBothView != null)
            postInitializer.initMenuUser(holder.cb_imageViewMore, AdapterPostGridAndList.this, iReportPost);
        if (gridViewBusinessBothView != null)
            postInitializer.initMenuBusiness(holder.cb_imageViewMore, gridViewBusinessBothView);
        if (activityPost != null)
            postInitializer.initMenuPost(holder.cb_imageViewMore, activityPost);
        postInitializer.initComments(view.findViewById(R.id.cb_ll_comments)
                , holder.cb_comment3, holder.cb_imgComment3, holder.cb_textViewComment3UserIdentifier, holder.cb_textViewComment3
                , holder.cb_comment2, holder.cb_imgComment2, holder.cb_textViewComment2UserIdentifier, holder.cb_textViewComment2
                , holder.cb_comment1, holder.cb_imgComment1, holder.cb_textViewComment1UserIdentifier, holder.cb_textViewComment1
                );
    }



    public void removePostByIntID(String postID_int) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).uniqueId.equals(postID_int)) {
                items.remove(i);
                notifyDataSetChanged();
                break;
            }
        }
    }


    public void setViewType(boolean isGrid){
        this.isViewGrid=isGrid;
        notifyDataSetChanged();
    }
}
