package ir.rasen.charsoo.view.activity.business;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.WorkDays;
import ir.rasen.charsoo.view.fragment.work_days.FragmentAddWorkday;
import ir.rasen.charsoo.view.fragment.work_days.FragmentWorkdays;

public class ActivityWorkdays
        extends Activity
        implements OnClickListener{
    ImageView ivSubmit,ivBack;
    View submitBtnArea,backBtnArea;

    FragmentManager fm;
    FragmentTransaction ft;

    public enum Frag{FRAGMENT_WORKDAYS,FRAGMENT_ADD_WORKDAY};

    public Frag currentFragment;

    FragmentWorkdays mFragmentWorkdays;
    FragmentAddWorkday mFragmentAddWorkday;


    public WorkDays mWorkDays;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workday);
        init();

        openFragment(Frag.FRAGMENT_WORKDAYS);
    }

    private void init() {
        ivSubmit = (ImageView) findViewById(R.id.iv_submit);
        ivSubmit.setOnClickListener(this);
        ivBack = (ImageView) findViewById(R.id.iv_back);
        ivBack.setOnClickListener(this);
        submitBtnArea = findViewById(R.id.rl_submit);
        submitBtnArea.setOnClickListener(this);


        backBtnArea = findViewById(R.id.rl_back);
        backBtnArea.setOnClickListener(this);

        mFragmentWorkdays = new FragmentWorkdays();
        mFragmentAddWorkday = new FragmentAddWorkday();

        findViewById(R.id.ll_header).setBackgroundColor(getResources().getColor(R.color.primaryColor));

        mWorkDays = Globals.getInstance().getValue().businessEditing.workDays;
        fm = getFragmentManager();
        ft = fm.beginTransaction();
    }

    public void openFragment(Frag frg){
        switch(frg){
            case FRAGMENT_WORKDAYS:
                ft = fm.beginTransaction();
                ft.replace(R.id.fragmentContainer, mFragmentWorkdays);
                ft.commit();
                currentFragment = frg;
                break;

            case FRAGMENT_ADD_WORKDAY:
                ft = fm.beginTransaction();
                ft.replace(R.id.fragmentContainer,mFragmentAddWorkday);
                ft.commit();
                currentFragment = frg;
                break;
        }
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.iv_submit :
                if(currentFragment == Frag.FRAGMENT_WORKDAYS){
                    Globals.getInstance().getValue().businessEditing.workDays =
                            mWorkDays;
                    finish();
                }else if(currentFragment == Frag.FRAGMENT_ADD_WORKDAY){
                    if(mFragmentAddWorkday.validation()){
                        mWorkDays = mFragmentAddWorkday.remakeWorkDays(mWorkDays);
                        mFragmentAddWorkday.resetTimes();
                        openFragment(Frag.FRAGMENT_WORKDAYS);
                    }
                        else{
                        SnackbarManager.show(
                                Snackbar.with(this)
                                        .text(R.string.worktimes_error)
                                        .color(Color.parseColor("#3f51b5"))
                                        .animation(true)
                        );
                    }
                }
                break;

            case R.id.iv_back :
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if(currentFragment == Frag.FRAGMENT_WORKDAYS){
            super.onBackPressed();
        }else if(currentFragment == Frag.FRAGMENT_ADD_WORKDAY){
            openFragment(Frag.FRAGMENT_WORKDAYS);
        }

    }
}
