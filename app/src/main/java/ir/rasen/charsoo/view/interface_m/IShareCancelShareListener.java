package ir.rasen.charsoo.view.interface_m;

/**
 * Created by hossein-pc on 7/6/2015.
 */
public interface IShareCancelShareListener {
    void onShareResult(boolean isSuccessful,String postIntId,String failureMessage) throws Exception;
    void onCancelShareResult(boolean isSuccessful,String postIntId,String failureMessage) throws Exception;
}
