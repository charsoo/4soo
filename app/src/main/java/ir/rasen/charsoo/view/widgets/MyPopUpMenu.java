package ir.rasen.charsoo.view.widgets;

import android.content.Context;
import android.view.View;
import android.widget.PopupMenu;

/**
 * Created by Rasen_iman on 9/5/2015.
 */
public class MyPopUpMenu extends PopupMenu{
    public MyPopUpMenu(Context context, View anchor,int menuResId) {
        super(context, anchor);
        getMenuInflater().inflate(menuResId,getMenu());
    }
}
