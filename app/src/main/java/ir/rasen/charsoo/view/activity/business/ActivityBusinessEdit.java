package ir.rasen.charsoo.view.activity.business;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;

import java.io.File;
import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.City;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.State;
import ir.rasen.charsoo.model.GetCountryStates;
import ir.rasen.charsoo.model.GetProvinceCities;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.business.GetBusinessProfileInfo;
import ir.rasen.charsoo.model.business.UpdateBusinessProfileInfo;
import ir.rasen.charsoo.view.activity.ActivityCamera;
import ir.rasen.charsoo.view.activity.ActivityGallery;
import ir.rasen.charsoo.view.dialog.DialogAreYouWantExit;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.dialog.PopupSelectCameraGallery;
import ir.rasen.charsoo.view.fragment.business_edit.FragmentBusinessEditBaseInfo;
import ir.rasen.charsoo.view.fragment.business_edit.FragmentBusinessEditContactInfo;
import ir.rasen.charsoo.view.fragment.business_edit.FragmentBusinessEditLocationInfo;
import ir.rasen.charsoo.view.interface_m.IGetCallForTakePicture;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;

public class ActivityBusinessEdit extends CharsooActivity implements
        IWebservice, IGetCallForTakePicture, NetworkStateChangeListener, TryAgainListener {

    public static final String TAG = "ActivityBusinessEdit";

    public static final int GET_BUSINESS_PROFILE_INFO_REQUEST = 100;
    public static final int UPDATE_BUSINESS_PROFILE_REQUEST = 200;
    public static final int GET_STATES_LIST_REQUEST = 12, GET_CITIES_LIST_REQUEST = 14;

    FragmentBusinessEditBaseInfo fragmentBaseInfo;
    FragmentBusinessEditContactInfo fragmentContactInfo;
    FragmentBusinessEditLocationInfo fragmentBusinessEditLocationInfo;
    String filePath, businessPictureString;
    Bundle bundle;
    Business originalBusiness;
    DialogMessage dialogMessage;
    public static Bitmap myBitmap ;
    public static boolean is_edited =false;
    private enum Fragments {BASE_INFO, CONTACT_INFO, LOCATION_INFO}

    private Fragments fragmentCurrent;
    FragmentManager fm;
    FragmentTransaction ft;
    String businessUniqueId = "0";
    String businessIdentifier;
    boolean isInitialed = false;
    RelativeLayout networkFailedLayout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        FragmentBusinessEditBaseInfo.selectedCategory = null;
        FragmentBusinessEditBaseInfo.selectedSubcategory = null;

        setContentView(R.layout.activity_business_register);

        networkFailedLayout = MyApplication.initNetworkErrorLayout(
                getWindow().getDecorView(), ActivityBusinessEdit.this, ActivityBusinessEdit.this);
        dialogMessage = new DialogMessage(ActivityBusinessEdit.this, "");

        try {
            businessUniqueId = getIntent().getExtras().getString(Params.BUSINESS_ID_STRING);
            businessIdentifier = getIntent().getExtras().getString(Params.BUSINESS_IDENTIFIER);
        } catch (Exception e) {

        }

        bundle = new Bundle();
        if (!businessUniqueId.equals("0") || businessUniqueId.length()<36) {


            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    new GetBusinessProfileInfo(ActivityBusinessEdit.this, businessUniqueId,
                            ActivityBusinessEdit.this, GET_BUSINESS_PROFILE_INFO_REQUEST, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

                }
            }, 100);

            bundle.putBoolean(Params.IS_EDITTING, true);

            showWaitDialog();
        }





    }

    public void init() {
        (findViewById(R.id.ll_action_bar)).setVisibility(View.GONE);

        fragmentBaseInfo = new FragmentBusinessEditBaseInfo();
        fragmentContactInfo = new FragmentBusinessEditContactInfo();
        fragmentBusinessEditLocationInfo = new FragmentBusinessEditLocationInfo();
        fragmentCurrent = Fragments.BASE_INFO;

        fragmentBaseInfo.setArguments(bundle);
        fragmentContactInfo.setArguments(bundle);
        fragmentBusinessEditLocationInfo.setArguments(bundle);

        fm = getSupportFragmentManager();
        ft = fm.beginTransaction();
        ft.add(R.id.fragmentContainer, fragmentBaseInfo);


        ft.commit();
        isInitialed = true;

    }

    @Override
    public void onBackPressed() {
        if (isInitialed) {
            switch (fragmentCurrent) {
                case BASE_INFO:
                    if (fragmentContactInfo.isBusinessDirty(originalBusiness) ||
                            fragmentBaseInfo.isBusinessDirty(originalBusiness) ||
                            fragmentBusinessEditLocationInfo.isBusinessDirty(originalBusiness) ||
                            (businessPictureString != null && !businessPictureString.equals(""))) {

                        new DialogAreYouWantExit(ActivityBusinessEdit.this).show();
                        myBitmap=null;
                    } else {
                        View view = this.getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                        myBitmap=null;
                        finish();
                    }

                    break;
                case CONTACT_INFO:
                    fragmentCurrent = Fragments.BASE_INFO;
                    ft = fm.beginTransaction();
                    ft.replace(R.id.fragmentContainer, fragmentBaseInfo);
                    ft.commit();
                    break;
                case LOCATION_INFO:
                    fragmentCurrent = Fragments.CONTACT_INFO;
                    ft = fm.beginTransaction();
                    ft.replace(R.id.fragmentContainer, fragmentContactInfo);
                    ft.commit();
                    break;
            }
        } else {
            fragmentCurrent = Fragments.BASE_INFO;
            finish();
        }
    }

    public void back(View view) {
        onBackPressed();
    }

    public void next(View view) {
        if (!isInitialed)
            return;
        switch (fragmentCurrent) {
            case BASE_INFO:
                if (fragmentBaseInfo.isVerified()) {
                    ft = fm.beginTransaction();
                    ft.replace(R.id.fragmentContainer, fragmentContactInfo);
                    ft.commit();
                    fragmentCurrent = Fragments.CONTACT_INFO;
                }
                break;
            case CONTACT_INFO:
                if (fragmentContactInfo.isVerified()) {
                    ft = fm.beginTransaction();
                    ft.replace(R.id.fragmentContainer, fragmentBusinessEditLocationInfo);
                    ft.commit();
                    fragmentCurrent = Fragments.LOCATION_INFO;

                }
                break;
            case LOCATION_INFO:
                myBitmap = null;
                if(fragmentBusinessEditLocationInfo.isVerified())
                    submit();
                break;
        }
    }


    public void submit() {
        if (fragmentContactInfo.isBusinessDirty(originalBusiness) ||
                fragmentBaseInfo.isBusinessDirty(originalBusiness) ||
                fragmentBusinessEditLocationInfo.isBusinessDirty(originalBusiness) ||
                (businessPictureString != null && !businessPictureString.equals(""))){

            showWaitDialog();
            new UpdateBusinessProfileInfo(
                    ActivityBusinessEdit.this, Globals.getInstance().getValue().businessEditing,
                    ActivityBusinessEdit.this, UPDATE_BUSINESS_PROFILE_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
        }
        else
            finish();


    }

    @Override
    public void getResult(int reqCode, Object result) {
        hideWaitDialog();
        switch (reqCode) {
            case GET_BUSINESS_PROFILE_INFO_REQUEST:
                //GetBusinessProfileInfo's result
                Globals.getInstance().getValue().businessEditing = (Business) result;
                try {
                    originalBusiness = ((Business) result).clone();
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }

                //TODO : Server Does Not Send BusinessIdentifier
                Globals.getInstance().getValue().businessEditing.businessIdentifier = businessIdentifier;
                originalBusiness.businessIdentifier = businessIdentifier;
                init();

                if (Globals.getInstance().getValue().loadedStates == null ||
                        Globals.getInstance().getValue().loadedStates.size() == 0) {
                    new GetCountryStates(ActivityBusinessEdit.this,
                            ActivityBusinessEdit.this, GET_STATES_LIST_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
                }else{
                    getResult(GET_STATES_LIST_REQUEST,Globals.getInstance().getValue().loadedStates);
                }
                break;

            case UPDATE_BUSINESS_PROFILE_REQUEST:
                Toast.makeText(ActivityBusinessEdit.this,"کسب و کار شما با موفقیت ویرایش شد",Toast.LENGTH_LONG).show();
                is_edited=false;

                MyApplication.broadCastEditedBusiness(originalBusiness.uniqueId);
                finish();
                break;

            case GET_STATES_LIST_REQUEST :
                Globals.getInstance().getValue().loadedStates =
                        State.addDefaultStateToStartOfList(ActivityBusinessEdit.this,
                                (ArrayList<State>) result);

                ArrayList<String> stateNames = State.getStatesName(Globals.getInstance().getValue().loadedStates);
                int stateIndex = stateNames.indexOf(Globals.getInstance().getValue().businessEditing.stateName);
                State loadedState = Globals.getInstance().getValue().loadedStates.get(stateIndex);
                loadedState.Recycle();

                new GetProvinceCities(ActivityBusinessEdit.this,loadedState.stateUniqueId,
                        ActivityBusinessEdit.this,GET_CITIES_LIST_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;

                FragmentBusinessEditLocationInfo.selectedState = loadedState;
                break;

            case GET_CITIES_LIST_REQUEST :

                Globals.getInstance().getValue().loadedCities =
                        City.addDefaultCityToStartOfList(ActivityBusinessEdit.this,(ArrayList < City >) result);

                ArrayList<String> citiesName = City.getCitiesName(Globals.getInstance().getValue().loadedCities);
                int cityIndex = citiesName.indexOf(Globals.getInstance().getValue().businessEditing.cityName);
                City loadedCity = Globals.getInstance().getValue().loadedCities.get(cityIndex);

                FragmentBusinessEditLocationInfo.selectedCity = loadedCity;

                break;
        }
        NetworkConnectivityReciever.removeIfHaveListener(TAG);
        networkFailedLayout.setVisibility(View.GONE);
    }

    @Override
    public void getError(int reqCode, Integer errorCode, String callerStringID) {
        hideWaitDialog();
//        new DialogMessage(ActivityBusinessEdit.this, ServerAnswer.getError(ActivityBusinessEdit.this, errorCode, callerStringID + ">" + this.getLocalClassName())).show();
        if (reqCode == GET_BUSINESS_PROFILE_INFO_REQUEST
                && ServerAnswer.canShowErrorLayout(errorCode)) {
            NetworkConnectivityReciever.setNetworkStateListener(TAG, ActivityBusinessEdit.this);
            networkFailedLayout.setVisibility(View.VISIBLE);
        } else if (!dialogMessage.isShowing()) {
            dialogMessage.show();
            dialogMessage.setMessage(ServerAnswer.getError(ActivityBusinessEdit.this, errorCode, callerStringID + ">" + this.getLocalClassName()));
        }
    }

    @Override
    public void doOnNetworkConnected() {
        showWaitDialog();
        new GetBusinessProfileInfo(ActivityBusinessEdit.this, businessUniqueId,
                ActivityBusinessEdit.this, GET_BUSINESS_PROFILE_INFO_REQUEST, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
    }

    @Override
    public void doTryAgain() {
        showWaitDialog();
        new GetBusinessProfileInfo(ActivityBusinessEdit.this, businessUniqueId,
                ActivityBusinessEdit.this, GET_BUSINESS_PROFILE_INFO_REQUEST, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
    }

    @Override
    public void notifyCallForTakePicture() {
        new PopupSelectCameraGallery(ActivityBusinessEdit.this).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == ActivityCamera.CAPTURE_PHOTO) {
                filePath = data.getStringExtra(ActivityCamera.FILE_PATH);
                displayCropedImage(filePath);
            } else if (requestCode == ActivityGallery.CAPTURE_GALLERY) {
                filePath = data.getStringExtra(ActivityGallery.FILE_PATH);
                displayCropedImage(filePath);
            }

        }

    }

    private void displayCropedImage(String filePath) {
        File file = new File(filePath);
        if (file.exists()) {

            myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            try {
                fragmentBaseInfo.setPicture(myBitmap);
                businessPictureString = Image_M.getBase64String(filePath);
                Globals.getInstance().getValue().businessEditing.profilePicture = businessPictureString;
            } catch (Exception e) {
                String s = e.getMessage();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }


}