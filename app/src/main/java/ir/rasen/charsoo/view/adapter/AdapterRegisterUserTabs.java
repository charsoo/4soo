package ir.rasen.charsoo.view.adapter;

/**
 * Created by Sina KH on 6/23/2015.
 */

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ir.rasen.charsoo.view.fragment.invite.FragmentInviteAddFriend;
import ir.rasen.charsoo.view.fragment.invite.FragmentInviteAppList;
import ir.rasen.charsoo.view.fragment.invite.FragmentInviteSMS;

public class AdapterRegisterUserTabs extends FragmentPagerAdapter {
    private int tabCount;
    private Context context;
    String firstPageTitle,secondPageTitle,thirdPageTitle;

    public class Fragments {
        public FragmentInviteAddFriend fragAdd;
        public FragmentInviteAppList fragInvite;
        public FragmentInviteSMS fragSMS;
    }
    Fragments fragments;

    public AdapterRegisterUserTabs(FragmentManager fragmentManager, Context context,String firstPageTitle,String secondPageTitle,String thirdPageTitle,int tabCount) {
        super(fragmentManager);
        this.context = context;
        fragments = new Fragments();
        getFragments().fragAdd=new FragmentInviteAddFriend();
        getFragments().fragInvite=new FragmentInviteAppList();
        getFragments().fragSMS=new FragmentInviteSMS();
        this.firstPageTitle=firstPageTitle;
        this.secondPageTitle=secondPageTitle;
        this.thirdPageTitle=thirdPageTitle;
        this.tabCount=tabCount;
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return tabCount;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        if (tabCount==2){
            switch (position) {
                case 0:
                    return getFragments().fragInvite;
                case 1:
                    return getFragments().fragSMS;
                default:
                    return null;
            }
        }else
        {
            switch (position) {
                case 0:
                    return getFragments().fragAdd;
                case 1:
                    return getFragments().fragInvite;
                case 2:
                    return getFragments().fragSMS;
                default:
                    return null;
            }
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return firstPageTitle;
            case 1:
                return secondPageTitle;
            case 2:
                return thirdPageTitle;
//            case 0:
//                return context.getString(R.string.txt_FindFriends);
//            case 1:
//                return context.getString(R.string.txt_InviteFriends);
//            case 2:
//                return context.getString(R.string.txt_sendSMS);
            default:
                return "";
        }

    }

    public Fragments getFragments() {
        return fragments;
    }

}
