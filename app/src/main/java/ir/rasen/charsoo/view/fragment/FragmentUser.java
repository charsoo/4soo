package ir.rasen.charsoo.view.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.PullToRefreshGrid;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.post.GetPost;
import ir.rasen.charsoo.model.post.GetSharedPosts;
import ir.rasen.charsoo.model.user.GetUserHomeInfo;
import ir.rasen.charsoo.view.activity.ActivityMain;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.INetworkStatusListener;
import ir.rasen.charsoo.view.interface_m.IOnUnsharePostListener;
import ir.rasen.charsoo.view.interface_m.IPostUpdateListener;
import ir.rasen.charsoo.view.interface_m.IPullToRefresh;
import ir.rasen.charsoo.view.interface_m.IUpdateUserProfile;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.shared.GridViewUserBothView;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.HFGridView;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.PullToRefreshGridViewWithHeaderAndFooter;

public class FragmentUser extends Fragment
        implements IWebservice, IUpdateUserProfile, IPullToRefresh,
        NetworkStateChangeListener,TryAgainListener,IPostUpdateListener,IOnUnsharePostListener {

    public static final String TAG = "FragmentUser";

    public static final int GET_SHARED_POSTS_INT_CODE=11,GET_USER_HOME_INFO_INT_CODE=14,GET_SINGLE_POST_INT_CODE=18;
    private int lastFailedWebservice;


    private enum Status {FIRST_TIME, LOADING_MORE, REFRESHING, NONE}
    Status status;

    private HFGridView gridView;
    private String visitedUserId;
    private User user;
    public static IPostUpdateListener iPostUpdateListener;
    public static IUpdateUserProfile iUpdateUserProfile;

//    NOT_USED_GridViewUser gridViewUser;
    GridViewUserBothView gridViewUser;
    ArrayList<Post> sharedPosts;
    BroadcastReceiver cancelShareReceiver, removeRequestAnnouncement, updateUserProfilePicture;
    DialogMessage dialogMessage;
    //PullToRefreshGridViewWithHeaderAndFooter pullToRefreshGridViewWithHeaderAndFooter;
    RelativeLayout networkErrorLayout;

    INetworkStatusListener networkStatusListener;
    Activity parentActivity;

    public void setiNetworkStatusListener(INetworkStatusListener delegate){
        networkStatusListener=delegate;
    }

    //pull_to_refresh_lib
    PullToRefreshGrid pullToRefreshGridView;

    CharsooActivity mCharsooActivity;

    RelativeLayout actionBarLayout;
    ImageView imageViewBack,imageViewSearch;


    public static IOnUnsharePostListener iOnUnsharePostListener;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CustomActivityOnCrash.install(getActivity());

        View view = null;
        try {
            view = inflater.inflate(R.layout.fragment_user,
                    container, false);

            iOnUnsharePostListener = FragmentUser.this;

            mCharsooActivity = (CharsooActivity) getActivity();


            actionBarLayout = (RelativeLayout) view.findViewById(R.id.ll_action_bar);
            actionBarLayout.setVisibility(View.VISIBLE);
            actionBarLayout.setOnClickListener(null);

            imageViewBack = (ImageView) view.findViewById(R.id.imageView_back);
            imageViewSearch = (ImageView) view.findViewById(R.id.imageView_search);

            parentActivity=getActivity();
            iPostUpdateListener =FragmentUser.this;
            networkErrorLayout=MyApplication.initNetworkErrorLayout(view, parentActivity,FragmentUser.this);
            mCharsooActivity.showWaitDialog();

            dialogMessage = new DialogMessage(parentActivity, "");

            visitedUserId = LoginInfo.getInstance().getUserUniqueId();
            iUpdateUserProfile = this;
            iUpdateUserProfile=FragmentUser.this;

//          networkErrorLayout.setVisibility(View.GONE);
//          NetworkConnectivityReciever.setNetworkStateListener(TAG, FragmentUser.this);
            pullToRefreshGridView = new PullToRefreshGrid(parentActivity, (PullToRefreshGridViewWithHeaderAndFooter) view.findViewById(R.id.gridView_HF), FragmentUser.this);
            gridView = pullToRefreshGridView.getGridViewHeaderFooter();
            gridViewUser=null;
            status=Status.FIRST_TIME;
            if (sharedPosts==null) {
                sharedPosts = new ArrayList<>();
                callWebservice(GET_USER_HOME_INFO_INT_CODE, true);
            }else {
                initializeUser();
            }


//            User user=new User();
//            MyApplication myApplication=(MyApplication) parentActivity.getApplication();
//            user.userIdentifier=myApplication.userIdentifier;

            //set progress dialog






//            recursivelyCallHandler();

        } catch (Exception e) {
            String s = e.getMessage();
        }

        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        imageViewSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityMain.openThisActivityAndGotoFragmentSearch(getActivity());
            }
        });

        cancelShareReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                String postId = intent.getExtras().getString(Params.POST_ID_INT, "0");

                //remove canceled post
                cancelShare(postId);

                //update time line
                Intent intentUpdateTimeLine = new Intent(Params.UPATE_TIME_LINE);
                intentUpdateTimeLine.putExtra(Params.UPDATE_TIME_LINE_TYPE, Params.UPATE_TIME_LINE_TYPE_CANCEL_SHARE);
                intentUpdateTimeLine.putExtra(Params.POST_ID_INT, postId);
                LocalBroadcastManager.getInstance(parentActivity).sendBroadcast(intentUpdateTimeLine);
            }
        };
        LocalBroadcastManager.getInstance(parentActivity).registerReceiver(cancelShareReceiver, new IntentFilter(Params.CANCEL_USER_SHARE_POST));


        removeRequestAnnouncement = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle bundle = intent.getExtras();
                if(gridViewUser != null)
                    gridViewUser.hideRequestAnnouncement();
            }
        };
        LocalBroadcastManager.getInstance(parentActivity).registerReceiver(removeRequestAnnouncement, new IntentFilter(Params.REMOVE_REQUEST_ANNOUNCEMENT));

        updateUserProfilePicture = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle bundle = intent.getExtras();
                notifyUpdateUserProfile(bundle.getString(Params.USER_PICUTE));
            }
        };
        LocalBroadcastManager.getInstance(parentActivity).registerReceiver(updateUserProfilePicture, new IntentFilter(Params.UPDATE_USER_PROFILE_PCITURE));

        return view;
    }

    private void initializeUser() {
/*        if (!(parentActivity instanceof IGoToRegisterBusinessActivity))
            return;
        boolean hasRequest = false;*/
        // TODO ActivityUserOther NABAYAD az Activity Main ers bari konad
//        ((ActivityMain) parentActivity).setUserDrawer(user);
        // TODO

        boolean beThreeColumn = gridViewUser == null || gridViewUser.isThreeColumn;
        boolean hasHeader = gridViewUser != null && gridViewUser.hasHeader;
        if ((gridViewUser != null) && (hasHeader))
            gridViewUser.refreshUserData(user, visitedUserId);
        else {
            gridViewUser =
                    new GridViewUserBothView(parentActivity, user,
                            visitedUserId, gridView, dialogMessage,
                            FragmentUser.TAG, FragmentUser.this);
        }
        if (/*(((MyApplication) parentActivity.getApplication()).isUserCreated) &&*/ (sharedPosts != null)) {
            try {
                gridViewUser.InitialGridViewUser(sharedPosts, beThreeColumn, hasHeader);
                gridViewUser.hideLoader();
//                if (sharedPosts.isEmpty())
                    callWebservice(GET_SHARED_POSTS_INT_CODE,false);
            } catch (Exception e) {

            }
        } else {
            if (!pullToRefreshGridView.isRefreshing()) {
                try {
                    gridViewUser.InitialGridViewUser(new ArrayList<Post>(), beThreeColumn, hasHeader);
                } catch (Exception e) {

                }
            }
            if (sharedPosts == null)
                sharedPosts = new ArrayList<>();
            callWebservice(GET_SHARED_POSTS_INT_CODE,false);
            //            ((MyApplication) parentActivity.getApplication()).isUserCreated = true;
        }

    }


    @Override
    public void getResult(int request, Object result) {
        switch (request){
            case GET_USER_HOME_INFO_INT_CODE:
                user = (User) result;
//                LoginInfo.getInstance().userIdentifier = user.userIdentifier;
//                LoginInfo.getInstance().userProfilePictureUniqueId = user.profilePictureUniqueId;
                LoginInfo.getInstance().userBusinesses = user.businesses;
                LoginInfo.getInstance().userReceivedFriendRequestCount =
                        user.friendRequestNumber;

                initializeUser();
                break;
            case GET_SHARED_POSTS_INT_CODE:

                //GetSharedPosts result
                if (status==Status.FIRST_TIME || status==Status.REFRESHING){
                    sharedPosts = new ArrayList<>((ArrayList<Post>) result);
                    pullToRefreshGridView.setResultSize(sharedPosts.size());
                    gridViewUser.InitialGridViewUser(sharedPosts, gridViewUser.isThreeColumn, gridViewUser.hasHeader);
                    if (pullToRefreshGridView.isRefreshing())
                        pullToRefreshGridView.onRefreshComplete();
                }
                else if (status==Status.LOADING_MORE){
                    gridViewUser.addMorePosts((ArrayList<Post>) result);
                    sharedPosts.addAll((ArrayList<Post>) result);
                }
                status=Status.NONE;
                break;

            case GET_SINGLE_POST_INT_CODE :
                Post newPost = (Post) result;
                for (int i = 0; i < sharedPosts.size() ; i++) {
                    if (sharedPosts.get(i).uniqueId.equals(newPost.uniqueId)){
                        sharedPosts.set(i,MyApplication.mergeTwoPost(sharedPosts.get(i),newPost));
                        gridViewUser.resetPostItems(sharedPosts);
                    }
                }
                break;
        }
        if (pullToRefreshGridView.isRefreshing()) {
            pullToRefreshGridView.onRefreshComplete();
        }
//        networkStatusListener.onNetworkStatusChanged(true);
        mCharsooActivity.hideWaitDialog();
        networkErrorLayout.setVisibility(View.GONE);
        NetworkConnectivityReciever.removeIfHaveListener(TAG);
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
        mCharsooActivity.hideWaitDialog();
        pullToRefreshGridView.onRefreshComplete();

        switch (request){
            case GET_USER_HOME_INFO_INT_CODE :
                if ((errorCode == ServerAnswer.NETWORK_CONNECTION_ERROR))
                    NetworkConnectivityReciever.setNetworkStateListener(TAG,FragmentUser.this);

                if(ServerAnswer.canShowErrorLayout(errorCode) && status!=Status.REFRESHING) {
                    networkErrorLayout.setVisibility(View.VISIBLE);
                }
                else if (!dialogMessage.isShowing() && networkErrorLayout.getVisibility()!=View.VISIBLE) {
                    dialogMessage.show();
                    dialogMessage.setMessage(ServerAnswer.getError(parentActivity, errorCode,callerStringID+">"+parentActivity.getLocalClassName()));
                }
                    lastFailedWebservice = request;
                break;

            case GET_SHARED_POSTS_INT_CODE :
                if(errorCode == ServerAnswer.POST_DOES_NOT_EXISTS){
                    gridViewUser.resetPostItems(new ArrayList<Post>());
                }

                gridViewUser.handleShowingNotExistsLayout();
                break;
        }
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
//            if (requestCode == Params.ACTION_ACTIVITY_POST) {
//
//            }
        }
    }

    public void cancelShare(String postId) {
        for (int i = 0; i < sharedPosts.size(); i++) {
            if (sharedPosts.get(i).uniqueId.equals(postId)) {
                sharedPosts.remove(i);
                break;
            }
        }
        gridViewUser.InitialGridViewUser(sharedPosts, gridViewUser.isThreeColumn, gridViewUser.hasHeader);
    }

    @Override
    public void notifyRefresh() {
        status=Status.REFRESHING;
        new GetUserHomeInfo(parentActivity, visitedUserId, LoginInfo.getInstance().getUserUniqueId(),
                FragmentUser.this,GET_USER_HOME_INFO_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
    }

    @Override
    public void notifyLoadMore() {
        status=Status.LOADING_MORE;
        callWebservice(GET_SHARED_POSTS_INT_CODE,false);
    }

    @Override
    public void doOnNetworkConnected() {
        callWebservice(lastFailedWebservice,true);
    }


    private void callWebservice(int reqCode,boolean showProgressDialog){
        if (showProgressDialog)
            mCharsooActivity.showWaitDialog();
        switch (reqCode) {
            case GET_USER_HOME_INFO_INT_CODE:
                new GetUserHomeInfo(parentActivity, visitedUserId, LoginInfo.getInstance().getUserUniqueId(),
                        FragmentUser.this,GET_USER_HOME_INFO_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                break;
            case GET_SHARED_POSTS_INT_CODE:
                if (status!=Status.LOADING_MORE)
                    new GetSharedPosts(parentActivity, visitedUserId, 0,
                            getResources().getInteger(R.integer.lazy_load_limitation),
                            FragmentUser.this,GET_SHARED_POSTS_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                else
                    new GetSharedPosts(parentActivity, visitedUserId,
                            gridViewUser.adapterPostGridAndList.getCount(), getResources().getInteger(R.integer.lazy_load_limitation),
                            FragmentUser.this,GET_SHARED_POSTS_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                break;
        }
    }

    @Override
    public void doTryAgain() {
        callWebservice(lastFailedWebservice,true);
    }

//    @Override
//    public void doOnPostModified(int postIntId) {
//        try {
//            if (sharedPosts!=null)
//                sharedPosts.clear();
//            notifyRefresh();
//        }catch (Exception e){}
//    }

    @Override
    public void onPostDetailsChange(String dirtyPostId) {
        for (int i = 0; i < sharedPosts.size(); i++) {
            if (sharedPosts.get(i).uniqueId.equals(dirtyPostId)){
                new GetPost(parentActivity,LoginInfo.getInstance().getUserUniqueId(),
                        sharedPosts.get(i).businessUniqueId,dirtyPostId, Post.GetPostType.SHARE,
                        FragmentUser.this,GET_SINGLE_POST_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
                break;
            }
        }
    }

    @Override
    public void notifyUpdateUserProfile(String userPictureString) {
        gridViewUser.initialProfilePicture(userPictureString);
    }

    @Override
    public void doUpdateUserProfileData() {
        callWebservice(GET_USER_HOME_INFO_INT_CODE, false);
    }


    @Override
    public void onUnsharePost(String targetPostId) {
        status = Status.REFRESHING;
        callWebservice(GET_SHARED_POSTS_INT_CODE,false);
    }

}
