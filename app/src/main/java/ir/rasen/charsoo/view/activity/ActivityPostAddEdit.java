package ir.rasen.charsoo.view.activity;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;

import java.io.File;
import java.text.DecimalFormat;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.TextProcessor;
import ir.rasen.charsoo.controller.helper.Validation;
import ir.rasen.charsoo.controller.helper.WebservicesHandler;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.post.AddPost;
import ir.rasen.charsoo.model.post.GetPost;
import ir.rasen.charsoo.model.post.UpdatePost;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.dialog.DialogSaveResult;
import ir.rasen.charsoo.view.dialog.PopupSelectCameraGallery;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.widgets.edittexts.EditTextFont;
import ir.rasen.charsoo.view.widgets.edittexts.EditTextFontPasteDisabled;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;


public class ActivityPostAddEdit extends CharsooActivity implements View.OnClickListener, IWebservice,NetworkStateChangeListener,TryAgainListener {

    public static final String TAG="ActivityPostAddEdit";

    public static final int ADD_POST_INT_CODE=10,UPDATE_POST_INT_CODE=20,GET_POST_INT_CODE=11;
    EditTextFont editTextTitle, editTextDescription, editTextCode, editTextHashtags;
    EditTextFontPasteDisabled editTextPrice;
//    WaitDialog progressDialog;
    ImageView imageViewPostPicture;
    String filePath, postPictureString;
    String businessUniqueId;
    String postUniqueId = "0";
    RelativeLayout networkFailedLayout;

    Post originalPost;
    boolean isSubmitClicked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.activity_post_add);

        networkFailedLayout = MyApplication.initNetworkErrorLayout(getWindow().getDecorView()
                ,ActivityPostAddEdit.this,ActivityPostAddEdit.this);

        businessUniqueId = getIntent().getExtras().getString(Params.BUSINESS_ID_STRING);
        try {
            //if there is postUniqueId it means the user is editing the post
            postUniqueId = getIntent().getExtras().getString(Params.POST_ID_INT,"0");
        } catch (Exception e) {

        }
        postPictureString = "";

//        progressDialog = new WaitDialog(this);
//        progressDialog.setMessage(getResources().getString(R.string.please_wait));

        imageViewPostPicture = (ImageView) findViewById(R.id.imageView_user_picture);
        imageViewPostPicture.setOnClickListener(this);
//        (findViewById(R.uniqueId.btn_submit)).setOnClickListener(this);

        editTextTitle = (EditTextFont) findViewById(R.id.edt_title);
        editTextDescription = (EditTextFont) findViewById(R.id.edt_description);
        editTextHashtags = (EditTextFont) findViewById(R.id.edt_hashtags);
        editTextPrice = (EditTextFontPasteDisabled) findViewById(R.id.edt_price);
        editTextCode = (EditTextFont) findViewById(R.id.edt_code);

        if (!postUniqueId.equals("0")) {
            setTitle(getResources().getString(R.string.edit_product));
            ((MyApplication) getApplication()).setCurrentWebservice(WebservicesHandler.Webservices.GET_POST);
            showWaitDialog();
            new GetPost(ActivityPostAddEdit.this, LoginInfo.getInstance().getUserUniqueId(),
                    businessUniqueId, postUniqueId, Post.GetPostType.BUSINESS, ActivityPostAddEdit.this,
                    GET_POST_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
            imageViewPostPicture.setEnabled(false);
        } else {
            setTitle(getString(R.string.new_product));
            (findViewById(R.id.dataContainer)).setVisibility(View.VISIBLE);
        }

        editTextPrice.addTextChangedListener(new TextWatcher() {
            String oldText;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                oldText = charSequence.toString();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (charSequence.toString().equals(oldText))
                    return;
                /*String price = editTextPrice.getText().toString();
                int commaCount = price.length() - price.replace(",", "").length();
                if ((price.length() - counter) % 3 == 0) {
                    editTextPrice.setText(price + ",");
                    counter++;
                }*/

                String price = editTextPrice.getText().toString().replace(",", "");
                if (price.equals(""))
                    return;
                double amount = Double.parseDouble(price);
                DecimalFormat formatter = new DecimalFormat("#,###");
                price = formatter.format(amount);
                editTextPrice.setText(price);
                editTextPrice.setSelection(price.length());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        //process hashtags
        editTextHashtags.addTextChangedListener(new TextWatcher() {
            String oldText;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                oldText = charSequence.toString();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (charSequence.toString().equals(oldText))
                    return;
                TextProcessor.parsTextForHashtag(editTextHashtags
                        ,getResources().getColor(R.color.hashtagColor)
                        ,getResources().getColor(R.color.accentColor)
                        ,getResources().getColor(R.color.hashtagBackgroundColor));
//                TextProcessor.processEdtHashtags(editTextHashtags.getText().toString(), editTextHashtags, ActivityPostAddEdit.this);

            }

            @Override
            public void afterTextChanged(Editable editable) {
//                final Pattern hashtagPattern = Pattern.compile("#([A-Za-z0-9_-\\u0600-\\u06FF\\uFB8A\\u067E\\u0686\\u06AF]*)");
//                Linkify.addLinks(editable, hashtagPattern,null);

            }
        });

        isSubmitClicked = false;
    }

    private void init(){

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == ActivityCamera.CAPTURE_PHOTO) {
                filePath = data.getStringExtra(ActivityCamera.FILE_PATH);
                displayCroppedImage(filePath);
            } else if (requestCode == ActivityGallery.CAPTURE_GALLERY) {
                filePath = data.getStringExtra(ActivityGallery.FILE_PATH);
                displayCroppedImage(filePath);
            }
        }

    }

    private void displayCroppedImage(String filePath) {
        File file = new File(filePath);
        if (file.exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            try {
                imageViewPostPicture.setImageBitmap(myBitmap);
                postPictureString = Image_M.getBase64String(filePath);
            } catch (Exception e) {
                String s = e.getMessage();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_tik_button, menu);
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageView_user_picture:
                new PopupSelectCameraGallery(ActivityPostAddEdit.this).show();
                break;
        }
    }

    public void submit() {
        if (!Validation.validateTitle(ActivityPostAddEdit.this, editTextTitle.getText().toString()).isValid()) {
            editTextTitle.setError(Validation.getErrorMessage());
            return;
        }

        if (postUniqueId.equals("0") && (postPictureString!=null && postPictureString.equals(""))) {
            new DialogMessage(ActivityPostAddEdit.this, R.string.product_info, getString(R.string.choose_post_picture)).show();
            return;
        }

        Post post = new Post();
        post.businessUniqueId = businessUniqueId;
        post.title = editTextTitle.getText().toString();
        post.picture = postPictureString;
        post.description = editTextDescription.getText().toString();
        post.price = editTextPrice.getText().toString();
        post.code = editTextCode.getText().toString();
        post.hashtagList = TextProcessor.getHashtags(editTextHashtags.getText().toString());

//        progressDialog.show();
        showWaitDialog();
        if (!postUniqueId.equals("0")) {
            //the user is updating the post
            post.uniqueId = postUniqueId;
            ((MyApplication) getApplication()).setCurrentWebservice(WebservicesHandler.Webservices.UPDATE_POST);
            new UpdatePost(ActivityPostAddEdit.this, post, ActivityPostAddEdit.this,UPDATE_POST_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
        } else {
            //the user is edding new post

            ((MyApplication) getApplication()).setCurrentWebservice(WebservicesHandler.Webservices.ADD_POST);
            new AddPost(ActivityPostAddEdit.this, post, ActivityPostAddEdit.this,
                    ADD_POST_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
        }

        isSubmitClicked = true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        else if (item.getItemId()==R.id.action_tik){
            submit();
            return true;
        }else
            return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        boolean isPostDirty=false;
        if (originalPost!=null){
            if (!originalPost.title.equals(editTextTitle.getText().toString()))
                isPostDirty=true;
            if (!postPictureString.equals("")&& !isPostDirty)
                isPostDirty=true;
            if (!originalPost.description.equals(editTextDescription.getText().toString()) && !isPostDirty)
                isPostDirty=true;
            if (!originalPost.price.equals(editTextPrice.getText().toString()) && !isPostDirty)
                isPostDirty=true;
            if (!originalPost.code.equals(editTextCode.getText().toString())&& !isPostDirty)
                isPostDirty=true;
            if(!originalPost.hashtagList.equals(TextProcessor.getHashtags(editTextHashtags.getText().toString())) && !isPostDirty)
                isPostDirty=true;
        }
        else{
            if (!editTextTitle.getText().toString().equals(""))
                isPostDirty=true;
            if (!postPictureString.equals("") && !isPostDirty)
                isPostDirty=true;
            if (!editTextDescription.getText().toString().equals("") && !isPostDirty)
                isPostDirty=true;
            if (!editTextPrice.getText().toString().equals("") && !isPostDirty)
                isPostDirty=true;
            if (!editTextCode.getText().toString().equals("") && !isPostDirty)
                isPostDirty=true;
            if(!TextProcessor.getHashtags(editTextHashtags.getText().toString()).isEmpty() && !isPostDirty)
                isPostDirty=true;
        }
        if (isPostDirty){
            new DialogSaveResult(ActivityPostAddEdit.this).show();
        }
        else{
            finish();
        }
    }


    @Override
    public void getResult(int request, Object result) {
//        progressDialog.dismiss();
        hideWaitDialog();
        NetworkConnectivityReciever.removeIfHaveListener(TAG);
        networkFailedLayout.setVisibility(View.GONE);

        if (request==GET_POST_INT_CODE){
            Post post = (Post) result;
            originalPost=(Post) result;

            //download and display the post picture


            editTextDescription.setText(post.description);
            editTextTitle.setText(post.title);
            if (post.hashtagList.size() != 0) {
                String hashtags = "";
                for (String hashtag : post.hashtagList) {
                    if (!hashtag.equals(""))
                        hashtags += "#" + hashtag;
                }
                editTextHashtags.setText(hashtags + " ");

            }

            editTextPrice.setText(post.price);
            editTextCode.setText(post.code);
            editTextTitle.setText(post.title);
            SimpleLoader simpleLoader = new SimpleLoader(ActivityPostAddEdit.this);
            simpleLoader.loadImage(post.pictureUniqueId, Image_M.MEDIUM, Image_M.ImageType.POST, imageViewPostPicture);
            (findViewById(R.id.dataContainer)).setVisibility(View.VISIBLE);
        }
        else if (request==ADD_POST_INT_CODE){
            //it is AddPost or UpdatePost. The postAdapter which calls this activity needs to be updated
            ((MyApplication) getApplication()).post = new Post();
            ((MyApplication) getApplication()).post = (Post) result;
//            Intent i = getIntent();
//            setResult(RESULT_OK, i);
            MyApplication.broadCastAddPostOnBusiness(TAG,(Post)result);
            finish();
        }else if(request==UPDATE_POST_INT_CODE){
            //it is AddPost or UpdatePost. The postAdapter which calls this activity needs to be updated
            ((MyApplication) getApplication()).post = new Post();
            ((MyApplication) getApplication()).post = (Post) result;
//            Intent i = getIntent();
//            setResult(RESULT_OK, i);
            MyApplication.broadCastUpdateDirtyPost(TAG,((Post) result).uniqueId);
            finish();
        }


    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
//        progressDialog.dismiss();
        hideWaitDialog();
        if ((request==GET_POST_INT_CODE)&&(errorCode==ServerAnswer.NETWORK_CONNECTION_ERROR)&& !isSubmitClicked) {
            networkFailedLayout.setVisibility(View.VISIBLE);
            NetworkConnectivityReciever.setNetworkStateListener(TAG, ActivityPostAddEdit.this);
        }else if((request==GET_POST_INT_CODE) && isSubmitClicked){
            new DialogMessage(ActivityPostAddEdit.this, ServerAnswer.getError(ActivityPostAddEdit.this, errorCode, callerStringID + ">" + this.getLocalClassName())).show();
        } else if(request == ADD_POST_INT_CODE){
            new DialogMessage(ActivityPostAddEdit.this, ServerAnswer.getError(ActivityPostAddEdit.this, errorCode, callerStringID + ">" + this.getLocalClassName())).show();
        }

    }

    @Override
    public void doOnNetworkConnected() {
//        progressDialog.show();
        hideWaitDialog();
        new GetPost(ActivityPostAddEdit.this,LoginInfo.getInstance().getUserUniqueId(), businessUniqueId, postUniqueId, Post.GetPostType.BUSINESS, ActivityPostAddEdit.this,GET_POST_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }

    @Override
    public void doTryAgain() {
//        progressDialog.show();
        showWaitDialog();
        new GetPost(ActivityPostAddEdit.this,LoginInfo.getInstance().getUserUniqueId(), businessUniqueId, postUniqueId, Post.GetPostType.BUSINESS, ActivityPostAddEdit.this,GET_POST_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
