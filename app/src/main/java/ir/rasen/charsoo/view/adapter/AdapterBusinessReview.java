package ir.rasen.charsoo.view.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import java.util.ArrayList;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.Review;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.view.widgets.RatingBar;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.imageviews.ImageViewCircle;

/**
 * Created by android on 3/7/2015.
 */
public class AdapterBusinessReview extends BaseAdapter  {

    private ArrayList<Review> reviews;
    private Context context;
    SimpleLoader simpleLoader;
    ListView listView;


    //this adapter will displays users who wrote reviews on a business
    public AdapterBusinessReview(Context context, ArrayList<Review> reviews) {
        this.context = context;
        this.reviews = reviews;
        simpleLoader = new SimpleLoader(context);
    }
    public void loadMore(ArrayList<Review> newItem){
        this.reviews.addAll(newItem);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return reviews.size();
    }

    @Override
    public Object getItem(int position) {
        return reviews.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final Holder holder;

        //review is watched by business shouldn't be able to select
        if(listView == null) {
            listView = (ListView) viewGroup;
            listView.setSelector(new ColorDrawable(0x00ffffff));
        }

        if (view == null) {
            holder = new Holder();
            view = LayoutInflater.from(context).inflate(R.layout.item_review_business, viewGroup, false);
            holder.imageViewImage = (ImageViewCircle) view.findViewById(R.id.review_user_image);
            holder.textViewIdentifier = (TextViewFont) view.findViewById(R.id.review_user);
            holder.linearLayout = (LinearLayout) view.findViewById(R.id.linear);
            holder.textViewText = (TextViewFont) view.findViewById(R.id.review_text);
            holder.ratingBar = (RatingBar) view.findViewById(R.id.review_rate);
            view.setTag(holder);
        } else
            holder = (Holder) view.getTag();

        //download business profile picture with customized class via imageId
        simpleLoader.loadImage(reviews.get(position).userPicutreUniqueId, Image_M.SMALL, Image_M.ImageType.USER, holder.imageViewImage, reviews.get(position).userName);
        holder.textViewIdentifier.setText(reviews.get(position).userName);
        holder.textViewText.setText(reviews.get(position).text);
        holder.ratingBar.setScore(reviews.get(position).rate);

        holder.textViewIdentifier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User.goUserHomeInfoPage(context,reviews.get(position).userUniqueId);
            }
        });

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User.goUserHomeInfoPage(context,reviews.get(position).userUniqueId);
            }
        });


        holder.imageViewImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User.goUserHomeInfoPage(context,reviews.get(position).userUniqueId);
            }
        });
        return view;
    }
    //Each item in this adapter has a picture and a title

    private class Holder {
        ImageViewCircle imageViewImage;
        TextViewFont textViewIdentifier;
        TextViewFont textViewText;
        RatingBar ratingBar;
        LinearLayout linearLayout;
    }
}
