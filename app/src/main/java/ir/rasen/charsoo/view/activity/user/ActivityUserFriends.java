package ir.rasen.charsoo.view.activity.user;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.BaseAdapterItem;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.PullToRefreshList;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.friend.GetUserFriendRequests;
import ir.rasen.charsoo.model.friend.GetUserFriends;
import ir.rasen.charsoo.view.adapter.AdapterUsersFromBAItems;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IPullToRefresh;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.PullToRefreshListView;


public class ActivityUserFriends
        extends CharsooActivity
        implements IWebservice, IPullToRefresh ,TryAgainListener,NetworkStateChangeListener{

    public static final int /*GET_USER_HOME_INFO_INT_CODE=11,*/
            GET_USER_FRIENDS_INT_CODE=14,GET_USER_FRIEND_REQUESTS_LIST_REQUEST=22;

    public static final String TAG = "ActivityUserFriends";

    String visitedUserId;
    AdapterUsersFromBAItems adapterFriends;
    ListView listView;
    ArrayList<BaseAdapterItem> friends;
    //    ArrayList<BaseAdapterItem> sampleFriends;
    DialogMessage dialogMessage;


    private enum Status {FIRST_TIME, LOADING_MORE, REFRESHING, NONE}

    private Status status;

    //pull_to_refresh_lib
    PullToRefreshList pullToRefreshListView;


    TextViewFont tvNoFriendExists;

    RelativeLayout networkFailLayout;


    boolean isOwnerUser;


    MyApplication mMyApplication;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.activity_user_friends);
        setTitle(getString(R.string.friends));
        dialogMessage=new DialogMessage(ActivityUserFriends.this, "");
//        boolean hasRequest = false;
//        try {
//            sampleFriends = TestUnit.getBaseAdapterItems(getResources());
//            hasRequest = getIntent().getBooleanExtra(Params.HAS_REQUEST, false);
//        } catch (Exception e) {
//
//        }

        mMyApplication = ((MyApplication)getApplication());

        visitedUserId = getIntent().getExtras().getString(Params.VISITED_USER_UNIQUE_ID);
        friends = new ArrayList<>();
        status = Status.FIRST_TIME;
        (findViewById(R.id.btn_friend_requests)).setVisibility(View.GONE);
//        if (visitedUserId == LoginInfo.getUserId(this))
//        {
        //help: checkout getResult Method of this
//            new GetUserHomeInfo(
//                    this,visitedUserId,visitedUserId,
//                    ActivityUserFriends.this,GET_USER_HOME_INFO_INT_CODE).executeWithNewSolution();;
//        }

        new GetUserFriends(ActivityUserFriends.this, visitedUserId, ActivityUserFriends.this,
                GET_USER_FRIENDS_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

        pullToRefreshListView = new PullToRefreshList(this, (PullToRefreshListView) findViewById(R.id.pull_refresh_list), ActivityUserFriends.this);
        listView = pullToRefreshListView.getListView();
        adapterFriends = new AdapterUsersFromBAItems(ActivityUserFriends.this, visitedUserId, friends, AdapterUsersFromBAItems.Mode.USERS);
        listView.setAdapter(adapterFriends);
        showWaitDialog();


        (findViewById(R.id.btn_friend_requests)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(ActivityUserFriends.this, ActivityUserFriendRequests.class);
                intent1.putExtra(Params.VISITED_USER_UNIQUE_ID, visitedUserId);
                startActivityForResult(intent1, Params.ACTION_NEW_FRIENDS);
            }
        });

        String visitedId = getIntent().getExtras().getString(Params.VISITED_USER_UNIQUE_ID);


        tvNoFriendExists = (TextViewFont) findViewById(R.id.tv_no_data_exist);
        isOwnerUser = (visitedId.equals(LoginInfo.getInstance().getUserUniqueId()))?true:false;

        networkFailLayout =
                MyApplication.initNetworkErrorLayout(
                        getWindow().getDecorView(),this,this);



    }


    // LOAD MORE DATA
    public void loadMoreData() {
        // LOAD MORE DATA HERE...

        //this webservice doesn't support load more yet.
        /*status = Status.LOADING_MORE;
        pullToRefreshListView.setFooterVisibility(View.VISIBLE);
        new GetUserFriends(ActivityFriends.this, visitedUserId, ActivityFriends.this).executeWithNewSolution();;*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        /*MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_next_button, menu);*/
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    @Override
    public void getResult(int request, Object result) {
        hideWaitDialog();
        networkFailLayout.setVisibility(View.GONE);
        NetworkConnectivityReciever.removeIfHaveListener(TAG);
        switch (request){
            case GET_USER_FRIENDS_INT_CODE:
                ArrayList<BaseAdapterItem> temp = (ArrayList<BaseAdapterItem>) result;
                friends.addAll(temp);
                pullToRefreshListView.setResultSize(friends.size());


                if (status == Status.FIRST_TIME) {
                    adapterFriends.resetItems(friends);
                } else if (status == Status.REFRESHING) {
                    friends.clear();
                    friends.addAll(temp);
                    adapterFriends.resetItems(friends);
                } else {
                    pullToRefreshListView.setFooterVisibility(View.GONE);
                    adapterFriends.loadMore(temp);
                }
                status = Status.NONE;
                if(isOwnerUser) {
                    new GetUserFriendRequests(ActivityUserFriends.this,
                            visitedUserId, ActivityUserFriends.this, GET_USER_FRIEND_REQUESTS_LIST_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                }
                break;

            case GET_USER_FRIEND_REQUESTS_LIST_REQUEST:
                ArrayList<BaseAdapterItem> friendRequestItems = (ArrayList<BaseAdapterItem>) result;

                handleShowFriendRequestsButton();
                pullToRefreshListView.onRefreshComplete();
                hideWaitDialog();
                break;
        }

        handleTvNoFriendExist();
        handleShowFriendRequestsButton();
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
        hideWaitDialog();
        pullToRefreshListView.onRefreshComplete();

        if(errorCode == ServerAnswer.NETWORK_CONNECTION_ERROR)
            NetworkConnectivityReciever.setNetworkStateListener(TAG,ActivityUserFriends.this);

        if(ServerAnswer.canShowErrorLayout(errorCode) && status!=Status.REFRESHING) {
            networkFailLayout.setVisibility(View.VISIBLE);
        }
        else if(errorCode == ServerAnswer.USER_FRIEND_REQUESTS_DOES_NOT_EXITSTS){
            adapterFriends.resetItems(new ArrayList<BaseAdapterItem>());
        }
        else if(networkFailLayout.getVisibility()!=View.VISIBLE){
            dialogMessage.show();
            dialogMessage.setMessage(
                    ServerAnswer.getError(
                            ActivityUserFriends.this, errorCode,
                            callerStringID+">"+this.getLocalClassName()));

        }

        handleTvNoFriendExist();
        handleShowFriendRequestsButton();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Params.ACTION_NEW_FRIENDS) {
                if(data.getExtras().getBoolean(Params.NEW_FIREND)){
                    friends.addAll(0,mMyApplication.newFriends);
                    adapterFriends.notifyDataSetChanged();
                }
                if(data.getExtras().getBoolean(Params.HAS_REMAINIG_FRIEND_REQUESTS)){
                    // hasRequest=false;
                    (findViewById(R.id.btn_friend_requests)).setVisibility(View.VISIBLE);
                }
                else
                {
                    //  hasRequest=true;
                    (findViewById(R.id.btn_friend_requests)).setVisibility(View.GONE);
                }
            }
        }

        handleTvNoFriendExist();
    }

    void handleTvNoFriendExist(){
        if(friends.size() <= 0 && isOwnerUser) {
            tvNoFriendExists.setVisibility(View.VISIBLE);
            tvNoFriendExists.setText(getString(R.string.txt_not_exists_user_friends));
        }
        else
            tvNoFriendExists.setVisibility(View.GONE);
    }


    @Override
    public void doTryAgain() {
        tryAgain();
    }


    @Override
    public void notifyRefresh() {
        status = Status.REFRESHING;
        new GetUserFriends(ActivityUserFriends.this, visitedUserId, ActivityUserFriends.this,
                GET_USER_FRIENDS_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
//        if (visitedUserId == LoginInfo.getUserId(this)){
//            new GetUserHomeInfo(this,visitedUserId,visitedUserId,ActivityUserFriends.this,GET_USER_HOME_INFO_INT_CODE).executeWithNewSolution();;
//        }
    }

    @Override
    public void notifyLoadMore() {
        loadMoreData();
    }

    @Override
    public void doOnNetworkConnected() throws Exception {
        tryAgain();
    }

    void tryAgain(){
        if(isShowingWaitDialog())
            return;

        showWaitDialog();
        status = Status.REFRESHING;
        new GetUserFriends(ActivityUserFriends.this, visitedUserId, ActivityUserFriends.this,
                GET_USER_FRIENDS_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
//        if (visitedUserId == LoginInfo.getUserId(this)){
//            new GetUserHomeInfo(this,visitedUserId,visitedUserId,ActivityUserFriends.this,GET_USER_HOME_INFO_INT_CODE).executeWithNewSolution();;
//        }
    }

    public void handleShowFriendRequestsButton(){
        if(isOwnerUser &&
                LoginInfo.isHaveFriendRequest(LoginInfo.getInstance().userReceivedFriendRequestCount))
            (findViewById(R.id.btn_friend_requests)).setVisibility(View.VISIBLE);
        else
            (findViewById(R.id.btn_friend_requests)).setVisibility(View.GONE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
