package ir.rasen.charsoo.view.dialog;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.model.post.Report;

/**
 * Created by Rasen_iman on 8/8/2015.
 */
public class PopupBusinessInviteFriends extends PopupWindow {
    private Context context;
    public PopupBusinessInviteFriends(
            Context context,View.OnClickListener inviteClickListener,
            View.OnClickListener blockedUsersClickListener){

        this.context = context;

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_business_invite_friends_layout, null, false);
        setContentView(view);

        view.findViewById(R.id.btn_popup_business_invite_friends).setOnClickListener(inviteClickListener);
        view.findViewById(R.id.btn_popup_business_blocked_users).setOnClickListener(blockedUsersClickListener);

        setBackgroundDrawable(new BitmapDrawable());
        setOutsideTouchable(true);
        setWindowLayoutMode(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
    }

}
