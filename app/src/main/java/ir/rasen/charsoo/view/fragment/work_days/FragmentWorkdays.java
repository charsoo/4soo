package ir.rasen.charsoo.view.fragment.work_days;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupMenu;

import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.WorkDays;
import ir.rasen.charsoo.view.activity.business.ActivityWorkdays;
import ir.rasen.charsoo.view.adapter.AdapterWorkDays;
import ir.rasen.charsoo.view.widgets.MyPopUpMenu;
import ir.rasen.charsoo.view.widgets.buttons.FloatButton;

/**
 * Created by Rasen_iman on 9/3/2015.
 */
public class FragmentWorkdays
        extends Fragment
        implements View.OnClickListener{
    ListView mListView;
    FloatButton addWorkDay;
    ActivityWorkdays mActivityWorkdays;
    AdapterWorkDays mAdapter;

    MyPopUpMenu mPopUp;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_workdays,
                        null,false);

        mActivityWorkdays = (ActivityWorkdays) getActivity();

        mListView = (ListView) v.findViewById(R.id.lv_workdays);
        addWorkDay = (FloatButton) v.findViewById(R.id.btn_add_workday);
        addWorkDay.setOnClickListener(this);

        handleShowingAddBtn();

        mAdapter = new AdapterWorkDays(mActivityWorkdays, WorkDays.convertToWorkDaysGroup(mActivityWorkdays.mWorkDays));

        mListView.setAdapter(mAdapter);
        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                mPopUp = new MyPopUpMenu(getActivity(),view,R.menu.popup_menu_edit_delete);
                mPopUp.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch(item.getItemId()){

                            case R.id.popup_delete :
                                deleteWorkDay(position);
                                break;
                        }
                        return false;
                    }
                });
                mPopUp.show();
                return true;
            }
        });

        return v;
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_add_workday :
                mActivityWorkdays.openFragment(ActivityWorkdays.Frag.FRAGMENT_ADD_WORKDAY);
                break;
        }
    }


    void handleShowingAddBtn(){
        if(mActivityWorkdays.mWorkDays.isAllDaysSetted()){
            addWorkDay.setVisibility(View.GONE);
        }else{
            addWorkDay.setVisibility(View.VISIBLE);
        }
    }

    void deleteWorkDay(int position){
        ArrayList<WorkDays.WorkDaysGroup> wdg = mAdapter.getAllItems();
        for(int i=0;i<wdg.get(position).mDayTimes.size();i++) {
            for(int j=0;j<mActivityWorkdays.mWorkDays.getDayTimes().size();j++)
                if(wdg.get(position).mDayTimes.get(i).mCurrentDay ==
                        mActivityWorkdays.mWorkDays.getDayTimes().get(j).mCurrentDay
                        )
                    mActivityWorkdays.mWorkDays.getDayTimes().get(j).resetTimes();
        }
        mAdapter.removeItem(position);
    }
}
