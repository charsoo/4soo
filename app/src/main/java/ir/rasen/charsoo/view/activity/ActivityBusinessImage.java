package ir.rasen.charsoo.view.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.PopupMenu;


import java.io.File;
import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.PullToRefreshGrid;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.model.business.DeleteImageFromGallery;
import ir.rasen.charsoo.model.business.GetGalleryImage;
import ir.rasen.charsoo.model.business.Report;
import ir.rasen.charsoo.model.business.SendImage;
import ir.rasen.charsoo.view.activity.business.ActivityBusiness;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessGallery;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessReviews;
import ir.rasen.charsoo.view.adapter.AdapterBusinessImage;
import ir.rasen.charsoo.view.adapter.AdapterBusinessReview;
import ir.rasen.charsoo.view.adapter.AdapterSpecialBusinesses;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.dialog.PopupSelectCameraGallery;
import ir.rasen.charsoo.view.interface_m.IPullToRefresh;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.OnImageDeletedListener;
import ir.rasen.charsoo.view.widgets.MyPopUpMenu;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.HFGridView;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.PullToRefreshBase;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.PullToRefreshGridView;


public class ActivityBusinessImage extends CharsooActivity implements IWebservice ,OnImageDeletedListener{

    private String businessId;
    String filePath, userPictureString;
    int req_sendimage = 100, req_getimagefromgallery = 200 , req_deleteimagefromgallery = 300 , req_report = 400;
    ArrayList<Business> results;
    DialogMessage dialogMessage;
    GridView gridView;
    private PullToRefreshGridView mPullRefreshGridView;



    private enum Status {FIRST_TIME, LOADING_MORE, REFRESHING, NONE}
    private Status status;
    private AdapterBusinessImage adapterBusinessImage;



    public static OnImageDeletedListener onImageDeletedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activitybusinessimage);

        mPullRefreshGridView = (PullToRefreshGridView) findViewById(R.id.pull_refresh_grid);
        gridView = mPullRefreshGridView.getRefreshableView();
        gridView.setNumColumns(3);

        onImageDeletedListener = this;


        // Set a listener to be invoked when the list should be refreshed.
        mPullRefreshGridView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<GridView>() {

            @Override
            public void onPullDownToRefresh(PullToRefreshBase<GridView> refreshView) {
                status = Status.REFRESHING;
                new GetGalleryImage(ActivityBusinessImage.this, 0, 20, businessId, ActivityBusinessImage.this,
                        req_getimagefromgallery, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
            }
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<GridView> refreshView) {
                status = Status.LOADING_MORE;
                new GetGalleryImage(ActivityBusinessImage.this,adapterBusinessImage.getCount() , 20, businessId, ActivityBusinessImage.this,
                        req_getimagefromgallery, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
            }

        });


        results = new ArrayList<>();



        businessId = getIntent().getExtras().getString(Params.BUSINESS_ID_STRING,"");

        setTitle(Globals.getInstance().getValue().businessVisiting.name);

        status = Status.FIRST_TIME;

        new GetGalleryImage(ActivityBusinessImage.this, 0, 20, businessId, ActivityBusinessImage.this,
                req_getimagefromgallery, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

        dialogMessage = new DialogMessage(this, "");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_business_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.imagegallery) {
            new PopupSelectCameraGallery(ActivityBusinessImage.this).show();
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == ActivityCamera.CAPTURE_PHOTO) {
                filePath = data.getStringExtra(ActivityCamera.FILE_PATH);
                displayCropedImage(filePath);
            } else if (requestCode == ActivityGallery.CAPTURE_GALLERY) {
                filePath = data.getStringExtra(ActivityGallery.FILE_PATH);
                displayCropedImage(filePath);
            }
        }

    }

    private void displayCropedImage(String filePath) {
        File file = new File(filePath);
        if (file.exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            //crop middle part of picture
            int width = getResources().getDisplayMetrics().widthPixels;
            //crop middle part of picture
            int bitmapWidth = myBitmap.getWidth();
            if (width > bitmapWidth)
                width = bitmapWidth;
            try {
                Bitmap croppedBitmap = Bitmap.createBitmap(myBitmap, 0, width / 6, width, (width / 3) * 2);
                userPictureString = Image_M.getBase64String(filePath);
                new SendImage(ActivityBusinessImage.this, businessId, req_sendimage, userPictureString,
                        ActivityBusinessImage.this, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

            } catch (Exception e) {
                String s = e.getMessage();
            }
        }
    }

    @Override
    public void getResult(int request, Object result) throws Exception {
        if (request == req_sendimage) {
            new DialogMessage(ActivityBusinessImage.this, R.string.profile_edit,
                    getString(R.string.dialog_success)).show();
            status = Status.REFRESHING;
            new GetGalleryImage(ActivityBusinessImage.this, 0, 20, businessId, ActivityBusinessImage.this,
                    req_getimagefromgallery, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
        }
        if (request == req_getimagefromgallery) {
            if (result instanceof ArrayList) {
                ArrayList<Business> temp = (ArrayList<Business>) result;
                results.addAll(temp);
                if (status == Status.FIRST_TIME) {
                    adapterBusinessImage = new AdapterBusinessImage(ActivityBusinessImage.this, results);
                    gridView.setAdapter(adapterBusinessImage);
                }
                else if (status == Status.REFRESHING) {
                    results.clear();
                    results.addAll(temp);
                    gridView.setAdapter(new AdapterBusinessImage(ActivityBusinessImage.this, results));
                    mPullRefreshGridView.onRefreshComplete();
                }
                else {
                    //it is loading more
                    mPullRefreshGridView.onRefreshComplete();
                    adapterBusinessImage.loadMore(temp);
                }
                status = Status.NONE;
            }
        }
        if(request == req_deleteimagefromgallery){
            adapterBusinessImage.notifyDataSetChanged();

        }
    }

    public void notifyrefresh() {
        status = Status.REFRESHING;
        new GetGalleryImage(ActivityBusinessImage.this, 0, 20, businessId, ActivityBusinessImage.this,
                req_getimagefromgallery, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
    }

    @Override
    protected void  onRestart(){
        super.onRestart();
        if(ActivityBusinessGallery.update) {
            new GetGalleryImage(ActivityBusinessImage.this, 0, 20, businessId, ActivityBusinessImage.this,
                    req_getimagefromgallery, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
            ActivityBusinessGallery.update = false;
        }

    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) throws Exception {
        mPullRefreshGridView.onRefreshComplete();
        if (!dialogMessage.isShowing()) {
            dialogMessage.show();
            dialogMessage.setMessage(ServerAnswer.getError(
                    ActivityBusinessImage.this,
                    errorCode, callerStringID + ">" + this.getLocalClassName()));
        }

    }

    @Override
    public void onImageDeleted() {
        notifyrefresh();
    }

}