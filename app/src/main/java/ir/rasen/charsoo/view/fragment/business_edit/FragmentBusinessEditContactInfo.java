package ir.rasen.charsoo.view.fragment.business_edit;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.Validation;
import ir.rasen.charsoo.controller.helper.WorkDays;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessEdit;
import ir.rasen.charsoo.view.activity.business.ActivityWorkdays;
import ir.rasen.charsoo.view.widgets.edittexts.EditTextFont;
import ir.rasen.charsoo.view.widgets.imageviews.ExpandableImageView;

public class FragmentBusinessEditContactInfo extends Fragment {


    EditTextFont editTextPhone, editTextMobile, editTextWebsite, editTextEmail, buttonWorkTime;
    WorkDays workDays;
    private boolean isEditing;
    private Business editingBusiness;
    private Boolean isViewCreated = false;
    private ExpandableImageView imageViewPicture;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CustomActivityOnCrash.install(getActivity());

        View view = inflater.inflate(R.layout.fragment_register_business_contact_info,
                container, false);



        isViewCreated = true;
        editTextEmail = (EditTextFont) view.findViewById(R.id.edt_email);
        editTextMobile = (EditTextFont) view.findViewById(R.id.edt_mobile);
        editTextWebsite = (EditTextFont) view.findViewById(R.id.edt_wesite);
        editTextPhone = (EditTextFont) view.findViewById(R.id.edt_phone);
        buttonWorkTime = (EditTextFont) view.findViewById(R.id.btn_work_time);
        imageViewPicture = (ExpandableImageView) view.findViewById(R.id.imageView_cover);

        buttonWorkTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ActivityWorkdays.class);
                if (isEditing) {
                    intent.putExtra(Params.IS_EDITTING, true);

                } else {
                    if (workDays != null) {
                        Globals.getInstance().getValue().workDays = workDays;
                        intent.putExtra(Params.IS_EDITTING, true);
                    } else
                        intent.putExtra(Params.IS_EDITTING, false);
                }
                startActivityForResult(intent, Params.ACTION_WORK_TIME);

            }
        });

        //if the user is editing the business
        isEditing = getArguments().getBoolean(Params.IS_EDITTING);
        if (isEditing) {
            editingBusiness = Globals.getInstance().getValue().businessEditing;
            if(ActivityBusinessEdit.myBitmap!= null)
                imageViewPicture.setImageBitmap(ActivityBusinessEdit.myBitmap);
            else {
                SimpleLoader simpleLoader = new SimpleLoader(getActivity());
                simpleLoader.loadImage(editingBusiness.profilePictureUniqueId, Image_M.LARGE, Image_M.ImageType.BUSINESS, imageViewPicture);
            }
            editTextMobile.setText(editingBusiness.mobile);
            editTextWebsite.setText(editingBusiness.webSite);
            editTextEmail.setText(editingBusiness.email);
            editTextPhone.setText(editingBusiness.phone);
            editTextPhone.requestFocus();

//           buttonWorkTime.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_check_green), null);
            Globals.getInstance().getValue().workDays = editingBusiness.workDays;
        }
        imageViewPicture.post(new Runnable() {
            @Override
            public void run() {
                imageViewPicture.requestFocus();
            }
        });
        return view;
    }

    public boolean isVerified() {
        if (!isViewCreated)
            return true;
            if (editTextPhone!=null && !editTextPhone.getText().toString().equals("") &&
                    !Validation.validTextSize(getActivity(), editTextPhone.getText().toString()).isValid()) {
                editTextPhone.setError(Validation.getErrorMessage());
                return false;
            }
            if (editTextMobile!=null &&!editTextMobile.getText().toString().equals("") &&
                    !Validation.validateMobile(getActivity(), editTextMobile.getText().toString()).isValid()) {
                editTextMobile.setError(Validation.getErrorMessage());
                return false;
            }
            if (editTextWebsite!=null &&!editTextWebsite.getText().toString().equals("") &&
                    !Validation.validateWebsite(getActivity(), editTextWebsite.getText().toString()).isValid()) {
                editTextWebsite.setError(Validation.getErrorMessage());
                return false;
            }
            if (editTextEmail!=null && !editTextEmail.getText().toString().equals("") &&
                    !Validation.validateEmail(getActivity(), editTextEmail.getText().toString()).isValid()) {
                editTextEmail.setError(Validation.getErrorMessage());
                return false;
            }

//
//        if (!isEditing && workDays == null) {
//            new DialogMessage(getActivity(), getString(R.string.work_time_do)).show();
//            return false;
//        }

            Business business = Globals.getInstance().getValue().businessEditing;

            business.mobile = editTextMobile.getText().toString();
            business.webSite = editTextWebsite.getText().toString();
            business.email = editTextEmail.getText().toString();
            business.phone = editTextPhone.getText().toString();
            if (workDays != null)
                business.workDays = workDays;

            return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Params.ACTION_WORK_TIME && resultCode == Activity.RESULT_OK) {
            workDays = Globals.getInstance().getValue().workDays;
        //    buttonWorkTime.setBackgroundResource(R.drawable.selector_button_register);
//           buttonWorkTime.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_check_green), null);
        }
    }
    public boolean isBusinessDirty(Business originalBusiness){
        boolean isDirty=false;

        if(!isDirty && editTextPhone!=null && !originalBusiness.phone.equals(editTextPhone.getText().toString()))
            isDirty=true;
        if(!isDirty && editTextMobile!=null && !originalBusiness.mobile.equals(editTextMobile.getText().toString()))
            isDirty=true;
        if (!isDirty && editTextWebsite!=null  && !originalBusiness.webSite.equals(editTextWebsite.getText().toString()))
            isDirty = true;
        if (!isDirty && editTextEmail!=null && !originalBusiness.email.equals(editTextEmail.getText().toString()))
            isDirty = true;
        if (!isDirty && WorkDays.areWorktimesDifferent(originalBusiness.workDays,Globals.getInstance().getValue().businessEditing.workDays))
            isDirty=true;
        return isDirty;
    }
}
