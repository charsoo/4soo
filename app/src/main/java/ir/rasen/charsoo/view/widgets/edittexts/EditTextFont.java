package ir.rasen.charsoo.view.widgets.edittexts;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.rengwuxian.materialedittext.MaterialEditText;

import ir.rasen.charsoo.R;

public class EditTextFont extends MaterialEditText {

    private Drawable drawableRight;
    private Drawable drawableLeft;
    private Drawable drawableTop;
    private Drawable drawableBottom;

    int actionX, actionY;

    boolean errorSet=false;

    public EditTextFont(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public EditTextFont(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public EditTextFont(Context context) {
        super(context);

        init();
    }



    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/b_yekan.ttf");
            setTypeface(tf);
        }
        setPrimaryColor(getResources().getColor(R.color.primaryColor));
        (new Handler()).post(new Runnable() {
            @Override
            public void run() {
                int textlenght = getText().length();
                setSelection(textlenght);
            }
        });
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (getError() != null)
            setError(null);

        return super.onTouchEvent(event);
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        super.setOnClickListener(l);
        int textlenght =getText().length();
        setSelection(textlenght);
    }


    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        if (getError() != null)
            setError(null);
  super.onTextChanged(text, start, lengthBefore, lengthAfter);
    }

    @Override
    public void setError(CharSequence error) {
        super.setError(error);
        if(errorSet) {
            errorSet=false;
            return;
        }
        errorSet=true;
        if(error!=null)
            YoYo.with(Techniques.Shake)
                .duration(700)
                .playOn(this);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD) {
            ForegroundColorSpan fgcspan = new ForegroundColorSpan(getResources().getColor(android.R.color.black));
            SpannableStringBuilder ssbuilder = new SpannableStringBuilder(error);
            ssbuilder.setSpan(fgcspan, 0, error.length(), 0);
            setError(ssbuilder);
        } else {
            setError(error);
        }
        requestFocus();
    }

    public void shake() {
        YoYo.with(Techniques.Shake)
                .duration(700)
                .playOn(this);
    }

}