package ir.rasen.charsoo.view.interface_m;

/**
 * Created by android on 3/15/2015.
 */
public interface IDeletePost {
    void notifyDeletePost(String postId) throws Exception;
    void notifyDeleteFailed(String postId,String failureMessage) throws Exception;
}
