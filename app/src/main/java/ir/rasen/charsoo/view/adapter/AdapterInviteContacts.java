package ir.rasen.charsoo.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Hashtable;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.ContactEntry;
import ir.rasen.charsoo.view.interface_m.IFragInviteSelectionListener;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.checkbox.CheckBox;
import ir.rasen.charsoo.view.widgets.imageviews.ImageViewCircle;


public class AdapterInviteContacts extends BaseAdapter {

    private ArrayList<ContactEntry> items;
    private Context context;
    SimpleLoader simpleLoader;
    private Hashtable<Integer, Boolean> isItemChecked;
    IFragInviteSelectionListener delegate;
    int prevProsition = 0;
    ViewGroup viewGroup;


    public AdapterInviteContacts(Context context, ArrayList<ContactEntry> items, IFragInviteSelectionListener delegate) {
        this.delegate = delegate;
        this.context = context;
        this.items = items;
        simpleLoader = new SimpleLoader(context);
        isItemChecked = new Hashtable<>();


    }

    public void loadMore(ArrayList<ContactEntry> newItem) {
        this.items.addAll(newItem);
        notifyDataSetChanged();
    }

    public void resetItems(ArrayList<ContactEntry> newItem) {
        this.items = new ArrayList<>(newItem);
        notifyDataSetChanged();
    }

    public void setUncheckedViewAt(final int position) {
        try {
            isItemChecked.put(position,false);
            int firstVisibleItemPosition=((ListView) viewGroup).getFirstVisiblePosition();
            int lastVisibleItemPosition=((ListView) viewGroup).getLastVisiblePosition();
            if (position>=firstVisibleItemPosition && position<=lastVisibleItemPosition) {
                isItemChecked.put(position, false);
                View v = viewGroup.getChildAt(position-firstVisibleItemPosition);
                ((CheckBox) v.findViewById(R.id.checkbox1)).setChecked(isItemChecked.get(position));

            }
        } catch (Exception ed) {
        }
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }



    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {

        this.viewGroup=viewGroup;

        final Holder holder;
        View v = view;

        //boolean flag = false;
        if (v == null) {
            v = LayoutInflater.from(context).inflate(R.layout.item_contact_phone, viewGroup, false);
            holder = new Holder();
            holder.imageViewProfileImage = (ImageViewCircle) v.findViewById(R.id.contact_picture);
            holder.textViewContactName = (TextViewFont) v.findViewById(R.id.contact_fullName);
            holder.textViewContaceData = (TextViewFont) v.findViewById(R.id.contact_userName);
            holder.checkLayout = (FrameLayout) v.findViewById(R.id.contact_check);
            holder.checkBox1=(CheckBox) v.findViewById(R.id.checkbox1);
            v.setTag(holder);
            //flag = true;
        }
        else{
            holder=(Holder) v.getTag();
        }
        if (!isItemChecked.containsKey(position)) {
            isItemChecked.put(position, false);
        }

        holder.checkBox1.setCheckedWithoutAnimation(isItemChecked.get(position));
//        holder.checkBox1.post(new Runnable() {
//            @Override
//            public void run() {
//                boolean b =isItemChecked.get(position);
//                holder.checkBox1.setChecked(isItemChecked.get(position));
//            }
//        });

        holder.checkBox1.setOncheckListener(new ir.rasen.charsoo.view.widgets.material_library.views.CheckBox.OnCheckListener() {
            @Override
            public void onCheck(ir.rasen.charsoo.view.widgets.material_library.views.CheckBox view, boolean check) {
                if (isItemChecked.get(position))
                    isItemChecked.put(position, false);
                else
                    isItemChecked.put(position, true);
                delegate.onItemCheckBoxClicked(position);
            }
        });

        if (items.get(position).contactPhoto != null)
            holder.imageViewProfileImage.setImageBitmap(items.get(position).contactPhoto);

//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) { // on pre-lollipop devices
//            final CheckBox
//                    checkBox = new CheckBox(context, null);
//
//            holder.checkLayout.addView(checkBox);
//
//            checkBox.post(new Runnable() {
//                @Override
//                public void run() {
//                    boolean b =isItemChecked.get(position);
//                    checkBox.setChecked(isItemChecked.get(position));
//                }
//            });
//
//            checkBox.setOncheckListener(new ir.rasen.charsoo.view.widgets.material_library.views.CheckBox.OnCheckListener() {
//                @Override
//                public void onCheck(ir.rasen.charsoo.view.widgets.material_library.views.CheckBox view, boolean check) {
//                    if (isItemChecked.get(position))
//                        isItemChecked.put(position, false);
//                    else
//                        isItemChecked.put(position, true);
//                    delegate.onItemCheckBoxClicked(position);
//                }
//            });
//        } else { // on lollipop devices
//            final android.widget.CheckBox
//                    check = new android.widget.CheckBox(context, null);
//
//            holder.checkLayout.addView(check);
//
//            check.setChecked(isItemChecked.get(position));
//
//            check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    if (isItemChecked.get(position))
//                        isItemChecked.put(position, false);
//                    else
//                        isItemChecked.put(position, true);
//                    delegate.onItemCheckBoxClicked(position);
//                }
//            });
//
//        }

        holder.textViewContaceData.setText(items.get(position).contactData);
        holder.textViewContactName.setText(items.get(position).fullName);


        if (position > prevProsition) {
            prevProsition = position;
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_in_from_bottom);
            v.startAnimation(animation);
        }

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isItemChecked.get(position))
                    isItemChecked.put(position, false);
                else
                    isItemChecked.put(position, true);
                delegate.onItemCheckBoxClicked(position);

                holder.checkBox1.setChecked(isItemChecked.get(position));
            }
        });
        holder.textViewContaceData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isItemChecked.get(position))
                    isItemChecked.put(position, false);
                else
                    isItemChecked.put(position, true);
                delegate.onItemCheckBoxClicked(position);

                holder.checkBox1.setChecked(isItemChecked.get(position));
            }
        });
        return v;
    }


    private class Holder {

        ImageViewCircle imageViewProfileImage;
        TextViewFont textViewContactName;
        TextViewFont textViewContaceData;
        FrameLayout checkLayout;
        CheckBox checkBox1;
    }


}


