package ir.rasen.charsoo.view.interface_m;

/**
 * Created by android on 9/9/2015.
 */
public interface OnImageDeletedListener {
    void onImageDeleted();
}
