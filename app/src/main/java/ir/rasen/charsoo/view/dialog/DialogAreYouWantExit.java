package ir.rasen.charsoo.view.dialog;

import android.app.Activity;
import android.view.View;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.view.activity.ActivityPostAddEdit;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessEdit;
import ir.rasen.charsoo.view.activity.user.ActivityUserProfile;
import ir.rasen.charsoo.view.widgets.material_library.widgets.Dialog;

public class DialogAreYouWantExit extends Dialog {

    public DialogAreYouWantExit(final Activity activity) {
        super(activity,
                activity.getString(R.string.dialog_areYouWantExit_title),
                activity.getString(R.string.dialog_areYouWantExit_message));

        addCancelButton(R.string.cancel);
        setAcceptText(R.string.yes);

        setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });

        setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

}
