package ir.rasen.charsoo.view.adapter;

/**
 * Created by Sina KH on 6/23/2015.
 */

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.SuggestedBusiness;
import ir.rasen.charsoo.view.interface_m.IUnfollowBusiness;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.MaterialProgressBarCircular;
import ir.rasen.charsoo.view.widgets.RatingBar;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.buttons.ButtonFont;
import ir.rasen.charsoo.view.widgets.imageviews.RoundedSquareImageView;
import ir.rasen.charsoo.view.widgets.imageviews.SquareImageView;

public class AdapterSpecialBusinesses extends BaseAdapter {
    private Activity activity;
    ArrayList<SuggestedBusiness> items;
    //private int screedWidth;
    SimpleLoader simpleLoader;
    private int prevPosition = 0;
    private IWebservice iWebservice;
    private IUnfollowBusiness iUnfollowBusiness;

    // Constructor
    public AdapterSpecialBusinesses(Activity activity, ArrayList<SuggestedBusiness> businesses, IWebservice iWebservice, IUnfollowBusiness iUnfollowBusiness) {
        this.activity = activity;
//        items = businesses;
        resetItems(businesses);
        //screedWidth = activity.getResources().getDisplayMetrics().widthPixels;
        simpleLoader = new SimpleLoader(activity);
        this.iWebservice = iWebservice;
        this.iUnfollowBusiness = iUnfollowBusiness;
    }

    public void resetItems(ArrayList<SuggestedBusiness> newItems) {
//        items=new ArrayList<>();
        items = (ArrayList<SuggestedBusiness>) newItems.clone();
//        for (int i = 0; i < newItems.size(); i++) {
//            items.add(newItems.get(i));
//        }
        notifyDataSetChanged();
    }

    public void loadMore(ArrayList<SuggestedBusiness> newItems) {
        this.items.addAll(newItems);
        notifyDataSetChanged();
    }

    public int getCount() {
        return items.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View view, ViewGroup parent) {
        final Holder holder;
        if (view == null) {
            holder = new Holder();
            view = LayoutInflater.from(activity).inflate(R.layout.item_business_preview_simple, parent, false);
            holder.imageView = (RoundedSquareImageView) view.findViewById(R.id.business_img);
            holder.name = (TextViewFont) view.findViewById(R.id.business_name);
            holder.id = (TextViewFont) view.findViewById(R.id.business_id);
            holder.progressBar = (MaterialProgressBarCircular) view.findViewById(R.id.business_pb);
            holder.rate = (RatingBar) view.findViewById(R.id.business_rate);
            holder.follow = (ButtonFont) view.findViewById(R.id.business_follow);
            view.setTag(holder);
        } else
            holder = (Holder) view.getTag();


        final SuggestedBusiness business = items.get(position);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Business.gotoBusinessOtherPage(activity, items.get(position).businessUniqueId);
            }
        });

        if (business.businessProfilePictureUniqueId == null && business.businessPictureString != ""
                && business.businessPictureString!= null)
            holder.imageView.setImageBitmap(Image_M.getBitmapFromString(business.businessPictureString));
        else
            simpleLoader.loadImage(business.businessProfilePictureUniqueId, Image_M.MEDIUM, Image_M.ImageType.BUSINESS,
                    holder.imageView, holder.progressBar, business.businessStrId);
        holder.name.setText(
                business.businessName != null ? business.businessName : ""
        );
        holder.id.setText(
                business.businessStrId != null ? business.businessStrId : ""
        );
        //simpleLoader.loadImage(business.coverPictureUniqueId, Image_M.MEDIUM, Image_M.ImageType.POST, holder.img1);
        //simpleLoader.loadImage(business.coverPictureUniqueId, Image_M.MEDIUM, Image_M.ImageType.POST, holder.img2);
        //simpleLoader.loadImage(business.coverPictureUniqueId, Image_M.MEDIUM, Image_M.ImageType.POST, holder.img3);

        holder.rate.setScore(business.businessRate);
//            if (business.isFollowing) {
//                holder.follow.setText(activity.getString(R.string.followed_business_page));
//                holder.follow.setTextColor(activity.getResources().getColor(R.color.gray_lighte));
//            } else {
//                holder.follow.setBackgroundColor(activity.getResources().getColor(R.color.gray_lighte));
//                holder.follow.setText(activity.getString(R.string.follow));
//                holder.follow.setTextColor(activity.getResources().getColor(R.color.primaryColor));
//            }

//            holder.follow.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (business.isFollowing) {
//                        new UnFollowBusiness(activity, LoginInfo.getUserId(activity), business.uniqueId, iUnfollowBusiness).executeWithNewSolution();;
//                    } else {
//                        new FollowBusiness(activity, LoginInfo.getUserId(activity), business.uniqueId, iWebservice,business.uniqueId+1000).executeWithNewSolution();;
//                    }
//                }
//            });


        if (position > prevPosition) {
            prevPosition = position;
            Animation animation = AnimationUtils.loadAnimation(activity, R.anim.slide_in_from_bottom);
            view.startAnimation(animation);
        }

        return view;
    }

    private class Holder {
        RoundedSquareImageView imageView;
        MaterialProgressBarCircular progressBar;
        TextViewFont name, id;
        SquareImageView img1, img2, img3;
        RatingBar rate;
        ButtonFont follow;
    }

}
