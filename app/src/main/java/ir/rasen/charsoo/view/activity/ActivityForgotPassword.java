package ir.rasen.charsoo.view.activity;

import android.os.Bundle;
import android.view.View;

import com.flurry.android.FlurryAgent;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.Validation;
import ir.rasen.charsoo.controller.helper.WebservicesHandler;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.model.user.ForgetPassword;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.WaitDialog;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.edittexts.EditTextFont;

public class ActivityForgotPassword extends CharsooActivity implements IWebservice {

    private static int REQUEST_RECOVER = 1000;

    EditTextFont editTextEmail;
//    WaitDialog progressDialog;
    MyApplication myApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.activity_forgot_password);

        setTitle(getString(R.string.login));
        removeBackOnActionbar();
        //for the test I need to disable automatically going to ActivityMain

        myApplication = (MyApplication) getApplication();
        myApplication.setCurrentWebservice(WebservicesHandler.Webservices.NONE);
        editTextEmail = (EditTextFont) findViewById(R.id.edt_email);

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void back(View view) {
        onBackPressed();
    }

    public void submit(View view) {
        if (!Validation.validateEmail(ActivityForgotPassword.this, editTextEmail.getText().toString()).isValid()) {
            editTextEmail.setError(Validation.getErrorMessage());
            return;
        }
//        progressDialog.show();
        showWaitDialog();
        new ForgetPassword(ActivityForgotPassword.this, editTextEmail.getText().toString(), ActivityForgotPassword.this, REQUEST_RECOVER, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }

    @Override
    public void getResult(int request, Object result) {
        try {
            if(request==REQUEST_RECOVER) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        progressDialog.dismiss();
                        hideWaitDialog();
                        new DialogMessage(ActivityForgotPassword.this, R.string.forgot_password, getResources().getString(R.string.forgot_pass_sent), true).show();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
        try {
//            progressDialog.dismiss();
            hideWaitDialog();
            new DialogMessage(ActivityForgotPassword.this, ServerAnswer.getError(ActivityForgotPassword.this, errorCode,callerStringID+">"+this.getLocalClassName())).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this,"2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
