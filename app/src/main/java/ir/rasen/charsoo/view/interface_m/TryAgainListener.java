package ir.rasen.charsoo.view.interface_m;

/**
 * Created by hossein-pc on 7/12/2015.
 */
public interface TryAgainListener {
    void doTryAgain();
}
