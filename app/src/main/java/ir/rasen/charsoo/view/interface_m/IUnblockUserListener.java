package ir.rasen.charsoo.view.interface_m;

/**
 * Created by Rasen_iman on 8/5/2015.
 */
public interface IUnblockUserListener {
    void unblockSuccessfull(String unblockedId);

    void unblockFailed(int errorCode);
}
