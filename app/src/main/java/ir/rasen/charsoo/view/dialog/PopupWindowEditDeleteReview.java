package ir.rasen.charsoo.view.dialog;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.object.Review;
import ir.rasen.charsoo.view.activity.business.ActivityAddUpdateReview;
import ir.rasen.charsoo.view.activity.user.ActivityUserReviews;
import ir.rasen.charsoo.view.adapter.AdapterUserReview;
import ir.rasen.charsoo.view.interface_m.IReviewChange;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.WaitDialog;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;


public class PopupWindowEditDeleteReview extends PopupWindow {

    public PopupWindowEditDeleteReview
            (final CharsooActivity charsooActivity, final Review review,
             final IWebservice iWebservice,
             final IReviewChange iReviewChange) {
        super(charsooActivity);


        LayoutInflater inflater = (LayoutInflater)
                charsooActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_post_business_more, null, false);
        setContentView(view);

        view.findViewById(R.id.btn_post_more_edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        charsooActivity.openUpdateReviewPage(review.uniqueId);
                    }
                }, Params.FREEZ);
            }
        });
        view.findViewById(R.id.btn_post_more_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        DialogDeleteReviewConfirmation d =
                                new DialogDeleteReviewConfirmation(
                                        charsooActivity, review, iWebservice,
                                         iReviewChange,
                                        ActivityUserReviews.REVIEW_DELETE_REQUEST);
                        d.show();
                        PopupWindowEditDeleteReview.this.dismiss();
                    }
                }, Params.FREEZ);
            }
        });

        setBackgroundDrawable(new BitmapDrawable());
        setOutsideTouchable(true);
        setWindowLayoutMode(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

    }
}
