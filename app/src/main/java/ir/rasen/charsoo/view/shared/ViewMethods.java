package ir.rasen.charsoo.view.shared;

import android.view.MotionEvent;
import android.view.View;

import ir.rasen.charsoo.R;

/**
 * Created by 'Sina KH' on 7/5/15.
 */
public class ViewMethods {

    public static View.OnTouchListener onTouchListenerGrayCircle = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if(event.getAction()==MotionEvent.ACTION_DOWN) {
                v.setBackgroundResource(R.mipmap.circle_gray);
            } else
                v.setBackgroundResource(android.R.color.transparent);
            return false;
        }
    };
}
