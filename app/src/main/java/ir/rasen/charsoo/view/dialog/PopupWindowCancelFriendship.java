package ir.rasen.charsoo.view.dialog;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.view.interface_m.ICancelFriendship;
import ir.rasen.charsoo.view.widgets.buttons.ButtonFont;


public class PopupWindowCancelFriendship extends PopupWindow {

    public PopupWindowCancelFriendship(final Context context, final String requestUserId, final ICancelFriendship iCancelFriendship) {
        super(context);

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_post_report_cancel_share_more, null, false);
        setContentView(view);

        view.findViewById(R.id.btn_post_more_cancel_share).setVisibility(View.GONE);
        ButtonFont textViewReport = (ButtonFont) view.findViewById(R.id.btn_post_more_report);

        textViewReport.setText(R.string.unfriend);
        textViewReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogCancelFriendshipConfirmation d = new DialogCancelFriendshipConfirmation(context, requestUserId, iCancelFriendship);
                d.show();
                PopupWindowCancelFriendship.this.dismiss();
            }
        });
        setBackgroundDrawable(new BitmapDrawable());
        setOutsideTouchable(true);
        setWindowLayoutMode(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

    }




}
