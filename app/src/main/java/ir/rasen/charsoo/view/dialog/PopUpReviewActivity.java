package ir.rasen.charsoo.view.dialog;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;

import java.io.File;

import eu.janmuller.android.simplecropimage.CropImage;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.model.review.ReviewActivity;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.material_library.views.Switch;
import ir.rasen.charsoo.view.widgets.material_library.widgets.SnackBar;


public class PopUpReviewActivity extends AlertDialog.Builder implements IWebservice{
    public static final int USEFUL = 100;
    public static final int NOT_USEFUL = 200;
    public static final int REPORT = 300;

    public Activity activity;

    public PopUpReviewActivity(final Activity activity, final String revUnqId) {
        super(activity);
        this.activity = activity;
        setItems(getContext().getResources().getStringArray(R.array.review_activity_texts), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        new ReviewActivity(activity,revUnqId,ReviewActivity.ACTIVITY_HELP_FULL,
                                PopUpReviewActivity.this,USEFUL);
                        break;

                    case 1:
                        new ReviewActivity(activity,revUnqId,ReviewActivity.ACTIVITY_NOT_HELP_FULL,
                                PopUpReviewActivity.this,NOT_USEFUL);
                        break;

                    case 2 :
                        new ReviewActivity(activity,revUnqId,ReviewActivity.REPORT,
                                PopUpReviewActivity.this,REPORT);
                        break;
                }
            }
        });

    }


    @Override
    public void getResult(int request, Object result) throws Exception {
        new SnackBar(activity,activity.getResources().getString(R.string.success_review_activity)).show();
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) throws Exception {
        new SnackBar(activity,activity.getResources().getString(R.string.error_network_faild)).show();
    }
}
