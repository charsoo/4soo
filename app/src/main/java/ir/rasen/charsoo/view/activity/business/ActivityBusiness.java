package ir.rasen.charsoo.view.activity.business;



import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.PullToRefreshGrid;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.business.GetBusinessHomeInfo;
import ir.rasen.charsoo.model.post.GetBusinessPosts;
import ir.rasen.charsoo.model.post.GetPost;
import ir.rasen.charsoo.view.activity.ActivityMain;
import ir.rasen.charsoo.view.activity.ActivityPostAddEdit;
import ir.rasen.charsoo.view.dialog.PopupBusinessInviteFriends;
import ir.rasen.charsoo.view.interface_m.IAddPost;
import ir.rasen.charsoo.view.interface_m.IDeletePost;
import ir.rasen.charsoo.view.interface_m.IOnBusinessEditedListener;
import ir.rasen.charsoo.view.interface_m.IPostUpdateListener;
import ir.rasen.charsoo.view.interface_m.IPullToRefresh;
import ir.rasen.charsoo.view.interface_m.ISelectBusiness;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.shared.GridViewBusinessBothView;
import ir.rasen.charsoo.view.shared.PostInitializerBothView;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.buttons.FloatButton;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.material_library.widgets.ProgressDialog;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.HFGridView;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.PullToRefreshGridViewWithHeaderAndFooter;


public class ActivityBusiness extends CharsooActivity
        implements ISelectBusiness, IWebservice, IPullToRefresh,
        NetworkStateChangeListener,IPostUpdateListener,TryAgainListener,IDeletePost,
        IOnBusinessEditedListener,IAddPost{

    public static final String TAG="ActivityBusiness";

    private static final int GET_BUSINESS_POSTS_INT_CODE=11,GET_BUSINESS_HOME_INFO_INT_CODE=14,GET_SINGLE_POST_INT_CODE=18;


    private enum Status {FIRST_TIME, LOADING_MORE, REFRESHING, NONE}
    Status status;

    //    WaitDialog progressDialog;
    String selectedBusinessId;
    HFGridView gridView;
    public Business business;
    //    NOT_USED_GridViewBusiness gridViewBusiness;
    GridViewBusinessBothView gridViewBusiness;
    ArrayList<Post> posts;
    //    BroadcastReceiver deletePost;
    RelativeLayout networkErrorLayout;

    //pull_to_refresh_lib
    private PullToRefreshGrid pullToRefreshGridView;

    private FloatButton btnAddPost;

    Context activityContext;

    public static IDeletePost iDeletePost;
    public static IPostUpdateListener iPostUpdateListener;
    public static IAddPost iAddPost;

    public static IOnBusinessEditedListener iOnBusinessEditedListener;


    RelativeLayout actionBarLayout;
    ImageView ivBusinessInviteFriends,ivBack,imageViewSearch;
    TextViewFont textViewIdentifier;


    public static PopupBusinessInviteFriends popupBusinessInviteFriends;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);


        setContentView(R.layout.activity_business);
        iPostUpdateListener = ActivityBusiness.this;

        iDeletePost = ActivityBusiness.this;

//        initNewActionBar();

        selectedBusinessId = getIntent().getExtras().getString(Params.BUSINESS_ID_STRING);

//        progressDialog = new WaitDialog(this);
//        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        networkErrorLayout=MyApplication.initNetworkErrorLayout(getWindow().getDecorView(), ActivityBusiness.this, ActivityBusiness.this);

        pullToRefreshGridView = new PullToRefreshGrid(ActivityBusiness.this, (PullToRefreshGridViewWithHeaderAndFooter) findViewById(R.id.gridView_HF), ActivityBusiness.this);
        gridView = pullToRefreshGridView.getGridViewHeaderFooter();

        actionBarLayout = (RelativeLayout) findViewById(R.id.ll_action_bar);
        actionBarLayout.setOnClickListener(null);

        ivBack = (ImageView) findViewById(R.id.imageView_back);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        textViewIdentifier = (TextViewFont)findViewById(R.id.textView_business_identifier_header);

        ivBusinessInviteFriends =
                (ImageView) findViewById(R.id.iv_business_invite_friends);
        imageViewSearch =
                (ImageView) findViewById(R.id.imageView_search);

        ivBusinessInviteFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View.OnClickListener onInviteClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ActivityBusiness.this,ActivityBusinessRegister.class);
                        intent.putExtra("please_open_invite",true);
                        intent.putExtra("business_str_id",business.businessIdentifier);
                        intent.putExtra("business_name",business.name);

                        popupBusinessInviteFriends.dismiss();

                        startActivity(intent);
                    }
                };

                View.OnClickListener onBlockedUsersClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ActivityBusiness.this,ActivityBusinessBlockedUsers.class);
                        intent.putExtra(Params.BUSINESS_ID_STRING,business.uniqueId);

                        popupBusinessInviteFriends.dismiss();

                        startActivity(intent);
                    }
                };

                popupBusinessInviteFriends =
                        new PopupBusinessInviteFriends(ActivityBusiness.this,
                                onInviteClick,onBlockedUsersClickListener);
                popupBusinessInviteFriends.showAsDropDown(ivBusinessInviteFriends);
            }
        });

        imageViewSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityMain.openThisActivityAndGotoFragmentSearch(ActivityBusiness.this);
            }
        });


        showWaitDialog();
        status=Status.FIRST_TIME;
        callWebservice(GET_BUSINESS_HOME_INFO_INT_CODE,true);

//        deletePost = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                //if the user delete a post from ActivityPost
//                gridViewBusiness.notifyDeletePost(intent.getIntExtra(Params.POST_ID_INT, 0));
//            }
//        };
//        LocalBroadcastManager.getInstance(this).registerReceiver(deletePost, new IntentFilter(Params.DELETE_POST_FROM_ACTIVITY));

        btnAddPost = (FloatButton) findViewById(R.id.btn_new_post);
        btnAddPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityBusiness.this, ActivityPostAddEdit.class);
                intent.putExtra(Params.BUSINESS_ID_STRING, business.uniqueId);
                startActivity(intent);
            }
        });


        activityContext = ActivityBusiness.this;

        iOnBusinessEditedListener = ActivityBusiness.this;

        iAddPost = ActivityBusiness.this;

    }

    @Override
    public void notifySelectBusiness(String businessId) {
        gridView.removeHeaderView(gridView.getHeaderView());
        gridView.setVisibility(View.GONE);
        selectedBusinessId=businessId;
        callWebservice(GET_BUSINESS_HOME_INFO_INT_CODE, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == Params.ACTION_ADD_POST) {

            } else if (requestCode == Params.ACTION_EDIT_BUSINESS) {
                /*if (data.getStringExtra(Params.TYPE).equals(Business.ChangeType.EDIT.name())) {
                    String picture = data.getStringExtra(Params.PROFILE_PICTURE);
                    if (picture != null) {
                        gridViewBusiness.changeProfilePicture(picture);
                    }
                }*//* else if (data.getStringExtra(Params.TYPE).equals(Business.ChangeType.DELETE.name())) {
                    Intent i = getIntent();
                    i.putExtra(Params.BUSINESS_ID_STRING, data.getIntExtra(Params.BUSINESS_ID_STRING, 0));
                    setResult(RESULT_OK, i);
                    finish();
                }*/
            } else if (requestCode == Params.ACTION_EDIT_POST) {
//                Post updatedPost = ((MyApplication) getApplication()).post;
//                for (int i = 0; i < posts.size(); i++) {
//                    if (posts.get(i).uniqueId == updatedPost.uniqueId) {
//                        Post p = posts.get(i);
//                        p.title = updatedPost.title;
//                        p.description = updatedPost.description;
//                        p.hashtagList = updatedPost.hashtagList;
//                        p.price = updatedPost.price;
//                        p.code = updatedPost.code;
//                        break;
//                    }
//                }
//                gridViewBusiness.notifyDatasetChanged();
            }


        }

    }

    @Override
    public void notifyRefresh() {
        if (posts == null)
            posts = new ArrayList<>();
        status=Status.REFRESHING;
        callWebservice(GET_BUSINESS_HOME_INFO_INT_CODE, false);
    }

    @Override
    public void notifyLoadMore() {
        status=Status.LOADING_MORE;
        callWebservice(GET_BUSINESS_POSTS_INT_CODE, false);
    }

    public void onClick(View view) {
        // to set null onClick, please don't clean this function
    }

    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        btnAddPost.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
    }

    @Override
    public void getResult(int request, Object result) {
        if(isShowingWaitDialog())
            hideWaitDialog();

        switch (request){
            case GET_BUSINESS_HOME_INFO_INT_CODE:
                business = (Business) result;
                gridView.setVisibility(View.VISIBLE);
                btnAddPost.setVisibility(View.VISIBLE);
                hideWaitDialog();

                Globals.getInstance().getValue().businessVisiting = business;

                boolean beThreeColumn = gridViewBusiness == null || gridViewBusiness.isThreeColumn;
                if (gridViewBusiness == null) {
//                    gridViewBusiness = new NOT_USED_GridViewBusiness(this, business, gridView);
                    gridViewBusiness = new GridViewBusinessBothView(this, business, gridView,ActivityBusiness.TAG,ActivityBusiness.this);
                    gridViewBusiness.InitialGridViewBusiness(new ArrayList<Post>(), beThreeColumn);
                } else
                    gridViewBusiness.refreshBusinessData(business);
                if (posts == null)
                    posts = new ArrayList<>();

                textViewIdentifier.setText(String.valueOf(business.businessIdentifier));

                gridViewBusiness.handleShowNotingLayout();
                callWebservice(GET_BUSINESS_POSTS_INT_CODE,false);
                break;
            case GET_BUSINESS_POSTS_INT_CODE:
                if (status==Status.FIRST_TIME || status==Status.REFRESHING){
                    posts=new ArrayList<>((ArrayList<Post>) result);
                    pullToRefreshGridView.setResultSize(posts.size());
                    if (pullToRefreshGridView.isRefreshing()) {
                        pullToRefreshGridView.onRefreshComplete();
/*                gridView.removeHeaderView(gridView.getHeaderView());*/
                    }
                    gridViewBusiness.InitialGridViewBusiness(posts, gridViewBusiness.isThreeColumn);
                }
                else if (status==Status.LOADING_MORE){
                    posts.addAll((ArrayList<Post>) result);
                    gridViewBusiness.addMorePosts((ArrayList<Post>) result);
                }
                status=Status.NONE;
                //
                break;

            case GET_SINGLE_POST_INT_CODE:
                Post newPost = (Post) result;
                for (int i = 0; i < posts.size() ; i++) {
                    if (posts.get(i).uniqueId.equals(newPost.uniqueId)){
                        posts.set(i, MyApplication.mergeTwoPost(posts.get(i), newPost));
                        gridViewBusiness.resetPostItems(posts);
                    }
                }
                gridViewBusiness.handleShowNotingLayout();
//                Post tempPost=(Post)result;
//                gridViewBusiness.adapterPostGridAndList.updateOnePost(tempPost);
                break;
        }
        if(isShowingWaitDialog())
//            progressDialog.dismiss();
            hideWaitDialog();
        NetworkConnectivityReciever.removeIfHaveListener(TAG);
        networkErrorLayout.setVisibility(View.GONE);
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
        if(isShowingWaitDialog())
            hideWaitDialog();


        switch(request){
            case GET_BUSINESS_HOME_INFO_INT_CODE :
                if (pullToRefreshGridView.isRefreshing())
                    pullToRefreshGridView.onRefreshComplete();
                if (errorCode==ServerAnswer.NETWORK_CONNECTION_ERROR){
                    NetworkConnectivityReciever.setNetworkStateListener(TAG,ActivityBusiness.this);
                }
                if(ServerAnswer.canShowErrorLayout(errorCode) && status==Status.FIRST_TIME) {
                    networkErrorLayout.setVisibility(View.VISIBLE);
                }
                else if(networkErrorLayout.getVisibility()!=View.VISIBLE){
                    new ProgressDialog(ActivityBusiness.this,
                            ServerAnswer.getError(
                                    ActivityBusiness.this,
                                    errorCode, callerStringID + ">" + this.getLocalClassName())).show();
                }
                break;
            case GET_BUSINESS_POSTS_INT_CODE :
                gridViewBusiness.listFooterView.setVisibility(View.GONE);
                if (pullToRefreshGridView.isRefreshing()) {
                    pullToRefreshGridView.onRefreshComplete();
                }
                if (errorCode==ServerAnswer.NETWORK_CONNECTION_ERROR){
                    NetworkConnectivityReciever.setNetworkStateListener(TAG,ActivityBusiness.this);
                }
                if(ServerAnswer.canShowErrorLayout(request) && status==Status.FIRST_TIME) {
                    networkErrorLayout.setVisibility(View.VISIBLE);
                }
                else if(networkErrorLayout.getVisibility()!=View.VISIBLE){
//                    new ProgressDialog(ActivityBusiness.this,
//                            ServerAnswer.getError(
//                                    ActivityBusiness.this,
//                                    errorCode, callerStringID + ">" + this.getLocalClassName())).show();
                }
                break;
        }
    }

    @Override
    public void doOnNetworkConnected() {
        status = Status.FIRST_TIME;
        callWebservice(GET_BUSINESS_HOME_INFO_INT_CODE, true);
    }

    private void callWebservice(int reqCode,boolean showProgressDialog){
        if(showProgressDialog)
            showWaitDialog();
        switch (reqCode) {
            case GET_BUSINESS_HOME_INFO_INT_CODE:
                new GetBusinessHomeInfo(ActivityBusiness.this, selectedBusinessId,LoginInfo.getInstance().getUserUniqueId(),
                        ActivityBusiness.this,GET_BUSINESS_HOME_INFO_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                break;
            case GET_BUSINESS_POSTS_INT_CODE:
                if(status!=Status.LOADING_MORE)
                    new GetBusinessPosts(ActivityBusiness.this,LoginInfo.getInstance().getUserUniqueId(),
                            business.uniqueId, 0, getResources().getInteger(R.integer.lazy_load_limitation),
                            ActivityBusiness.this,GET_BUSINESS_POSTS_INT_CODE,business.profilePictureUniqueId,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                else
                    new GetBusinessPosts(ActivityBusiness.this,LoginInfo.getInstance().getUserUniqueId(),
                            business.uniqueId,gridViewBusiness.adapterPostGridAndList.getCount(), getResources().getInteger(R.integer.lazy_load_limitation),
                            ActivityBusiness.this,GET_BUSINESS_POSTS_INT_CODE,business.profilePictureUniqueId,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                break;
        }
    }

//    @Override
//    public void doTryAgainDialog() {
//        callWebservice(lastFailedWebservice,true);
//    }

    @Override
    public void onPostDetailsChange(String dirtyPostId) {
        for (int i = 0; i < posts.size(); i++) {
            if (posts.get(i).uniqueId.equals(dirtyPostId)){
                new GetPost(activityContext,LoginInfo.getInstance().getUserUniqueId(),
                        posts.get(i).businessUniqueId,dirtyPostId, Post.GetPostType.BUSINESS,
                        ActivityBusiness.this,GET_SINGLE_POST_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                break;
            }
        }
    }

    @Override
    public void doTryAgain() {
        status = Status.FIRST_TIME;
        callWebservice(GET_BUSINESS_HOME_INFO_INT_CODE,true);
    }



    @Override
    public void notifyDeletePost(String postId) throws Exception {
        gridViewBusiness.notifyDeletePost(postId);
    }

    @Override
    public void notifyDeleteFailed(String postId, String failureMessage) throws Exception {}


    @Override
    public void onBackPressed() {
        if(PostInitializerBothView.dismissAllPopupsIfAnyShowing())
            return;

        if(popupBusinessInviteFriends!=null &&
                popupBusinessInviteFriends.isShowing()){
            popupBusinessInviteFriends.dismiss();
            return;
        }

        super.onBackPressed();
    }


    @Override
    public void onBusinessEdited(String editedBusinessId) {
        callWebservice(GET_BUSINESS_HOME_INFO_INT_CODE, true);
    }


    @Override
    public void notifyAddPost(Post post) {
//        Post p = ((MyApplication) getApplication()).post;
//        p.businessIdentifier=business.businessIdentifier;
//        p.businessProfilePictureUniqueId=business.profilePictureUniqueId;
//        p.businessUniqueId=business.uniqueId;
//        gridViewBusiness.notifyDataSetChanged(p);
        status = Status.REFRESHING;
        callWebservice(GET_BUSINESS_POSTS_INT_CODE, true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    public void setTextViewIdentifier(String s) {
        textViewIdentifier.setText(s);
    }
}