package ir.rasen.charsoo.view.fragment.business_edit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LocationManagerTracker;
import ir.rasen.charsoo.controller.helper.Location_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.City;
import ir.rasen.charsoo.controller.object.State;
import ir.rasen.charsoo.model.GetCountryStates;
import ir.rasen.charsoo.model.GetProvinceCities;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.view.activity.ActivityMapChoose;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessEdit;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.widgets.buttons.ButtonFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.edittexts.EditTextFont;
import ir.rasen.charsoo.view.widgets.imageviews.ExpandableImageView;

public class FragmentBusinessEditLocationInfo extends Fragment implements IWebservice, NetworkStateChangeListener {

    public static final String TAG = "FragmentBusinessEditLocationInfo";

    private static final int GET_STATES_LIST_REQUEST = 12, GET_CITIES_LIST_REQUEST = 14;

    private Spinner spinnerStates, spinnerCities;

    //    boolean doRefreshCitiesList = true;
//    int selectedStatePosition, selectedCityPosition;
    private boolean isViewCreated = false;

    public static State selectedState;
    public static City selectedCity;


//    Hashtable<String, Integer> statesHashtable, citiesHashtable;
//    ArrayList<String> citiesSortedList, statesSortedList;

    EditTextFont editTextStreet;
    ButtonFont buttonMap;
    CharsooActivity mCharsooActivity;
    private ExpandableImageView imageViewPicture;
    //MapView mapView;
    //LatLng choosedLatLng;
    Business business;
    String latitude, longitude;
    boolean isEditting = false;
    //    private WaitDialog progressDialog;
    private int autoLoadRequestCode;
    SimpleLoader simpleLoader;





    @Override/**/
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CustomActivityOnCrash.install(getActivity());

        View view = inflater.inflate(R.layout.fragment_register_business_location_info,
                container, false);
        mCharsooActivity = (CharsooActivity) getActivity();
        mCharsooActivity.showWaitDialog();

        isViewCreated=true;

        imageViewPicture = (ExpandableImageView) view.findViewById(R.id.imageView_cover);
//

        spinnerStates = (Spinner) view.findViewById(R.id.spinner_States);
        spinnerCities = (Spinner) view.findViewById(R.id.spinner_Cities);


        if(selectedCity==null || selectedState==null) {
            selectedState = new State("-1", Globals.getInstance().getValue().businessEditing.stateName);
            selectedCity = new City("-1", Globals.getInstance().getValue().businessEditing.cityName);
        }


        /*if (selectedStateName == null || selectedStateName.equals("") ||
                Globals.getInstance().getValue().loadedStates==null
                ) {*/
        Globals.getInstance().getValue().loadedStates = new ArrayList<>();
        setSpinnerStatesAdapter(new ArrayList<String>(), getActivity());

        if(Globals.getInstance().getValue().loadedStates==null || Globals.getInstance().getValue().loadedStates.size()==0) {
            mCharsooActivity.showWaitDialog();
            new GetCountryStates(getActivity(), FragmentBusinessEditLocationInfo.this, GET_STATES_LIST_REQUEST, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
        }
        else{
            mCharsooActivity.hideWaitDialog();
            ArrayList<String> statesName =
                    State.getStatesName(Globals.getInstance().getValue().loadedStates);

            setSpinnerStatesAdapter(
                    statesName,
                    getActivity());

            if(selectedState!=null &&
                    selectedState.stateName!=null &&
                    !selectedState.stateName.equals("") &&
                    !selectedState.stateName.equals("null"))
                spinnerStates.setSelection(statesName.indexOf(selectedState.stateName));
        }
        setSpinnerCitiesAdapter(new ArrayList<String>(), getActivity());
        spinnerCities.setEnabled(false);

        spinnerStates.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==0){
                    spinnerCities.setVisibility(View.GONE);
                }
                else if(i>0){
                    spinnerCities.setVisibility(View.VISIBLE);
                    mCharsooActivity.showWaitDialog();

                    String selectedStateId = Globals.getInstance().getValue().loadedStates.get(i).stateUniqueId;

                    new GetProvinceCities(getActivity(),selectedStateId,
                            FragmentBusinessEditLocationInfo.this,GET_CITIES_LIST_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;

                    selectedState.stateName = Globals.getInstance().getValue().loadedStates.get(i).stateName;
                    selectedState.stateUniqueId = selectedStateId;

                    Globals.getInstance().getValue().businessEditing.stateUniqueId = selectedStateId;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerCities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i>0) {
                    selectedCity.cityName = Globals.getInstance().getValue().loadedCities.get(i).cityName;
                    selectedCity.cityUniqueId = Globals.getInstance().getValue().loadedCities.get(i).cityUniqueId;

                    Globals.getInstance().getValue().businessEditing.cityUniqueId = selectedCity.cityUniqueId;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        editTextStreet = (EditTextFont) view.findViewById(R.id.edt_street);


        try {
            isEditting = getArguments().getBoolean(Params.IS_EDITTING);
        } catch (Exception e) {

        }

        buttonMap = (ButtonFont) view.findViewById(R.id.btn_map);

        if (isEditting) {
            business = Globals.getInstance().getValue().businessEditing;
            editTextStreet.setText(business.address);
//
            buttonMap.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_check_white_24dp), null);
            latitude = business.location_m.getLatitude();
            longitude = business.location_m.getLongitude();
            if(ActivityBusinessEdit.myBitmap!= null)
                imageViewPicture.setImageBitmap(ActivityBusinessEdit.myBitmap);
            else {
                simpleLoader = new SimpleLoader(mCharsooActivity);
                simpleLoader.loadImage(business.profilePictureUniqueId, Image_M.LARGE, Image_M.ImageType.BUSINESS, imageViewPicture);
            }
            imageViewPicture.post(new Runnable() {
                @Override
                public void run() {
                    imageViewPicture.requestFocus();
                }
            });
        }

        if (!LocationManagerTracker.isGooglePlayServicesAvailable(getActivity())) {
            buttonMap.setEnabled(false);
            view.findViewById(R.id.textView_play_service).setVisibility(View.VISIBLE);

            LocationManagerTracker lt = new LocationManagerTracker(getActivity());
            // check if GPS enabled
            if (lt.canGetLocation()) {

                latitude = String.valueOf(lt.getLatitude());
                longitude = String.valueOf(lt.getLongitude());
                Globals.getInstance().getValue().businessEditing.location_m = new Location_M(latitude, longitude);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                lt.showSettingsAlert();
            }
        }
        buttonMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ActivityMapChoose.class);
                if (isEditting) {
                    intent.putExtra(Params.LATITUDE, business.location_m.getLatitude());
                    intent.putExtra(Params.LONGITUDE, business.location_m.getLongitude());
                    intent.putExtra(Params.IS_EDITTING, true);
                } else
                    intent.putExtra(Params.IS_EDITTING, false);
                startActivityForResult(intent, Params.ACTION_CHOOSE_LOCATION);
            }
        });

        return view;
    }


    @Override
    public void onResume() {
        //mapView.onResume();
        super.onResume();
    }

    public boolean isVerified() {
        if(!isViewCreated)
            return true;

        try {
            if (spinnerStates.getSelectedItemPosition() == 0) {
                new DialogMessage(getActivity(), getActivity().getString(R.string.err_PleaseSelectState)).show();
                return false;
            }

            if (spinnerCities.getSelectedItemPosition() == 0) {
                new DialogMessage(getActivity(), getActivity().getString(R.string.err_PleaseSelectCity)).show();
                return false;
            }
            Globals.getInstance().getValue().businessEditing.stateName = selectedState.stateName;
            Globals.getInstance().getValue().businessEditing.cityName = selectedCity.cityName;
            Globals.getInstance().getValue().businessEditing.address = editTextStreet.getText().toString();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Params.ACTION_CHOOSE_LOCATION && resultCode == Activity.RESULT_OK) {
            latitude = data.getStringExtra(Params.LATITUDE);
            String s = data.getStringExtra(Params.LATITUDE);
            longitude = data.getStringExtra(Params.LONGITUDE);
            Globals.getInstance().getValue().businessEditing.location_m = new Location_M(String.valueOf(latitude), String.valueOf(longitude));
            buttonMap.setBackgroundResource(R.drawable.selector_button_register);
            buttonMap.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_check_white_24dp), null);
        }
    }


    @Override
    public void getResult(int request, Object result) {
        mCharsooActivity.hideWaitDialog();

        if (request == GET_STATES_LIST_REQUEST) {

            Globals.getInstance().getValue().loadedStates =
                    State.addDefaultStateToStartOfList(getActivity(), (ArrayList<State>) result);

            ArrayList<String> statesName = State.getStatesName(Globals.getInstance().getValue().loadedStates);

            setSpinnerStatesAdapter(
                    statesName,
                    getActivity());

            if(selectedState!=null &&
                    selectedState.stateName!=null &&
                    !selectedState.stateName.equals("") &&
                    !selectedState.stateName.equals("null"))
                spinnerStates.setSelection(statesName.indexOf(selectedState.stateName));
        } else if (request == GET_CITIES_LIST_REQUEST) {
            Globals.getInstance().getValue().loadedCities =
                    City.addDefaultCityToStartOfList(getActivity(),(ArrayList<City>) result);

            ArrayList<String> citiesName = City.getCitiesName(Globals.getInstance().getValue().loadedCities);
            setSpinnerCitiesAdapter(citiesName,
                    getActivity());


            int selectedPos = citiesName.indexOf(selectedCity.cityName);
            if(selectedCity!=null &&
                    selectedCity.cityName!=null &&
                    !selectedCity.cityName.equals("") &&
                    !selectedCity.cityName.equals("null") &&
                    selectedPos != -1){
                spinnerCities.setSelection(selectedPos);

            }
        }
        NetworkConnectivityReciever.removeIfHaveListener(TAG);
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
        mCharsooActivity.hideWaitDialog();
        new DialogMessage(getActivity(), ServerAnswer.getError(getActivity(), errorCode, callerStringID + ">" + TAG)).show();
        if (errorCode == ServerAnswer.NETWORK_CONNECTION_ERROR) {
            autoLoadRequestCode = request;
            NetworkConnectivityReciever.setNetworkStateListener(TAG, FragmentBusinessEditLocationInfo.this);
        }
    }

    @Override
    public void doOnNetworkConnected() {
//        if (autoLoadRequestCode == GET_CITIES_LIST_REQUEST) {
//            new GetProvinceCities(getActivity(), statesHashtable.get(selectedState), FragmentBusinessEditLocationInfo.this, GET_CITIES_LIST_REQUEST).executeWithNewSolution();;
//        } else if (autoLoadRequestCode == GET_STATES_LIST_REQUEST) {
//            new GetCountryStates(getActivity(), FragmentBusinessEditLocationInfo.this, GET_STATES_LIST_REQUEST).executeWithNewSolution();;
//        }
    }

    private void setSpinnerStatesAdapter(final ArrayList<String> statesName, Context c) {

        ArrayAdapter<String> statesAdapter = new ArrayAdapter<>(c,
                android.R.layout.simple_spinner_dropdown_item, statesName);
        spinnerStates.setAdapter(statesAdapter);


//        spinnerStates.post(new Runnable() {
//            @Override
//            public void run() {
//                if (isEditting && spinnerStates.getSelectedItemPosition() == 0) {
//                    int i = statesName.indexOf(business.stateName);
//                    spinnerStates.setSelection(i+1);
//                }
//            }
//        });
    }

    private void setSpinnerCitiesAdapter(final ArrayList<String> citiesName, Context c) {
        ArrayAdapter<String> citiesAdapter = new ArrayAdapter<>(c,
                android.R.layout.simple_spinner_dropdown_item, citiesName);
        spinnerCities.setAdapter(citiesAdapter);
        if (citiesName.size() > 0)
            spinnerCities.setEnabled(true);
        else
            spinnerCities.setEnabled(false);


//        spinnerCities.post(new Runnable() {
//            @Override
//            public void run() {
//                if (isEditting && spinnerCities.getSelectedItemPosition() == 0) {
//                    int i = cities.indexOf(business.cityName);
//                    spinnerCities.setSelection(i+1);
//                }
//            }
//        });
    }

    public boolean isBusinessDirty(Business originalBusiness) {
        boolean isDirty = false;
        if (!isDirty &&
                selectedState != null &&
                !originalBusiness.stateName.equals(selectedState.stateName))
            isDirty = true;
        if (!isDirty &&
                selectedCity != null &&
                !originalBusiness.cityName.equals(selectedCity.cityName))
            isDirty = true;

        String ss;
        if(editTextStreet != null) {
            String newAddress;
            newAddress = editTextStreet.getText().toString();
            ss = newAddress;
        }
        String orginalAddress = originalBusiness.address;

        if (!isDirty && editTextStreet != null && !originalBusiness.address.equals(editTextStreet.getText().toString()))
            isDirty = true;
        if (!isDirty && Location_M.areLocationsDifferent(originalBusiness.location_m, new Location_M(latitude, longitude)))
            isDirty = true;
        return isDirty;
    }

    public boolean isCreated() {
        return isViewCreated;
    }


}
