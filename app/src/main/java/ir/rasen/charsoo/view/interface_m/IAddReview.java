package ir.rasen.charsoo.view.interface_m;

import ir.rasen.charsoo.controller.object.RequestObject;

/**
 * Created by android on 4/11/2015.
 */
public interface IAddReview {
    void successAddReview(RequestObject.ReviewItem reviewItem) throws  Exception;
    void failAddReview(int errorCode,String error) throws Exception;
}
