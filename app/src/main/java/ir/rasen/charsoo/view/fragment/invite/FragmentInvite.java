package ir.rasen.charsoo.view.fragment.invite;


import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.GetContactData;
import ir.rasen.charsoo.controller.helper.GetInstalledApps;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.ContactEntry;
import ir.rasen.charsoo.controller.object.GlobalContactPassingClass;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.PackageInfoCustom;
import ir.rasen.charsoo.model.friend.TakeContactList;
import ir.rasen.charsoo.view.activity.ActivityInvite;
import ir.rasen.charsoo.view.adapter.AdapterRegisterUserTabs;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.fragment.timeline.FragmentTimelineAllPosts;
import ir.rasen.charsoo.view.fragment.timeline.FragmentTimelineFollowingBusinessPosts;
import ir.rasen.charsoo.view.interface_m.IGetContactListener;
import ir.rasen.charsoo.view.interface_m.IGetInstalledAppsListener;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.widgets.CustomViewPager;
import ir.rasen.charsoo.view.widgets.PagerSlidingTabStrip;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;


/**
 * Created by hossein-pc on 6/9/2015.
 */
public class FragmentInvite extends Fragment implements IGetContactListener, IGetInstalledAppsListener, IWebservice,TryAgainListener {
    public static final String TAG = "FragmentInvite";
    public static final int TAKE_CONTACTS_INT_CODE = 11,GET_REMAINING_SMS_COUNT_INT_CODE=14;
    public static String Business_name="";
    public static String Business_id="";
    public String userIntId = "0";
    public Thread th1,th2,th3;
    private boolean isGettingContacts = false;

    public ArrayList<ContactEntry> noneCharsooEmailContactsList, noneCharsooPhoneNumberContactsList, charsooContactsList;
    ArrayList<PackageInfoCustom> applicationList;
    Hashtable<String, ArrayList<ContactEntry>> emailContactsTable, numberContactsTable;
    boolean haveContactList = false;
    public static boolean isBusinessPromotion;

    private ViewPager mViewPager;
    private PagerSlidingTabStrip tabs;
    private AdapterRegisterUserTabs adapterTabs;
    Context context;

    RelativeLayout networkFailedLayout;

    String firstChildTitle, secondChildTitle, thirdChildTitle;
    public static String smsMessage, emailMessage;
    DialogMessage dialogMessage;

    View view;

    public static CharsooActivity charsooActivityContext;

    public static FragmentInvite newInstance(Context c, String userIntId,
                                             boolean isBusinessPromotion,
                                             String firstChildTitle,
                                             String secondChildTitle,
                                             String thirdChildTitle,
                                             String smsMessage,
                                             String emailMessage,
                                             Hashtable<String, ArrayList<ContactEntry>> emailContacts,
                                             Hashtable<String, ArrayList<ContactEntry>> numberContacts,
                                             ArrayList<PackageInfoCustom> appList) {
        FragmentInvite fragment = new FragmentInvite();
        fragment.firstChildTitle = firstChildTitle;
        fragment.secondChildTitle = secondChildTitle;
        fragment.thirdChildTitle = thirdChildTitle;
        fragment.smsMessage = smsMessage;
        fragment.emailMessage = emailMessage;
        fragment.userIntId = userIntId;
        fragment.isBusinessPromotion = isBusinessPromotion;
        fragment.setPhoneContactsHashtabe(c, emailContacts, numberContacts);
        fragment.setApplicationList(appList);


        return fragment;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CustomActivityOnCrash.install(getActivity());

//        FragmentInvite.email_sms_format=false;
        context = getActivity();
        // dialogMessage=new DialogMessage(context,"");
        charsooActivityContext = (CharsooActivity) getActivity();
        charsooActivityContext.showWaitDialog();
        //dialogMessage=new DialogMessage(context,"");

        view = inflater.inflate(R.layout.fragment_invite,
                container, false);

        view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);


        networkFailedLayout= MyApplication.initNetworkErrorLayout(view,getActivity(),FragmentInvite.this);



        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        charsooActivityContext.hideWaitDialog();
        // BEGIN_INCLUDE (setup_viewpager)
        // Get the ViewPager and set it's PagerAdapter so that it can display items
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        if (isBusinessPromotion) {
            adapterTabs = new AdapterRegisterUserTabs(getChildFragmentManager(), context, firstChildTitle, secondChildTitle, thirdChildTitle, 2);
            mViewPager.setOffscreenPageLimit(2);


        }
        else
        {
            adapterTabs = new AdapterRegisterUserTabs(getChildFragmentManager(), context, firstChildTitle, secondChildTitle, thirdChildTitle, 3);
            mViewPager.setOffscreenPageLimit(3);


        }
        mViewPager.setAdapter(adapterTabs);
        // END_INCLUDE (setup_viewpager)

        tabs = (PagerSlidingTabStrip) view.findViewById(R.id.sliding_tabs);

        try {
            tabs.setViewPager(mViewPager);

        }catch (Exception e){

        }

        // TODO UNCOMMENT THIS
//        userIntId = LoginInfo.getUserId(context);


        // END_INCLUDE (setup_slidingtablayout)
        if (applicationList != null) {
            adapterTabs.getFragments().fragInvite.setApplicationList(applicationList);
        } else {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
//                    new GetInstalledApps(getActivity(), FragmentInvite.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                else
            if (!GetInstalledApps.isRunning)
                th3=new Thread(new Runnable() {
                    @Override
                    public void run() {
                        new GetInstalledApps(getActivity(), FragmentInvite.this).execute();
                    }
                });

            else
                GetInstalledApps.addDelegate(FragmentInvite.this);

            view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        }
        if ((emailContactsTable == null) || (numberContactsTable == null)) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
//                    new GetContactData(getActivity(), FragmentInvite.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                else
            if (!GetContactData.isRunning){
                th1=new Thread(new Runnable() {
                    @Override
                    public void run() {
                        new GetContactData(getActivity(), FragmentInvite.this).execute();
                    }
                });
                th1.start();
            }

            else
                GetContactData.addDelegate(FragmentInvite.this);
        } else if ((charsooContactsList == null) && (!isGettingContacts)) {
            doOnPhoneContactsReady(getActivity(), isBusinessPromotion);
        } else if (!isGettingContacts)
            doOnContactsFriendsReady();


        if ((applicationList != null) && (charsooContactsList != null))
            view.findViewById(R.id.progressBar).setVisibility(View.GONE);

    }

    @Override
    public void getPhoneContacts() {
        setPhoneContactsHashtabe(context, GlobalContactPassingClass.getInstance().getEmailContacts(),
                GlobalContactPassingClass.getInstance().getNumberContacts());
    }

    public void doOnContactsFriendsReady() {
        if (view != null) {
            if ( !isBusinessPromotion) {
                   mViewPager.setCurrentItem(2, true);
            }
            if(isBusinessPromotion){
                mViewPager.setCurrentItem(1, true);
            }

            adapterTabs.getFragments().fragAdd.setCharsooContacts(charsooContactsList);
            adapterTabs.getFragments().fragSMS.setNoneCharsooContacts(noneCharsooPhoneNumberContactsList);
            adapterTabs.getFragments().fragInvite.setNoneCharsooContacts(noneCharsooEmailContactsList);
            if (applicationList != null)
                view.findViewById(R.id.progressBar).setVisibility(View.GONE);
        }
    }

    @Override
    public void setAppResults() {
        setApplicationList(GlobalContactPassingClass.getInstance().getAppList());
    }

    @Override
    public void getResult(int request, Object result) {
        charsooActivityContext.hideWaitDialog();
        if (request == TAKE_CONTACTS_INT_CODE) {
            ArrayList<ContactEntry> tempResult = (ArrayList<ContactEntry>) result;
            for (int i = 0; i < tempResult.size(); i++) {
                ContactEntry contactEntry = tempResult.get(i);
                if (contactEntry.type == ContactEntry.ContactType.Email) {
                    for (int j = 0; j < contactEntry.namesMatchThisContactData.size(); j++) {
                        String str = contactEntry.namesMatchThisContactData.get(j);
                        if (emailContactsTable.containsKey(str)) { // TODO : niazi be in if nist, mitavan bad az amade shodane webservice in if ra hazf kard
                            for (int k = 0; k < emailContactsTable.get(str).size(); k++) {
                                if (contactEntry.contactData.equals(emailContactsTable.get(str).get(k).contactData))
                                    emailContactsTable.get(str).remove(k);
                            }
                        }
                    }

                } else if (contactEntry.type == ContactEntry.ContactType.PhoneNumber) {
                    for (int j = 0; j < contactEntry.namesMatchThisContactData.size(); j++) {
                        String str = contactEntry.namesMatchThisContactData.get(j);
                        if (numberContactsTable.containsKey(str)) { // TODO : niazi be in if nist, mitavan bad az amade shodane webservice in if ra hazf kard
                            for (int k = 0; k < numberContactsTable.get(str).size(); k++) {
                                if (contactEntry.contactData.equals(numberContactsTable.get(str).get(k).contactData))
                                    numberContactsTable.get(str).remove(k);
                            }
                        }
                    }
                }
            }

            sortPhoneContacts();
            charsooContactsList = new ArrayList<>(tempResult);
            doOnContactsFriendsReady();
            isGettingContacts = false;
            networkFailedLayout.setVisibility(View.GONE);
        }
//        else if (request==GET_REMAINING_SMS_COUNT_INT_CODE){
//            adapterTabs.getFragments().fragSMS.remainingSMSCount=(Integer) result;
//        }

    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
        charsooActivityContext.hideWaitDialog();
        isGettingContacts = false;
        if (request==TAKE_CONTACTS_INT_CODE && ServerAnswer.canShowErrorLayout(errorCode)){
            networkFailedLayout.setVisibility(View.VISIBLE);
        }
        else {
            if (!dialogMessage.isShowing()){
                dialogMessage.show();
                dialogMessage.setMessage(ServerAnswer.getError(context,errorCode,callerStringID+">"+TAG));
            }
        }
    }


    public void sortPhoneContacts(){
        noneCharsooEmailContactsList = new ArrayList<>();
        noneCharsooPhoneNumberContactsList = new ArrayList<>();

        ArrayList<String> fullNameList = new ArrayList<>(emailContactsTable.keySet());
        Collections.sort(fullNameList);
        for (int i = 0; i < fullNameList.size(); i++) {
            for (int j = 0; j < emailContactsTable.get(fullNameList.get(i)).size(); j++) {
                noneCharsooEmailContactsList.add(emailContactsTable.get(fullNameList.get(i)).get(j));
            }
        }
        fullNameList = new ArrayList<>(numberContactsTable.keySet());
        Collections.sort(fullNameList);
        for (int i = 0; i < fullNameList.size(); i++) {
            for (int j = 0; j < numberContactsTable.get(fullNameList.get(i)).size(); j++) {
                noneCharsooPhoneNumberContactsList.add(numberContactsTable.get(fullNameList.get(i)).get(j));
            }
        }
    }


    public void setApplicationList(ArrayList<PackageInfoCustom> appList) {
        if (appList != null) {
            applicationList = new ArrayList<>(appList);
            if (adapterTabs != null && adapterTabs.getFragments().fragInvite != null) {
                adapterTabs.getFragments().fragInvite.setApplicationList(applicationList);
            }
            if ((view != null) && (charsooContactsList != null)) {
                view.findViewById(R.id.progressBar).setVisibility(View.GONE);
            }
        }
    }

    public boolean ifHideEmailList() {
        if (adapterTabs != null && adapterTabs.getFragments().fragInvite != null) {
            return adapterTabs.getFragments().fragInvite.ifHideEmailList();
        } else
            return false;
    }

    public void doOnPhoneContactsReady(final Context c,boolean isBusinessPromotion){
        if (!isBusinessPromotion) {
            if ((!userIntId.equals("0")) && (!isGettingContacts)) {
                charsooContactsList = new ArrayList<>();
                isGettingContacts = true;
                th2=new Thread(new Runnable() {
                    @Override
                    public void run() {
                        new TakeContactList(c, userIntId, numberContactsTable, emailContactsTable, FragmentInvite.this, TAKE_CONTACTS_INT_CODE, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                    }
                });
                th2.start();
            }
        }
        else{
            charsooContactsList = new ArrayList<>();
            sortPhoneContacts();
            doOnContactsFriendsReady();
        }
    }

    public void setPhoneContactsHashtabe(Context c, Hashtable<String, ArrayList<ContactEntry>> emailContacts, Hashtable<String, ArrayList<ContactEntry>> numberContacts) {
        if (emailContacts != null && numberContacts != null) {
            if (emailContactsTable == null || numberContactsTable == null) {
                emailContactsTable = new Hashtable<>(emailContacts);
                numberContactsTable = new Hashtable<>(numberContacts);
                doOnPhoneContactsReady(c,isBusinessPromotion);
            }
        }
    }

    @Override
    public void doTryAgain() {
        doOnPhoneContactsReady(context,isBusinessPromotion);
    }

}







