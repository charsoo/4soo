package ir.rasen.charsoo.view.interface_m;

/**
 * Created by Rasen_iman on 8/17/2015.
 */
public interface IFollowBusiness {
    void notifyFollowBusiness(String businessId) throws Exception;
    void notifyFollowBusinessFailed(Integer errorCode,String callerStringID) throws Exception;
}
