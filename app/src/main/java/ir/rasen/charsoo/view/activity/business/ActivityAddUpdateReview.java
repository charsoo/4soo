package ir.rasen.charsoo.view.activity.business;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.controller.object.Review;
import ir.rasen.charsoo.model.review.AddReview;
import ir.rasen.charsoo.model.review.DeleteReview;
import ir.rasen.charsoo.model.review.GetReviewById;
import ir.rasen.charsoo.model.review.UpdateReview;
import ir.rasen.charsoo.view.activity.ActivityCamera;
import ir.rasen.charsoo.view.activity.ActivityGallery;
import ir.rasen.charsoo.view.activity.user.ActivityUserReviews;
import ir.rasen.charsoo.view.adapter.AdapterReviewImagesGrid;
import ir.rasen.charsoo.view.adapter.AdapterReviewImagesGrid.ReviewImage;
import ir.rasen.charsoo.view.dialog.DialogDeleteReviewConfirmation;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.dialog.PopupSelectCameraGallery;
import ir.rasen.charsoo.view.interface_m.IAddReview;
import ir.rasen.charsoo.view.interface_m.IReviewChange;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.edittexts.EditTextFont;
import ir.rasen.charsoo.view.widgets.imageviews.ImageViewCircular;

public class ActivityAddUpdateReview
        extends CharsooActivity
        implements IAddReview,IWebservice,IReviewChange{
    public final static int GET_REVIEW_REQUEST = 1341;
    public final static int UPDATE_REVIEW = 100;
    public final static int DELETE_REVIEW = 200;

    public static final int MODE_ADDING = 0;
    public static final int MODE_UPDATING = 1;
    public static final int MODE_VISITING = 2;

    LayoutInflater inflater;
    RelativeLayout rlAddPicture;
    RatingBar rateBar;
    EditTextFont etReview;
    ImageViewCircular ivProfile;
    TextViewFont tvDesc,tvRateText;

    String bizUnqId;
    String reviewUnqId;

    public int openingMode;


    GridView gridReviewImages;
    AdapterReviewImagesGrid adapterReviewImages;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_update_reviews);



        setTitle("");
        inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        try {
            openingMode = getIntent().getExtras().getInt(Params.OPENING_MODE, MODE_VISITING);
        } catch (Exception e) {
            openingMode = MODE_VISITING;
        }

        try {
            bizUnqId = getIntent().getExtras().getString(Params.BUSINESS_UNIQUE_ID);

            if(bizUnqId == null || bizUnqId.equals(""))
                bizUnqId = Globals.getInstance().getValue().businessVisiting.uniqueId;
        } catch (Exception e) {
            bizUnqId = Globals.getInstance().getValue().businessVisiting.uniqueId;
        }

        gridReviewImages = (GridView) findViewById(R.id.gv_reviewImages);
        tvDesc = (TextViewFont) findViewById(R.id.tvDescription);
        tvRateText = (TextViewFont) findViewById(R.id.tv_rateText);

        ivProfile = (ImageViewCircular) findViewById(R.id.iv_profile);

        rlAddPicture = (RelativeLayout) findViewById(R.id.rl_add_picture);
        rlAddPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PopupSelectCameraGallery(ActivityAddUpdateReview.this).show();
            }
        });

        rateBar = (RatingBar) findViewById(R.id.ratingBar1);
        rateBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                setRateTextDependOnStars((int) rating);
            }
        });

        etReview = (EditTextFont) findViewById(R.id.et_review);



        adapterReviewImages =
                new AdapterReviewImagesGrid(ActivityAddUpdateReview.this,
                        null,openingMode);

        gridReviewImages.setAdapter(adapterReviewImages);

        resetReviewDetails();

        handleOpeningMode(openingMode);

    }

    private void handleOpeningMode(int openingMode) {

        switch(openingMode){
            case MODE_ADDING :
                bizUnqId =
                        getIntent().getExtras().getString(Params.BUSINESS_UNIQUE_ID);
                rlAddPicture.setVisibility(View.VISIBLE);
                break;
            case MODE_UPDATING :
                reviewUnqId =
                        getIntent().getExtras().getString(Params.REVIEW_ID,"");
                getReviewDetails(reviewUnqId);
                rlAddPicture.setVisibility(View.VISIBLE);
                break;
            case MODE_VISITING :
                reviewUnqId =
                        getIntent().getExtras().getString(Params.REVIEW_ID,"");
                getReviewDetails(reviewUnqId);
                rlAddPicture.setVisibility(View.GONE);
                break;
        }
    }

    private void setRateTextDependOnStars(int star) {
        switch (star) {
            case 0:
                tvRateText.setText("");
                break;

            case 1:
                tvRateText.setText(getResources().getString(R.string.rate_1star));
                break;

            case 2:
                tvRateText.setText(getResources().getString(R.string.rate_2star));
                break;

            case 3:
                tvRateText.setText(getResources().getString(R.string.rate_3star));
                break;

            case 4:
                tvRateText.setText(getResources().getString(R.string.rate_4star));
                break;

            case 5:
                tvRateText.setText(getResources().getString(R.string.rate_5star));
                break;
        }
    }

    private void loadUserProfilePicture(ImageView ivProfile) {
        MyApplication.loadUserProfilePicture(this, ivProfile, Image_M.LARGE, Image_M.ImageType.USER);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add_update_reviews, menu);

        switch(openingMode){
            case MODE_ADDING :
                menu.getItem(1).setVisible(true);
                menu.getItem(0).setVisible(false);
                break;
            case MODE_UPDATING :
                menu.getItem(1).setVisible(true);
                menu.getItem(0).setVisible(true);
                break;

            case MODE_VISITING :
                menu.getItem(1).setVisible(false);
                menu.getItem(0).setVisible(false);
                break;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.action_tik) {
            if (((int) rateBar.getRating()) == 0){
                new DialogMessage(ActivityAddUpdateReview.this, R.string.submit_review,
                        getString(R.string.insert_star_rate)).show();
                return true;
            }
            if(openingMode == MODE_UPDATING)
                updateReview();
            else if(openingMode == MODE_ADDING)
                sendNewReview();
            return true;
        }
        else if(item.getItemId()==R.id.action_delete){
            Review rv = new Review();
            rv.userUniqueId = LoginInfo.getInstance().getUserUniqueId();
            rv.uniqueId = reviewUnqId;
            DialogDeleteReviewConfirmation d =
                    new DialogDeleteReviewConfirmation(
                            ActivityAddUpdateReview.this, rv,ActivityAddUpdateReview.this,
                            ActivityAddUpdateReview.this,
                            DELETE_REVIEW);
            d.show();
            return true;
        }
        else
            return super.onOptionsItemSelected(item);
    }

    private void updateReview(){
        showWaitDialog();

        RequestObject.ReviewItem reviewItem =
                createReviewItem();

        new UpdateReview(this,reviewItem,ActivityAddUpdateReview.this,UPDATE_REVIEW).executeWithNewSolution();
    }
    private void sendNewReview() {
        showWaitDialog();

        RequestObject.ReviewItem newReviewItem =
                createReviewItem();

        new AddReview(this,newReviewItem,ActivityAddUpdateReview.this,
                LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

    }

    private RequestObject.ReviewItem createReviewItem() {
        RequestObject.ReviewItem revItem = new RequestObject.ReviewItem();
        revItem.BusinessId = bizUnqId;
        revItem.Rate = String.valueOf((int)rateBar.getRating());
        revItem.UserId = LoginInfo.getInstance().getUserUniqueId();
        revItem.Text = etReview.getText().toString();
        revItem.Images = getBase64ListFormPaths();
        revItem.RevId = reviewUnqId;
        return revItem;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            int reqWidth= Params.IMAGE_MAX_WIDTH_HEIGHT_PIX,reqHeight=Params.IMAGE_MAX_WIDTH_HEIGHT_PIX;

            String resizedPath = null;
            if (requestCode == ActivityCamera.CAPTURE_PHOTO) {
                resizedPath =
                        Image_M.resizeImageThenGetPath(data.getStringExtra(ActivityCamera.FILE_PATH),
                                reqWidth,reqHeight);

            } else if (requestCode == ActivityGallery.CAPTURE_GALLERY) {
                resizedPath =
                        Image_M.resizeImageThenGetPath(data.getStringExtra(ActivityGallery.FILE_PATH),
                                reqWidth, reqHeight);
            }
            adapterReviewImages.addNewImage(
                    new AdapterReviewImagesGrid.ReviewImage(
                            BitmapFactory.decodeFile(resizedPath)));
        }
    }

    public ArrayList<String> getBase64ListFormPaths() {
        ArrayList<String> base64List = new ArrayList<>();
        for(ReviewImage ri:adapterReviewImages.getAllReviewImages()){
            if(ri.isUnqId())
                base64List.add(ri.imageUnqId);
            else
                base64List.add(Image_M.getBase64FromBitmap(ri.bitmap));
        }
        return base64List;
    }

    @Override
    public void successAddReview(RequestObject.ReviewItem reviewItem) throws Exception {
        MyApplication.broadCastAddReview(reviewItem);
        hideWaitDialog();
        onBackPressed();
    }

    @Override
    public void failAddReview(int errorCode, String error) throws Exception {
        hideWaitDialog();
        Toast.makeText(getBaseContext(),"Error : " + errorCode,Toast.LENGTH_SHORT).show();
    }

    void getReviewDetails(String reviewUniqueId){
        showWaitDialog();
        new GetReviewById(this,reviewUniqueId,ActivityAddUpdateReview.this,GET_REVIEW_REQUEST).
                executeWithNewSolution();
    }

    @Override
    public void getResult(int request, Object result) throws Exception {
        hideWaitDialog();
        switch(request){
            case GET_REVIEW_REQUEST :
                RequestObject.ReviewItem ri = (RequestObject.ReviewItem) result;
                setReviewDetails(ri);
                break;

            case UPDATE_REVIEW :
                hideWaitDialog();
                onBackPressed();
                MyApplication.broadCastUpdateReview(reviewUnqId);
                break;

            case DELETE_REVIEW :
                MyApplication.broadCastDeleteReview(reviewUnqId);
                onBackPressed();
                break;
        }
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) throws Exception {
        hideWaitDialog();
        Toast.makeText(getBaseContext(),"Error : " + errorCode,Toast.LENGTH_SHORT).show();
        switch(request){
            case GET_REVIEW_REQUEST :
                break;

            case UPDATE_REVIEW :
                break;

            case DELETE_REVIEW :
                break;
        }
    }

    private void resetReviewDetails() {
        tvDesc.setText(getResources().getString(R.string.review_by) + " " + LoginInfo.getInstance().username);
        loadUserProfilePicture(ivProfile);
        setRateTextDependOnStars(0);
        rateBar.setRating(0f);
        etReview.setText("");

    }
    public void setReviewDetails(RequestObject.ReviewItem reviewDetails) {
        tvDesc.setText(getResources().getString(R.string.review_by) + " " + LoginInfo.getInstance().username);
        loadUserProfilePicture(ivProfile);
        setRateTextDependOnStars(Integer.valueOf(reviewDetails.Rate));
        rateBar.setRating(Float.valueOf(reviewDetails.Rate));
        etReview.setText(reviewDetails.Text);
        addReviewImagesUniqueId(reviewDetails.Images);
    }

    private void addReviewImagesUniqueId(ArrayList<String> imagesUniqueId) {
        for(String imageUniqueId : imagesUniqueId){
            adapterReviewImages.addNewImage(new ReviewImage(imageUniqueId));
        }
    }


    @Override
    public void notifyDeleteReview(String reviewId) throws Exception {

    }
    @Override
    public void notifyDeleteReviewFailed(String reviewIntId, String errorMessage) throws Exception {

    }
    @Override
    public void notifyUpdateReview(Review review) throws Exception {

    }
    @Override
    public void notifyUpdateReviewFailed(String reviewIntId, String errorMessage) throws Exception {

    }
}
