package ir.rasen.charsoo.view.dialog;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.object.Comment;
import ir.rasen.charsoo.view.interface_m.ICommentChange;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.WaitDialog;
import ir.rasen.charsoo.view.widgets.buttons.ButtonFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;


public class PopupWindowDeleteCommentBlockUser extends PopupWindow {

    public PopupWindowDeleteCommentBlockUser
            (final CharsooActivity charsooActivity, final String postOwnerBusinessId,
             final Comment comment, final IWebservice iWebservice,
              final ICommentChange iCommentChange, final int webserviceReqCode) {
        super(charsooActivity);

        LayoutInflater inflater = (LayoutInflater)
                charsooActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_post_business_more, null, false);
        setContentView(view);

        ((ButtonFont) view.findViewById(R.id.btn_post_more_edit)).setText(R.string.delete_comment);
        view.findViewById(R.id.btn_post_more_edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        DialogDeleteCommentConfirmation d =
                                new DialogDeleteCommentConfirmation(charsooActivity, comment, iCommentChange);
                        d.show();
                        PopupWindowDeleteCommentBlockUser.this.dismiss();
                    }
                }, Params.FREEZ);
            }
        });

        ((ButtonFont) view.findViewById(R.id.btn_post_more_delete)).setText(R.string.block_user);
        view.findViewById(R.id.btn_post_more_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        DialogBlockUserConfirmation d = new DialogBlockUserConfirmation(charsooActivity
                                , postOwnerBusinessId, comment, iWebservice, webserviceReqCode);
                        d.show();
                        PopupWindowDeleteCommentBlockUser.this.dismiss();
                    }
                }, Params.FREEZ);
            }
        });

        setBackgroundDrawable(new BitmapDrawable());
        setOutsideTouchable(true);
        setWindowLayoutMode(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
    }
}
