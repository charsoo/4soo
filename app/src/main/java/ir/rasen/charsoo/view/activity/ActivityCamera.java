package ir.rasen.charsoo.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;

import java.io.File;

import eu.janmuller.android.simplecropimage.CropImage;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.view.photocrop.*;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;


public class ActivityCamera extends CharsooActivity {

    private static String TAG = "CameraActivity";
    public static String FILE_PATH = "file_path";
    public static final int CAPTURE_PHOTO = 123;
    static final int REQUEST_CODE_UPDATE_PIC = 0x1;

    private String filePath;
    private String cameraOutputDirectory,cropOutputDirectory;

    public static String cameraOutputFileName="32543543.jpg";
    public static String tempFolderToPreventRecreation ="tempdirectorytoprevent";

    public static String getCapturedImageDirectory(){
        String temp =Environment.getExternalStorageDirectory().getAbsolutePath()+Params.CHARSOO_TEMP_FILE_SUBDIRECTORY;
        return temp;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_camera);
        cameraOutputDirectory=getCapturedImageDirectory();
        File f=new File(cameraOutputDirectory);

        if (!f.exists())
            f.mkdirs();

        if (new File(cameraOutputDirectory+cameraOutputFileName).exists()){
            if (new File(CropImage.getCroppedImageDirectory(ActivityCamera.this)+cameraOutputFileName).exists()){
                finishActivityOnOkResult();
            }
            else
            {
                runCropIntent();
            }
        }
        else{
            if (new File(cameraOutputDirectory+ tempFolderToPreventRecreation).exists()){
                finish();
            }
            else {
                new File(cameraOutputDirectory+tempFolderToPreventRecreation).mkdirs();
                takePhoto();
            }
        }
//        if(savedInstanceState!=null){
//            if (savedInstanceState.containsKey("mycamerakey")) {
//                Log.d("activity camera", "test1212");
//                finish();
//            }else
//                takePhoto();
//
//        }
//        else
//            takePhoto();

//        if (!GlobalContactPassingClass.getInstance().isPictureTaken) {
//            takePhoto();
//            GlobalContactPassingClass.getInstance().isPictureTaken=true;
//        }
    }


//    @Override
//    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
//        outState.putString("mycamerakey","");
//        Log.d("activity camera", "destroy2342");
//        super.onSaveInstanceState(outState, outPersistentState);
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        Bundle saveInstanceState=new Bundle();
//        Log.d("activity camera", "destroy2342");
////        saveInstanceState.putString("mycamerakey","");
////        ActivityCamera.this.onSaveInstanceState(saveInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }


    public void takePhoto() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        filePath= cameraOutputDirectory+ cameraOutputFileName;
        File photo = new File(filePath);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(photo));
        startActivityForResult(intent, CAPTURE_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAPTURE_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
//                    resampleTakenPhoto(Environment.getExternalStorageDirectory()+"/",Environment.getExternalStorageDirectory()+"/sffs/");
                    runCropIntent();
                }
                else{
                    finish();
                }
                break;
            case REQUEST_CODE_UPDATE_PIC:
                try {
                    if (requestCode == REQUEST_CODE_UPDATE_PIC) {
                        if (resultCode == RESULT_OK) {
                            String imagePath = data.getStringExtra(Constants.IntentExtras.IMAGE_PATH);

                            String resizedImagePath= Image_M.resizeImageThenGetPath(imagePath, Params.IMAGE_MAX_WIDTH_HEIGHT_PIX,
                                    Params.IMAGE_MAX_WIDTH_HEIGHT_PIX);
                            Intent i = getIntent();
                            i.putExtra(ActivityCamera.FILE_PATH, resizedImagePath);
                            setResult(RESULT_OK, i);
                            if (new File(cameraOutputDirectory+cameraOutputFileName).exists())
                                new File(cameraOutputDirectory+cameraOutputFileName).delete();
                            finish();
                        }else if(resultCode == RESULT_CANCELED){
                            finish();
                        }
                    }
                }catch (Exception eCrop){finish();}
                break;
        }
    }

    private void runCropIntent(){
        String action = Constants.IntentExtras.ACTION_CAMERA;

        Intent intent = new Intent(this, ir.rasen.charsoo.view.photocrop.ImageCropActivity.class);
        intent.putExtra("ACTION", action);
        intent.putExtra(Constants.IntentExtras.IMAGE_PATH, cameraOutputDirectory+cameraOutputFileName);
        startActivityForResult(intent, REQUEST_CODE_UPDATE_PIC);
    }


    private void finishActivityOnOkResult(){
        Intent i = getIntent();
        i.putExtra(ActivityCamera.FILE_PATH, getCacheDir().getAbsolutePath() + cameraOutputFileName);
        setResult(RESULT_OK, i);
//        if (new File(cameraOutputDirectory+cameraOutputFileName).exists())
//            new File(cameraOutputDirectory+cameraOutputFileName).delete();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

}