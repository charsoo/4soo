package ir.rasen.charsoo.view.shared;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;

/**
 * Created by Sina on 7/1/15.
 */
public class ScreenUtils {
    public static int convertDpToPx(Context context, float dpToConvert) {
        Resources r = context.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpToConvert, r.getDisplayMetrics());
        return (int) px;
    }
}
