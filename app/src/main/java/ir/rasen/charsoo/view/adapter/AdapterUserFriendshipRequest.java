package ir.rasen.charsoo.view.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.BaseAdapterItem;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.model.friend.AnswerRequestFriendship;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.WaitDialog;
import ir.rasen.charsoo.view.widgets.imageviews.ImageViewCircle;


/**
 * Created by android on 3/7/2015.
 */
public class AdapterUserFriendshipRequest extends BaseAdapter {

    public static final String TAG = "AdapterUserFriendshipRequest";

    private ArrayList<BaseAdapterItem> items;
    private Context context;
    SimpleLoader simpleLoader;
    ListView listView;
    ArrayList<BaseAdapterItem> acceptedUsers;
    WaitDialog progressDialog;
    DialogMessage dialogMessage;






    public static final int ANSWER_REQUEST_FRIENDSHIP_NO_REQUEST = 100;
    public static final int ANSWER_REQUEST_FRIENDSHIP_YES_REQUEST = 200;

    public ArrayList<BaseAdapterItem> getRemainingFriendRequests(){
        return items;
    }

    public AdapterUserFriendshipRequest(Context context, ArrayList<BaseAdapterItem> items,WaitDialog progressDialog) {
        this.context = context;
        this.items = items;
        simpleLoader = new SimpleLoader(context);
        acceptedUsers = new ArrayList<>();
        dialogMessage=new DialogMessage(context, "");
        this.progressDialog = progressDialog;
    }

    public void loadMore(ArrayList<BaseAdapterItem> newItem){
        this.items.addAll(newItem);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final Holder holder;

        if (listView == null) {
            listView = (ListView) viewGroup;
            listView.setSelector(new ColorDrawable(0x00ffffff));
        }

        if (view == null) {
            holder = new Holder();
            view = LayoutInflater.from(context).inflate(R.layout.item_friendship_requests, viewGroup, false);
            holder.imageViewImage = (ImageViewCircle) view.findViewById(R.id.imageView_base_adapter_item_image);
            holder.textViewUserIdentifier = (TextViewFont) view.findViewById(R.id.textView_base_adapter_item_title);
            holder.imageViewYes = (ImageView) view.findViewById(R.id.imageView_yes);
            holder.imageViewNo = (ImageView) view.findViewById(R.id.imageView_no);
            view.setTag(holder);
        } else
            holder = (Holder) view.getTag();

        holder.imageViewImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User.goUserHomeInfoPage(context,items.get(position).getId());
            }
        });
        holder.imageViewYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                answerYes(position, holder);
            }
        });
        holder.imageViewNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                answerNo(position,holder);
            }
        });
        //download image with customized class via imageId
        simpleLoader.loadImage(items.get(position).getImageId(), Image_M.SMALL, Image_M.ImageType.USER, holder.imageViewImage);
        holder.textViewUserIdentifier.setText(items.get(position).getTitle());

        return view;
    }


    private void answerNo(final int position,Holder holder) {
        progressDialog.show();
//        context.showWaitDialog();

        //TODO DECLARED FOR "3" FOR ACCEPT REQUEST
        new AnswerRequestFriendship(
                context,LoginInfo.getInstance().getUserUniqueId(), items.get(position).getId(),
                "3", new IWebservice() {
            @Override
            public void getResult(int reqCode,Object result) {
                progressDialog.dismiss();
//                context.hideWaitDialog();
                if ((boolean) result){
                    items.remove(position);
                    notifyDataSetChanged();
                }
                else{
                    if (!dialogMessage.isShowing()){
                        dialogMessage.show();
                        dialogMessage.setMessage(ServerAnswer.getError(context,ServerAnswer.NONE_DEFINED_ERROR,TAG));
                    }
                }
            }

            @Override
            public void getError(int reqCode,Integer errorCode, String callerStringID) {
                progressDialog.dismiss();
//                context.hideWaitDialog();
                if (!dialogMessage.isShowing()){
                    dialogMessage.show();
                    dialogMessage.setMessage(ServerAnswer.getError(context, errorCode, callerStringID + ">" + TAG));
                }
            }
        }, ANSWER_REQUEST_FRIENDSHIP_NO_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();


    }

    private void answerYes(final int position,final Holder holder) {
        progressDialog.show();

        //TODO DECLARED FOR "1" FOR REJECT REQUEST
        new AnswerRequestFriendship(
                context,LoginInfo.getInstance().getUserUniqueId(), items.get(position).getId(),
                "1", new IWebservice() {
            @Override
            public void getResult(int reqCode,Object result) {
                progressDialog.dismiss();
//                context.hideWaitDialog();
                if ((boolean) result){
                    holder.imageViewYes.setImageResource(R.drawable.ic_check_green);
                    holder.imageViewNo.setVisibility(View.GONE);
                    acceptedUsers.add(items.get(position));
                    new AsyncTask<Integer, Void, Void>() {
                        @Override
                        protected Void doInBackground(Integer... integers) {
                            int p=Integer.valueOf(integers[0]);
                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            // TODO:
//                            items.remove(p);
                            return null;
                        }
                        @Override
                        protected void onPostExecute(Void result) {
                            notifyDataSetChanged();
                        }
                    }.execute(position);
                }
                else{
                    if (!dialogMessage.isShowing()){
                        dialogMessage.show();
                        dialogMessage.setMessage(ServerAnswer.getError(context,ServerAnswer.NONE_DEFINED_ERROR,TAG));
                    }
                }
            }

            @Override
            public void getError(int reqCode,Integer errorCode, String callerStringID) {
                progressDialog.dismiss();
//                context.hideWaitDialog();
                if (!dialogMessage.isShowing()){
                    dialogMessage.show();
                    dialogMessage.setMessage(ServerAnswer.getError(context, errorCode, callerStringID + ">" + TAG));
                }
            }
        }, ANSWER_REQUEST_FRIENDSHIP_YES_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

    }

    public ArrayList<BaseAdapterItem> getAcceptedUsers() {
        return acceptedUsers;
    }


    private class Holder {
        ImageViewCircle imageViewImage;
        TextViewFont textViewUserIdentifier;
        ImageView imageViewYes;
        ImageView imageViewNo;
    }

}
