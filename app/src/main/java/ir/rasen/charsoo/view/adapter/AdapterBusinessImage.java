package ir.rasen.charsoo.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;

import java.util.ArrayList;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.SearchItemPost;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.controller.object.Review;
import ir.rasen.charsoo.model.business.DeleteImageFromGallery;
import ir.rasen.charsoo.model.business.Report;
import ir.rasen.charsoo.view.activity.ActivityBusinessImage;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessGallery;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IWebServiceDeleteItem;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.MaterialProgressBarCircular;
import ir.rasen.charsoo.view.widgets.MyPopUpMenu;


/**
 * Created by android on 9/5/2015.
 */
public class AdapterBusinessImage extends BaseAdapter implements IWebServiceDeleteItem {

    private Context context;
    private final ArrayList<Business> gridValues;
    SimpleLoader simpleLoader;
    MaterialProgressBarCircular progressBar;
    private ArrayList<Business> items;
    MyPopUpMenu mPopUp;
    int req_deleteimagefromgallery = 100, req_report = 200;
    DialogMessage dialogMessage;



    //Constructor to initialize values
    public AdapterBusinessImage(Context context, ArrayList<Business> gridValues) {
        this.context= context;
        this.gridValues = gridValues;
        simpleLoader = new SimpleLoader(context);
        resetItems(gridValues);
    }
    public void loadMore(ArrayList<Business> newItem){
        this.gridValues.addAll(newItem);
        notifyDataSetChanged();
    }
    public void resetItems(ArrayList<Business> newItems) {
        items = new ArrayList<>();
        for (Business business : newItems) {
            items.add(business);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        // Number of times getView method call depends upon gridValues.length
        return gridValues.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    // Number of times getView method call depends upon gridValues.length

    public View getView(final int position, View convertView, ViewGroup parent) {



        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;
        if (convertView == null) {
            gridView = new View(context);
            gridView = inflater.inflate( R.layout.business_gridimage , null);
            ImageView imageView = (ImageView) gridView
                    .findViewById(R.id.grid_item_image);
            progressBar = (MaterialProgressBarCircular) gridView.findViewById(R.id.pb_grid_post);

            String id = gridValues.get(position).profilePictureUniqueId;
            simpleLoader.loadImage(id, Image_M.SMALL,
                        Image_M.ImageType.BUSINESS, imageView, progressBar);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ActivityBusinessGallery.class);
                    intent.putExtra(Params.Gallery_Id, gridValues.get(position).galleryId);
                    context.startActivity(intent);
                }
            });
           imageView.setOnLongClickListener(new View.OnLongClickListener() {
               @Override
               public boolean onLongClick(View v) {
                   if (gridValues.get(position).isowner)
                       mPopUp = new MyPopUpMenu(context, v, R.menu.poput_menu_report_delete);
                   else
                       mPopUp = new MyPopUpMenu(context, v, R.menu.poput_menu_report);
                   mPopUp.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                       @Override
                       public boolean onMenuItemClick(MenuItem item) {
                           switch (item.getItemId()) {

                               case R.id.popup_report:
                                   new Report(context, gridValues.get(position).galleryId
                                           , AdapterBusinessImage.this, req_report, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                                   break;

                               case R.id.popup_delete:
                                   new DeleteImageFromGallery(context, gridValues.get(position).galleryId, AdapterBusinessImage.this
                                           , req_deleteimagefromgallery, LoginInfo.getInstance().userAccessToken, position).exectueWithNewSolution();
                                   break;
                           }
                           return false;
                       }
                   });
                   mPopUp.show();


                   return true;
               }
           });

            dialogMessage = new DialogMessage(context, "");
            }

                else

                {
                    gridView = (View) convertView;
                }

                return gridView;


            }

    @Override
    public void getResult(int request, Object result ,int position) throws Exception {
        if(request == req_deleteimagefromgallery){
            gridValues.remove(position);
            notifyDataSetChanged();
        }

    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID ,int position) throws Exception {
        if (!dialogMessage.isShowing()) {
            dialogMessage.show();
            dialogMessage.setMessage(callerStringID);
        }
    }
}