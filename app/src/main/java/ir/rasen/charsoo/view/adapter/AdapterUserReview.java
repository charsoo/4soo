package ir.rasen.charsoo.view.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;


import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.Review;
import ir.rasen.charsoo.view.dialog.PopupWindowEditDeleteReview;
import ir.rasen.charsoo.view.interface_m.IReviewChange;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.shared.ViewMethods;
import ir.rasen.charsoo.view.widgets.RatingBar;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.imageviews.RoundedImageView;

/**
 * Created by android on 3/7/2015.
 */
public class AdapterUserReview extends BaseAdapter{

    private ArrayList<Review> reviews;
    private CharsooActivity context;
    SimpleLoader simpleLoader;
    IReviewChange iReviewChange;
    //    WaitDialog progressDialog;
    String visitedUserId;

    IWebservice iWebservice;

    public AdapterUserReview
            (CharsooActivity charsooActivity,String visitedUserId, ArrayList<Review> reviews,
             IReviewChange iReviewChange,IWebservice iWebservice) {
        this.context = charsooActivity;
        resetItems(reviews);
        simpleLoader = new SimpleLoader(context);
        this.iReviewChange=iReviewChange;
        this.visitedUserId = visitedUserId;
        this.iWebservice = iWebservice;
    }

    public void resetItems(ArrayList<Review> newItems){
        this.reviews=new ArrayList<>();
        for (int i = 0; i < newItems.size(); i++) {
            reviews.add(newItems.get(i));
        }
        notifyDataSetChanged();
    }

    public void loadMore(ArrayList<Review> newItem){
        this.reviews.addAll(newItem);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return reviews.size();
    }

    @Override
    public Object getItem(int position) {
        return reviews.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final Holder holder;

        if (view == null) {
            holder = new Holder();
            view = LayoutInflater.from(context).inflate(R.layout.item_review_user, viewGroup, false);
            holder.imageViewImage = (RoundedImageView) view.findViewById(R.id.review_business_image);
            holder.card=(LinearLayout)view.findViewById(R.id.card);
            holder.textViewIdentifier = (TextViewFont) view.findViewById(R.id.review_user);
            holder.textViewText = (TextViewFont) view.findViewById(R.id.review_text);
            holder.ratingBar = (RatingBar) view.findViewById(R.id.review_rate);
            holder.imgMore = (ImageView)view.findViewById(R.id.review_more);
            view.setTag(holder);
        } else
            holder = (Holder) view.getTag();

        //download business profile picture with customized class via imageId
        simpleLoader.loadImage(reviews.get(position).businessPicutreUniqueId, Image_M.LARGE, Image_M.ImageType.BUSINESS, holder.imageViewImage, reviews.get(position).userName);
        holder.textViewIdentifier.setText(reviews.get(position).businessUserName);
        holder.textViewText.setText(reviews.get(position).text);
        holder.ratingBar.setScore(reviews.get(position).rate);

        if(visitedUserId.equals(LoginInfo.getInstance().getUserUniqueId()))
            holder.imgMore.setVisibility(View.VISIBLE);
        else
            holder.imgMore.setVisibility(View.GONE);
        holder.imgMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupWindowEditDeleteReview p = new PopupWindowEditDeleteReview(context,reviews.get(position),iWebservice,iReviewChange);
                p.showAsDropDown(view);
            }
        });
        holder.imgMore.setOnTouchListener(ViewMethods.onTouchListenerGrayCircle);
        holder.card.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Business.gotoBusinessOtherPage(context, reviews.get(position).businessUniqueId);
            }
        });
        holder.textViewIdentifier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //GetBusinessHomeInfo via comment.businessUniqueId
                Business.gotoBusinessOtherPage(context, reviews.get(position).businessUniqueId);
            }
        });
        return view;
    }


    private class Holder {
        ImageView imageViewImage;
        TextViewFont textViewIdentifier;
        TextViewFont textViewText;
        RatingBar ratingBar;
        ImageView imgMore;
        LinearLayout card;
    }


}
