package ir.rasen.charsoo.view.widgets;


        import android.app.Activity;
        import android.content.Context;
        import android.content.res.TypedArray;
        import android.graphics.Point;
        import android.os.Build;
        import android.util.AttributeSet;
        import android.view.Display;
        import android.view.View;
        import android.view.WindowManager;
        import android.view.animation.Animation;
        import android.view.animation.Transformation;
        import android.widget.RelativeLayout;

        import ir.rasen.charsoo.R;

/**
 * Created by 'Sina KH' on 7/6/2015.
 */
public class ExpandableView extends RelativeLayout {

    private static int IMPORTANT_DIFF = 100;

    private AttributeSet attrs;

    private boolean mExpanded = false;
    private int mCollapsedHeight = 0;
    private int mContentHeight = 0;
    private int mAnimationDuration = 0;
    private int screenWidth = 0;
    private int screenHeight = 0;

    private View expand;
    //private ImageViewArrow expandArrow;

    private OnExpandListener mListener;

    public ExpandableView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ExpandableView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.attrs = attrs;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if(mContentHeight==0) {
            mContentHeight = getMeasuredHeight();
            init(getContext(), attrs);
        }
    }

    private void init(Context context, AttributeSet attrs) {

        expand = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.layout_expand, null);
        addView(expand);
        LayoutParams expandLayoutParams = (LayoutParams) expand.getLayoutParams();
        expandLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        expand.setLayoutParams(expandLayoutParams);
        //expandArrow = (ImageViewArrow) expand.findViewById(R.id.arrow);

        mListener = new DefaultOnExpandListener();
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ExpandableView, 0, 0);

        measureWindowSize(context);

        // How high the content should be in "collapsed" state
        mCollapsedHeight = (int) a.getDimension(R.styleable.ExpandableView_collapsedHeightt, (float) (screenWidth*0.4));

        if(mCollapsedHeight+IMPORTANT_DIFF<mContentHeight) {
            Animation anim;
            anim = new ExpandAnimation(mContentHeight, mCollapsedHeight);
            anim.setDuration(0);
            startAnimation(anim);
            expand.setVisibility(VISIBLE);
            //expandArrow.setExpanded(false);

            // How long the animation should take
            mAnimationDuration = a.getInteger(R.styleable.ExpandableView_animationDurationn, 500);
            setOnClickListener(new PanelToggle());
        } else {
            expand.setVisibility(GONE);
        }

        a.recycle();
    }

    public void setOnExpandListener(OnExpandListener listener) {
        mListener = listener;
    }

    public void setCollapsedHeight(int collapsedHeight) {
        mCollapsedHeight = collapsedHeight;
    }

    public void setAnimationDuration(int animationDuration) {
        mAnimationDuration = animationDuration;
    }

    private class PanelToggle implements OnClickListener {
        public void onClick(View v) {
            Animation a;
            if (mExpanded) {
                a = new ExpandAnimation(mContentHeight, mCollapsedHeight);
                mListener.onCollapse();
                //expandArrow.setExpanded(false);
                expand.setVisibility(VISIBLE);
            } else {
                a = new ExpandAnimation(mCollapsedHeight, mContentHeight);
                mListener.onExpand();
                //expandArrow.setExpanded(true);
                expand.setVisibility(GONE);
            }
            a.setDuration(mAnimationDuration);
            if (getLayoutParams().height == 0) { // Need to do this or
                // else the
                // animation
                // will not play if
                // the height is 0
                android.view.ViewGroup.LayoutParams lp = getLayoutParams();
                lp.height = 1;
                setLayoutParams(lp);
                requestLayout();
            }
            startAnimation(a);
            mExpanded = !mExpanded;
        }
    }

    private class ExpandAnimation extends Animation {
        private final int mStartHeight;
        private final int mDeltaHeight;

        public ExpandAnimation(int startHeight, int endHeight) {
            mStartHeight = startHeight;
            mDeltaHeight = endHeight - startHeight;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            android.view.ViewGroup.LayoutParams lp = getLayoutParams();
            lp.height = (int) (mStartHeight + mDeltaHeight * interpolatedTime);
            setLayoutParams(lp);
        }

        @Override
        public boolean willChangeBounds() {
            return true;
        }
    }

    private void measureWindowSize(Context context) {

        WindowManager w = ((Activity) context).getWindowManager();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            Point size = new Point();
            w.getDefaultDisplay().getSize(size);
            screenWidth = size.x;
            screenHeight = size.y;
        } else {
            Display d = w.getDefaultDisplay();
            screenWidth = d.getWidth();
            screenHeight = d.getHeight();
        }
    }

    public interface OnExpandListener {
        public void onExpand();

        public void onCollapse();
    }

    private class DefaultOnExpandListener implements OnExpandListener {
        public void onCollapse() {
        }

        public void onExpand() {
        }
    }
}