package ir.rasen.charsoo.view.dialog;

import android.content.Context;
import android.view.View;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.object.Review;
import ir.rasen.charsoo.model.review.DeleteReview;
import ir.rasen.charsoo.view.interface_m.IReviewChange;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.WaitDialog;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.material_library.widgets.Dialog;


public class DialogDeleteReviewConfirmation extends Dialog {
    Context context;

    public DialogDeleteReviewConfirmation
            (final CharsooActivity charsooActivity, final Review review,
             final IWebservice iWebserviceResponse, final IReviewChange iReviewChange,
             final int webserviceReqCode
            ) {
        super(charsooActivity, charsooActivity.getResources().getString(R.string.delete),
                charsooActivity.getResources().getString(R.string.confirmation_delete_review));
        this.context = charsooActivity;

        addCancelButton(R.string.cancel);
        setAcceptText(R.string.delete);

        //set onClickListener for the ok button
        setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                charsooActivity.showWaitDialog();
                new DeleteReview(context,review, iWebserviceResponse, iReviewChange,webserviceReqCode, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;

                dismiss();
            }
        });

    }

}
