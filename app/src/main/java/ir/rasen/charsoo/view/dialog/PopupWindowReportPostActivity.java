package ir.rasen.charsoo.view.dialog;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.model.post.Report;
import ir.rasen.charsoo.view.interface_m.IReportPost;
import ir.rasen.charsoo.view.interface_m.IWebservice;


public class PopupWindowReportPostActivity extends PopupWindow implements  IWebservice {
    Context context;
    private IReportPost iReportPost;
    private ImageView imageViewMore;

    private static String RETURN_ID = "0";

    public static final int SHARE_REQUEST = 100;

    public PopupWindowReportPostActivity(final Context context, final String userId, final String postId, ImageView imageViewMore, final IReportPost iReportPost) {
        super(context);

        this.iReportPost = iReportPost;
        this.imageViewMore = imageViewMore;

        this.context = context;
        this.iReportPost = iReportPost;

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_post_report_cancel_share_more, null, false);
        setContentView(view);

        view.findViewById(R.id.btn_post_more_cancel_share).setVisibility(View.GONE);
        View textViewReport = view.findViewById(R.id.btn_post_more_report);

        textViewReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Report(context,userId,postId,PopupWindowReportPostActivity.this,SHARE_REQUEST, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
                Toast.makeText(context, R.string.TAKHALOF, Toast.LENGTH_SHORT).show();
                PopupWindowReportPostActivity.this.dismiss();

            }
        });
        setBackgroundDrawable(new BitmapDrawable());
        setOutsideTouchable(true);
        setWindowLayoutMode(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

    }


    @Override
    public void getResult(int reqCode,Object result) {
        iReportPost.notifyReportPost(RETURN_ID,imageViewMore);
    }

    @Override
    public void getError(int reqCode, Integer errorCode,String callerStringID) {

    }
}
