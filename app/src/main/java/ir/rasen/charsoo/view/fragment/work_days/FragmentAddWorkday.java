package ir.rasen.charsoo.view.fragment.work_days;

import android.app.Fragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.WorkDays;
import ir.rasen.charsoo.view.activity.business.ActivityWorkdays;
import ir.rasen.charsoo.view.widgets.checkbox.CheckBox;
import ir.rasen.charsoo.view.widgets.edittexts.EditTextFont;

/**
 * Created by Rasen_iman on 9/3/2015.
 */
public class FragmentAddWorkday
        extends Fragment
        implements View.OnClickListener{

    final int OPEN = 0;
    final int CLOSE = 1;


    ArrayList<CheckBox> checkBoxes;
    ArrayList<View> clickableBoxeArea;

    EditTextFont etOpenTime,etCloseTime;
    View openClickableArea,closeClickavleArea;

    ActivityWorkdays mActivityWorkdays;


    int selectedOHT = 0;
    int selectedOMT = 0;
    int selectedCHT = 0;
    int selectedCMT = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_workday,null,false);

        init(v);

        return v;
    }

    private void init(View v) {
        checkBoxes = new ArrayList<>();
        clickableBoxeArea = new ArrayList<>();

        checkBoxes.add((CheckBox) v.findViewById(R.id.cb_day0));
        checkBoxes.add((CheckBox) v.findViewById(R.id.cb_day1));
        checkBoxes.add((CheckBox) v.findViewById(R.id.cb_day2));
        checkBoxes.add((CheckBox) v.findViewById(R.id.cb_day3));
        checkBoxes.add((CheckBox) v.findViewById(R.id.cb_day4));
        checkBoxes.add((CheckBox) v.findViewById(R.id.cb_day5));
        checkBoxes.add((CheckBox) v.findViewById(R.id.cb_day6));

        clickableBoxeArea.add(v.findViewById(R.id.rl_day0));
        clickableBoxeArea.add(v.findViewById(R.id.rl_day1));
        clickableBoxeArea.add(v.findViewById(R.id.rl_day2));
        clickableBoxeArea.add(v.findViewById(R.id.rl_day3));
        clickableBoxeArea.add(v.findViewById(R.id.rl_day4));
        clickableBoxeArea.add(v.findViewById(R.id.rl_day5));
        clickableBoxeArea.add(v.findViewById(R.id.rl_day6));

        for(int i=0;i<clickableBoxeArea.size();i++){
            clickableBoxeArea.get(i).setOnClickListener(this);
        }

        etOpenTime = (EditTextFont) v.findViewById(R.id.et_open_time);
        etOpenTime.setOnClickListener(this);

        etCloseTime = (EditTextFont) v.findViewById(R.id.et_close_time);
        etCloseTime.setOnClickListener(this);

        openClickableArea = v.findViewById(R.id.ll_work_open_time);
        openClickableArea.setOnClickListener(this);

        closeClickavleArea = v.findViewById(R.id.ll_work_close_time);
        closeClickavleArea.setOnClickListener(this);

        mActivityWorkdays = (ActivityWorkdays) getActivity();

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                handleDisablingEditBoxs();

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.rl_day0 :
                handleClickCBox(checkBoxes.get(0));
                break;
            case R.id.rl_day1 :
                handleClickCBox(checkBoxes.get(1));
                break;
            case R.id.rl_day2 :
                handleClickCBox(checkBoxes.get(2));
                break;
            case R.id.rl_day3 :
                handleClickCBox(checkBoxes.get(3));
                break;
            case R.id.rl_day4 :
                handleClickCBox(checkBoxes.get(4));
                break;
            case R.id.rl_day5 :
                handleClickCBox(checkBoxes.get(5));
                break;
            case R.id.rl_day6 :
                handleClickCBox(checkBoxes.get(6));
                break;
            case R.id.et_open_time :
            case R.id.ll_work_open_time :
                openTimePickerDialog(OPEN);
                break;
            case R.id.et_close_time:
            case R.id.ll_work_close_time :
                openTimePickerDialog(CLOSE);
                break;
        }
    }

    private void handleClickCBox(CheckBox checkBox) {
        if(checkBox.isCheck())
            checkBox.setChecked(false);
        else
            checkBox.setChecked(true);
    }

    private void openTimePickerDialog(int r) {
        int hour;
        int minute;
        TimePickerDialog mTimePicker;
        switch (r){
            case OPEN :
                hour = selectedOHT;
                minute = selectedOMT;
                mTimePicker = new TimePickerDialog(mActivityWorkdays, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        setOpenTime(hourOfDay,minute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle(mActivityWorkdays.getResources().getString(R.string.work_time));
                mTimePicker.show();
                break;

            case CLOSE :
                hour = selectedCHT;
                minute = selectedCMT;
                mTimePicker = new TimePickerDialog(mActivityWorkdays, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        setCloseTime(hourOfDay,minute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle(mActivityWorkdays.getResources().getString(R.string.work_time));
                mTimePicker.show();
                break;
        }

    }

    private void setOpenTime(int hourOfDay, int minute) {
        selectedOHT = hourOfDay;
        selectedOMT = minute;
        etOpenTime.setText(selectedOHT + ":" + selectedOMT);
    }

    private void setCloseTime(int hourOfDay, int minute) {
        selectedCHT = hourOfDay;
        selectedCMT = minute;
        etCloseTime.setText(selectedCHT + ":" + selectedCMT);
    }

    void handleDisablingEditBoxs(){
        ArrayList<WorkDays.DayTime> days =
                mActivityWorkdays.mWorkDays.getDayTimes();

        for(int i=0;i<days.size();i++){
            if(days.get(i).isSet()){
                checkBoxes.get(i).setCheckedWithoutAnimation(true);
//                checkBoxes.get(i).setEnabled(false);
            }else{
                checkBoxes.get(i).setCheckedWithoutAnimation(false);
//                checkBoxes.get(i).setEnabled(true);
            }
        }
    }

    public WorkDays remakeWorkDays(WorkDays mWorkDays) {
        for(int i=0;i<checkBoxes.size();i++){
            if(checkBoxes.get(i).isCheck() && checkBoxes.get(i).isEnabled()){
                mWorkDays.getDayTimes().get(i).mOpenTime.mHourTime =
                        selectedOHT;

                mWorkDays.getDayTimes().get(i).mOpenTime.mMinuteTime =
                        selectedOMT;

                mWorkDays.getDayTimes().get(i).mCloseTime.mHourTime =
                        selectedCHT;

                mWorkDays.getDayTimes().get(i).mCloseTime.mMinuteTime =
                        selectedCMT;
            }
        }
        return mWorkDays;
    }

    public void resetTimes(){
        selectedOHT = 0;
        selectedOMT = 0;
        selectedCHT = 0;
        selectedCMT = 0;
        setOpenTime(0,0);
        setCloseTime(0,0);
    }
    public boolean validation(){
        boolean check=true;
        if(selectedOHT==selectedOMT && selectedCHT==selectedCMT) check=false;
        if(selectedCHT<selectedOHT)check=false;
        if(selectedCHT==selectedOHT && selectedCMT<=selectedOMT)  check=false;

        return check;
    }
}
