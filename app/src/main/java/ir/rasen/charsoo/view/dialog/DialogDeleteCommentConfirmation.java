package ir.rasen.charsoo.view.dialog;

import android.view.View;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.object.Comment;
import ir.rasen.charsoo.model.comment.DeleteComment;
import ir.rasen.charsoo.view.interface_m.ICommentChange;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.material_library.widgets.Dialog;


public class DialogDeleteCommentConfirmation extends Dialog {


    public DialogDeleteCommentConfirmation
            (final CharsooActivity charsooActivity, final Comment comment
                    , final ICommentChange iCommentChange) {
        super(charsooActivity, charsooActivity.getResources().getString(R.string.delete),
                charsooActivity.getResources().getString(R.string.confirmation_delete_comment));

        addCancelButton(R.string.cancel);
        setAcceptText(R.string.delete);

        //set onClickListener for the ok button
        setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                charsooActivity.showWaitDialog();
                new DeleteComment(charsooActivity,comment.ownerUniqueId, comment.uniqueId,comment.isUserSendedComment,
                        iCommentChange, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                dismiss();
            }
        });

    }

}
