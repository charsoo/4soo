package ir.rasen.charsoo.view.activity.user;

import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.flurry.android.FlurryAgent;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import java.util.logging.Handler;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.SharedPrefHandler;
import ir.rasen.charsoo.controller.helper.Validation;
import ir.rasen.charsoo.model.user.UpdatePassword;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.edittexts.EditTextFont;

public class ActivityUserChangePassword extends CharsooActivity implements IWebservice {

    private static final int UPDATE_PASSWORD_REQUEST = 14;

    EditTextFont editTextOldPassword, editTextNewPassword, editTextRepeatPassword;

    public Handler handler;

    public String userOldPassword = "";
    public String userNewPassword = "";
    public String userRetypedNewPassword = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.activity_change_password);

        setTitle(getString(R.string.login));

        editTextOldPassword = (EditTextFont) findViewById(R.id.edt_oldPassword);
        editTextNewPassword = (EditTextFont) findViewById(R.id.edt_newPassword);
        editTextRepeatPassword = (EditTextFont) findViewById(R.id.edt_repeatPassword);

    }

    @Override
    public void onBackPressed() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        finish();
    }

    public void back(View view) {
        onBackPressed();
    }

    public void submit(View view) {

        if(!Validation.validatePassword(this,editTextOldPassword.getText().toString()).isValid()) {
            editTextOldPassword.setError(Validation.getErrorMessage());
            return;
        }
        if(!Validation.validatePassword(this,editTextNewPassword.getText().toString()).isValid()){
            editTextNewPassword.setError(Validation.getErrorMessage());
            return;
        }
        if (!Validation.validateRepeatPassword(this, editTextNewPassword.getText().toString(),
                editTextRepeatPassword.getText().toString()).isValid()) {
            editTextRepeatPassword.setError(Validation.getErrorMessage());
            return;
        }

        userOldPassword = editTextOldPassword.getText().toString();
        userNewPassword = editTextNewPassword.getText().toString();
        userRetypedNewPassword = editTextRepeatPassword.getText().toString();

        if(isNetworkAvailable()){
            if(userNewPassword.equals(userRetypedNewPassword)){
                showWaitDialog();
                new UpdatePassword(ActivityUserChangePassword.this,userOldPassword,
                        editTextNewPassword.getText().toString(),
                        ActivityUserChangePassword.this, UPDATE_PASSWORD_REQUEST, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
            }
            else{
                // khataye ramze eshtebah bedahad
                SnackbarManager.show(
                        Snackbar.with(this)
                                .text("رمز عبور وارد شده صحیح نیست.")
                                .color(Color.parseColor("#3f51b5"))
                                .animation(true)
                );
            }

        }
        else{
            // show snack bar
            SnackbarManager.show(
                    Snackbar.with(this)
                            .text("خطای اتصال به اینترنت")
                            .color(Color.parseColor("#3f51b5"))
                            .animation(true));
        }



    }

    @Override
    public void getResult(final int request, Object result) {
                try {
                    if(request== UPDATE_PASSWORD_REQUEST) {
                        // namayeshe dialoge moafaghiat amiz
                        SnackbarManager.show(
                                Snackbar.with(this)
                                        .text("رمز عبور با موفقیت تغییر یافت.")
                                        .color(Color.parseColor("#3f51b5"))
                                        .animation(true));
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Intent i = new Intent();
                                i.putExtra(Params.PASSWORD_NEW,userNewPassword);
                                setResult(RESULT_OK, i);
                                myfinish();
                                SharedPrefHandler.saveUserEmailPassword(ActivityUserChangePassword.this,
                                        null,userNewPassword);
                            }
                        });

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
        try {
            hideWaitDialog();
            new DialogMessage(ActivityUserChangePassword.this, ServerAnswer.getError(ActivityUserChangePassword.this, errorCode,callerStringID+">"+this.getLocalClassName())).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public  void myfinish(){
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {

                    sleep(1000);
                    finish();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        thread.start();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(ActivityUserChangePassword.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
