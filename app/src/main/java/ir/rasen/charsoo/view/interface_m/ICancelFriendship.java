package ir.rasen.charsoo.view.interface_m;

/**
 * Created by android on 3/10/2015.
 */
public interface ICancelFriendship {
    public void notifyDeleteFriend(String userId) throws Exception;
    public void notifyDeleteFriendFailed(String failureMessage) throws Exception;
}
