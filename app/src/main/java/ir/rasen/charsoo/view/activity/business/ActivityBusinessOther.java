package ir.rasen.charsoo.view.activity.business;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.PullToRefreshGrid;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.business.GetBusinessHomeInfo;
import ir.rasen.charsoo.model.post.GetBusinessPosts;
import ir.rasen.charsoo.model.post.GetPost;
import ir.rasen.charsoo.view.activity.ActivityMain;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IPostUpdateListener;
import ir.rasen.charsoo.view.interface_m.IPullToRefresh;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.shared.GridViewBusinessOtherBothView;
import ir.rasen.charsoo.view.shared.PostInitializerBothView;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.HFGridView;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.PullToRefreshGridViewWithHeaderAndFooter;

public class ActivityBusinessOther
        extends CharsooActivity
        implements IWebservice, IPullToRefresh,NetworkStateChangeListener,
        TryAgainListener,IPostUpdateListener {

    public static final String TAG="ActivityBusinessOther";

    private static final int GET_BUSINESS_POSTS_INT_CODE=11,GET_BUSINESS_HOME_INFO_INT_CODE=14;
    private int lastFailedWebservice;

    private enum Status {FIRST_TIME, LOADING_MORE, REFRESHING, NONE}
    Status status;

    //    WaitDialog progressDialog;
    String selectedBusinessId;
    private HFGridView gridView;
    Business business;
    GridViewBusinessOtherBothView gridViewBusiness;
    ArrayList<Post> posts;
    DialogMessage dialogMessage;
    RelativeLayout networkErrorLayout;

    Context activityContext;




    public static IPostUpdateListener iPostUpdateListener;

    public static final int GET_SINGLE_POST_INT_CODE=18;


    //pull_to_refresh_lib
    PullToRefreshGrid pullToRefreshGridView;

    ImageView imageViewBack,imageViewSearch;
    RelativeLayout actionBarLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.activity_business_other);
        iPostUpdateListener = ActivityBusinessOther.this;
        networkErrorLayout = MyApplication.initNetworkErrorLayout(getWindow().getDecorView(),
                this, ActivityBusinessOther.this);

        actionBarLayout = (RelativeLayout) findViewById(R.id.ll_action_bar);
        actionBarLayout.setOnClickListener(null);
        imageViewBack = (ImageView) findViewById(R.id.imageView_back);
        imageViewSearch = (ImageView) findViewById(R.id.imageView_search);

        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        imageViewSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityMain.openThisActivityAndGotoFragmentSearch(ActivityBusinessOther.this);
            }
        });

        selectedBusinessId = getIntent().getExtras().getString(Params.BUSINESS_ID_STRING);

//        progressDialog = new WaitDialog(this);
//        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        dialogMessage=new DialogMessage(this,"");

        pullToRefreshGridView = new PullToRefreshGrid(ActivityBusinessOther.this, (PullToRefreshGridViewWithHeaderAndFooter) findViewById(R.id.gridView_HF), ActivityBusinessOther.this);
        gridView = pullToRefreshGridView.getGridViewHeaderFooter();

        status=Status.FIRST_TIME;
        callWebservice(GET_BUSINESS_HOME_INFO_INT_CODE,true);

        activityContext = this;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == Params.ACTION_ADD_POST) {
                gridViewBusiness.notifyDataSetChanged(((MyApplication) getApplication()).post);
            }
        }

    }

    @Override
    public void notifyRefresh() {
        if (posts == null)
            posts=new ArrayList<>();
        status = Status.REFRESHING;
        callWebservice(GET_BUSINESS_HOME_INFO_INT_CODE, false);
    }

    @Override
    public void notifyLoadMore() {
        status=Status.LOADING_MORE;
        callWebservice(GET_BUSINESS_POSTS_INT_CODE,false);
    }

    //    IWebservice
    @Override
    public void getResult(int request, Object result) {
//        progressDialog.dismiss();
        hideWaitDialog();
        switch (request){
            case GET_BUSINESS_HOME_INFO_INT_CODE:
                gridView.setVisibility(View.VISIBLE);
                business = (Business) result;

                Globals.getInstance().getValue().businessVisiting = (Business) result;

                boolean beThreeColumn = gridViewBusiness == null || gridViewBusiness.isThreeColumn;
                if (gridViewBusiness==null) {
                    gridViewBusiness = new GridViewBusinessOtherBothView(ActivityBusinessOther.this, business,
                            gridView,ActivityBusinessOther.TAG,ActivityBusinessOther.this);
                    gridViewBusiness.InitialGridViewBusiness(new ArrayList<Post>(), beThreeColumn);
                }
                else
                    gridViewBusiness.refreshBusinessData(business);
                if (posts==null)
                    posts=new ArrayList<>();
                callWebservice(GET_BUSINESS_POSTS_INT_CODE,false);
                break;
            case GET_BUSINESS_POSTS_INT_CODE:
                if (status==Status.FIRST_TIME || status==Status.REFRESHING){
                    posts = new ArrayList<> ((ArrayList<Post>) result);
                    pullToRefreshGridView.setResultSize(posts.size());
                    if (pullToRefreshGridView.isRefreshing()) {
                        pullToRefreshGridView.onRefreshComplete();
                    }
                    gridViewBusiness.InitialGridViewBusiness(posts, gridViewBusiness.isThreeColumn);
                }
                else if (status==Status.LOADING_MORE){
                    posts.addAll((ArrayList<Post>) result);
                    gridViewBusiness.addMorePosts((ArrayList<Post>) result);
                }
                status=Status.NONE;
                break;

            case GET_SINGLE_POST_INT_CODE:
                Post newPost = (Post) result;
                for (int i = 0; i < posts.size() ; i++) {
                    if (posts.get(i).uniqueId.equals(newPost.uniqueId)){
                        posts.set(i,MyApplication.mergeTwoPost(posts.get(i),newPost));
                        gridViewBusiness.resetPostItems(posts);
                    }
                }
                break;

        }
        networkErrorLayout.setVisibility(View.GONE);
        NetworkConnectivityReciever.removeIfHaveListener(TAG);
    }

    //    IWebservice
    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
        try {
//            progressDialog.dismiss();
            hideWaitDialog();
            if (pullToRefreshGridView.isRefreshing())
                pullToRefreshGridView.onRefreshComplete();
            if (errorCode == ServerAnswer.NETWORK_CONNECTION_ERROR) {
                if (request == GET_BUSINESS_HOME_INFO_INT_CODE)
                    networkErrorLayout.setVisibility(View.VISIBLE);
                lastFailedWebservice = request;
                NetworkConnectivityReciever.setNetworkStateListener(TAG, ActivityBusinessOther.this);
            } else if (!dialogMessage.isShowing()) {
                dialogMessage.show();
                dialogMessage.setMessage(ServerAnswer.getError(ActivityBusinessOther.this, errorCode, callerStringID + ">" + this.getLocalClassName()));
            }
        }catch (Exception e){}
    }

    @Override
    public void doTryAgain() {
        callWebservice(lastFailedWebservice, true);
    }

    @Override
    public void doOnNetworkConnected() {
        callWebservice(lastFailedWebservice,true);
    }

    private void callWebservice(int reqCode,boolean showProgressDialog){
        if (showProgressDialog)
            showWaitDialog();

        switch (reqCode) {
            case GET_BUSINESS_HOME_INFO_INT_CODE:
                new GetBusinessHomeInfo(ActivityBusinessOther.this, selectedBusinessId,
                        LoginInfo.getInstance().getUserUniqueId(), ActivityBusinessOther.this,
                        GET_BUSINESS_HOME_INFO_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                break;
            case GET_BUSINESS_POSTS_INT_CODE:
                if (status!=Status.LOADING_MORE)
                    new GetBusinessPosts(ActivityBusinessOther.this,LoginInfo.getInstance().getUserUniqueId(),
                            business.uniqueId, 0, getResources().getInteger(R.integer.lazy_load_limitation),
                            ActivityBusinessOther.this,GET_BUSINESS_POSTS_INT_CODE,business.profilePictureUniqueId,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                else
                    new GetBusinessPosts(ActivityBusinessOther.this,LoginInfo.getInstance().getUserUniqueId(),
                            business.uniqueId, gridViewBusiness.adapterPostGridAndList.getCount(), getResources().getInteger(R.integer.lazy_load_limitation),
                            ActivityBusinessOther.this,GET_BUSINESS_POSTS_INT_CODE,business.profilePictureUniqueId,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                break;
        }
    }

    @Override
    public void onPostDetailsChange(String dirtyPostId) {
        for (int i = 0; i < posts.size(); i++) {
            if (posts.get(i).uniqueId.equals(dirtyPostId)){
                new GetPost(activityContext,LoginInfo.getInstance().getUserUniqueId(),
                        posts.get(i).businessUniqueId,dirtyPostId, Post.GetPostType.BUSINESS,
                        ActivityBusinessOther.this,GET_SINGLE_POST_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
                break;
            }
        }

    }


    @Override
    public void onBackPressed() {
        if(PostInitializerBothView.dismissAllPopupsIfAnyShowing())
            return;

        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this,"2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
