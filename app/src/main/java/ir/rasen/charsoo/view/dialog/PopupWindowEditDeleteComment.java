package ir.rasen.charsoo.view.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.object.Comment;
import ir.rasen.charsoo.view.interface_m.ICommentChange;
import ir.rasen.charsoo.view.widgets.WaitDialog;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;


public class PopupWindowEditDeleteComment extends PopupWindow {
    CharsooActivity charsooActivity;
    public View view;
//    public View myview;
    static boolean cx;
    ICommentChange iCommentChange;

    WaitDialog progressDialog;
    @SuppressLint("NewApi")
    public PopupWindowEditDeleteComment(CharsooActivity charsooActivity,Comment comment, final ICommentChange iCommentChange) {
        super(charsooActivity);

        LayoutInflater inflater = (LayoutInflater)
                charsooActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.popup_post_business_more, null, false);
        this.charsooActivity=charsooActivity;
//        view.setVisibility(View.GONE);
        this.iCommentChange=iCommentChange;
//        myview=view;
        setContentView(view);
        setViewOnClick(this.charsooActivity,comment);
        setBackgroundDrawable(new BitmapDrawable());
        setOutsideTouchable(true);
        setWindowLayoutMode(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        this.progressDialog = progressDialog;

    }


    public void setViewOnClick(final Context context,final Comment comment){
        view.findViewById(R.id.btn_post_more_edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        DialogEditComment d = new DialogEditComment(charsooActivity, comment, iCommentChange);
                        d.show();
                        PopupWindowEditDeleteComment.this.dismiss();
                    }
                }, Params.FREEZ);
            }
        });
        view.findViewById(R.id.btn_post_more_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        DialogDeleteCommentConfirmation d = new DialogDeleteCommentConfirmation(charsooActivity, comment, iCommentChange);
                        d.show();
                        PopupWindowEditDeleteComment.this.dismiss();
                    }
                }, Params.FREEZ);
            }
        });


    }


}