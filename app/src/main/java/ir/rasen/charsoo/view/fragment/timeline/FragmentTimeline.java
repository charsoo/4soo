package ir.rasen.charsoo.view.fragment.timeline;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.view.adapter.AdapterTimeLineTabs;
import ir.rasen.charsoo.view.widgets.PagerSlidingTabStrip;

/**
 * Created by Rasen_iman(harekat sooski) on 8/12/2015.
 */
public class FragmentTimeline extends Fragment{

    private PagerSlidingTabStrip mPagerSlidingTabStrip;
    private ViewPager mViewPager;

    AdapterTimeLineTabs mAdapterTimeLineTabs;

    Fragment currentFragment;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        CustomActivityOnCrash.install(getActivity());

        View convertView = inflater.inflate(R.layout.fragment_timeline_layout,null);
        PagerSlidingTabStrip tabs=(PagerSlidingTabStrip)convertView.findViewById(R.id.tabs);
//
        tabs.setBackgroundColor(Color.parseColor("#3f51b5"));
        init(convertView);
        return convertView;
    }

    private void init(View view) {
        mPagerSlidingTabStrip = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);

        mAdapterTimeLineTabs = new AdapterTimeLineTabs(getChildFragmentManager(),getActivity());

        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        mViewPager.setAdapter(mAdapterTimeLineTabs);
        mViewPager.setOffscreenPageLimit(2);


        mPagerSlidingTabStrip.setViewPager(mViewPager);

        mPagerSlidingTabStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        FragmentTimelineAllPosts mFragmentTimelineAllPosts =
                                (FragmentTimelineAllPosts) mAdapterTimeLineTabs.getItem(0);

                        currentFragment = mFragmentTimelineAllPosts;
                        break;
                    case 1:
                        FragmentTimelineFollowingBusinessPosts mFragmentTimelineFollowingBusinessPosts =
                                (FragmentTimelineFollowingBusinessPosts) mAdapterTimeLineTabs.getItem(1);

                        currentFragment = mFragmentTimelineFollowingBusinessPosts;
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        mViewPager.setCurrentItem(0);
    }
}
