package ir.rasen.charsoo.view.widgets.charsoo_activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.view.activity.ActivityBusinessImage;
import ir.rasen.charsoo.view.activity.business.ActivityAddUpdateReview;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessBlockedUsers;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.widgets.TextViewFontActionBarTitle;
import ir.rasen.charsoo.view.widgets.WaitDialog;
// testing push......
/**
 * Created by Sina on 5/21/15.
 */
public class CharsooActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private boolean actionBarSet = false, finishTransition=true;
    private WaitDialog waitDialog;

    Toast toast;

    DialogMessage dialogMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        customizeActionbar();

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN |
                        WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        waitDialog = new WaitDialog(this);
        waitDialog.setMessage(getResources().getString(R.string.please_wait));


        toast = Toast.makeText(CharsooActivity.this, getString(R.string.txt_toast_no_connection), Toast.LENGTH_SHORT);

        dialogMessage = new DialogMessage(CharsooActivity.this,"");
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflator.inflate(R.layout.layout_action_bar_home, null);
            ((TextViewFontActionBarTitle) v.findViewById(R.id.textView_title)).setText(title);
            actionBar.setCustomView(v);
        }

    }

    @Override
    public void setTitle(int titleId) {
        super.setTitle(titleId);

        setTitle(getString(titleId));
    }

    @Override
    public void setSupportActionBar(Toolbar toolbar) {
        super.setSupportActionBar(toolbar);
        if (actionBarSet) {
            actionBarSet = false;
            return;
        }
        actionBarSet = true;
        this.toolbar = toolbar;

        setSupportActionBar(toolbar);
        customizeActionbar();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            toolbar.setElevation(0);
    }

    public void customizeActionbar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && toolbar == null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.primaryColor)));
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowCustomEnabled(true);
            rtlHandles();
            setTitle(getTitle());
        } else {
            setTitle(null);
        }
    }


    public void customizeActionbar(int color) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && toolbar == null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(color)));
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowCustomEnabled(true);
            rtlHandles();
            setTitle(getTitle());
        } else {
            setTitle(null);
        }
    }

    private void rtlHandles() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            //final ArrayList<View> outViews = new ArrayList<>();
            //toolbar.findViewsWithText(outViews, "up", View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
            //outViews.get(0).setRotation(180f);
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    public void removeBackOnActionbar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

    public void doTryAgainDialog() {

    }

    public void finish(boolean finishTransition) {
        this.finishTransition = finishTransition;
        finish();
    }

    @Override
    public void startActivity(Intent intent) {
        try {
            super.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
        overridePendingTransition(R.anim.anim_right_to_0, R.anim.anim_0_to_left);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing() && finishTransition)
            overridePendingTransition(R.anim.anim_left_to_0, R.anim.anim_0_to_right);
    }


    public void removeActionBarBackButton(){
        try {
            (findViewById(R.id.btn_home)).setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showWaitDialog() {
        if(!waitDialog.isShowing())
            waitDialog.show();
    }
    public void showWaitDialog(String message){
        if(!waitDialog.isShowing())
            waitDialog.show();
        waitDialog.setMessage(message);
    }

    public void hideWaitDialog() {
        if(waitDialog.isShowing())
            waitDialog.dismiss();
    }

    public boolean isShowingWaitDialog() {
        return waitDialog.isShowing();
    }


    public void openHtmlLink(String rawLink){
        try {
            Intent intentViewHTMLLink = new Intent(Intent.ACTION_VIEW, Uri.parse(rawLink));
            startActivity(intentViewHTMLLink);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }


    public void showNoConnectionToast(){
//        toast.cancel();
        if(toast.getView().getWindowVisibility() != View.VISIBLE)
            toast.show();
    }


    public void showDialogMessage(String message){
        if(!dialogMessage.isShowing()) {
            dialogMessage.show();
        }
        dialogMessage.setMessage(message);
    }
    public void hideDialogMessage(){
        if(dialogMessage.isShowing())
            dialogMessage.hide();
    }



    public void openAddReviewPage(String bizUnqId){
        Intent i = new Intent(this, ActivityAddUpdateReview.class);
        i.putExtra(Params.OPENING_MODE,ActivityAddUpdateReview.MODE_ADDING);
        i.putExtra(Params.BUSINESS_UNIQUE_ID,bizUnqId);
        startActivity(i);
    }
    public void openUpdateReviewPage(String revUnqId){
        Intent i = new Intent(this, ActivityAddUpdateReview.class);
        i.putExtra(Params.OPENING_MODE,ActivityAddUpdateReview.MODE_UPDATING);
        i.putExtra(Params.REVIEW_ID,revUnqId);
        startActivity(i);
    }
    public void openReviewPageVisiting(String revUnqId){
        Intent i = new Intent(this, ActivityAddUpdateReview.class);
        i.putExtra(Params.OPENING_MODE,ActivityAddUpdateReview.MODE_VISITING);
        i.putExtra(Params.REVIEW_ID,revUnqId);
        startActivity(i);
    }

    public void openGalleryActivity(String bizUnqId){
        Intent myintent=new Intent(this,ActivityBusinessImage.class);
        myintent.putExtra(Params.BUSINESS_ID_STRING,bizUnqId);
        startActivity(myintent);
    }

    public void openBusinessOtherActivity(String bizUnqId){
        Intent intent = new Intent(this, ActivityBusinessBlockedUsers.class);
        intent.putExtra(Params.BUSINESS_ID_STRING, bizUnqId);
        startActivity(intent);
    }


}