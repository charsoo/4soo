package ir.rasen.charsoo.view.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.view.activity.ActivityPostAddEdit;
import ir.rasen.charsoo.view.interface_m.IDeletePost;


public class PopupWindowEditDeletePost extends PopupWindow {
    Context context;

    public PopupWindowEditDeletePost(final Context context, final IDeletePost iDeletePost, final Post post) {
        super(context);

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_post_business_more, null, false);
        setContentView(view);

        view.findViewById(R.id.btn_post_more_edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(context, ActivityPostAddEdit.class);
                        intent.putExtra(Params.BUSINESS_ID_STRING, post.businessUniqueId);
                        intent.putExtra(Params.POST_ID_INT, post.uniqueId);
                        ((Activity) context).startActivityForResult(intent, Params.ACTION_EDIT_POST);
                        PopupWindowEditDeletePost.this.dismiss();
                    }
                }, Params.FREEZ);
            }
        });
        view.findViewById(R.id.btn_post_more_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        DialogDeletePostConfirmation d = new DialogDeletePostConfirmation(context, post.businessUniqueId, post.uniqueId, iDeletePost);
                        d.show();
                        PopupWindowEditDeletePost.this.dismiss();
                    }
                }, Params.FREEZ);
            }
        });

        setBackgroundDrawable(new BitmapDrawable());
        setOutsideTouchable(true);
        setWindowLayoutMode(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

    }




}
