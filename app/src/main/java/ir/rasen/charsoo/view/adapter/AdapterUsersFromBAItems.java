package ir.rasen.charsoo.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.BaseAdapterItem;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.dialog.PopupWindowCancelFriendship;
import ir.rasen.charsoo.view.interface_m.ICancelFriendship;
import ir.rasen.charsoo.view.shared.ScreenUtils;
import ir.rasen.charsoo.view.widgets.MaterialProgressBarCircular;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.buttons.NoShadowFloatButton;
import ir.rasen.charsoo.view.widgets.imageviews.ImageViewCircle;

/**
 * Created by android on 3/7/2015.
 * Edited by Sina KH on 23/6/2015 : Changing view to new layout (item_user.xml) and optimizations
 */
public class AdapterUsersFromBAItems extends BaseAdapter implements ICancelFriendship{

    public static final String TAG="AdapterUsersFromBAItems";

    private ArrayList<BaseAdapterItem> items;
    private Context context;
    SimpleLoader simpleLoader;
    //IWebserviceResponse iWebserviceResponse;
    //WaitDialog progressDialog;
    String visitedId;
    Mode mode;
    DialogMessage dialogMessage;



    public enum Mode {
        USERS, OWN_FOLLOWERS
    }

    public AdapterUsersFromBAItems(Context context, String visitedId, ArrayList<BaseAdapterItem> items, Mode mode) {
        this.context = context;
        resetItems(items);
        simpleLoader = new SimpleLoader(context);
        //this.iWebserviceResponse = iWebserviceResponse;
        //this.progressDialog = progressDialog;
        this.visitedId = visitedId;
        this.mode = mode;

        dialogMessage=new DialogMessage(context,"");
    }


    public void loadMore(ArrayList<BaseAdapterItem> newItem){
        this.items.addAll(newItem);
        notifyDataSetChanged();
    }

    public void resetItems(ArrayList<BaseAdapterItem> newItem){
        items=(ArrayList<BaseAdapterItem>)newItem.clone();
        notifyDataSetChanged();
    }

    public void notifyRemoveAllItems(){
        items.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        Holder holder;

        if (view == null) {
            holder = new Holder();
            view = LayoutInflater.from(context).inflate(R.layout.item_user, viewGroup, false);
            holder.imageView = (ImageViewCircle) view.findViewById(R.id.user_img);
            holder.name = (TextViewFont) view.findViewById(R.id.user_name);
            holder.id = (TextViewFont) view.findViewById(R.id.user_id);
            holder.progressBar = (MaterialProgressBarCircular) view.findViewById(R.id.user_pb);
            holder.request = (NoShadowFloatButton) view.findViewById(R.id.user_send_request);
            holder.remove = (NoShadowFloatButton) view.findViewById(R.id.user_remove);
            view.setTag(holder);
        } else
            holder = (Holder) view.getTag();

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User.goUserHomeInfoPage(context, items.get(position).getId());
            }
        });
        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new PopupWindowCancelFriendship(context,items.get(position).getId(),AdapterUsersFromBAItems.this).showAsDropDown(view);
            }
        });

        // TODO:: WE SHOULD GET ID AND TITLE SEPERATELY TO USE HERE...
        //holder.uniqueId.setText(items.get(position).getTitle());
        holder.id.setVisibility(View.GONE);


        final int w = ScreenUtils.convertDpToPx(context, context.getResources().getDimension(R.dimen.profile_pic));
        //download image with customized class via imageId

        simpleLoader.loadImage(items.get(position).getImageId(),Image_M.LARGE,Image_M.ImageType.USER,
                holder.imageView,w,w,holder.progressBar,items.get(position).getTitle());

        holder.name.setText(items.get(position).getTitle());

        switch (mode) {
            case USERS:
                // TODO:: we should get friendship status to know when 'request' button should be available and...
                holder.request.setVisibility(View.GONE);
                if (visitedId.equals(LoginInfo.getInstance().getUserUniqueId())) {
                    holder.remove.setVisibility(View.VISIBLE);
                }
                break;
            case OWN_FOLLOWERS:
                holder.request.setVisibility(View.GONE);
                holder.remove.setVisibility(View.GONE);
                break;
        }

        return view;
    }

    @Override
    public void notifyDeleteFriend(String userId) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getId().equals(userId))
                items.remove(i);
        }
        notifyDataSetChanged();
    }

    @Override
    public void notifyDeleteFriendFailed(String failureMessage) {
        if (!dialogMessage.isShowing()){
            dialogMessage.show();
            dialogMessage.setMessage(failureMessage);
        }
    }


    //Each item in this adapter has a picture and a title

    private class Holder {
        ImageViewCircle imageView;
        TextViewFont name, id;
        MaterialProgressBarCircular progressBar;
        NoShadowFloatButton request, remove;
    }
}
