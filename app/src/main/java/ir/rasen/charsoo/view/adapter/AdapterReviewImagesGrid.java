package ir.rasen.charsoo.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.view.activity.business.ActivityAddUpdateReview;

/**
 * Created by Rasen_iman on 9/9/2015.
 */
public class AdapterReviewImagesGrid extends BaseAdapter{

    Context mContext;
    ArrayList <ReviewImage> reviewImages;
    LayoutInflater mInflater;
    int openingMode;

    public AdapterReviewImagesGrid(Context ctx,ArrayList<ReviewImage> reviewImages,int openingModeReview){
        this.mContext = ctx;
        if(reviewImages != null && reviewImages.size()!=0)
            this.reviewImages = reviewImages;
        else
            this.reviewImages = new ArrayList<>();

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.openingMode = openingModeReview;
    }



    @Override
    public int getCount() {
        return reviewImages.size();
    }

    @Override
    public Object getItem(int position) {
        return reviewImages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final View newView = mInflater.inflate(R.layout.review_image_layout,null,false);
        ImageView ivPicture = (ImageView) newView.findViewById(R.id.iv_picture);
        ImageView ivCancel = (ImageView) newView.findViewById(R.id.iv_cancel);
        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewImages.remove(position);
                notifyDataSetChanged();
            }
        });

        switch(openingMode){
            case ActivityAddUpdateReview.MODE_ADDING :
            case ActivityAddUpdateReview.MODE_UPDATING :
                ivCancel.setVisibility(View.VISIBLE);
                break;

            case ActivityAddUpdateReview.MODE_VISITING :
                ivCancel.setVisibility(View.GONE);
                break;
        }


        if(!reviewImages.get(position).isUnqId){
            ivPicture.setImageBitmap(reviewImages.get(position).bitmap);
        }else{
            MyApplication.loadReviewImage(mContext, ivPicture, reviewImages.get(position).imageUnqId);
        }


        return newView;
    }


    public void addNewImage(ReviewImage reviewImage){
        reviewImages.add(reviewImage);
        notifyDataSetChanged();
    }

    public ArrayList<ReviewImage> getAllReviewImages(){
        return (ArrayList<ReviewImage>) reviewImages.clone();
    }

    public static class ReviewImage {
        public String imageUnqId;
        public Bitmap bitmap;
        private boolean isUnqId;

        public ReviewImage(String imageUnqId){
            this.isUnqId = true;
            this.imageUnqId = imageUnqId;
        }
        public ReviewImage(Bitmap bmp){
            this.isUnqId = false;
            this.bitmap = bmp;
        }
        public boolean isUnqId(){
            return isUnqId;
        }
    }
}
