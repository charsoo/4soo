package ir.rasen.charsoo.view.shared;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;

import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.TextProcessor;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.post.GetBusinessPosts;
import ir.rasen.charsoo.view.activity.business.ActivityBusiness;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessContactInfo;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessEdit;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessFollowers;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessReviews;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessReviews2;
import ir.rasen.charsoo.view.adapter.AdapterPostGridAndList;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IDeletePost;
import ir.rasen.charsoo.view.interface_m.IPullToRefresh;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.buttons.FloatButton;
import ir.rasen.charsoo.view.widgets.imageviews.ExpandableImageView;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.HFGridView;

/**
 * Created by android on 3/14/2015.
 */
public class GridViewBusinessBothView implements IWebservice, IDeletePost, NetworkStateChangeListener {
    public static final String TAG = "GridViewBusinessBothView";

    private static final int GET_BUSINESS_POSTS_INT_CODE = 11;


    HFGridView gridViewHeader;
    //    AdapterPostGrid adapterPostGrid;
//    AdapterPost adapterPost;
    public AdapterPostGridAndList adapterPostGridAndList;
    public boolean isThreeColumn;
    boolean isLoadingMore;

    /*  ImageView imageViewMore, imageViewSwitch, imageViewCover, imageViewFollowers, imageViewReviews, imageViewContactInfo, imageViewCirecle, imageViewBack, imageViewEdit;
      TextViewFont textViewFollowersNumber, textViewIdentifier, textViewName;
  */
    FloatButton imageViewFollowers, imageViewReviews, imageViewContactInfo, imageViewEdit;
    ImageView imageViewCover;

    TextViewFont textViewFollowersNumber, textViewName;

    public View listFooterView;
    View viewHeader;
    Activity activity;
    Business business;
    ArrayList<Post> posts;
    boolean hasHeader;
    SimpleLoader simpleLoader;
    View switchGrid, switchList;
    DialogMessage dialogMessage;
    View viewNothing;
    ImageView imgNothing;
    TextViewFont txtNothing;
    IPullToRefresh iPullToRefresh;

    public static String parentTag;

    public GridViewBusinessBothView(Activity activity, Business business,
                                    HFGridView gridViewHeader, String parentTag, IPullToRefresh iPullToRefresh) {
        this.activity = activity;
        this.business = business;
        this.gridViewHeader = gridViewHeader;
        dialogMessage = new DialogMessage(activity, "");
        isThreeColumn = true;
        hasHeader = false;
        isLoadingMore = false;
        this.iPullToRefresh = iPullToRefresh;
        this.parentTag = parentTag;
    }

    public void notifyDatasetChanged() {
        adapterPostGridAndList.notifyDataSetChanged();
    }

    public void changeProfilePicture(String picture) {
        imageViewCover.setImageBitmap(Image_M.getBitmapFromString(picture));
    }

    public void notifyDataSetChanged(Post post) {
        posts.add(0, post);
    }

    public void InitialGridViewBusiness(ArrayList<Post> postList, boolean beThreeColumn) {

        this.isThreeColumn = beThreeColumn;

        adapterPostGridAndList =
                new AdapterPostGridAndList(
                        activity,
                        business.uniqueId, Post.GetPostType.BUSINESS,
                        GridViewBusinessBothView.this, activity, postList, isThreeColumn, parentTag);

        if (!hasHeader) {
            viewHeader = activity.getLayoutInflater().inflate(R.layout.layout_header_business, null);

            switchGrid = viewHeader.findViewById(R.id.btn_switch_grid);
            switchList = viewHeader.findViewById(R.id.btn_switch_list);
            imageViewCover = (ExpandableImageView) viewHeader.findViewById(R.id.imageView_cover);
            imageViewFollowers = (FloatButton) viewHeader.findViewById(R.id.imageView_followers);
            imageViewReviews = (FloatButton) viewHeader.findViewById(R.id.imageView_reviews);
            imageViewContactInfo = (FloatButton) viewHeader.findViewById(R.id.imageView_conatct_info);

            imageViewEdit = (FloatButton) viewHeader.findViewById(R.id.imageView_edit);

            textViewFollowersNumber = (TextViewFont) viewHeader.findViewById(R.id.textView_followers_number22);

            textViewName = (TextViewFont) viewHeader.findViewById(R.id.textView_business_name);

            textViewName.setText(String.valueOf(business.name));
            textViewFollowersNumber.setText(TextProcessor.numberToMiniNum(business.followersNumber));

            viewNothing = viewHeader.findViewById(R.id.view_nothing);
            txtNothing = (TextViewFont) viewHeader.findViewById(R.id.txt_nothing);
            imgNothing = (ImageView) viewHeader.findViewById(R.id.img_nothing);


            simpleLoader = new SimpleLoader(activity);
            simpleLoader.loadImage(business.profilePictureUniqueId, Image_M.LARGE, Image_M.ImageType.BUSINESS, imageViewCover);

            // now it has three column
            switchGrid.setBackgroundColor(activity.getResources().getColor(R.color.lightPrimaryColor));
            switchList.setBackgroundColor(activity.getResources().getColor(R.color.material_gray_light));



            imageViewEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, ActivityBusinessEdit.class);
                    intent.putExtra(Params.BUSINESS_PICUTE_ID,business.profilePictureUniqueId);
                    intent.putExtra(Params.BUSINESS_ID_STRING, business.uniqueId);
                    intent.putExtra(Params.BUSINESS_IDENTIFIER, business.businessIdentifier);
                    activity.startActivityForResult(intent, Params.ACTION_EDIT_BUSINESS);
                }
            });

            imageViewFollowers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent1 = new Intent(activity, ActivityBusinessFollowers.class);
                    intent1.putExtra(Params.BUSINESS_UNIQUE_ID, business.uniqueId);
                    intent1.putExtra(Params.POST_OWNER_BUSINESS_ID, business.uniqueId);
                    intent1.putExtra(Params.USER_UNIQUE_ID_STR, LoginInfo.getInstance().getUserUniqueId());
                    intent1.putExtra(Params.BUSINESS_OWNER,true);
                    activity.startActivity(intent1);
                }
            });
            imageViewReviews.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent1 = new Intent(activity, ActivityBusinessReviews2.class);
                    intent1.putExtra(Params.BUSINESS_UNIQUE_ID, business.uniqueId);
                    intent1.putExtra(Params.BUSINESS_OWNER, true);
                    activity.startActivity(intent1);
                }
            });
            imageViewContactInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //commented for the test
                    Intent intent = new Intent(activity, ActivityBusinessContactInfo.class);
                    intent.putExtra(Params.BUSINESS_OWNER, true);
                    try {
                        Globals.getInstance().getValue().businessVisiting = business.clone();
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                    activity.startActivity(intent);
                }
            });

            switchList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gridViewHeader.setNumColumns(1);
                    gridViewHeader.setColumnWidth(activity.getWindowManager().getDefaultDisplay().getWidth());
//                    gridViewHeader.setVerticalSpacing(3);
//                    gridViewHeader.setHorizontalSpacing(9);
//                    gridViewHeader.setViewWidthIfItsZero(activity.getWindowManager().getDefaultDisplay().getWidth());
//                    gridViewHeader.setAdapter(adapterPostGridAndList);

                    //now it has one column
                    isThreeColumn = false;
                    adapterPostGridAndList.setViewType(isThreeColumn);

                    switchList.setBackgroundColor(activity.getResources().getColor(R.color.lightPrimaryColor));
                    switchGrid.setBackgroundColor(activity.getResources().getColor(R.color.material_gray_light));
                }
            });
            switchGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    prepareGridThreeColumn(gridViewHeader);
                    gridViewHeader.setNumColumns(3);
                    isThreeColumn = true;
                    adapterPostGridAndList.setViewType(isThreeColumn);

                    // now it has three column
                    switchGrid.setBackgroundColor(activity.getResources().getColor(R.color.lightPrimaryColor));
                    switchList.setBackgroundColor(activity.getResources().getColor(R.color.material_gray_light));
                }
            });


            gridViewHeader.addHeaderView(viewHeader);
            hasHeader = true;

            listFooterView = ((LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_loading_more, null, false);
            //listFooterView.setVisibility(View.GONE);
            gridViewHeader.addFooterView(listFooterView);


        } else {
            listFooterView.setVisibility(View.GONE);
        }
        gridViewHeader.setBackgroundColor(Color.parseColor("#ffffff"));


        if (isThreeColumn) {
//            adapterPostGridAndList.setViewType(isThreeColumn);
            prepareGridThreeColumn(gridViewHeader);
//            gridViewHeader.setNumColumns(3);
        }/* else {
//            adapterPostGridAndList.setViewType(isThreeColumn);
        }*/

        gridViewHeader.setAdapter(adapterPostGridAndList);


        gridViewHeader.setOnScrollListener(new AbsListView.OnScrollListener() {
            int currentFirstVisibleItem,
                    currentVisibleItemCount,
                    currentScrollState;

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                ((ActivityBusiness) activity).onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
            }

            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            private void isScrollCompleted() {
                if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
                    if (!isLoadingMore &&  posts!=null
                            && posts.size() > 0 && posts.size() % activity.getResources().getInteger(R.integer.lazy_load_limitation) == 0) {
                        loadMoreData();
                    }
                }
            }
        });


        handleShowNotingLayout();


    }

    // LOAD MORE DATA
    public void loadMoreData() {
        isLoadingMore = true;
        listFooterView.setVisibility(View.VISIBLE);
        iPullToRefresh.notifyLoadMore();
//        callWebservice(GET_BUSINESS_POSTS_INT_CODE, false);
    }

    public void addMorePosts(ArrayList<Post> newPosts) {
        posts.addAll(newPosts);
        handleShowNotingLayout();
        listFooterView.setVisibility(View.GONE);
        adapterPostGridAndList.loadMoreItems(newPosts);
        isLoadingMore = false;
    }

    public void resetPostItems(ArrayList<Post> newPosts) {
        posts = new ArrayList<>(newPosts);
        adapterPostGridAndList.resetItems(posts);
        handleShowNotingLayout();
    }

    private void prepareGridThreeColumn(HFGridView gridViewHeader) {
        gridViewHeader.setNumColumns(3);
        gridViewHeader.setVerticalSpacing(3);
        gridViewHeader.setHorizontalSpacing(9);
        gridViewHeader.setViewWidthIfItsZero(activity.getWindowManager().getDefaultDisplay().getWidth());
    }


    @Override
    public void notifyDeletePost(String postId) {
        for (int i = 0; i < posts.size(); i++) {
            if (posts.get(i).uniqueId.equals(postId)) {
                posts.remove(i);
                break;
            }
        }
        adapterPostGridAndList.resetItems(posts);
//        adapterPostGridAndList.notifyDataSetChanged();

        handleShowNotingLayout();
    }

    @Override
    public void notifyDeleteFailed(String postId, String failureMessage) {
        if (!dialogMessage.isShowing()) {
            dialogMessage.show();
            dialogMessage.setMessage(failureMessage);
        }
    }

    public void refreshBusinessData(Business newBusiness) {
        if (!this.business.profilePictureUniqueId.equals(newBusiness.profilePictureUniqueId)) {
            this.business = newBusiness;
            simpleLoader = new SimpleLoader(activity);
            simpleLoader.loadImage(business.profilePictureUniqueId, Image_M.LARGE, Image_M.ImageType.BUSINESS, imageViewCover);
        } else
            this.business = newBusiness;

        ((ActivityBusiness)activity).setTextViewIdentifier(String.valueOf(business.businessIdentifier));

        textViewName.setText(String.valueOf(business.name));
        textViewFollowersNumber.setText(TextProcessor.numberToMiniNum(business.followersNumber));
    }

    @Override
    public void getResult(int request, Object result) {
        switch (request) {
            case GET_BUSINESS_POSTS_INT_CODE:
                ArrayList<Post> newPosts = (ArrayList<Post>) result;
                posts.addAll(newPosts);
                listFooterView.setVisibility(View.GONE);
                adapterPostGridAndList.loadMoreItems(newPosts);
                isLoadingMore = false;
                handleShowNotingLayout();
                break;
        }
        NetworkConnectivityReciever.removeIfHaveListener(TAG);
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
        listFooterView.setVisibility(View.GONE);
        if (!dialogMessage.isShowing()) {
            dialogMessage.show();
            dialogMessage.setMessage(ServerAnswer.getError(activity, errorCode, callerStringID + ">" + TAG));
        }
        if (errorCode == ServerAnswer.NETWORK_CONNECTION_ERROR) {
            NetworkConnectivityReciever.setNetworkStateListener(TAG, GridViewBusinessBothView.this);
        }
    }

    @Override
    public void doOnNetworkConnected() {
        callWebservice(GET_BUSINESS_POSTS_INT_CODE, false);
    }

    private void callWebservice(int reqCode, boolean showProgressDialog) {
//        if (showProgressDialog)
//            progressDialog.show();
        switch (reqCode) {
            case GET_BUSINESS_POSTS_INT_CODE:
                // LOAD MORE DATA HERE...
                isLoadingMore = true;
                listFooterView.setVisibility(View.VISIBLE);
                new GetBusinessPosts(activity,LoginInfo.getInstance().getUserUniqueId(), business.uniqueId,
                        adapterPostGridAndList.getCount(), activity.getResources().getInteger(R.integer.lazy_load_limitation),
                        GridViewBusinessBothView.this, GET_BUSINESS_POSTS_INT_CODE, business.profilePictureUniqueId,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                break;

        }
    }


    public void handleShowNotingLayout(){
        if(adapterPostGridAndList!=null && adapterPostGridAndList.getCount()==0)
            viewNothing.setVisibility(View.VISIBLE);
        else
            viewNothing.setVisibility(View.GONE);
    }
}
