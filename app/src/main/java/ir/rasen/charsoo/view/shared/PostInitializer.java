package ir.rasen.charsoo.view.shared;

import android.content.Context;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.PersianDate;
import ir.rasen.charsoo.controller.helper.TextProcessor;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.Comment;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.model.post.Like;
import ir.rasen.charsoo.model.post.Share;
import ir.rasen.charsoo.model.post.Unlike;
import ir.rasen.charsoo.view.activity.ActivityPost;
import ir.rasen.charsoo.view.adapter.AdapterPost;
import ir.rasen.charsoo.view.dialog.DialogCancelShareConfirmationTimeLine;
import ir.rasen.charsoo.view.dialog.PopupWindowEditDeletePost;
import ir.rasen.charsoo.view.dialog.PopupWindowReportCancelSharePost;
import ir.rasen.charsoo.view.dialog.PopupWindowReportPostAdapter;
import ir.rasen.charsoo.view.interface_m.IDeletePost;
import ir.rasen.charsoo.view.interface_m.ILikeDislikeListener;
import ir.rasen.charsoo.view.interface_m.IReportPost;
import ir.rasen.charsoo.view.interface_m.IShareCancelShareListener;
import ir.rasen.charsoo.view.interface_m.IUpdateTimeLine;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.material_library.views.CustomView;

/**
 * Created by Sina on 7/5/15.
 */
public class PostInitializer {

    private Context context;
    private ILikeDislikeListener delegate;
    private Post post;
    private SimpleLoader simpleLoader;

    private AdapterPost adapterPost;
    private IShareCancelShareListener delegateShareCancelShare;

    private static PopupWindowReportPostAdapter popupWindowReportPostAdapter;
    private static PopupWindowEditDeletePost popupWindowEditDeletePost;
    private static PopupWindowReportCancelSharePost popupWindowReportCancelSharePost;

    public PostInitializer(AdapterPost adapterPost, Context context,
                           SimpleLoader simpleLoader, ILikeDislikeListener delegate,
                           Post post, IShareCancelShareListener delegateShareCancelShare) {
        this.adapterPost = adapterPost;
        this.context = context;
        this.delegate = delegate;
        this.post = post;
        this.simpleLoader = simpleLoader;
        this.delegateShareCancelShare = delegateShareCancelShare;
    }

    public void initPostDetails(final ImageView imgProfile, TextViewFont txtBusinessId, TextViewFont date
            , final ImageView imgPost, final CustomView pbPost, TextViewFont title, TextViewFont description
            , View priceSection, TextViewFont price
            , View codeSection, TextViewFont code, View sections) {

        date.setText(PersianDate.getCreationDate(context, post.creationDate));
        txtBusinessId.setText(post.businessIdentifier);
        txtBusinessId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Business.gotoBusinessOtherPage(context, post.businessUniqueId);

            }
        });
        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Business.gotoBusinessOtherPage(context, post.businessUniqueId);
            }
        });

        imgProfile.postDelayed(new Runnable() {
            @Override
            public void run() {
                simpleLoader.loadImage(post.businessProfilePictureUniqueId, Image_M.SMALL, Image_M.ImageType.BUSINESS, imgProfile,post.businessIdentifier);
            }
        },50);

        imgPost.postDelayed(new Runnable() {
            @Override
            public void run() {
                simpleLoader.loadImage(
                        post.pictureUniqueId, Image_M.LARGE, Image_M.ImageType.POST,
                        imgPost, pbPost);
            }
        },50);

        description.setText(TextProcessor.removeHashtags(post.description));
        title.setText(TextProcessor.removeHashtags(post.title));
        if (post.price != null && !post.price.equals("") && !post.price.equals("null")) {
            price.setText(post.price);
            priceSection.setVisibility(View.VISIBLE);
        } else
            priceSection.setVisibility(View.GONE);
        if (post.code != null && !post.code.equals("") && !post.code.equals("null")) {
            code.setText(post.code);
            codeSection.setVisibility(View.VISIBLE);
        } else
            codeSection.setVisibility(View.GONE);
        if(codeSection.getVisibility()==View.GONE && priceSection.getVisibility()==View.GONE) {
            sections.setVisibility(View.GONE);
        } else {
            sections.setVisibility(View.VISIBLE);
        }
    }

    public void initLike(final ImageView imgPost, final ImageView like, final TextViewFont likeNumber, final ImageView postLike) {
        likeNumber.setText(String.valueOf(post.likeNumber));
        if (post.isLiked) {
            like.setImageResource(R.mipmap.ic_liked);
        } else {
            like.setImageResource(R.mipmap.ic_like);
        }
        like.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (post.isLiked)
                        like.setBackgroundResource(R.mipmap.circle_pink);
                    else
                        like.setBackgroundResource(R.mipmap.circle_gray);
                } else
                    like.setBackgroundResource(android.R.color.transparent);
                return false;
            }
        });
        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (post.isLiked) {
                    //unlike the post

                    new Unlike(context,LoginInfo.getInstance().getUserUniqueId(), post.uniqueId, delegate,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                    post.isLiked = false;
                    like.setImageResource(R.mipmap.ic_like);
                    post.likeNumber--;
                    adapterPost.updatePost(post);
                    initLike(imgPost, like, likeNumber, postLike);
                } else {
                    //like the post
                    likePost(imgPost, like, likeNumber, postLike);

                }

            }
        });
        imgPost.setOnTouchListener(new View.OnTouchListener() {
            //variable for counting two successive up-down events
            int clickCount = 0;
            //variable for storing the time of first click
            long startTime;
            //variable for calculating the total time
            long duration;
            //constant for defining the time duration between the click that can be considered as double-tap
            static final int MAX_DURATION = 500;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(postLike.getVisibility()==View.VISIBLE)
                    return false;
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        startTime = System.currentTimeMillis();
                        clickCount++;
                        break;
                    case MotionEvent.ACTION_UP:
                        long time = System.currentTimeMillis() - startTime;
                        duration = duration + time;
                        if (clickCount == 2) {
                            if (duration <= MAX_DURATION) {
                                if (!post.isLiked) {
                                    likePost(imgPost, like, likeNumber, postLike);
                                    YoYo.with(Techniques.Shake)
                                            .duration(700)
                                            .playOn(like);
                                    YoYo.with(Techniques.ZoomIn)
                                            .duration(700)
                                            .playOn(postLike);
                                    postLike.setVisibility(View.VISIBLE);
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            postLike.setVisibility(View.GONE);
                                        }
                                    }, 1000);
                                }
                            }
                            clickCount = 0;
                            duration = 0;
                            break;
                        }
                }
                return true;
            }
        });
    }

    public void initComment(ImageView comment, TextViewFont commentNumber, final boolean isOwner) {
        commentNumber.setText(String.valueOf(post.commentNumber));
        comment.setOnTouchListener(ViewMethods.onTouchListenerGrayCircle);
        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Comment.openCommentActivity(context, isOwner, post.uniqueId, post.businessUniqueId);
            }
        });
    }

    public void initShare(final ImageView share, final TextViewFont shareNumber) {
        shareNumber.setText(String.valueOf(post.shareNumber));
        if (post.isShared)
            share.setImageResource(R.mipmap.ic_shared);
        else
            share.setImageResource(R.mipmap.ic_share);
        share.setOnTouchListener(ViewMethods.onTouchListenerGrayCircle);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (post.isShared) {
                    //cancel share the post
                    new DialogCancelShareConfirmationTimeLine(
                            context, post,share,shareNumber, delegateShareCancelShare,
                            PostInitializer.this).show();
                        /*new CancelShare(context, LoginInfo.getUserId(context), post.uniqueId, null).executeWithNewSolution();;

                        post.isShared = false;
                        cb_imageViewShare.setImageResource(R.drawable.ic_reply_grey);*/
                } else {
                    //share the post

                    new Share(context,LoginInfo.getInstance().getUserUniqueId(), post.uniqueId, null, delegateShareCancelShare,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

                    post.isShared = true;
                    post.shareNumber++;
//                    share.setImageResource(R.mipmap.ic_shared);

                    initShare(share,shareNumber);
                }
            }
        });
    }

    public void initComments(final View llComments, final View c1view, final ImageView c1img, final TextViewFont c1uId, final TextViewFont c1text
            , final View c2view, final ImageView c2img, final TextViewFont c2uId, final TextViewFont c2text
            , final View c3view, final ImageView c3img, final TextViewFont c3uId, final TextViewFont c3text, final boolean isOwner) {

        final ArrayList<Comment> lastThreeComments = post.lastThreeComments;
        if (lastThreeComments.size() == 0) {
            llComments.setVisibility(View.GONE);
        } else
            llComments.setVisibility(View.VISIBLE);

        c1view.setVisibility(View.GONE);
        c2view.setVisibility(View.GONE);
        c3view.setVisibility(View.GONE);
        if (lastThreeComments.size() >= 1) {
            c1uId.setText(post.lastThreeComments.get(0).username);
            c1text.setText(post.lastThreeComments.get(0).text);
            simpleLoader.loadImage(lastThreeComments.get(0).userProfilePictureUniqueId, Image_M.SMALL, Image_M.ImageType.USER
                    , c1img, lastThreeComments.get(0).username);
            c1view.setVisibility(View.VISIBLE);
        }
        if (lastThreeComments.size() >= 2) {
            c2uId.setText(post.lastThreeComments.get(1).username);
            c2text.setText(post.lastThreeComments.get(1).text);
            simpleLoader.loadImage(lastThreeComments.get(1).userProfilePictureUniqueId, Image_M.SMALL, Image_M.ImageType.USER
                    , c2img, lastThreeComments.get(1).username);
            c2view.setVisibility(View.VISIBLE);
        }
        if (lastThreeComments.size() >= 3) {
            c3uId.setText(post.lastThreeComments.get(2).username);
            c3text.setText(post.lastThreeComments.get(2).text);
            simpleLoader.loadImage(lastThreeComments.get(2).userProfilePictureUniqueId, Image_M.SMALL, Image_M.ImageType.USER
                    , c3img, lastThreeComments.get(2).username);
            c3view.setVisibility(View.VISIBLE);
        }

        c1uId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoOwnerPage(post.lastThreeComments.get(0));
            }
        });
        c1img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoOwnerPage(post.lastThreeComments.get(0));
            }
        });

        c2uId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoOwnerPage(post.lastThreeComments.get(1));
            }
        });
        c2img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoOwnerPage(post.lastThreeComments.get(1));
            }
        });

        c3uId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoOwnerPage(post.lastThreeComments.get(2));
            }
        });
        c3img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoOwnerPage(post.lastThreeComments.get(2));
            }
        });
    }


    public void gotoOwnerPage(Comment cm){
        if(cm.isUserSendedComment)
            User.goUserHomeInfoPage(context,cm.ownerUniqueId);
        else
            Business.gotoBusinessOtherPage(context,cm.ownerUniqueId);
    }

    private void likePost(ImageView imgProfile, ImageView like, TextViewFont likeNumber, ImageView postLike) {
        new Like(context,LoginInfo.getInstance().getUserUniqueId(), post.uniqueId, delegate,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
        post.isLiked = true;
        like.setImageResource(R.mipmap.ic_liked);
        post.likeNumber++;
        adapterPost.updatePost(post);
        initLike(imgProfile, like, likeNumber, postLike);
    }

    public void initMenu(final ImageView more, final IReportPost iReportPost) {
        if (post.isReported)
            more.setVisibility(View.GONE);
        else
            more.setVisibility(View.VISIBLE);
        more.setOnTouchListener(ViewMethods.onTouchListenerGrayCircle);
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindowReportPostAdapter =
                        new PopupWindowReportPostAdapter(context,LoginInfo.getInstance().getUserUniqueId(),
                                post.uniqueId, more, iReportPost);
                popupWindowReportPostAdapter.showAsDropDown(more);
            }
        });
    }

    public void initMenuUser(final ImageView more, final IUpdateTimeLine iUpdateTimeLine, final IReportPost iReportPost) {
        more.setOnTouchListener(ViewMethods.onTouchListenerGrayCircle);
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindowReportCancelSharePost =
                        new PopupWindowReportCancelSharePost(
                                context,LoginInfo.getInstance().getUserUniqueId(), post.uniqueId,
                                more, iUpdateTimeLine, iReportPost);
                popupWindowReportCancelSharePost.showAsDropDown(more);

            }
        });
    }

    public void initMenuBusiness(final ImageView more, final IDeletePost iDeletePost) {
        more.setOnTouchListener(ViewMethods.onTouchListenerGrayCircle);
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindowEditDeletePost = new PopupWindowEditDeletePost(context, iDeletePost, post);
                popupWindowEditDeletePost.showAsDropDown(more);
            }
        });
    }

    public void initMenuPost(ImageView more, ActivityPost activityPost) {
        if(post.getPostType == Post.GetPostType.BUSINESS) {
            initMenuBusiness(more, activityPost);
        } else if(post.isShared) {
            initMenuUser(more, activityPost, activityPost);
        } else
            initMenu(more, activityPost);
    }


    private static boolean dismissPopupReportIfIsShowing(){
        try {
            if(popupWindowReportPostAdapter.isShowing()) {
                popupWindowReportPostAdapter.dismiss();
                return true;
            }else{
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    private static boolean dismissPopupWindowEditDeletePostIfIsShowing(){
        try {
            if(popupWindowEditDeletePost.isShowing()) {
                popupWindowEditDeletePost.dismiss();
                return true;
            }
            else
                return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    private static boolean dismissPopupWindowReportCancelSharePostIfIsShowing(){
        try {
            if(popupWindowReportCancelSharePost.isShowing()) {
                popupWindowReportCancelSharePost.dismiss();
                return true;
            }else
                return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public static boolean dismissAllPopupsIfAnyShowing(){
        if(
                dismissPopupReportIfIsShowing() ||
                dismissPopupWindowEditDeletePostIfIsShowing() ||
                dismissPopupWindowReportCancelSharePostIfIsShowing()
                )
            return true;
        else
            return false;
    }
}
