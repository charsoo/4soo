package ir.rasen.charsoo.view.interface_m;

/**
 * Created by hossein-pc on 7/16/2015.
 */
public interface RequestFriendshipListener {
    void onRequestSent(String UserIntId);
    void onRequestFailed(String UserIntId,String failureMessage);
}
