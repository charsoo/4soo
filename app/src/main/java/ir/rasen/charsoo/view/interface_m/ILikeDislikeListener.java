package ir.rasen.charsoo.view.interface_m;

/**
 * Created by hossein-pc on 7/5/2015.
 */
public interface ILikeDislikeListener {
    void onDislikeSuccessful(String dislikedPostUniqueId) throws Exception;
    void onDislikeFailed(String dislikedPostUniqueId,String failureMessage) throws Exception;
    void onLikeSuccessful(String likedPostUniqueId) throws Exception;
    void onLikeFailed(String likedPostUniqueId,String failureMessage) throws Exception;
}
