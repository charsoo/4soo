package ir.rasen.charsoo.view.activity.business;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.controller.object.Review;
import ir.rasen.charsoo.model.business.SendImage;
import ir.rasen.charsoo.model.review.GetOverallReviewStatus;
import ir.rasen.charsoo.model.review.GetReviewHeader;
import ir.rasen.charsoo.model.review.GetReviewsForBusiness;
import ir.rasen.charsoo.model.review.GetUserReviewByBizId;
import ir.rasen.charsoo.model.user.FollowBusiness;
import ir.rasen.charsoo.model.user.UnFollowBusiness;
import ir.rasen.charsoo.view.activity.ActivityCamera;
import ir.rasen.charsoo.view.activity.ActivityGallery;
import ir.rasen.charsoo.view.adapter.AdapterReviews;
import ir.rasen.charsoo.view.dialog.PopupSelectCameraGallery;
import ir.rasen.charsoo.view.interface_m.IFollowBusiness;
import ir.rasen.charsoo.view.interface_m.IReviewChange;
import ir.rasen.charsoo.view.interface_m.IUnfollowBusiness;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.buttons.ButtonFont;
import ir.rasen.charsoo.view.widgets.buttons.FloatButton;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.imageviews.RoundedImageView;

/**
 * Created by Rasen_iman on 9/5/2015.
 */
public class ActivityBusinessReviews2
        extends CharsooActivity
        implements IWebservice,IFollowBusiness,IUnfollowBusiness,IReviewChange{

    public static final int
            GET_USER_REVIEW = 100,
            GET_OVERALL_REVIEW_STATUS = 200,
            GET_REVIEW_HEADER = 300,
            GET_ALL_REVIEWS = 400,
            SEND_IMAGE = 500;

    public String[] orderStrs;

    enum Status{
        REFRESH,LOADINGMORE
    }

    //Head Section
    RelativeLayout rlBuzinessInfoSection;
    ButtonFont btnFollowStatus;
    ImageView ivBizProfilePicture;
    TextViewFont tvBizName,tvWorkDay,tvIsOpen;
    //ImagesSection
    ImageView ivBizImage1,ivBizImage2,ivBizImage3;
    RelativeLayout rlAddImage;
    ImageView ivOpenStatus;

    //UserReview Section
    RelativeLayout rlUserReviewSection;
    TextViewFont tvUserName;
    RoundedImageView ivUserProfilePicture;
    TextViewFont tvUserDate;
    LinearLayout llEdit;
    TextViewFont tvUserComment;
    ImageView ivUserCommentPicture;
    RatingBar rbUserRatingBar;

    //Review OverallStatus Section
    RelativeLayout rlReviewOverallSection;
    TextViewFont tvAverageNumber,tvReviewsCount;
    RatingBar averageRateBar;
    View view1Star,view2Star,view3Star,view4Star,view5Star;
    TextViewFont tvStar1,tvStar2,tvStar3,tvStar4,tvStar5;

    //All Reviews Section
    RelativeLayout rlAllReviewsSection;
    Spinner spOrder;
    ListView lvReviews;
    AdapterReviews adapterReviews;


    /********************************************************************/
    String bizUniqueId;
    FloatButton btnAddReview;


    public Status currentStatus;
    public int currentSelectedOrder;
    public boolean isFirstTime = true;



    public static IReviewChange mOnReviewChange;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_businessreviews2);
        setTitle(Globals.getInstance().getValue().businessVisiting.name);

        currentStatus = Status.REFRESH;
        currentSelectedOrder = 1;

        init();

        try {
            bizUniqueId = getIntent().getExtras().getString(Params.BUSINESS_UNIQUE_ID,"");
        } catch (Exception e) {
            bizUniqueId = "";
        }


        resetAllSections();
        startLoadingAllSections();


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
       if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }
    private void init() {
        //Head Section
        rlBuzinessInfoSection = (RelativeLayout) findViewById(R.id.rl_BusinessInfoSection);
        btnFollowStatus = (ButtonFont) findViewById(R.id.btn_follow_satus);
        ivBizProfilePicture = (ImageView) findViewById(R.id.iv_bizProfilePicture);
        tvBizName = (TextViewFont) findViewById(R.id.tv_bizName);
        tvWorkDay = (TextViewFont) findViewById(R.id.tv_workDay);
        tvIsOpen = (TextViewFont) findViewById(R.id.tv_isOpen);
        //Images
        ivBizImage1 = (ImageView) findViewById(R.id.iv_BizImage1);
        ivBizImage2 = (ImageView) findViewById(R.id.iv_BizImage2);
        ivBizImage3 = (ImageView) findViewById(R.id.iv_BizImage3);
        rlAddImage = (RelativeLayout) findViewById(R.id.rl_AddImage);
        ivOpenStatus = (ImageView) findViewById(R.id.iv_onlineStatus);


        //UserReview Section
        rlUserReviewSection = (RelativeLayout) findViewById(R.id.rl_userReviewSection);
        tvUserName = (TextViewFont) findViewById(R.id.tv_userName);
        ivUserProfilePicture = (RoundedImageView) findViewById(R.id.iv_UserProfilePicture);
        tvUserDate = (TextViewFont) findViewById(R.id.tv_userDate);
        llEdit = (LinearLayout) findViewById(R.id.ll_edit);
        tvUserComment = (TextViewFont) findViewById(R.id.tv_userComment);
        ivUserCommentPicture = (ImageView) findViewById(R.id.ivUserCommentPicture);
        rbUserRatingBar = (RatingBar) findViewById(R.id.rb_userRate);

        //Review OverallStatus Section
        rlReviewOverallSection = (RelativeLayout) findViewById(R.id.rl_reviewOverallSection);
        tvAverageNumber = (TextViewFont) findViewById(R.id.tv_averageNumber);
        tvReviewsCount = (TextViewFont) findViewById(R.id.tv_reviewsCount);
        averageRateBar = (RatingBar) findViewById(R.id.rb_averageRateBar);
        view1Star = findViewById(R.id.view_1star);
        tvStar1 = (TextViewFont) findViewById(R.id.tv1star);

        view2Star = findViewById(R.id.view_2star);
        tvStar2 = (TextViewFont) findViewById(R.id.tv2star);

        view3Star = findViewById(R.id.view_3star);
        tvStar3 = (TextViewFont) findViewById(R.id.tv3star);

        view4Star = findViewById(R.id.view_4star);
        tvStar4 = (TextViewFont) findViewById(R.id.tv4star);

        view5Star = findViewById(R.id.view_5star);
        tvStar5 = (TextViewFont) findViewById(R.id.tv5star);


        //All Reviews Section
        rlAllReviewsSection = (RelativeLayout) findViewById(R.id.rl_allReviewsSection);
        spOrder = (Spinner) findViewById(R.id.sp_order);
        lvReviews = (ListView) findViewById(R.id.lv_reviews);
        adapterReviews = new AdapterReviews(this,null);

        orderStrs = new String[]{
                getResources().getString(R.string.mofid_tarin_ha),
                getResources().getString(R.string.jadid_taring_ha),
                getResources().getString(R.string.emtizae_bala)
        };


        btnAddReview = (FloatButton) findViewById(R.id.btn_addReview);
        btnAddReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddReviewPage(bizUniqueId);
            }
        });

        mOnReviewChange = this;
    }



    private void resetAllSections() {
        resetHeaderInfo();
        resetUserReviewSection();
        resetOverallReviewStatus();
        resetAllReviewsSection();
    }
    private void startLoadingAllSections() {
        receiveHeaderSection();
    }


    public void receiveAllSections(){
        isFirstTime = true;
        receiveHeaderSection();
    }
    public void receiveHeaderSection(){
        showWaitDialog();
        new GetReviewHeader(this,this,bizUniqueId,LoginInfo.getInstance().getUserUniqueId(),
                GET_REVIEW_HEADER).executeWithNewSolution();
    }
    public void receiveUserReview(){
        showWaitDialog();
        new GetUserReviewByBizId(this,this, LoginInfo.getInstance().getUserUniqueId(),
                bizUniqueId,GET_USER_REVIEW).executeWithNewSolution();
    }
    public void receiveOverallReviewStatus(){
        showWaitDialog();
        new GetOverallReviewStatus(this,bizUniqueId,this,
                GET_OVERALL_REVIEW_STATUS).executeWithNewSolution();
    }
    public void receiveAllReviews(int limit,int selectedOrder){
        showWaitDialog();
        new GetReviewsForBusiness(this,bizUniqueId,LoginInfo.getInstance().getUserUniqueId(),limit,
                getResources().getInteger(R.integer.lazy_load_limitation),selectedOrder,this,GET_ALL_REVIEWS).executeWithNewSolution();
    }


    @Override
    public void getResult(int request, Object result) throws Exception {
        switch (request){
            case GET_REVIEW_HEADER :
                initHeaderInfo((RequestObject.ReviewHeaderInfo) result);
                if(isFirstTime)
                    receiveUserReview();
                else
                    hideWaitDialog();
                break;

            case GET_USER_REVIEW :
                RequestObject.ReviewItem reviewItem = (RequestObject.ReviewItem) result;
                initUserReviewSection(reviewItem);
                if(isFirstTime)
                    receiveOverallReviewStatus();
                else
                    hideWaitDialog();
                break;

            case GET_OVERALL_REVIEW_STATUS :
                initOverallReviewStatus((ArrayList<RequestObject.RevStatisticInfo>) result);
                if(isFirstTime)
                    switch(currentStatus){
                        case REFRESH:
                            receiveAllReviews(0,currentSelectedOrder);
                            break;

                        case LOADINGMORE:
                            receiveAllReviews(adapterReviews.getCount()+1,currentSelectedOrder);
                            break;
                    }
                else
                    hideWaitDialog();
                break;

            case GET_ALL_REVIEWS :
                hideWaitDialog();
                if(isFirstTime)
                    isFirstTime = false;

                switch(currentStatus){
                    case REFRESH:
                        initAllReviewsSection((ArrayList<RequestObject.ReviewItem>) result);
                        break;

                    case LOADINGMORE:
                        loadMoreReviews((ArrayList<RequestObject.ReviewItem>) result);
                        break;
                }
                break;

            case SEND_IMAGE:
                receiveHeaderSection();
                break;
        }

    }

    private void loadMoreReviews(ArrayList<RequestObject.ReviewItem> result) {
        adapterReviews.addItems(result);
    }


    @Override
    public void getError(int request, Integer errorCode, String callerStringID) throws Exception {
        hideWaitDialog();
        showDialogMessage(ServerAnswer.getError(
                ActivityBusinessReviews2.this,
                errorCode, callerStringID + ">" + this.getLocalClassName()));
        switch (request){
            case GET_REVIEW_HEADER :
                break;

            case GET_USER_REVIEW :
                break;

            case GET_OVERALL_REVIEW_STATUS :
                break;

            case GET_ALL_REVIEWS :
                break;


            case SEND_IMAGE:
                break;
        }
    }


    public void resetHeaderInfo(){
        configBtnForFollowed(btnFollowStatus);
        tvBizName.setText("");
        tvWorkDay.setText("");
        tvIsOpen.setText("");
        //Images
        rlAddImage.setVisibility(View.VISIBLE);
    }
    public void initHeaderInfo(RequestObject.ReviewHeaderInfo reviewHeaderInfo){
        if(reviewHeaderInfo.IsFollowing) {
            configBtnForFollowed(btnFollowStatus);
        }
        else {
            configBtnForNotFollow(btnFollowStatus);
        }

        rlBuzinessInfoSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Business.gotoBusinessOtherPage(ActivityBusinessReviews2.this,bizUniqueId);
            }
        });

        MyApplication.loadBusinessProfilePicture(this, ivBizProfilePicture,
                reviewHeaderInfo.BizImageId, reviewHeaderInfo.BizName);

        tvBizName.setText(reviewHeaderInfo.BizName);
        String workdayStr = createWorkDayText(reviewHeaderInfo);
        if(workdayStr.equals("")) {
            tvWorkDay.setVisibility(View.GONE);
        }
        else{
            tvWorkDay.setText(workdayStr);
            tvWorkDay.setVisibility(View.VISIBLE);
        }
        if(reviewHeaderInfo.IsOpen) {
            tvIsOpen.setText(getResources().getString(R.string.is_open));
            ivOpenStatus.setImageDrawable(getResources().getDrawable(R.drawable.bg_online_status));
        }
        else {
            tvIsOpen.setText(getResources().getString(R.string.is_close));
            ivOpenStatus.setImageDrawable(getResources().getDrawable(R.drawable.bg_offline_status));
        }


        ivBizImage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PopupSelectCameraGallery(ActivityBusinessReviews2.this).show();
            }
        });

        ivBizImage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGalleryActivity(bizUniqueId);
            }
        });

        ivBizImage3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGalleryActivity(bizUniqueId);
            }
        });

        MyApplication.loadReviewImage(this, ivBizImage2, reviewHeaderInfo.FirstImage);
        MyApplication.loadReviewImage(this, ivBizImage3, reviewHeaderInfo.SecondImage);
    }

    private String createWorkDayText(RequestObject.ReviewHeaderInfo reviewHeaderInfo) {
        if(reviewHeaderInfo.CloseTime.equals("-1:-1") || reviewHeaderInfo.OpenTime.equals("-1:-1"))
            return "";

        StringBuilder sb = new StringBuilder();
        sb.append(getDayStr(reviewHeaderInfo.TodayIntValue));
        sb.append(" " + reviewHeaderInfo.OpenTime + " ");
        sb.append(getResources().getString(R.string.until));
        sb.append(" " + reviewHeaderInfo.CloseTime + " ");
        return sb.toString();
    }
    private String getDayStr(int todayIntValue) {
        switch (todayIntValue){
            case 0 :
                return getResources().getString(R.string.Sat);
            case 1 :
                return getResources().getString(R.string.Sun);
            case 2 :
                return getResources().getString(R.string.Mon);
            case 3 :
                return getResources().getString(R.string.Tue);
            case 4 :
                return getResources().getString(R.string.Wed);
            case 5 :
                return getResources().getString(R.string.Thr);
            case 6 :
                return getResources().getString(R.string.Fri);
        }
        return "";
    }

    public void resetUserReviewSection(){
        rlUserReviewSection.setVisibility(View.GONE);
        tvUserName.setText("");
        tvUserDate.setText("");
        tvUserComment.setText("");
        ivUserCommentPicture.setVisibility(View.GONE);
    }
    public void initUserReviewSection(final RequestObject.ReviewItem ri){
        if(ri == null){
            rlUserReviewSection.setVisibility(View.GONE);
            btnAddReview.setVisibility(View.VISIBLE);
            return;
        }
        btnAddReview.setVisibility(View.GONE);
        rlUserReviewSection.setVisibility(View.VISIBLE);
        tvUserName.setText(LoginInfo.getInstance().username);
        loadUserProfilePicture(ivUserProfilePicture);
        tvUserDate.setText(ri.LastUpdate);
        llEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUpdateReviewPage(ri.RevId);
            }
        });

        if(!ri.Text.equals("")){
            tvUserComment.setText(ri.Text);
            tvUserComment.setVisibility(View.VISIBLE);
        }
        else
            tvUserComment.setVisibility(View.GONE);

        if(ri.Images != null && ri.Images.size()!=0 && ri.Images.get(0)!=null) {
            MyApplication.loadReviewImage(this, ivUserCommentPicture, ri.Images.get(0));
            ivUserCommentPicture.setVisibility(View.VISIBLE);
        }
        else{
            ivUserCommentPicture.setVisibility(View.GONE);
        }

        rbUserRatingBar.setRating(Float.valueOf(ri.Rate));
    }

    public void resetOverallReviewStatus(){
        rlReviewOverallSection.setVisibility(View.GONE);
        tvAverageNumber.setText("5");
        averageRateBar.setRating(5f);
        tvReviewsCount.setText("0");

        setLineWeight(view1Star, 0);
        setLineWeight(view2Star, 0);
        setLineWeight(view3Star, 0);
        setLineWeight(view4Star, 0);
        setLineWeight(view5Star, 0);
    }
    public void initOverallReviewStatus(ArrayList<RequestObject.RevStatisticInfo> revStatisticInfos){
        if(isAllZero(revStatisticInfos)) {
            rlReviewOverallSection.setVisibility(View.GONE);
            return;
        }

        rlReviewOverallSection.setVisibility(View.VISIBLE);

        float averageNumber = calculateAverage(revStatisticInfos);
        tvAverageNumber.setText(String.valueOf(averageNumber));
        averageRateBar.setRating(averageNumber);

        int reviewsCount = 0;

        for(RequestObject.RevStatisticInfo rsi : revStatisticInfos){
            reviewsCount += rsi.Count;
        }

        tvReviewsCount.setText(String.valueOf(reviewsCount));

        for(final RequestObject.RevStatisticInfo rsi : revStatisticInfos){
            switch(rsi.Star){
                case 1 :
                    setLineWeight(view1Star, calculatePercentOfLine(reviewsCount, revStatisticInfos.get(0).Count));
                    tvStar1.setText(String.valueOf(rsi.Count));
                    break;
                case 2 :
                    setLineWeight(view2Star, calculatePercentOfLine(reviewsCount, revStatisticInfos.get(1).Count));
                    tvStar2.setText(String.valueOf(rsi.Count));
                    break;
                case 3 :
                    setLineWeight(view3Star, calculatePercentOfLine(reviewsCount, revStatisticInfos.get(2).Count));
                    tvStar3.setText(String.valueOf(rsi.Count));
                    break;
                case 4 :
                    setLineWeight(view4Star, calculatePercentOfLine(reviewsCount, revStatisticInfos.get(3).Count));
                    tvStar4.setText(String.valueOf(rsi.Count));
                    break;
                case 5 :
                    setLineWeight(view5Star, calculatePercentOfLine(reviewsCount, revStatisticInfos.get(4).Count));
                    tvStar5.setText(String.valueOf(rsi.Count));
                    break;
            }
        }






    }

    private boolean isAllZero(ArrayList<RequestObject.RevStatisticInfo> revStatisticInfos) {
        for(RequestObject.RevStatisticInfo rsi : revStatisticInfos){
            if(rsi.Count!=0)
                return false;
        }
        return true;
    }

    public void resetAllReviewsSection(){
        spOrder.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, orderStrs));
        spOrder.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        currentSelectedOrder = GetReviewsForBusiness.ORDER_MOFID_TARIN;
                        break;

                    case 1:
                        currentSelectedOrder = GetReviewsForBusiness.ORDER_JADID_TARIN;
                        break;

                    case 2:
                        currentSelectedOrder = GetReviewsForBusiness.ORDER_EMTIAZE_BALA;
                        break;
                }
                currentStatus = Status.REFRESH;
                if (!isFirstTime)
                    receiveAllReviews(0, currentSelectedOrder);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        lvReviews.setAdapter(adapterReviews);
        rlAllReviewsSection.setVisibility(View.GONE);
    }
    public void initAllReviewsSection(ArrayList<RequestObject.ReviewItem> reviewItems){
        if(reviewItems==null || reviewItems.size()==0) {
            rlAllReviewsSection.setVisibility(View.GONE);
            return;
        }
        rlAllReviewsSection.setVisibility(View.VISIBLE);
        adapterReviews.resetAllItems(reviewItems);
    }

    void setLineWeight(View v, int percent){
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) v.getLayoutParams();
        v.setLayoutParams(
                new LinearLayout.LayoutParams(params.width, params.height, percent));
    }
    private void loadUserProfilePicture(ImageView ivProfile) {
        MyApplication.loadUserProfilePicture(this, ivProfile, Image_M.LARGE,
                Image_M.ImageType.USER);
    }

    public float calculateAverage(ArrayList<RequestObject.RevStatisticInfo> revStatisticInfos){
        int sumOfStars = 0;
        int countOfReviews = 0;

        for (int d=0;d<revStatisticInfos.size();d++){
            sumOfStars += (revStatisticInfos.get(d).Count * revStatisticInfos.get(d).Star);
            countOfReviews += revStatisticInfos.get(d).Count;
        }

        if(countOfReviews==0)
            return 0;
        return sumOfStars/countOfReviews;
    }
    public int calculatePercentOfLine(int sum,int count){
        if(sum==0)
            return 0;

        return 100 * (count/sum);
    }



    @Override
    public void notifyFollowBusiness(String businessId) throws Exception {

    }

    @Override
    public void notifyFollowBusinessFailed(Integer errorCode, String callerStringID) throws Exception {
        configBtnForNotFollow(btnFollowStatus);
    }

    @Override
    public void notifyUnfollowBusiness(String businessId) throws Exception {

    }

    @Override
    public void notifyUnfollowBusinessFailed(Integer errorCode, String callerStringID) throws Exception {
        configBtnForFollowed(btnFollowStatus);
    }


    public void configBtnForNotFollow(final ButtonFont button){
        button.setBackgroundColor(getResources().getColor(R.color.primaryColor));
        button.setText(getString(R.string.follow));
        button.setTextColor(getResources().getColor(R.color.white));
        button.setCompoundDrawablesWithIntrinsicBounds(
                null, null, getResources().getDrawable(R.drawable.ic_follow), null);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FollowBusiness(ActivityBusinessReviews2.this, LoginInfo.getInstance().getUserUniqueId(),
                        bizUniqueId, ActivityBusinessReviews2.this, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                configBtnForFollowed(button);
            }
        });
    }
    public void configBtnForFollowed(final ButtonFont button){
        button.setBackgroundColor(getResources().getColor(R.color.gray_light));
        button.setText(getString(R.string.followed_business_page));
        button.setTextColor(getResources().getColor(R.color.primaryColor));
        button.setCompoundDrawablesWithIntrinsicBounds(
                null, null, getResources().getDrawable(R.drawable.ic_followed), null);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new UnFollowBusiness(ActivityBusinessReviews2.this, LoginInfo.getInstance().getUserUniqueId(),
                        bizUniqueId, ActivityBusinessReviews2.this, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                configBtnForNotFollow(button);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            int reqWidth= Params.IMAGE_MAX_WIDTH_HEIGHT_PIX,reqHeight=Params.IMAGE_MAX_WIDTH_HEIGHT_PIX;

            String resizedPath = null;
            if (requestCode == ActivityCamera.CAPTURE_PHOTO) {
                String filePath = data.getStringExtra(ActivityCamera.FILE_PATH);
                resizedPath =
                        Image_M.resizeImageThenGetPath(data.getStringExtra(ActivityCamera.FILE_PATH),
                                reqWidth,reqHeight);
            } else if (requestCode == ActivityGallery.CAPTURE_GALLERY) {
                resizedPath =
                        Image_M.resizeImageThenGetPath(data.getStringExtra(ActivityCamera.FILE_PATH),
                                reqWidth,reqHeight);
                String filePath = data.getStringExtra(ActivityGallery.FILE_PATH);
            }
            sendImageToServer(resizedPath);
        }
    }

    private void sendImageToServer(String resizedPath) {
        showWaitDialog();
        new SendImage(ActivityBusinessReviews2.this, bizUniqueId, SEND_IMAGE, Image_M.getBase64String(resizedPath),
                ActivityBusinessReviews2.this, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
    }


    @Override
    public void notifyDeleteReview(String reviewId) throws Exception {
        receiveAllSections();
    }

    @Override
    public void notifyDeleteReviewFailed(String reviewIntId, String errorMessage) throws Exception {

    }

    @Override
    public void notifyUpdateReview(Review review) throws Exception {
        receiveAllSections();
    }

    @Override
    public void notifyUpdateReviewFailed(String reviewIntId, String errorMessage) throws Exception {

    }
}
