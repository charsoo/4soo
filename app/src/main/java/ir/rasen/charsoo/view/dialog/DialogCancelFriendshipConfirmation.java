package ir.rasen.charsoo.view.dialog;

import android.content.Context;
import android.view.View;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.model.friend.UnfriendUser;
import ir.rasen.charsoo.view.interface_m.ICancelFriendship;
import ir.rasen.charsoo.view.widgets.WaitDialog;
import ir.rasen.charsoo.view.widgets.material_library.widgets.Dialog;


public class DialogCancelFriendshipConfirmation extends Dialog {
    Context context;
    WaitDialog progressDialog;

    public DialogCancelFriendshipConfirmation(final Context context, final String requestedUserId, final ICancelFriendship iCancelFriendship) {
        super(context, context.getResources().getString(R.string.unfriend),
                context.getResources().getString(R.string.confirmation_unfriend));

        addCancelButton(R.string.cancel);
        setAcceptText(R.string.yes);

        setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new UnfriendUser(context,LoginInfo.getInstance().getUserUniqueId(),requestedUserId,iCancelFriendship,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
            }
        });


//        setOnAcceptButtonClickListener(new OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                //progressDialog will be closed in getResult or getError in calling class
//                progressDialog=new WaitDialog(context,context.getString(R.string.please_wait));
//                progressDialog.show();
//
////                new RequestCancelFriendship(context, LoginInfo.getUserId(context), requestedUserId, new IWebserviceResponse() {
////                    @Override
////                    public void getResult(Object result) {
////                        progressDialog.dismiss();
////                        if ((boolean) result){
////                            iCancelFriendship.notifyDeleteFriend(requestedUserId);
////                        }
////                        else{
////                            iCancelFriendship.notifyDeleteFriendFailed(ServerAnswer.getError(context, ServerAnswer.NONE_DEFINED_ERROR, ""));
////                        }
////                    }
////
////                    @Override
////                    public void getError(Integer errorCode, String callerStringID) {
////                        progressDialog.dismiss();
////                        iCancelFriendship.notifyDeleteFriendFailed(ServerAnswer.getError(context, errorCode, callerStringID + ">" + ""));
////                    }
////                }).executeWithNewSolution();;
//
//                dismiss();
//            }
//        });

    }

}
