package ir.rasen.charsoo.view.interface_m;

/**
 * Created by hossein-pc on 7/7/2015.
 */
public interface INetworkStatusListener {
    void onNetworkStatusChanged(boolean isNetworkConnected);
}
