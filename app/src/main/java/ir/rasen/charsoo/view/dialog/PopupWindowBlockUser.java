package ir.rasen.charsoo.view.dialog;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.model.business.BlockUser;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.buttons.ButtonFont;


public class PopupWindowBlockUser extends PopupWindow {
    Context context;

    public PopupWindowBlockUser(
            final Context context, final String businessId,
            final String blockingUserId, final IWebservice iWebserviceResponse,final int webserviceReqCode) {
        super(context);

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_post_report_cancel_share_more, null, false);
        setContentView(view);

        view.findViewById(R.id.btn_post_more_cancel_share).setVisibility(View.GONE);
        ButtonFont viewReport = (ButtonFont) view.findViewById(R.id.btn_post_more_report);

        viewReport.setText(R.string.block_user);

        viewReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new BlockUser(context,businessId,blockingUserId
                        ,iWebserviceResponse,webserviceReqCode, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                PopupWindowBlockUser.this.dismiss();

            }
        });

        setBackgroundDrawable(new BitmapDrawable());
        setOutsideTouchable(true);
        setWindowLayoutMode(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
    }



}
