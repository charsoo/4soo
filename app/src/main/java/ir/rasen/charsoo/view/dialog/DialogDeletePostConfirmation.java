package ir.rasen.charsoo.view.dialog;

import android.content.Context;
import android.view.View;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.model.post.DeletePost;
import ir.rasen.charsoo.view.interface_m.IDeletePost;
import ir.rasen.charsoo.view.widgets.material_library.widgets.Dialog;


public class DialogDeletePostConfirmation extends Dialog {
    Context context;

    public DialogDeletePostConfirmation(final Context context,final String businessId, final String postId, final IDeletePost iDeletePost) {
        super(context, context.getResources().getString(R.string.delete),
                context.getResources().getString(R.string.confirmation_delete_post));

        addCancelButton(R.string.cancel);
        setAcceptText(R.string.delete);

        //set onClickListener for the ok button
        setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DeletePost(context,businessId,postId,iDeletePost, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                dismiss();
            }
        });

    }

}
