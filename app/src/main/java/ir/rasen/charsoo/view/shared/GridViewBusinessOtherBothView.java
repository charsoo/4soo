package ir.rasen.charsoo.view.shared;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;

import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.TextProcessor;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.model.user.FollowBusiness;
import ir.rasen.charsoo.model.user.UnFollowBusiness;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessContactInfo;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessFollowers;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessReviews;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessReviews2;
import ir.rasen.charsoo.view.adapter.AdapterPostGridAndList;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IFollowBusiness;
import ir.rasen.charsoo.view.interface_m.IPullToRefresh;
import ir.rasen.charsoo.view.interface_m.IUnfollowBusiness;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.buttons.ButtonFont;
import ir.rasen.charsoo.view.widgets.buttons.FloatButton;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.HFGridView;

/**
 * Created by android on 3/14/2015.
 */
public class GridViewBusinessOtherBothView
        implements  IUnfollowBusiness ,IFollowBusiness{
    public static final String TAG = "GridViewBusinessOtherBothView";

    HFGridView gridViewHeader;
    //    AdapterPostGrid adapterPostGrid;
//    AdapterPost adapterPost;
    public AdapterPostGridAndList adapterPostGridAndList;

    public boolean isThreeColumn;
    boolean isLoadingMore;
    FloatButton imageViewFollowers, imageViewReviews, imageViewContactInfo;
    ImageView imageViewCover;
    TextViewFont textViewFollowersNumber, textViewIdentifier, textViewName;
    View listFooterView;
    View viewHeader;
    Activity activity;
    Business business;
    ArrayList<Post> posts;
    boolean hasHeader;
    ButtonFont buttonFollowStatus;
    IUnfollowBusiness iUnfollowBusiness;
    //    IWebserviceResponse iWebserviceResponse;
    View switchGrid, switchList;
    DialogMessage dialogMessage;
    View viewNothing;
    ImageView imgNothing;
    TextViewFont txtNothing;
    IPullToRefresh iPullToRefresh;

    public static String parentTag;

    public GridViewBusinessOtherBothView(Activity activity, Business business, HFGridView gridViewHeader,
                                         String parentTag,IPullToRefresh iPullToRefresh) {
        this.activity = activity;
        this.business = business;
        this.gridViewHeader = gridViewHeader;
        iUnfollowBusiness = this;
        dialogMessage=new DialogMessage(activity,"");
        hasHeader=false;
        isThreeColumn=true;
        isLoadingMore=false;
//        iWebserviceResponse = this;
        this.iPullToRefresh=iPullToRefresh;
        this.parentTag = parentTag;
    }

    public void notifyDataSetChanged(Post post) {
        posts.add(0, post);
        adapterPostGridAndList.resetItems(posts);
    }

    public void InitialGridViewBusiness(ArrayList<Post> postList, boolean beThreeColumn) {

        this.isThreeColumn = beThreeColumn;

        this.posts = new ArrayList<>();
//        iWebserviceResponse = this;
        for (Post post : postList) {
            posts.add(post);
        }
//        adapterPostGrid = new AdapterPostGrid(activity, searchItemPosts, business.uniqueId, Post.GetPostType.SHARE);
//        adapterPost = new AdapterPost(this, activity, posts);

        adapterPostGridAndList =
                new AdapterPostGridAndList(
                        activity,business.uniqueId,
                        Post.GetPostType.SHARE,GridViewBusinessOtherBothView.this,
                        activity,posts,isThreeColumn,parentTag);

        if (!hasHeader) {
            viewHeader = (activity).getLayoutInflater().inflate(R.layout.layout_header_business_another, null);


            switchGrid = viewHeader.findViewById(R.id.btn_switch_grid);
            switchList = viewHeader.findViewById(R.id.btn_switch_list);
            imageViewCover = (ImageView) viewHeader.findViewById(R.id.imageView_cover);
            imageViewFollowers = (FloatButton) viewHeader.findViewById(R.id.imageView_followers);
            imageViewReviews = (FloatButton) viewHeader.findViewById(R.id.imageView_reviews);
            imageViewContactInfo = (FloatButton) viewHeader.findViewById(R.id.imageView_contact_info);


            textViewFollowersNumber = (TextViewFont) viewHeader.findViewById(R.id.textView_followers_number22);
            buttonFollowStatus = (ButtonFont) viewHeader.findViewById(R.id.btn_follow_satus);
            textViewIdentifier = (TextViewFont) viewHeader.findViewById(R.id.textView_business_identifier);
            textViewName = (TextViewFont) viewHeader.findViewById(R.id.textView_business_name);

            viewNothing = viewHeader.findViewById(R.id.view_nothing);
            txtNothing = (TextViewFont) viewHeader.findViewById(R.id.txt_nothing);
            imgNothing = (ImageView) viewHeader.findViewById(R.id.img_nothing);
            viewNothing.setVisibility(View.GONE);

            textViewIdentifier.setText(String.valueOf(business.businessIdentifier));
            textViewName.setText(String.valueOf(business.name));
            textViewFollowersNumber.setText(TextProcessor.numberToMiniNum(business.followersNumber));

            SimpleLoader simpleLoader = new SimpleLoader(activity);
            simpleLoader.loadImage(business.profilePictureUniqueId, Image_M.LARGE, Image_M.ImageType.BUSINESS, imageViewCover);

            if (business.isFollowing) {
                configBtnForFollowed(buttonFollowStatus);
            } else {
                configBtnForNotFollow(buttonFollowStatus);
            }

            buttonFollowStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (business.isFollowing) {
                        configBtnForNotFollow(buttonFollowStatus);
                        business.isFollowing = false;
                        ((CharsooActivity)activity).showWaitDialog();
                        new UnFollowBusiness(
                                activity,LoginInfo.getInstance().getUserUniqueId(),
                                business.uniqueId, iUnfollowBusiness,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
                    } else {
                        configBtnForFollowed(buttonFollowStatus);
                        business.isFollowing = true;
                        ((CharsooActivity)activity).showWaitDialog();
                        new FollowBusiness(
                                activity,LoginInfo.getInstance().getUserUniqueId(),
                                business.uniqueId, GridViewBusinessOtherBothView.this,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
                    }
                }
            });

            imageViewFollowers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent1 = new Intent(activity, ActivityBusinessFollowers.class);
                    intent1.putExtra(Params.BUSINESS_UNIQUE_ID, business.uniqueId);
                    intent1.putExtra(Params.USER_UNIQUE_ID_STR, business.userUniqueId);
                    intent1.putExtra(Params.BUSINESS_OWNER, false);
                    activity.startActivity(intent1);
                }
            });
            imageViewReviews.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent1 = new Intent(activity, ActivityBusinessReviews2.class);
                    intent1.putExtra(Params.BUSINESS_UNIQUE_ID, business.uniqueId);
                    intent1.putExtra(Params.BUSINESS_OWNER, false);
                    activity.startActivity(intent1);
                }
            });
            imageViewContactInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, ActivityBusinessContactInfo.class);
                    Globals.getInstance().getValue().businessVisiting = business;
                    activity.startActivity(intent);
                }
            });

            switchList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gridViewHeader.setNumColumns(1);
                    gridViewHeader.setColumnWidth(activity.getWindowManager().getDefaultDisplay().getWidth());

                    //now it has one column
                    isThreeColumn = false;
                    adapterPostGridAndList.setViewType(isThreeColumn);

                    switchList.setBackgroundColor(activity.getResources().getColor(R.color.lightPrimaryColor));
                    switchGrid.setBackgroundColor(activity.getResources().getColor(R.color.material_gray_light));
                }
            });
            switchGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gridViewHeader.setNumColumns(3);
                    // now it has three column
                    isThreeColumn = true;
                    adapterPostGridAndList.setViewType(isThreeColumn);

                    switchGrid.setBackgroundColor(activity.getResources().getColor(R.color.lightPrimaryColor));
                    switchList.setBackgroundColor(activity.getResources().getColor(R.color.material_gray_light));
                }
            });

            gridViewHeader.addHeaderView(viewHeader);
            hasHeader = true;

            listFooterView = ((LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_loading_more, null, false);
            //listFooterView.setVisibility(View.GONE);
            gridViewHeader.addFooterView(listFooterView);
        } else {
            listFooterView.setVisibility(View.GONE);
        }
        gridViewHeader.setBackgroundColor(Color.parseColor("#ffffff"));

        if (isThreeColumn) {
//            gridViewHeader.setAdapter(adapterPostGrid);
            prepareGridThreeColumn(gridViewHeader);

            switchGrid.setBackgroundColor(activity.getResources().getColor(R.color.lightPrimaryColor));
            switchList.setBackgroundColor(activity.getResources().getColor(R.color.material_gray_light));
        } /*else {
            gridViewHeader.setAdapter(adapterPost);
        }*/

        gridViewHeader.setAdapter(adapterPostGridAndList);

        gridViewHeader.setOnScrollListener(new AbsListView.OnScrollListener() {
            int currentFirstVisibleItem,
                    currentVisibleItemCount,
                    currentScrollState;

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
            }

            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            private void isScrollCompleted() {
                if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
                    if (!isLoadingMore
                            && posts.size() > 0 && posts.size() % activity.getResources().getInteger(R.integer.lazy_load_limitation) == 0) {
                        loadMoreData();
                    }
                }
            }
        });

    }

    // LOAD MORE DATA
    public void loadMoreData() {
        // LOAD MORE DATA HERE...
        isLoadingMore = true;
        listFooterView.setVisibility(View.VISIBLE);
        iPullToRefresh.notifyLoadMore();
//        new GetBusinessPosts(activity, LoginInfo.getUserId(activity), business.uniqueId, posts.get(posts.size() - 1).uniqueId, activity.getResources().getInteger(R.integer.lazy_load_limitation), GridViewBusinessOtherBothView.this,GET_BUSINESS_POSTS_INT_CODE,business.profilePictureUniqueId).executeWithNewSolution();;
    }

    public void addMorePosts(ArrayList<Post> newPosts){
        posts.addAll(newPosts);
        if(posts.size()==0) {
            viewNothing.setVisibility(View.VISIBLE);
        } else {
            viewNothing.setVisibility(View.GONE);
        }
        listFooterView.setVisibility(View.GONE);
        adapterPostGridAndList.loadMoreItems(newPosts);
        isLoadingMore = false;
    }

    public void resetPostItems(ArrayList<Post> newPosts){
        posts=new ArrayList<>(newPosts);
        adapterPostGridAndList.resetItems(posts);
    }

    private void prepareGridThreeColumn(HFGridView gridViewHeader) {
        gridViewHeader.setNumColumns(3);
        gridViewHeader.setVerticalSpacing(3);
        gridViewHeader.setHorizontalSpacing(9);
        gridViewHeader.setViewWidthIfItsZero(activity.getWindowManager().getDefaultDisplay().getWidth());
    }

    @Override
    public void notifyFollowBusiness(String businessId) throws Exception {
        ((CharsooActivity)activity).hideWaitDialog();

        configBtnForFollowed(buttonFollowStatus);
        business.isFollowing = true;
        business.followersNumber++;
        textViewFollowersNumber.setText(String.valueOf(business.followersNumber));

        MyApplication.broadCastReloadTimeline();
        MyApplication.boradCastFollowStateChanged(businessId);
    }

    @Override
    public void notifyFollowBusinessFailed(Integer errorCode, String callerStringID) throws Exception {
        ((CharsooActivity)activity).hideWaitDialog();

        if (!dialogMessage.isShowing())
        {
            dialogMessage.show();
            dialogMessage.setMessage(ServerAnswer.getError(activity, errorCode, callerStringID + ">" + TAG));
        }
        configBtnForNotFollow(buttonFollowStatus);
        business.isFollowing = false;
    }

    @Override
    public void notifyUnfollowBusiness(String businessId) {
        ((CharsooActivity)activity).hideWaitDialog();

        configBtnForNotFollow(buttonFollowStatus);
        business.isFollowing = false;
        business.followersNumber--;
        textViewFollowersNumber.setText(String.valueOf(business.followersNumber));

        MyApplication.broadCastReloadTimeline();
        MyApplication.boradCastFollowStateChanged(businessId);
    }

    @Override
    public void notifyUnfollowBusinessFailed(Integer errorCode, String callerStringID) {
        ((CharsooActivity)activity).hideWaitDialog();

        if (!dialogMessage.isShowing()){
            dialogMessage.show();
            dialogMessage.setMessage(ServerAnswer.getError(activity, errorCode, callerStringID + ">" + TAG));
        }
        configBtnForFollowed(buttonFollowStatus);
        business.isFollowing = true;
    }

    public void refreshBusinessData(Business newBusiness) {
        if (!this.business.profilePictureUniqueId.equals(newBusiness.profilePictureUniqueId)) {
            this.business = newBusiness;
            SimpleLoader simpleLoader = new SimpleLoader(activity);
            simpleLoader.loadImage(business.profilePictureUniqueId, Image_M.LARGE, Image_M.ImageType.BUSINESS, imageViewCover);
        } else
            this.business = newBusiness;

        textViewIdentifier.setText(String.valueOf(business.businessIdentifier));
        textViewName.setText(String.valueOf(business.name));
        textViewFollowersNumber.setText(TextProcessor.numberToMiniNum(business.followersNumber));

        if (business.isFollowing) {
            configBtnForFollowed(buttonFollowStatus);
        } else {
            configBtnForNotFollow(buttonFollowStatus);
        }

    }

    public void updatePost(Post post) {
        for (int i = 0; i < posts.size(); i++) {
            if (posts.get(i).uniqueId.equals(post.uniqueId)) {
                posts.set(i, post);
                break;
            }
        }
    }

    public void configBtnForNotFollow(ButtonFont button){
        button.setBackgroundColor(activity.getResources().getColor(R.color.primaryColor));
        button.setText(activity.getString(R.string.follow));
        button.setTextColor(activity.getResources().getColor(R.color.white));
        button.setCompoundDrawablesWithIntrinsicBounds(
                null, null, activity.getResources().getDrawable(R.drawable.ic_follow), null);
    }
    public void configBtnForFollowed(ButtonFont button){
        button.setBackgroundColor(activity.getResources().getColor(R.color.gray_light));
        button.setText(activity.getString(R.string.followed_business_page));
        button.setTextColor(activity.getResources().getColor(R.color.primaryColor));
        button.setCompoundDrawablesWithIntrinsicBounds(
                null, null, activity.getResources().getDrawable(R.drawable.ic_followed), null);
    }
}
