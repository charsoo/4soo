package ir.rasen.charsoo.view.activity.business;

/**
 * Created by android on 3/28/2015.
 */


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.flurry.android.FlurryAgent;

import java.io.File;
import java.util.Hashtable;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.GetContactData;
import ir.rasen.charsoo.controller.helper.GetInstalledApps;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.GlobalContactPassingClass;
import ir.rasen.charsoo.model.business.GetBusinessStrIdAvailability;
import ir.rasen.charsoo.model.business.RegisterBusiness;
import ir.rasen.charsoo.model.user.Login;
import ir.rasen.charsoo.view.activity.ActivityCamera;
import ir.rasen.charsoo.view.activity.ActivityGallery;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.dialog.PopupSelectCameraGallery;
import ir.rasen.charsoo.view.fragment.FragmentUserBusinesses;
import ir.rasen.charsoo.view.fragment.business_edit.FragmentBusinessEditLocationInfo;
import ir.rasen.charsoo.view.fragment.business_register.FragmentBusinessRegisterPageOne;
import ir.rasen.charsoo.view.fragment.business_register.FragmentBusinessRegisterPageThree;
import ir.rasen.charsoo.view.fragment.business_register.FragmentBusinessRegisterPageTwo;
import ir.rasen.charsoo.view.fragment.invite.FragmentInvite;
import ir.rasen.charsoo.view.interface_m.IGetCallForTakePicture;
import ir.rasen.charsoo.view.interface_m.IGetContactListener;
import ir.rasen.charsoo.view.interface_m.IGetInstalledAppsListener;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.TextViewFontActionBarTitle;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.edittexts.EditTextFont;

public class ActivityBusinessRegister
        extends CharsooActivity
        implements IWebservice, IGetCallForTakePicture,
        IGetContactListener,IGetInstalledAppsListener {

    public static final int CHECK_ID_AVAILABILITY_INT_CODE=11,REGISTER_BUSINESS_INT_CODE=14;

    public static final String FRAG_ONE="FragOne";
    public static final String FRAG_TWO="FragTwo";
    public static final String FRAG_THREE="FragThree";
    public static final String FRAG_INVITE="FragInvite";
    public EditTextFont edittext_name;


    FragmentBusinessRegisterPageOne fragOne;
    FragmentBusinessRegisterPageTwo fragTwo;
    FragmentBusinessRegisterPageThree fragThree;

    String filePath;
    String currentFragment;

    Business unregisteredBusiness;
    FragmentManager fm;
    FragmentTransaction ft;
    MenuItem menuItemNext;
    DialogMessage dialogMessage;

    boolean isCheckingIdAvailability;

    View actionBarView;

    Context contextActivity;

    public static boolean isComesFromBusinessEditPage = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.activity_business_register_new);
        setTitle(getString(R.string.txt_BusinessRegister));

        contextActivity = ActivityBusinessRegister.this;

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        actionBarView = inflater.inflate(R.layout.layout_action_bar_home, null);

        unregisteredBusiness=new Business();
//        progressDialog = new WaitDialog(this);
//        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        dialogMessage=new DialogMessage(this,"");

        fragOne=new FragmentBusinessRegisterPageOne();
        fragTwo=new FragmentBusinessRegisterPageTwo();
        fragThree=new FragmentBusinessRegisterPageThree();

        //Reset All Data
        FragmentBusinessRegisterPageTwo.selectedCategory = null;
        FragmentBusinessRegisterPageTwo.selectedSubcategory = null;
        FragmentBusinessEditLocationInfo.selectedState = null;
        FragmentBusinessEditLocationInfo.selectedCity = null;

        boolean blnOpenInvitePage =false;

        try {
            blnOpenInvitePage = getIntent().getExtras().getBoolean("please_open_invite",false);
        } catch (Exception e) {
            blnOpenInvitePage = false;
        }
        if(blnOpenInvitePage) {
            String businessStrId = getIntent().getExtras().getString("business_str_id");
            String businessName = getIntent().getExtras().getString("business_name");
            isComesFromBusinessEditPage = true;
            openInvitePage(businessStrId,businessName);
            return;
        }


        fm = getSupportFragmentManager();
        ft = fm.beginTransaction();
        currentFragment=FRAG_ONE;
        ft.replace(R.id.fragmentContainer, fragOne);
        ft.commit();


        ((TextViewFontActionBarTitle) actionBarView.findViewById(R.id.textView_title)).setText(getString(R.string.register_business));

        (new Handler()).post(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    if (GlobalContactPassingClass.getInstance().getAppList()==null)
                        new GetInstalledApps(ActivityBusinessRegister.this,ActivityBusinessRegister.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    if ((GlobalContactPassingClass.getInstance().getEmailContacts()==null)||(GlobalContactPassingClass.getInstance().getNumberContacts()==null))
                        new GetContactData(ActivityBusinessRegister.this,ActivityBusinessRegister.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }else {
                    if (GlobalContactPassingClass.getInstance().getAppList()==null)
                        new GetInstalledApps(ActivityBusinessRegister.this,ActivityBusinessRegister.this).execute();
                    if ((GlobalContactPassingClass.getInstance().getEmailContacts()==null)||(GlobalContactPassingClass.getInstance().getNumberContacts()==null))
                        new GetContactData(ActivityBusinessRegister.this,ActivityBusinessRegister.this).execute();
                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        if(isComesFromBusinessEditPage) {
            finish();
            return;
        }

        switch (currentFragment) {
            case FRAG_ONE:
                super.onBackPressed();
                finish();
                break;
            case FRAG_TWO:
                ft = fm.beginTransaction();
                currentFragment=FRAG_ONE;
                ft.replace(R.id.fragmentContainer, fragOne);
                ft.commit();
                break;
            case FRAG_THREE:
                ft = fm.beginTransaction();
                currentFragment=FRAG_TWO;
                ft.replace(R.id.fragmentContainer, fragTwo);
                ft.commit();
                menuItemNext.setIcon(R.drawable.ic_arrow_next_white_24dp);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_next_button, menu);
        this.menuItemNext = menu.findItem(R.id.action_next);


        if(isComesFromBusinessEditPage){
            menuItemNext.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.action_next) {

            if (isComesFromBusinessEditPage){
                finish();
                return true;
            }

            switch (currentFragment) {
                case FRAG_ONE:
                    // go fragment two
                    goSecondPageOnNextPressed();
                    break;
                case FRAG_TWO:
                    // GO FRAGMNET TWO
                    goThirdPageOnNextPressed();
                    break;
                case FRAG_THREE:
                    // DO REGISTER BUSINESS
                    // BEFORE REGISTER  CHECK FOR STRING ID AVAILABILITY
                    checkIdThenRegister();
                    break;
                case FRAG_INVITE:
                    Globals.getInstance().getValue().businessVisiting = unregisteredBusiness;
                    Intent i = getIntent();
                    setResult(RESULT_OK, i);
                    finish(false);
                    Business.gotoBusinessPage(contextActivity, unregisteredBusiness.uniqueId);
                    break;
            }
            return true;
        } else return super.onOptionsItemSelected(item);
    }

    @Override
    public void getResult(int request, Object result) {
        switch (request){
            case CHECK_ID_AVAILABILITY_INT_CODE:
                if ((Boolean)result)
                {
                    isCheckingIdAvailability=false;
                    if (currentFragment.equals(FRAG_THREE)){
                        new RegisterBusiness(
                                ActivityBusinessRegister.this, unregisteredBusiness,
                                ActivityBusinessRegister.this,REGISTER_BUSINESS_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                    }
                    else if (currentFragment.equals(FRAG_ONE)){
                        ((CharsooActivity)contextActivity).hideWaitDialog();
                        ft= fm.beginTransaction();
                        currentFragment=FRAG_TWO;
                        ft.replace(R.id.fragmentContainer,fragTwo);
                        ft.commit();
                    }
                }
                else{
                    hideWaitDialog();
                    isCheckingIdAvailability=false;
                    if (!currentFragment.equals(FRAG_ONE)){
                        menuItemNext.setIcon(R.drawable.ic_arrow_next_white_24dp);
                        fragOne.isBackedForId=true;
                        ft = fm.beginTransaction();
                        currentFragment=FRAG_ONE;
                        ft.replace(R.id.fragmentContainer, fragOne);
                        ft.commit();
                    }
                    else{
                        hideWaitDialog();
                        fragOne.setErrorStringIdIsNotAvailable();
                    }
                }
                break;
            case REGISTER_BUSINESS_INT_CODE:
                hideWaitDialog();
                setTitle(getString(R.string.business_invite_friends));
                removeBackOnActionbar();
                FragmentUserBusinesses.onBusinessCreatedListener.onBusinessCreated();


                FragmentInvite fragInvite=FragmentInvite.newInstance(ActivityBusinessRegister.this,
                        LoginInfo.getInstance().getUserUniqueId(),
                        true,
                        getString(R.string.txt_BusinessPromoteFirstTabTitle),
                        getString(R.string.txt_BusinessPromoteSecondTabTitle),
                        "",
                        "sms",
                        "email",
                        GlobalContactPassingClass.getInstance().getEmailContacts(),
                        GlobalContactPassingClass.getInstance().getNumberContacts(),
                        GlobalContactPassingClass.getInstance().getAppList());


                ft= fm.beginTransaction();
                currentFragment=FRAG_INVITE;
                ft.replace(R.id.fragmentContainer,fragInvite).commit();
                unregisteredBusiness.uniqueId =(String) result;
                break;
        }
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
        hideWaitDialog();
        if(!dialogMessage.isShowing()){
            dialogMessage.show();
            dialogMessage.setMessage(ServerAnswer.getError(ActivityBusinessRegister.this, errorCode,callerStringID+">"+this.getLocalClassName()));
        }
    }


    @Override
    public void notifyCallForTakePicture() {
        new PopupSelectCameraGallery(ActivityBusinessRegister.this).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == ActivityCamera.CAPTURE_PHOTO) {
                filePath = data.getStringExtra(ActivityCamera.FILE_PATH);
                displayCropedImage(filePath);
            } else if (requestCode == ActivityGallery.CAPTURE_GALLERY) {
                filePath = data.getStringExtra(ActivityGallery.FILE_PATH);
                displayCropedImage(filePath);
            }

        }

    }

    private void displayCropedImage(String filePath) {
        File file = new File(filePath);
        if (file.exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            try {
                fragOne.setPicture(myBitmap);
                unregisteredBusiness.profilePicture = Image_M.getBase64String(filePath);
            } catch (Exception e) {
                String s = e.getMessage();
            }
        }
    }


    public void goSecondPageOnNextPressed(){
        isCheckingIdAvailability=true;
        Hashtable<String,String> tempInputData=fragOne.getInputData();
        if (tempInputData!=null) {
//            progressDialog.show();
            showWaitDialog();
            unregisteredBusiness.businessIdentifier=tempInputData.get(Params.STRING_IDENTIFIER);
            unregisteredBusiness.name=tempInputData.get(Params.FULL_NAME);
        }
    }

    public void goThirdPageOnNextPressed(){
        if (fragTwo.isVerified()){
            unregisteredBusiness.categoryName =fragTwo.selectedCategory.categoryName;
            unregisteredBusiness.categoryUniqueId =fragTwo.selectedCategory.categoryUniqueId;
            unregisteredBusiness.subcategoryName =fragTwo.selectedSubcategory.subCategoryName;
            unregisteredBusiness.subCategoryUniqueId =fragTwo.selectedSubcategory.subCaregoryUniqueId;
            unregisteredBusiness.description=fragTwo.getInputDescription();
            unregisteredBusiness.hashtagList=fragTwo.getHashtagList();
            ft = fm.beginTransaction();
            currentFragment=FRAG_THREE;
            ft.replace(R.id.fragmentContainer, fragThree);
            ft.commit();
            menuItemNext.setIcon(R.drawable.ic_check_white_24dp);
        }

    }

    private void checkIdThenRegister(){
        Business tempData = fragThree.getInputData();
        if (tempData!=null){
            unregisteredBusiness.stateName =tempData.stateName;
            unregisteredBusiness.cityName =tempData.cityName;
            unregisteredBusiness.stateUniqueId =tempData.stateUniqueId;
            unregisteredBusiness.cityUniqueId =tempData.cityUniqueId;
            unregisteredBusiness.address = tempData.address;
            unregisteredBusiness.location_m=tempData.location_m;


            showWaitDialog();
            new GetBusinessStrIdAvailability(
                    this,unregisteredBusiness.businessIdentifier,
                    ActivityBusinessRegister.this,CHECK_ID_AVAILABILITY_INT_CODE, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

        }
    }

    @Override
    public void getPhoneContacts() {
//        GlobalContactPassingClass.getInstance().setEmailContacts(emailContacts);
//        GlobalContactPassingClass.getInstance().setNumberContacts(numberContacts);
    }

    @Override
    public void setAppResults() {
//        GlobalContactPassingClass.getInstance().setAppList(appList);
    }

    public void openInvitePage(String businessStrId,String businessName){
        if(!isComesFromBusinessEditPage)
            return;

        setTitle(getString(R.string.business_invite_friends));
//        removeBackOnActionbar();
        if(menuItemNext!=null) {
            menuItemNext.setEnabled(false);
            menuItemNext.setVisible(false);
        }
        fm = getSupportFragmentManager();
        ft = fm.beginTransaction();


        FragmentInvite fragInvite=FragmentInvite.newInstance(ActivityBusinessRegister.this,
                LoginInfo.getInstance().getUserUniqueId(),
                true,
                getString(R.string.txt_BusinessPromoteFirstTabTitle),
                getString(R.string.txt_BusinessPromoteSecondTabTitle),
                "",
                "sms",
                "email",
                GlobalContactPassingClass.getInstance().getEmailContacts(),
                GlobalContactPassingClass.getInstance().getNumberContacts(),
                GlobalContactPassingClass.getInstance().getAppList());

        FragmentInvite.Business_id = businessStrId;
        FragmentInvite.Business_name = businessName;

        Bundle bundle = new Bundle();

        bundle.putBoolean("email_sms_format_two", true);
        fragInvite.setArguments(bundle);


        ft= fm.beginTransaction();
        currentFragment=FRAG_INVITE;
        ft.replace(R.id.fragmentContainer,fragInvite).commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this,"2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}