package ir.rasen.charsoo.view.activity.user;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;

import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.GetContactData;
import ir.rasen.charsoo.controller.helper.GetInstalledApps;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.ContactEntry;
import ir.rasen.charsoo.controller.object.GlobalContactPassingClass;
import ir.rasen.charsoo.controller.object.PackageInfoCustom;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.model.user.GetUserStrIdAvailability;
import ir.rasen.charsoo.model.user.RegisterUser;
import ir.rasen.charsoo.view.activity.ActivityCamera;
import ir.rasen.charsoo.view.activity.ActivityGallery;
import ir.rasen.charsoo.view.activity.ActivityLogin;
import ir.rasen.charsoo.view.activity.ActivityMain;
import ir.rasen.charsoo.view.activity.ActivitySplash;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.fragment.invite.FragmentInvite;
import ir.rasen.charsoo.view.fragment.user_register.FragmentUserRegisterPageOne;
import ir.rasen.charsoo.view.fragment.user_register.FragmentUserRegisterPageTwo;
import ir.rasen.charsoo.view.interface_m.IGetContactListener;
import ir.rasen.charsoo.view.interface_m.IGetInstalledAppsListener;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;

public class ActivityUserRegister extends
        CharsooActivity implements
        IWebservice,IGetContactListener,IGetInstalledAppsListener {


    private static final int CHECK_ID_AVAILABILITY_INT_CODE=11,REGISTER_USER_INT_CODE=14;

    public static final String FIRST_PAGE="FirstPage";
    public static final String SECOND_PAGE="SecondPage";
    public static final String THIRD_PAGE_OFFER_FRIEND="ThirdPage";
    FragmentTransaction ft;
    FragmentUserRegisterPageOne fragOne;
    FragmentUserRegisterPageTwo fragTwo;
    FragmentInvite fragThree_OfferFriends;
    String userPictureString,userFullName,userStringId,userEmail,userPassword,userPhoneNumber;
    String filePath,currentFragment;
    Bitmap tempUserPicture;
    ImageView nextMenu;
    TextViewFont Title;
    public int userIntId;
//    public ArrayList<ContactEntry> noneCharsooEmailContactsList, noneCharsooPhoneNumberContactsList, charsooContactsList;
    public Hashtable<String,ArrayList<ContactEntry>> emailContacts,numberContacts;
    public ArrayList<PackageInfoCustom> appList;

    FragmentManager fm;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.activity_user_register);
        setTitle(getString(R.string.str_RegisterInCharsoo));
        Title = (TextViewFont) findViewById(R.id.Title);
        Title.setText(getString(R.string.str_RegisterInCharsoo));



        (new Handler()).post(new Runnable() {
            @Override
            public void run() {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new GetInstalledApps(ActivityUserRegister.this, ActivityUserRegister.this).
                            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    new GetContactData(ActivityUserRegister.this, ActivityUserRegister.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    GetContactData.isRunning = true;
                } else {
                    new GetContactData(ActivityUserRegister.this, ActivityUserRegister.this).execute();
                    new GetInstalledApps(ActivityUserRegister.this, ActivityUserRegister.this).execute();
                    GetContactData.isRunning = true;
                }

            }
        });

        fragOne=new FragmentUserRegisterPageOne();
        fragTwo=new FragmentUserRegisterPageTwo();
//        fragThree_OfferFriends =new FragmentInvite();

        fm=getSupportFragmentManager();
        ft=fm.beginTransaction();
        ft.replace(R.id.fragmentContainer, fragOne);
        currentFragment=FIRST_PAGE;
        ft.commit();
//        showWaitDialog();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (GlobalContactPassingClass.getInstance().getAppList()==null)
                new GetInstalledApps(ActivityUserRegister.this,ActivityUserRegister.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            if ((GlobalContactPassingClass.getInstance().getEmailContacts()==null)||(GlobalContactPassingClass.getInstance().getNumberContacts()==null))
                new GetContactData(ActivityUserRegister.this,ActivityUserRegister.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }else {
            if (GlobalContactPassingClass.getInstance().getAppList()==null)
                new GetInstalledApps(ActivityUserRegister.this,ActivityUserRegister.this).execute();
            if ((GlobalContactPassingClass.getInstance().getEmailContacts()==null)||(GlobalContactPassingClass.getInstance().getNumberContacts()==null))
                new GetContactData(ActivityUserRegister.this,ActivityUserRegister.this).execute();
        }

        nextMenu = (ImageView) findViewById(R.id.img_sumbit);
        findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (currentFragment) {
                    case FIRST_PAGE:
                        switchToSecondPage();

                        break;
                    case SECOND_PAGE:
                        //register User
                        doRegisterUser();
                        break;
                    case THIRD_PAGE_OFFER_FRIEND:
                        Intent intent = new Intent(ActivityUserRegister.this,ActivityMain.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish(false);
                        break;

                }
            }
        });

//            getContactList();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == ActivityCamera.CAPTURE_PHOTO) {
                filePath = data.getStringExtra(ActivityCamera.FILE_PATH);
                displayCropedImage(filePath);
            } else if (requestCode == ActivityGallery.CAPTURE_GALLERY) {
                filePath = data.getStringExtra(ActivityGallery.FILE_PATH);
                displayCropedImage(filePath);
            }
        }

    }

    private void displayCropedImage(String filePath) {
        File file = new File(filePath);
        if (file.exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            if (myBitmap!=null) {
                tempUserPicture = myBitmap;
                fragOne.setProfileImage(tempUserPicture);
                userPictureString = Image_M.getBase64String(filePath);
                fragOne.setUserImageString(userPictureString);
            }
        }
    }

    public void back(View view) {
        onBackPressed();
    }


    @Override
    public void onBackPressed() {
        switch (currentFragment) {
            case FIRST_PAGE:
                // back to login page
                Intent i= new Intent(this, ActivitySplash.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                this.startActivity(i);
                finish();
                break;
            case SECOND_PAGE:
                //back to prev fragment

                ft=fm.beginTransaction();
                ft.replace(R.id.fragmentContainer,fragOne);

                currentFragment=FIRST_PAGE;
                ft.commit();

                nextMenu.setImageResource(R.drawable.ic_arrow_next_white_24dp);

                break;
            case THIRD_PAGE_OFFER_FRIEND:
//                if (!fragThree_OfferFriends.ifHideEmailList()){
//                    Intent intent = new Intent(this, ActivityLogin.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    this.startActivity(intent);
//                }
                fragThree_OfferFriends.ifHideEmailList();
                break;
            default:
                finish();
                break;

        }
    }



    public void onDoneButtonPressed(String callerStringId)
    {
        if (callerStringId.equals(FIRST_PAGE))
        {
            switchToSecondPage();
        }
        else{
            doRegisterUser();
        }

    }


    private void switchToSecondPage(){
        String[] tempStringOne = fragOne.getInputData();
        if (tempStringOne!=null) {
            userFullName = tempStringOne[0];
            userStringId = tempStringOne[1];
            showWaitDialog();
            new GetUserStrIdAvailability(ActivityUserRegister.this,userStringId,ActivityUserRegister.this,CHECK_ID_AVAILABILITY_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
        }
    }

    private void doRegisterUser(){
        String[] tempStringTwo=fragTwo.getInputData();
        if (tempStringTwo!=null){
            userEmail=tempStringTwo[0];
            userPassword=tempStringTwo[1];
            userPhoneNumber=tempStringTwo[2];
            //now register business
            User user = new User();
            user.userIdentifier = userStringId;
            user.name = userFullName;
            user.email = userEmail;
            user.password = userPassword;
            user.profilePicture = userPictureString;
            if(user.profilePicture==null)
                user.profilePicture = "";
            user.phoneNumber=userPhoneNumber;

            showWaitDialog();
            new RegisterUser(
                    ActivityUserRegister.this,user,
                    ActivityUserRegister.this,REGISTER_USER_INT_CODE).exectueWithNewSolution();;
        }
    }


    @Override
    public void getPhoneContacts() {
//        fragThree_OfferFriends.setPhoneContactsHashtabe(this,emailContacts, numberContacts);
//        ActivityUserRegister.this.emailContacts=new Hashtable<>(emailContacts);
//        ActivityUserRegister.this.numberContacts=new Hashtable<>(numberContacts);
    }

    @Override
    public void setAppResults() {
//        fragThree_OfferFriends.setApplicationList(appList);
//        ActivityUserRegister.this.appList=new ArrayList<>(appList);
    }

    @Override
    public void getResult(int request, Object result) {
        hideWaitDialog();
        switch (request){
            case CHECK_ID_AVAILABILITY_INT_CODE:
                if ((boolean) result){
                    ft = fm.beginTransaction();
                    ft.replace(R.id.fragmentContainer, fragTwo);
                    currentFragment = SECOND_PAGE;
                    ft.commit();
                    nextMenu.setImageResource(R.drawable.ic_check_white_24dp);
                }
                else
                {
                    fragOne.setErrorStringIdIsNotAvailable();
                }
                break;
            case REGISTER_USER_INT_CODE:
                Title.setText(getString(R.string.invite_friends));
                fragThree_OfferFriends= FragmentInvite.newInstance(this,
                        LoginInfo.getInstance().getUserUniqueId(),
                        false,
                        getString(R.string.txt_FindFriends),
                        getString(R.string.txt_InviteFriends),
                        getString(R.string.txt_sendSMS),
                        "SMS Message",
                        "Email Message",
                        GlobalContactPassingClass.getInstance().getEmailContacts(),
                        GlobalContactPassingClass.getInstance().getNumberContacts(),
                        GlobalContactPassingClass.getInstance().getAppList()
                );

//                ActivityUserRegister.THIRD_PAGE_OFFER_FRIEND
                ft=fm.beginTransaction();
                ft.replace(R.id.fragmentContainer, fragThree_OfferFriends);
                currentFragment=THIRD_PAGE_OFFER_FRIEND;
                ft.commit();
                removeActionBarBackButton();
                break;
        }

    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
        hideWaitDialog();
        if (errorCode==ServerAnswer.DUPLICATE_USER_STRING_ID && request==REGISTER_USER_INT_CODE){
            fragOne.isBackedForID=true;
            ft=fm.beginTransaction();
            Toast.makeText(ActivityUserRegister.this,"این نام کاربری قبلا وارد شده است",Toast.LENGTH_LONG).show();
            ft.replace(R.id.fragmentContainer, fragOne);
            currentFragment=FIRST_PAGE;
            ft.commit();
            fragOne.setErrorStringIdIsNotAvailable();
            nextMenu.setImageResource(R.drawable.ic_arrow_next_white_24dp);
        }else
            new DialogMessage(ActivityUserRegister.this, ServerAnswer.getError(ActivityUserRegister.this, errorCode, callerStringID + ">" + this.getLocalClassName())).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
