package ir.rasen.charsoo.view.interface_m;

/**
 * Created by Rasen_iman on 7/30/2015.
 */
public interface IOnBusinessEditedListener {

    void onBusinessEdited(String editedBusinessId);
}
