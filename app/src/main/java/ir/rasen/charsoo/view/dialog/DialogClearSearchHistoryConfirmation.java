package ir.rasen.charsoo.view.dialog;

import android.content.Context;
import android.view.View;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.view.widgets.material_library.widgets.Dialog;


public class DialogClearSearchHistoryConfirmation extends Dialog {
    Context context;

    public DialogClearSearchHistoryConfirmation(final Context context) {
        super(context, context.getResources().getString(R.string.clear),
                context.getResources().getString(R.string.confirmation_search_history));

        addCancelButton(R.string.cancel);
        setAcceptText(R.string.yes);

        //set onClickListener for the ok button
        setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User.clearSearchHistory(context);
                dismiss();
            }
        });

    }

}
