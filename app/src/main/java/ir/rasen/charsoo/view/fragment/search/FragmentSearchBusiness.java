package ir.rasen.charsoo.view.fragment.search;

/**
 * Created by Sina KH on 6/23/2015.
 */

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.BaseAdapterItem;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.SuggestedBusiness;
import ir.rasen.charsoo.model.business.GetSuggestedBusiness;
import ir.rasen.charsoo.model.search.SearchBusinessesLocation;
import ir.rasen.charsoo.view.activity.ActivityMain;
import ir.rasen.charsoo.view.adapter.AdapterBusinessSearchResult;
import ir.rasen.charsoo.view.adapter.AdapterSpecialBusinesses;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IUnfollowBusiness;
import ir.rasen.charsoo.view.interface_m.IWebservice;

public class  FragmentSearchBusiness extends Fragment implements IWebservice, IUnfollowBusiness {
    public static final String TAG = "FragmentSearchBusiness";

    private static String SPACE_HEIGHT = "space_height";

    private ListView listView;
    private AdapterBusinessSearchResult adaterResult;
    private AdapterSpecialBusinesses adapterSpecialBusinesses;

    private ArrayList<BaseAdapterItem> searchResults;
    private boolean showingResults = false;
    private String currentResultIsFor = "";
    private Context contextActivity;
    LatLng latLng;
    private boolean isLoadingMoreAds=false;

    ArrayList<SuggestedBusiness> businessAds = new ArrayList<>();

    public static final int SEARCH_BUSINESS_LOCATION_REQUEST = 100;
    public static final int GET_BURST_ADS_INT_CODE=120;
    boolean searchCatchedEnd=false,adsCatchedEnd=false;


    public static FragmentSearchBusiness newInstance() {
        FragmentSearchBusiness fragment = new FragmentSearchBusiness();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(getActivity());
        CustomActivityOnCrash.setShowErrorDetails(false);
        //page = getArguments().getInt("someInt", 0);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CustomActivityOnCrash.install(getActivity());

        View view = inflater.inflate(R.layout.fragment_search_business_specials,
                container, false);
        contextActivity=getActivity();
        businessAds=new ArrayList<>();
        init(view);
        getBurstAds();
        searchResults = new ArrayList<>();
        adaterResult = new AdapterBusinessSearchResult(getActivity(), searchResults);

        return view;
    }

    private void init(View view) {
        listView = (ListView) view.findViewById(R.id.listView);
        adapterSpecialBusinesses = new AdapterSpecialBusinesses(getActivity(), businessAds, FragmentSearchBusiness.this, FragmentSearchBusiness.this);
        listView.setAdapter(adapterSpecialBusinesses);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                ((ActivityMain) getActivity()).getFragmentSearch().onScroll(firstVisibleItem);
                if ((firstVisibleItem+visibleItemCount)==totalItemCount && businessAds.size()>0) {
                    loadMoreData();
                }
            }
        });
    }

    // LOAD MORE DATA
    public void loadMoreData() {
        // LOAD MORE DATA HERE...
        if (currentResultIsFor!=null && !currentResultIsFor.equals("") && searchResults.size()>0 && !searchCatchedEnd) {
          //  ((ActivityMain) getActivity()).getFragmentSearch().searchStarted();
            // TODO: ATTENTION NEEDED
            new SearchBusinessesLocation(
                    getActivity(), currentResultIsFor, "0",
                    "35.463900", "48.873900", adapterSpecialBusinesses.getCount()
                   ,
                    getResources().getInteger(R.integer.lazy_load_limitation),
                    FragmentSearchBusiness.this, SEARCH_BUSINESS_LOCATION_REQUEST, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
        }
        else if (latLng != null && !adsCatchedEnd && !isLoadingMoreAds){
            isLoadingMoreAds=true;
            if (businessAds.size()>0){
                new GetSuggestedBusiness(contextActivity,LoginInfo.getInstance().getUserUniqueId(),
                        String.valueOf(latLng.latitude),String.valueOf(latLng.longitude),
                        adapterSpecialBusinesses.getCount(),
                        getResources().getInteger(R.integer.lazy_load_limitation),
                        FragmentSearchBusiness.this,GET_BURST_ADS_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
            }
            else{
                new GetSuggestedBusiness(contextActivity,LoginInfo.getInstance().getUserUniqueId(),
                        String.valueOf(latLng.latitude),String.valueOf(latLng.longitude),
                        0,getResources().getInteger(R.integer.lazy_load_limitation),
                        FragmentSearchBusiness.this,GET_BURST_ADS_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

            }
        }
    }


    public void reset() {
        searchResults.clear();
    }

    public void setCurrentResultIsFor(String currentResultIsFor) {
        this.currentResultIsFor=currentResultIsFor;
    }
    public String getCurrentResultIsFor() {
        return currentResultIsFor;
    }

    public boolean isSearching() {
        return ((ActivityMain) getActivity()).getFragmentSearch().isSearching();
    }

    public View getListView() {
        return listView;
    }

    @Override
    public void notifyUnfollowBusiness(String businessId) {
//        for(int i=0; i<businesses.size(); i++) {
//            if(businessUniqueId==businesses.get(i).uniqueId) {
//                businesses.get(i).isFollowing = false;
//                adapterSpecialBusinesses.notifyDataSetChanged();
//                break;
//            }
//        }
//        for (int i = 0; i < businessAds.size(); i++) {
//            if(businessUniqueId==businessAds.get(i).businessUniqueId) {
//                businessAds.get(i).isFollowing = false;
//                adapterSpecialBusinesses.notifyDataSetChanged();
//                break;
//            }
//        }
    }

    @Override
    public void notifyUnfollowBusinessFailed(Integer errorCode, String callerStringID) {
        new DialogMessage(getActivity(), ServerAnswer.getError(getActivity(), errorCode, callerStringID + ">" + TAG)).show();
    }

    @Override
    public void getResult(int request, Object result) {
        ((ActivityMain) getActivity()).getFragmentSearch().searchFinished();
        if(request == SEARCH_BUSINESS_LOCATION_REQUEST){
            if(!showingResults) {
                listView.setAdapter(adaterResult);
                showingResults=true;
            }

            ArrayList<BaseAdapterItem> temp = (ArrayList<BaseAdapterItem>) result;
            if (temp.size()==0)
                searchCatchedEnd=true;
            else
                searchCatchedEnd=false;
            searchResults.addAll(temp);
            adaterResult.notifyDataSetChanged();
        }
        else if (request==GET_BURST_ADS_INT_CODE){
            // todo : add load more mechanism
            ArrayList<SuggestedBusiness> temp =(ArrayList<SuggestedBusiness>)result;
            isLoadingMoreAds=false;
            if (temp.size()==0)
                adsCatchedEnd=true;
            else {
                adsCatchedEnd=false;
                businessAds.addAll(temp);
                adapterSpecialBusinesses.resetItems(businessAds);
           }
        }
        // NOTICE :: WE HAVE USED ID+1000 AS REQUEST CODE HERE
        else if(request>1000) {
//            int businessUniqueId = request-1000;
//            for (int i = 0; i < businesses.size(); i++) {
//                if (businessUniqueId == businesses.get(i).uniqueId) {
//                    businesses.get(i).isFollowing = true;
//                    adapterSpecialBusinesses.notifyDataSetChanged();
//                    return;
//                }
//            }
//            for (int i = 0; i < businessAds.size(); i++) {
//
//            }
        }
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
        isLoadingMoreAds=false;
    }

    private void getBurstAds() {
        // Get location from GPS if it's available

        Location myLocation=null;
//        checkIfLocationEnabled();
        LocationManager locationManager=(LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled=false,network_enabled=false;
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
        if (network_enabled){
            myLocation=locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);
        }
        if (gps_enabled){
            myLocation=locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
        }

        if (myLocation==null){
            latLng=new LatLng(0,0);
        }
        else{
            latLng=new LatLng(myLocation.getLatitude(),myLocation.getLongitude());
        }
        isLoadingMoreAds=false;
        new GetSuggestedBusiness(contextActivity,LoginInfo.getInstance().getUserUniqueId(),
                String.valueOf(latLng.latitude),String.valueOf(latLng.longitude),
                 0,getResources().getInteger(R.integer.lazy_load_limitation),FragmentSearchBusiness.this,GET_BURST_ADS_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

    }
}
