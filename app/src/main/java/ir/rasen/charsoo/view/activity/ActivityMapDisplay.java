package ir.rasen.charsoo.view.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Timer;
import java.util.TimerTask;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;

public class ActivityMapDisplay extends CharsooActivity {

    // Google Map
    private GoogleMap googleMap;
    MapView mapView;
    Marker marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setTitle(getString(R.string.display_location));
        setContentView(R.layout.activity_map);

        try {
            MapsInitializer.initialize(ActivityMapDisplay.this);
            mapView = (MapView) findViewById(R.id.map);
            mapView.onCreate(savedInstanceState);

            if (mapView != null) {
                googleMap = mapView.getMap();
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                googleMap.setMyLocationEnabled(true);
                googleMap.getUiSettings().setZoomControlsEnabled(true);
            }
            try {
                double lat = getIntent().getExtras().getDouble(Params.LATITUDE);
                double lng = getIntent().getExtras().getDouble(Params.LONGITUDE);
                LatLng loc = new LatLng(lat, lng);
                doOnLocationChanged(loc);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onBackPressed() {
        mapView.onPause();
        finish();
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        return true;
//    }


    private void doOnLocationChanged(LatLng loc){
        if (googleMap != null) {
            navigateToLocation(loc);
            replaceMarker(loc,getString(R.string.business_location));
        }
    }

    private void replaceMarker(LatLng loc,String title){
        if (marker != null)
            marker.remove();
        marker = googleMap.addMarker(new MarkerOptions().position(loc)
                .title(title));
    }

    private boolean navigateToLocation(LatLng loc){
        if (loc!=null)
        {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(loc)      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .bearing(0)                // Sets the orientation of the camera to east
                    .tilt(10)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            return true;
        }
        else{
//            timer.schedule(new GetLastLocation(), locationCeckIntervalTime);
            return false;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this,"2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}

