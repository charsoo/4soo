package ir.rasen.charsoo.view.activity;

import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import eu.janmuller.android.simplecropimage.CropImage;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.view.photocrop.Constants;
import ir.rasen.charsoo.view.photocrop.ImageCropActivity;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;

public class ActivityGallery extends CharsooActivity {

    private static String TAG = "GalleryActivity";
    final int GALLERY_CAPTURE = 3;


    private int size;
    private int quality;

    public static String FILE_PATH = "file_path";
    public static Integer CAPTURE_GALLERY =2;
    public static String SIZE ="size";
    public static String QUALITY ="quality";

    int REQUEST_CODE_UPDATE_PIC = 0x1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        size = getResources().getInteger(R.integer.image_size);
        quality = getResources().getInteger(R.integer.image_quality);
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        try {
            intent.putExtra("return-data", true);
            startActivityForResult(intent, GALLERY_CAPTURE);
        } catch (ActivityNotFoundException e) {
            Log.d(TAG, e.getMessage());

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_CAPTURE) {
                performCrop(data.getData());
            } else if (requestCode == REQUEST_CODE_UPDATE_PIC) {
                if (resultCode == RESULT_OK) {
                    String imagePath = data.getStringExtra(Constants.IntentExtras.IMAGE_PATH);

                    String resizedImagePath= Image_M.resizeImageThenGetPath(imagePath, Params.IMAGE_MAX_WIDTH_HEIGHT_PIX,
                            Params.IMAGE_MAX_WIDTH_HEIGHT_PIX);
                    Intent i = getIntent();
                    i.putExtra(ActivityGallery.FILE_PATH, resizedImagePath);
                    setResult(RESULT_OK, i);
                    finish();
                }
            }
        }else if(resultCode == RESULT_CANCELED) {
            finish();
        }
    }

    private void performCrop(Uri uri) {
        try {
            try {
                String filePath = getPath(this, uri);
                if (filePath==null)
                    filePath=getRealPathFromURI(uri);
                if (filePath!=null) {
                    Intent intent = new Intent(this, ImageCropActivity.class);
//                    intent.putExtra("ACTION", Constants.IntentExtras.ACTION_GALLERY);
                    intent.putExtra(Constants.IntentExtras.IMAGE_PATH, filePath);
                    startActivityForResult(intent, REQUEST_CODE_UPDATE_PIC);
                }
            }
            catch(Exception e1){
                e1.printStackTrace();
            }

        } catch (ActivityNotFoundException anfe) {
            //display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }


    }

    private String getRealPathFromURI(Uri contentURI) {
        String result = "";
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            if (idx >= 0)
                result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static String getPath(final Context context, final Uri uri) {

        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            if (DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if ("com.android.externalstorage.documents".equals(uri.getAuthority())) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }

                    // TODO handle non-primary volumes
                }
                // DownloadsProvider
                else if ("com.android.providers.downloads.documents".equals(uri.getAuthority())) {

                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                    return getDataColumn(context, contentUri, null, null);
                }
                // MediaProvider
                else if ("com.android.providers.media.documents".equals(uri.getAuthority())) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    }/* else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }*/

                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{
                            split[1]
                    };

                    return getDataColumn(context, contentUri, selection, selectionArgs);
                }
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }


    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this,"2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}