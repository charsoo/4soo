package ir.rasen.charsoo.view.fragment.business_register;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.LocationManagerTracker;
import ir.rasen.charsoo.controller.helper.Location_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.City;
import ir.rasen.charsoo.controller.object.State;
import ir.rasen.charsoo.model.GetCountryStates;
import ir.rasen.charsoo.model.GetProvinceCities;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.view.activity.ActivityMapChoose;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.widgets.buttons.ButtonFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.edittexts.EditTextFont;

/**
 * Created by hossein-pc on 6/14/2015.
 */
public class FragmentBusinessRegisterPageThree
        extends Fragment implements
        IWebservice,NetworkStateChangeListener {

    public static final String TAG="FragmentBusinessRegisterPageThree";

    private static final int GET_STATES_LIST_REQUEST =12, GET_CITIES_LIST_REQUEST =14;

    private Spinner spinnerStates,spinnerCities;

    EditTextFont editTextStreet;
    String latitude, longitude;
    ButtonFont buttonMap;
    State selectedState;
    City selectedCity;
    private int autoLoadRequestCode;

    CharsooActivity mCharsooActivity;

    String defaultStateName;
    String defaultCityName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CustomActivityOnCrash.install(getActivity());

        View view = inflater.inflate(R.layout.fragment_register_business_page_three,
                container, false);

        mCharsooActivity = (CharsooActivity) getActivity();


        defaultStateName = getResources().getString(R.string.state);
        defaultCityName = getResources().getString(R.string.city);

        spinnerStates = (Spinner) view.findViewById(R.id.spinner_States);
        spinnerCities = (Spinner) view.findViewById(R.id.spinner_Cities);

        if(selectedCity==null || selectedState==null) {
            selectedState = new State("-1",defaultStateName);
            selectedCity = new City("-1",defaultCityName);
        }


        if (Globals.getInstance().getValue().loadedStates == null ||
                Globals.getInstance().getValue().loadedStates.size()==0
                ){
            Globals.getInstance().getValue().loadedStates = new ArrayList<>();
            setSpinnerStatesAdapter(new ArrayList<String>(),getActivity());

            new GetCountryStates(getActivity(),
                    FragmentBusinessRegisterPageThree.this,
                    GET_STATES_LIST_REQUEST, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;

            mCharsooActivity.showWaitDialog();
        }else{
            mCharsooActivity.hideWaitDialog();
            ArrayList<String> statesName =
                    State.getStatesName(Globals.getInstance().getValue().loadedStates);

            setSpinnerStatesAdapter(
                    statesName,
                    getActivity());

            if(selectedState!=null &&
                    selectedState.stateName!=null &&
                    !selectedState.stateName.equals("") &&
                    !selectedState.stateName.equals("null"))
                spinnerStates.setSelection(statesName.indexOf(selectedState.stateName));
        }

        setSpinnerCitiesAdapter(new ArrayList<String>(), getActivity());
        spinnerCities.setEnabled(false);


        editTextStreet = (EditTextFont) view.findViewById(R.id.edt_street);


        buttonMap = (ButtonFont) view.findViewById(R.id.btn_map);

        if (!LocationManagerTracker.isGooglePlayServicesAvailable(getActivity())) {
            buttonMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getActivity(),"Please Install LastVersion GooglePlayServices...",Toast.LENGTH_SHORT).show();
                }
            });
            view.findViewById(R.id.textView_play_service).setVisibility(View.VISIBLE);

            LocationManagerTracker lt = new LocationManagerTracker(getActivity());
            // check if GPS enabled
            if(lt.canGetLocation()){

                latitude = String.valueOf(lt.getLatitude());
                longitude = String.valueOf(lt.getLongitude());
                Globals.getInstance().getValue().businessRegistering.location_m = new Location_M(latitude, longitude);
            }else{
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                lt.showSettingsAlert();
            }
        }else{
            buttonMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), ActivityMapChoose.class);
                    intent.putExtra(Params.IS_EDITTING, false);
                    startActivityForResult(intent, Params.ACTION_CHOOSE_LOCATION);
                }
            });
        }



        spinnerStates.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==0){
                    spinnerCities.setVisibility(View.INVISIBLE);
                }
                else if(i>0){
                    spinnerCities.setVisibility(View.VISIBLE);
                    mCharsooActivity.showWaitDialog();

                    String selectedStateId = Globals.getInstance().getValue().loadedStates.get(i).stateUniqueId;

                    new GetProvinceCities(getActivity(),selectedStateId,
                            FragmentBusinessRegisterPageThree.this,GET_CITIES_LIST_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;

                    selectedState.stateName = Globals.getInstance().getValue().loadedStates.get(i).stateName;
                    selectedState.stateUniqueId = selectedStateId;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerCities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(Globals.getInstance().getValue().loadedCities==null)
                    return;

                selectedCity.cityName = Globals.getInstance().getValue().loadedCities.get(i).cityName;
                selectedCity.cityUniqueId = Globals.getInstance().getValue().loadedCities.get(i).cityUniqueId;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return view;
    }


    @Override
    public void onResume() {
        //mapView.onResume();
        super.onResume();
    }

    public boolean isVerified() {
        if (spinnerStates.getSelectedItemPosition() == 0) {
            new DialogMessage(getActivity(), getString(R.string.err_choose_one_state)).show();
            return false;
        }
        if (spinnerCities.getSelectedItemPosition()==0){
            new DialogMessage(getActivity(), getString(R.string.err_choose_city)).show();
            return false;
        }
//        if (!Validation.validateAddress(getActivity(), editTextStreet.getText().toString()).isValid()) {
//            editTextStreet.setError(Validation.getErrorMessage());
//            return false;
//        }
//        else
//            streetAddress=editTextStreet.getText().toString();

        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Params.ACTION_CHOOSE_LOCATION && resultCode == Activity.RESULT_OK) {
            latitude = data.getStringExtra(Params.LATITUDE);
            longitude = data.getStringExtra(Params.LONGITUDE);
            buttonMap.setBackgroundResource(R.drawable.selector_button_register);
            buttonMap.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_check_white_24dp), null);
        }
    }


    private void setSpinnerStatesAdapter(ArrayList<String> states,Context c){
        ArrayAdapter<String> statesAdapter = new ArrayAdapter<>(c,
                android.R.layout.simple_spinner_dropdown_item, states);
        spinnerStates.setAdapter(statesAdapter);
    }

    private void setSpinnerCitiesAdapter(ArrayList<String> cities,Context c){
        ArrayAdapter<String> citiesAdapter = new ArrayAdapter<>(c,
                android.R.layout.simple_spinner_dropdown_item, cities);
        spinnerCities.setAdapter(citiesAdapter);
        if (cities.size()>0)
            spinnerCities.setEnabled(true);
        else
            spinnerCities.setEnabled(false);
    }

    public Business getInputData(){
        if (isVerified()){
            Business result=new Business();
            result.stateName = selectedState.stateName;
            result.cityName = selectedCity.cityName;
            result.stateUniqueId = selectedState.stateUniqueId;
            result.cityUniqueId = selectedCity.cityUniqueId;
            result.address = editTextStreet.getText().toString();
            if ((latitude != null)&&(longitude != null)) {
                result.location_m=new Location_M(latitude,longitude);
            }
            return result;
        }
        else
            return null;
    }

    @Override
    public void getResult(int request, Object result) {

        mCharsooActivity.hideWaitDialog();

        if (request == GET_STATES_LIST_REQUEST) {
            ArrayList<State> tempStates = (ArrayList<State>) result;

            Globals.getInstance().getValue().loadedStates =
                    addDefaultStateToStartOfList(tempStates);


            ArrayList<String> statesName = State.getStatesName(Globals.getInstance().getValue().loadedStates);

            setSpinnerStatesAdapter(statesName,
                    getActivity());

            if (selectedState != null &&
                    selectedState.stateName != null &&
                    !selectedState.stateName.equals("") &&
                    !selectedState.stateName.equals("null"))
                spinnerStates.setSelection(statesName.indexOf(selectedState.stateName));

        } else if (request == GET_CITIES_LIST_REQUEST) {
            Globals.getInstance().getValue().loadedCities =
                    addDefaultCityToStartOfList((ArrayList<City>) result);

            ArrayList<String> citiesName =
                    City.getCitiesName(Globals.getInstance().getValue().loadedCities);

            setSpinnerCitiesAdapter(citiesName, getActivity());


            int selectedPosition = citiesName.indexOf(selectedCity.cityName);

            if (selectedCity != null &&
                    selectedCity.cityName != null &&
                    !selectedCity.cityName.equals("") &&
                    !selectedCity.cityName.equals("null") &&
                    selectedPosition != -1) {
                spinnerCities.setSelection(selectedPosition);
            }
        }
        NetworkConnectivityReciever.removeIfHaveListener(TAG);
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
        mCharsooActivity.hideWaitDialog();
        new DialogMessage(getActivity(), ServerAnswer.getError(getActivity(), errorCode, callerStringID + ">" + TAG)).show();
        if (errorCode==ServerAnswer.NETWORK_CONNECTION_ERROR){
            autoLoadRequestCode=request;
            NetworkConnectivityReciever.setNetworkStateListener(TAG,FragmentBusinessRegisterPageThree.this);
        }
    }

    @Override
    public void doOnNetworkConnected() {
//        if (autoLoadRequestCode== GET_CITIES_LIST_REQUEST){
//            new GetProvinceCities(getActivity(), statesHashtable.get(selectedStateName),FragmentBusinessRegisterPageThree.this, GET_CITIES_LIST_REQUEST).executeWithNewSolution();;
//        }
//        else if (autoLoadRequestCode== GET_STATES_LIST_REQUEST){
//            new GetCountryStates(getActivity(),FragmentBusinessRegisterPageThree.this, GET_STATES_LIST_REQUEST).executeWithNewSolution();;
//        }
    }


    ArrayList<State> addDefaultStateToStartOfList(ArrayList<State> states){
        ArrayList<State> tempStates = new ArrayList<>();
        tempStates.add(new State("-1", defaultStateName));
        tempStates.addAll(states);

        return tempStates;
    }

    ArrayList<City> addDefaultCityToStartOfList(ArrayList<City> cities){
        ArrayList<City> tempCities = new ArrayList<>();
        tempCities.add(new City("-1", defaultCityName));
        tempCities.addAll(cities);

        return tempCities;
    }


}
