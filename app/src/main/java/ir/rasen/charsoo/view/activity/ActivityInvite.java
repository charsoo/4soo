package ir.rasen.charsoo.view.activity;


import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.flurry.android.FlurryAgent;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.object.GlobalContactPassingClass;
import ir.rasen.charsoo.controller.object.PackageInfoCustom;
import ir.rasen.charsoo.view.fragment.invite.FragmentInvite;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;

public class ActivityInvite extends CharsooActivity {



    public ArrayList<PackageInfoCustom> appList;
    String firstTabTitle,secondTabTitle,thirdTabTitle;
    String smsMessage,emailMessage;
    String userUniqueId;
    FragmentInvite fragment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.activity_invite);


        setTitle(getString(R.string.txt_InviteFriends));

        Intent intent = getIntent();
        firstTabTitle = intent.getStringExtra(Params.FIRST_TAB_TITLE);
        secondTabTitle = intent.getStringExtra(Params.SECOND_TAB_TITLE);
        thirdTabTitle=intent.getStringExtra(Params.THIRD_TAB_TITLE);
        smsMessage=intent.getStringExtra(Params.SMS_MESSAGE);
        emailMessage=intent.getStringExtra(Params.EMAIL_MESSAGE);
        userUniqueId=intent.getExtras().getString(Params.USER_UNIQUE_ID, "0");
        appList=GlobalContactPassingClass.getInstance().getAppList();
        fragment= FragmentInvite.newInstance(this, userUniqueId,
                intent.getBooleanExtra(Params.IS_BUSINESS_PROMOTION,false),
                firstTabTitle,
                secondTabTitle,
                thirdTabTitle,
                smsMessage,
                emailMessage,
                GlobalContactPassingClass.getInstance().getEmailContacts(),
                GlobalContactPassingClass.getInstance().getNumberContacts(),
                appList);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, fragment).commit();
    }



    @Override
    public void onBackPressed() {
        if(!fragment.ifHideEmailList())
            finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        /*MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_next_button, menu);*/
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this,"2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

}
