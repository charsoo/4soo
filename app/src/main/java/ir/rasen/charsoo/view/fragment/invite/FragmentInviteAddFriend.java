package ir.rasen.charsoo.view.fragment.invite;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.ContactEntry;
import ir.rasen.charsoo.view.adapter.AdapterOfferFriendsToAdd;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;

/**
 * Created by hossein-pc on 6/9/2015.
 */
public class FragmentInviteAddFriend extends Fragment implements IWebservice {

    public static final int REQUEST_FRIENDSHIP_INT_CODE=11;
    public  CharsooActivity charsooActivityContext;
    public static final String TAG="FragmentInviteAddFriend";
    ListView listView;
    AdapterOfferFriendsToAdd listViewAdapter;


    LinearLayout hasNoFriend;
    private ArrayList<ContactEntry> charsooContactList;
    DialogMessage dialogMessage;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CustomActivityOnCrash.install(getActivity());

        View view = inflater.inflate(R.layout.fragment_register_user_offer_friends_add,
                container, false);
        charsooActivityContext=(CharsooActivity)getActivity();
      //  dialogMessage=new DialogMessage(getActivity(),"");
        charsooActivityContext.showWaitDialog();
        hasNoFriend=(LinearLayout) view.findViewById(R.id.view_nothing);
        if (charsooContactList==null){
            charsooContactList=new ArrayList<>();
        }
        else
        {
            if (charsooContactList.isEmpty())
                hasNoFriend.setVisibility(View.VISIBLE);
        }
//        hasApplicationX=new Hashtable<>();
//        new GetInstalledApps(getActivity()).executeWithNewSolution();;
        listView=(ListView) view.findViewById(R.id.listViewCharsooContacts);
        if (listViewAdapter==null)
            listViewAdapter=new AdapterOfferFriendsToAdd(getActivity(),LoginInfo.getInstance().getUserUniqueId(),charsooContactList,
                    FragmentInviteAddFriend.this,REQUEST_FRIENDSHIP_INT_CODE);
//        sc.setAdapter(af);
        listView.setAdapter(listViewAdapter);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
//        if (charsooContactList==null);
////            af=new AdapterInviteNoneCharsooContact(getActivity(),new ArrayList<ContactEntry>());
//        else{
////            af=new AdapterInviteNoneCharsooContact(getActivity(),charsooContactList);
//            if (charsooContactList.isEmpty())
//                ((LinearLayout) view.findViewById(R.uniqueId.ll_hasNoFriend)).setVisibility(View.VISIBLE);
//            else
//                af.resetItems(charsooContactList);
//        }
//        sc=(ListView) view.findViewById(R.uniqueId.listView2);
    }


//    @Override
//    public void getResult(Object result) {
//
//    }
//
//    @Override
//    public void getError(Integer errorCode, String callerStringID) {
//
//    }


    public void setCharsooContacts(ArrayList<ContactEntry> charsooContacts){
        charsooContactList=new ArrayList<>(charsooContacts);
        if (hasNoFriend!=null) {
            if (charsooContactList.isEmpty())
                hasNoFriend.setVisibility(View.VISIBLE);
            else
                hasNoFriend.setVisibility(View.GONE);
        }
        if (listViewAdapter != null){
//            if (af.getCount()<=0)
            listViewAdapter.resetItems(charsooContactList);
        }

    }


    @Override
    public void getResult(int request, Object result) {
        charsooActivityContext.hideWaitDialog();
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
//        new DialogMessage(getActivity())
        charsooActivityContext.hideWaitDialog();
        if (request==REQUEST_FRIENDSHIP_INT_CODE){
            try{
                    if (!dialogMessage.isShowing()) {
                        dialogMessage.show();
                        dialogMessage.setMessage(ServerAnswer.getError(getActivity(), errorCode, callerStringID + ">" + TAG));
                    }
            }catch (Exception e){}
        }
    }
}
