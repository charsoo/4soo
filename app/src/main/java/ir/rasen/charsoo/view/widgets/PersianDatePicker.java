package ir.rasen.charsoo.view.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.PersianDate;

/**
 * CREATED BY 'Sina Khalili' on JULY 8, 2015
 */
public class PersianDatePicker extends RelativeLayout {

    private static int HUNDRED = 100;

    public static final int DEFAULT_YEAR_INT=1294;
    public static final int DEFAULT_MONTH_INT=1;
    public static final int DEFAULT_DAY_INT=1;

    private NumberPicker numberPickerDay, numberPickerMonth, numberPickerYear;

    public PersianDatePicker(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public PersianDatePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public PersianDatePicker(Context context) {
        super(context);
        init(null);
    }

    private void init(AttributeSet attrs) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_persian_date_picker, this, false);

        numberPickerYear = (NumberPicker) view.findViewById(R.id.edt_year);
        numberPickerMonth = (NumberPicker) view.findViewById(R.id.edt_month);
        numberPickerDay = (NumberPicker) view.findViewById(R.id.edt_day);

        numberPickerMonth.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                numberPickerDay.setMaxValue(PersianDate.getDayCount(numberPickerYear.getValue(), newVal));
            }
        });
        numberPickerYear.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                numberPickerDay.setMaxValue(PersianDate.getDayCount(newVal, numberPickerMonth.getValue()));
            }
        });
        PersianDate persianDate = new PersianDate(getContext());
        numberPickerYear.setMinValue(persianDate.getYear() - HUNDRED);
        numberPickerYear.setMaxValue(persianDate.getYear());
        numberPickerMonth.setMinValue(1);
        numberPickerMonth.setMaxValue(12);
        numberPickerMonth.setDisplayedValues(persianDate.getMonthStringArray());
        numberPickerYear.setMinValue(persianDate.getYear() - HUNDRED);
        numberPickerYear.setMaxValue(persianDate.getYear());
        numberPickerDay.setMinValue(1);
        numberPickerDay.setMaxValue(31);

        addView(view);

    }

    public void setYear(int year) {
        numberPickerYear.setValue(year);
    }
    public int getYear() {
        return numberPickerYear.getValue();
    }

    public void setMonth(int month) {
        numberPickerMonth.setValue(month);
    }
    public int getMonth() {
        return numberPickerMonth.getValue();
    }

    public void setDay(int day) {
        numberPickerDay.setValue(day);
    }
    public int getDay() {
        return numberPickerDay.getValue();
    }
}