package ir.rasen.charsoo.view.interface_m;

import java.util.ArrayList;
import java.util.Hashtable;

import ir.rasen.charsoo.controller.object.CommentNotification;
import ir.rasen.charsoo.controller.object.ContactEntry;

/**
 * Created by hossein-pc on 6/23/2015.
 */
public interface IGetContactListener {
//    void getPhoneContacts(Hashtable<String,ArrayList<ContactEntry>> emailContacts,Hashtable<String,ArrayList<ContactEntry>> numberContacts);
    void getPhoneContacts();
}
