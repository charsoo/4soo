package ir.rasen.charsoo.view.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.WorkDays;
import ir.rasen.charsoo.view.widgets.TextViewFont;

public class AdapterWorkDays extends BaseAdapter{
    ArrayList <WorkDays.WorkDaysGroup> mWorkDaysGroup;
    LayoutInflater mInflater;
    Context mContext;

    public AdapterWorkDays(Context ctx,ArrayList<WorkDays.WorkDaysGroup> workDaysGroups){
        mContext = ctx;
        mWorkDaysGroup = workDaysGroups;
        mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mWorkDaysGroup.size();
    }

    @Override
    public Object getItem(int position) {
        return mWorkDaysGroup.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = mInflater.inflate(R.layout.item_workday_layout,null,false);

        TextViewFont tvDays = (TextViewFont) v.findViewById(R.id.tv_days);
        TextViewFont tvOpenCloseTime =
                (TextViewFont) v.findViewById(R.id.tv_open_and_close_time);

        tvDays.setText(mWorkDaysGroup.get(position).getDaysStr(mContext));
        tvOpenCloseTime.setText(mWorkDaysGroup.get(position).getOpenCloseTimeStr(mContext));

        return v;
    }

    public ArrayList<WorkDays.WorkDaysGroup> getAllItems(){
        return mWorkDaysGroup;
    }

    public void removeItem(int pos){
        mWorkDaysGroup.remove(pos);
        notifyDataSetChanged();
    }
}
