package ir.rasen.charsoo.view.interface_m;

public interface IWebservice {
  /*  void getResult(User result);

    void getResult(Business result);

    void getResult(ResultStatus result);

    void getResult(CommentNotification result);

    <T> void getResult(Collection<T> result);*/

    void getResult(int request, Object result) throws Exception;

    void getError(int request, Integer errorCode, String callerStringID) throws Exception;

}
