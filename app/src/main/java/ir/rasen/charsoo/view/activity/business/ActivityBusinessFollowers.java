package ir.rasen.charsoo.view.activity.business;


import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.BaseAdapterItem;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.PullToRefreshList;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.business.GetBlockedUsers;
import ir.rasen.charsoo.model.business.GetBusinessFollowers;
import ir.rasen.charsoo.view.adapter.AdapterUsersFromBAItems;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IPullToRefresh;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.buttons.ButtonFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.PullToRefreshListView;


public class ActivityBusinessFollowers
        extends CharsooActivity
        implements IWebservice, IPullToRefresh,NetworkStateChangeListener,TryAgainListener {

    public static final String TAG="ActivityBusinessFollowers";
    public static final int GET_BUSINESS_FOLLOWERS_REQUEST = 100,GET_BUSINESS_BLOCK_USERS_REQUEST = 200;

    //    WaitDialog progressDialog;
    String businessId;
    AdapterUsersFromBAItems adapterFollowers;
    ListView listView;
    ArrayList<BaseAdapterItem> followers;
    private String ownerId;
    ButtonFont buttonFontBlockUsers;


    RelativeLayout networkFailLayout;

    TextViewFont tvNoFollowerExists;

    boolean isOwnerBusiness;

    boolean isHaveBusinessBlockUser;

    @Override
    public void notifyRefresh() {
        status = Status.REFRESHING;
        new GetBusinessFollowers(ActivityBusinessFollowers.this,
                businessId, ActivityBusinessFollowers.this,GET_BUSINESS_FOLLOWERS_REQUEST,
                LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
    }

    @Override
    public void notifyLoadMore() {
        loadMoreData();
    }

    @Override
    public void doOnNetworkConnected() {
//        progressDialog.show();
//        showWaitDialog();
//        status = Status.REFRESHING;
//        new GetBusinessFollowers(ActivityBusinessFollowers.this, businessUniqueId, ActivityBusinessFollowers.this,GET_BUSINESS_FOLLOWERS_REQUEST).execute();
        doTryAgain();
    }

    private enum Status {
        FIRST_TIME, LOADING_MORE, REFRESHING, NONE
    }

    private Status status;
    PullToRefreshList pullToRefreshListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.activity_business_followers);
        setTitle(getResources().getString(R.string.followers));

        isHaveBusinessBlockUser = false;

        buttonFontBlockUsers = (ButtonFont) findViewById(R.id.btn_blocked_users);
        buttonFontBlockUsers.setVisibility(View.GONE);

        networkFailLayout =
                MyApplication.initNetworkErrorLayout(getWindow().getDecorView(),
                        ActivityBusinessFollowers.this, ActivityBusinessFollowers.this);
        networkFailLayout.setVisibility(View.GONE);

        businessId = getIntent().getExtras().getString(Params.BUSINESS_UNIQUE_ID,"");
        ownerId = getIntent().getExtras().getString(Params.POST_OWNER_BUSINESS_ID,"");

        followers = new ArrayList<>();
        status = Status.FIRST_TIME;

//        progressDialog = new WaitDialog(this);
//        progressDialog.setMessage(getResources().getString(R.string.please_wait));

        pullToRefreshListView =
                new PullToRefreshList(this, (PullToRefreshListView) findViewById(R.id.pull_refresh_list),
                        ActivityBusinessFollowers.this);
        listView = pullToRefreshListView.getListView();


        AdapterUsersFromBAItems.Mode mode =
                ownerId.equals(LoginInfo.getInstance().getUserUniqueId()) ?
                        AdapterUsersFromBAItems.Mode.OWN_FOLLOWERS :
                        AdapterUsersFromBAItems.Mode.USERS;
        adapterFollowers = new AdapterUsersFromBAItems(ActivityBusinessFollowers.this, "0", followers, mode);
        listView.setAdapter(adapterFollowers);

//        progressDialog.show();
        showWaitDialog();
        new GetBusinessFollowers(ActivityBusinessFollowers.this, businessId,
                ActivityBusinessFollowers.this,GET_BUSINESS_FOLLOWERS_REQUEST,
                LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

        (findViewById(R.id.btn_blocked_users)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openBusinessOtherActivity(businessId);
            }
        });

        tvNoFollowerExists = (TextViewFont) findViewById(R.id.tv_no_data_exist);

        isOwnerBusiness = getIntent().getExtras().getBoolean(Params.BUSINESS_OWNER);



    }


    // LOAD MORE DATA
    public void loadMoreData() {
        // LOAD MORE DATA HERE...

      /*  status = Status.LOADING_MORE;
        pullToRefreshListView.setFooterVisibility(View.VISIBLE);
        new GetBusinessFollowers(ActivityBusinessFollowers.this, businessUniqueId, ActivityBusinessFollowers.this).execute();*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        /*MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_next_button, menu);*/
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }


    @Override
    public void getResult(int reqCode,Object result) {
        hideWaitDialog();
        networkFailLayout.setVisibility(View.GONE);
        switch (reqCode){
            case GET_BUSINESS_FOLLOWERS_REQUEST :
                if (result instanceof ArrayList) {
                    ArrayList<BaseAdapterItem> temp = (ArrayList<BaseAdapterItem>) result;

                    if (status == Status.FIRST_TIME) {
//                AdapterUsersFromBAItems.Mode mode =
//                        ownerId==LoginInfo.getUserId(this) ? AdapterUsersFromBAItems.Mode.OWN_FOLLOWERS : AdapterUsersFromBAItems.Mode.USERS;
//                adapterFollowers = new AdapterUsersFromBAItems(ActivityBusinessFollowers.this, 0, followers, mode);
//                listView.setAdapter(adapterFollowers);
                        followers=new ArrayList<>(temp);
                        adapterFollowers.resetItems(followers);
                    } else if (status == Status.REFRESHING) {
                        followers=new ArrayList<>(temp);
                        adapterFollowers.resetItems(new ArrayList<BaseAdapterItem>());
                        adapterFollowers.resetItems(followers);
                        pullToRefreshListView.onRefreshComplete();
                    } else {
                        //it is loading more
                        pullToRefreshListView.setFooterVisibility(View.GONE);
                        adapterFollowers.loadMore(temp);
                        pullToRefreshListView.onLoadmoreComplete();
                    }
                    status = Status.NONE;
                    pullToRefreshListView.setResultSize(followers.size());

                }
                NetworkConnectivityReciever.removeIfHaveListener(TAG);

                new GetBlockedUsers(
                        ActivityBusinessFollowers.this,
                        businessId,0,5,ActivityBusinessFollowers.this,
                        GET_BUSINESS_BLOCK_USERS_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;

                break;

            case GET_BUSINESS_BLOCK_USERS_REQUEST :
                ArrayList<BaseAdapterItem> firstFiveBlockUsers = (ArrayList<BaseAdapterItem>) result;
                isHaveBusinessBlockUser = (firstFiveBlockUsers.size() >0)?true:false;
                handleShowButtonBlockUsers();

                hideWaitDialog();
                pullToRefreshListView.onRefreshComplete();
                break;
        }

        handleTvNoReviewExist();
    }

    @Override
    public void getError(int reqCode,Integer errorCode,String callerStringID) {
        switch (reqCode){
            case GET_BUSINESS_FOLLOWERS_REQUEST :
//                progressDialog.dismiss();
                hideWaitDialog();
                pullToRefreshListView.onRefreshComplete();

                if (errorCode==ServerAnswer.NETWORK_CONNECTION_ERROR){
                    NetworkConnectivityReciever.setNetworkStateListener(TAG,ActivityBusinessFollowers.this);
                }
                if(ServerAnswer.canShowErrorLayout(errorCode) && status != Status.REFRESHING){
                    networkFailLayout.setVisibility(View.VISIBLE);
                }else if(networkFailLayout.getVisibility()!=View.VISIBLE){
                    new DialogMessage(
                            ActivityBusinessFollowers.this, ServerAnswer.getError(ActivityBusinessFollowers.this,
                            errorCode, callerStringID + ">" + this.getLocalClassName())).show();
                }
                break;
        }

        handleTvNoReviewExist();
    }


    @Override
    public void doTryAgain() {
        if(isShowingWaitDialog())
            return;

        showWaitDialog();
        status = Status.REFRESHING;
        new GetBusinessFollowers(ActivityBusinessFollowers.this, businessId,
                ActivityBusinessFollowers.this,GET_BUSINESS_FOLLOWERS_REQUEST,
                LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }

    void handleTvNoReviewExist(){
        if(followers.size() <= 0) {
            tvNoFollowerExists.setVisibility(View.VISIBLE);
            if (isOwnerBusiness)
                tvNoFollowerExists.setText(getString(R.string.txt_not_exists_follower_for_business));
            else
                tvNoFollowerExists.setText(getString(R.string.txt_not_exists_follower_for_business_other));
        }
        else
            tvNoFollowerExists.setVisibility(View.GONE);
    }

    void handleShowButtonBlockUsers(){
        if (isOwnerBusiness && isHaveBusinessBlockUser)
            buttonFontBlockUsers.setVisibility(View.VISIBLE);
        else
            buttonFontBlockUsers.setVisibility(View.GONE);

    }

    @Override
    protected void onDestroy() {
        NetworkConnectivityReciever.removeIfHaveListener(TAG);
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this,"2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
