package ir.rasen.charsoo.view.dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;

import java.io.File;

import eu.janmuller.android.simplecropimage.CropImage;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.view.activity.ActivityCamera;
import ir.rasen.charsoo.view.activity.ActivityGallery;


public class PopupSelectCameraGallery extends AlertDialog.Builder {

    public PopupSelectCameraGallery(final Activity activity) {
        super(activity);

        setTitle(getContext().getString(R.string.choose_photo));
        setItems(getContext().getResources().getStringArray(R.array.camera_or_gallery), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        File f=new File(CropImage.getCroppedImageDirectory(activity)+ActivityCamera.cameraOutputFileName);
                        if (f.exists())
                            f.delete();
                        f=new File(ActivityCamera.getCapturedImageDirectory()+ActivityCamera.cameraOutputFileName);
                        if (f.exists())
                            f.delete();
                        f=new File(ActivityCamera.getCapturedImageDirectory()+ActivityCamera.tempFolderToPreventRecreation);
                        if (f.exists())
                            f.delete();
                        Intent myIntent = new Intent(activity, ActivityCamera.class);
                        activity.startActivityForResult(myIntent, ActivityCamera.CAPTURE_PHOTO);
                        break;
                    case 1:
                        Intent galleryIntent = new Intent(activity, ActivityGallery.class);
                        activity.startActivityForResult(galleryIntent, ActivityGallery.CAPTURE_GALLERY);
                }
            }
        });

    }





}
