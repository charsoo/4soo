package ir.rasen.charsoo.view.interface_m;

/**
 * Created by android on 4/27/2015.
 */
public interface IUpdateTimeLine {
    public void notifyUpdateTimeLineShare(String postId);
    public void notifyUpdateTimeLineCancelShare(String postId);

}
