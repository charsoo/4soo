package ir.rasen.charsoo.view.activity;


import android.app.Service;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.PullToRefreshList;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.Validation;
import ir.rasen.charsoo.controller.object.Comment;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.comment.GetPostAllComments;
import ir.rasen.charsoo.model.comment.SendComment;
import ir.rasen.charsoo.view.adapter.AdapterComments;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IPullToRefresh;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.edittexts.EditTextFont;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.PullToRefreshListView;


public class ActivityComments extends CharsooActivity
        implements IWebservice, IPullToRefresh, TryAgainListener, NetworkStateChangeListener {
    public static final String TAG = "ActivityComments";

    private static final int GET_POST_COMMENTS_INT_CODE = 11, SEND_COMMENT_INT_CODE = 14;
    public static boolean comment_change = false;
    private String postUniqueId, postOwnerBusinessId;
    private AdapterComments adapterComments;
    private ListView listView;
    //    private WaitDialog progressDialog;
    private String commentText;

    RelativeLayout networkFailedLayout;


    TextViewFont tvNoCommentExists;

    DialogMessage dialogMessage;

    @Override
    public void notifyRefresh() {
        status = Status.REFRESHING;
        new GetPostAllComments(ActivityComments.this, postUniqueId, postOwnerBusinessId,
                0, getResources().getInteger(R.integer.lazy_load_limitation), ActivityComments.this, GET_POST_COMMENTS_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
    }

    @Override
    public void notifyLoadMore() {
        loadMoreData();
    }


    private enum Status {FIRST_TIME, LOADING_MORE, REFRESHING, NONE}

    private EditTextFont editTextComment;
    private View btnSend;
    private Status status;
    boolean isUserOwner;

    //pull_to_refresh_lib
    PullToRefreshList pullToRefreshListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.activity_comments);
        setTitle(getString(R.string.comments));

        showWaitDialog();

        postUniqueId = getIntent().getExtras().getString(Params.POST_ID_INT,"");
        postOwnerBusinessId = getIntent().getExtras().getString(Params.POST_OWNER_BUSINESS_ID,"");
        isUserOwner = getIntent().getExtras().getBoolean(Params.IS_OWNER);


        pullToRefreshListView = new PullToRefreshList(this, (PullToRefreshListView) findViewById(R.id.pull_refresh_list), ActivityComments.this);
        pullToRefreshListView.setFooterVisibility(View.GONE);
        listView = pullToRefreshListView.getListView();

        adapterComments = new AdapterComments(ActivityComments.this, isUserOwner, postUniqueId,
                postOwnerBusinessId,new ArrayList<Comment>());
        listView.setAdapter(adapterComments);

//        progressDialog.show();
        showWaitDialog();

        status = Status.FIRST_TIME;
        new GetPostAllComments(
                ActivityComments.this, postUniqueId, postOwnerBusinessId,
                0, getResources().getInteger(R.integer.lazy_load_limitation),
                ActivityComments.this, GET_POST_COMMENTS_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

        editTextComment = (EditTextFont) findViewById(R.id.cb_comment_text);
        editTextComment.setBackgroundColor(getResources().getColor(R.color.gray_light));
        editTextComment.setPrimaryColor(getResources().getColor(android.R.color.transparent));
        editTextComment.setUnderlineColor(getResources().getColor(android.R.color.transparent));

        btnSend = findViewById(R.id.cb_comment_send);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commentText = editTextComment.getText().toString().trim();
                if (!Validation.validateComment(ActivityComments.this, commentText).isValid()){
                    editTextComment.setError(Validation.getErrorMessage());
                    return;
                }
                new SendComment(ActivityComments.this, LoginInfo.getInstance().getUserUniqueId(),
                        postUniqueId, commentText, ActivityComments.this, SEND_COMMENT_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
            }
        });

        if (getIntent().getExtras().getBoolean(Params.NEW_COMMENT_DIRECTLY)) {
            editTextComment.requestFocus();
            InputMethodManager imm = (InputMethodManager) this.getSystemService(Service.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editTextComment, 0);
        }


        networkFailedLayout =
                MyApplication.initNetworkErrorLayout(
                        getWindow().getDecorView(), ActivityComments.this, ActivityComments.this);


        tvNoCommentExists = (TextViewFont) findViewById(R.id.tv_no_data_exist);
        handleTvNoCommentExist();

        dialogMessage = new DialogMessage(ActivityComments.this,"");
    }


    // LOAD MORE DATA
    public void loadMoreData() {
        // LOAD MORE DATA HERE...
        status = Status.LOADING_MORE;
        pullToRefreshListView.setFooterVisibility(View.VISIBLE);
        new GetPostAllComments(ActivityComments.this, postUniqueId, postOwnerBusinessId,adapterComments.getCount(),
                getResources().getInteger(R.integer.lazy_load_limitation), ActivityComments.this,
                GET_POST_COMMENTS_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        /*MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_next_button, menu);*/
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }


    @Override
    public void getResult(int request, Object result) {
//        progressDialog.dismiss();
        hideWaitDialog();
        switch (request) {
            case GET_POST_COMMENTS_INT_CODE:
                ArrayList<Comment> tempComments = (ArrayList<Comment>) result;

                if (status == Status.FIRST_TIME) {
                    adapterComments.resetItems(tempComments);
                } else if (status == Status.REFRESHING) {
                    adapterComments.resetItems(tempComments);
                    pullToRefreshListView.onRefreshComplete();
                } else if (status == Status.LOADING_MORE) {
                    pullToRefreshListView.onLoadmoreComplete();
                    adapterComments.loadMore(tempComments);
                }
                pullToRefreshListView.setResultSize(adapterComments.getCount());
                status = Status.NONE;
                break;

            case SEND_COMMENT_INT_CODE:
                Comment c = new Comment(LoginInfo.getInstance().getUserUniqueId(),
                        LoginInfo.getInstance().userIdentifier,LoginInfo.getInstance().getUserProfilePictureUniqueId(),
                        commentText);

                //TODO SERVER RETURN UNIQUEID 0;
//                c.uniqueId = (String) result;
//                adapterComments.addCommentToFirst(c);

                editTextComment.setText("");
                editTextComment.setEnabled(true);
                btnSend.setClickable(true);

                notifyRefresh();

                MyApplication.broadCastUpdateDirtyPost(TAG, postUniqueId);
                break;
        }
        networkFailedLayout.setVisibility(View.GONE);
        handleTvNoCommentExist();
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
//        progressDialog.dismiss();
        hideWaitDialog();
        switch (request){
            case GET_POST_COMMENTS_INT_CODE :
                if (ServerAnswer.NETWORK_CONNECTION_ERROR == errorCode){
                    NetworkConnectivityReciever.setNetworkStateListener(TAG, ActivityComments.this);
                }

                if(ServerAnswer.canShowErrorLayout(errorCode) && status != Status.REFRESHING){
                    networkFailedLayout.setVisibility(View.VISIBLE);
                } else {
                    new DialogMessage(ActivityComments.this,ServerAnswer.getError
                            (ActivityComments.this, errorCode,
                                    callerStringID + ">" + ActivityComments.this.getLocalClassName())).show();
                }

                if (pullToRefreshListView.isRefreshing())
                    pullToRefreshListView.onRefreshComplete();
                if (pullToRefreshListView.isLoadigMore)
                    pullToRefreshListView.onLoadmoreComplete();

                break;
        }

        handleTvNoCommentExist();
    }

    @Override
    public void onBackPressed() {
        if (!adapterComments.ifHidePopup()) {
            super.onBackPressed();
            finish();
        }

    }

    @Override
    public void doTryAgain() {
//        progressDialog.show();
        if(isShowingWaitDialog())
            return;

        showWaitDialog();
        callWebservice(GET_POST_COMMENTS_INT_CODE, true);
    }

    @Override
    public void doOnNetworkConnected() throws Exception {
//        progressDialog.show();
//        showWaitDialog();
//        callWebservice(GET_POST_COMMENTS_INT_CODE,false);
        doTryAgain();
    }

    private void callWebservice(int reqCode, boolean showProgressDialog){
        switch (reqCode){
            case GET_POST_COMMENTS_INT_CODE:
                if (status!=Status.LOADING_MORE)
                    new GetPostAllComments(
                            ActivityComments.this, postUniqueId, postOwnerBusinessId,
                            0, getResources().getInteger(R.integer.lazy_load_limitation),
                            ActivityComments.this, GET_POST_COMMENTS_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                else
                {
                    pullToRefreshListView.setFooterVisibility(View.VISIBLE);
                    new GetPostAllComments(ActivityComments.this, postUniqueId, postOwnerBusinessId,
                            adapterComments.getCount(), getResources().getInteger(R.integer.lazy_load_limitation),
                            ActivityComments.this, GET_POST_COMMENTS_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                }
                break;
            case SEND_COMMENT_INT_CODE:
                break;
        }

    }



    void handleTvNoCommentExist(){
        if(adapterComments.getCount() <= 0) {
            tvNoCommentExists.setVisibility(View.VISIBLE);
            tvNoCommentExists.setText(getString(R.string.txt_not_exists_comments));
        }
        else
            tvNoCommentExists.setVisibility(View.GONE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this,"2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

}

