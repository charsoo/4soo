package ir.rasen.charsoo.view.fragment;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.user.GetUserBusinesses;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessRegister;
import ir.rasen.charsoo.view.activity.ActivityMain;
import ir.rasen.charsoo.view.adapter.AdapterUserBusinesses;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IOnBusinessCreatedListener;
import ir.rasen.charsoo.view.interface_m.IChangeTabs;
import ir.rasen.charsoo.view.interface_m.IOnBusinessEditedListener;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.widgets.buttons.FloatButton;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;

public class FragmentUserBusinesses extends Fragment
        implements IWebservice,NetworkStateChangeListener,
        IOnBusinessCreatedListener ,IOnBusinessEditedListener, TryAgainListener {
    public static final String TAG = "FragmentUserBusinesses";

    public static final int GET_USER_BUSINESSES_INT_CODE = 11;
    ArrayList<Business> userBusinesses;
    AdapterUserBusinesses adapterUserBusinesses;
    ListView listView;
    View view;
    FloatButton addBtn;
    BroadcastReceiver deleteBusinessReciever;
    String userUniqueId;
    DialogMessage dialogMessage;

    Context context;
    CharsooActivity charsooActivityContext;

    RelativeLayout notExistsBusinessLayout,networkErrorLayout;

    public static IOnBusinessCreatedListener onBusinessCreatedListener;

    public static IOnBusinessEditedListener iOnBusinessEditedListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CustomActivityOnCrash.install(getActivity());

        view = inflater.inflate(R.layout.activity_user_businesses,
                container, false);

        charsooActivityContext = (CharsooActivity) getActivity();
        networkErrorLayout = MyApplication.initNetworkErrorLayout(view, getActivity(),FragmentUserBusinesses.this);
        dialogMessage = new DialogMessage(getActivity(), "");
        userUniqueId = LoginInfo.getInstance().getUserUniqueId();
        addBtn = (FloatButton) (view.findViewById(R.id.btn_creat_new_business));
        listView = (ListView) view.findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Business.gotoBusinessPage(getActivity(),userBusinesses.get(i).uniqueId);
            }
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (view.getId() == listView.getId()) {
                    addBtn.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
                }
            }
        });
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startRegisterActivity();
            }
        });

        userBusinesses = new ArrayList<>();
        adapterUserBusinesses = new AdapterUserBusinesses(getActivity(), userBusinesses);
        listView.setAdapter(adapterUserBusinesses);

        iOnBusinessEditedListener = this;

        new GetUserBusinesses(getActivity(), userUniqueId, FragmentUserBusinesses.this, GET_USER_BUSINESSES_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;

        deleteBusinessReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int deletedBusinessId = intent.getIntExtra(Params.BUSINESS_ID_STRING, 0);
                //TODO:: UPDATE THE BUSINESSES LIST
                ((ActivityMain) getActivity()).setFragment(ActivityMain.FragmentTag.BUSINESSES);
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(deleteBusinessReciever, new IntentFilter(Params.DELETE_BUSINESS));

        context = getActivity();
        onBusinessCreatedListener = this;


        notExistsBusinessLayout = initBusinessLayout(view);



        return view;
    }

    private void startRegisterActivity() {
        Intent intent = new Intent(getActivity(), ActivityBusinessRegister.class);
        startActivity(intent);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MyApplication myApplication = (MyApplication) getActivity().getApplication();
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Params.ACTION_REGISTER_BUSINESS) {
                LoginInfo.getInstance().userBusinesses.add(
                        0, new User.UserBusinesses(Globals.getInstance().getValue().businessVisiting.uniqueId,
                                Globals.getInstance().getValue().businessVisiting.businessIdentifier));

                //changed by Sina
                //if user just add a business
                if (LoginInfo.getInstance().userBusinesses.size() == 1) {
                    userBusinesses = new ArrayList<>();
                }

                User.UserBusinesses u = new User.UserBusinesses(Globals.getInstance().getValue().businessVisiting.uniqueId, Globals.getInstance().getValue().businessVisiting.businessIdentifier);
                Business business = new Business();
                business.uniqueId = u.businessUniqueId;
                business.name = u.businessIdentifier;
                // TODO:: should be added to class
//                business.description = u.description;
//                business.profilePictureUniqueId = u.businessPicId;
                userBusinesses.add(0, business);

                //if user just add a business
                if (LoginInfo.getInstance().userBusinesses.size() == 1) {
                    adapterUserBusinesses.notifyDataSetChanged();

                    if (getActivity() instanceof IChangeTabs)
                        ((IChangeTabs) getActivity()).notifyMakeFourTabsWithInitialize();
//                    LoginInfo.submitBusiness(getActivity());
                } else
                    adapterUserBusinesses.notifyDataSetChanged();

            } else if(requestCode == Params.ACTION_DELETE_BUSIENSS) {
            }

        }
    }

    @Override
    public void doOnNetworkConnected() {
        new GetUserBusinesses(getActivity(), userUniqueId, FragmentUserBusinesses.this, GET_USER_BUSINESSES_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
//        recursivelyCallHandler();
    }

    @Override
    public void getResult(int request, Object result) {
        networkErrorLayout.setVisibility(View.GONE);
        NetworkConnectivityReciever.removeIfHaveListener(TAG);
        charsooActivityContext.hideWaitDialog();
        if (request == GET_USER_BUSINESSES_INT_CODE) {

            userBusinesses = new ArrayList<>((ArrayList<Business>) result);
            adapterUserBusinesses.resetItems(userBusinesses);
            view.findViewById(R.id.progressBar).setVisibility(View.GONE);
        }

        charsooActivityContext.hideWaitDialog();

        handleShowNotExistsLayout();
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
        charsooActivityContext.hideWaitDialog();
        if (errorCode == ServerAnswer.BUSINESS_DOES_NOT_EXISTS) {
            view.findViewById(R.id.progressBar).setVisibility(View.GONE);
            setShowingNotExistsLayout(true);
        } else if (errorCode == ServerAnswer.NETWORK_CONNECTION_ERROR) {
            networkErrorLayout.setVisibility(View.VISIBLE);
            NetworkConnectivityReciever.setNetworkStateListener(TAG, FragmentUserBusinesses.this);
        } else if (!dialogMessage.isShowing()) {
            dialogMessage.show();
            dialogMessage.setMessage(ServerAnswer.getError(getActivity(), errorCode, callerStringID + ">" + TAG));
        }

    }

    @Override
    public void onBusinessCreated() {
        new GetUserBusinesses(context, userUniqueId, FragmentUserBusinesses.this, GET_USER_BUSINESSES_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }

    public RelativeLayout initBusinessLayout(View v) {
        RelativeLayout tempRelLay = (RelativeLayout) v.findViewById(R.id.notExistsUserBusiness);

        tempRelLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        v.findViewById(R.id.notExistsClickableArea).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRegisterActivity();
            }
        });


        return tempRelLay;
    }



    void handleShowNotExistsLayout(){
        if(adapterUserBusinesses.getCount() <= 0)
            notExistsBusinessLayout.setVisibility(View.VISIBLE);
        else
            notExistsBusinessLayout.setVisibility(View.GONE);
    }

    void setShowingNotExistsLayout(boolean showing){
        if(showing)
            notExistsBusinessLayout.setVisibility(View.VISIBLE);
        else
            notExistsBusinessLayout.setVisibility(View.GONE);
    }

    @Override
    public void onBusinessEdited(String editedBusinessId) {
        new GetUserBusinesses(getActivity(), userUniqueId, FragmentUserBusinesses.this, GET_USER_BUSINESSES_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }


    @Override
    public void doTryAgain() {
        new GetUserBusinesses(getActivity(), userUniqueId, FragmentUserBusinesses.this, GET_USER_BUSINESSES_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }
}