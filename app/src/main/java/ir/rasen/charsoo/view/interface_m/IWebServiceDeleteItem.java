package ir.rasen.charsoo.view.interface_m;

/**
 * Created by android on 9/9/2015.
 */
public interface IWebServiceDeleteItem {
    void getResult(int request, Object result , int position) throws Exception;

    void getError(int request, Integer errorCode, String callerStringID,int position) throws Exception;

}
