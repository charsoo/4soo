package ir.rasen.charsoo.view.interface_m;

/**
 * Created by Rasen_iman on 7/20/2015.
 */
public interface IPostUpdateListener {
    void onPostDetailsChange(String dirtyPostId) throws  Exception;
}
