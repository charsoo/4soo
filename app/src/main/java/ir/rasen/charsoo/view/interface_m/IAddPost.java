package ir.rasen.charsoo.view.interface_m;

import ir.rasen.charsoo.controller.object.Post;

/**
 * Created by android on 4/6/2015.
 */
public interface IAddPost {
    void notifyAddPost(Post post);
}
