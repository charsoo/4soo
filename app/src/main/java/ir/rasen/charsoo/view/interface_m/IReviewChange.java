package ir.rasen.charsoo.view.interface_m;

import ir.rasen.charsoo.controller.object.Review;

/**
 * Created by android on 3/9/2015.
 */
public interface IReviewChange {
    void notifyDeleteReview(String reviewId) throws Exception;
    void notifyDeleteReviewFailed(String reviewIntId,String errorMessage) throws Exception;
    void notifyUpdateReview(Review review) throws Exception;
    void notifyUpdateReviewFailed(String reviewIntId,String errorMessage) throws Exception;
}
