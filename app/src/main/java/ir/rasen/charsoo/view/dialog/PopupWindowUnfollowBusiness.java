package ir.rasen.charsoo.view.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.view.interface_m.IUnfollowBusiness;
import ir.rasen.charsoo.view.widgets.WaitDialog;
import ir.rasen.charsoo.view.widgets.buttons.ButtonFont;


public class PopupWindowUnfollowBusiness extends PopupWindow {
    Context context;


    @SuppressLint("NewApi")
    public PopupWindowUnfollowBusiness(final Context context, final String businessId, final IUnfollowBusiness iUnfollowBusiness) {
        super(context);

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_post_report_cancel_share_more, null, false);
        setContentView(view);

        view.findViewById(R.id.btn_post_more_cancel_share).setVisibility(View.GONE);
        ButtonFont viewReport = (ButtonFont) view.findViewById(R.id.btn_post_more_report);

        viewReport.setText(R.string.unfollow_business);

        viewReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogUnfollowBusinessConfirmation d = new DialogUnfollowBusinessConfirmation(context, businessId, iUnfollowBusiness);
                d.show();
                PopupWindowUnfollowBusiness.this.dismiss();
            }
        });

        setBackgroundDrawable(new BitmapDrawable());
        setOutsideTouchable(true);
        setWindowLayoutMode(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

    }


}
