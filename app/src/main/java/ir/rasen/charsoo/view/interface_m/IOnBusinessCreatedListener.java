package ir.rasen.charsoo.view.interface_m;

/**
 * Created by Rasen_iman on 7/21/2015.
 */
public interface IOnBusinessCreatedListener {
    public void onBusinessCreated();

}
