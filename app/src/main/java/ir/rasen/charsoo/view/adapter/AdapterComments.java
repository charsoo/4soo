package ir.rasen.charsoo.view.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Globals;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.Comment;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.view.activity.ActivityComments;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.dialog.PopupWindowDeleteCommentBlockUser;
import ir.rasen.charsoo.view.dialog.PopupWindowEditDeleteComment;
import ir.rasen.charsoo.view.interface_m.ICommentChange;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.imageviews.ImageViewCircle;

/**
 * Created by android on 3/7/2015.
 */
public class AdapterComments extends BaseAdapter implements ICommentChange, IWebservice {
    public static final String TAG="AdapterPostComments";


    private ArrayList<Comment> comments;
    private Activity context;
    SimpleLoader simpleLoader;
    IWebservice iWebservice;
//    WaitDialog progressDialog;
    String postOwnerBusinessId, postId;
    private ICommentChange iCommentChange;
    boolean isUserOwner;
    PopupWindowEditDeleteComment popupWindowEditDeleteComment;
    CharsooActivity charsooActivity;

    DialogMessage dialogMessage;


    public static final int BLOCK_USER_REQUEST = 100;

    public AdapterComments(CharsooActivity charsooActivity, boolean isUserOwner, String postId,
                           String postOwnerBusinessId, ArrayList<Comment> comments) {
        this.charsooActivity=charsooActivity;
        this.comments = new ArrayList<>(comments);

        context = charsooActivity;

        simpleLoader = new SimpleLoader(context);
        this.iWebservice = this;

        this.postOwnerBusinessId = postOwnerBusinessId;
        this.iCommentChange = this;
        this.isUserOwner = isUserOwner;
        this.postId = postId;

        dialogMessage = new DialogMessage(charsooActivity,"");
    }

    @Override
    public int getCount() {
        return comments.size();
    }

    @Override
    public Object getItem(int position) {
        return comments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public boolean ifHidePopup(){
        if (popupWindowEditDeleteComment==null)
            return false;
        else if (popupWindowEditDeleteComment.isShowing()){
            popupWindowEditDeleteComment.dismiss();
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final Holder holder;

        if (view == null) {
            holder = new Holder();
            view = LayoutInflater.from(context).inflate(R.layout.item_comment, viewGroup, false);
            holder.imageViewImage = (ImageViewCircle) view.findViewById(R.id.item_comment_img);
            holder.textViewUserIdentifier = (TextViewFont) view.findViewById(R.id.item_comment_user_identifier);
            holder.textViewText = (TextViewFont) view.findViewById(R.id.item_comment_text);
            holder.imgMore = (ImageView) view.findViewById(R.id.img_more);
            view.setTag(holder);
        } else
            holder = (Holder) view.getTag();



        //download image with customized class via imageId
        simpleLoader.loadImage(comments.get(position).userProfilePictureUniqueId, Image_M.SMALL,
                Image_M.ImageType.USER, holder.imageViewImage);


        holder.textViewUserIdentifier.setText(comments.get(position).username);
        holder.textViewText.setText(comments.get(position).text);

        if (isUserOwner ||
            comments.get(position).ownerUniqueId.equals(LoginInfo.getInstance().getUserUniqueId())
            )
            holder.imgMore.setVisibility(View.VISIBLE);
        else
            holder.imgMore.setVisibility(View.GONE);

        holder.imgMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isUserOwner &&
                        !comments.get(position).ownerUniqueId.equals(LoginInfo.getInstance().getUserUniqueId()) &&
                        !comments.get(position).ownerUniqueId.equals(Globals.getInstance().getValue().businessVisiting.uniqueId)
                        ) {
                    //the business owner is watching the posts and the comment doesn't belong to the user
                    //new PopupBlockUser(context,postOwnerBusinessId, comments.get(position).ownerUniqueId, AdapterPostComments.this).show();
                    new PopupWindowDeleteCommentBlockUser(charsooActivity, Globals.getInstance().getValue().businessVisiting.uniqueId
                            , comments.get(position),
                            iWebservice, iCommentChange,BLOCK_USER_REQUEST).showAsDropDown(view);
                } else {
                    popupWindowEditDeleteComment= new PopupWindowEditDeleteComment(charsooActivity, comments.get(position), iCommentChange);
                    popupWindowEditDeleteComment.showAsDropDown(holder.imgMore);

                }

            }
        });


        holder.textViewUserIdentifier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoOwnerPage(comments.get(position));
            }
        });
        holder.imageViewImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoOwnerPage(comments.get(position));
            }
        });
        return view;
    }

    public void gotoOwnerPage(Comment cm){
        if(cm.isUserSendedComment)
            User.goUserHomeInfoPage(context,cm.ownerUniqueId);
        else
            Business.gotoBusinessOtherPage(context,cm.ownerUniqueId);
    }

    @Override
    public void notifyDeleteComment(String commentId) {
        for (int i = 0; i < comments.size(); i++) {
            if (comments.get(i).uniqueId.equals(commentId)) {
                //notify the FragmentHome to get updated last three comments for the post
                Intent intent = new Intent(Params.UPDATE_TIME_LINE_POST_LAST_THREE_COMMENTS);
                intent.putExtra(Params.POST_ID_INT, postId);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

                comments.remove(i);
                notifyDataSetChanged();
                break;
            }
        }
        if(charsooActivity.isShowingWaitDialog())
//            progressDialog.dismiss();
            charsooActivity.hideWaitDialog();

        MyApplication.broadCastUpdateDirtyPost(TAG,postId);
    }

    @Override
    public void notifyDeleteCommentFailed(String commentId, String errorMessag) {
        if(charsooActivity.isShowingWaitDialog())
            charsooActivity.hideWaitDialog();

        dialogMessage.show();
        dialogMessage.setMessage(errorMessag);
    }

    @Override
    public void notifyUpdateComment(Comment comment) {
        for (int i = 0; i < comments.size(); i++) {
            if (comments.get(i).uniqueId.equals(comment.uniqueId)) {
                comments.get(i).text = comment.text;
//                Intent intent = new Intent(Params.UPDATE_TIME_LINE_POST_LAST_THREE_COMMENTS);
//                intent.putExtra(Params.POST_ID_INT, postId);
//                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                MyApplication.broadCastUpdateDirtyPost(ActivityComments.TAG,comment.postUniqueId);

                notifyDataSetChanged();
                break;
            }
        }

        if(charsooActivity.isShowingWaitDialog())
//            progressDialog.dismiss();
              charsooActivity.hideWaitDialog();

        MyApplication.broadCastUpdateDirtyPost(TAG,postId);
    }

    @Override
    public void notifyUpdateCommentFailed(String commentId, String errorMessag) {
        if(charsooActivity.isShowingWaitDialog())
//            progressDialog.dismiss();
            charsooActivity.hideWaitDialog();

        if(!charsooActivity.isShowingWaitDialog()) {
            charsooActivity.showWaitDialog(errorMessag);
        }
    }

    @Override
    public void getResult(int reqCode,Object result) {
        switch (reqCode){
            case BLOCK_USER_REQUEST :
                charsooActivity.hideWaitDialog();
                Toast.makeText(context, context.getString(R.string.block_done), Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void getError(int reqCode,Integer errorCode,String callerStringID) {
        switch (reqCode){
            case BLOCK_USER_REQUEST :
                //get BlockUser's error
                new DialogMessage(context,callerStringID+">"+TAG + context.getString(R.string.block_error)).show();
                break;
        }

    }
    //Each item in this adapter has a picture and a title

    private class Holder {
        ImageViewCircle imageViewImage;
        TextViewFont textViewUserIdentifier;
        TextViewFont textViewText;
        ImageView imgMore;
    }


    public void loadMore(ArrayList<Comment> newComments) {
        this.comments.addAll(newComments);
        notifyDataSetChanged();
    }

    public void resetItems(ArrayList<Comment> newComments) {
        this.comments=new ArrayList<>(newComments);
        notifyDataSetChanged();
    }
    public void addCommentToFirst(Comment cm){
        comments.add(0, cm);
        notifyDataSetChanged();
    }
}
