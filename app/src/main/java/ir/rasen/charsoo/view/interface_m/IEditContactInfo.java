package ir.rasen.charsoo.view.interface_m;

/**
 * Created by android on 8/8/2015.
 */
public interface IEditContactInfo {
    void onContactEdited();
}
