package ir.rasen.charsoo.view.shared;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.Permission;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.view.activity.user.ActivityUserFollowingBusinesses;
import ir.rasen.charsoo.view.activity.user.ActivityUserFriends;
import ir.rasen.charsoo.view.activity.user.ActivityUserProfile;
import ir.rasen.charsoo.view.activity.user.ActivityUserReviews;
import ir.rasen.charsoo.view.adapter.AdapterPostGridAndList;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IPullToRefresh;
import ir.rasen.charsoo.view.interface_m.ISharePostChange;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.buttons.FloatButton;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.HFGridView;

/**
 * Created by android on 3/14/2015.
 */
public class GridViewUserBothView implements ISharePostChange {
    public static final String TAG = "GridViewUserBothView";

    public static final int GET_SHARED_POSTS = 11;

    public HFGridView gridViewHeader;
    //    AdapterPostGrid adapterPostGrid;
//    AdapterPost adapterPost;
    public AdapterPostGridAndList adapterPostGridAndList;

    public boolean isThreeColumn;

    FloatButton imageViewFriends, imageViewReviews, imageViewFollowingBusinesses, imageViewEdit;
    View switchGrid, switchList;
    ImageView imageViewCover, imageViewHasRequest;
    //TextViewFont textViewFriends, textViewBusinesses, textViewReviews,
    TextViewFont textViewIdentifier, textViewName, textViewAboutMe;
    View viewHeader;
    Activity activity;
    String profilePictureId;
    View listFooterView;
    boolean isLoadingMore;
    ArrayList<Post> posts;
    String visitedUserId;
    public boolean hasHeader;
    boolean hasRequest;
    String userIdentifier, userName, aboutMe;
    Permission userPermissions;
    DialogMessage dialogMessage;
    IPullToRefresh iPullToRefresh;
    User user;

    View viewNothing;
    ImageView imgNothing;
    TextViewFont txtNothing;

    public static String parentTag;

    public GridViewUserBothView
            (Activity activity, User user, String visitedUserId, HFGridView gridViewHeader,
             DialogMessage dialogMessage, String parentTag, IPullToRefresh iPullToRefresh) {
        this.activity = activity;
        this.profilePictureId = user.profilePictureUniqueId;
        this.gridViewHeader = gridViewHeader;
        this.visitedUserId = visitedUserId;
        this.hasHeader = false;
        isThreeColumn = true;
        isLoadingMore = false;
        this.userIdentifier = user.userIdentifier;
        this.userName = user.name;
        this.hasRequest = (user.friendRequestNumber > 0);
        this.aboutMe = user.aboutMe;
        this.dialogMessage = dialogMessage;
        this.iPullToRefresh = iPullToRefresh;
        this.parentTag = parentTag;
    }

    public void hideRequestAnnouncement() {
        imageViewHasRequest.setVisibility(View.GONE);
    }

    public void initialProfilePicture(String userPictureString) {
        imageViewCover.setImageBitmap(Image_M.getBitmapFromString(userPictureString));
    }

    public void InitialGridViewUser(ArrayList<Post> postList, boolean beThreeColumn, boolean hasHeader) {
        this.isThreeColumn = beThreeColumn;
        this.hasHeader = hasHeader;
        posts=(ArrayList<Post>)postList.clone();
//        adapterPostGrid = new AdapterPostGrid(activity, searchItemPosts, 0, Post.GetPostType.SHARE);
//
//        adapterPost = new AdapterPost(GridViewUserBothView.this, activity, posts);

        adapterPostGridAndList
                = new AdapterPostGridAndList(
                activity, "0", Post.GetPostType.SHARE,
                GridViewUserBothView.this, activity, posts, isThreeColumn, parentTag);


        if (!hasHeader) {
            viewHeader = (activity).getLayoutInflater().inflate(R.layout.layout_header_user, null);


            switchGrid = viewHeader.findViewById(R.id.btn_switch_grid);
            switchList = viewHeader.findViewById(R.id.btn_switch_list);
            imageViewCover = (ImageView) viewHeader.findViewById(R.id.imageView_cover);

            imageViewFriends = (FloatButton) viewHeader.findViewById(R.id.imageView_friends);
            imageViewHasRequest = (ImageView) viewHeader.findViewById(R.id.imageView_has_request);
            imageViewReviews = (FloatButton) viewHeader.findViewById(R.id.imageView_reviews);
            imageViewFollowingBusinesses = (FloatButton) viewHeader.findViewById(R.id.imageView_businesses);
            imageViewEdit = (FloatButton) viewHeader.findViewById(R.id.imageView_edit);

            //textViewBusinesses = (TextViewFont) viewHeader.findViewById(R.uniqueId.textView_businesses);
            //textViewFriends = (TextViewFont) viewHeader.findViewById(R.uniqueId.textView_friends);
            //textViewReviews = (TextViewFont) viewHeader.findViewById(R.uniqueId.textView_reviews);

            textViewIdentifier = (TextViewFont) viewHeader.findViewById(R.id.textView_user_identifier);
            textViewName = (TextViewFont) viewHeader.findViewById(R.id.textView_user_name);
            textViewAboutMe = (TextViewFont) viewHeader.findViewById(R.id.textView_user_about_me);

            viewNothing = viewHeader.findViewById(R.id.view_nothing);
            txtNothing = (TextViewFont) viewHeader.findViewById(R.id.txt_nothing);
            imgNothing = (ImageView) viewHeader.findViewById(R.id.img_nothing);
            viewNothing.setVisibility(View.GONE);

            textViewIdentifier.setText(String.valueOf(userIdentifier));


            SpannableStringBuilder builder = new SpannableStringBuilder();

            String userId;
            if (userName.substring(0, 1).matches("[a-zA-Z]"))
                userId = "@" + userIdentifier;
            else
                userId = userIdentifier + "@";
            SpannableString redSpannable = new SpannableString(userId);
            redSpannable.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.button_on_dark)), 0, userId.length(), 0);


            builder.append(userName + " ");
            builder.append("(");
            builder.append(redSpannable);
            builder.append(")");

            textViewName.setText(builder, TextView.BufferType.SPANNABLE);

            if (!aboutMe.equals("null"))
                textViewAboutMe.setText(aboutMe);


            if (!hasRequest)
                imageViewHasRequest.setVisibility(View.GONE);

            SimpleLoader simpleLoader = new SimpleLoader(activity);
            simpleLoader.loadImage(profilePictureId, Image_M.LARGE, Image_M.ImageType.USER, imageViewCover);


            imageViewEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, ActivityUserProfile.class);
                    activity.startActivity(intent);
                }
            });

//            imageViewSearch.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent intent = new Intent(activity, UNUSED_ActivitySearchUser.class);
//                    intent.putExtra(Params.SEARCH_KEY_WORD, "key");
//                    activity.startActivity(intent);
//                }
//            });

            imageViewFollowingBusinesses.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent1 = new Intent(activity, ActivityUserFollowingBusinesses.class);
                    intent1.putExtra(Params.VISITED_USER_UNIQUE_ID, visitedUserId);
                    activity.startActivity(intent1);
                }
            });
            imageViewFriends.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent1 = new Intent(activity, ActivityUserFriends.class);
                    intent1.putExtra(Params.VISITED_USER_UNIQUE_ID, visitedUserId);
                    intent1.putExtra(Params.HAS_REQUEST, hasRequest);
                    activity.startActivityForResult(intent1, 0);
                }
            });

           /* textViewRequests.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent1 = new Intent(activity, ActivityFriendRequests.class);
                    intent1.putExtra(Params.VISITED_USER_UNIQUE_ID, visitedUserId);
                    activity.startActivity(intent1);
                }
            });*/
            imageViewReviews.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent1 = new Intent(activity, ActivityUserReviews.class);
                    intent1.putExtra(Params.VISITED_USER_UNIQUE_ID, visitedUserId);
                    activity.startActivity(intent1);
                }
            });

            switchList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gridViewHeader.setNumColumns(1);
                    gridViewHeader.setColumnWidth(activity.getWindowManager().getDefaultDisplay().getWidth());
//                    gridViewHeader.setVerticalSpacing(3);
//                    gridViewHeader.setHorizontalSpacing(9);
//                    gridViewHeader.setViewWidthIfItsZero(activity.getWindowManager().getDefaultDisplay().getWidth());

                    //now it has one column
                    isThreeColumn = false;
                    adapterPostGridAndList.setViewType(isThreeColumn);
//                    gridViewHeader.setAdapter(adapterPostGridAndList);

                    switchList.setBackgroundColor(activity.getResources().getColor(R.color.lightPrimaryColor));
                    switchGrid.setBackgroundColor(activity.getResources().getColor(R.color.material_gray_light));
                }
            });
            switchGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    prepareGridThreeColumn(gridViewHeader);
                    gridViewHeader.setNumColumns(3);
                    isThreeColumn = true;
                    adapterPostGridAndList.setViewType(isThreeColumn);

                    // now it has three column
                    switchGrid.setBackgroundColor(activity.getResources().getColor(R.color.lightPrimaryColor));
                    switchList.setBackgroundColor(activity.getResources().getColor(R.color.material_gray_light));
                }
            });


            gridViewHeader.addHeaderView(viewHeader);
//            if (gridViewHeader.getHeaderViewsCount()>1)
//                gridViewHeader.removeHeaderView(gridViewHeader.getHeaderView());
            this.hasHeader = true;

            listFooterView = ((LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_loading_more, null, false);
            gridViewHeader.addFooterView(listFooterView);
        } else {
            if (listFooterView != null) {
                listFooterView.setVisibility(View.GONE);
            }
        }


        gridViewHeader.setBackgroundColor(Color.parseColor("#ffffff"));


        gridViewHeader.setOnScrollListener(new AbsListView.OnScrollListener() {
            int currentFirstVisibleItem,
                    currentVisibleItemCount,
                    currentScrollState;

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
            }

            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            private void isScrollCompleted() {
                if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
                    if (!isLoadingMore
                            && posts.size() > 0 && posts.size() % activity.getResources().getInteger(R.integer.lazy_load_limitation) == 0) {
                        loadMoreData();
                    }
                }
            }
        });

//        if (posts.size() == 0) {
//            //imageViewCirecle.setVisibility(View.GONE);
//        } else {
//            //if (imageViewCirecle != null) {
//            //    imageViewCirecle.setVisibility(View.VISIBLE);
//            //}
//        }

        /*if(isThreeColumn) {
            prepareGridThreeColumn(gridViewHeader);
            gridViewHeader.setAdapter(adapterPostGrid);
        } else {
            gridViewHeader.setNumColumns(1);
            gridViewHeader.setAdapter(adapterPostShared);
        }*/
        if (isThreeColumn) {
//            gridViewHeader.setNumColumns(3);
//            gridViewHeader.setVerticalSpacing(3);
//            gridViewHeader.setHorizontalSpacing(9);
            //        gridViewHeader.setAdapter(adapterPostShared);
            //now it has one column
            //        isThreeColumn = false;
            //        switchList.setBackgroundColor(activity.getResources().getColor(R.color.material_blue_light));
            //        switchGrid.setBackgroundColor(activity.getResources().getColor(R.color.material_gray_light));
//            gridViewHeader.setNumColumns(3);
//            gridViewHeader.setVerticalSpacing(3);
//            gridViewHeader.setHorizontalSpacing(9);
            prepareGridThreeColumn(gridViewHeader);
            gridViewHeader.setAdapter(adapterPostGridAndList);
            // now it has three column
            switchGrid.setBackgroundColor(activity.getResources().getColor(R.color.lightPrimaryColor));
            switchList.setBackgroundColor(activity.getResources().getColor(R.color.material_gray_light));
        } else {
            gridViewHeader.setNumColumns(1);
            gridViewHeader.setAdapter(adapterPostGridAndList);
            switchList.setBackgroundColor(activity.getResources().getColor(R.color.lightPrimaryColor));
            switchGrid.setBackgroundColor(activity.getResources().getColor(R.color.material_gray_light));
        }
        noPosts();
    }

    // LOAD MORE DATA
    public void loadMoreData() {
        // LOAD MORE DATA HERE...
        isLoadingMore = true;
        listFooterView.setVisibility(View.VISIBLE);
        iPullToRefresh.notifyLoadMore();
//        new GetSharedPosts(activity, visitedUserId, posts.get(posts.size() - 1).uniqueId, activity.getResources().getInteger(R.integer.lazy_load_limitation), GridViewUserBothView.this,GET_SHARED_POSTS).executeWithNewSolution();;
    }

    public void addMorePosts(ArrayList<Post> newPosts) {
        posts.addAll(newPosts);
        handleShowingNotExistsLayout();
//        noPosts();
        listFooterView.setVisibility(View.GONE);
        adapterPostGridAndList.loadMoreItems(newPosts);
        isLoadingMore = false;
    }

    private void prepareGridThreeColumn(HFGridView gridViewHeader) {
        gridViewHeader.setNumColumns(3);
        gridViewHeader.setVerticalSpacing(3);
        gridViewHeader.setHorizontalSpacing(9);
        gridViewHeader.setViewWidthIfItsZero(activity.getWindowManager().getDefaultDisplay().getWidth());
        isThreeColumn = true;
    }


    public void hideLoader() {
        listFooterView.setVisibility(View.GONE);
    }

    @Override
    public void notifyOnShareCanceled(String postID_int) {
        if (adapterPostGridAndList != null)
            adapterPostGridAndList.removePostByIntID(postID_int);
    }


    public void refreshUserData(User user, String visitedUserId) {
        if (this.profilePictureId != user.profilePictureUniqueId) {
            this.profilePictureId = user.profilePictureUniqueId;
            SimpleLoader simpleLoader = new SimpleLoader(activity);
            simpleLoader.loadImage(profilePictureId, Image_M.LARGE, Image_M.ImageType.USER, imageViewCover);
        }
        this.visitedUserId = visitedUserId;
        this.userIdentifier = user.userIdentifier;
        this.userName = user.name;
        this.hasRequest = (user.friendRequestNumber > 0);
        this.aboutMe = user.aboutMe;

        textViewIdentifier.setText(String.valueOf(userIdentifier));

        SpannableStringBuilder builder = new SpannableStringBuilder();

        String userId;
        if (userName.substring(0, 1).matches("[a-zA-Z]"))
            userId = "@" + userIdentifier;
        else
            userId = userIdentifier + "@";
        SpannableString redSpannable = new SpannableString(userId);
        redSpannable.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.button_on_dark)), 0, userId.length(), 0);


        builder.append(userName);
        builder.append(" (");
        builder.append(redSpannable);
        builder.append(")");

        textViewName.setText(builder, TextView.BufferType.SPANNABLE);

        if (!aboutMe.equals("null"))
            textViewAboutMe.setText(aboutMe);


        if (!hasRequest)
            imageViewHasRequest.setVisibility(View.GONE);


    }


    public void resetPostItems(ArrayList<Post> newPosts) {
        posts = new ArrayList<>(newPosts);
        adapterPostGridAndList.resetItems(posts);
    }
    public void noPosts() {
        handleShowingNotExistsLayout();
    }

    public void handleShowingNotExistsLayout(){
        if (posts.size() == 0) {

            txtNothing.setText(R.string.no_posts);
            imgNothing.setImageResource(R.mipmap.ic_ops);

            viewNothing.setVisibility(View.VISIBLE);
        } else {
            viewNothing.setVisibility(View.GONE);
        }
    }
}
