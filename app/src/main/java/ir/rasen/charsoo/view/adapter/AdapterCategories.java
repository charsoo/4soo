package ir.rasen.charsoo.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.object.ParentCategory;
import ir.rasen.charsoo.view.fragment.search.FragmentSearch;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.imageviews.ImageViewArrow;

/**
 * Created by 'Sina KH' on 6/29/2015.
 */
public class AdapterCategories extends BaseAdapter {

    private ArrayList<ParentCategory> items;
    private Context context;
    private FragmentSearch fragmentSearch;
    public String selectedId;

    public AdapterCategories(Context context, ArrayList<ParentCategory> items, FragmentSearch fragmentSearch) {
        this.context = context;
        this.items = items;
        this.fragmentSearch = fragmentSearch;
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        Holder holder;

        if (view == null) {
            holder = new Holder();
            view = LayoutInflater.from(context).inflate(R.layout.item_category, viewGroup, false);
            holder.arrow = (ImageViewArrow) view.findViewById(R.id.arrow);
            holder.name = (TextViewFont) view.findViewById(R.id.txt);
            holder.divider = view.findViewById(R.id.ll_divider);
            view.setTag(holder);
        } else
            holder = (Holder) view.getTag();

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (items.get(position).isSub) {
                    selectedId=items.get(position).uniqueId;
                    notifyDataSetChanged();
                    fragmentSearch.subcatSelected(items.get(position).uniqueId);
                } else if (items.size() > position + 1 ? items.get(position + 1).isSub : false)
                    fragmentSearch.catUnselected(position);
                else
                    fragmentSearch.catSelected(items.get(position).uniqueId, position);
            }
        });
        holder.name.setText(items.get(position).name);
        if (items.get(position).isSub) {
            holder.arrow.setVisibility(View.INVISIBLE);
        } else {
            holder.arrow.setVisibility(View.VISIBLE);
            holder.arrow.setExpanded(items.size() > position + 1 ? items.get(position + 1).isSub : false);
        }
        holder.divider.setVisibility(items.size() > position + 1 && items.get(position + 1).isSub && !items.get(position).isSub ? View.VISIBLE : View.INVISIBLE);
        if (items.get(position).uniqueId == selectedId) {
            holder.name.setTextColor(context.getResources().getColor(R.color.primaryColor));
            //view.setBackgroundResource(android.R.color.holo_blue_light);
        } else {
            holder.name.setTextColor(context.getResources().getColor(android.R.color.black));
            //view.setBackgroundResource(R.drawable.view_bg);
        }

        return view;
    }


    private class Holder {
        ImageViewArrow arrow;
        TextViewFont name;
        View divider;
    }
}
