package ir.rasen.charsoo.view.dialog;

import android.content.Context;
import android.view.View;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.model.post.CancelShare;
import ir.rasen.charsoo.view.interface_m.IShareCancelShareListener;
import ir.rasen.charsoo.view.interface_m.IUpdateTimeLine;
import ir.rasen.charsoo.view.widgets.material_library.widgets.Dialog;


public class DialogCancelShareConfirmationUserShared extends Dialog {
    Context context;

    IShareCancelShareListener delegate;

    public DialogCancelShareConfirmationUserShared(final Context context,final String postId,final IUpdateTimeLine iUpdateTimeLine, final IShareCancelShareListener delegate) {
        super(context, context.getResources().getString(R.string.cancel_share),context.getResources().getString(R.string.confirmation_cancel_share));

        this.delegate=delegate;

        addCancelButton(R.string.cancel);
        setAcceptText(R.string.yes);

        //set onClickListener for the ok button
        setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CancelShare(context,LoginInfo.getInstance().getUserUniqueId(), postId,iUpdateTimeLine,delegate,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                dismiss();
            }
        });

    }

}
