package ir.rasen.charsoo.view.widgets;

import android.content.Context;
import android.util.AttributeSet;

import ir.rasen.charsoo.view.widgets.material_library.views.ProgressBarIndeterminate;

public class MaterialProgressBar extends ProgressBarIndeterminate {

    public MaterialProgressBar(Context context) {
        super(context, null);
        init();
    }

    public MaterialProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
    }

}