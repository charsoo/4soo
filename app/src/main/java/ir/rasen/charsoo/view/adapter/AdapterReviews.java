package ir.rasen.charsoo.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.view.dialog.PopUpReviewActivity;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.imageviews.RoundedImageView;

/**
 * Created by Rasen_iman on 9/8/2015.
 */
public class AdapterReviews extends BaseAdapter{
    Context mContext;
    ArrayList<RequestObject.ReviewItem> reviewItems;

    LayoutInflater mInflater;

    public AdapterReviews(Context context, ArrayList<RequestObject.ReviewItem> items){
        if(items!=null)
            reviewItems = items;
        else
            reviewItems = new ArrayList<>();

        mContext = context;

        mInflater =
                (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return reviewItems.size();
    }

    @Override
    public Object getItem(int position) {
        return reviewItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = mInflater.inflate(R.layout.review_item_layout,null,false);
        RoundedImageView ivUserProfileImage =
                (RoundedImageView) v.findViewById(R.id.iv_UserProfilePicture);

        TextViewFont tvUserName =
                (TextViewFont) v.findViewById(R.id.tv_userName);

        RatingBar ratingBar =
                (RatingBar) v.findViewById(R.id.rate1);

        TextViewFont tvReviewDate =
                (TextViewFont) v.findViewById(R.id.tv_userDate);

        TextViewFont tvReviewText =
                (TextViewFont) v.findViewById(R.id.tv_userComment);

        ImageView ivReview =
                (ImageView) v.findViewById(R.id.ivUserCommentPicture);

        ImageView ivHand =
                (ImageView) v.findViewById(R.id.imageHand);

        final RequestObject.ReviewItem ri = reviewItems.get(position);

        MyApplication.loadUserProfilePicture(mContext, ivUserProfileImage, Image_M.LARGE,
                Image_M.ImageType.USER, ri.UserId);

        tvUserName.setText(ri.UserName);
        ratingBar.setRating(Float.valueOf(ri.Rate));
        tvReviewDate.setText(ri.LastUpdate);
        tvReviewText.setText(ri.Text);

        if(ri.Images!=null && ri.Images.size()!=0 && ri.Images.get(0)!=null)
            MyApplication.loadReviewImage(mContext,ivReview,ri.Images.get(0));
        else
            ivReview.setVisibility(View.GONE);

        ivHand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PopUpReviewActivity((Activity) mContext,ri.RevId).show();
            }
        });

        return v;
    }


    public void resetAllItems(ArrayList<RequestObject.ReviewItem> newItems){
        if(newItems == null || newItems.size()==0)
            reviewItems = new ArrayList<>();
        else
            reviewItems = newItems;
        notifyDataSetChanged();
    }
    public void addItems(ArrayList<RequestObject.ReviewItem> newItems){
        if(newItems!=null && newItems.size()!=0) {
            reviewItems.addAll(newItems);
            notifyDataSetChanged();
        }
    }
}
