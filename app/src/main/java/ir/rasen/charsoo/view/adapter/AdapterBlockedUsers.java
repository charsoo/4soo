package ir.rasen.charsoo.view.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.nfc.Tag;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.BaseAdapterItem;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.model.business.UnblockUser;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.dialog.DialogUnBlockUserConfirmation;
import ir.rasen.charsoo.view.interface_m.IUnblockUserListener;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.imageviews.ImageViewCircle;
import ir.rasen.charsoo.view.widgets.material_library.views.Switch;

/**
 * Created by android on 3/7/2015.
 */
public class AdapterBlockedUsers
        extends BaseAdapter implements IUnblockUserListener{
    public static final String TAG = "AdapterBlockedUsers";


    private ArrayList<BaseAdapterItem> items;
    private CharsooActivity contextActivity;
    SimpleLoader simpleLoader;
    ListView listView;
    String businessId;


    public AdapterBlockedUsers(Context context,String businessId, ArrayList<BaseAdapterItem> items) {
        this.contextActivity = (CharsooActivity) context;
        this.items = new ArrayList<>(items);
        simpleLoader = new SimpleLoader(context);
        this.businessId = businessId;
    }

    public void loadMore(ArrayList<BaseAdapterItem> newItems){
        this.items.addAll(newItems);
        notifyDataSetChanged();
    }

    public void resetItems(ArrayList<BaseAdapterItem> newItems){
        this.items = new ArrayList<>(newItems);
        notifyDataSetChanged();

    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        Holder holder;

        if(listView == null) {
            listView = (ListView) viewGroup;
            listView.setSelector(new ColorDrawable(0x00ffffff));
        }

        if (view == null) {
            holder = new Holder();
            view = LayoutInflater.from(contextActivity).inflate(R.layout.item_blocked_user, viewGroup, false);
            holder.imageViewImage = (ImageViewCircle) view.findViewById(R.id.imageView_base_adapter_item_image);
            holder.textViewUserIdentifier = (TextViewFont) view.findViewById(R.id.textView_base_adapter_item_title);
            holder.imageViewUnblock = (ImageView) view.findViewById(R.id.img_unblock);
            view.setTag(holder);
        } else
            holder = (Holder) view.getTag();

        //download image with customized class via imageId
        simpleLoader.loadImage(items.get(position).getImageId(), Image_M.SMALL, Image_M.ImageType.USER, holder.imageViewImage);
        holder.textViewUserIdentifier.setText(items.get(position).getTitle());
        holder.imageViewUnblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DialogUnBlockUserConfirmation(
                        contextActivity,businessId,items.get(position).getId(),
                        AdapterBlockedUsers.this).show();
            }
        });
        return view;
    }

    @Override
    public void unblockSuccessfull(String unblockedId) {
        contextActivity.hideWaitDialog();
        removeItemFromList(unblockedId);
    }

    @Override
    public void unblockFailed(int errorCode) {
        contextActivity.hideWaitDialog();
        new DialogMessage(contextActivity,
                ServerAnswer.getError(
                        contextActivity,
                        errorCode, TAG)).show();
    }

    void removeItemFromList(String userId){
        for (int i=0;i<items.size();i++){
            if(items.get(i).getId().equals(userId)) {
                items.remove(i);
                notifyDataSetChanged();
                break;
            }

        }

    }


    private class Holder {
        ImageViewCircle imageViewImage;
        TextViewFont textViewUserIdentifier;
        ImageView imageViewUnblock;
    }
}
