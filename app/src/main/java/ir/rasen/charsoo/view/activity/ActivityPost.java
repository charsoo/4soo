package ir.rasen.charsoo.view.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;

import java.util.ArrayList;
import java.util.Arrays;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.post.GetPost;
import ir.rasen.charsoo.view.activity.business.ActivityBusiness;
import ir.rasen.charsoo.view.adapter.AdapterPost;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IDeletePost;
import ir.rasen.charsoo.view.interface_m.ILikeDislikeListener;
import ir.rasen.charsoo.view.interface_m.IPostUpdateListener;
import ir.rasen.charsoo.view.interface_m.IReportPost;
import ir.rasen.charsoo.view.interface_m.IShareCancelShareListener;
import ir.rasen.charsoo.view.interface_m.IUpdateTimeLine;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.shared.PostInitializer;
import ir.rasen.charsoo.view.widgets.WaitDialog;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;


public class ActivityPost extends CharsooActivity
        implements IWebservice, IReportPost, IDeletePost, IUpdateTimeLine,
        NetworkStateChangeListener, ILikeDislikeListener, IShareCancelShareListener,
        TryAgainListener,IPostUpdateListener{

    public static final String TAG = "ActivityPost";
    private static final int GET_POST_INT_CODE = 10,GET_SINGLE_POST_INT_CODE=14;

    WaitDialog progressDialog;

    AdapterPost adapterPost;

    Post post;
    //SimpleLoader simpleLoader;
    Post.GetPostType getPostType;
    String postId, businessId;
    DialogMessage dialogMessage;
    RelativeLayout networkFailedLayout;

    public static IPostUpdateListener iPostUpdateListener;

    boolean isBusinessOwner = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.activity_post);
        setTitle(getString(R.string.product));

        iPostUpdateListener = ActivityPost.this;

        networkFailedLayout=MyApplication.initNetworkErrorLayout(getWindow().getDecorView(), ActivityPost.this,ActivityPost.this);
        dialogMessage = new DialogMessage(this, "");
        postId = getIntent().getExtras().getString(Params.POST_ID_INT);
        businessId = getIntent().getExtras().getString(Params.BUSINESS_ID_STRING);
        isBusinessOwner = getIntent().getExtras().getBoolean(Params.IS_OWNER);
        final String postType = getIntent().getExtras().getString(Params.POST_TYPE);

        if (postType.equals(Post.GetPostType.BUSINESS.name()))
            getPostType = Post.GetPostType.BUSINESS;
        else if (postType.equals(Post.GetPostType.SHARE.name()))
            getPostType = Post.GetPostType.SHARE;
        else if (postType.equals(Post.GetPostType.SEARCH.name()))
            getPostType = Post.GetPostType.SEARCH;

        progressDialog = new WaitDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        //downloadImages = new DownloadImages(this);

        post = new Post();

//        isBusinessOwner = post.getPostType == post.getPostType.BUSINESS;

        adapterPost = new AdapterPost(this, post,isBusinessOwner,ActivityPost.TAG);
        ((ListView) findViewById(R.id.listView)).setAdapter(adapterPost);

        progressDialog.show();
        new GetPost(this, LoginInfo.getInstance().getUserUniqueId(), businessId, postId, getPostType, this, GET_POST_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_post_activity, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Params.ACTION_EDIT_POST) {
                post = ((MyApplication) getApplication()).post;
                adapterPost.notifyDataSetChanged();
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else if (item.getItemId() == R.id.action_refresh) {
            progressDialog.show();
            new GetPost(this,LoginInfo.getInstance().getUserUniqueId(), businessId, postId, getPostType, this, GET_POST_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }


    @Override
    public void notifyDeletePost(String postId) {
//        Intent intent = new Intent(Params.DELETE_POST_FROM_ACTIVITY);
//        intent.putExtra(Params.POST_ID_INT, postUniqueId);
//        LocalBroadcastManager.getInstance(ActivityPost.this).sendBroadcast(intent);

        try {
            ActivityBusiness.iDeletePost.notifyDeletePost(postId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }

    @Override
    public void notifyDeleteFailed(String postId, String failureMessage) {
        if (!dialogMessage.isShowing()){
            dialogMessage.show();
            dialogMessage.setMessage(failureMessage);
        }
    }

    @Override
    public void notifyUpdateTimeLineShare(String postId) {
        Intent intent = new Intent(Params.UPATE_TIME_LINE);
        intent.putExtra(Params.UPDATE_TIME_LINE_TYPE, Params.UPATE_TIME_LINE_TYPE_SHARE);
        intent.putExtra(Params.POST_ID_INT, postId);
        LocalBroadcastManager.getInstance(ActivityPost.this).sendBroadcast(intent);
    }

    @Override
    public void notifyUpdateTimeLineCancelShare(String postId) {
        //it goes to the FragmentUser and the goes to FragmentHome
        if (getPostType == Post.GetPostType.SHARE) {
            Intent intent = new Intent(Params.CANCEL_USER_SHARE_POST);
            intent.putExtra(Params.POST_ID_INT, postId);
            LocalBroadcastManager.getInstance(ActivityPost.this).sendBroadcast(intent);
            finish();
        } else {
            //update time line
            Intent intentUpdateTimeLine = new Intent(Params.UPATE_TIME_LINE);
            intentUpdateTimeLine.putExtra(Params.UPDATE_TIME_LINE_TYPE, Params.UPATE_TIME_LINE_TYPE_CANCEL_SHARE);
            intentUpdateTimeLine.putExtra(Params.POST_ID_INT, postId);
            LocalBroadcastManager.getInstance(ActivityPost.this).sendBroadcast(intentUpdateTimeLine);
        }
    }

    @Override
    public void notifyReportPost(String position, ImageView imageViewMore) {
        post.isReported = true;
        if (!post.isShared)
            adapterPost.notifyDataSetChanged();
    }


    @Override
    public void getResult(int request, Object result) {
        progressDialog.dismiss();

        switch (request){
            case GET_POST_INT_CODE :
                post = (Post) result;
                post.getPostType = getPostType;
                businessId=post.businessUniqueId;
                adapterPost.resetItems(new ArrayList<Post>(Arrays.asList(post)));
                break;

            case GET_SINGLE_POST_INT_CODE :
                Post newPost = (Post) result;
                post = MyApplication.mergeTwoPost(post,newPost);
                adapterPost.resetItems(new ArrayList<Post>(Arrays.asList(post)));
                break;
        }
        NetworkConnectivityReciever.removeIfHaveListener(TAG);
        networkFailedLayout.setVisibility(View.GONE);
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
        progressDialog.dismiss();



        if ( errorCode == ServerAnswer.NETWORK_CONNECTION_ERROR) {
            networkFailedLayout.setVisibility(View.VISIBLE);
            NetworkConnectivityReciever.setNetworkStateListener(TAG, ActivityPost.this);
        }else if (!dialogMessage.isShowing()) {
                dialogMessage.show();
                dialogMessage.setMessage(ServerAnswer.getError(ActivityPost.this, errorCode, callerStringID + ">" + this.getLocalClassName()));

        }
    }

    @Override
    public void doOnNetworkConnected() {
        progressDialog.show();
//        int iiid=LoginInfo.getUserId(this);
        new GetPost(this,LoginInfo.getInstance().getUserUniqueId(), businessId, postId, getPostType, this, GET_POST_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }

    @Override
    public void onDislikeSuccessful(String dislikedPostIntId) {

    }

    @Override
    public void onDislikeFailed(String dislikePostIntId, String failureMessage) {
        if (!dialogMessage.isShowing()) {
            dialogMessage.show();
            dialogMessage.setMessage(failureMessage);
        }
        post.isLiked = true;
        adapterPost.notifyDataSetChanged();
    }

    @Override
    public void onLikeSuccessful(String dislikedPostIntId) {

    }

    @Override
    public void onLikeFailed(String dislikePostIntId, String failureMessage) {
        if (!dialogMessage.isShowing()) {
            dialogMessage.show();
            dialogMessage.setMessage(failureMessage);
        }
        post.isLiked = false;
        adapterPost.notifyDataSetChanged();
    }


    //    IShareCancelShareListener
    @Override
    public void onShareResult(boolean isSuccessful, String postIntId, String failureMessage) {
        if (!isSuccessful) {
            if (!dialogMessage.isShowing()) {
                dialogMessage.show();
                dialogMessage.setMessage(failureMessage);
            }

            post.isShared = false;
            adapterPost.notifyDataSetChanged();
        }else
            MyApplication.broadCastUpdateDirtyPost(ActivityPost.TAG,postIntId);
    }

    //    IShareCancelShareListener
    @Override
    public void onCancelShareResult(boolean isSuccessful, String postIntId, String failureMessage) {
        if(!isSuccessful) {
            if (!dialogMessage.isShowing()) {
                dialogMessage.show();
                dialogMessage.setMessage(failureMessage);
            }
            post.isShared = true;
            adapterPost.notifyDataSetChanged();
        }else
            MyApplication.broadCastUpdateDirtyPost(ActivityPost.TAG,postIntId);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (ActivityComments.comment_change == true) {
            progressDialog.show();
            new GetPost(this,LoginInfo.getInstance().getUserUniqueId(), businessId, postId, getPostType, this, GET_POST_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
        }

    }


    @Override
    public void doTryAgain() {
        progressDialog.show();
        new GetPost(this,LoginInfo.getInstance().getUserUniqueId(), businessId, postId, getPostType, this, GET_POST_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }

    @Override
    public void onPostDetailsChange(String dirtyPostId) {
        if(post.uniqueId.equals(dirtyPostId))
            new GetPost(this,LoginInfo.getInstance().getUserUniqueId(), businessId,
                    dirtyPostId, getPostType, this, GET_SINGLE_POST_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }

    @Override
    public void onBackPressed() {
        if(PostInitializer.dismissAllPopupsIfAnyShowing())
            return;

        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this,"2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}

