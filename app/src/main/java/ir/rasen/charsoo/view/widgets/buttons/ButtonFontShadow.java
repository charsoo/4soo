package ir.rasen.charsoo.view.widgets.buttons;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import ir.rasen.charsoo.view.widgets.material_library.views.ButtonRectangle;

public class ButtonFontShadow extends ButtonRectangle {

    public ButtonFontShadow(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ButtonFontShadow(Context context) {
        super(context, null);
        init(null);
    }

    private void init(AttributeSet attrs) {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/b_yekan.ttf");
        setTypeface(tf);
    }

}