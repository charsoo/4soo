package ir.rasen.charsoo.view.activity.user;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.BaseAdapterItem;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.PullToRefreshList;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.model.friend.GetUserFriendRequests;
import ir.rasen.charsoo.view.adapter.AdapterUserFriendshipRequest;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IPullToRefresh;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.WaitDialog;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.PullToRefreshListView;


public class ActivityUserFriendRequests extends CharsooActivity implements IWebservice, IPullToRefresh {


    String visitedUserId;
    AdapterUserFriendshipRequest adapterFriendshipRequest;
    ListView listView;
    ArrayList<BaseAdapterItem> requests;
    ArrayList<BaseAdapterItem> sampleRequests;
    DialogMessage dialogMessage;
    boolean isRequestsLoadedSuccessful;
    WaitDialog progressDialog;
    //pull_to_refresh_lib
    PullToRefreshList pullToRefreshListView;

    public static final int GET_USER_FRIEND_REQUESTS_REQUEST = 100;

    @Override
    public void notifyRefresh() {
        status = Status.REFRESHING;
        requests.clear();
        new GetUserFriendRequests(
                ActivityUserFriendRequests.this,
                LoginInfo.getInstance().getUserUniqueId(),
                ActivityUserFriendRequests.this,GET_USER_FRIEND_REQUESTS_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
    }

    @Override
    public void notifyLoadMore() {
        loadMoreData();
    }


    private enum Status {FIRST_TIME, LOADING_MORE, REFRESHING, NONE}

    private Status status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.layout_listview_pull_to_refresh);
        setTitle(getString(R.string.friend_requests));

        progressDialog = new WaitDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        dialogMessage=new DialogMessage(ActivityUserFriendRequests.this, "");

        visitedUserId = getIntent().getExtras().getString(Params.VISITED_USER_UNIQUE_ID);

        requests = new ArrayList<>();
        status = Status.FIRST_TIME;
        isRequestsLoadedSuccessful=false;

        pullToRefreshListView = new PullToRefreshList(this, (PullToRefreshListView) findViewById(R.id.pull_refresh_list), ActivityUserFriendRequests.this);
        listView = pullToRefreshListView.getListView();
        adapterFriendshipRequest = new AdapterUserFriendshipRequest(ActivityUserFriendRequests.this, requests ,progressDialog);
        listView.setAdapter(adapterFriendshipRequest);
//        progressDialog.show();
        showWaitDialog();
        new GetUserFriendRequests(
                ActivityUserFriendRequests.this,
                visitedUserId, ActivityUserFriendRequests.this,GET_USER_FRIEND_REQUESTS_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

        //notify FragmentUser to hide request announcement
        Intent intent = new Intent(Params.REMOVE_REQUEST_ANNOUNCEMENT);
        LocalBroadcastManager.getInstance(ActivityUserFriendRequests.this).sendBroadcast(intent);
    }


    public void loadMoreData() {
        // LOAD MORE DATA HERE...
        Toast.makeText(ActivityUserFriendRequests.this, "Load more data", Toast.LENGTH_LONG).show();
        //this webservice doesn't support load more by the now!

       /* status = Status.LOADING_MORE;
         pullToRefreshListView.setFooterVisibility(View.VISIBLE);
        new GetUserFriendRequests(ActivityFriendRequests.this,visitedUserId,ActivityFriendRequests.this).executeWithNewSolution();;*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        /*MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_next_button, menu);*/
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }


    @Override
    public void getResult(int reqCode,Object result) {
        switch (reqCode){
            case GET_USER_FRIEND_REQUESTS_REQUEST :
//                progressDialog.dismiss();
                  hideWaitDialog();

                if (result instanceof ArrayList) {
                    ArrayList<BaseAdapterItem> temp = (ArrayList<BaseAdapterItem>) result;
                    requests.addAll(temp);

                    pullToRefreshListView.setResultSize(requests.size());

                    if (status == Status.FIRST_TIME) {
                        adapterFriendshipRequest = new AdapterUserFriendshipRequest(ActivityUserFriendRequests.this, requests ,progressDialog);
                        listView.setAdapter(adapterFriendshipRequest);
                    } else if (status == Status.REFRESHING) {
                        adapterFriendshipRequest.notifyDataSetChanged();
                        pullToRefreshListView.onRefreshComplete();
                    } else {
                        //it is loading more
                        adapterFriendshipRequest.loadMore(temp);
                        pullToRefreshListView.setFooterVisibility(View.GONE);
                    }
                    status = Status.NONE;
                    isRequestsLoadedSuccessful=true;
                }

                break;
        }
    }



    @Override
    public void getError(int reqCode,Integer errorCode,String callerStringID) {
        switch (reqCode){
            case GET_USER_FRIEND_REQUESTS_REQUEST :
//                progressDialog.dismiss();
                hideWaitDialog();
                pullToRefreshListView.onRefreshComplete();
                if (!dialogMessage.isShowing()){
                    dialogMessage.show();
                    dialogMessage.setMessage(ServerAnswer.getError(ActivityUserFriendRequests.this, errorCode,callerStringID+">"+this.getLocalClassName()));
                }

                break;
        }

    }

    @Override
    public void onBackPressed() {
        backToParent();
    }

    public void backToParent(){
        Intent i = getIntent();
        if (isRequestsLoadedSuccessful) {
            if (adapterFriendshipRequest.getAcceptedUsers().size() != 0) {
                ((MyApplication) getApplication()).newFriends = adapterFriendshipRequest.getAcceptedUsers();
                i.putExtra(Params.NEW_FIREND, true);
            } else {
                ((MyApplication) getApplication()).newFriends = new ArrayList<>();
                i.putExtra(Params.NEW_FIREND, false);
            }
            ArrayList<BaseAdapterItem> acceptedUsers = adapterFriendshipRequest.getAcceptedUsers(), items = adapterFriendshipRequest.getRemainingFriendRequests();
            boolean hasRemainingRequests = false;
            for (int j = 0; j < items.size(); j++) {
                if (!acceptedUsers.contains(items.get(j))) {
                    hasRemainingRequests = true;
                    break;
                }
            }
            i.putExtra(Params.HAS_REMAINIG_FRIEND_REQUESTS, hasRemainingRequests);
        }else {
            ((MyApplication) getApplication()).newFriends = new ArrayList<>();
            i.putExtra(Params.NEW_FIREND, false);
            i.putExtra(Params.HAS_REMAINIG_FRIEND_REQUESTS, true);
        }
        setResult(RESULT_OK, i);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
