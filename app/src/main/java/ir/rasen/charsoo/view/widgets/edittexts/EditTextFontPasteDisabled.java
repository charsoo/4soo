package ir.rasen.charsoo.view.widgets.edittexts;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class EditTextFontPasteDisabled extends EditTextFont {

    boolean errorSet=false;

    public EditTextFontPasteDisabled(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public EditTextFontPasteDisabled(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditTextFontPasteDisabled(Context context) {
        super(context);
        init();
    }

    boolean canPaste()
    {
        return false;
    }
    @Override
    public boolean isSuggestionsEnabled()
    {
        return false;
    }

    private void init() {
        if (!isInEditMode()) {
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (getError() != null)
            setError(null);
        return super.onTouchEvent(event);
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        if (getError() != null)
            setError(null);
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
    }

}