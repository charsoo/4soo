package ir.rasen.charsoo.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.flurry.android.FlurryAgent;

import java.util.ArrayList;
import java.util.List;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.avatar.AvatarActivity;
import ir.rasen.charsoo.avatar.AvatarParams;
import ir.rasen.charsoo.controller.helper.GetContactData;
import ir.rasen.charsoo.controller.helper.GetInstalledApps;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.SharedPrefHandler;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.model.user.GetUserHomeInfo;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessReviews;
import ir.rasen.charsoo.view.activity.user.ActivityUserSetting;
import ir.rasen.charsoo.view.dialog.DialogExit;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.fragment.FragmentUser;
import ir.rasen.charsoo.view.fragment.FragmentUserBusinesses;
import ir.rasen.charsoo.view.fragment.search.FragmentSearch;
import ir.rasen.charsoo.view.fragment.timeline.FragmentTimeline;
import ir.rasen.charsoo.view.interface_m.IGetContactListener;
import ir.rasen.charsoo.view.interface_m.IGetInstalledAppsListener;
import ir.rasen.charsoo.view.interface_m.ILogout;
import ir.rasen.charsoo.view.interface_m.IUpdateUserProfile;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.shared.PostInitializer;
import ir.rasen.charsoo.view.shared.PostInitializerBothView;
import ir.rasen.charsoo.view.shared.ScreenUtils;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.imageviews.ImageViewCircular;

public class ActivityMain extends
        CharsooActivity implements
        View.OnClickListener, IWebservice, IGetContactListener,
        IGetInstalledAppsListener, IUpdateUserProfile ,ILogout {//, IChangeTabs {

    public static final int GET_USER_HOME_INFO_INT_CODE = 11;
    private static final int FRAG_TIMELINE = 0, FRAG_SEARCH = 1, FRAG_BUSINESS = 2, FRAG_USER = 3, Avater = 4;
    private DrawerLayout drawerLayout;
    private View btnHome;

    public static IUpdateUserProfile iUpdateUserProfile;

    boolean footerHome = true, footerUser, footerSearch, footerBusiness;

    //    FragmentHome fragHome;
    FragmentTimeline fragTimeline;
    FragmentUser fragUser;
    FragmentSearch fragSearch;
    FragmentUserBusinesses fragUserBusinesses;

    FragmentManager fm;
    FragmentTransaction ft;
    int screenWidth;
    User user;
    public  int w;

    @Override
    public void successlogout() throws Exception {
        SharedPrefHandler.removeUserEmailPassword(ActivityMain.this);
        Intent i= new Intent(ActivityMain.this, ActivitySplash.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        ActivityMain.this.startActivity(i);
        ActivityMain.this.finish();
    }

    @Override
    public void faillogout(int errorCode, String error) throws Exception {
        hideWaitDialog();
        new DialogMessage(ActivityMain.this, ServerAnswer.getError(ActivityMain.this,
                errorCode, error + ">" + this.getLocalClassName())).show();
    }


    public enum FragmentTag {HOME, SEARCH, BUSINESSES, USER}

    ArrayList<FragmentTag> fragmentTagList = new ArrayList<FragmentTag>();
    Toolbar toolbar;

    boolean openSearchFragmentBln;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        CustomActivityOnCrash.install(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.primaryColor));
        setSupportActionBar(toolbar);
        btnHome = toolbar.findViewById(R.id.btn_home);
        btnHome.setVisibility(View.GONE);
        w = ScreenUtils.convertDpToPx(this, this.getResources().getDimension(R.dimen.profile_pic));
        fragTimeline = new FragmentTimeline();
        fragSearch = new FragmentSearch();
        fragUser = new FragmentUser();
        fragUserBusinesses = new FragmentUserBusinesses();

        fm = getSupportFragmentManager();

        openSearchFragmentBln = getIntent().getBooleanExtra("pleaseOpenFragmentSearch", false);


        screenWidth = getResources().getDisplayMetrics().widthPixels;

        if(openSearchFragmentBln){
            gotoFragmentSearch();
        }else{
            // change!
//        fm.beginTransaction()
//        .show(fm.findFragmentById(R.uniqueId.frag_home))
//        .hide(fm.findFragmentById(R.uniqueId.frag_search))
//        .hide(fm.findFragmentById(R.uniqueId.frag_user))
//        .hide(fm.findFragmentById(R.uniqueId.frag_user_businesses))
//        .commit();
            // change //
            ft = fm.beginTransaction();
            ft.replace(R.id.fragmentContainer, fragTimeline);
            ft.commit();
            fragmentTagList.add(FragmentTag.HOME);

       /* ft.add(R.uniqueId.fragmentContainer, fragmentHome);
        ft.commit();*/

            setUpGeneralFeatures(this.findViewById(android.R.id.content));
            (new Handler()).post(new Runnable() {
                @Override
                public void run() {
                    new GetUserHomeInfo(ActivityMain.this,
                            LoginInfo.getInstance().getUserUniqueId(),LoginInfo.getInstance().getUserUniqueId(),
                            ActivityMain.this, GET_USER_HOME_INFO_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new GetInstalledApps(ActivityMain.this, ActivityMain.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        new GetContactData(ActivityMain.this, ActivityMain.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new GetContactData(ActivityMain.this, ActivityMain.this).execute();
                        new GetInstalledApps(ActivityMain.this, ActivityMain.this).execute();
                    }
                    iUpdateUserProfile = ActivityMain.this;
                }
            });
        }

        initDrawerLayout();
        initNavigationView();

    }

    private void gotoFragmentSearch() {
        ft = fm.beginTransaction();
        toolbar.setVisibility(View.GONE);
        ft.replace(R.id.fragmentContainer, fragSearch);
        ft.commit();
        fragmentTagList.add(FragmentTag.SEARCH);
    }

    @Override
    public void getResult(int request, Object result) {
        switch (request) {
            case GET_USER_HOME_INFO_INT_CODE:
                user = (User) result;
                LoginInfo.getInstance().userUniqueName = user.userIdentifier;
                LoginInfo.getInstance().username = user.name;
                LoginInfo.getInstance().setUserProfilePictureUniqueId(user.profilePictureUniqueId);
                LoginInfo.getInstance().userBusinesses = user.businesses;
                LoginInfo.getInstance().userReceivedFriendRequestCount = user.friendRequestNumber;

                setUserDrawer(user);
                break;
        }
    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {
    }

    public void setFragment(FragmentTag fragmentTag) {
        switch (fragmentTag) {
            case HOME:
                setSelection(FRAG_TIMELINE);
                break;
            case SEARCH:
                setSelection(FRAG_SEARCH);
                break;
        }

    }

    private void exit() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void addFragment(FragmentTag fragmentTag) {
        int fragmentPosition = checkFragment(fragmentTag);
        //if the fragment is not added before
        if (fragmentPosition == -1)
            fragmentTagList.add(fragmentTag);
        else {
            //if the fragment is added before, remove it and add the fragment in the end of the list
            fragmentTagList.remove(fragmentPosition);
            fragmentTagList.add(fragmentTag);
        }
    }

    private int checkFragment(FragmentTag fragmentTag) {
        for (int i = 0; i < fragmentTagList.size(); i++) {
            if (fragmentTag == fragmentTagList.get(i))
                return i;
        }
        return -1;
    }

    // change

//    Handler handlerUserBusinessesFragment = new Handler();
//
//    public void recursivelyCallHandlerUserBusinessesFragment() {
//        handlerUserBusinessesFragment.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //We don't want to run all webservices together
//                //first HomeFragment, second SearchFragment and last UserFragment
//                if (((MyApplication) getApplication()).isUserCreated) {
//                    if (footerBusiness) {
//                        fm.beginTransaction()
//                                .show(fm.findFragmentById(R.uniqueId.frag_user_businesses))
//                                .hide(fm.findFragmentById(R.uniqueId.frag_search))
//                                .hide(fm.findFragmentById(R.uniqueId.frag_home))
//                                .hide(fm.findFragmentById(R.uniqueId.frag_user))
//                                .commit();
//                    }
//                } else
//                    recursivelyCallHandlerUserBusinessesFragment();
//            }
//        }, 500);
//    }

//    Handler handlerUserFragment = new Handler();
//
//    public void recursivelyCallHandlerUserFragment() {
//        handlerUserFragment.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //We don't want to run all webservices together
//                //first HomeFragment, second SearchFragment and last UserFragment
//                if (footerUser) {
//                    fm.beginTransaction()
//                            .show(fm.findFragmentById(R.uniqueId.frag_user))
//                            .hide(fm.findFragmentById(R.uniqueId.frag_search))
//                            .hide(fm.findFragmentById(R.uniqueId.frag_home))
//                            .hide(fm.findFragmentById(R.uniqueId.frag_user_businesses))
//                            .commit();
//                }
//            }
//        }, 500);
//    }

    // change

    private void setSelection(int relativeLayoutId) {
        drawerLayout.closeDrawer(Gravity.RIGHT);
//        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        toolbar.setVisibility(View.VISIBLE);
        switch (relativeLayoutId) {
            case FRAG_TIMELINE:
                if (footerHome)
                    return;
                //getSupportActionBar().show();

                addFragment(FragmentTag.HOME);

                footerHome = true;
                footerBusiness = false;
                footerSearch = false;
                footerUser = false;


                // change
//                fm.beginTransaction()
//                //ft.replace(R.uniqueId.fragmentContainer, fragmentHome);
//                .show(fm.findFragmentById(R.uniqueId.frag_home))
//                .hide(fm.findFragmentById(R.uniqueId.frag_search))
//                .hide(fm.findFragmentById(R.uniqueId.frag_user))
//                .hide(fm.findFragmentById(R.uniqueId.frag_user_businesses))
//                .commit();
                ft = fm.beginTransaction();
                ft.replace(R.id.fragmentContainer, fragTimeline);
                ft.commit();
                // change


                ((ImageView) drawerLayout.findViewById(R.id.drawer_home_img)).setImageResource(R.mipmap.ic_home_active);
                ((ImageView) drawerLayout.findViewById(R.id.drawer_businesses_img)).setImageResource(R.mipmap.ic_business);
                ((ImageView) drawerLayout.findViewById(R.id.avater_img)).setImageResource(R.mipmap.avater);
                findViewById(R.id.drawer_home).setBackgroundColor(getResources().getColor(R.color.material_gray_light));
                findViewById(R.id.drawer_businesses).setBackgroundResource(R.drawable.background_menu);
                findViewById(R.id.avater).setBackgroundResource(R.drawable.background_menu);
                btnHome.setVisibility(View.GONE);

                break;
            case FRAG_SEARCH:
                if (footerSearch)
                    return;

                //getSupportActionBar().hide();
                addFragment(FragmentTag.SEARCH);

                footerHome = false;
                footerBusiness = false;
                footerSearch = true;
                footerUser = false;

//                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                toolbar.setVisibility(View.GONE);

                // change
//                fm.beginTransaction()
//                .show(fm.findFragmentById(R.uniqueId.frag_search))
//                .hide(fm.findFragmentById(R.uniqueId.frag_home))
//                .hide(fm.findFragmentById(R.uniqueId.frag_user))
//                .hide(fm.findFragmentById(R.uniqueId.frag_user_businesses))
//                .commit();
                ft = fm.beginTransaction();
                ft.replace(R.id.fragmentContainer, fragSearch);
                ft.commit();

                ((ImageView) drawerLayout.findViewById(R.id.drawer_home_img)).setImageResource(R.mipmap.ic_home);
                ((ImageView) drawerLayout.findViewById(R.id.avater_img)).setImageResource(R.mipmap.avater);
                ((ImageView) drawerLayout.findViewById(R.id.drawer_businesses_img)).setImageResource(R.mipmap.ic_business);
                findViewById(R.id.drawer_home).setBackgroundResource(R.drawable.background_menu);
                findViewById(R.id.drawer_businesses).setBackgroundResource(R.drawable.background_menu);
                findViewById(R.id.avater).setBackgroundResource(R.drawable.background_menu);
                btnHome.setVisibility(View.VISIBLE);
                break;
            case FRAG_USER:
//                if (footerUser)
//                    return;

                //getSupportActionBar().hide();
//                addFragment(FragmentTag.USER);

                footerHome = false;
                footerBusiness = false;
                footerSearch = false;
//                footerUser=true;

                // change
//                fm.beginTransaction()
//                .show(fm.findFragmentById(R.uniqueId.frag_user))
//                .hide(fm.findFragmentById(R.uniqueId.frag_search))
//                .hide(fm.findFragmentById(R.uniqueId.frag_home))
//                .hide(fm.findFragmentById(R.uniqueId.frag_user_businesses))
//                .commit();
//                ft=fm.beginTransaction();
//                ft.replace(R.uniqueId.fragmentContainer,fragUser);
//                ft.commit();
//                // change
//
//                //ft.replace(R.uniqueId.fragmentContainer, fragmentUser);
//
//                // change
////                if (((MyApplication) getApplication()).isUserCreated)
////                    recursivelyCallHandlerUserFragment();
//                // change

                User.goUserHomeInfoPage(ActivityMain.this, LoginInfo.getInstance().getUserUniqueId());

                ((ImageView) drawerLayout.findViewById(R.id.drawer_home_img)).setImageResource(R.mipmap.ic_home);
                ((ImageView) drawerLayout.findViewById(R.id.avater_img)).setImageResource(R.mipmap.avater);
                ((ImageView) drawerLayout.findViewById(R.id.drawer_businesses_img)).setImageResource(R.mipmap.ic_business);
                findViewById(R.id.drawer_home).setBackgroundResource(R.drawable.background_menu);
                findViewById(R.id.drawer_businesses).setBackgroundResource(R.drawable.background_menu);
                findViewById(R.id.avater).setBackgroundResource(R.drawable.background_menu);

                btnHome.setVisibility(View.VISIBLE);
                break;
            case FRAG_BUSINESS:
                //getSupportActionBar().hide();
                if (footerBusiness)
                    return;
                addFragment(FragmentTag.BUSINESSES);
                initialUserBusinessesTab();

                ((ImageView) drawerLayout.findViewById(R.id.drawer_home_img)).setImageResource(R.mipmap.ic_home);
                ((ImageView) drawerLayout.findViewById(R.id.avater_img)).setImageResource(R.mipmap.avater);
                ((ImageView) drawerLayout.findViewById(R.id.drawer_businesses_img)).setImageResource(R.mipmap.ic_business_active);
                findViewById(R.id.drawer_home).setBackgroundResource(R.drawable.background_menu);
                findViewById(R.id.avater).setBackgroundResource(R.drawable.background_menu);
                findViewById(R.id.drawer_businesses).setBackgroundColor(getResources().getColor(R.color.material_gray_light));

                btnHome.setVisibility(View.VISIBLE);
                break;
            case Avater:
                if (LoginInfo.getInstance().userUniqueName != null & LoginInfo.getInstance().userUniqueName != "") {
                    Intent intent = new Intent(ActivityMain.this, AvatarActivity.class);
                    intent.putExtra(AvatarParams.userStrID, LoginInfo.getInstance().userUniqueName);

                    ((ImageView) drawerLayout.findViewById(R.id.drawer_home_img)).setImageResource(R.mipmap.ic_home);
                    ((ImageView) drawerLayout.findViewById(R.id.drawer_businesses_img)).setImageResource(R.mipmap.ic_business);
                    ((ImageView) drawerLayout.findViewById(R.id.avater_img)).setImageResource(R.mipmap.avaterselect);
                    findViewById(R.id.drawer_home).setBackgroundResource(R.drawable.background_menu);
                    findViewById(R.id.drawer_businesses).setBackgroundResource(R.drawable.background_menu);
                    findViewById(R.id.avater).setBackgroundColor(getResources().getColor(R.color.material_gray_light));
                    startActivity(intent);
                }
                break;

        }
    }

    public void initialUserBusinessesTab() {

        footerHome = false;
        footerBusiness = true;
        footerSearch = false;
        footerUser = false;


        // change
//        fm.beginTransaction()
//        .hide(fm.findFragmentById(R.uniqueId.frag_search))
//        .hide(fm.findFragmentById(R.uniqueId.frag_home))
//        .hide(fm.findFragmentById(R.uniqueId.frag_user))
//        .show(fm.findFragmentById(R.uniqueId.frag_user_businesses))
//        .commit();
        ft = fm.beginTransaction();
        ft.replace(R.id.fragmentContainer, fragUserBusinesses);
        ft.commit();
        // change
        //ft.replace(R.uniqueId.fragmentContainer, fragmentUser);

        //if user.businesses in intialized

        // change
//        if (((MyApplication) getApplication()).isUserCreated)
//            recursivelyCallHandlerUserBusinessesFragment();
        // change

        btnHome.setVisibility(View.VISIBLE);
        /*nothingChoseInHeader();

        imageViewBusinesses.setImageResource(R.drawable.ic_store_mall_directory_blue_36dp);

        footerHome=false;
        footerBusiness=true;
        footerSearch=false;
        footerUser=false;

        ft = fm.beginTransaction();
        //ft.replace(R.uniqueId.fragmentContainer, fragmentUser);

        //if user.businesses in intialized
        if (((MyApplication) getApplication()).isUserCreated) {
            //ft.show(fm.findFragmentById(R.uniqueId.frag_user));
            ft.hide(fm.findFragmentById(R.uniqueId.frag_search));
            ft.hide(fm.findFragmentById(R.uniqueId.frag_home));
            ft.hide(fm.findFragmentById(R.uniqueId.frag_user));
            ft.show(fm.findFragmentById(R.uniqueId.frag_user_businesses));
            ft.commit();
        } else
            recursivelyCallHandlerUserBusinessesFragment();*/
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);

            return;
        }

        if (PostInitializer.dismissAllPopupsIfAnyShowing())
            return;

        if (PostInitializerBothView.dismissAllPopupsIfAnyShowing()) {
            return;
        }

        if(openSearchFragmentBln)
            finish();


        if (btnHome.getVisibility() == View.VISIBLE)
            toHome(null);
        else
            finish();
//        checkBack();
    }


    private void initialPopupOptionsUser(View view, final PopupWindow popupWindow) {
/*        (view.findViewById(R.uniqueId.imageView_drawer_edit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, ActivityProfileUser.class);
                activity.startActivity(intent);
                popupWindow.dismiss();
            }
        });

        (view.findViewById(R.uniqueId.ll_drawer_businesses)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityMain.this, ActivityBusinessEdit.class);
                startActivityForResult(intent, Params.ACTION_REGISTER_BUSINESS);
                notifyGo();
                popupWindow.dismiss();
            }
        });
        (view.findViewById(R.uniqueId.ll_drawer_setting)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityMain.this, ActivityUserSetting.class);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        (view.findViewById(R.uniqueId.ll_drawer_exit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DialogExit(ActivityMain.this).show();
                popupWindow.dismiss();
            }
        });
        (view.findViewById(R.uniqueId.ll_drawer_contact_us)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.url_contact_us)));
                startActivity(browserIntent);
                popupWindow.dismiss();
            }
        });
        (view.findViewById(R.uniqueId.ll_drawer_guide)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.url_guide)));
                startActivity(browserIntent);
                popupWindow.dismiss();
            }
        });

        (view.findViewById(R.uniqueId.ll_drawer)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });*/
    }

    public void switchPopupWindow(View view) {
        drawerLayout.openDrawer(Gravity.RIGHT);
    }

    private void initDrawerLayout() {

        final Animation anim_0_180 = AnimationUtils.loadAnimation(ActivityMain.this, R.anim.rotate_0_180);
        anim_0_180.setFillAfter(true);
        final Animation anim_180_0 = AnimationUtils.loadAnimation(ActivityMain.this, R.anim.rotate_180_0);
        anim_180_0.setFillAfter(true);

        findViewById(R.id.drawer_arrow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                findViewById(R.id.drawer_arrow).startAnimation(anim_0_180);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                findViewById(R.id.drawer_arrow).startAnimation(anim_180_0);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                hideSoftKeyboard();
            }
        });
    }

    private void initNavigationView() {
        //Initializing NavigationView

        findViewById(R.id.drawer_home).setBackgroundColor(getResources().getColor(R.color.material_gray_light));
        (findViewById(R.id.drawer_profile)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelection(FRAG_USER);
            }
        });
        (findViewById(R.id.drawer_home)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelection(FRAG_TIMELINE);
            }
        });
        (findViewById(R.id.drawer_businesses)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelection(FRAG_BUSINESS);
            }
        });
        (findViewById(R.id.drawer_Invite)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.url_contact_us)));
//                startActivity(browserIntent);
                Intent intent = new Intent(ActivityMain.this, ActivityInvite.class);
                intent.putExtra(Params.FIRST_TAB_TITLE, getString(R.string.txt_FindFriends));
                intent.putExtra(Params.SECOND_TAB_TITLE, getString(R.string.txt_InviteFriends));
                intent.putExtra(Params.THIRD_TAB_TITLE, getString(R.string.txt_sendSMS));
                intent.putExtra(Params.SMS_MESSAGE, "sms");
                intent.putExtra(Params.EMAIL_MESSAGE, "email");
                intent.putExtra(Params.USER_UNIQUE_ID, LoginInfo.getInstance().getUserUniqueId());
                intent.putExtra(Params.IS_BUSINESS_PROMOTION, false);
                startActivity(intent);
                drawerLayout.closeDrawer(Gravity.RIGHT);
            }
        });

        (findViewById(R.id.drawer_feedback)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.url_contact_us)));
//                startActivity(browserIntent);
                startActivity(createEmailIntent(new String[]{getResources().getString(R.string.feedback_EmailReceiver)},
                        getResources().getString(R.string.feedback_EmailSubject),
                        getResources().getString(R.string.feedback_EmailDefaultBody)));
                drawerLayout.closeDrawer(Gravity.RIGHT);
            }
        });
        (findViewById(R.id.drawer_settings)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityMain.this, ActivityUserSetting.class);
                startActivity(intent);
                drawerLayout.closeDrawer(Gravity.RIGHT);
            }
        });
        (findViewById(R.id.drawer_settings)).setVisibility(View.GONE);
        (findViewById(R.id.drawer_help)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openHtmlLink(getResources().getString(R.string.txt_helpPageURL));
                drawerLayout.closeDrawer(Gravity.RIGHT);
            }
        });
        (findViewById(R.id.drawer_exit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DialogExit(ActivityMain.this,ActivityMain.this).show();
                drawerLayout.closeDrawer(Gravity.RIGHT);
            }
        });

        (findViewById(R.id.avater)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setSelection(Avater);
//                showWaitDialog();
            }
        });


    }

    public void setUserDrawer(User user) {
        if (user.profilePicture==null || user.profilePicture==""){
        SimpleLoader simpleLoader = new SimpleLoader(this);
            simpleLoader.loadImage(user.profilePictureUniqueId, Image_M.LARGE, Image_M.ImageType.USER
                    , (ImageViewCircular) findViewById(R.id.drawer_user_pic), w, w, null, user.userIdentifier);
            ((TextViewFont) findViewById(R.id.drawer_user_name)).setText(user.name);
            ((TextViewFont) findViewById(R.id.drawer_user_id)).setText(user.userIdentifier);

        }
        else{
            SimpleLoader simpleLoader = new SimpleLoader(this);
            simpleLoader.loadImage(user.profilePictureUniqueId, Image_M.LARGE, Image_M.ImageType.USER
                    , (ImageViewCircular) findViewById(R.id.drawer_user_pic));
            ((TextViewFont) findViewById(R.id.drawer_user_name)).setText(user.name);
            ((TextViewFont) findViewById(R.id.drawer_user_id)).setText(user.userIdentifier);

        }


    }

    public void toHome(View view) {
        if (footerSearch) {
            if (getFragmentSearch().back())
                return;
        }
        setSelection(FRAG_TIMELINE);
    }

    public void toSearch(View view) {
        setSelection(FRAG_SEARCH);
    }


    private void setUpGeneralFeatures(View view) {
        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard();
                    return false;
                }

            });
        }
        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setUpGeneralFeatures(innerView);
            }
        }
    }

    public void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    public FragmentSearch getFragmentSearch() {
        return fragSearch;//(FragmentSearch) getSupportFragmentManager().findFragmentById(R.uniqueId.frag_search);
    }

    @Override
    public void onClick(View v) {

    }

    public void unlockDrawer() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    public void lockDrawer() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }


    @Override
    public void getPhoneContacts() {
//        GlobalContactPassingClass.getInstance().setEmailContacts(emailContacts);
//        GlobalContactPassingClass.getInstance().setNumberContacts(numberContacts);
    }

    @Override
    public void setAppResults() {
//        GlobalContactPassingClass.getInstance().setAppList(appList);
    }


    @Override
    public void notifyUpdateUserProfile(String userPictureString) {

    }

    @Override
    public void doUpdateUserProfileData() throws Exception {
        new GetUserHomeInfo(ActivityMain.this,LoginInfo.getInstance().getUserUniqueId(),
                LoginInfo.getInstance().getUserUniqueId(), ActivityMain.this, GET_USER_HOME_INFO_INT_CODE,LoginInfo.getInstance().userAccessToken).
                exectueWithNewSolution();
    }


    public Intent createEmailIntent(final String[] toEmail,
                                    final String subject,
                                    final String message) {
        String emailsString = "";
        if (toEmail.length > 0)
            emailsString = toEmail[0];

        for (int i = 1; i < toEmail.length; i++) {
            emailsString += ",";
            emailsString += toEmail[i];
        }
        Intent sendTo = new Intent(Intent.ACTION_SENDTO);
        String uriText = "mailto:" + Uri.encode(emailsString) +
                "?subject=" + Uri.encode(subject) +
                "&body=" + Uri.encode(message);
        Uri uri = Uri.parse(uriText);
        sendTo.setData(uri);

        List<ResolveInfo> resolveInfos = getPackageManager().queryIntentActivities(sendTo, 0);

        // Emulators may not like this check...
        if (!resolveInfos.isEmpty()) {
            return Intent.createChooser(sendTo, "Send Email");
        }

        // Nothing resolves send to, so fallback to send...
        Intent send = new Intent(Intent.ACTION_SEND);

        send.setType("message/rfc822");
        send.putExtra(Intent.EXTRA_EMAIL, toEmail);
        send.putExtra(Intent.EXTRA_SUBJECT, subject);
        send.putExtra(Intent.EXTRA_TEXT, message);

        return Intent.createChooser(send, "Send Email");
    }


    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideWaitDialog();

    }


    public final static void openThisActivityAndGotoFragmentSearch(Context callerActivityContext){
        Intent intent = new Intent(callerActivityContext,ActivityMain.class);
        intent.putExtra("pleaseOpenFragmentSearch", true);
        callerActivityContext.startActivity(intent);
    }




}
