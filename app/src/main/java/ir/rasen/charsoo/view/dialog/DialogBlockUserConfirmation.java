package ir.rasen.charsoo.view.dialog;

import android.view.View;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.object.Comment;
import ir.rasen.charsoo.model.business.BlockUser;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.material_library.widgets.Dialog;


public class DialogBlockUserConfirmation extends Dialog {

    public DialogBlockUserConfirmation
            (final CharsooActivity charsooActivity, final String ownerBusinessId,
             final Comment comment, final IWebservice iWebservice,
             final int webserviceReqCode) {
        super(charsooActivity, charsooActivity.getResources().getString(R.string.block_user),
                charsooActivity.getResources().getString(R.string.popup_block_follower));

        addCancelButton(charsooActivity.getResources().getString(R.string.cancel));
        setAcceptText(R.string.yes);

        setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        //set onClickListener for the ok button
        setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                charsooActivity.showWaitDialog();
                //progressDialog will be closed in getResult or getError in calling class
                new BlockUser(charsooActivity, ownerBusinessId, comment.ownerUniqueId
                        ,iWebservice,webserviceReqCode, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

                dismiss();
            }
        });

    }

}
