package ir.rasen.charsoo.view.dialog;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.model.post.CancelShare;
import ir.rasen.charsoo.view.interface_m.IShareCancelShareListener;
import ir.rasen.charsoo.view.shared.PostInitializer;
import ir.rasen.charsoo.view.shared.PostInitializerBothView;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.material_library.widgets.Dialog;


public class DialogCancelShareConfirmationTimeLine extends Dialog {
    Context context;

    IShareCancelShareListener delegate;

    public DialogCancelShareConfirmationTimeLine(final Context context,
                                                 final Post post, final ImageView imageViewShareIcon, final TextViewFont tvShareNumber,
                                                 final IShareCancelShareListener delegate , final PostInitializer postInitializer) {
        super(context, context.getResources().getString(R.string.cancel_share),context.getResources().getString(R.string.confirmation_cancel_share));

        this.delegate=delegate;

        addCancelButton(R.string.cancel);
        setAcceptText(R.string.yes);

        setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CancelShare(context,LoginInfo.getInstance().getUserUniqueId(), post.uniqueId, null, delegate,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                post.isShared = false;
                post.shareNumber--;
                /*imageViewShareIcon.setImageResource(R.mipmap.ic_share);*/
                postInitializer.initShare(imageViewShareIcon,tvShareNumber);
                dismiss();
            }
        });
    }

    public DialogCancelShareConfirmationTimeLine(final Context context,
                                                 final Post post, final ImageView imageViewShareIcon, final TextViewFont tvShareNumber,
                                                 final IShareCancelShareListener delegate , final PostInitializerBothView postInitializerBothView) {
        super(context, context.getResources().getString(R.string.cancel_share),context.getResources().getString(R.string.confirmation_cancel_share));

        this.delegate=delegate;

        addCancelButton(R.string.cancel);
        setAcceptText(R.string.yes);

        setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CancelShare(context,LoginInfo.getInstance().getUserUniqueId(), post.uniqueId, null, delegate,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
                post.isShared = false;
                post.shareNumber--;
                /*imageViewShareIcon.setImageResource(R.mipmap.ic_share);*/
                postInitializerBothView.initShare(imageViewShareIcon,tvShareNumber);
                dismiss();
            }
        });
    }

}
