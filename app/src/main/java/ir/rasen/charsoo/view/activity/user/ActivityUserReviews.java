package ir.rasen.charsoo.view.activity.user;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.PullToRefreshList;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.Review;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.review.GetUserReviews;
import ir.rasen.charsoo.view.adapter.AdapterUserReview;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IPullToRefresh;
import ir.rasen.charsoo.view.interface_m.IReviewChange;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.widgets.RatingBar;
import ir.rasen.charsoo.view.widgets.TextViewFont;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.PullToRefreshListView;


public class ActivityUserReviews
        extends CharsooActivity
        implements IWebservice,IReviewChange,IPullToRefresh,NetworkStateChangeListener,TryAgainListener {
    public static final String TAG="ActivityUserReviews";
    String visitedUserId;
    AdapterUserReview adapterUserReview;
    ListView listView;
    ArrayList<Review> results;
    private View listFooterView;
    DialogMessage dialogMessage;
    RelativeLayout networkFailedLayout;

    PullToRefreshList pullToRefreshListView;

    public static final int REVIEW_EDIT_REQUEST = 100;
    public static final int REVIEW_DELETE_REQUEST = 200;
    public static final int GET_USER_REVIEWS_REQUSET = 300;

    TextViewFont tvNoExistsReview;
    boolean isOwnerUser;


    public static IReviewChange iReviewChange;

    @Override
    public void notifyDeleteReview(String reviewId) {
        notifyRefresh();
    }

    @Override
    public void notifyDeleteReviewFailed(String reviewIntId, String errorMessage) {
        hideWaitDialog();
        if (!dialogMessage.isShowing())
        {
            dialogMessage.show();
            dialogMessage.setMessage(errorMessage);
        }
    }

    @Override
    public void notifyUpdateReview(Review review) {
        notifyRefresh();
    }

    @Override
    public void notifyUpdateReviewFailed(String reviewIntId, String errorMessage) {
        hideWaitDialog();
        if (!dialogMessage.isShowing())
        {
            dialogMessage.show();
            dialogMessage.setMessage(errorMessage);
        }
    }

    @Override
    public void notifyRefresh() {
        status = Status.REFRESHING;
        new GetUserReviews(ActivityUserReviews.this,visitedUserId, 0, getResources().getInteger(R.integer.lazy_load_limitation),
                ActivityUserReviews.this,GET_USER_REVIEWS_REQUSET,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }

    @Override
    public void notifyLoadMore() {
        loadMoreData();
    }


    private enum Status {FIRST_TIME, LOADING_MORE, REFRESHING, NONE}

    private Status status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.layout_listview);
        setTitle(getString(R.string.reviews));

        iReviewChange = this;

        networkFailedLayout=MyApplication.initNetworkErrorLayout(getWindow().getDecorView(),
                ActivityUserReviews.this, ActivityUserReviews.this);
        networkFailedLayout.setVisibility(View.GONE);

        visitedUserId = getIntent().getExtras().getString(Params.VISITED_USER_UNIQUE_ID);

        results = new ArrayList<>();
        status = Status.FIRST_TIME;

        dialogMessage=new DialogMessage(ActivityUserReviews.this,"");

        listView = (ListView) findViewById(R.id.listView);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            int currentFirstVisibleItem,
                    currentVisibleItemCount,
                    currentScrollState;

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
            }

            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            private void isScrollCompleted() {
                if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
                    if (status != Status.LOADING_MORE
                            && results.size() > 0 && results.size() % getResources().getInteger(R.integer.lazy_load_limitation) == 0) {
                        loadMoreData();
                    }
                }
            }
        });

        pullToRefreshListView = new PullToRefreshList(this, (PullToRefreshListView) findViewById(R.id.pull_refresh_list), ActivityUserReviews.this);
        listView = pullToRefreshListView.getListView();

        listFooterView = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_loading_more, null, false);
        listFooterView.setVisibility(View.GONE);
        listView.addFooterView(listFooterView, null, false);
        adapterUserReview = new AdapterUserReview(ActivityUserReviews.this,visitedUserId,
                results,ActivityUserReviews.this,ActivityUserReviews.this);
        listView.setAdapter(adapterUserReview);

        showWaitDialog();
        new GetUserReviews(ActivityUserReviews.this,visitedUserId, 0, getResources().getInteger(R.integer.lazy_load_limitation),
                ActivityUserReviews.this,GET_USER_REVIEWS_REQUSET,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();


        tvNoExistsReview = (TextViewFont) findViewById(R.id.tv_no_data_exist);

        isOwnerUser = (visitedUserId.equals(LoginInfo.getInstance().getUserUniqueId()))?true:false;
    }


    // LOAD MORE DATA
    public void loadMoreData() {
        status = Status.LOADING_MORE;
        listFooterView.setVisibility(View.VISIBLE);

        new GetUserReviews(ActivityUserReviews.this,visitedUserId ,adapterUserReview.getCount(), getResources().getInteger(R.integer.lazy_load_limitation), ActivityUserReviews.this,GET_USER_REVIEWS_REQUSET,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

    @Override
    public void getResult(int reqCode,Object result) {
        hideWaitDialog();
        networkFailedLayout.setVisibility(View.GONE);
        switch (reqCode){
            case GET_USER_REVIEWS_REQUSET :
                if (result instanceof ArrayList) {
                    ArrayList<Review> temp = (ArrayList<Review>) result;
//                    results.addAll(temp);

                    if (status == Status.FIRST_TIME) {
//                        adapterUserReview = new AdapterUserReview(ActivityUserReviews.this,visitedUserId,
//                                results,ActivityUserReviews.this,ActivityUserReviews.this);
//                        listView.setAdapter(adapterUserReview);
                        results=new ArrayList<>(temp);
                        adapterUserReview.resetItems(results);
                    }else if (status == Status.REFRESHING) {
                        results=new ArrayList<>(temp);
                        adapterUserReview.resetItems(results);
                        results.clear();
                        results.addAll(temp);
                        adapterUserReview = new AdapterUserReview(ActivityUserReviews.this,visitedUserId,
                                results,ActivityUserReviews.this,ActivityUserReviews.this);
                        listView.setAdapter(adapterUserReview);
                        pullToRefreshListView.onRefreshComplete();
                    }
                    else {
                        //it is loading more
                        pullToRefreshListView.setFooterVisibility(View.GONE);
                        listFooterView.setVisibility(View.GONE);
                        adapterUserReview.loadMore(temp);
                    }
                    status = Status.NONE;
                    pullToRefreshListView.setResultSize(results.size());
                }
                break;

            case REVIEW_DELETE_REQUEST :
                break;

            case REVIEW_EDIT_REQUEST :
                break;
        }

        handleTvNoReviewExist();
    }

    @Override
    public void getError(int reqCode,Integer errorCode,String callerStringID) {
        hideWaitDialog();
        switch (reqCode){
            case GET_USER_REVIEWS_REQUSET :
                pullToRefreshListView.onRefreshComplete();
                if (errorCode==ServerAnswer.NETWORK_CONNECTION_ERROR) {
                    NetworkConnectivityReciever.setNetworkStateListener(TAG, ActivityUserReviews.this);
                }
                if(ServerAnswer.canShowErrorLayout(errorCode)){
                    networkFailedLayout.setVisibility(View.VISIBLE);
                }else if (!dialogMessage.isShowing() && networkFailedLayout.getVisibility()!=View.VISIBLE)
                    new DialogMessage(
                            ActivityUserReviews.this, ServerAnswer.getError(ActivityUserReviews.this,
                            errorCode, callerStringID + ">" + this.getLocalClassName())).show();
                break;

            case REVIEW_DELETE_REQUEST :
                break;

            case REVIEW_EDIT_REQUEST :
                break;
        }

        handleTvNoReviewExist();
    }
    @Override
    public void doOnNetworkConnected() {
        showWaitDialog();
        status = Status.REFRESHING;
        new GetUserReviews(ActivityUserReviews.this,visitedUserId, 0, getResources().getInteger(R.integer.lazy_load_limitation),
                ActivityUserReviews.this,GET_USER_REVIEWS_REQUSET,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }

    @Override
    public void doTryAgain() {
        status = Status.REFRESHING;
        showWaitDialog();
        new GetUserReviews(ActivityUserReviews.this,visitedUserId, 0, getResources().getInteger(R.integer.lazy_load_limitation),
                ActivityUserReviews.this,GET_USER_REVIEWS_REQUSET,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }


    void handleTvNoReviewExist(){
        if(results.size() <= 0) {
            tvNoExistsReview.setVisibility(View.VISIBLE);
            if (isOwnerUser)
                tvNoExistsReview.setText(getString(R.string.txt_not_exists_review_for_user));
            else
                tvNoExistsReview.setText(getString(R.string.txt_not_exists_review_for_user_other));
        }
        else
            tvNoExistsReview.setVisibility(View.GONE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

}
