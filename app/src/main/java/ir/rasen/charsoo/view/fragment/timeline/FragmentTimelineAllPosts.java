package ir.rasen.charsoo.view.fragment.timeline;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.nispok.snackbar.SnackbarManager;

import java.util.ArrayList;
import java.util.Hashtable;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.PullToRefreshList;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.model.NetworkConnectivityReciever;
import ir.rasen.charsoo.model.post.GetPost;
import ir.rasen.charsoo.model.post.GetTimeLineAllBusinessPosts;
import ir.rasen.charsoo.view.activity.ActivityMain;
import ir.rasen.charsoo.view.adapter.AdapterPost;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IAddPost;
import ir.rasen.charsoo.view.interface_m.IGetNewTimeLinePost;
import ir.rasen.charsoo.view.interface_m.IPostUpdateListener;
import ir.rasen.charsoo.view.interface_m.IPullToRefresh;
import ir.rasen.charsoo.view.interface_m.ITimeLineReload;
import ir.rasen.charsoo.view.interface_m.IUpdateUserProfile;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.interface_m.NetworkStateChangeListener;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.pull_to_refresh.PullToRefreshListView;

/**
 * Created by Rasen_iman on 8/12/2015.
 */
public class FragmentTimelineAllPosts extends Fragment implements
        IWebservice, IPullToRefresh, IGetNewTimeLinePost,
        NetworkStateChangeListener,IPostUpdateListener,IAddPost,
        ITimeLineReload,TryAgainListener {


    public static final String TAG = "FragmentTimelineAllPosts";

    public static final int GET_ALL_BUSINESSES_POSTS_REQUEST =11,GET_SINGLE_POST_INT_CODE=14;

    AdapterPost adapterPost;
    ListView listView;
    ArrayList<Post> results;
    //    ArrayList<Post> sampleResults;
    DialogMessage dialogMessage;
    Context fragmentContext;
    public static IPostUpdateListener iPostUpdateListener;
    public static IAddPost iAddPost;

    boolean timelineCatchedEnd;


    RelativeLayout layoutNotExistsTimeline;
    public static ITimeLineReload iTimeLineReload;


    RelativeLayout networkFailLayout;

    CharsooActivity charsooActivityContext;


    private enum Status {FIRST_TIME, LOADING_MORE, REFRESHING, NONE}

    private Status status;
    BroadcastReceiver timeLineUpdateReceiver;
    PullToRefreshList pullToRefreshListView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        CustomActivityOnCrash.install(getActivity());

        View view = inflater.inflate(R.layout.fragment_time_line_all_posts, null);


        timelineCatchedEnd=false;

        layoutNotExistsTimeline = initNotExistsLayout(view);


        fragmentContext=getActivity();
        iPostUpdateListener=FragmentTimelineAllPosts.this;

        charsooActivityContext = (CharsooActivity) getActivity();


        dialogMessage=new DialogMessage(getActivity(),"");
        if (results==null){
            results = new ArrayList<>();
            charsooActivityContext.showWaitDialog();

            new GetTimeLineAllBusinessPosts(getActivity(),LoginInfo.getInstance().getUserUniqueId(),0,
                    getResources().getInteger(R.integer.lazy_load_limitation),
                    FragmentTimelineAllPosts.this, GET_ALL_BUSINESSES_POSTS_REQUEST,LoginInfo.getInstance().userAccessToken).
                    exectueWithNewSolution();

        }else if(results.size()==0){
            handleShowNotExistsLayout();
        }else if(results.size()>0){
            handleShowNotExistsLayout();
        }

        status = Status.FIRST_TIME;



        pullToRefreshListView = new PullToRefreshList(getActivity(),
                (PullToRefreshListView) view.findViewById(R.id.pull_refresh_list),
                FragmentTimelineAllPosts.this);

        listView = pullToRefreshListView.getListView();
        if (adapterPost==null)
            adapterPost =
                    new AdapterPost(FragmentTimelineAllPosts.this, getActivity(),
                            results,FragmentTimelineAllPosts.TAG);
        listView.setAdapter(adapterPost);
        status = Status.FIRST_TIME;


        timeLineUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle bundle = intent.getExtras();
                switch (bundle.getString(Params.UPDATE_TIME_LINE_TYPE)) {
                    case Params.UPATE_TIME_LINE_TYPE_SHARE:
                        updateShare(true, bundle.getString(Params.POST_ID_INT));
                        break;
                    case Params.UPATE_TIME_LINE_TYPE_CANCEL_SHARE:
                        updateShare(false, bundle.getString(Params.POST_ID_INT));
                        break;
                }

            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(timeLineUpdateReceiver,
                new IntentFilter(Params.UPATE_TIME_LINE));

        iAddPost = FragmentTimelineAllPosts.this;

        iTimeLineReload = FragmentTimelineAllPosts.this;


        networkFailLayout = MyApplication.initNetworkErrorLayout(view, getActivity(),
                FragmentTimelineAllPosts.this);

        return view;
    }

    public static FragmentTimelineAllPosts newInstance() {
        return new FragmentTimelineAllPosts();
    }


    public void updatePost(Post post) {
        for (int i = 0; i < results.size(); i++) {
            if (results.get(i).uniqueId == post.uniqueId) {
                results.set(i, post);
                break;
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(timeLineUpdateReceiver);
    }

    private void initialize(ArrayList<Post> results) {
        adapterPost =
                new AdapterPost(FragmentTimelineAllPosts.this, getActivity(),
                        results,FragmentTimelineAllPosts.TAG);
        listView.setAdapter(adapterPost);
    }

    // LOAD MORE DATA
    public void loadMoreData() {
        // LOAD MORE DATA HERE...
        if (status == Status.NONE && !timelineCatchedEnd) {
            status = Status.LOADING_MORE;
            pullToRefreshListView.setFooterVisibility(View.VISIBLE);
            Hashtable<String, String> lastLoadedItems = Post.getLastLoadedTimelineItems(results);

            new GetTimeLineAllBusinessPosts(getActivity(),  LoginInfo.getInstance().getUserUniqueId(),
                    adapterPost.getCount(),getResources().getInteger(R.integer.lazy_load_limitation),
                    FragmentTimelineAllPosts.this, GET_ALL_BUSINESSES_POSTS_REQUEST,LoginInfo.getInstance().userAccessToken).
                    exectueWithNewSolution();
        }
        else {
            pullToRefreshListView.onLoadmoreComplete();
        }

    }

    public void updateShare(boolean isShared, String postId) {
        for (int i = 0; i < results.size(); i++) {
            if (results.get(i).uniqueId.equals(postId)) {
                results.get(i).isShared = isShared;
                adapterPost.notifyDataSetChanged();
                break;
            }
        }
    }


    @Override
    public void notifyAddPost(Post post) {
        charsooActivityContext.showWaitDialog();

        status = Status.REFRESHING;
        new GetTimeLineAllBusinessPosts(
                fragmentContext,LoginInfo.getInstance().getUserUniqueId(), 0,fragmentContext.getResources().getInteger(R.integer.lazy_load_limitation),
                FragmentTimelineAllPosts.this, GET_ALL_BUSINESSES_POSTS_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
    }

    @Override
    public void notifyGetNewPost(Post post) throws Exception {
        adapterPost.updatePost(post);
    }

    @Override
    public void onPostDetailsChange(String dirtyPostId) throws Exception {
        for (int i = 0; i < results.size(); i++) {
            if (results.get(i).uniqueId.equals(dirtyPostId)){
                new GetPost(fragmentContext,LoginInfo.getInstance().getUserUniqueId(),
                        results.get(i).businessUniqueId,dirtyPostId, Post.GetPostType.TIMELINE,
                        FragmentTimelineAllPosts.this,GET_SINGLE_POST_INT_CODE,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
                break;
            }
        }
    }

    @Override
    public void notifyRefresh() {
        status = Status.REFRESHING;
        timelineCatchedEnd=false;
        new GetTimeLineAllBusinessPosts(
                fragmentContext,LoginInfo.getInstance().getUserUniqueId(), 0,fragmentContext.getResources().getInteger(R.integer.lazy_load_limitation),
                FragmentTimelineAllPosts.this, GET_ALL_BUSINESSES_POSTS_REQUEST,LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();
    }

    @Override
    public void notifyLoadMore() {
        loadMoreData();

    }

    @Override
    public void onTimeLineReload() {
        notifyRefresh();
    }

    @Override
    public void getResult(int request, Object result) throws Exception {

        networkFailLayout.setVisibility(View.GONE);
        charsooActivityContext.hideWaitDialog();


        NetworkConnectivityReciever.removeIfHaveListener(TAG);
        switch (request){
            case GET_ALL_BUSINESSES_POSTS_REQUEST:
                ArrayList<Post> temp = (ArrayList<Post>) result;
                results.addAll(temp);

                pullToRefreshListView.setResultSize(results.size());

                if (status == Status.FIRST_TIME) {
                    ((MyApplication) getActivity().getApplication()).homePosts = new ArrayList<>(results);
                    initialize(results);
                } else if (status == Status.REFRESHING) {
                    results.clear();
                    adapterPost.resetItems(new ArrayList<Post>());
                    results.addAll(temp);
                    adapterPost.resetItems(results);
//                adapterPostTimeLine.notifyDataSetChanged();
                    pullToRefreshListView.onRefreshComplete();
                    ((MyApplication) getActivity().getApplication()).homePosts = new ArrayList<>(results);
                } else if (status == Status.LOADING_MORE) {
                    adapterPost.loadMore(temp);
                    ((MyApplication) getActivity().getApplication()).homePosts = new ArrayList<>(results);
                    pullToRefreshListView.onLoadmoreComplete();
//                pullToRefreshListView.setFooterVisibility(View.GONE);
                }
                status = Status.NONE;
                break;
            case GET_SINGLE_POST_INT_CODE:
                Post newPost=(Post)result;

                for (int i = 0; i < results.size(); i++) {
                    if (results.get(i).uniqueId.equals(newPost.uniqueId)){
                        results.set(i,MyApplication.mergeTwoPost(results.get(i),newPost));
                        adapterPost.resetItems(results);
                        break;
                    }
                }
                break;
        }

        handleShowNotExistsLayout();

    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) throws Exception {


        try {
            pullToRefreshListView.onRefreshComplete();
            charsooActivityContext.hideWaitDialog();


            if(errorCode== ServerAnswer.NETWORK_CONNECTION_ERROR)
                NetworkConnectivityReciever.setNetworkStateListener(TAG,FragmentTimelineAllPosts.this);


            if (errorCode==ServerAnswer.POST_DOES_NOT_EXISTS){
                timelineCatchedEnd=true;
                if (status==Status.LOADING_MORE)
                {
                    status=Status.NONE;
                    pullToRefreshListView.onLoadmoreComplete();
                }
                setShowingNotExistsLayout(true);
            }
            else if (ServerAnswer.canShowErrorLayout(errorCode) && status!=Status.REFRESHING){
                networkFailLayout.setVisibility(View.VISIBLE);
            }else{
                dialogMessage.show();
                dialogMessage.setMessage(
                        ServerAnswer.getError(getActivity(),
                                errorCode, callerStringID + ">" + TAG));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void doOnNetworkConnected() throws Exception {
    }

    @Override
    public void doTryAgain() {

//        Toast.makeText(getActivity(),"doTryAgain",Toast.LENGTH_SHORT).show();
        SnackbarManager.show(
                com.nispok.snackbar.Snackbar.with(fragmentContext)
                        .text(R.string.Conecction_Error)
                        .color(Color.parseColor("#3f51b5"))
                        .animation(true));
    }

    RelativeLayout initNotExistsLayout(View parentView){
        RelativeLayout notExistsLay = (RelativeLayout) parentView.findViewById(R.id.notExistsTimeLine);

        notExistsLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {}});

        notExistsLay.findViewById(R.id.notExistsClickableArea).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ActivityMain)getActivity()).toSearch(null);
            }
        });

        return notExistsLay;
    }

    void handleShowNotExistsLayout(){
        int count = adapterPost.getCount();
        if(count<=0)
            layoutNotExistsTimeline.setVisibility(View.VISIBLE);
        else
            layoutNotExistsTimeline.setVisibility(View.GONE);
    }


    void setShowingNotExistsLayout(boolean show){
        if(show)
            layoutNotExistsTimeline.setVisibility(View.VISIBLE);
        else
            layoutNotExistsTimeline.setVisibility(View.GONE);
    }

}



