package ir.rasen.charsoo.view.fragment.business_register;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.Hashtable;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.ImageHelper;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.Validation;
import ir.rasen.charsoo.model.business.GetBusinessStrIdAvailability;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessRegister;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.fragment.invite.FragmentInvite;
import ir.rasen.charsoo.view.fragment.invite.FragmentInviteAppList;
import ir.rasen.charsoo.view.interface_m.IGetCallForTakePicture;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.edittexts.EditTextFont;
import ir.rasen.charsoo.view.widgets.WaitDialog;

/**
 * Created by hossein-pc on 6/14/2015.
 */
public class FragmentBusinessRegisterPageOne extends Fragment {
    public static final String TAG="FragmentBusinessRegisterPageOne";

    private EditTextFont editTextName, editTextIdentifier;


    private ImageView imageViewPicture;
    private IGetCallForTakePicture iGetCallForTakePicture;
    Bitmap selectedProfilePicture;
    public boolean isBackedForId=false;


    public void setPicture(Bitmap bitmap) {
        selectedProfilePicture=bitmap.copy(Bitmap.Config.ARGB_8888,false);
        imageViewPicture.setImageBitmap(ImageHelper.getRoundedCornerBitmap(selectedProfilePicture, 20));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            iGetCallForTakePicture = (IGetCallForTakePicture) activity;
        } catch (ClassCastException castException) {
            /** The activity does not implement the listener. */
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CustomActivityOnCrash.install(getActivity());

        View view = inflater.inflate(R.layout.fragment_register_business_page_one,
                container, false);


        editTextIdentifier = (EditTextFont) view.findViewById(R.id.editText_identifier);
        if (isBackedForId){
            setErrorStringIdIsNotAvailable();
            isBackedForId=false;
        }
        editTextName = (EditTextFont) view.findViewById(R.id.editText_Name);



        /*textViewCategories.setTextSize(editTextIdentifier.getTextSize()-10);
        textViewSubcategories.setTextSize(editTextIdentifier.getTextSize()-10);
*/
        imageViewPicture = (ImageView) view.findViewById(R.id.imageView_picture);
        if (selectedProfilePicture!=null)
            imageViewPicture.setImageBitmap(ImageHelper.getRoundedCornerBitmap(selectedProfilePicture, 20));
        imageViewPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iGetCallForTakePicture.notifyCallForTakePicture();
            }
        });


        return view;
    }


//    @Override
//    public void getResult(int reqCode,Object result) {
//        progressDialog.dismiss();
//
//    }
//
//
//    @Override
//    public void getError(int reqCode,Integer errorCode,String callerStringID) {
//        progressDialog.dismiss();
//        new DialogMessage(getActivity(), ServerAnswer.getError(getActivity(), errorCode, callerStringID + ">" + TAG)).show();
//    }

    public boolean checkInputData() {

        if (!Validation.validateName(getActivity(), editTextName.getText().toString()).isValid()) {
            editTextName.setError(Validation.getErrorMessage());
            return false;
        }
        if (!Validation.validateBusinessIdentifier(getActivity(), editTextIdentifier.getText().toString()).isValid()) {
            editTextIdentifier.setError(Validation.getErrorMessage());
            return false;
        }
        new GetBusinessStrIdAvailability(getActivity(),editTextIdentifier.getText().toString(),
                (ActivityBusinessRegister)getActivity()
                ,((ActivityBusinessRegister)getActivity()).CHECK_ID_AVAILABILITY_INT_CODE, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();

        FragmentInvite.Business_name=editTextName.getText().toString();
        FragmentInvite.Business_id=editTextIdentifier.getText().toString();
        return true;
    }



    public Hashtable<String,String> getInputData(){
        if (checkInputData()) {
            Hashtable<String,String> result= new Hashtable<>();
            result.put(Params.FULL_NAME, editTextName.getText().toString());
            result.put(Params.STRING_IDENTIFIER,editTextIdentifier.getText().toString());
            return result;
        }
        else
            return null;
    }


    public void setErrorStringIdIsNotAvailable(){
        editTextIdentifier.setError(getActivity().getString(R.string.txt_StringIdentifierIsNotAvailable));
    }
}
