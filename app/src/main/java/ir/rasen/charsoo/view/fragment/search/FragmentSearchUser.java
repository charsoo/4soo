package ir.rasen.charsoo.view.fragment.search;

/**
 * Created by Sina KH on 6/23/2015.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import java.util.ArrayList;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.BaseAdapterItem;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.model.search.SearchUser;
import ir.rasen.charsoo.view.activity.ActivityMain;
import ir.rasen.charsoo.view.adapter.AdapterUsers;
import ir.rasen.charsoo.view.adapter.AdapterUsersFromBAItems;
import ir.rasen.charsoo.view.interface_m.IWebservice;

public class FragmentSearchUser extends Fragment implements IWebservice {
    public static final String TAG = "FragmentSearchUser";

    private ListView listView;
    private AdapterUsersFromBAItems adapter;

    private ArrayList<BaseAdapterItem> searchResults;
    private boolean showingResults = false;
    private String currentResultIsFor = "";

    public static final int SEARCH_USER_REQUEST = 100;

    public static FragmentSearchUser newInstance() {
        FragmentSearchUser fragment = new FragmentSearchUser();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(getActivity());

        //page = getArguments().getInt("someInt", 0);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        CustomActivityOnCrash.install(getActivity());
        CustomActivityOnCrash.setShowErrorDetails(false);
        View view = inflater.inflate(R.layout.fragment_search_user_specials,
                container, false);


        searchResults = new ArrayList<>();

        init(view);
        return view;
    }

    private void init(View view) {
        listView = (ListView) view.findViewById(R.id.listView);
        adapter = new AdapterUsersFromBAItems(getActivity(), "0", searchResults, AdapterUsersFromBAItems.Mode.USERS);
        listView.setAdapter(adapter);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            int currentFirstVisibleItem,
                    currentVisibleItemCount,
                    currentScrollState;

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
            }

            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            private void isScrollCompleted() {
                if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
                    if (!isSearching() && searchResults.size() > 0 && searchResults.size() % getResources().getInteger(R.integer.lazy_load_limitation) == 0) {
                        loadMoreData();
                    }
                }
            }
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                ((ActivityMain) getActivity()).getFragmentSearch().onScroll(firstVisibleItem);
            }
        });
    }

    // LOAD MORE DATA
    public void loadMoreData() {
        // LOAD MORE DATA HERE...
        ((ActivityMain) getActivity()).getFragmentSearch().searchStarted();
        new SearchUser(
                getActivity(),currentResultIsFor,
                adapter.getCount(),
                getResources().getInteger(R.integer.lazy_load_limitation),
                FragmentSearchUser.this,SEARCH_USER_REQUEST, LoginInfo.getInstance().userAccessToken).exectueWithNewSolution();;
    }

    @Override
    public void getResult(int reqCode,Object result) {
        if (reqCode==SEARCH_USER_REQUEST) {
            if(!showingResults) {
                listView.setAdapter(adapter);
                showingResults=true;
            }
            ((ActivityMain) getActivity()).getFragmentSearch().searchFinished();
            ArrayList<BaseAdapterItem> temp = (ArrayList<BaseAdapterItem>) result;
            searchResults=temp;
            adapter.resetItems(searchResults);
        }
    }

    @Override
    public void getError(int reqCode,Integer errorCode, String callerStringID) {
        switch(reqCode){
            case SEARCH_USER_REQUEST :
                if(errorCode == ServerAnswer.USER_DOES_NOT_EXISTS)
                    adapter.resetItems(new ArrayList<BaseAdapterItem>());
                break;
        }

    }

    public void reset() {
        searchResults.clear();
    }

    public void setCurrentResultIsFor(String currentResultIsFor) {
        this.currentResultIsFor=currentResultIsFor;
    }
    public String getCurrentResultIsFor() {
        return currentResultIsFor;
    }

    public boolean isSearching() {
        return ((ActivityMain) getActivity()).getFragmentSearch().isSearching();
    }
}
