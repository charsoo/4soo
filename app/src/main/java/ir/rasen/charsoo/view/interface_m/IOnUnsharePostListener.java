package ir.rasen.charsoo.view.interface_m;

/**
 * Created by Rasen_iman on 8/17/2015.
 */
public interface IOnUnsharePostListener {
    void onUnsharePost(String targetPostId);
}
