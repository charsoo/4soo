package ir.rasen.charsoo.view.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ResultStatus;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.SharedPrefHandler;
import ir.rasen.charsoo.controller.helper.Validation;
import ir.rasen.charsoo.controller.helper.WebservicesHandler;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.ServerAppVersion;
import ir.rasen.charsoo.model.app.GetVersion;
import ir.rasen.charsoo.model.user.Login;
import ir.rasen.charsoo.view.activity.user.ActivityUserRegister;
import ir.rasen.charsoo.view.dialog.DialogMessage;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;
import ir.rasen.charsoo.view.widgets.edittexts.EditTextFont;

public class ActivityLogin
        extends CharsooActivity
        implements View.OnClickListener,IWebservice {

    EditTextFont editTextEmail, editTextPassword;
    MyApplication myApplication;
    DialogMessage dialogMessage;

    public static final int LOGIN_REQUST = 100;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomActivityOnCrash.install(this);

        setContentView(R.layout.activity_login);

        setTitle(getString(R.string.login));
        removeBackOnActionbar();

        myApplication = (MyApplication) getApplication();
        myApplication.setCurrentWebservice(WebservicesHandler.Webservices.NONE);

        dialogMessage=new DialogMessage(this,"");
        //edit texts
        editTextEmail = (EditTextFont) findViewById(R.id.edt_login_email);
        editTextPassword = (EditTextFont) findViewById(R.id.edt_login_password);

        editTextPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    doIfLogin();
                }
                return false;
            }
        });

        //views which do an action
        findViewById(R.id.btn_login_login).setOnClickListener(this);
        findViewById(R.id.btn_login_login).setBackgroundColor(getResources().getColor(R.color.primaryColor));
        findViewById(R.id.btn_login_forget).setOnClickListener(this);

        continueAfterSuccessingVersionCode();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       /* MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_next_button, menu);*/
        return true;
    }

    private void doIfLogin(){
        if (!Validation.validateEmail(ActivityLogin.this, editTextEmail.getText().toString()).isValid()) {
            editTextEmail.setError(Validation.getErrorMessage());
            return;
        }

        if (!Validation.validatePassword(ActivityLogin.this, editTextPassword.getText().toString()).isValid()) {
            editTextPassword.setError(Validation.getErrorMessage());
            return;
        }

        showWaitDialog();
        new Login(ActivityLogin.this, editTextEmail.getText().toString(),
                editTextPassword.getText().toString(), ActivityLogin.this,LOGIN_REQUST).
                exectueWithNewSolution();

        myApplication.setCurrentWebservice(WebservicesHandler.Webservices.LOGIN);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login_login:
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doIfLogin();
                    }
                }, Params.FREEZ);
                break;
            case R.id.btn_login_forget:
                Intent intentForgot = new Intent(ActivityLogin.this, ActivityForgotPassword.class);
                startActivity(intentForgot);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

       /* if (item.getItemId() == R.uniqueId.action_charsoo) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
            startActivity(browserIntent);
        }*/
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void getResult(int reqCode,Object result) {
        hideWaitDialog();
        switch (reqCode){
            case LOGIN_REQUST :
                if (!(result instanceof ResultStatus))
                    return;
                Intent intent = new Intent(ActivityLogin.this, ActivityMain.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                myApplication.setCurrentWebservice(WebservicesHandler.Webservices.NONE);
                finish(false);

                break;

//            case GET_VERSION_REQUEST :
//                ServerAppVersion serverAppVersion = (ServerAppVersion) result;
//                int currentAppVersionCode = myApplication.getApplicationVersionCode();
//
//                if(currentAppVersionCode<0)
//                    showNonDefineErrorForGettingVersionCode();
//                else if(currentAppVersionCode<serverAppVersion.minVersion)
//                    showForceUpdateDialog();
//                else if(currentAppVersionCode<serverAppVersion.maxVersion)
//                    showNotifyUpdateDialog();
//                else
//                    continueAfterSuccessingVersionCode();
//
//                break;
        }
    }

    @Override
    public void getError(int reqCode,Integer errorCode,String callerStringID) {
        hideWaitDialog();
        switch (reqCode){
            case LOGIN_REQUST :
                if (!dialogMessage.isShowing()) {
                    dialogMessage.show();
                    dialogMessage.setMessage(ServerAnswer.getError(ActivityLogin.this, errorCode, callerStringID + ">" + this.getLocalClassName()));
                }

                break;

//            case GET_VERSION_REQUEST :
//                showTryAgainDialog(GET_VERSION_REQUEST);
//                break;
        }
    }
//
//    private void showNonDefineErrorForGettingVersionCode() {
//        dialogMessage = new DialogMessage(ActivityLogin.this,"Non Defined Error");
//        dialogMessage.setAcceptText("exit");
//        dialogMessage.setOnAcceptButtonClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//
//        dialogMessage.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialog) {
//                finish();
//            }
//        });
//        dialogMessage.show();
//    }
//
//    private void showForceUpdateDialog() {
//        dialogMessage =
//                new DialogMessage(ActivityLogin.this,
//                        getString(R.string.force_update_dlg_message));
//
//        dialogMessage.setAcceptText(getString(R.string.force_update_dlg_accept_btn_text));
//        dialogMessage.setOnAcceptButtonClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openHtmlLink("http://www.icharsoo.com");
//                finish();
//            }
//        });
//
//        dialogMessage.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialog) {
//                finish();
//            }
//        });
//        dialogMessage.show();
//    }

//    boolean isContinuAfterUpdateDialog = true;
//    private void showNotifyUpdateDialog() {
//        isContinuAfterUpdateDialog = true;
//        dialogMessage =
//                new DialogMessage(ActivityLogin.this,
//                        getString(R.string.notify_update_dlg_message));
//
//        dialogMessage.setAcceptText(getString(R.string.notify_update_dlg_accept_btn_text));
//        dialogMessage.addCancelButton(getString(R.string.notify_update_dlg_Cancel_btn_text), new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //TODO : CancleBtn
//                dialogMessage.dismiss();
//            }
//        });
//        dialogMessage.setOnAcceptButtonClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                isContinuAfterUpdateDialog = false;
//                openHtmlLink("http://www.icharsoo.com");
//                finish();
//            }
//        });
//        dialogMessage.show();
//        dialogMessage.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialog) {
//                if (isContinuAfterUpdateDialog)
//                    continueAfterSuccessingVersionCode();
//            }
//        });
//    }

    void continueAfterSuccessingVersionCode(){
        hideWaitDialog();
        if (SharedPrefHandler.isSavedUserEmailPassword(this)) {
            editTextEmail.setText(SharedPrefHandler.getUserSavedEmail(ActivityLogin.this));
            editTextPassword.setText(SharedPrefHandler.getUserSavedPassword(ActivityLogin.this));
            doIfLogin();
        }
    }
//
//    private void showTryAgainDialog(int reqCode) {
//        switch(reqCode){
//            case GET_VERSION_REQUEST :
//                dialogMessage.setAcceptText(getString(R.string.try_again_dlg_accept_btn_text));
//                dialogMessage.addCancelButton(getString(R.string.try_again_dlg_Cancel_btn_text), new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        finish();
//                    }
//                });
//                dialogMessage.setOnAcceptButtonClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialogMessage.dismiss();
//                        TryAgainGetVersionCode();
//                    }
//                });
//                dialogMessage.show();
//                dialogMessage.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                    @Override
//                    public void onDismiss(DialogInterface dialog) {
//                        dialogMessage.dismiss();
//                        TryAgainGetVersionCode();
//                    }
//                });
//
//                dialogMessage.setMessage(getString(R.string.try_again_dlg_message));
//
//                break;
//        }
//    }

//    private void TryAgainGetVersionCode() {
//        showWaitDialog();
//        new GetVersion(ActivityLogin.this,ActivityLogin.this,GET_VERSION_REQUEST).executeWithNewSolution();
//    }

    @Override
    public void onBackPressed() {
        Intent i= new Intent(this, ActivitySplash.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.startActivity(i);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "2TRS3S7HJMTDDQSSYB5J");
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
