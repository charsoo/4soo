package ir.rasen.charsoo.view.interface_m;

/**
 * Created by android on 9/3/2015.
 */
public interface ILogout {
    void successlogout() throws  Exception;
    void faillogout(int errorCode,String error) throws Exception;
}
