package ir.rasen.charsoo.controller.helper;

import android.database.sqlite.SQLiteBindOrColumnIndexOutOfRangeException;
import android.location.Location;

import java.util.concurrent.BrokenBarrierException;

public class Location_M {
    private String latitude;
    private String longitude;

    public Location_M() {

    }

    public Location_M(Location location) {
        latitude = "" + location.getLatitude();
        longitude = "" + location.getLongitude();
    }

    public Location_M(String latitude, String longitude) {
        if (latitude!=null || longitude!=null){
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }

    public String getLatitude() {
        if (latitude == null)
            return "";
        else
            return latitude;
    }

    public String getLongitude() {
        if (longitude == null)
            return "";
        else
            return longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public static boolean hasLocation(Location_M lm) {
        if (lm == null) {
            return false;
        } else
            return !lm.getLatitude().equals("")
                    && !lm.getLatitude().startsWith("0")
                    && !lm.getLongitude().equals("")
                    && !lm.getLongitude().startsWith("0");
    }

    public static boolean areLocationsDifferent(Location_M oldLocation, Location_M newLocation) {
        boolean areDifferent=false;
        if (hasLocation(oldLocation)){
            if (hasLocation(newLocation)){
                if (!oldLocation.getLatitude().equals(newLocation.getLatitude()))
                    areDifferent=true;
                if (!oldLocation.getLongitude().equals(newLocation.getLongitude()))
                    areDifferent=true;
            }
        }
        else{
            if (hasLocation(newLocation))
                areDifferent=true;
        }
        return areDifferent;
    }
}
