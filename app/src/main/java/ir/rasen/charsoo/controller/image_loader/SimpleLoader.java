/*
 ===========================================================================
 Copyright (c) 2012 Three Pillar Global Inc. http://threepillarglobal.com
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sub-license, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ===========================================================================
 */
/*J was here*/

package ir.rasen.charsoo.controller.image_loader;

import android.content.Context;
import android.content.Loader;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.ImageView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.image_loader.core.DisplayImageOptions;
import ir.rasen.charsoo.controller.image_loader.core.ImageLoader;
import ir.rasen.charsoo.controller.image_loader.core.ImageLoaderConfiguration;
import ir.rasen.charsoo.controller.image_loader.core.assist.FailReason;
import ir.rasen.charsoo.controller.image_loader.core.listener.ImageLoadingListener;
import ir.rasen.charsoo.controller.image_loader.core.listener.ImageLoadingProgressListener;
import ir.rasen.charsoo.controller.image_loader.core.listener.SimpleImageLoadingListener;
import ir.rasen.charsoo.view.widgets.imageviews.RoundedImageView;
import ir.rasen.charsoo.view.widgets.material_library.views.CustomView;

/**
 * Class for Downloading image
 *
 * @author Vineet Aggarwal & J
 */

public class SimpleLoader {
    private ImageView imageView;

    private static final String TAG = "ImageDownloader";

    private ServerAnswer serverAnswer;
    private Context context;
    Image_M.ImageType imageType;
    DisplayImageOptions options;
    private int DRAWABLE_EMPTY = R.drawable.ic_empty;
    private int DRAWABLE_ON_FAIL = R.drawable.ic_empty;
//    Bitmap businessDefaultImage;

    public SimpleLoader(Context context) {
        this.context = context;

//        businessDefaultImage=(BitmapFactory.decodeResource(context.getResources(), R.drawable.default_user_image)).copy(Bitmap.Config.ARGB_8888,true);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
        ImageLoader.getInstance().init(config);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_empty)
                .showImageForEmptyUri(DRAWABLE_EMPTY)
                .showImageOnFail(DRAWABLE_ON_FAIL)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        //DownloadImages downloadImages = new DownloadImages(context);
        //downloadImages.download(pictureUniqueId,imageSize,imageType,imageView,false);
    }

    public void loadImage(String pictureId, int imageSize, Image_M.ImageType imageType, ImageView imageView) {
        DisplayImageOptions mOptions;
        if(imageType == Image_M.ImageType.REVIEW){
            mOptions = new DisplayImageOptions.Builder()
//                    .showImageOnLoading(bd)
//                    .showImageForEmptyUri(bd)
//                    .showImageOnFail(bd)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .build();
        }else {
            BitmapDrawable bd =
                    new BitmapDrawable(context.getResources(),
                            BitmapFactory.decodeResource(context.getResources(),
                                    R.drawable.default_user_image));
            mOptions = new DisplayImageOptions.Builder()
                    .showImageOnLoading(bd)
                    .showImageForEmptyUri(bd)
                    .showImageOnFail(bd)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .build();

        }
        loadImage(pictureId, imageSize, imageType, imageView, null, mOptions);
    }

    public void loadImage(String pictureId, int imageSize, Image_M.ImageType imageType, ImageView imageView,
                          final CustomView progressBar) {
        loadImage(pictureId, imageSize, imageType, imageView, progressBar, options);
    }

    public void loadImage(String pictureId, int imageSize,
                          Image_M.ImageType imageType, ImageView imageView, String string) {
        loadImage(pictureId, imageSize, imageType, imageView,
                imageView.getLayoutParams().width, imageView.getLayoutParams().height,
                null, string);

    }

    public void loadImage(String pictureId, int imageSize, Image_M.ImageType imageType, ImageView imageView, CustomView progressBar, String string) {
        loadImage(pictureId, imageSize, imageType, imageView, imageView.getLayoutParams().width, imageView.getLayoutParams().height, progressBar, string);
    }

    public void loadImage(
            String pictureId, int imageSize, Image_M.ImageType imageType,
            ImageView imageView, int width, int height, CustomView progressBar, String string){

        if (string == null) {
            string = "Ch";
        } else if (string.length() > 0)
            string = string.substring(0, 1).toUpperCase();

        DisplayImageOptions mOptions = options;
        TextDrawable td = null;

        if(imageType == Image_M.ImageType.USER) {
            td = TextDrawable.builder()
                    .beginConfig()
                    .width(width)  // width in px
                    .height(height) // height in px
                    .endConfig().buildRound(string, ColorGenerator.MATERIAL.getColor(string));

            mOptions = new DisplayImageOptions.Builder()
                    .showImageOnLoading(td)
                    .showImageForEmptyUri(td)
                    .showImageOnFail(td)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .build();

            if(pictureId==null || pictureId.length()<36) {
                imageView.setImageDrawable(td);
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                return;
            }

        }else if(imageType == Image_M.ImageType.BUSINESS && imageSize==Image_M.SMALL){
            td = TextDrawable.builder()
                    .beginConfig()
                    .width(width)  // width in px
                    .height(height) // height in px
                    .endConfig().buildRoundRect(string, ColorGenerator.MATERIAL.getColor(string), 5);

            mOptions = new DisplayImageOptions.Builder()
                    .showImageOnLoading(td)
                    .showImageForEmptyUri(td)
                    .showImageOnFail(td)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .build();

            if(pictureId==null || pictureId.length()<36) {
                imageView.setImageDrawable(td);
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                return;
            }
        }



        loadImage(pictureId, imageSize, imageType, imageView, progressBar, mOptions);
    }

    public void loadImage(String pictureId, final int imageSize, final Image_M.ImageType imageType,
                          final ImageView imageView, final CustomView progressBar, DisplayImageOptions options) {

        this.imageType = imageType;
        this.imageView = imageView;
        ImageLoader loder =ImageLoader.getInstance();

        String url=  URLs.DOWNLOAD_IMAGE + "/" + pictureId + "/" + imageSize+ "/"+ LoginInfo.getInstance().userAccessToken;
        loder.displayImage(url, imageView, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                if(progressBar!=null)
                    progressBar.setVisibility(View.VISIBLE);

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                if(progressBar!=null)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if(progressBar!=null)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                if(progressBar!=null)
                    progressBar.setVisibility(View.GONE);
            }
        });
    }

}
