package ir.rasen.charsoo.controller.object;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import ir.rasen.charsoo.controller.helper.Hashtag;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.view.activity.ActivityPost;


/**
 * Created by android on 12/17/2014.
 */
public class Post {

    public String uniqueId;
    public String businessUniqueId;
    public String businessUserName;
    public String businessIdentifier;
    public String businessProfilePictureUniqueId;
    public String businessName;

    public String friendUniqueId;
    public String friendIdStr;


    public String userUniqueId;//used when post is follow or review announcement
    public String userName;////used when post is follow or review announcement

    public Date creationDate;

    public String title;
    public String picture;
    public String pictureUniqueId;
    public String description;
    public String price;
    public String code;
    public boolean isLiked = false;
    public boolean isShared = false;
    public boolean isReported = false;

    public int likeNumber;
    public int commentNumber;
    public int shareNumber;

    public enum Type {CompleteBusiness, Follow, Review, CompleteFriend}

    public enum GetPostType {TIMELINE, SHARE, BUSINESS, SEARCH}

    public GetPostType getPostType;
    public Type type;
    public ArrayList<Comment> lastThreeComments = new ArrayList<>();
    public ArrayList<String> hashtagList = new ArrayList<>();

    //in case type equals review
    public int reviewRate;
    public String reviewText;


    public static Date setCreationDate(AnswerObjects.GetBusinessPosts gson) throws Exception {
        String dateStr = gson.CreationDate;
        dateStr = dateStr.replace("/Date(", "").replace(")/", "");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        //Date date =  dateFormat.parse(dateStr);
        Date date = new Date(Long.parseLong(dateStr));
        return date;
    }

    public static Date setCreationDate(AnswerObjects.GetSharedPosts gson) throws Exception {
        String dateStr = gson.CreationDate;
        dateStr = dateStr.replace("/Date(", "").replace(")/", "");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        //Date date =  dateFormat.parse(dateStr);
        Date date = new Date(Long.parseLong(dateStr));
        return date;
    }
    public static Date setCreationDate(AnswerObjects.GetTimeLineAllBusinessPosts gson) throws Exception {
        String dateStr = gson.CreationDate;
        dateStr = dateStr.replace("/Date(", "").replace(")/", "");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        //Date date =  dateFormat.parse(dateStr);
        Date date = new Date(Long.parseLong(dateStr));
        return date;
    }
    public static Date setCreationDate(AnswerObjects.GetPost gson) throws Exception {
        String dateStr = gson.CreationDate;
        dateStr = dateStr.replace("/Date(", "").replace(")/", "");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        //Date date =  dateFormat.parse(dateStr);
        Date date = new Date(Long.parseLong(dateStr));
        return date;
    }


    public static Type getType(int type) {
        switch (type) {
            case 1:
                return Type.Follow;
            case 2:
                return Type.CompleteBusiness;
            case 3:
                return Type.Review;
            case 4:
                return Type.CompleteFriend;
        }

        return Type.CompleteBusiness;
    }

    public static int getTypeCode(Type type) {
        switch (type) {
            case CompleteBusiness:
                return 1;
            case Follow:
                return 2;
            case Review:
                return 3;
            case CompleteFriend:
                return 4;
        }

        return 0;
    }


    public static Post getFromJSONObjectShare(AnswerObjects.GetSharedPosts gson) throws Exception {
        Post post = new Post();
        post.uniqueId = gson.PostId;
        post.businessIdentifier = gson.engbizName;
        try {
            post.businessUniqueId = gson.Business_Id;
        } catch (Exception e) {
            post.businessUniqueId =gson.BusinessId;
        }
        try {
            post.businessUserName = gson.BusinessUserName;
        } catch (Exception e) {

        }

        post.businessProfilePictureUniqueId = gson.BusinessProfilePictureId;
        post.title = gson.Title;

        post.pictureUniqueId = gson.PostPictureId;
        post.description = gson.Description;
        post.code = gson.Code;
        post.price = gson.Price;

      // String comments = gson.Comments;
        AnswerObjects.GetPostAllComments[] cmts= new Gson().fromJson(gson.Comments, AnswerObjects.GetPostAllComments[].class);
        ArrayList<Comment> retval= new ArrayList<>();
        for (int z= 0; z<cmts.length;z++){
            Comment s= new Comment();
            s.isUserSendedComment=cmts[z].IsUser;
            s.ownerUniqueId = cmts[z].CommntOwnerId;
            s.text = cmts[z].Text;
            s.userProfilePictureUniqueId = cmts[z].ProfilePictureId;
            s.uniqueId = cmts[z].CommentId;
            s.username = cmts[z].CommntOwnerTitle;

            retval.add(s);
        }
        post.lastThreeComments = retval;

        post.hashtagList = Hashtag.getListFromString(gson.HashTagList);

        post.isLiked = gson.IsLiked;

        try {
            post.isShared = gson.IsShared;
        } catch (Exception ee) {
            post.isShared = true;
        }

        post.likeNumber = gson.LikeNumber;
        post.commentNumber = gson.CommentNumber;
        post.shareNumber = gson.ShareNumber;

        post.creationDate = setCreationDate(gson);
        post.isReported =gson.IsReported;
        return post;
    }


    public static Post getFromJSONObjectBusiness(String businessID, AnswerObjects.GetBusinessPosts gson) throws Exception {
        Post post = new Post();
        post.uniqueId = gson.PostId;
        post.businessUniqueId = businessID;
        try {

            post.businessIdentifier = gson.BizuniqName;
            post.businessUserName = post.businessIdentifier;
        } catch (Exception e1) {
        }
        post.title = gson.Title;
        post.creationDate = setCreationDate(gson);
        post.pictureUniqueId = gson.PostPictureId;
        post.description = gson.Description;
        post.code = gson.Code;
        post.price = gson.Price;

        String comments = gson.Comments;
        JSONArray jsonArrayComments = new JSONArray(comments);


        post.lastThreeComments = Comment.getFromJSONArray(jsonArrayComments);

        post.hashtagList = Hashtag.getListFromString(gson.HashTagList);

        post.isLiked = gson.IsLiked;
        try {
            post.isShared = gson.IsShared;
        } catch (Exception e) {}

        post.isReported = gson.IsReported;
        post.likeNumber = gson.LikeNumber;
        post.commentNumber = gson.CommentNumber;
        post.shareNumber = gson.ShareNumber;

        return post;
    }


    public static Post getFromJSONObjectTimeLine(AnswerObjects.GetTimeLineAllBusinessPosts gson) throws Exception {
        Post post = new Post();
        post.businessUniqueId =gson.Business_Id;
        post.businessUserName = gson.BusinessUserName;
        post.uniqueId =gson.PostId;
        post.type = getType(gson.PostTypeId);
        post.businessProfilePictureUniqueId = gson.BusinessProfilePictureId;
        if (post.type == Type.CompleteBusiness) {
            post.businessIdentifier = gson.engbizName;
            post.title = gson.Title;
            post.creationDate = setCreationDate(gson);
            post.pictureUniqueId = gson.PostPictureId;
            post.description =  gson.Description;
            post.code = gson.Code;
            post.price = gson.Price;
            post.isLiked = gson.IsLiked;
            post.isReported =gson.IsReported;
            post.isShared = gson.IsShared;

            post.likeNumber = gson.LikeNumber;
            post.commentNumber = gson.CommentNumber;
            post.shareNumber = gson.ShareNumber;
            post.hashtagList = Hashtag.getListFromString(gson.HashTagList);

            String comments =gson.Comments;
            ArrayList<Comment> lastThreeComments = new ArrayList<>();
            AnswerObjects.GetPostAllComments[] tmpcms= new Gson().fromJson(comments, AnswerObjects.GetPostAllComments[].class);
            for (int i=0 ;i< tmpcms.length;i++){

                Comment comment = new Comment();
                comment.uniqueId = tmpcms[i].CommentId;
                comment.ownerUniqueId = tmpcms[i].CommntOwnerId;
                comment.username = tmpcms[i].CommntOwnerTitle;
                comment.userProfilePictureUniqueId = tmpcms[i].ProfilePictureId;
                comment.text =tmpcms[i].Text;
                comment.postUniqueId = gson.PostId;
                comment.businessUniqueId = gson.Business_Id;
                comment.isUserSendedComment =tmpcms[i].IsUser;
                lastThreeComments.add(comment);
            }
            post.lastThreeComments = lastThreeComments;

        }
        return post;
    }

    public static String getReviewText(String jsonObjectString) throws Exception {
        JSONObject jsonObject = new JSONObject(jsonObjectString);
        return jsonObject.getString(Params.REVIEW);
    }

    public static int getReviewRate(String jsonObjectString) throws Exception {
        JSONObject jsonObject = new JSONObject(jsonObjectString);
        return jsonObject.getInt(Params.RATE);
    }

    public boolean isMine(Context context) {
        String ownerId = LoginInfo.getInstance().getUserUniqueId();
        if (ownerId == this.userUniqueId)
            return true;
        return false;
    }

    public static void goPostPage(Context context, String postId, String businessIdForBusinessType, GetPostType getPostType ,boolean isOwnerBusiness) {
        Intent intent = new Intent(context, ActivityPost.class);
        intent.putExtra(Params.POST_ID_INT, postId);
        intent.putExtra(Params.BUSINESS_ID_STRING, businessIdForBusinessType);
        intent.putExtra(Params.POST_TYPE, getPostType.name());
        intent.putExtra(Params.IS_OWNER,isOwnerBusiness);
        context.startActivity(intent);
    }

    public static void goPostPageFromUserHome(Activity activity, String postId, String businessIdForBusinessType, GetPostType getPostType) {
        Intent intent = new Intent(activity, ActivityPost.class);
        intent.putExtra(Params.POST_ID_INT, postId);
        intent.putExtra(Params.BUSINESS_ID_STRING, businessIdForBusinessType);
        intent.putExtra(Params.POST_TYPE, getPostType.name());
        activity.startActivityForResult(intent, Params.ACTION_ACTIVITY_POST);
    }

    public static int getIndexOfPost(ArrayList<Post> posts, String postId) {
        for (int i = 0; i < posts.size(); i++) {
            if (posts.get(i).uniqueId.equals(postId))
                return i;
        }


        //there is not such a post in the posts
        return -1;
    }


    public static Hashtable<String, String> getLastLoadedTimelineItems(ArrayList<Post> loadedItems) {
        Hashtable<String, String> result = new Hashtable<>();
        for (int i = loadedItems.size() - 1; i >= 0; i--) {
            Post p = loadedItems.get(i);
            if ((!result.containsKey(Params.BUSINESS_POST_FOR_TIMELINE)) && (p.type == Type.CompleteBusiness)) {
                result.put(Params.BUSINESS_POST_FOR_TIMELINE, p.uniqueId);
            } else if ((!result.containsKey(Params.FRIEND_SHARED_POST_FOR_TIMELINE)) && (p.type == Type.CompleteFriend)) {
                result.put(Params.FRIEND_SHARED_POST_FOR_TIMELINE, p.uniqueId);
            } else if ((!result.containsKey(Params.FRIEND_FOLLOW_ANNOUNCE_FOR_TIMELINE)) && (p.type == Type.Follow)) {
                result.put(Params.FRIEND_FOLLOW_ANNOUNCE_FOR_TIMELINE, p.uniqueId);
            } else if ((!result.containsKey(Params.FRIEND_REVIEW_ANNOUNCE_FOR_TIMELINE)) && (p.type == Type.Review)) {
                result.put(Params.FRIEND_REVIEW_ANNOUNCE_FOR_TIMELINE, p.uniqueId);
            } else if (result.size() == 4)
                break;
        }
        if (!result.containsKey(Params.BUSINESS_POST_FOR_TIMELINE))
            result.put(Params.BUSINESS_POST_FOR_TIMELINE, "0");

        if (!result.containsKey(Params.FRIEND_SHARED_POST_FOR_TIMELINE))
            result.put(Params.FRIEND_SHARED_POST_FOR_TIMELINE, "0");

        if (!result.containsKey(Params.FRIEND_FOLLOW_ANNOUNCE_FOR_TIMELINE))
            result.put(Params.FRIEND_FOLLOW_ANNOUNCE_FOR_TIMELINE, "0");

        if (!result.containsKey(Params.FRIEND_REVIEW_ANNOUNCE_FOR_TIMELINE))
            result.put(Params.FRIEND_REVIEW_ANNOUNCE_FOR_TIMELINE, "0");

        return result;
    }

}
