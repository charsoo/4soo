package ir.rasen.charsoo.controller.object;

import android.content.Context;

import java.util.ArrayList;

import ir.rasen.charsoo.R;

/**
 * Created by android on 2/2/2015.
 */
public class Category {

    public String categoryUniqueId;
    public String categoryName;

    public Category(){};
    public Category(String categoryUniqueId,String categoryName){
        this.categoryUniqueId = categoryUniqueId;
        this.categoryName = categoryName;
    }

    public static ArrayList<String> getCategoriesName(ArrayList<Category> categories){
        ArrayList<String> tempCategoriesName = new ArrayList<>();

        for(Category tempCategory : categories){
            tempCategoriesName.add(tempCategory.categoryName);
        }

        return tempCategoriesName;
    }


    public static ArrayList<Category> addDefaultCategoryToStartOfList(Context ctx,ArrayList<Category> categories){
        String defaultCategoryName = ctx.getResources().getString(R.string.category);

        if(categories.get(0).categoryName.equals(defaultCategoryName) || Integer.parseInt(categories.get(0).categoryUniqueId)==-1)
            return categories;

        ArrayList<Category> tempCategories = new ArrayList<>();
        tempCategories.add(new Category("-1", defaultCategoryName));
        tempCategories.addAll(categories);

        return tempCategories;
    }
}
