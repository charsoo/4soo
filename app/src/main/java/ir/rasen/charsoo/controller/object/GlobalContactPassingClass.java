package ir.rasen.charsoo.controller.object;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by hossein-pc on 7/14/2015.
 */
public class GlobalContactPassingClass {
    private static GlobalContactPassingClass instance;
    public boolean isPictureTaken;

    private Hashtable<String,ArrayList<ContactEntry>> emailContacts,numberContacts;
    private ArrayList<PackageInfoCustom> appList;

    public static synchronized GlobalContactPassingClass getInstance(){
        if (instance==null)
            instance=new GlobalContactPassingClass();
        return instance;
    }

    public ArrayList<PackageInfoCustom> getAppList(){
        if (appList!=null) {
            ArrayList<PackageInfoCustom> temp = new ArrayList<>();
            for (int i = 0; i < appList.size(); i++) {
                temp.add(appList.get(i));
            }
            return temp;
        }
        else return null;
    }
    public void setAppList(ArrayList<PackageInfoCustom> appList){
        this.appList=new ArrayList<>(appList);
    }

    public Hashtable<String,ArrayList<ContactEntry>> getEmailContacts(){
        if (emailContacts!=null) {
            Hashtable<String, ArrayList<ContactEntry>> temp = new Hashtable<>();
            for (String str : emailContacts.keySet()) {
                temp.put(str, new ArrayList<ContactEntry>());
                for (int i = 0; i < emailContacts.get(str).size(); i++) {
                    temp.get(str).add(emailContacts.get(str).get(i));
                }
            }
            return temp;
        }
        else return null;
    }
    public void setEmailContacts(Hashtable<String,ArrayList<ContactEntry>> emailContacts){
        this.emailContacts=new Hashtable<>(emailContacts);
    }

    public Hashtable<String,ArrayList<ContactEntry>> getNumberContacts(){
        if (numberContacts!=null) {
            Hashtable<String,ArrayList<ContactEntry>> temp=new Hashtable<>();

            for (String str : numberContacts.keySet()) {
                temp.put(str, new ArrayList<ContactEntry>());
                for (int i = 0; i < numberContacts.get(str).size(); i++) {
                    temp.get(str).add(numberContacts.get(str).get(i));
                }
            }
            return temp;
        }
        else
            return null;
    }
    public void setNumberContacts(Hashtable<String,ArrayList<ContactEntry>> numberContacts){
        this.numberContacts=new Hashtable<>(numberContacts);
    }

}
