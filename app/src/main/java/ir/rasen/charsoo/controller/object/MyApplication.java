package ir.rasen.charsoo.controller.object;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;

import java.util.ArrayList;
import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.Alarm_M;
import ir.rasen.charsoo.controller.helper.BaseAdapterItem;
import ir.rasen.charsoo.controller.helper.Image_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Permission;
import ir.rasen.charsoo.controller.helper.WebservicesHandler;
import ir.rasen.charsoo.controller.image_loader.SimpleLoader;
import ir.rasen.charsoo.model.user.Login;
import ir.rasen.charsoo.view.activity.ActivityPost;
import ir.rasen.charsoo.view.activity.business.ActivityBusiness;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessContactInfo;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessOther;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessReviews2;
import ir.rasen.charsoo.view.activity.user.ActivityUserFollowingBusinesses;
import ir.rasen.charsoo.view.activity.user.ActivityUserReviews;
import ir.rasen.charsoo.view.fragment.FragmentUser;
import ir.rasen.charsoo.view.fragment.FragmentUserBusinesses;
import ir.rasen.charsoo.view.fragment.FragmentUserOther;
import ir.rasen.charsoo.view.fragment.timeline.FragmentTimelineAllPosts;
import ir.rasen.charsoo.view.fragment.timeline.FragmentTimelineFollowingBusinessPosts;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.shared.ScreenUtils;

/**
 * Created by android on 3/15/2015.
 */
public class MyApplication extends Application {

    private WebservicesHandler webservicesHandler;
    public Post post;
    public ArrayList<BaseAdapterItem> searchUserResult;
    public ArrayList<BaseAdapterItem> newFriends;
    //    public boolean isHomeCreated, isUserCreated;
    public ArrayList<Post> homePosts;

    @Override
    public void onCreate() {
        super.onCreate();
        webservicesHandler = new WebservicesHandler();
        webservicesHandler.currentWebservice = WebservicesHandler.Webservices.NONE;

        post = new Post();
        searchUserResult = new ArrayList<>();
        newFriends = new ArrayList<>();
        Alarm_M alarm_m = new Alarm_M();
        alarm_m.set(getApplicationContext());

//        isHomeCreated = false;
//        isUserCreated = false;
        homePosts = new ArrayList<>();

        // configure Flurry
        FlurryAgent.setLogEnabled(false);

        // init Flurry
        FlurryAgent.init(this, "2TRS3S7HJMTDDQSSYB5J");
    }

    public WebservicesHandler.Webservices getCurrentWebservice() {
        return webservicesHandler.currentWebservice;
    }

    public void setCurrentWebservice(WebservicesHandler.Webservices currentWebservice) {
        this.webservicesHandler.currentWebservice = currentWebservice;
    }

    public static RelativeLayout initNetworkErrorLayout(View view, final Activity activity, final TryAgainListener delegate){
        RelativeLayout parentLayout = null;
        if(view != null){
            parentLayout = (RelativeLayout) view.findViewById(R.id.networkFaildView);
            parentLayout.findViewById(R.id.networkFailClickableArea)
                    .setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            delegate.doTryAgain();
                        }
                    });

            parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        }
        return parentLayout;
    }


    public static void broadCastUpdateDirtyPost(String senderTag,String postUniqueId) {

        try {
            if(!senderTag.equals(ActivityPost.TAG))
                ActivityPost.iPostUpdateListener.onPostDetailsChange(postUniqueId);
        } catch (Exception e) {
        }


        try {
            if(!senderTag.equals(FragmentTimelineFollowingBusinessPosts.TAG))
                FragmentTimelineFollowingBusinessPosts.iPostUpdateListener.onPostDetailsChange(postUniqueId);
        } catch (Exception e) {
        }

        try {
            if(!senderTag.equals(FragmentTimelineAllPosts.TAG))
                FragmentTimelineAllPosts.iPostUpdateListener.onPostDetailsChange(postUniqueId);
        } catch (Exception e) {
        }

        try {
            if(!senderTag.equals(ActivityBusinessOther.TAG))
                ActivityBusinessOther.iPostUpdateListener.onPostDetailsChange(postUniqueId);
        } catch (Exception e) {
        }


        try {
            if(!senderTag.equals(ActivityBusiness.TAG))
                ActivityBusiness.iPostUpdateListener.onPostDetailsChange(postUniqueId);
        } catch (Exception e) {
        }

        try {
            if(!senderTag.equals(FragmentUserOther.TAG))
                FragmentUserOther.iPostUpdateListener.onPostDetailsChange(postUniqueId);
        } catch (Exception e) {
        }

        try {
            if(!senderTag.equals(FragmentUser.TAG))
                FragmentUser.iPostUpdateListener.onPostDetailsChange(postUniqueId);
        } catch (Exception e) {
        }
    }
    public static void broadCastAddPostOnBusiness(String TAG,Post post){
        try {
            ActivityBusiness.iAddPost.notifyAddPost(post);
        } catch (Exception e) {
        }
        try {
            FragmentTimelineFollowingBusinessPosts.iAddPost.notifyAddPost(post);
        } catch (Exception e) {
        }
    }
    public static void broadCastReloadTimeline(){
        try {
            FragmentTimelineFollowingBusinessPosts.iTimeLineReload.onTimeLineReload();
        } catch (Exception e) {
        }

        try {
            FragmentTimelineAllPosts.iTimeLineReload.onTimeLineReload();
        } catch (Exception e) {
        }
    }
    public static void boradCastFollowStateChanged(String targetBusinessId){

        try {
            ActivityUserFollowingBusinesses.iOnFollowStatudChangedListener.onFollowStatusChanged(targetBusinessId);
        } catch (Exception e) {
        }


    }
    public static void broadCastUnsharePost(String targetPostId){
        //when we unShare a post on activity post , its not remove from fragment user,
        //And this function for solve this problem

        FragmentUser.iOnUnsharePostListener.onUnsharePost(targetPostId);

    }
    public static void broadCastEditedBusiness(String businessEditedId){
        try {
            ActivityBusiness.iOnBusinessEditedListener.onBusinessEdited(businessEditedId);
        }catch (Exception e){}
        try {
            ActivityBusinessContactInfo.iEditContactInfo.onContactEdited();
        }catch (Exception e){}


        try {
            FragmentUserBusinesses.iOnBusinessEditedListener.onBusinessEdited(businessEditedId);
        }catch (Exception e){}
    }
    public static void broadCastUpdateReview(String revId){
        try {
            Review rv = new Review();
            rv.uniqueId = revId;
            ActivityBusinessReviews2.mOnReviewChange.notifyUpdateReview(rv);
        } catch (Exception e) {
        }

        try {
            Review rv = new Review();
            rv.uniqueId = revId;
            ActivityUserReviews.iReviewChange.notifyUpdateReview(rv);
        } catch (Exception e) {
        }
    }

    public static void broadCastDeleteReview(String revUnqId){
        try {
            ActivityBusinessReviews2.mOnReviewChange.notifyDeleteReview(revUnqId);
        } catch (Exception e) {
        }

        try {
            ActivityUserReviews.iReviewChange.notifyDeleteReview(revUnqId);
        } catch (Exception e) {
        }
    }

    public static void broadCastAddReview(RequestObject.ReviewItem revItem){
        Review rv = new Review();
        rv.uniqueId = revItem.RevId;

        try {
            ActivityBusinessReviews2.mOnReviewChange.notifyUpdateReview(rv);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            ActivityUserReviews.iReviewChange.notifyUpdateReview(rv);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static Post mergeTwoPost(Post mainPost,Post newPost){
        Post tempPost = new Post();

        tempPost.uniqueId = mainPost.uniqueId;
        tempPost.businessUniqueId = mainPost.businessUniqueId;

        tempPost.businessUserName =
                (newPost.businessUserName != null && newPost.businessUserName != "")?
                        newPost.businessName:
                        mainPost.businessUserName;

        tempPost.businessIdentifier =
                (newPost.businessIdentifier != null && newPost.businessIdentifier != "")?
                        newPost.businessIdentifier:
                        mainPost.businessIdentifier;

        tempPost.businessProfilePictureUniqueId =
                (newPost.businessProfilePictureUniqueId!=null && !newPost.businessProfilePictureUniqueId.equals("0"))?
                        newPost.businessProfilePictureUniqueId :
                        mainPost.businessProfilePictureUniqueId;

        tempPost.businessName =
                (newPost.businessName != null && newPost.businessName != "")?
                        newPost.businessName:
                        mainPost.businessName;


        tempPost.friendUniqueId =
                (newPost.friendUniqueId!=null && !newPost.friendUniqueId.equals("0"))?
                        newPost.friendUniqueId :
                        mainPost.friendUniqueId;


        tempPost.friendIdStr =
                (newPost.friendIdStr != null && newPost.friendIdStr != "")?
                        newPost.friendIdStr:
                        mainPost.friendIdStr;


        tempPost.userUniqueId =
                (newPost.userUniqueId!=null &&!newPost.userUniqueId.equals("0"))?
                        newPost.userUniqueId :
                        mainPost.userUniqueId;


        tempPost.userName =
                (newPost.userName != null && newPost.userName != null && newPost.userName != "")?
                        newPost.userName:
                        mainPost.userName;

        tempPost.creationDate =
                (newPost.creationDate != null)?
                        newPost.creationDate:
                        mainPost.creationDate;


        tempPost.title =
                (newPost.title != null && newPost.title != "")?
                        newPost.title:
                        mainPost.title;


        tempPost.picture =
                (newPost.picture != null && newPost.picture != "")?
                        newPost.picture:
                        mainPost.picture;


        tempPost.pictureUniqueId =
                (newPost.pictureUniqueId!=null && !newPost.pictureUniqueId.equals("0"))?
                        newPost.pictureUniqueId :
                        mainPost.pictureUniqueId;


        tempPost.description =
                (newPost.description != null && newPost.description != "")?
                        newPost.description:
                        mainPost.description;


        tempPost.price =
                (newPost.price != null && newPost.price != "")?
                        newPost.price:
                        mainPost.price;



        tempPost.code =
                (newPost.code != null && newPost.code != "")?
                        newPost.code:
                        mainPost.code;

        tempPost.isLiked = newPost.isLiked;
        tempPost.isShared = newPost.isShared;
        tempPost.isReported = newPost.isReported;


        tempPost.likeNumber = newPost.likeNumber;
        tempPost.commentNumber = newPost.commentNumber;
        tempPost.shareNumber = newPost.shareNumber;

        tempPost.getPostType =
                (newPost.getPostType != null)?
                        newPost.getPostType:
                        mainPost.getPostType;


        tempPost.type =
                (newPost.type != null)?
                        newPost.type:
                        mainPost.type;

        tempPost.lastThreeComments =
                (newPost.lastThreeComments != null)?
                        newPost.lastThreeComments:
                        mainPost.lastThreeComments;


        tempPost.hashtagList =
                (newPost.hashtagList != null)?
                        newPost.hashtagList:
                        mainPost.hashtagList;

        return tempPost;
    }


    public int getApplicationVersionCode(){
        try {
            return getPackageManager().getPackageInfo(getPackageName(),0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            return -1;
        }
    }


    public static void loadUserProfilePicture(Context ctx,ImageView ivProfile,int imageSize,Image_M.ImageType type) {
        String profilePictureUniqueId =
                LoginInfo.getInstance().getUserProfilePictureUniqueId();
        int size = ScreenUtils.convertDpToPx(ctx, ctx.getResources().getDimension(R.dimen.profile_pic));
        SimpleLoader simpleLoader = new SimpleLoader(ctx);
        simpleLoader.loadImage(profilePictureUniqueId, imageSize, type
                , ivProfile, size, size, null, LoginInfo.getInstance().username);
    }

    public static void loadUserProfilePicture(Context ctx,ImageView ivProfile,
                                              int imageSize,Image_M.ImageType type,String userId) {
        int size = ScreenUtils.convertDpToPx(ctx, ctx.getResources().getDimension(R.dimen.profile_pic));
        SimpleLoader simpleLoader = new SimpleLoader(ctx);
        simpleLoader.loadImage(userId, imageSize, type
                , ivProfile);
    }

    public static void loadReviewImage(Context ctx,ImageView ivPicture, String imageUniqueId) {
        SimpleLoader simpleLoader = new SimpleLoader(ctx);
        simpleLoader.loadImage(imageUniqueId, Image_M.LARGE,
                Image_M.ImageType.REVIEW,ivPicture);
    }

    public static void loadBusinessProfilePicture(Context ctx,ImageView ivProfile,String imageUnqId,String bizUserId){

        SimpleLoader simpleLoader = new SimpleLoader(ctx);
        simpleLoader.loadImage(imageUnqId, Image_M.SMALL,Image_M.ImageType.BUSINESS,ivProfile,
                bizUserId);
    }


}
