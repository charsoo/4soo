package ir.rasen.charsoo.controller.helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Rasen_iman on 8/27/2015.
 */
public class SharedPrefHandler {
    public static void removeUserEmailPassword(Context activity) {
        SharedPreferences preferences = activity.getSharedPreferences(
                activity.getPackageName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString(Params.USER_EMAIL_PREFERENCES, "0");
        edit.putString(Params.USER_PASSWORD_PREFERENCES, "0");
        edit.commit();
    }

    public static void saveUserEmailPassword(
            Context context,String userEmail, String userPassword) {

        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(), Context.MODE_PRIVATE);

        SharedPreferences.Editor edit = preferences.edit();

        if(userEmail!=null && !userEmail.equals(""))
            edit.putString(Params.USER_EMAIL_PREFERENCES, userEmail);
        if(userPassword!=null && !userPassword.equals(""))
            edit.putString(Params.USER_PASSWORD_PREFERENCES, userPassword);
        edit.commit();
    }

    public static boolean isSavedUserEmailPassword(Context activity){
        SharedPreferences preferences =
                activity.getSharedPreferences(
                        activity.getPackageName(),Context.MODE_PRIVATE);

        String userEmail = preferences.getString(Params.USER_EMAIL_PREFERENCES, "");
        String userPassword = preferences.getString(Params.USER_PASSWORD_PREFERENCES, "");

        if(!userEmail.equals("")    && !userEmail.equals("0") &&
           !userPassword.equals("") && !userPassword.equals("0"))
            return true;
        else
            return false;
    }

    public static String getUserSavedEmail(Context activity){
        SharedPreferences preferences = activity.getSharedPreferences(
                activity.getPackageName(), Context.MODE_PRIVATE);

        String email = preferences.getString(Params.USER_EMAIL_PREFERENCES,"");
        return email;
    }

    public static String getUserSavedPassword(Context activity){
        SharedPreferences preferences = activity.getSharedPreferences(
                activity.getPackageName(), Context.MODE_PRIVATE);

        String pass = preferences.getString(Params.USER_PASSWORD_PREFERENCES,"");

        return pass;
    }
}
