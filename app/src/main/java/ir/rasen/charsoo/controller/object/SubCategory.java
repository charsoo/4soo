package ir.rasen.charsoo.controller.object;

import android.content.Context;

import java.util.ArrayList;

import ir.rasen.charsoo.R;

/**
 * Created by android on 2/2/2015.
 */
public class SubCategory {

    public String subCaregoryUniqueId;
    public String subCategoryName;

    public SubCategory(){

    }

    public SubCategory(String subCaregoryUniqueId, String subCategoryName){
        this.subCaregoryUniqueId = subCaregoryUniqueId;
        this.subCategoryName = subCategoryName;
    }

    public static ArrayList<String> getSubCategoriesName(ArrayList<SubCategory> subCategories){
        ArrayList<String> tempSubCategoriesName = new ArrayList<>();

        for(SubCategory tempSubCategory : subCategories){
            tempSubCategoriesName.add(tempSubCategory.subCategoryName);
        }


        return tempSubCategoriesName;
    }

    public static ArrayList<SubCategory> addDefaultSubCategoryToStartOfList(Context ctx,ArrayList<SubCategory> subCategories){
        String defaultSubCategoryName = ctx.getResources().getString(R.string.subcategory);

        if(subCategories.get(0).subCategoryName.equals(defaultSubCategoryName) ||
                Integer.parseInt(subCategories.get(0).subCaregoryUniqueId)==-1)
            return subCategories;

        ArrayList<SubCategory> tempSubCategories = new ArrayList<>();
        tempSubCategories.add(new SubCategory("-1", defaultSubCategoryName));
        tempSubCategories.addAll(subCategories);

        return tempSubCategories;
    }
}
