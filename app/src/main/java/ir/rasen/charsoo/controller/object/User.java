package ir.rasen.charsoo.controller.object;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import ir.rasen.charsoo.view.activity.user.ActivityUserOther;
import ir.rasen.charsoo.controller.helper.FriendshipRelation;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.Permission;
import ir.rasen.charsoo.controller.helper.Sex;


/**
 * Created by A.Kaveh on 11/29/2014.
 */
public class User {

    public String uniqueId;
    public String userIdentifier;
    public String name;
    public String email;
    public String phoneNumber;
    public boolean isEmailConfirmed;
    public String password;
    public String aboutMe;
    public Sex sex;
    public String birthDate;
    public String profilePicture;
    public String profilePictureUniqueId;
    public String coverPictureUniqueId;
    public String coverPicture;
    public FriendshipRelation.Status friendshipRelationStatus;
    public int friendRequestNumber;
    public int reviewsNumber;
    public int followedBusinessesNumber;
    public int friendsNumber;
    public ArrayList<UserBusinesses> businesses;

    public boolean isUserSeenInSuggestion;




    public static class UserPicture{
        public int pictureId;
        public String pictureString;
    }

    public static class UserBusinesses{
        public String businessUniqueId;
        public String businessIdentifier;
        public UserBusinesses(String businessUniqueId,String businessIdentifier){
            this.businessUniqueId = businessUniqueId;
            this.businessIdentifier = businessIdentifier;
        }
        public UserBusinesses(){

        }
    }

    public static ArrayList<UserBusinesses> getUserBusinesses(JSONArray jsonArray)throws Exception {
        ArrayList<UserBusinesses> businesses = new ArrayList<>();

        for (int j = 0; j < jsonArray.length(); j++) {
            JSONObject jsonObject = jsonArray.getJSONObject(j);
            UserBusinesses userBusinesses = new UserBusinesses();
            userBusinesses.businessUniqueId = jsonObject.getString(Params.BUSINESS_ID_STRING);
            userBusinesses.businessIdentifier = jsonObject.getString(Params.BUSINESS_USERNAME_STRING);
            businesses.add(userBusinesses);
        }

        return businesses;
    }

    public static void goUserHomeInfoPage(Context context,String visitedUserUniqueId){
        Intent intent = new Intent(context, ActivityUserOther.class);
        intent.putExtra(Params.VISITED_USER_UNIQUE_ID,visitedUserUniqueId);
        context.startActivity(intent);
    }

    public static void clearSearchHistory(Context context){
        Toast.makeText(context,"Clear Search History",Toast.LENGTH_LONG).show();
    }


}
