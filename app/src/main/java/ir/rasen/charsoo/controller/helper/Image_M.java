package ir.rasen.charsoo.controller.helper;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Base64;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import ir.rasen.charsoo.R;


/**
 * Created by android on 1/20/2015.
 */
public class Image_M {


    public static int LARGE = 1;
    public static int MEDIUM = 2;
    public static int SMALL = 3;
    public static int COVER = 4;

    public enum ImageType {USER, BUSINESS, POST , REVIEW}


    public static int getDefaultImage(ImageType imageType) {
        int imageId = 0;
        switch (imageType) {
            case BUSINESS:
                imageId = R.drawable.ic_person_grey600_36dp;
                break;
            case POST:
                imageId = R.drawable.ic_person_grey600_36dp;
                break;
            case USER:
                imageId = R.drawable.ic_person_grey600_36dp;
                break;

        }
        return imageId;
    }

    public static String getBase64String(String imageFilePath) {

        int reqWidth=Params.IMAGE_MAX_WIDTH_HEIGHT_PIX,reqHeight=Params.IMAGE_MAX_WIDTH_HEIGHT_PIX;
        Bitmap bm=BitmapFactory.decodeFile(resizeImageThenGetPath(imageFilePath,reqWidth,reqHeight));
        double si = (double) Image_M.sizeOf(bm);


        int w = bm.getWidth();
        int h = bm.getHeight();

        if(si>2048000){
            double ratio = (double) 2048000/si;
            int newW = (int)(ratio*(double)w);
            int newH = (int)(ratio*(double)h);

            bm = BitmapFactory.decodeFile(resizeImageThenGetPath(imageFilePath,newW,newH));
        }


        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encodedImage.replace("\n", "");
    }

    public static String getBase64FromBitmap(Bitmap image)
    {
        Bitmap immagex=image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b,Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }

    public static Bitmap getBitmapFromString(String codedImage) {
        byte[] decodedString = Base64.decode(codedImage, Base64.DEFAULT);
//        Bitmap decodedByte =
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    public static void saveBitmap(String path, String imageName, Bitmap bitmap) {
        File checkDirectory = new File(path);
        if (!checkDirectory.exists())
            checkDirectory.mkdirs();

        File file = new File(path, imageName);
        file.mkdir();
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        } catch (Exception e) {
            String s = e.getMessage();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    public static int sizeOf(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return data.getRowBytes() * data.getHeight();
        } else {
            return data.getByteCount();
        }
    }

    static long getImageLength(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] imageInByte = stream.toByteArray();
        return imageInByte.length;
    }

    static int getBytesPerPixel(Config config) {
        if (config == Config.ARGB_8888) {
            return 4;
        } else if (config == Config.RGB_565) {
            return 2;
        } else if (config == Config.ARGB_4444) {
            return 2;
        } else if (config == Config.ALPHA_8) {
            return 1;
        }
        return 1;
    }

    public static Bitmap readBitmapFromStorate(String filePath) {
        return BitmapFactory.decodeFile(filePath);
    }

    public static void deletePictureById(Context context, int id) {
        String storagePath = Environment.getExternalStorageDirectory().getAbsolutePath() +
                context.getResources().getString(R.string.download_storage_path);

        File file = new File(storagePath + "/" + String.valueOf(id) + "_" + String.valueOf(1) + ".jpg");
        if (file.exists())
            file.delete();
        file = new File(storagePath + "/" + String.valueOf(id) + "_" + String.valueOf(2) + ".jpg");
        if (file.exists())
            file.delete();

        file = new File(storagePath + "/" + String.valueOf(id) + "_" + String.valueOf(3) + ".jpg");
        if (file.exists())
            file.delete();

    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }


    public static Bitmap drawableToBitmap (Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable)drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }


    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;

        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


    public static String resizeImageThenGetPath(String imagePath,int reqWidth,int reqHeight)
    {
//        String resultFileSubdirectory=getFileDirectory(imagePath);//+"/ResizedImage/";
        String resultFileSubdirectory=Environment.getExternalStorageDirectory().getAbsolutePath()+Params.CHARSOO_TEMP_FILE_SUBDIRECTORY;
        String resultFileName="ResizedImage.jpg";
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(imagePath,options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
//        float maxHeight = 640.0f;
//        float maxWidth = 640.0f;
//            float imgRatio = actualWidth / actualHeight;
//            float maxRatio = maxWidth / maxHeight;
//
//            if (actualHeight > maxHeight || actualWidth > maxWidth) {
//                if (imgRatio < maxRatio) {
//                    imgRatio = maxHeight / actualHeight;
//                    actualWidth = (int) (imgRatio * actualWidth);
//                    actualHeight = (int) maxHeight;
//                } else if (imgRatio > maxRatio) {
//                    imgRatio = maxWidth / actualWidth;
//                    actualHeight = (int) (imgRatio * actualHeight);
//                    actualWidth = (int) maxWidth;
//                } else {
//                    actualHeight = (int) maxHeight;
//                    actualWidth = (int) maxWidth;
//
//                }
//            }

        int outputImageWidth=actualWidth,outputImageHeight=actualHeight;
        if (actualHeight>actualWidth){
            if (actualHeight> reqHeight){
                outputImageHeight=reqHeight;
                outputImageWidth=(int) Math.floor((double) actualWidth *( (double) outputImageHeight / (double) actualHeight));
            }
        }
        else{
            if (actualWidth> reqWidth){
                outputImageWidth=reqWidth;
                outputImageHeight= (int) Math.floor((double) actualHeight *( (double) outputImageWidth / (double) actualWidth));
            }
        }
//        if (actualHeight<actualWidth){
//            if (actualHeight> (Params.IMAGE_MAX_WIDTH_HEIGHT_PIX*2)){
//                outputImageHeight=Params.IMAGE_MAX_WIDTH_HEIGHT_PIX;
//                outputImageWidth=(int) Math.floor((double) actualWidth *( (double) outputImageHeight / (double) actualHeight));
//            }
//        }
//        else{
//            if (actualWidth> (Params.IMAGE_MAX_WIDTH_HEIGHT_PIX*2)){
//                outputImageWidth=Params.IMAGE_MAX_WIDTH_HEIGHT_PIX;
//                outputImageHeight= (int) Math.floor((double) actualHeight *( (double) outputImageWidth / (double) actualWidth));
//            }
//        }

        actualHeight=outputImageHeight;
        actualWidth=outputImageWidth;
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16*1024];

        try{
            bmp = BitmapFactory.decodeFile(imagePath,options);
        }
        catch(OutOfMemoryError exception){
            exception.printStackTrace();
        }

        try{
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
            int i=1;
        }
        catch(OutOfMemoryError exception){
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float)options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth()/2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));


        ExifInterface exif;
        try {
            exif = new ExifInterface(imagePath);

            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileOutputStream out;
        String filename = resultFileSubdirectory+resultFileName;
        try {
            File f=new File(resultFileSubdirectory);
            if (!f.exists())
                f.mkdirs();
            out = new FileOutputStream(filename);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return filename;
    }
}
