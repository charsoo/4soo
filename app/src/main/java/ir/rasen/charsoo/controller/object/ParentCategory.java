package ir.rasen.charsoo.controller.object;

import java.util.ArrayList;

/**
 * Created by android on 2/2/2015.
 */
public class ParentCategory {

    public String uniqueId;
    public String name;
    public boolean isSub = false;

    public ParentCategory(String uniqueId, String name, boolean isSub){
        this.uniqueId = uniqueId;
        this.name = name;
        this.isSub = isSub;
    }

    public static ArrayList<ParentCategory> fromArrayCategories(ArrayList<Category> result) {
        ArrayList<ParentCategory> parentCategories = new ArrayList<>();
        for(Category category : result) {
            parentCategories.add(new ParentCategory(category.categoryUniqueId, category.categoryName, false));
        }
        return parentCategories;
    }

    public static ArrayList<ParentCategory> fromArraySubCategories(ArrayList<SubCategory> result) {
        ArrayList<ParentCategory> parentCategories = new ArrayList<>();
        for(SubCategory subCategory : result) {
            parentCategories.add(new ParentCategory(subCategory.subCaregoryUniqueId, subCategory.subCategoryName, true));
        }
        return parentCategories;
    }
}
