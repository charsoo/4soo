package ir.rasen.charsoo.controller.helper;

/**
 * Created by android on 12/16/2014.
 */
public enum Sex {
    NONE, MALE, FEMALE;


    public static int getInt(Sex sex) {
        if(sex==null) return 0;
        switch (sex) {
            case NONE:
                return 0;
            case MALE:
                return 1;
            case FEMALE:
                return 2;
        }
        return 0;
    }

    public static Sex getSex(String sexInt) {
        if(sexInt==null){
            return NONE;
        }
        if(sexInt.equals("")){
            return NONE;
        }
        if (sexInt.equals("0"))
            return NONE;
        else if (sexInt.equals("1"))
            return MALE;
        else if (sexInt.equals("2"))
            return FEMALE;
        return NONE;

    }
}
