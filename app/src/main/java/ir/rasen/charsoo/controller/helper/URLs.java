package ir.rasen.charsoo.controller.helper;

public class URLs {

   public static String MAIN_URL = "http://bia4soo.com:5228/";

    //lcoal address
//    public static String MAIN_URL = "http://192.168.1.133:5228/";

    public static String LOGIN = MAIN_URL+"login";
    public static String Logout = MAIN_URL+"logout";
    public static String Report_Image = MAIN_URL + "ReportImage";
    public static String Delete_Image_From_Galllry = MAIN_URL+"RemoveImageFromGallery";
    public static String Get_Gallery_Image = MAIN_URL+"GetGalleryImages";
    public static String Send_Image = MAIN_URL +"AddImageToGallery";
    public static String FORGET_PASSWORD = MAIN_URL+"forgotPassword";
    public static String REGISTER_USER = MAIN_URL+"registerUser";
    public static String REGISTER_BUSINESS = MAIN_URL+"registerBusiness";
    public static String UPDATE_PROFILE_USER = MAIN_URL+"updateUserProfile";
    public static String UPDATE_PROFILE_BUSINESS = MAIN_URL+"updateBusinessProfile";
    public static String REQUEST_CONFIRMATION = MAIN_URL+"requestUserConfirmation";
    public static String UPDATE_PASSWORD = MAIN_URL+"UpdatepassWord";
    public static String GET_HOME_INFO = MAIN_URL+"getUserHomeInfo";
    public static String GET_BUSINESS_HOME_INFO = MAIN_URL+"getBusinessHomeInfo";
    public static String GET_PROFILE_INFO = MAIN_URL+"getUserProfileInfo";
    public static String GET_BUSINESS_PROFILE_INFO = MAIN_URL+"getBusinessProfileInfo";
    public static String GET_FOLLOWING_BUSINESSES = MAIN_URL+"getUserFollowedBusinesses";
    public static String GET_USER_FRIENDS = MAIN_URL+"getUserFriendList";
    public static String GET_USER_FRIEND_REQUEST = MAIN_URL+"getUserFriendRequestList";
    public static String GET_USER_BUSINESSES= MAIN_URL+"getUserBusinesses";
    public static String UPDATE_SETTING = MAIN_URL+"updateUserPermission";
    public static String GET_POSTS = MAIN_URL+"getBusinessPosts";
    public static String GET_USER_POST = MAIN_URL+"getUserPost";
    public static String GET_TIME_LINE_POSTS = MAIN_URL+"getWallPosts2";
    public static String GET_TIMELINE_ALL_BUSINESSES_POSTS = MAIN_URL + "getAllProducts";

    public final static String GET_SHARED_POSTS = MAIN_URL+"getUserSharedPosts";
    public final static String LIKE = MAIN_URL+"likePost";
    public final static String DISLIKE = MAIN_URL+"dislikePost";
    public final static String SEND_COMMENT = MAIN_URL+"commentOnPost";
    public final static String UPDATE_COMMENT = MAIN_URL+"updateComment";
    public final static String DELETE_COMMENT = MAIN_URL+"deleteComment";
    public final static String GET_COMMENTS = MAIN_URL+"getPostComments";
    public final static String SHARE_POST = MAIN_URL+"sharePost";
    public final static String CANCEL_SHARE = MAIN_URL+"cancelShare";
    public final static String REPORT = MAIN_URL+"reportPost";
    public final static String ADD_POST = MAIN_URL+"addPost";
    public final static String UPDATE_POST = MAIN_URL+"updatePost";
    public final static String DELETE_POST = MAIN_URL+"deletePost";
    public final static String REQUEST_FRIENDSHIP = MAIN_URL+"requestFriendship";
    public final static String ANSWER_REQUEST_FRIENDSHIP = MAIN_URL+"answerRequestFriendship";
    public final static String GET_USER_REVIEWS = MAIN_URL+"getUserReviews";
    public final static String GET_BUSINESS_REVIEWS = MAIN_URL+"getBusinessReviews";
    public final static String REVIEW_BUSINESS = MAIN_URL+"reviewBusiness";
    public final static String DELETE_REVIEW = MAIN_URL+"deleteReview";
    public final static String SEARCH_BUSINESS_LOCATION = MAIN_URL+"searchBusinessByLocation";
    public final static String SEARCH_USER = MAIN_URL+"searchUser";
    public final static String SEARCH_POST= MAIN_URL+"searchPosts";
    public final static String GET_BUSINESS_FOLLOWERS= MAIN_URL+"getBusinessFollowers";
    public final static String BLOCK_BUSINESS= MAIN_URL+"blockBusiness";
    public final static String BLOCK_USER= MAIN_URL+"blockUser";
    public final static String UNBLOCK_USER= MAIN_URL+"unBlockUser";
    public final static String GET_BLOCKED_USERS= MAIN_URL+"getBlockedUsersList";
    public final static String RATE_BUSINESS= MAIN_URL+"rateBusiness";
    public final static String GET_ALL_COMMENT_NOTIFICATIONS= MAIN_URL+"getAllCommentNotifications";
    public final static String FOLLOW_BUSINESS= MAIN_URL+"followBusiness";
    public final static String UN_FOLLOW_BUSINESS= MAIN_URL+"unfollowBusiness";
    public final static String GET_BUSINESS_CATEGORIES= MAIN_URL+"getBusinessCategories";
    public final static String GET_BUSINESS_SUBCATEGORIES= MAIN_URL+"getBusinessSubCategories";
    public final static String DOWNLOAD_IMAGE= MAIN_URL+"downloadImage";
    public final static String CHECK_BUSINESS_IDENTIFIER = MAIN_URL + "checkBusinessIdentifier";
    public final static String CHECK_USER_IDENTIFIER = MAIN_URL + "checkUserIdentifier";
    public final static String GET_COUNTRY_STATES = MAIN_URL+"getAllProviences";
    public final static String GET_PROVINCE_CITIES = MAIN_URL + "getProvienceCities";
    public final static String TAKE_CONTACT_LIST = MAIN_URL + "takeContactList";
    public final static String UNFRIEND_USER= MAIN_URL + "unfriendUser";
    public final static String GET_BURST_ADS= MAIN_URL + "getBurstAds";

    public final static String GET_VERSION = MAIN_URL + "getVersion";

    public final static String GET_REVIEW_BY_ID = MAIN_URL+"GetReviewById";
    public final static String GALLERY_ID = MAIN_URL+"getimagefromgallery";

    public final static String GET_USER_REVIEW_BY_BIZ_ID = MAIN_URL + "GetUserReviewByBizId";
    public final static String GET_OVERALL_REVIEW_STATUS = MAIN_URL + "GetOverallReviewStatus";
    public final static String GET_REVIEW_HEADER = MAIN_URL + "GetReviewHeader";
    public final static String GET_REVIEWS_FOR_BUSINESS = MAIN_URL + "GetReviewsForBusiness";
    public final static String REVIEW_ACTIVITY = MAIN_URL + "ReviewActivity";
    public final static String UPDATE_REVIEW = MAIN_URL + "UpdateReview";

}
