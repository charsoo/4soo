package ir.rasen.charsoo.controller.object;

import com.google.gson.Gson;

import org.json.JSONObject;

import ir.rasen.charsoo.controller.helper.Params;

/**
 * Created by hossein-pc on 7/28/2015.
 */
public class SuggestedBusiness {
    public String businessUniqueId;
    public String businessStrId;
    public String businessName;
    public float businessRate;
    public String businessProfilePictureUniqueId;
    public double distance;
    public String businessPictureString;


    public static SuggestedBusiness getSuggestedBusinessFromJSON(AnswerObjects.SuggestedBusiness gson) throws Exception{
        SuggestedBusiness suggestedBusiness=new SuggestedBusiness();
        suggestedBusiness.businessUniqueId =gson.BusinessId;
        suggestedBusiness.businessStrId=gson.BusinessUserName;
        suggestedBusiness.businessName=gson.BusinessName;
        suggestedBusiness.businessProfilePictureUniqueId =gson.BusinessProfilePictureId;
        suggestedBusiness.businessRate=(float)gson.BusinessRate;
        suggestedBusiness.distance= gson.Distance;
        suggestedBusiness.businessPictureString=gson.businessPictureString;
        return suggestedBusiness;
    }

}
