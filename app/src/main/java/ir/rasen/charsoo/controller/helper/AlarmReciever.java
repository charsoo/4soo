package ir.rasen.charsoo.controller.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.object.CommentNotification;
import ir.rasen.charsoo.view.activity.ActivityCommentsNotifications;
import ir.rasen.charsoo.view.interface_m.IWebservice;


/**
 * Created by android on 12/29/2014.
 */
public class AlarmReciever extends BroadcastReceiver implements IWebservice {

    Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
//        if (LoginInfo.getUserId(context) != 0)
//            new GetLastCommentNotification(context, LoginInfo.getUserId(context), AlarmReciever.this).executeWithNewSolution();;
    }

    @Override
    public void getResult(int reqCode,Object result) {
        if (LoginInfo.getInstance().getUserUniqueId().equals(""))
            return;
        if (result instanceof CommentNotification) {
            CommentNotification commentNotification = (CommentNotification) result;
//            if (CommentNotification.isDisplayed(context, commentNotification.uniqueId))
//                return;

            Intent intent = new Intent(context, ActivityCommentsNotifications.class);
            intent.putExtra(Params.NOTIFICATION, true);
            MyNotification.displayNotificationCustomView(context, intent, commentNotification.getCommentNotificationContentView(context), R.drawable.ic_app);
        }
    }

    @Override
    public void getError(int reqCode,Integer errorCode,String callerStringID) {

    }


}
