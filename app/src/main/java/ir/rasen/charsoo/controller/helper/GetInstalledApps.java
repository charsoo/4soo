package ir.rasen.charsoo.controller.helper;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.object.GlobalContactPassingClass;
import ir.rasen.charsoo.controller.object.PackageInfoCustom;
import ir.rasen.charsoo.view.interface_m.IGetInstalledAppsListener;

/**
 * Created by hossein-pc on 6/23/2015.
 */
public class GetInstalledApps extends AsyncTask<Void,Void,Void> {

    Context context;
    IGetInstalledAppsListener delegate;
    ArrayList<PackageInfoCustom> applicationList;
    public static boolean isRunning=false;
    private static ArrayList<IGetInstalledAppsListener> delegateList;

    public GetInstalledApps(Context c,IGetInstalledAppsListener delegate){
        this.context=c;
        this.delegate=delegate;
        applicationList=new ArrayList<>();
    }

    public static void addDelegate(IGetInstalledAppsListener d){
        if (delegateList==null)
            delegateList=new ArrayList<>();
        delegateList.add(d);
    }

    @Override
    protected Void doInBackground(Void... params) {

        isRunning=true;


        List<PackageInfo> packs = context.getPackageManager().getInstalledPackages(0);
        for(int i=0;i<packs.size();i++) {
            PackageInfo p = packs.get(i);
                /*if ((!getSysPackages) && (p.versionName == null)) {
                    continue ;
                }*/
            PackageInfoCustom newInfo = new PackageInfoCustom();
            newInfo.appname = p.applicationInfo.loadLabel(context.getPackageManager()).toString();
            newInfo.pname = p.packageName;
            newInfo.versionName = p.versionName;
            newInfo.versionCode = p.versionCode;
            newInfo.icon = Image_M.drawableToBitmap(p.applicationInfo.loadIcon(context.getPackageManager()));

            if (newInfo.appname.equalsIgnoreCase(Params.WHATSAPP))
            {
                newInfo.appname=Params.WHATSAPP;
                applicationList.add(newInfo);
            }
            else if (newInfo.appname.equalsIgnoreCase(Params.LINE))
            {
                newInfo.appname=Params.LINE;
                applicationList.add(newInfo);
            }
            else if (newInfo.appname.equalsIgnoreCase(Params.VIBER))
            {
                newInfo.appname=Params.VIBER;
                applicationList.add(newInfo);
            }
            else if (newInfo.appname.equalsIgnoreCase(Params.TELEGRAM))
            {
                newInfo.appname=Params.TELEGRAM;
                applicationList.add(newInfo);
            }

        }
        PackageInfoCustom customPackage=new PackageInfoCustom();
        customPackage.appname= Params.SHARE_APP;
        customPackage.icon= BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_share_black_48dp);
        applicationList.add(customPackage);
        customPackage=new PackageInfoCustom();
        customPackage.appname=Params.EMAIL_APP;
        customPackage.icon=BitmapFactory.decodeResource(context.getResources(),R.drawable.ic_email_blue);
        applicationList.add(customPackage);
        if (GlobalContactPassingClass.getInstance().getAppList()==null)
            GlobalContactPassingClass.getInstance().setAppList(applicationList);
        return null;
    }

    @Override
    protected void onPostExecute(Void result){
        if (delegateList!=null){
            for (int i = 0; i < delegateList.size(); i++) {
                try
                {
                    delegateList.get(i).setAppResults();
                }catch (Exception e){}
            }
        }
        delegate.setAppResults();
        isRunning=false;
//            if (res!=null) {
//                for (PInfo p : res){
//                    p.prettyPrint();
//                }
//            }

//            for (String s: applications.keySet()){
//                View view=LayoutInflater.from(context).inflate(R.layout.item_application_to_invite,null);
//                Holder holder=new Holder();
//                holder.imageViewAppIcone=(RoundedSquareImageView) view.findViewById(R.uniqueId.imageView_ApplicationIcone);
//                holder.textViewAppName=(TextViewFont) view.findViewById(R.uniqueId.textViewFont_ApplicationName);
//                holder.textViewAppName.setText(s);
//
//                holder.imageViewAppIcone.setImageBitmap(drawableToBitmap(applications.get(s).icon));
//                holder.TAG=s;
//            }
    }
}