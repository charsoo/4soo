package ir.rasen.charsoo.controller.helper;

import java.util.ArrayList;

import ir.rasen.charsoo.controller.object.User;

public class LoginInfo{

    private String userUniqueId;
    public  String userAccessToken;
    private String userProfilePictureUniqueId;
    public  String userIdentifier;
    public  String username;
    public  ArrayList<User.UserBusinesses> userBusinesses;
    public  Permission userPermissions;
    public  String userUniqueName;
    public  String userEmail;

    public int userReceivedFriendRequestCount;


    private static LoginInfo mLoginInfo;


    public static LoginInfo getInstance(){
        if(mLoginInfo == null)
            mLoginInfo = new LoginInfo();

        return mLoginInfo;
    }




    public LoginInfo(){
        userUniqueId = "";
        userAccessToken = "";
        userProfilePictureUniqueId = "";
        userIdentifier = "";
        username = "";
        userBusinesses = new ArrayList<>();
        userPermissions = new Permission(false, false, false);
        userReceivedFriendRequestCount = 0;
        userUniqueName = "";
        userEmail = "";
    }


    public static boolean isHaveFriendRequest(int requestCounts){
        if(requestCounts>0)
            return true;
        else
            return false;
    }


    public String getUserUniqueId(){
        return userUniqueId;
    }
    public void setUniqueUserId(String userUniqueId){
        if(userUniqueId!=null && !userUniqueId.equals("") && !(userUniqueId.length()<32))
            this.userUniqueId = userUniqueId;
    }

    public String getUserProfilePictureUniqueId(){
        return userProfilePictureUniqueId;
    }

    public void setUserProfilePictureUniqueId(String userProfilePictureUniqueId){
        if(userProfilePictureUniqueId!=null && !userProfilePictureUniqueId.equals("") && !(userProfilePictureUniqueId.length()<36))
            this.userProfilePictureUniqueId = userProfilePictureUniqueId;

    }

    public void setUserAccessToken(String userAccessToken){
        if(userAccessToken!=null && !userAccessToken.equals("") && !(userAccessToken.length()<32))
            this.userAccessToken = userAccessToken;
    }
}
