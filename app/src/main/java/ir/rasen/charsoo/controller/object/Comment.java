package ir.rasen.charsoo.controller.object;

import android.content.Context;
import android.content.Intent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.view.activity.ActivityComments;


/**
 * Created by android on 12/17/2014.
 */
public class Comment {
    public String uniqueId;
    public String businessUniqueId;
    public String postUniqueId;
    public String ownerUniqueId;
    public String username;
    public String userProfilePictureUniqueId;
    public String text;
    public boolean isUserSendedComment;


    public Comment(String ownerUniqueId, String userIdentifier, String text) {
        this.ownerUniqueId = ownerUniqueId;
        this.username = userIdentifier;
        this.text = text;
    }

    public Comment(String ownerUniqueId, String userIdentifier, String userProfilePictureUniqueId, String commentText) {
        this.ownerUniqueId = ownerUniqueId;
        this.username = userIdentifier;
        this.userProfilePictureUniqueId = userProfilePictureUniqueId;
        this.text = commentText;
    }

    public Comment() {

    }

    public static ArrayList<Comment> getFromJSONArray(JSONArray jsonArray) throws Exception {
        ArrayList<Comment> comments = new ArrayList<Comment>();
        for (int j = 0; j < jsonArray.length(); j++) {
            JSONObject jsonObjectComment = jsonArray.getJSONObject(j);

            Comment comment = new Comment();
            comment.uniqueId = jsonObjectComment.getString(Params.COMMENT_ID);
            comment.username = jsonObjectComment.getString(Params.COMMENT_OWNER_TITLE);
            comment.ownerUniqueId = jsonObjectComment.getString(Params.COMMENT_OWNER_ID);
            comment.userProfilePictureUniqueId = jsonObjectComment.getString(Params.COMMENT_USER_PROFILE_PICTURE_UNIQUE_ID);
            comment.text = jsonObjectComment.getString(Params.COMMENT_TEXT);
            comment.isUserSendedComment = jsonObjectComment.getBoolean(Params.COMMENT_IS_USER);

            comments.add(comment);
        }

        return comments;
    }


    public static void openCommentActivity(Context context, boolean isOwner, String postUniqueId,
                                           String ownerBusinessUniqueId) {
        Intent intent = new Intent(context, ActivityComments.class);
        intent.putExtra(Params.POST_ID_INT, postUniqueId);
        intent.putExtra(Params.POST_OWNER_BUSINESS_ID, ownerBusinessUniqueId);
        intent.putExtra(Params.IS_OWNER, isOwner);
        intent.putExtra(Params.NEW_COMMENT_DIRECTLY, false);
        context.startActivity(intent);
    }
}
