package ir.rasen.charsoo.controller.helper;

import android.content.Context;

import java.util.ArrayList;
import java.util.Stack;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.object.State;
import ir.rasen.charsoo.view.adapter.AdapterWorkDays;

/**
 * Created by Rasen_iman on 9/3/2015.
 */
public class WorkDays {
    public ArrayList<DayTime> mDayTimes;

    public WorkDays(ArrayList<DayTime> dayTimes){
        mDayTimes = dayTimes;
    }
    public WorkDays(){}
    public void setDayTimes(ArrayList<DayTime> dayTimes){
        mDayTimes = dayTimes;
    }
    public ArrayList<DayTime> getDayTimes(){
        return mDayTimes;
    }
    public boolean isAllDaysSetted(){
        for(DayTime dt : mDayTimes){
            if(
                    dt.mOpenTime.mHourTime <=-1 || dt.mOpenTime.mMinuteTime <= -1 ||
                    dt.mCloseTime.mHourTime <=-1 || dt.mCloseTime.mMinuteTime <= -1)
                return false;
        }

        return true;
    }

    public static boolean areWorktimesDifferent(WorkDays workDays1, WorkDays workDays2) {


        if(workDays2 == null)
            return false;

        ArrayList<DayTime> days1 = workDays1.getDayTimes();
        ArrayList<DayTime> days2 = workDays2.getDayTimes();

        for(int i=0;i<days1.size();i++){
            if(days1.get(i).mCurrentDay != days2.get(i).mCurrentDay)
                return true;

            if(days1.get(i).mOpenTime.mHourTime != days2.get(i).mOpenTime.mHourTime)
                return true;
            if(days1.get(i).mOpenTime.mMinuteTime != days2.get(i).mOpenTime.mMinuteTime)
                return true;

            if(days1.get(i).mCloseTime.mHourTime != days2.get(i).mCloseTime.mHourTime)
                return true;
            if(days1.get(i).mCloseTime.mMinuteTime != days2.get(i).mCloseTime.mMinuteTime)
                return true;
        }
        return false;
    }
    public static String createWorkDaysStr(Context ctx,ArrayList<DayTime> days){
        if(days==null || days.size()==0)
            return "";

        StringBuilder sb = new StringBuilder();
        for(DayTime d : days){
            if(d.isSet()) {
                sb.append(d.getDayStr(ctx));
                sb.append(" , ");
            }
        }
        return sb.toString();
    }

    public class Time{
        public int mHourTime = -1;
        public int mMinuteTime = -1;
        public boolean isSet(){
            if(mHourTime<0 || mMinuteTime<0)
                return false;
            return true;
        }

        public Time(int h,int m){
            mHourTime = h;
            mMinuteTime = m;
        }
    }
    public class DayTime{
        public int mCurrentDay = -1;
        public Time mOpenTime,mCloseTime;
        public DayTime(int day,Time openTime,Time closeTime){
            mCurrentDay = day;
            this.mOpenTime = openTime;
            this.mCloseTime = closeTime;
        }

        public boolean isSet(){
            if(mCurrentDay >= 0 &&
                    mOpenTime.mHourTime >= 0 && mOpenTime.mMinuteTime >=0 &&
                    mCloseTime.mHourTime>= 0 && mCloseTime.mMinuteTime>=0)
                return true;
            else
                return false;
        }
        public void resetTimes(){
            mOpenTime.mHourTime = -1;
            mOpenTime.mMinuteTime = -1;
            mCloseTime.mHourTime = -1;
            mCloseTime.mMinuteTime = -1;
        }

        public String getDayStr(Context ctx){
            switch(mCurrentDay){
                case 0 :
                    return ctx.getResources().getString(R.string.Sat);
                case 1 :
                    return ctx.getResources().getString(R.string.Sun);
                case 2 :
                    return ctx.getResources().getString(R.string.Mon);
                case 3 :
                    return ctx.getResources().getString(R.string.Tue);
                case 4 :
                    return ctx.getResources().getString(R.string.Wed);
                case 5 :
                    return ctx.getResources().getString(R.string.Thr);
                case 6 :
                    return ctx.getResources().getString(R.string.Fri);
            }
            return "";
        }

        public boolean isTimeEqualsTo(DayTime dt){
            if(mOpenTime.mHourTime == dt.mOpenTime.mHourTime &&
                    mOpenTime.mMinuteTime == dt.mOpenTime.mMinuteTime &&
                    mCloseTime.mHourTime == dt.mCloseTime.mHourTime &&
                    mCloseTime.mMinuteTime == dt.mCloseTime.mMinuteTime)
                return true;
            else return  false;
        }
    }
//  baraye etminan felan pak nakonid :-P  :-@
//    public static ArrayList<WorkDaysGroup> convertToWorkDaysGroups(WorkDays workDays){
//        boolean[] addNums = new boolean[7];
//
//        for(int i=0;i<addNums.length;i++)
//            addNums[i] = false;
//
//
//        ArrayList<WorkDaysGroup> wdgs = new ArrayList<>();
//
//        ArrayList<DayTime> dayTimes =
//                (ArrayList<DayTime>) workDays.mDayTimes.clone();
//
//
//        for(int i=0;i<dayTimes.size()-1;i++){
//            if(!dayTimes.get(i).isSet() || addNums[i])
//                continue;
//
//            WorkDaysGroup workDaysGroup = new WorkDaysGroup();
//
//            workDaysGroup.mDayTimes.add(dayTimes.get(i));
//            addNums[i] = true;
//            for(int j=i+1;j<dayTimes.size();j++){
//                if(dayTimes.get(i).isTimeEqualsTo(dayTimes.get(j)) && !addNums[j]) {
//                    workDaysGroup.mDayTimes.add(dayTimes.get(j));
//                    addNums[j] = true;
//                }
//            }
//            if(workDaysGroup.mDayTimes.size() != 0)
//                wdgs.add(workDaysGroup);
//        }
//
//        return wdgs;
//    }
    /////////////////////////////////////////////////////////////////////////////////////
    public static ArrayList<WorkDaysGroup> convertToWorkDaysGroup(WorkDays workDays){
        ArrayList<WorkDaysGroup> wdgs = new ArrayList<>();
        ArrayList<DayTime> dayTimes = (ArrayList<DayTime>) workDays.mDayTimes.clone();

        for(int i=0;i<dayTimes.size();i++){
            if(!dayTimes.get(i).isSet())
                continue;
            WorkDaysGroup workDaysGroup = new WorkDaysGroup();
            workDaysGroup.mDayTimes.add(dayTimes.get(i));
            wdgs.add(workDaysGroup);
        }

        return wdgs;
    }
    /////////////////////////////////////////////////////////////////////////////////////
    public static class WorkDaysGroup{
        public ArrayList<WorkDays.DayTime> mDayTimes;

        public WorkDaysGroup(){
            mDayTimes = new ArrayList<>();
        }

        public String getDaysStr(Context ctx){
            StringBuilder sb = new StringBuilder();
            for(WorkDays.DayTime dt : mDayTimes) {
                sb.append(dt.getDayStr(ctx));
                sb.append(" , ");
            }
            return sb.toString();
        }
        public String getOpenCloseTimeStr(Context ctx){
            if(mDayTimes.size()<=0)
            for(int i=0;i<7;i++){
                mDayTimes.get(i).mCloseTime.mHourTime=mDayTimes.get(i).mCloseTime.mMinuteTime=0;
                mDayTimes.get(i).mOpenTime.mHourTime= mDayTimes.get(i).mOpenTime.mMinuteTime=0;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(ctx.getResources().getString(R.string.work_time_open));
            sb.append(" ");
            sb.append(mDayTimes.get(0).mOpenTime.mHourTime + ":" + mDayTimes.get(0).mOpenTime.mMinuteTime);
            sb.append(" ");
            sb.append(ctx.getResources().getString(R.string.until));
            sb.append(" ");
            sb.append(mDayTimes.get(0).mCloseTime.mHourTime + ":" + mDayTimes.get(0).mCloseTime.mMinuteTime);

            return sb.toString();
        }
    }
}
