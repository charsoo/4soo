package ir.rasen.charsoo.controller.helper;

public class Params {

    /**
     * Created by 'SINA KH'.
     */

    public static int USER_USERNAME_MIN_LENGTH = 4;
    public static int USER_USERNAME_MAX_LENGTH = 15;
    public static String USER_USERNAME_VALIDATION = "[0-9a-zA-Z._]{"+USER_USERNAME_MIN_LENGTH+","+USER_USERNAME_MAX_LENGTH+"}";

    public static int BUSINESS_STRING_ID_MIN_LENGTH = 4;
    public static int BUSINESS_STRING_ID_MAX_LENGTH = 25;
    public static String BUSINESS_STRING_ID_VALIDATION = "[0-9a-zA-Z._]{"+BUSINESS_STRING_ID_MIN_LENGTH+","+BUSINESS_STRING_ID_MAX_LENGTH+"}";

    public static int USER_NAME_MIN_LENGTH = 3;
    public static int USER_NAME_MAX_LENGTH = 25;
    public static String USER_NAME_VALIDATION = "[0-9a-zA-Z._ \\u0600-\\u06FF\\uFB8A\\u067E\\u0686\\u06AF\\u0020]{"+USER_NAME_MIN_LENGTH+","+USER_NAME_MAX_LENGTH+"}";

    public static int TITLE_MIN_LENGTH = 3;
    public static int TITLE_MAX_LENGTH = 50;
    public static String TITLE_VALIDATION = "[0-9a-zA-Z._ \\u0600-\\u06FF\\uFB8A\\u067E\\u0686\\u06AF\\u0020]{"+TITLE_MIN_LENGTH+","+TITLE_MAX_LENGTH+"}";


    public static String CITY_VALIDATION = "[\\u0600-\\u06FF\\uFB8A\\u067E\\u0686\\u06AF]*";

    public static int ABOUT_ME_MIN_LENGTH = 3;
    public static int ABOUT_ME_MAX_LENGTH = 100;

    public static int PASSWORD_MIN_LENGTH = 5;
    public static int PASSWORD_MAX_LENGTH=20;
    public static String PASSWORD_MATCH_PATTERN="[0-9a-zA-Z_\\u0600-\\u06FF\\uFB8A\\u067E\\u0686\\u06AF]{"+ PASSWORD_MIN_LENGTH +","+PASSWORD_MAX_LENGTH+"}";

    public static int COMMENT_TEXT_MIN_LENGTH = 1;
    public static int COMMENT_TEXT_MAX_LENGTH = 255;

    public static int ADDRESS_TEXT_MIN_LENGTH=15;
    public static int ADDRESS_TEXT_MAX_LENGTH=150;

    public static int HASHTAG_MIN_LENGTH = 2;
    public static String TEXT_HASHTAG_VALIDATION = "[#]+[0-9a-zA-Z_\\u0600-\\u06FF\\uFB8A\\u067E\\u0686\\u06AF]{"+HASHTAG_MIN_LENGTH+",}";

    public static String BUSINESS = "business";

    public static String BUSINESS_OWNER = "business_owner";

    public static String NOTIFICATION = "notification";
    public static String NEW_FIREND= "newFriend";
    public static final int FREEZ = 250;
    public static final String NEW_COMMENT_DIRECTLY = "newCommmentDirectly";

    /**
     * Created by android on 12/15/2014.
     */
    public static String EMAIL = "Email";
    public static String PASSWORD_NEW = "PasswordNew";
    public static String USER_UNIQUE_ID_STR = "UserId";
    public static String COMMENT_OWNER_ID = "CommntOwnerId";
    public static String USER_STR_ID="UserId";
    public static String USER_FULL_NAME="UserFullName";
    public static String User_Uniq_Name = "UserUniqName";
    public static String NAME = "Name";
    public static String ABOUT_ME = "AboutMe";
    public static String COVER_PICTURE_ID = "CoverPictureId";
    public static String FRIEND_REQUEST_NUMBER = "FriendRequestNumber";
    public static String REVIEWS_NUMBER = "ReviewsNumber";
    public static String FOLLOWED_BUSINESSES_NUMBER = "FollowedBusinessNumber";
    public static String FRIENDS_NUMBER = "FriendsNumber";
    public static String PERMISSION = "Permission";
    public static String FOLLOWED_BUSINESS = "FollowedBusinesses";
    public static String FRIENDS = "Friends";
    public static String REVIEWS = "Reviews";
    public static String FRIENDSHIP_RELATION_STATUS_CODE = "FriendshipRelationStatusCode";
    public static String IS_USER_SEEN="IsUserSeen";
    public static String REVIEW_ID="ReviewId";


    public static String POST_ID_INT = "PostId";
    public static String POST_TYPE= "PostType";
    public static String POST_OWNER_BUSINESS_ID= "PostOwnerBusinessId";
    public static String BUSINESS_ID_STRING = "BusinessId";
    public static String BUSINESS_IDENTIFIER= "BusinessIdentifier";
    public static String BUSINESS_USERNAME_STRING = "BusinessUserName";
    public static String LATITUDE= "Latitude";
    public static String LONGITUDE= "Longitude";
    public static String PHONE= "Phone";
    public static String ADDRESS= "Address";
    public static String MOBILE= "Mobile";
    public static String WORK_TIME_OPEN= "WorkTimeOpen";
    public static String WORK_TIME_CLOSE= "WorkTimeClose";
    public static String COMMENT_ID= "CommentId";

    public static String RATE= "Rate";
    public static String REVIEW= "Review";
    public static String STATE= "State";
    public static String CITY= "City";
    public static String SUCCESS = "SuccessStatus";
    public static String RESULT = "Result";
    public static String ERROR = "Error";
    public static String USER_PICUTE = "UserPicture";
    public static String USER_PICTURE_ID="UserPictureId";
    public static String BUSINESS_PICUTE_ID = "BusinessPictureId";
    public static String INTERVAL_TIME = "IntervalTime";
    public static String ERROR_CODE = "ErrorCode";
    public static String ACCESS_TOKEN = "AccessToken";
    public static String IMAGE = "Image";
    public static String BUSINESSES = "Businesses";
    public static String PROFILE_PICTURE_UNIQUE_ID = "ProfilePictureId";
    public static String WEBSITE = "Website";
    public static String IS_EDITTING = "isEditting";
    public static String VISITED_USER_UNIQUE_ID = "visitedUserId";
    public static String HAS_REQUEST = "hasRequest";
    public static String IS_OWNER = "isOwner";
    public static String UPATE_TIME_LINE = "updateTimeLine";
    public static String UPDATE_TIME_LINE_POST_LAST_THREE_COMMENTS = "updateTimeLinePostLastThreeComments";
    public static String CANCEL_USER_SHARE_POST = "cancelUserSharePost";
    public static String UPDATE_TIME_LINE_TYPE = "updateTimeLineType";
    public static String REMOVE_REQUEST_ANNOUNCEMENT = "removeRequestAnnouncement";
    public static String UPDATE_USER_PROFILE_PCITURE = "updateUserProfilePicture";
    public static final String UPATE_TIME_LINE_TYPE_SHARE = "updateTimeLineTypeShare";
    public static final String UPATE_TIME_LINE_TYPE_CANCEL_SHARE = "updateTimeLineTypeCancelShare";
    public static final String DELETE_BUSINESS= "deleteBusiness";
    public static final String MATCHED_FIELD_DATA_STR ="MatchedFieldData";
    public static final String MATCHED_FIELD_TYPE_STR="MatchedFieldType";


    public static final String COMMENT_OWNER_TITLE = "CommntOwnerTitle";
    public static final String COMMENT_TEXT = "Text";
    public static final String COMMENT_USER_PROFILE_PICTURE_UNIQUE_ID = "ProfilePictureId";
    public static final String COMMENT_IS_USER = "IsUser";




    //Actions
    public static int ACTION_DELETE_BUSIENSS = 1003;
    public static int ACTION_WORK_TIME = 1004;
    public static int ACTION_REGISTER_BUSINESS = 1005;
    public static int ACTION_ADD_POST = 1006;
    public static int ACTION_CHOOSE_LOCATION = 1008;
    public static int ACTION_EDIT_BUSINESS = 1009;
    public static int ACTION_ACTIVITY_POST = 1010;
    public static int ACTION_EDIT_POST = 1011;
    public static int ACTION_NEW_FRIENDS = 1012;






    /**
     * Created by MHFathi
     */

    public static final String BUSINESS_POST_FOR_TIMELINE="businessPost";
    public static final String FRIEND_SHARED_POST_FOR_TIMELINE="friendSharedPost";
    public static final String FRIEND_FOLLOW_ANNOUNCE_FOR_TIMELINE="friendFollowAnnounce";
    public static final String FRIEND_REVIEW_ANNOUNCE_FOR_TIMELINE="friendReviewAnnounce";

    public static final String HAS_REMAINIG_FRIEND_REQUESTS="hasRemainingFriendRequests";
    public static final String FULL_NAME= "fullName";
    public static final String STRING_IDENTIFIER="stringIdentifier";

    public static final String WHATSAPP = "WhatsApp";
    public static final String LINE="LINE";
    public static final String TELEGRAM="Telegram";
    public static final String VIBER="Viber";
    public static final String SHARE_APP ="Share";
    public static final String EMAIL_APP ="Email";


    public static final String FIRST_TAB_TITLE="firstTabTitle";
    public static final String SECOND_TAB_TITLE="secondTabTitle";
    public static final String THIRD_TAB_TITLE="thirdTabTitle";
    public static final String SMS_MESSAGE="smsMessage";
    public static final String EMAIL_MESSAGE="emailMessage";
    public static final String USER_UNIQUE_ID = "userIntId";
    public static final String IS_BUSINESS_PROMOTION="isBusinessPromotion";

    public static final String CHARSOO_TEMP_FILE_SUBDIRECTORY="/Charsoo/temp/";

    public static final int IMAGE_MAX_WIDTH_HEIGHT_PIX=640;
    public static final int RETRY_COUNT_ON_CONNECTION_FAILED=10;
    public static final int PROFILE_ROUNDED_THUMBNAIL_DIP=38;


    public static final String USER_EMAIL_PREFERENCES = "PREF_EMAIL";
    public static final String USER_PASSWORD_PREFERENCES = "PREF_PASSWORD";

    public static final String Gallery_Id = "GalleryID";

    public static final String BUSINESS_UNIQUE_ID = "BusinessUniqueId";


    public static final String OPENING_MODE = "OpeningMode";
}
