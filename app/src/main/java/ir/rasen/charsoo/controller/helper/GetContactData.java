package ir.rasen.charsoo.controller.helper;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.TypedValue;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.concurrent.ExecutionException;

import ir.rasen.charsoo.controller.object.ContactEntry;
import ir.rasen.charsoo.controller.object.GlobalContactPassingClass;
import ir.rasen.charsoo.view.interface_m.IGetContactListener;

/**
 * Created by hossein-pc on 6/23/2015.!
 */

public class GetContactData extends AsyncTask<Void,Void,Void> {
    Hashtable<String,ArrayList<ContactEntry>> noneCharsooEmailContacts;
    Hashtable<String,ArrayList<ContactEntry>> noneCharsooPhoneContacts;
    Context context;
    IGetContactListener delegate;
    ColorGenerator generator ;
    TextDrawable drawable2;
    int drawableWidthHeight;
    public static boolean isRunning=false;
    private static ArrayList<IGetContactListener> delegateList;


    public GetContactData(Context c,IGetContactListener delegate){
        this.delegate=delegate;
        context=c;
        generator= ColorGenerator.MATERIAL;
        drawableWidthHeight=getSizeInPixelFromDp(Params.PROFILE_ROUNDED_THUMBNAIL_DIP);
    }

    public static void addDelegate(IGetContactListener d){
        if (delegateList==null)
            delegateList=new ArrayList<>();
        delegateList.add(d);
    }

    @Override
    protected Void doInBackground(Void... params) {
        isRunning=true;
        noneCharsooEmailContacts=getEmailContacts();
        noneCharsooPhoneContacts=getPhoneNumberContacts();
        if (GlobalContactPassingClass.getInstance().getEmailContacts()==null)
            GlobalContactPassingClass.getInstance().setEmailContacts(noneCharsooEmailContacts);
        if (GlobalContactPassingClass.getInstance().getNumberContacts()==null){
            GlobalContactPassingClass.getInstance().setNumberContacts(noneCharsooPhoneContacts);
        }
        return null;
    }


    @Override
    protected void onPostExecute(Void result) {
//        delegate.getPhoneContacts(noneCharsooEmailContacts, noneCharsooPhoneContacts);
        if (delegateList!=null){
            for (int i = 0; i < delegateList.size(); i++) {
                try
                {
                    delegateList.get(i).getPhoneContacts();
                }catch (Exception e){}
            }
        }
        delegate.getPhoneContacts();
        isRunning=false;
    }


    private Hashtable<String,ArrayList<ContactEntry>> getPhoneNumberContacts(){
        Hashtable<String,ArrayList<ContactEntry>> result=new Hashtable<>();
        ArrayList<Integer> arr=null;
        Cursor phones = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        if (phones.getCount()>0) {
            phones.moveToFirst();
            do {
                ContactEntry contactEntry = new ContactEntry();
                contactEntry.type = ContactEntry.ContactType.PhoneNumber;
                contactEntry.fullName = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
//                contactEntry.contactData = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                String ss= phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//
                String noSpaceString="";
                for (int i = 0; i < ss.length(); i++) {
                    if (ss.charAt(i) != ' ')
                    {
                        noSpaceString+=ss.charAt(i);
                    }
                }
//                ss.replaceAll("[^0-9]", ""); // this doesent work !!!
//                ss.replaceAll("\\s+",""); //  this doesent work too!!!
                contactEntry.contactData=noSpaceString;
                String image_uri = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
                try {
                    arr.add(1);
                    Bitmap bitmap = MediaStore.Images.Media
                            .getBitmap(context.getContentResolver(),
                                    Uri.parse(image_uri));
                    contactEntry.contactPhoto = bitmap.copy(Bitmap.Config.ARGB_8888, true);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    String s = contactEntry.fullName.substring(0, 1).toUpperCase();
                    if (Character.isDigit(s.charAt(0))) {
                        s = null;
                        s = "N";
                    }
                    try {
                        if (contactEntry.fullName.contains(" ")) {
                            s += contactEntry.fullName.substring(contactEntry.fullName.indexOf(" ") + 1, contactEntry.fullName.indexOf(" ") + 2).toUpperCase();
                        }
                    } catch (Exception ee) {
                    }

                    contactEntry.contactPhoto = Image_M.drawableToBitmap(TextDrawable.builder()
                            .beginConfig()
                            .width(drawableWidthHeight)  // width in px
                            .height(drawableWidthHeight) // height in px
                            .endConfig().buildRound(s, generator.getColor(s)));
                }
                if (Validation.checkNumberIfIsMobile(contactEntry.contactData).isValid()) {
                    contactEntry.contactData=ContactEntry.getStandardPhoneNumber(contactEntry.contactData);
                    if (result.containsKey(contactEntry.fullName)) {
                        boolean addIt = true;
                        for (int i = 0; i < result.get(contactEntry.fullName).size(); i++) {
                            if (contactEntry.contactData.equals(result.get(contactEntry.fullName).get(i).contactData))
                                addIt = false;
                        }
                        if (addIt)
                            result.get(contactEntry.fullName).add(contactEntry);
                    } else
                        result.put(contactEntry.fullName, new ArrayList<>(Arrays.asList(contactEntry)));
                }
            } while (phones.moveToNext());
        }
        phones.close();
        return result;
    }

    private Hashtable<String,ArrayList<ContactEntry>> getEmailContacts() {
        Hashtable<String, ArrayList<ContactEntry>> result = new Hashtable<>();
        Cursor phones = context.getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, null, null, null);
        ArrayList<Integer> arr=null;
        if (phones.getCount() > 0) {
            phones.moveToFirst();
            do {
                ContactEntry contactEntry = new ContactEntry();
                contactEntry.type = ContactEntry.ContactType.Email;
                contactEntry.fullName = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                contactEntry.contactData = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS)).toLowerCase();
                String image_uri = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
                try {

                    arr.add(1);
                    Bitmap bitmap = MediaStore.Images.Media
                            .getBitmap(context.getContentResolver(),
                                    Uri.parse(image_uri));
                    contactEntry.contactPhoto = bitmap.copy(Bitmap.Config.ARGB_8888, true);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    String s = contactEntry.fullName.substring(0, 1).toUpperCase();
                    if (Character.isDigit(s.charAt(0))) {
                        s = null;
                        s = "N";
                    }
                    try {
                        if (contactEntry.fullName.contains(" ")) {
                            s += contactEntry.fullName.substring(contactEntry.fullName.indexOf(" ") + 1, contactEntry.fullName.indexOf(" ") + 2).toUpperCase();
                        }
                    } catch (Exception ee) {
                    }

                    contactEntry.contactPhoto = Image_M.drawableToBitmap(TextDrawable.builder()
                            .beginConfig()
                            .width(drawableWidthHeight)  // width in px
                            .height(drawableWidthHeight) // height in px
                            .endConfig().buildRound(s, generator.getColor(s)));
                }

                if (Validation.validateEmail(context, contactEntry.contactData).isValid()) {
                    if (result.containsKey(contactEntry.fullName)) {
                        boolean addIt = true;
                        for (int i = 0; i < result.get(contactEntry.fullName).size(); i++) {
                            if (contactEntry.contactData.equals(result.get(contactEntry.fullName).get(i).contactData))
                                addIt = false;
                        }
                        if (addIt)
                            result.get(contactEntry.fullName).add(contactEntry);
                    } else
                        result.put(contactEntry.fullName, new ArrayList<>(Arrays.asList(contactEntry)));
                }
            } while (phones.moveToNext());
        }
        phones.close();
        return result;
    }




    public int getSizeInPixelFromDp(int dpToConvert){
        Resources r = context.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpToConvert, r.getDisplayMetrics());
        return (int) px;
    }
}