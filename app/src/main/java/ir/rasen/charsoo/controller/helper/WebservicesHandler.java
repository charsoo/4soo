package ir.rasen.charsoo.controller.helper;

import android.os.AsyncTask;
import android.os.Build;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 3/16/2015.
 */
public class WebservicesHandler {
    public  enum Webservices {NONE,LOGIN,FORGET_PASSWORD,GET_USER_HOME_INFO,GET_USER_SHARED_POSTS,GET_BUSINESS_CATEGORY,GET_BUSINESS_SUB_CATEGORY,ADD_POST,GET_POST,UPDATE_POST};

    public  Webservices currentWebservice = Webservices.NONE;

    public static List<String> runningWebservicesTag;

    public static ServerAnswer getNetworkConnectionError(){
        ServerAnswer serverAnswer = new ServerAnswer();
        serverAnswer.setSuccessStatus(false);
        serverAnswer.setResult(null);
        serverAnswer.setErrorCode(ServerAnswer.NETWORK_CONNECTION_ERROR);
        return serverAnswer;
    }

    public static void addWebService(String TAG){
        if(runningWebservicesTag == null)
            runningWebservicesTag = new ArrayList<String>();

        if(!runningWebservicesTag.contains(TAG))
            runningWebservicesTag.add(TAG);
    }

    public static void removeWebService(String TAG){
        if(runningWebservicesTag == null)
            runningWebservicesTag = new ArrayList<String>();
        if(runningWebservicesTag.contains(TAG)) {
            for(int i=0;i<runningWebservicesTag.size();i++){
                if(runningWebservicesTag.get(i).equals(TAG)) {
                    runningWebservicesTag.remove(i);
                    break;
                }
            }
        }
    }


//    public static void runAsyncTaskByNewSolution(AsyncTask myTask){
//        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
//            myTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//        else
//            myTask.executeWithNewSolution();;
//    }
}
