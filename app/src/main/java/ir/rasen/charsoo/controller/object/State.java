package ir.rasen.charsoo.controller.object;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ir.rasen.charsoo.R;

/**
 * Created by Rasen_iman on 8/9/2015.
 */
public class State {


    public String stateUniqueId;
    public String stateName;


    public State(){
        stateUniqueId = "-1";
        stateName = "";
    }

    public State(String stateUniqueId,String name){
        this.stateUniqueId = stateUniqueId;
        this.stateName = name;
    }
    public void  Recycle(){
     try {
         this.finalize();
     } catch (Throwable throwable) {
         throwable.printStackTrace();
     }
 }
    public static ArrayList<String> getStatesId(List<State> states){
        ArrayList<String> tempIds = new ArrayList<>();

        for(State state : states){
            tempIds.add(state.stateUniqueId);
        }

        return tempIds;
    }

    public static ArrayList<String> getStatesName(List<State> states){
        ArrayList<String> tempNames = new ArrayList<>();

        for(State state : states){
            tempNames.add(state.stateName);
        }

        return tempNames;
    }


    public static ArrayList<State> addDefaultStateToStartOfList(Context ctx,ArrayList<State> states){
        String defaultStateName = ctx.getResources().getString(R.string.state);
        if(states.get(0).stateName.equals(defaultStateName) || Integer.parseInt(states.get(0).stateUniqueId) == -1)
            return states;

        ArrayList<State> tempStates = new ArrayList<>();
        tempStates.add(new State("-1", defaultStateName));
        tempStates.addAll(states);

        return tempStates;
    }
}
