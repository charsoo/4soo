package ir.rasen.charsoo.controller.helper;

import java.util.ArrayList;

import ir.rasen.charsoo.controller.object.Post;

/**
 * Created by android on 12/16/2014.
 */

public class SearchItemPost {
    public String postId;
    public String postPictureId;
    public String postPicture;
    public String businessIntId;

    public SearchItemPost(Post post){
        this.postId =  post.uniqueId;
        this.postPictureId = post.pictureUniqueId;
        this.postPicture = post.picture;
        businessIntId=post.businessUniqueId;
    }

    public SearchItemPost( String postId, String postPictureId,String postPicture){
        this.postId =  postId;
        this.postPictureId = postPictureId;
        this.postPicture = postPicture;

    }

    public static ArrayList<SearchItemPost> getItems(ArrayList<Post> posts){
        ArrayList<SearchItemPost> result = new ArrayList<>();
        for (Post post:posts){
            result.add(new SearchItemPost(post));
        }

        return result;
    }

}
