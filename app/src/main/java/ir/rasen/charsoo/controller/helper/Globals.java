package ir.rasen.charsoo.controller.helper;

public class Globals {

    private static final Globals instance = new Globals();
    private GlobalVariables globalVariables = new GlobalVariables();

    private Globals() {
    }

    public static Globals getInstance() {
        return instance;
    }

    public GlobalVariables getValue() {
        return globalVariables;
    }

}