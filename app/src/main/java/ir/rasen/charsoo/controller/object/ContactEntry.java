package ir.rasen.charsoo.controller.object;

import android.graphics.Bitmap;

import com.amulyakhare.textdrawable.TextDrawable;

import org.json.JSONObject;

import java.util.ArrayList;

import ir.rasen.charsoo.controller.helper.Params;

/**
 * Created by hossein-pc on 6/21/2015.
 */
public class ContactEntry {

    public String fullName;
    public enum ContactType {PhoneNumber, Email, None }
    public ContactType type;
    public String contactData;
    public Bitmap contactPhoto;
    public TextDrawable contactPhotoDrawable;

    public boolean isInCharsoo;
    public String charsooUniqueId;
    public String charsooStringId;
    public String charsooFullName;
    public String charsooPictureUniqueId;
    public boolean isSeenBefore;
    public ArrayList<String> namesMatchThisContactData;

    public ContactType getContactType(int typeCode){
        if (typeCode==1)
            return ContactType.PhoneNumber;
        else if (typeCode==2)
            return ContactType.Email;
        else
            return ContactType.None;
    }

    public int getContactTypeCode(ContactType type){
        if (type==ContactType.PhoneNumber)
            return 1;
        else if (type==ContactType.Email)
            return 2;
        else
            return -1;
    }

    public static String getStandardPhoneNumber(String phoneNumber){
        if (phoneNumber.startsWith("9"))
            return "0098"+phoneNumber;
        else if (phoneNumber.startsWith("+98"))
            return "0098"+phoneNumber.substring(3);
        else if (phoneNumber.startsWith("09"))
            return "0098"+phoneNumber.substring(1);
        else return phoneNumber;
    }

    public static ContactEntry getContactEntryFromJSON(JSONObject jsonObject){
        ContactEntry contactEntry=new ContactEntry();
        try {
            contactEntry.charsooUniqueId =jsonObject.getString(Params.User_Uniq_Name);
            contactEntry.charsooStringId=jsonObject.getString(Params.USER_STR_ID);
            contactEntry.charsooFullName=jsonObject.getString(Params.USER_FULL_NAME);
            contactEntry.charsooPictureUniqueId =jsonObject.getString(Params.USER_PICTURE_ID);
            contactEntry.isSeenBefore=jsonObject.getBoolean(Params.IS_USER_SEEN);
            contactEntry.contactData=jsonObject.getString(Params.MATCHED_FIELD_DATA_STR).toLowerCase();
            if (jsonObject.getString(Params.MATCHED_FIELD_TYPE_STR).equalsIgnoreCase(Params.EMAIL))
                contactEntry.type=ContactType.Email;
            else
                contactEntry.type=ContactType.PhoneNumber;
            contactEntry.isInCharsoo=true;

        }catch (Exception e){}

        return contactEntry;
    }
}
