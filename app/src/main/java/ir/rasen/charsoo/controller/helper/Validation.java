package ir.rasen.charsoo.controller.helper;

import android.content.Context;
import android.util.Patterns;

import ir.rasen.charsoo.R;

/**
 * Created by android on 3/16/2015.
 */
public class Validation {
    static private boolean valid;
    static private String errorMessage;

    public static boolean isValid() {
        return valid;
    }

    public static String getErrorMessage() {
        return errorMessage;
    }

    private Validation(boolean success, String errorMessage) {
        this.valid = success;
        this.errorMessage = errorMessage;
    }

    public static Validation validateEmail(Context context, String email) {
//        String trimmedEmail=email.trim();
//        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(trimmedEmail).matches())
//            return new Validation(false, context.getResources().getString(R.string.validationError_EmailAddress));
//        else
//            return new Validation(true, "null");
        String trimmedEmail=email.trim();
        if (trimmedEmail.length()==0)
            return new Validation(false, context.getResources().getString(R.string.Fill_Field));
        else
            return new Validation(true, "null");
    }

    public static Validation validatePassword(Context context, String password) {
        String trimmedPassword=password.trim();
        if (trimmedPassword.length()<Params.PASSWORD_MIN_LENGTH || trimmedPassword.length()>Params.PASSWORD_MAX_LENGTH){
            return new Validation(false, getLengthError(context,context.getResources().getString(R.string.validationError_PasswordLength),
                    Params.PASSWORD_MIN_LENGTH,Params.PASSWORD_MAX_LENGTH));
        }
        else if (!trimmedPassword.matches(Params.PASSWORD_MATCH_PATTERN))
            return new Validation(false, context.getResources().getString(R.string.validationError_PasswordPattern));
        else
            return new Validation(true, "null");
    }

    public static Validation validateBusinessIdentifier(Context context, String identifier) {
        String trimmedId=identifier.trim();
        if (trimmedId.length()<Params.BUSINESS_STRING_ID_MIN_LENGTH || trimmedId.length()>Params.BUSINESS_STRING_ID_MAX_LENGTH){
            return new Validation(false, getLengthError(context,context.getResources().getString(R.string.validationError_BusinessStrIdLength)
                    ,Params.BUSINESS_STRING_ID_MIN_LENGTH
                    ,Params.BUSINESS_STRING_ID_MAX_LENGTH));
        }
        else if (!trimmedId.matches(Params.BUSINESS_STRING_ID_VALIDATION)/* || identifier.length() < Params.USER_USERNAME_MIN_LENGTH*/)
            return new Validation(false, context.getResources().getString(R.string.validationError_BusinessStrIdPattern));
        else
            return new Validation(true, "null");
    }
    
    public static Validation validateUserIdentifier(Context context, String identifier) {
        String trimmedId=identifier.trim();
        if (trimmedId.length()<Params.USER_USERNAME_MIN_LENGTH || trimmedId.length()>Params.USER_USERNAME_MAX_LENGTH){
            return new Validation(false, getLengthError(context, context.getResources().getString(R.string.validationError_UserStrIdLength)
                    , Params.USER_USERNAME_MIN_LENGTH
                    , Params.USER_USERNAME_MAX_LENGTH));
        }
        else if (!trimmedId.matches(Params.USER_USERNAME_VALIDATION)/* || identifier.length() < Params.USER_USERNAME_MIN_LENGTH*/)
            return new Validation(false, context.getResources().getString(R.string.validationError_UserStrIdPattern));
        else
            return new Validation(true, "null");
    }

    public static Validation validateName(Context context, String name) {
        String trimmedText=name.trim();
        if (trimmedText.length()<Params.USER_NAME_MIN_LENGTH || trimmedText.length()>Params.USER_NAME_MAX_LENGTH){
            return new Validation(false, getLengthError(context, context.getResources().getString(R.string.validationError_NameLength)
                    , Params.USER_NAME_MIN_LENGTH
                    , Params.USER_NAME_MAX_LENGTH));
        }
        else if (!trimmedText.toString().matches(Params.USER_NAME_VALIDATION))
            return new Validation(false, context.getResources().getString(R.string.validationError_NamePattern));
        else
            return new Validation(true, "null");
    }

    public static Validation validateComment(Context context, String comment) {
        String trimmedText=comment.trim();
        if (trimmedText.length()<Params.COMMENT_TEXT_MIN_LENGTH || trimmedText.length()>Params.COMMENT_TEXT_MAX_LENGTH){
            return new Validation(false, getLengthError(context, context.getResources().getString(R.string.validationError_CommentLength)
                    , Params.COMMENT_TEXT_MIN_LENGTH
                    , Params.COMMENT_TEXT_MAX_LENGTH));
        }
        else
            return new Validation(true, "null");
    }

    public static Validation validateTitle(Context context, String title) {
        String trimmedTitle=title.trim();
        if (trimmedTitle.length()<Params.TITLE_MIN_LENGTH || trimmedTitle.length()>Params.TITLE_MAX_LENGTH){
            return new Validation(false, getLengthError(context, context.getResources().getString(R.string.validationError_TitleLength)
                    , Params.TITLE_MIN_LENGTH
                    , Params.TITLE_MAX_LENGTH));
        }
        else if (!trimmedTitle.toString().matches(Params.TITLE_VALIDATION))
            return new Validation(false, context.getResources().getString(R.string.validationError_TitlePattern));
        else
            return new Validation(true, "null");
    }

    public static Validation validateAboutMe(Context context, String aboutMe) {
        String trimmedText=aboutMe.trim();
        if (trimmedText.length() < Params.ABOUT_ME_MIN_LENGTH || trimmedText.length()>Params.ABOUT_ME_MAX_LENGTH){
            return new Validation(false, getLengthError(context, context.getResources().getString(R.string.validationError_AboutMeLength)
                    , Params.ABOUT_ME_MIN_LENGTH
                    , Params.ABOUT_ME_MAX_LENGTH));
        }
        else
            return new Validation(true, "null");
    }

    public static Validation validateRepeatPassword(Context context, String password, String repeatePassword) {

        if (!password.equals(repeatePassword))
            return new Validation(false, context.getResources().getString(R.string.enter_same_passwords));
        else
            return new Validation(true, "null");
    }

    public static Validation validateDay(Context context, String day) {
        if(day.equals(""))
            return new Validation(false, context.getResources().getString(R.string.enter_non_empty_value));

        int d = Integer.valueOf(day);
        if (d < 1 || d > 31)
            return new Validation(false, context.getResources().getString(R.string.enter_correct_value));
        else
            return new Validation(true, "null");
    }

    public static Validation validateMonth(Context context, String month) {
        if(month.equals(""))
            return new Validation(false, context.getResources().getString(R.string.enter_non_empty_value));

        int m = Integer.valueOf(month);
        if (m < 1 || m > 12)
            return new Validation(false, context.getResources().getString(R.string.enter_correct_value));
        else
            return new Validation(true, "null");
    }

    public static Validation validateYear(Context context, String year) {
        if(year.equals(""))
            return new Validation(false, context.getResources().getString(R.string.enter_non_empty_value));


        SolarCalendar solarCalendar = new SolarCalendar(context);
        if (year.length() < 4)
            return new Validation(false, context.getResources().getString(R.string.enter_correct_year));
        else if (Integer.valueOf(year) < 1250 || Integer.valueOf(year) > solarCalendar.get_year())
            return new Validation(false, context.getResources().getString(R.string.enter_correct_value));
        else
            return new Validation(true, "null");
    }

    public static Validation validateHour(Context context, String hourStr) {
        String trimmedHour=hourStr.trim();
        if(trimmedHour.equals(""))
            return new Validation(false, context.getResources().getString(R.string.fill));

        int hour = Integer.valueOf(trimmedHour);
        if (hour < 0 || hour > 23)
            return new Validation(false, context.getResources().getString(R.string.validationError_ValidHourValue));
        else
            return new Validation(true, "null");
    }

    public static Validation validateMinute(Context context, String minuteStr) {
        String trimmedMinute=minuteStr.trim();
        if(trimmedMinute.equals(""))
            return new Validation(false, context.getResources().getString(R.string.fill));

        int minute = Integer.valueOf(trimmedMinute);
        if (minute < 0 || minute > 59)
            return new Validation(false, context.getResources().getString(R.string.validationError_ValidMinuteValue));
        else
            return new Validation(true, "null");
    }

    public static Validation validateMobile(Context context, String mobileNumber) {
        String trimmedMobileNumber=mobileNumber.trim();
        if(trimmedMobileNumber.length()<2)
            return new Validation(false, context.getResources().getString(R.string.validationError_ValidMobileNumber));

        if (!trimmedMobileNumber.substring(0, 2).equals("09") || trimmedMobileNumber.length() != 11)
            return new Validation(false, context.getResources().getString(R.string.validationError_ValidMobileNumber));
        else
            return new Validation(true, "null");

    }

    public static Validation checkNumberIfIsMobile(String mobileNumber){
        if ((mobileNumber.startsWith("9")&& mobileNumber.length()>9)
                ||(mobileNumber.startsWith("09") && mobileNumber.length()>10)
                ||(mobileNumber.startsWith("00989") && mobileNumber.length()>13)
                ||(mobileNumber.startsWith("+989")&& mobileNumber.length()>12))
            return new Validation(true,"");
        else
            return new Validation(false,"");
    }

    public static Validation validateWebsite(Context context, String website) {
        String trimmedWebsite=website.trim();
        if (!trimmedWebsite.equals("")&& !Patterns.WEB_URL.matcher(trimmedWebsite).matches())
            return new Validation(false, context.getResources().getString(R.string.validationError_ValidWebsiteAddress));
        else
            return new Validation(true, "null");
    }

    public static Validation validatePhone(Context context, String phone) {
        String trimmedPhone=phone.trim();
        if (trimmedPhone.length() < 11 || !trimmedPhone.startsWith("0") )
            return new Validation(false, context.getString(R.string.validationError_PhoneNumber));
        else
            return new Validation(true, "null");
    }

    public static Validation validTextSize(Context context,String text){
        String trimmedTexte=text.trim();
        if (trimmedTexte.length() > 25)
            return new Validation(false, context.getString(R.string.validationError_TextSize));
        else
            return new Validation(true, "null");
    }

    public static Validation validateCity(Context context, String city) {
        String trimmedCity=city.trim();
        if (!trimmedCity.toString().matches(Params.CITY_VALIDATION))
            return new Validation(false, context.getResources().getString(R.string.enter_valid_city));
        else if (trimmedCity.length() > Params.USER_NAME_MAX_LENGTH)
            return new Validation(false, context.getResources().getString(R.string.enter_is_too_long));
        else
            return new Validation(true, "null");
    }

    public static Validation validateAddress(Context context, String address) {
        String trimmedAddress=address.trim();
        if (trimmedAddress.length()<Params.ADDRESS_TEXT_MIN_LENGTH ||
                trimmedAddress.length() > Params.ADDRESS_TEXT_MAX_LENGTH ||
                address.trim().equals(""))
            return new Validation(false, getLengthError(context, context.getResources().getString(R.string.validationError_AddressLength)
                    , Params.ADDRESS_TEXT_MIN_LENGTH
                    , Params.ADDRESS_TEXT_MAX_LENGTH));
        else
            return new Validation(true, context.getResources().getString(R.string.validationError_AddressLength));
    }

    private static String getLengthError(Context context,String errorMessageStart,int minLength,int maxLength){
        String s=errorMessageStart+" ";
        s+=String.valueOf(minLength)+" ";
        s+=context.getResources().getString(R.string.txt_TAA)+" ";
        s+=String.valueOf(maxLength)+" ";
        s+=context.getResources().getString(R.string.txt_HARF)+" ";
        s+=context.getResources().getString(R.string.txt_AST);
        return s;
    }
}
