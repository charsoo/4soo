package ir.rasen.charsoo.controller.object;

import java.util.ArrayList;

/**
 * Created by android on 8/26/2015.
 */
public class AnswerObjects{

    public class LoginInfos
    {
        public String AccessToken = "";
        public String ProfilePictureId = "";
        public String UserId = "";

    }
    public class VersionInfos
    {
        public int Minimum = -1 ;
        public int Maxmimum = -1;
    }
    public  class GetBlockUsers{
        public String UserId;
        public String UserProfilePictureId;
        public String UserName;
    }
    public class SuggestedBusiness {
        public String BusinessId;
        public String BusinessUserName;
        public String BusinessName;
        public float BusinessRate;
        public String BusinessProfilePictureId;
        public double Distance;
        public String businessPictureString;
    }
    public class Category{
        public String CategoryId;
        public String Category;
    }
    public class BusinessFollowers{
        public String UserId;
        public String UserProfilePictureId;
        public String UserName;
    }
    public class Business{
        public String BusinessUserName;
        public String UserId;
        public String BusinessName;
        public String ProfilePictureId;
        public String CoverPictureId;
        public String Category;
        public String SubCategory;
        public String Description;
        public int ReviewsNumber;
        public int FollowersNumber;
        public boolean IsFollowing;
        public int Rate;
    }
    public class BusinessInfo{
        public String InstaPage = "" ;
        public String Name = "";
        public String ProfilePicture = "";
        public String CoverPicture = "";
        public String Category = "";
        public String SubCategory = "";
        public String Description = "";
        public String WorkItems = "";
        public String Phone = "";
        public String State = "";
        public String City = "";
        public String Address = "";
        public String LocationLatitude = "";
        public String LocationLongitude = "";
        public String Email = "";
        public String Website = "";
        public String Mobile = "";
        public String HashTagList = "";
    }
    public class SubCategories{
        public String SubCategoryId;
        public String SubCategory;
    }
    public class BusinessReviews{
        public String ReviewId;
        public String UserId;
        public String UserName;
        public float Rate;
        public String UserProfilePictureId;
        public String Text;
        public String Review;
    }
    public class UserRevies{
        public String ReviewId;
        public String BusinessId;
        public String BusinessUserName;
        public String BusinessProfilePictureId;
        public String Text;
        public float Rate;
    }
    public class BusinessFollowing{
        public String BusinessId;
        public String SearchPictureId;
        public String BusinessName;
    }
    public class UserBusiness{
        public String Name;
        public String PictureId;
        public String Business_Id;
        public String BusinessId;
        public String Desc;
    }
    public class UserHomeInfo{
        public String UserId;
        public String Name;
        public String AboutMe;
        public String ProfilePictureId;
        public String CoverPictureId;
        public int FriendRequestNumber;
        public int ReviewsNumber;
        public int FollowedBusinessNumber;
        public int FriendsNumber;
        public String Permission;
        public String FriendshipRelationStatusCode;
        public String Businesses;
    }
    public class UserProfileInfo{
        public String Name;
        public String Email;
        public String Password;
        public String AboutMe;
        public String Sex;
        public String BirthDate;
        public String ProfilePictureId;
        public boolean IsEmailConfirmed;

    }
    public class RegisterUser{
        public String UserId;
        public String ProfilePictureId;
        public String AccessToken;
    }
    public class State{
        public String Id;
        public String Name;
    }
    public class city{
        public String Id;
        public String Name;
    }
    public class getallcommentnotifications {
        public String   UserProfilePicture;
        public String   Image;
        public String   PostPicture;

        public String   PostId;
        public String   UserName;
        public String   Text;
        public int     IntervalTime;
        public String   CommentId;
    }

    public class GetPostAllComments{
        public String CommentId;
        public String  CommntOwnerId;
        public String  CommntOwnerTitle;
        public String  ProfilePictureId;
        public String Text;
        public boolean IsUser;

    }


    public class GetUserFriendRequests{
        public String   UserProfilePictureId;
        public String         UserId;
        public String Id;
    }
    public class GetUserFriends{
        public String    UserProfilePictureId;
        public String         UserId;
        public String Id;
    }

    public  class  GetBusinessPosts{
        public String  PostId;
        public String  BusinessId;
        public String  Title;
        public String  PostPictureId;
        public String  Description;
        public String  Code;
        public String  Price;
        public String  Comments;
        public String  HashTagList;
        public boolean IsLiked;
        public boolean IsShared;
        public boolean IsReported;
        public int     LikeNumber;
        public int     CommentNumber;
        public int     ShareNumber;
        public String  CreationDate;
        public String  BizuniqName;
    }

    public class GetPost{
        public String BizID;
        public String   PostId;
        public String          BusinessProfilePictureId;
        public String  BusinessId;
        public String          Title;
        public String     PostPictureId;
        public String         Description;
        public String Code;
        public String      Price;
        public String  Comments;
        public String     HashTagList;
        public boolean    IsLiked;
        public boolean      IsShared;
        public int     LikeNumber;
        public int          CommentNumber;
        public int   ShareNumber;
        public String CreationDate;
    }
    public class GetSharedPosts{
        public String PostId;
        public String CreationDate;
        public String  engbizName;
        public String  BusinessId;
        public String Business_Id;
        public String   BusinessUserName;
        public String  BusinessProfilePictureId;
        public String Title;
        public  String PostPictureId;
        public  String Description;
        public String Code;
        public String Price;
        public String Comments;
        public  String  HashTagList;
        public boolean IsLiked;
        public boolean IsShared;
        public boolean IsReported;
        public int LikeNumber;
        public int ShareNumber;
        public int CommentNumber;

    }
    public class GetTimeLineAllBusinessPosts{
        public String PostId;
        public String        Business_Id;
        public String  BusinessUserName;
        public int        PostTypeId;
        public String BusinessProfilePictureId;
        public String         Title;
        public String PostPictureId;
        public String         Description;
        public String  Code;
        public String          Price;
        public boolean  IsLiked;
        public boolean      IsReported;
        public boolean  IsShared;
        public String          Comments;
        public String CreationDate;
        public int  LikeNumber;
        public int         CommentNumber;
        public int  ShareNumber;
        public String         HashTagList;
        public  String engbizName;



    }

    public class SearchBusinessesLocation{
        public String  BusinessProfilePictureId;
        public String       BusinessUserName;
        public double  Distance;
        public String BusinessId;
    }
    public class SearchPost{
        public String   PostId;
        public String   PostPictureId;

    }
    public class SearchUser{
        public String  Id;
        public String  UserProfilePictureId;
        public String  uniqUserName;
        public String  UserName;
    }
    public class BooleanAnswer{
        public boolean  Result;
    }
    public  class addcomment{
        public String Result;
    }


    public class WorkingTimes
    {
        /*
       Oht stand for OpenHourTime
       Omt stand for OpenMinuteTime
       Cht stand for CloseHourTime
       Cmt stand for CloseMinuteTime
       */
        public int Day;
        public int Oht;
        public int Omt;
        public int Cht;
        public int Cmt;
    }

    public class ImageFromGallery
    {
        public String GalleryItemId ;
        public String BizId ;
        public boolean IsOwner ;
        public String UserName ;
        public String PublisherUserId;
        public String LastUpdate ;
        public String ImageId ;
        public String UserImageId ;
    }

    public class ServerAnswer
    {

        public Boolean SuccessStatus ;

        public String Result ;

        public ServerError Error ;

    }
    public class ServerError
    {
        public String ErrorCode;


    }
}