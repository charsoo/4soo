package ir.rasen.charsoo.controller.helper;

/**
 * Created by android on 12/24/2014.
 */
public class SearchItemAnnouncementComment {
    public String userUniqueId;
    public String postID;
    public String user_picture;
    public String post_picture;


    public SearchItemAnnouncementComment(String userUniqueId, String postID, String user_picture, String post_picture){
        this.userUniqueId = userUniqueId;
        this.postID = postID;
        this.user_picture = user_picture;
        this.post_picture = post_picture;
    }
}
