package ir.rasen.charsoo.controller.object;

import android.content.Context;

import java.util.ArrayList;

import ir.rasen.charsoo.controller.helper.LoginInfo;

/**
 * Created by android on 12/20/2014.
 */
public class Review {
    public String uniqueId;
    public String businessUniqueId;
    public String businessUserName;
    public String userUniqueId;
    public String userName;
    public String businessPicutre;
    public String businessPicutreUniqueId;
    public String userPicutre;
    public String userPicutreUniqueId;
    public String text;
    public float rate;

    public  Review(String userIdentifier,String userPicutreUniqueId,int rate,String text){
        this.userName = userIdentifier;
        this.userPicutreUniqueId = userPicutreUniqueId;
        this.rate = rate;
        this.text = text;
    }
    public Review(){

    }

    public static boolean submitBefore(Context context,ArrayList<Review> reviews){
        for (Review review : reviews)
            if(review.userUniqueId.equals(LoginInfo.getInstance().getUserUniqueId()))
                return true;
        return false;


    }
}
