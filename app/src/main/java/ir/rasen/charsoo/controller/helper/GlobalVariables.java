package ir.rasen.charsoo.controller.helper;

import java.util.ArrayList;

import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.Category;
import ir.rasen.charsoo.controller.object.City;
import ir.rasen.charsoo.controller.object.State;
import ir.rasen.charsoo.controller.object.SubCategory;
import ir.rasen.charsoo.controller.object.User;

public class GlobalVariables {
    public User userOld;

    public Business businessRegistering = new Business();
    public Business businessEditing = new Business();
    public Business businessVisiting = new Business();

    public ArrayList<State> loadedStates;
    public ArrayList<City> loadedCities;

    public WorkDays workDays;

    public ArrayList<Category> loadedCategories;
    public ArrayList<SubCategory> loadedSubCategories;
}