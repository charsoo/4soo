package ir.rasen.charsoo.controller.object;

import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;

import ir.rasen.charsoo.controller.helper.WorkDays;
import ir.rasen.charsoo.view.activity.business.ActivityBusiness;
import ir.rasen.charsoo.view.activity.business.ActivityBusinessOther;
import ir.rasen.charsoo.controller.helper.Location_M;
import ir.rasen.charsoo.controller.helper.Params;


/**
 * Created by android on 12/1/2014.
 */
public class Business implements Cloneable {
    public String uniqueId;
    public String businessIdentifier;
    public String userUniqueId;
    public String name;
    public String coverPicture;
    public String coverPictureUniqueId;
    public String profilePicture;
    public String profilePictureUniqueId;
    public String categoryName;
    public String subcategoryName;
    public String categoryUniqueId;
    public String subCategoryUniqueId;
    public String description;
    public WorkDays workDays = new WorkDays();
    public String phone;
    public String stateName;
    public String cityName;
    public String stateUniqueId;
    public String cityUniqueId;
    public String address;
    public Location_M location_m = new Location_M();
    public String email;
    public String mobile;
    public String webSite;
    public ArrayList<String> hashtagList = new ArrayList<>();
    public int reviewsNumber;
    public int followersNumber;
    public boolean isFollowing;
    public float rate;
    public String IdInstagram;
    public String date;
    public String galleryId;
    public boolean isowner;
    public String PublisherUserId;
    public String LastUpdate;
    public String UserImageId;

    public static enum ChangeType{EDIT,DELETE}

    public Business(){
        uniqueId = "";
        businessIdentifier = "";
        userUniqueId = "";
        name = "";
        coverPicture = "";
        coverPictureUniqueId = "";
        profilePicture = "";
        profilePictureUniqueId = "";
        categoryName = "";
        subcategoryName = "";
        categoryUniqueId = "";
        subCategoryUniqueId = "";
        description = "";
        phone = "";
        stateName = "";
        cityName = "";
        stateUniqueId = "";
        cityUniqueId = "";
        address = "";
        email = "";
        mobile = "";
        webSite = "";
        IdInstagram = "";
        date = "";
    }

    public static void gotoBusinessOtherPage(Context context, String businessId) {
        Intent intent = new Intent(context, ActivityBusinessOther.class);
        intent.putExtra(Params.BUSINESS_ID_STRING,businessId);
        context.startActivity(intent);
    }

    public static void gotoBusinessPage(Context context,String businessId){
        Intent intent = new Intent(context, ActivityBusiness.class);
        intent.putExtra(Params.BUSINESS_ID_STRING,businessId);
        context.startActivity(intent);
    }



    public Business clone() throws CloneNotSupportedException {
        return (Business)super.clone();
    }
}
