package ir.rasen.charsoo.controller.object;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ir.rasen.charsoo.R;

/**
 * Created by Rasen_iman on 8/9/2015.
 */
public class City {
    public String cityUniqueId;
    public String cityName;

    public City(){
        cityUniqueId = "-1";
        cityName = "";
    }

    public City(String id,String name){
        this.cityUniqueId = id;
        this.cityName = name;
    }


    public static ArrayList<String> getCitiesId(List<City> cities){
        ArrayList<String> tempCitiesId = new ArrayList<>();

        for(City city : cities){
            tempCitiesId.add(city.cityUniqueId);
        }

        return tempCitiesId;
    }

    public static ArrayList<String> getCitiesName(List<City> cities){
        ArrayList<String> tempCitiesId = new ArrayList<>();

        for(City city : cities){
            tempCitiesId.add(city.cityName);
        }

        return tempCitiesId;
    }


    public static ArrayList<City> addDefaultCityToStartOfList(Context ctx,ArrayList<City> cities){
        String defaultCityName = ctx.getResources().getString(R.string.city);

        if(cities.get(0).cityName.equals(defaultCityName) || Integer.parseInt(cities.get(0).cityUniqueId)==-1)
            return cities;


        ArrayList<City> tempCities = new ArrayList<>();
        tempCities.add(new City("-1", defaultCityName));
        tempCities.addAll(cities);

        return tempCities;
    }


}
