package ir.rasen.charsoo.controller.helper;

import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * Created by android on 12/28/2014.
 */
public class Hashtag {

    public static ArrayList<String> getListFromString(String hashtagListString) {
        //e.g. "hashtag1,hashtag2,hashtag3"
        if (hashtagListString==null || hashtagListString.equals(""))
            return new ArrayList<>();
        else
            return new ArrayList<>(Arrays.asList(hashtagListString.split(",")));
    }

    public static String getStringFromList(ArrayList<String> hashtagList) {
        if (hashtagList != null && hashtagList.size() != 0) {
            String result = "";
            for (String str : hashtagList) {
                result += str + ",";
            }
            return result.substring(0, result.length() - 1);
        } else
            return "";

    }

    private void hashtagParser(TextView view,String message){
        //Pattern to find if there's a hash tag in the message
        //i.e. any word starting with a # and containing letter or numbers or _
        Pattern tagMatcher = Pattern.compile("[#]+[A-Za-z0-9-_]+\\b");

        //Scheme for Linkify, when a word matched tagMatcher pattern,
        //that word is appended to this URL and used as content URI
        String newActivityURL = "";
        view.setText(message);
        //Attach Linkify to TextView
        Linkify.addLinks(view, tagMatcher, newActivityURL);
    }
}
