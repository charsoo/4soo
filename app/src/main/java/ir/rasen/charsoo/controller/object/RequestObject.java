package ir.rasen.charsoo.controller.object;

import java.util.ArrayList;

public class RequestObject {

    public class RegisterBiz{
        public String UserId = "";
        public String BusinessId = "";
        public String Name = "";
        public String Email = "";
        public String CoverPicture = "";
        public String ProfilePicture = "";
        public String CategoryId = "";
        public String SubCategoryId = "";
        public String Description = "";
        public String WorkDays = "";
        public String WorkTimeOpen = "";
        public String WorkTimeClose = "";
        public String Phone = "";
        public String StateId = "";
        public String CityId = "";
        public String Address = "";
        public String LocationLatitude = "";
        public String LocationLongitude = "";
        public String Mobile = "";
        public String Website = "";
        public String HashTagList = "";
    }

    public class RegisterUser{
        public String UserId = "";
        public String Name = "";
        public String Email = "";
        public String Password = "";
        public String Phone = "";
        public String ProfilePicture = "";
    }

    public class TakeContactList {
        public String UserId = "";
        public String Emails = "";
        public String PhoneNumbers = "";
    }

    public class UpdateBizProfileInfo{
        public String InstaPage = "" ;
        public String BusinessId = "";
        public String Name = "";
        public String Email = "";
        public String ProfilePictureId = "";
        public String CoverPicture = "";
        public String ProfilePicture = "";
        public String CategoryId = "";
        public String SubCategoryId = "";
        public String Description = "";
        public String WorkItems = "";
        public String Phone = "";
        public String StateId = "";
        public String CityId = "";
        public String Address = "";
        public String LocationLatitude = "";
        public String LocationLongitude = "";
        public String Website = "";
        public String Mobile = "";
        public String HashTagList = "";
    }

    public class UpdatePassword{
        public String UserId = "";
        public String Password = "";
        public String PasswordNew = "";
    }

    public class UpdatePost{
        public String PostId = "";
        public String Title = "";
        public String Description = "";
        public String Price = "";
        public String Code = "";
        public String HashTagList = "";
        public String Discount = "";
    }

    public class UpdateUserProfile{
        public String UserId = "";
        public String Name = "";
        public String Email = "";
        public String Password = "";
        public String AboutMe = "";
        public String Sex = "";
        public String BirthDate = "";
        public String ProfilePicture = "";
        public String CoverPicture = "";
    }

    public class AddPost{
        public String BusinessId = "";
        public String Title = "";
        public String Picture = "";
        public String Description = "";
        public String Price = "";
        public String Code = "";
        public String HashTagList = "";
    }

    public class WorkingTimes
    {
        /*
       Oht stand for OpenHourTime
       Omt stand for OpenMinuteTime
       Cht stand for CloseHourTime
       Cmt stand for CloseMinuteTime
       */
        public int Day;
        public int Oht;
        public int Omt;
        public int Cht;
        public int Cmt;
    }


    public static class ReviewItem
    {
        public String RevId;
        public String UserId;
        public String BusinessId;
        public String Text;
        public String Rate;
        public ArrayList<String> Images ;
        public String LastUpdate;
        public String UserName;
    }

    public class SendImageReview{
        public String UserId;
        public String BusinessId;
        public String Image;
    }

    public class RevStatisticInfo
    {
        public int Star;
        public int Count;
    }

    public class ReviewHeaderInfo
    {
        public boolean IsFollowing ;
        public boolean IsOpen ;
        public String BizImageId ;
        public String BizId;
        public String BizName;
        public int TodayIntValue;
        public String OpenTime;
        public String CloseTime;
        public String FirstImage;
        public String SecondImage;
    }

}
