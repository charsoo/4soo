package ir.rasen.charsoo.model.comment;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.view.interface_m.ICommentChange;
import ir.rasen.charsoo.model.WebserviceGET;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class DeleteComment extends AsyncTask<Void, Void, Boolean> {
    private static final String TAG = "DeleteComment ";
    private ICommentChange iCommentChange = null;
    private String ownerUniqueId;
    private String commentUniqueId;
    private  boolean isUserSendedComment;
    private ServerAnswer serverAnswer;
    private Context context;
    private String accesstoken;

    //if ownerUniqueId = comment.ownerUniqueId delete the comment which write the user with uniqueId = ownerUniqueId
    //if ownerUniqueId != comment.ownerUniqueId (user is not the writer) and ownerUniqueId == comment.ownerUniqueId delete the comment which user with uniqueId=ownerUniqueId is owner of the business which is owner of the post

    public DeleteComment(Context context, String ownerUniqueId, String commentUniqueId,boolean isUserSendedComment, ICommentChange iCommentChange,String accesstoken) {
        this.ownerUniqueId = ownerUniqueId;
        this.commentUniqueId = commentUniqueId;
        this.iCommentChange = iCommentChange;
        this.context = context;
        this.isUserSendedComment = isUserSendedComment;
        this.accesstoken=accesstoken;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.DELETE_COMMENT, new ArrayList<>(
                Arrays.asList(String.valueOf(ownerUniqueId), String.valueOf(commentUniqueId),String.valueOf(isUserSendedComment),accesstoken)));

        try {
            serverAnswer = webserviceGET.execute_new(context);
            if (serverAnswer.getSuccessStatus())
            {
                AnswerObjects.BooleanAnswer answer= new Gson().fromJson(serverAnswer.getResult(true),
                        AnswerObjects.BooleanAnswer.class);
                return answer.Result;

            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null)
                iCommentChange.notifyDeleteCommentFailed(commentUniqueId,ServerAnswer.getError(context,ServerAnswer.EXECUTION_ERROR,TAG));
            else if(serverAnswer.getSuccessStatus()) {
                if(result)
                    iCommentChange.notifyDeleteComment(commentUniqueId);
                else
                    iCommentChange.notifyDeleteCommentFailed(commentUniqueId,ServerAnswer.getError(context,ServerAnswer.EXECUTION_ERROR,TAG));
            }else
                iCommentChange.notifyDeleteCommentFailed(commentUniqueId,ServerAnswer.getError(context,serverAnswer.getErrorCode(),TAG));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
