package ir.rasen.charsoo.model.comment;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebserviceGET;

import static android.os.Build.VERSION;
import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class SendComment extends AsyncTask<Void, Void, String> implements IWebservice {
    private static final String TAG = "SendComment";

    private IWebservice delegate = null;
    private String userUniqueId;
    private String postUniqueId;
    private String commentText;
    private ServerAnswer serverAnswer;
    private Context context;
    private int reqCode;
    private String accesstoken;

    public SendComment(Context context, String userUniqueId, String postUniqueId,
                       String commentText, IWebservice delegate,int reqCode,String accesstoken) {
        this.userUniqueId = userUniqueId;
        this.postUniqueId = postUniqueId;
        this.commentText = commentText;
        this.delegate = delegate;
        this.context = context;
        this.reqCode=reqCode;
        this.accesstoken=accesstoken;
    }

    @Override
    protected String doInBackground(Void... voids) {
        WebserviceGET webserviceGET;
        try {
            ArrayList arrayList = new ArrayList<>(
                    Arrays.asList(userUniqueId,postUniqueId, commentText,accesstoken));
            webserviceGET = new WebserviceGET(URLs.SEND_COMMENT,arrayList );
        }
        catch (Exception e){

            String s = e.getMessage();
            return  null;
        }


        try {
            serverAnswer = webserviceGET.execute_new(context);
            if (serverAnswer.getSuccessStatus()) {
                AnswerObjects.addcomment answer= new Gson().fromJson(serverAnswer.getResult(true),
                        AnswerObjects.addcomment.class);
                return answer.Result;
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus()) {
                try {
                    delegate.getResult(reqCode,serverAnswer.getResult().getString(Params.RESULT));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getResult(int request, Object result) {

    }

    @Override
    public void getError(int request, Integer errorCode, String callerStringID) {

    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
