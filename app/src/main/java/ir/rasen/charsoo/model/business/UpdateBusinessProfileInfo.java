package ir.rasen.charsoo.model.business;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Hashtag;
import ir.rasen.charsoo.controller.helper.WorkDays;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.helper.ResultStatus;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.model.WebservicePOST;
import ir.rasen.charsoo.view.interface_m.IWebservice;


/**
 * Created by android on 12/16/2014.
 */
public class UpdateBusinessProfileInfo extends AsyncTask<Void, Void, ResultStatus> {
    private static final String TAG = "UpdateBusinessProfileInfo";

    private IWebservice delegate = null;
    private Business business;
    private ServerAnswer serverAnswer;
    private Context context;
    private String accesstoken;
    private int reqCode;

    public UpdateBusinessProfileInfo(Context context,Business business,IWebservice delegate,int reqCode,String accesstoken) {
        this.business = business;
        this.delegate = delegate;
        this.context = context;
        this.accesstoken=accesstoken;
        this.reqCode = reqCode;
    }

    public String createWorkTimesObjectJson(WorkDays wd){
        RequestObject obj = new RequestObject();
        ArrayList<WorkDays.DayTime> dt = wd.getDayTimes();

        RequestObject.WorkingTimes[] wts = new RequestObject.WorkingTimes[dt.size()];
        for(int i=0;i<dt.size();i++){
            RequestObject.WorkingTimes workTime = obj.new WorkingTimes();
            workTime.Day = dt.get(i).mCurrentDay;
            workTime.Cht = dt.get(i).mCloseTime.mHourTime;
            workTime.Cmt = dt.get(i).mCloseTime.mMinuteTime;
            workTime.Oht = dt.get(i).mOpenTime.mHourTime;
            workTime.Omt = dt.get(i).mOpenTime.mMinuteTime;
            wts[i] = workTime;
        }

        return new Gson().toJson(wts);
    }

    public RequestObject.UpdateBizProfileInfo createUpdateBusinessProfileInfo(){
        RequestObject obj = new RequestObject();
        RequestObject.UpdateBizProfileInfo req =
                obj.new UpdateBizProfileInfo();

        req.BusinessId = business.uniqueId;
        req.Name = business.name;
        req.Email = business.email;
        req.ProfilePictureId = business.profilePictureUniqueId;
        req.CoverPicture = "";
        req.ProfilePicture = business.profilePicture;
        req.CategoryId = business.categoryUniqueId;
        req.SubCategoryId = business.subCategoryUniqueId;
        req.Description = business.description;

        req.WorkItems = createWorkTimesObjectJson(business.workDays);

        req.Phone = business.phone;
        req.StateId = business.stateUniqueId;
        req.CityId = business.cityUniqueId;
        req.Address = business.address;
        req.LocationLatitude = business.location_m.getLatitude();
        req.LocationLongitude = business.location_m.getLongitude();
        req.Website = business.webSite;
        req.Mobile = business.mobile;
        req.HashTagList = Hashtag.getStringFromList(business.hashtagList);
        req.InstaPage = business.IdInstagram ;

        return req;
    }

    @Override
    protected ResultStatus doInBackground(Void... voids) {
        WebservicePOST webservicePOST = new WebservicePOST(URLs.UPDATE_PROFILE_BUSINESS,new ArrayList<>(Arrays.asList(accesstoken)));

        webservicePOST.postParam = new Gson().toJson(createUpdateBusinessProfileInfo());

        try {

            serverAnswer = webservicePOST.execute(context);
            if (serverAnswer.getSuccessStatus())
                return ResultStatus.getResultStatus(serverAnswer);
        } catch (Exception e) {
            //Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }


    @Override
    protected void onPostExecute(ResultStatus result) {


        try {
            //if webservice.executeWithNewSolution(); throws exception
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
