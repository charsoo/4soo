package ir.rasen.charsoo.model.business;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ResultStatus;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.model.WebserviceGET;

import ir.rasen.charsoo.view.interface_m.IWebServiceDeleteItem;
import ir.rasen.charsoo.view.interface_m.IWebservice;

/**
 * Created by android on 9/7/2015.
 */
public class DeleteImageFromGallery extends AsyncTask<Void, Void, ResultStatus> {
    private static final String TAG = "DeleteImageFromGallery";

    private IWebServiceDeleteItem delegate = null;
    private ServerAnswer serverAnswer;
    private Context context;
    private String accesstoken;
    private String galleryid;
    private int reqCode;
    private int positon;


    public DeleteImageFromGallery(Context context,String galleryid,IWebServiceDeleteItem delegate,int reqCode,String accestoken,int position) {
        this.delegate = delegate;
        this.context = context;
        this.galleryid = galleryid;
        this.accesstoken = accestoken;
        this.reqCode = reqCode;
        this.positon = position;
    }

    @Override
    protected ResultStatus doInBackground(Void... voids) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.Delete_Image_From_Galllry, new ArrayList<>(
                Arrays.asList(LoginInfo.getInstance().getUserUniqueId(), galleryid,accesstoken)));

        try {
            serverAnswer = webserviceGET.execute(context);
            if (serverAnswer.getSuccessStatus()){
                return ResultStatus.getResultStatus(serverAnswer);}
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ResultStatus result) {

        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG,positon);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result,positon);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG,positon);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
