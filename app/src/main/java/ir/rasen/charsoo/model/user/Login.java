package ir.rasen.charsoo.model.user;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ResultStatus;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.SharedPrefHandler;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

    public class Login extends AsyncTask <Void, Void, ResultStatus> {
    private static final String TAG = "Login";
    public  AnswerObjects.LoginInfos log;
    public IWebservice delegate = null;
    private Context context;
    private String email;
    private String password;
    private ServerAnswer serverAnswer;

    private int reqCode;

    public Login(Activity context, String email, String password, IWebservice delegate,int reqCode) {
        this.context = context;
        this.email = email;
        this.password = password;
        this.delegate = delegate;
        this.reqCode = reqCode;
    }

    @Override
    protected ResultStatus doInBackground(Void... voids) {

        //params: email,password
        WebserviceGET webserviceGET = new WebserviceGET(URLs.LOGIN, new ArrayList<>(
                Arrays.asList(email, password)));
        try {
            serverAnswer = webserviceGET.execute(context);
            if (serverAnswer.getSuccessStatus()) {
                String strobj= serverAnswer.getResult(true);
                Gson gson= new Gson();
                log = gson.fromJson(strobj , AnswerObjects.LoginInfos.class);
                SharedPrefHandler.saveUserEmailPassword(context, email, password);
                LoginInfo.getInstance().setUniqueUserId(log.UserId);
                LoginInfo.getInstance().setUserProfilePictureUniqueId(log.ProfilePictureId);
                LoginInfo.getInstance().setUserAccessToken(log.AccessToken);
                return new ResultStatus(true, ServerAnswer.NONE_ERROR);
            } else{
                return new ResultStatus(false, serverAnswer.getErrorCode());
            }
        } catch (Exception e) {
            return new ResultStatus(false, serverAnswer.getErrorCode());
        }
    }

    @Override
    protected void onPostExecute(ResultStatus result) {

        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (result.success)
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,result.errorCode,TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exectueWithNewSolution(){
        try{
            if (SDK_INT >= HONEYCOMB)
                executeOnExecutor(THREAD_POOL_EXECUTOR);
            else
                execute();
        }catch (Exception r){
            execute();
        }

    }
}
