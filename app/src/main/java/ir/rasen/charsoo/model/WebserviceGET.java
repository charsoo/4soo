package ir.rasen.charsoo.model;

import android.content.Context;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.util.ArrayList;

import ir.rasen.charsoo.controller.helper.NetworkHandler;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.WebservicesHandler;
import ir.rasen.charsoo.controller.object.AnswerObjects;


/**
 * Created by android on 12/1/2014.
 */
public class WebserviceGET {
    HttpClient httpclient;
    HttpGet httpGet;


    public WebserviceGET(String url, ArrayList<String> paramsList) {
        httpclient = new DefaultHttpClient();
        if (paramsList != null) {
            for (String param : paramsList) {
                url += "/" + param;
            }
        }
        url = url.replaceAll ( " ", "%20" );
       /* try {
            url = URLEncoder.encode(url, "UTF-8");
        }
        catch (Exception e){
            String s = e.getMessage();
        }*/
        httpGet = new HttpGet(url);

    }


    public ServerAnswer  execute(Context context) throws Exception {
        if (!NetworkHandler.isNetworkAvailable(context))
            return getNetworkConnectionError();

        HttpResponse httpResponse = null;
        try {
            httpResponse = httpclient.execute(httpGet);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < Params.RETRY_COUNT_ON_CONNECTION_FAILED; i++) {
            if (httpResponse!=null) {
                return ServerAnswer.get(httpResponse);
            }
            try {
                httpResponse = httpclient.execute(httpGet);
            } catch (Exception e) {
                String s = e.getMessage();
            }
        }
        return ServerAnswer.get(httpResponse);
    }

    public ServerAnswer execute_new(Context context) throws Exception {
        if (!NetworkHandler.isNetworkAvailable(context))
            return getNetworkConnectionError();

        HttpResponse httpResponse = null;
        try {
            httpResponse = httpclient.execute(httpGet);
        } catch (Exception e) {
            String s = e.getMessage();
        }
        for (int i = 0; i < Params.RETRY_COUNT_ON_CONNECTION_FAILED; i++) {
            if (httpResponse!=null) {
                return ServerAnswer.get_new(httpResponse);
            }
            try {
                httpResponse = httpclient.execute(httpGet);
            } catch (Exception e) {
                String s = e.getMessage();
            }
        }
        return ServerAnswer.get_new(httpResponse);
    }

    public ServerAnswer executeList(Context context) throws Exception {
        if (!NetworkHandler.isNetworkAvailable(context))
            return getNetworkConnectionError();

        HttpResponse httpResponse = null;
        try {
            httpResponse = httpclient.execute(httpGet);
        } catch (Exception e) {

        }
        for (int i = 0; i < Params.RETRY_COUNT_ON_CONNECTION_FAILED ; i++) {
            if (httpResponse!=null)
            {
                return ServerAnswer.getList(httpResponse);
            }
            try {
                httpResponse = httpclient.execute(httpGet);
            } catch (Exception e) {

            }
        }
        return ServerAnswer.getList(httpResponse);
    }


    public AnswerObjects.ServerAnswer executeP(Context context) throws Exception {
        if (!NetworkHandler.isNetworkAvailable(context))
            return getNetworkConnectionErrorP();

        HttpResponse httpResponse = null;
        try {
            httpResponse = httpclient.execute(httpGet);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < Params.RETRY_COUNT_ON_CONNECTION_FAILED; i++) {
            if (httpResponse!=null) {
                return ServerAnswer.getP(httpResponse);
            }
            try {
                httpResponse = httpclient.execute(httpGet);
            } catch (Exception e) {
                String s = e.getMessage();
            }
        }
        return ServerAnswer.getP(httpResponse);
    }


    public ServerAnswer getNetworkConnectionError(){
        ServerAnswer serverAnswer = new ServerAnswer();
        serverAnswer.setSuccessStatus(false);
        serverAnswer.setResult(null);
        serverAnswer.setErrorCode(ServerAnswer.NETWORK_CONNECTION_ERROR);
        return serverAnswer;
    }
    public AnswerObjects.ServerAnswer getNetworkConnectionErrorP(){
        AnswerObjects obj=new AnswerObjects();
        AnswerObjects.ServerAnswer serverAnswer = obj.new ServerAnswer();
        AnswerObjects.ServerError err= obj.new ServerError();
        err.ErrorCode=String.valueOf(ServerAnswer.NETWORK_CONNECTION_ERROR);
        return serverAnswer;
    }

}
