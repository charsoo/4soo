package ir.rasen.charsoo.model.post;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

//import org.apache.http.entity.mime.MultipartEntity;
//import org.apache.http.entity.mime.content.FileBody;
//import org.apache.http.entity.mime.content.StringBody;

import com.google.gson.Gson;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.controller.helper.Hashtag;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebservicePOST;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

/**
 * Created by android on 12/16/2014.
 */
public class AddPost extends AsyncTask<Void, Void, Post> {
    private static final String TAG = "AddPost";

    private IWebservice delegate = null;
    private Post post;
    private ServerAnswer serverAnswer;
    private Context context;
    private int reqCode;
    private String accesstoken;

    public AddPost(Context context,Post post,IWebservice delegate,int reqCode,String accesstoken) {
        this.post = post;
        this.delegate = delegate;
        this.context= context;
        this.reqCode=reqCode;
        this.accesstoken=accesstoken;
    }


    public RequestObject.AddPost createAddPost(){
     RequestObject obj = new RequestObject();
        RequestObject.AddPost req = obj.new AddPost();

        req.BusinessId = post.businessUniqueId;
        req.Title = post.title;
        req.Picture = post.picture;
        req.Description = post.description;
        req.Price = post.price;
        req.Code = post.code;
        req.HashTagList = Hashtag.getStringFromList(post.hashtagList);

        return req;
    }

    @Override
    protected Post doInBackground(Void... voids) {
        WebservicePOST webservicePOST = new WebservicePOST(URLs.ADD_POST,new ArrayList<>(Arrays.asList(accesstoken)));

        webservicePOST.postParam = new Gson().toJson(createAddPost());

        try {
            serverAnswer = webservicePOST.execute(context);
            if (serverAnswer.getSuccessStatus()) {
                post.uniqueId=serverAnswer.getResult().getString(Params.POST_ID_INT);
                // TODO bad az eslahe webservice tarikhe Ejade post ra az pasokhe webservice daryaft konim
                try {
                    Date d=new Date();
                    DateFormat.getDateTimeInstance().format(d);
                    post.creationDate=d;
                }catch(Exception ee){
                    ee.getMessage();
                }
                return post;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Post result) {
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
