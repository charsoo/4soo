package ir.rasen.charsoo.model.business;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.BaseAdapterItem;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;


/**
 * Created by android on 12/16/2014.
 */
public class GetBusinessFollowers extends AsyncTask<Void, Void, ArrayList<BaseAdapterItem>> {
    private static final String TAG = "GetBusinessFollowers";

    private IWebservice delegate = null;
    private String businessUniqueId;
    private ServerAnswer serverAnswer;
    private Context context;
    public String accesstoken;

    private int reqCode;

    public GetBusinessFollowers(Context context,String businessUniqueId,IWebservice delegate,int reqCode,String accesstoken) {
        this.businessUniqueId = businessUniqueId;
        this.delegate = delegate;
        this.context = context;
        this.accesstoken=accesstoken;
        this.reqCode = reqCode;
    }

    @Override
    protected ArrayList<BaseAdapterItem> doInBackground(Void... voids) {
        ArrayList<BaseAdapterItem> list = new ArrayList<BaseAdapterItem>();

        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_BUSINESS_FOLLOWERS,new ArrayList<>(
                Arrays.asList(businessUniqueId,accesstoken)));
        try {
            serverAnswer = webserviceGET.executeList(context);
            if (serverAnswer.getSuccessStatus()) {
                JSONArray jsonArray = serverAnswer.getResultList();
                Gson gson = new Gson();
                AnswerObjects.BusinessFollowers[] businessFollowers = gson.fromJson(serverAnswer.getResultList(true),
                        AnswerObjects.BusinessFollowers[].class);
                for (int i = 0; i < jsonArray.length(); i++) {
                    list.add(new BaseAdapterItem(context.getResources(),businessFollowers[i].UserId,
                            businessFollowers[i].UserProfilePictureId,
                            businessFollowers[i].UserName));
                }
                return list;
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<BaseAdapterItem> result) {

        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(), TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
