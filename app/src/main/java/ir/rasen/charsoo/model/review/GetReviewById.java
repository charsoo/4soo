package ir.rasen.charsoo.model.review;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.plus.model.people.Person;
import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

public class GetReviewById extends AsyncTask<Void,Void,RequestObject.ReviewItem>{
    public static final String TAG = "GetReviewById";

    IWebservice iWebservice;
    Context context;
    int reqCode;
    String reviewId;
    String accessToken;

    ServerAnswer serverAnswer;
    public GetReviewById(Context ctx,String reviewId,IWebservice delegate,int reqCode){
        this.context = ctx;
        this.reviewId = reviewId;
        this.iWebservice = delegate;
        this.reqCode = reqCode;
        this.accessToken = LoginInfo.getInstance().userAccessToken;

    }

    @Override
    protected RequestObject.ReviewItem doInBackground(Void... params) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_REVIEW_BY_ID,new ArrayList<>(Arrays.asList(
                LoginInfo.getInstance().getUserUniqueId(),reviewId,accessToken
        )));

        try {
            serverAnswer = webserviceGET.execute(context);

            Gson gson = new Gson();
            RequestObject.ReviewItem ri =
                    gson.fromJson(serverAnswer.getResult(true), RequestObject.ReviewItem.class);

            return ri;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(RequestObject.ReviewItem reviewItem) {
        try {
            if (serverAnswer == null) {
                iWebservice.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                iWebservice.getResult(reqCode,reviewItem);
            else
                iWebservice.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
