package ir.rasen.charsoo.model.business;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ResultStatus;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.model.WebservicePOST;
import ir.rasen.charsoo.view.interface_m.IWebservice;

/**
 * Created by android on 9/5/2015.
 */
public class SendImage extends AsyncTask<Void, Void, Boolean> {
    private static final String TAG = "SendImage";
    private IWebservice delegate = null;
    private ServerAnswer serverAnswer;
    private String businessid;
    private Context context;
    private String imageid;
    private int reqCode;
    private String accesstoken;

    public SendImage(Context context,String businessid,int reqCode,String imageid,IWebservice delegate,String accesstoken) {
        this.businessid = businessid;
        this.delegate = delegate;
        this.context = context;
        this.reqCode = reqCode;
        this.accesstoken = accesstoken;
        this.imageid = imageid;
    }
    public RequestObject.SendImageReview CreateSendImage(){

        RequestObject obj= new RequestObject();
        RequestObject.SendImageReview sendImageReview = obj.new SendImageReview();
        sendImageReview.UserId= LoginInfo.getInstance().getUserUniqueId();
        sendImageReview.BusinessId=businessid;
        sendImageReview.Image = imageid;
        return sendImageReview;
    }
    @Override
    protected Boolean doInBackground(Void... voids) {
        WebservicePOST webservicePOST = new WebservicePOST(URLs.Send_Image+"/"+accesstoken);
        webservicePOST.postParam =
                new Gson().toJson(CreateSendImage());
            try {
                serverAnswer = webservicePOST.execute_new(context);
                if (serverAnswer.getSuccessStatus()){
                    AnswerObjects.BooleanAnswer booleanAnswer = new Gson().fromJson(serverAnswer.getResult(true),
                            AnswerObjects.BooleanAnswer.class);
                    return booleanAnswer.Result;
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
                serverAnswer = null;
            }
            return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {

        try {
            try {
                if (serverAnswer == null) {
                    delegate.getError(reqCode, ServerAnswer.EXECUTION_ERROR, TAG);
                    return;
                }
                if (serverAnswer.getSuccessStatus())
                    delegate.getResult(reqCode, result);
                else
                    delegate.getError(reqCode, serverAnswer.getErrorCode(), TAG);
            } catch (Exception e) {
                delegate.getError(reqCode, ServerAnswer.EXECUTION_ERROR, TAG);
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}


