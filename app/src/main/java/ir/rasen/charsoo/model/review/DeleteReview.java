package ir.rasen.charsoo.model.review;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.Review;
import ir.rasen.charsoo.controller.helper.ResultStatus;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.view.interface_m.IReviewChange;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebserviceGET;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class DeleteReview extends AsyncTask<Void, Void, Void> {
    private static final String TAG = "DeleteReview";

    private IWebservice delegate = null;
    private Review review;
    private ServerAnswer serverAnswer;
    private IReviewChange iReviewChange;
    private Context context;
    private String accesstoken;
    private int reqCode;

    public DeleteReview(Context context,Review review, IWebservice delegate, IReviewChange iReviewChange, int reqCode,String accesstoken) {
        this.review = review;
        this.delegate = delegate;
        this.iReviewChange = iReviewChange;
        this.context = context;
        this.reqCode = reqCode;
        this.accesstoken=accesstoken;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.DELETE_REVIEW, new ArrayList<>(
                Arrays.asList(review.userUniqueId, review.uniqueId,accesstoken)));


        try {
            serverAnswer = webserviceGET.execute_new(context);
//            if (serverAnswer.getSuccessStatus())
//                return ResultStatus.getResultStatus(serverAnswer);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {


        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                iReviewChange.notifyDeleteReviewFailed(review.uniqueId,ServerAnswer.getError(context,ServerAnswer.EXECUTION_ERROR,TAG));
                return;
            }
            if (serverAnswer.getSuccessStatus()) {
                boolean results = serverAnswer.getResult().getBoolean(Params.RESULT);
                AnswerObjects.BooleanAnswer booleanAnswer = new Gson().fromJson(serverAnswer.getResult(true),
                        AnswerObjects.BooleanAnswer.class);
                boolean res = Boolean.valueOf(results);
                delegate.getResult(reqCode, res);
                iReviewChange.notifyDeleteReview(review.uniqueId);
            } else {
                delegate.getError(reqCode, serverAnswer.getErrorCode(), TAG);
                iReviewChange.notifyDeleteReviewFailed(review.uniqueId,ServerAnswer.getError(context,serverAnswer.getErrorCode(),TAG));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
