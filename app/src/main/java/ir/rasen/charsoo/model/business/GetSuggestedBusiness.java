package ir.rasen.charsoo.model.business;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.SuggestedBusiness;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;


/**
 * Created by android on 12/16/2014.
 */
public class GetSuggestedBusiness extends AsyncTask<Void, Void, ArrayList<SuggestedBusiness>> {
    private static final String TAG = "GetBurstAds";

    private IWebservice delegate = null;
    private String userUniqueId;
    String locLatitude,locLongitude;
    private ServerAnswer serverAnswer;
    private Context context;
    private int beforeThisId,limitation;
    private int reqCode;
    private String accesstoken;

    public GetSuggestedBusiness(Context context, String userUniqueId, String locLatitude, String locLongitude,
                       int beforeThisId, int limitation, IWebservice delegate, int reqCode,String accesstoken) {
        this.userUniqueId = userUniqueId;
        this.locLatitude=locLatitude;
        this.locLongitude=locLongitude;
        this.delegate = delegate;
        this.context = context;
        this.beforeThisId = beforeThisId;
        this.limitation = limitation;
        this.reqCode = reqCode;
        this.accesstoken=accesstoken;
    }

    @Override
    protected ArrayList<SuggestedBusiness> doInBackground(Void... voids) {
        ArrayList<SuggestedBusiness> list = new ArrayList();

        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_BURST_ADS, new ArrayList<>(
                Arrays.asList(userUniqueId,
                        locLatitude,locLongitude,
                        String.valueOf(beforeThisId),
                        String.valueOf(limitation)
                       ,accesstoken)));

        try {
            serverAnswer = webserviceGET.executeList(context);
            if (serverAnswer.getSuccessStatus()) {
                JSONArray jsonArray = serverAnswer.getResultList();
                Gson gson= new Gson();
                AnswerObjects.SuggestedBusiness[] bizs= gson.fromJson(serverAnswer.getResultList(true)
                        , AnswerObjects.SuggestedBusiness[].class );


                for (int i = 0; i < jsonArray.length(); i++) {
                    list.add(SuggestedBusiness.getSuggestedBusinessFromJSON(bizs[i]));
                }
                return list;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<SuggestedBusiness> result) {
      /*  if (result == null)
            delegate.getError(serverAnswer.getErrorCode());
        else
            delegate.getResult(result);*/

        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
