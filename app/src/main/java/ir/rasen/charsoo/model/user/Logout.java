package ir.rasen.charsoo.model.user;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;


import ir.rasen.charsoo.controller.helper.ServerAnswer;

import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.model.WebserviceGET;

import ir.rasen.charsoo.view.interface_m.ILogout;


import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

public class Logout extends AsyncTask <Void, Void, Boolean> {
    private static final String TAG = "Logout";

    private ILogout delegate = null;
    private String userUniqueId;
    private ServerAnswer serverAnswer;
    private String accessToken;
    private Context context;

    public Logout(Context context,String userUniqueId, String accesstoken,ILogout delegate) {
        this.context = context;
        this.userUniqueId = userUniqueId;
        this.accessToken = accesstoken;
        this.delegate = delegate;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {


        WebserviceGET webserviceGET = new WebserviceGET(URLs.Logout, new ArrayList<>(
                Arrays.asList(userUniqueId, accessToken)));
        try {
            serverAnswer = webserviceGET.execute_new(context);
            if (serverAnswer.getSuccessStatus()){
                AnswerObjects.BooleanAnswer booleanAnswer = new Gson().fromJson(serverAnswer.getResult(true),
                        AnswerObjects.BooleanAnswer.class);
                return booleanAnswer.Result;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {

        try {

            if (serverAnswer == null) {

                delegate.faillogout(ServerAnswer.EXECUTION_ERROR, TAG);

                return;
            }
            if (serverAnswer.getSuccessStatus())
                if(result)
                    delegate.successlogout();
                else
                    delegate.faillogout(serverAnswer.getErrorCode(), TAG);
            else
                delegate.faillogout(serverAnswer.getErrorCode(), TAG);

        } catch (Exception e) {
        }
    }

    public void exectueWithNewSolution(){
        try{
            if (SDK_INT >= HONEYCOMB)
                executeOnExecutor(THREAD_POOL_EXECUTOR);
            else
                execute();
        }catch (Exception r){
            execute();
        }

    }
}
