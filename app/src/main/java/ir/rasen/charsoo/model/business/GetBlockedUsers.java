package ir.rasen.charsoo.model.business;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.BaseAdapterItem;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;


/**
 * Created by android on 12/16/2014.
 */
public class GetBlockedUsers extends AsyncTask<Void, Void, ArrayList<BaseAdapterItem>> {
    private static final String TAG = "GetBlockedUsers";

    private IWebservice delegate = null;
    private String businessUniqueId;
    private ServerAnswer serverAnswer;
    private Context context;
    private int beforeThisId,limitation;
    private int reqCode;
    private String accesstoken;

    public
    GetBlockedUsers(Context context,String businessUniqueId,int beforeThisId,int limitation,IWebservice delegate,int reqCode,String accesstoken) {
        this.businessUniqueId = businessUniqueId;
        this.delegate = delegate;
        this.context = context;
        this.beforeThisId = beforeThisId;
        this.limitation = limitation;
        this.reqCode = reqCode;
        this.accesstoken=accesstoken;
        this.accesstoken=accesstoken;
    }

    @Override
    protected ArrayList<BaseAdapterItem> doInBackground(Void... voids) {
        ArrayList<BaseAdapterItem> list = new ArrayList<>();

        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_BLOCKED_USERS, new ArrayList<>(
                Arrays.asList(businessUniqueId,
                        String.valueOf(beforeThisId),
                        String.valueOf(limitation)
                        , accesstoken)
                        ));

        try {
            serverAnswer = webserviceGET.executeList(context);
            if (serverAnswer.getSuccessStatus()) {
                JSONArray jsonArray = serverAnswer.getResultList();
                String StrString = serverAnswer.getResultList(true);
                Gson gson = new Gson();
                AnswerObjects.GetBlockUsers[] bluckUsers = gson.fromJson(StrString,
                        AnswerObjects.GetBlockUsers[].class);

                for (int i = 0; i < jsonArray.length(); i++) {
                    list.add(new BaseAdapterItem(context.getResources(),
                            bluckUsers[i].UserId,
                            bluckUsers[i].UserProfilePictureId,
                            bluckUsers[i].UserName));
                }
                return list;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<BaseAdapterItem> result) {
      /*  if (result == null)
            delegate.getError(serverAnswer.getErrorCode());
        else
            delegate.getResult(result);*/

        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
