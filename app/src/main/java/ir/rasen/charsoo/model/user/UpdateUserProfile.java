package ir.rasen.charsoo.model.user;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ResultStatus;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.Sex;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.model.WebservicePOST;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

/**
 * Created by android on 12/16/2014.
 */
public class UpdateUserProfile extends AsyncTask<Void, Void, ResultStatus> {
    private static final String TAG = "UpdateUserProfile";

    private IWebservice delegate = null;
    private User user;
    private ServerAnswer serverAnswer;
    private Context context;
    private int reqCode;
    private String accesstoken;

    public UpdateUserProfile(Context context, User user, IWebservice delegate, int reqCode,String accesstoken) {
        this.delegate = delegate;
        this.user = user;
        this.context = context;
        this.reqCode = reqCode;
        this.accesstoken=accesstoken;
    }


    public RequestObject.UpdateUserProfile createUpdateUserProfile(){
        RequestObject obj = new RequestObject();
        RequestObject.UpdateUserProfile req =
                obj.new UpdateUserProfile();

        req.UserId = user.uniqueId;
        req.Name = user.name;
        req.Email = user.email;
        req.Password = user.password;
        req.AboutMe = user.aboutMe;
        req.Sex = String.valueOf(Sex.getInt(user.sex));
        req.BirthDate = user.birthDate;
        req.ProfilePicture = user.profilePicture;
        req.CoverPicture = "";

        return req;
    }


    @Override
    protected ResultStatus doInBackground(Void... voids) {

        WebservicePOST webservicePOST = new WebservicePOST(URLs.UPDATE_PROFILE_USER,new ArrayList<>(Arrays.asList(accesstoken)));

        try {

            webservicePOST.postParam = new Gson().toJson(createUpdateUserProfile());

            serverAnswer = webservicePOST.execute_new(context);
//            if (serverAnswer.getSuccessStatus())
//                return ResultStatus.getResultStatus(serverAnswer);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ResultStatus result) {
       /* if (result == null)
            delegate.getError(serverAnswer.getErrorCode());
        else
            delegate.getResult(result);*/

        //if webservice.executeWithNewSolution(); throws exception
        try {
            try {
                if (serverAnswer == null) {
                    delegate.getError(reqCode, ServerAnswer.EXECUTION_ERROR, TAG);
                    return;
                }
                if (serverAnswer.getSuccessStatus())
                    delegate.getResult(reqCode, serverAnswer.getResult().getInt(Params.RESULT));
                else
                    delegate.getError(reqCode, serverAnswer.getErrorCode(), TAG);
            } catch (Exception e) {
                delegate.getError(reqCode, ServerAnswer.EXECUTION_ERROR, TAG);
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
