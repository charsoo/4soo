package ir.rasen.charsoo.model.user;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.SharedPrefHandler;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ResultStatus;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebservicePOST;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class RegisterUser extends AsyncTask<Void, Void, ResultStatus> {
    private static final String TAG = "RegisterUser";

    private IWebservice delegate = null;
    private User user;
    private ServerAnswer serverAnswer;
    private Context context;
    int reqCode;


    public RegisterUser(Context context, User user, IWebservice delegate,int reqCode) {
        this.user = user;
        this.delegate = delegate;
        this.context = context;
        this.reqCode=reqCode;

    }

    public RequestObject.RegisterUser createRegisterUser(){
        RequestObject obj = new RequestObject();
        RequestObject.RegisterUser req = obj.new RegisterUser();

        req.UserId = user.userIdentifier;
        req.Name = user.name;
        req.Email = user.email;
        req.Password = user.password;
        req.Phone = user.phoneNumber;
        req.ProfilePicture = user.profilePicture;

        return req;
    }
    @Override
    protected ResultStatus doInBackground(Void... voids) {
        WebservicePOST webservicePOST = new WebservicePOST(URLs.REGISTER_USER);

        try {

            webservicePOST.postParam = new Gson().toJson(createRegisterUser());

            serverAnswer = webservicePOST.execute(context);
            if (serverAnswer.getSuccessStatus()) {
                JSONObject jsonObject = serverAnswer.getResult();
                Gson gson = new Gson();
                AnswerObjects.RegisterUser registerUser=gson.fromJson(serverAnswer.getResult(true)
                        ,AnswerObjects.RegisterUser.class);

                //save user_id and access token

                //TODO uncomment after modifying webservice
                String userUniqueId = "0",  profilePictureUniqueId = "2022";
                String access_token = null;
                if (jsonObject != null) {
                    userUniqueId = jsonObject.getString(Params.USER_UNIQUE_ID_STR);
                    profilePictureUniqueId = jsonObject.getString(Params.PROFILE_PICTURE_UNIQUE_ID);
                    access_token = jsonObject.getString(Params.ACCESS_TOKEN);

                    SharedPrefHandler.saveUserEmailPassword(context, user.email, user.password);
                    LoginInfo.getInstance().userAccessToken = access_token;
                    LoginInfo.getInstance().setUniqueUserId(userUniqueId);
                    LoginInfo.getInstance().setUserProfilePictureUniqueId(profilePictureUniqueId);
                    LoginInfo.getInstance().userUniqueName = user.userIdentifier;
                    LoginInfo.getInstance().username = user.name;
                }
            }
            return ResultStatus.getResultStatus(serverAnswer);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ResultStatus result) {
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode, ServerAnswer.EXECUTION_ERROR, TAG);
                return;
            }
            if (result.success)
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,result.errorCode,TAG);
        } catch (Exception e) {
            e.printStackTrace();


        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
