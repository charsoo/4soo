package ir.rasen.charsoo.model.business;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Hashtag;
import ir.rasen.charsoo.controller.helper.Location_M;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.helper.WorkDays;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.controller.object.SuggestedBusiness;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;

/**
 * Created by android on 9/5/2015.
 */
public class GetGalleryImage extends AsyncTask<Void, Void,  ArrayList<Business>> {
    private static final String TAG = "GetGalleryImage";
    private IWebservice delegate = null;
    private String businessUniqueId;
    private ServerAnswer serverAnswer;
    private Context context;
    private String accesstoken;
    private int reqCode,count,limit;

    public GetGalleryImage(Context context,int count,int limit,String businessUniqueId,IWebservice delegate,int reqCode,String accesstoken) {
        this.delegate = delegate;
        this.businessUniqueId = businessUniqueId;
        this.context = context;
        this.reqCode = reqCode;
        this.accesstoken=accesstoken;
        this.count = count;
        this.limit = limit;
    }

    @Override
    protected  ArrayList<Business> doInBackground(Void... voids) {
        ArrayList<Business> list = new  ArrayList<Business>();

        WebserviceGET webserviceGET = new WebserviceGET(URLs.Get_Gallery_Image, new ArrayList<>(
                Arrays.asList(LoginInfo.getInstance().getUserUniqueId(), String.valueOf(businessUniqueId),String.valueOf(count)
                        ,String.valueOf(limit), accesstoken)));
                try {
                    serverAnswer = webserviceGET.executeList(context);
                    if (serverAnswer.getSuccessStatus()) {
                        Gson gson= new Gson();
                        AnswerObjects.ImageFromGallery[] imageFromGalleries= gson.fromJson(serverAnswer.getResultList(true)
                                , AnswerObjects.ImageFromGallery[].class );

                        if(imageFromGalleries!= null) {
                            for (int i = 0; i < imageFromGalleries.length; i++) {
                                Business business = new Business();
                                business.uniqueId = (imageFromGalleries[i].BizId == null) ? "" : imageFromGalleries[i].BizId;
                                business.galleryId = (imageFromGalleries[i].GalleryItemId == null) ? "" : imageFromGalleries[i].GalleryItemId;
                                business.isowner = imageFromGalleries[i].IsOwner;
                                business.userUniqueId = (imageFromGalleries[i].UserName == null) ? "" : imageFromGalleries[i].UserName;
                                business.UserImageId = (imageFromGalleries[i].UserImageId == null) ? "" : imageFromGalleries[i].UserImageId;
                                business.profilePictureUniqueId = (imageFromGalleries[i].ImageId == null) ? "" : imageFromGalleries[i].ImageId;
                                business.PublisherUserId = (imageFromGalleries[i].PublisherUserId == null) ? "" : imageFromGalleries[i].PublisherUserId;
                                business.LastUpdate = (imageFromGalleries[i].LastUpdate == null) ? "" : imageFromGalleries[i].LastUpdate;
                                list.add(business);
                            }
                            return list;
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                    serverAnswer = null;
                }
                return null;
    }

    @Override
    protected void onPostExecute(ArrayList<Business> result) {


        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null && serverAnswer.equals("")) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}

