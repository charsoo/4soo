package ir.rasen.charsoo.model.business;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ResultStatus;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebServiceDeleteItem;
import ir.rasen.charsoo.view.interface_m.IWebservice;

/**
 * Created by android on 9/7/2015.
 */
public class Report extends AsyncTask<Void, Void, ResultStatus> {
    private static final String TAG = "Report";

    private IWebServiceDeleteItem delegate = null;
    private ServerAnswer serverAnswer;
    private Context context;
    private String accesstoken;
    private String imageid;
    private int reqCode;


    public Report(Context context,String imageid,IWebServiceDeleteItem delegate,int reqCode,String accestoken) {
        this.delegate = delegate;
        this.context = context;
        this.imageid = imageid;
        this.accesstoken = accestoken;
        this.reqCode = reqCode;
    }

    @Override
    protected ResultStatus doInBackground(Void... voids) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.Report_Image, new ArrayList<>(
                Arrays.asList(imageid, LoginInfo.getInstance().getUserUniqueId(), accesstoken)));

        try {
            serverAnswer = webserviceGET.execute(context);
            if (serverAnswer.getSuccessStatus()){
                return ResultStatus.getResultStatus(serverAnswer);}
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ResultStatus result) {

        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG,0);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result,0);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG,0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}

