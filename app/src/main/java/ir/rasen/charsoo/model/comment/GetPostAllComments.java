package ir.rasen.charsoo.model.comment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;


import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.Comment;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebserviceGET;


/**
 * Created by android on 12/16/2014.
 */
public class GetPostAllComments extends AsyncTask<Void, Void, ArrayList<Comment>> {
    private static final String TAG = "GetPostAllComments";
    private IWebservice delegate = null;
    private int beforThisId;
    private int limitaion;
    private ServerAnswer serverAnswer;
    private Context context;
    private String postUniqueId, postBusinessUniqueId;
    private int reqCode;
    private String accesstoken;

    public GetPostAllComments(Context context,String postUniqueId,String postBusinessUniqueId, int beforThisId, int limitaion, IWebservice delegate,int reqCode,String accesstoken) {
        this.delegate = delegate;
        this.beforThisId = beforThisId;
        this.limitaion = limitaion;
        this.context = context;
        this.postUniqueId = postUniqueId;
        this.postBusinessUniqueId = postBusinessUniqueId;
        this.reqCode=reqCode;
        this.accesstoken=accesstoken;
    }

    @Override
    protected ArrayList<Comment> doInBackground(Void... voids) {
        ArrayList<Comment> list = new ArrayList<Comment>();
        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_COMMENTS, new ArrayList<>(
                Arrays.asList(postUniqueId, LoginInfo.getInstance().getUserUniqueId(),
                        String.valueOf(beforThisId), String.valueOf(limitaion),accesstoken)));

        try {
              serverAnswer = webserviceGET.executeList(context);
            if (serverAnswer.getSuccessStatus()) {


                Gson gson= new Gson();
                String res=serverAnswer.getResultList(true);
                AnswerObjects.GetPostAllComments[] getpostallcomments =
                        gson.fromJson(res, AnswerObjects.GetPostAllComments[].class);

                if(getpostallcomments == null)
                    return list;
                for (int i = 0; i < getpostallcomments.length; i++) {

                    Comment comment = new Comment();
                    comment.uniqueId = getpostallcomments[i].CommentId;
                    comment.ownerUniqueId = getpostallcomments[i].CommntOwnerId;
                    comment.username = getpostallcomments[i].CommntOwnerTitle;
                    comment.userProfilePictureUniqueId = getpostallcomments[i].ProfilePictureId;
                    comment.text =getpostallcomments[i].Text;
                    comment.postUniqueId = postUniqueId;
                    comment.businessUniqueId = postBusinessUniqueId;
                    comment.isUserSendedComment =getpostallcomments[i].IsUser;
                    list.add(comment);
                }
                return list;
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<Comment> result) {


        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null || result==null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    public void exectueWithNewSolution(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
