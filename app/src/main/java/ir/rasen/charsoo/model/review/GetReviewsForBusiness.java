package ir.rasen.charsoo.model.review;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

/**
 * Created by Rasen_iman on 9/8/2015.
 */
public class GetReviewsForBusiness extends AsyncTask<Void,Void,ArrayList<RequestObject.ReviewItem>>{
    public static String TAG = "GetReviewsForBusiness";

    public final static int ORDER_MOFID_TARIN = 1;
    public final static int ORDER_JADID_TARIN = 2;
    public final static int ORDER_EMTIAZE_BALA = 3;


    AnswerObjects.ServerAnswer serverAnswer;
    IWebservice iWebservice;
    Context context;
    String bizId,userId,accessToken;
    int limit,count,sortOrder,reqCode;

    public GetReviewsForBusiness(Context ctx,String bizId,String userId,
                                 int limit,int count,int sortOrder,
                                 IWebservice iw,int reqCode){
        this.context = ctx;
        this.bizId = bizId;
        this.userId = userId;
        this.limit = limit;
        this.count = count;
        this.sortOrder = sortOrder;
        this.accessToken = LoginInfo.getInstance().userAccessToken;
        this.iWebservice = iw;
        this.reqCode = reqCode;
    }

    @Override
    protected ArrayList<RequestObject.ReviewItem> doInBackground(Void... params) {

        WebserviceGET webserviceGET =
                new WebserviceGET(URLs.GET_REVIEWS_FOR_BUSINESS,new ArrayList<>(Arrays.asList(
                        bizId,userId,String.valueOf(limit),String.valueOf(count),String.valueOf(sortOrder),accessToken
                )));


        try {
            serverAnswer = webserviceGET.executeP(context);

            RequestObject.ReviewItem[] reviewItems =
                    new Gson().fromJson(serverAnswer.Result,RequestObject.ReviewItem[].class);

            return new ArrayList<>(Arrays.asList(reviewItems));
        } catch (Exception e) {
        }

        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<RequestObject.ReviewItem> reviewItems) {
        try {
            if (serverAnswer == null) {
                iWebservice.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.SuccessStatus)
                iWebservice.getResult(reqCode,reviewItems);
            else
                iWebservice.getError(reqCode, Integer.valueOf(serverAnswer.Error.ErrorCode),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
