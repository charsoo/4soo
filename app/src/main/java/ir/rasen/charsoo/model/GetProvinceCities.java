package ir.rasen.charsoo.model;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.City;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class GetProvinceCities extends AsyncTask<Void, Void,ArrayList<City>> {
    private static final String TAG = "GetProvinceCities";
    private IWebservice delegate = null;
    private ServerAnswer serverAnswer;
    private String stateName;
    private Context context;
    private int reqCode;
    private String provinceIntId;
    private String accesstoken;



    public GetProvinceCities(Context context, String provinceIntId, IWebservice delegate,int reqCode,String accesstoken) {
        this.provinceIntId=provinceIntId;
        this.delegate = delegate;
        this.context = context;
        this.reqCode=reqCode;
        this.accesstoken=accesstoken;
    }

    @Override
    protected ArrayList<City> doInBackground(Void... voids) {
        ArrayList<City> cities = new ArrayList<>();


        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_PROVINCE_CITIES,new ArrayList<>(
                Arrays.asList(String.valueOf(provinceIntId),accesstoken)));

        try {

            // TODO fetch list from server
            serverAnswer = webserviceGET.executeList(context);
            if (serverAnswer!=null){
                if (serverAnswer.getSuccessStatus()){
                    JSONArray resultList=serverAnswer.getResultList();
                    Gson gson = new Gson();
                    AnswerObjects.city[] city = gson.fromJson(serverAnswer.getResultList(true)
                            ,AnswerObjects.city[].class);
                    for (int i = 0; i < resultList.length(); i++) {
                        cities.add(new City(city[i].Id,city[i].Name));
                    }
                    return cities;
                }
            }

            return null;

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<City> cities) {
//        ArrayList<String> list=new ArrayList<>();
//        list.addAll(Arrays.asList(context.getResources().getStringArray(R.array.default_CityList)));
//        delegate.getResult(reqCode,list);


        // TODO fetch list from server
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,cities);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
