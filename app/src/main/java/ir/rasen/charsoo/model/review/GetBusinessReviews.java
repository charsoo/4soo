package ir.rasen.charsoo.model.review;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.Review;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebserviceGET;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class GetBusinessReviews extends AsyncTask<Void, Void, ArrayList<Review>> {
    private static final String TAG = "GetBusinessReviews";
    private IWebservice delegate = null;
    private String userUniqueId;
    private String businessUniqueId;
    private int afterThisID;
    private int limitation;
    private ServerAnswer serverAnswer;
    private Context context;
    private String accesstoken;
    private int reqCode;


    public GetBusinessReviews(Context context,String userUniqueId,String businessUniqueId, int afterThisID, int limitation, IWebservice delegate,int reqCode,String accesstoken) {
        this.userUniqueId = userUniqueId;
        this.businessUniqueId = businessUniqueId;
        this.afterThisID = afterThisID;
        this.limitation = limitation;
        this.delegate = delegate;
        this.context = context;
        this.accesstoken=accesstoken;
        this.reqCode = reqCode;
    }

    @Override
    protected ArrayList<Review> doInBackground(Void... voids) {
        ArrayList<Review> list = new ArrayList<Review>();
        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_BUSINESS_REVIEWS, new ArrayList<>(
                Arrays.asList(String.valueOf(userUniqueId), String.valueOf(businessUniqueId),
                        String.valueOf(afterThisID), String.valueOf(limitation),accesstoken)));

        try {
            serverAnswer = webserviceGET.executeList(context);
            if (serverAnswer.getSuccessStatus()) {
                JSONArray jsonArray = serverAnswer.getResultList();
                Gson gson=new Gson();
                AnswerObjects.BusinessReviews[] businessReviews = gson.fromJson(serverAnswer.getResultList(true)
                        ,AnswerObjects.BusinessReviews[].class);
                for (int i = 0; i < jsonArray.length(); i++) {
                    Review review = new Review();
                    review.uniqueId = businessReviews[i].ReviewId;
                    review.userUniqueId = businessReviews[i].UserId;
                    review.userName = businessReviews[i].UserName;
                    review.rate = businessReviews[i].Rate;
                    review.userPicutreUniqueId = businessReviews[i].UserProfilePictureId;
                    review.text = businessReviews[i].Text;
                    list.add(review);
                }
                return list;
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<Review> result) {
        /*if (result == null)
            delegate.getError(serverAnswer.getErrorCode());
        else
            delegate.getResult(result);*/

        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
