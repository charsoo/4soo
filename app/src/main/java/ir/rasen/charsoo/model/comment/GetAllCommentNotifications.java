package ir.rasen.charsoo.model.comment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.CommentNotification;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION;
import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class GetAllCommentNotifications extends AsyncTask<Void, Void, ArrayList<CommentNotification>> {
    private static final String TAG = "GetAllCommentNotifications";

    private IWebservice delegate = null;
    private String userUniqueId;
    private int beforeThisId;
    private int limitation;
    private ServerAnswer serverAnswer;
    private Context context;
    private String accesstoken;
    private int reqCode;

    public GetAllCommentNotifications(Context context,String userUniqueId, int beforeThisId, int limitation, IWebservice delegate,int reqCode,String accesstoken) {
        this.delegate = delegate;
        this.userUniqueId = userUniqueId;
        this.beforeThisId = beforeThisId;
        this.limitation = limitation;
        this.context = context;
        this.accesstoken=accesstoken;
        this.reqCode = reqCode;
    }

    @Override
    protected ArrayList<CommentNotification> doInBackground(Void... voids) {
        ArrayList<CommentNotification> list = new ArrayList<CommentNotification>();

        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_ALL_COMMENT_NOTIFICATIONS, new ArrayList<>(
                Arrays.asList(userUniqueId, String.valueOf(beforeThisId), String.valueOf(limitation),accesstoken)));


        try {
            serverAnswer = webserviceGET.executeList(context);
            if (serverAnswer.getSuccessStatus()) {
                JSONArray jsonArray = serverAnswer.getResultList();

                Gson gson= new Gson();
                AnswerObjects.getallcommentnotifications[] get_all_cm_notifications = gson.fromJson(serverAnswer.getResultList(true)
                        ,AnswerObjects.getallcommentnotifications[].class);

                for (int i = 0; i < jsonArray.length(); i++) {


                    list.add(new CommentNotification(
                            get_all_cm_notifications[i].CommentId,
                            get_all_cm_notifications[i].PostId,
                            get_all_cm_notifications[i].UserName,
                            get_all_cm_notifications[i].UserProfilePicture,
                            get_all_cm_notifications[i].PostPicture,
                            get_all_cm_notifications[i].Text,
                            get_all_cm_notifications[i].IntervalTime));

                }
                return list;
            }
        } catch (Exception e) {
            e.printStackTrace();
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<CommentNotification> result) {

        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
