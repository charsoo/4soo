package ir.rasen.charsoo.model.business;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.helper.Hashtag;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebservicePOST;


/**
 * Created by android on 12/16/2014.
 */
public class RegisterBusiness extends AsyncTask<Void, Void, String> {
    private static final String TAG = "RegisterBusiness";

    private IWebservice delegate = null;
    private Business business;
    private ServerAnswer serverAnswer;
    private Context context;
    private int reqCode;
    private String accesstoken;

    public RegisterBusiness(Context context,Business business, IWebservice delegate,int reqCode,String accesstoken) {
        this.business = business;
        this.delegate = delegate;
        this.context = context;
        this.reqCode=reqCode;
        this.accesstoken=accesstoken;
    }
    public RequestObject.RegisterBiz CreateRegisterBusiness(){

        RequestObject obj= new RequestObject();
        RequestObject.RegisterBiz req = obj.new RegisterBiz();
        req.UserId=LoginInfo.getInstance().getUserUniqueId();
        req.BusinessId=business.businessIdentifier;
        req.Name=business.name;
        req.Email=business.email;
        req.CoverPicture=business.coverPicture;
        req.ProfilePicture=business.profilePicture;
        req.CategoryId=business.categoryUniqueId;
        req.SubCategoryId=business.subCategoryUniqueId;
        req.Description=business.description;
        req.WorkDays=null;
        req.WorkTimeOpen=null;
        req.WorkTimeClose=null;
        req.Phone=business.phone;
        req.StateId=business.stateUniqueId;
        req.CityId=business.cityUniqueId;
        req.Address=business.address;
        req.LocationLatitude=business.location_m.getLatitude();
        req.LocationLongitude=business.location_m.getLongitude();
        req.Mobile=business.mobile;
        req.Website=business.webSite;
        req.HashTagList=Hashtag.getStringFromList(business.hashtagList);
        return req;
    }
    @Override
    protected String doInBackground(Void... voids) {
        WebservicePOST webservicePOST = new WebservicePOST(URLs.REGISTER_BUSINESS,new ArrayList<>(Arrays.asList(accesstoken)));

        try {

             webservicePOST.postParam =
                     new Gson().toJson(CreateRegisterBusiness());

             serverAnswer = webservicePOST.execute(context);
            if (serverAnswer.getSuccessStatus()) {
                String busienssUniqueId = "0";
                JSONObject jsonObject = serverAnswer.getResult();
                if (jsonObject != null) {
                    busienssUniqueId = jsonObject.getString(Params.BUSINESS_ID_STRING);
                }
                return busienssUniqueId;

            } else
                return "0";
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {

        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
