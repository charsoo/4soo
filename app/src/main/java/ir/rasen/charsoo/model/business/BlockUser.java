package ir.rasen.charsoo.model.business;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ResultStatus;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebserviceGET;


/**
 * Created by android on 12/16/2014.
 */
public class   BlockUser extends AsyncTask<Void, Void, ResultStatus> {
    private static final String TAG = "BlockUser";

    private IWebservice delegate = null;
    private String businessUniqueId;
    private String userUniqueId;
    private ServerAnswer serverAnswer;
    private Context context;
    private int reqCode;
    private String accesstoken;

    public BlockUser(Context context,String businessUniqueId, String userUniqueId,IWebservice delegate,int reqCode,String accesstoken) {
        this.businessUniqueId = businessUniqueId;
        this.userUniqueId = userUniqueId;
        this.delegate = delegate;
        this.context = context;
        this.reqCode = reqCode;
        this.accesstoken=accesstoken;
    }

    @Override
    protected ResultStatus doInBackground(Void... voids) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.BLOCK_USER, new ArrayList<>(
                Arrays.asList(businessUniqueId
                        , userUniqueId,accesstoken)));


        try {
            serverAnswer = webserviceGET.execute(context);
            if (serverAnswer.getSuccessStatus()) {
                return ResultStatus.getResultStatus(serverAnswer);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ResultStatus result) {
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus()) {

                AnswerObjects.BooleanAnswer booleanAnswer = new Gson().fromJson(serverAnswer.getResult(true),
                        AnswerObjects.BooleanAnswer.class);

                delegate.getResult(reqCode,true);
            }
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
