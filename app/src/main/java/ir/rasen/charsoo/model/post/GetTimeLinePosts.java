package ir.rasen.charsoo.model.post;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION;
import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

/**
 * Created by android on 12/16/2014.
 */
public class GetTimeLinePosts extends AsyncTask<Void, Void, ArrayList<Post>> {
    private static final String TAG = "GetTimeLinePosts";
    private IWebservice delegate = null;
    private String userUniqueId;
    private int fromBusinessPost;
    private int limitation;
    private ServerAnswer serverAnswer;
    private Context context;
    private int reqCode;
    private String accesstoken;

    public GetTimeLinePosts(Context context, String userUniqueId, int fromBusinessPostId, int limitation, IWebservice delegate, int reqCode,String accesstoken) {
        this.userUniqueId = userUniqueId;
        fromBusinessPost=fromBusinessPostId;
        this.limitation = limitation;
        this.delegate = delegate;
        this.context = context;
        this.reqCode=reqCode;
        this.accesstoken=accesstoken;
    }

    @Override
    protected ArrayList<Post> doInBackground(Void... voids) {
        ArrayList<Post> list = new ArrayList<Post>();
        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_TIME_LINE_POSTS,new ArrayList<String>(
                Arrays.asList(userUniqueId,
                        String.valueOf(fromBusinessPost),
                        String.valueOf(limitation),
                        accesstoken)));


        try {
            serverAnswer = webserviceGET.executeList(context);
            if (serverAnswer.getSuccessStatus()) {
                JSONArray jsonArray = serverAnswer.getResultList();


                Gson gson= new Gson();
                AnswerObjects.GetTimeLineAllBusinessPosts[] gettimelineposts = gson.fromJson(serverAnswer.getResultList(true), AnswerObjects.GetTimeLineAllBusinessPosts[].class);

                for (int i = 0; i < jsonArray.length(); i++) {

                    try {
                        list.add(Post.getFromJSONObjectTimeLine(gettimelineposts[i]));
                    }
                    catch (Exception e){
                        Log.e(TAG, e.getMessage());
                    }
                }

                return list;
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<Post> result) {

        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
