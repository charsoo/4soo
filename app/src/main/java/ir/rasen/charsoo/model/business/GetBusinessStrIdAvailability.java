package ir.rasen.charsoo.model.business;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.google.gson.Gson;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;


/**
 * Created by android on 12/16/2014.
 */
public class GetBusinessStrIdAvailability extends AsyncTask<Void, Void, Boolean> {
    private static final String TAG = "GetBusinessStrIdAvailability";
    private IWebservice delegate = null;
    private String businessUniqueId;
    private ServerAnswer serverAnswer;
    private Context context;
    private int reqCode;
    private String accesstoken;

    public GetBusinessStrIdAvailability(Context context, String businessUniqueId, IWebservice delegate,int reqCode,String accesstoken) {
        this.businessUniqueId =businessUniqueId;
        this.delegate = delegate;
        this.context = context;
        this.reqCode=reqCode;
        this.accesstoken=accesstoken;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.CHECK_BUSINESS_IDENTIFIER,new ArrayList<>(
                Arrays.asList(businessUniqueId,accesstoken)));

        try {
            serverAnswer = webserviceGET.execute_new(context);

            if (serverAnswer.getSuccessStatus()){
                return true;
                // TODO: BAD AZ ESLAHE WEB SERVICE AVAILABILITY BAR ASASE PASOKHE SERVER SET SHAVAD
//                return serverAnswer.getResult().getBoolean(Params.IS_STR_ID_AVAILABLE);
            }

        } catch (Exception e) {
//            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR, TAG);
            }
            else if (serverAnswer.getSuccessStatus()) {
                try {
                    AnswerObjects.BooleanAnswer booleanAnswer = new Gson().fromJson(serverAnswer.getResult(true),
                            AnswerObjects.BooleanAnswer.class);
                    delegate.getResult(reqCode,!booleanAnswer.Result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else {
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    public void exectueWithNewSolution(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
