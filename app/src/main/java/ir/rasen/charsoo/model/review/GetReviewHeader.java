package ir.rasen.charsoo.model.review;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

/**
 * Created by Rasen_iman on 9/8/2015.
 */
public class GetReviewHeader extends AsyncTask<Void,Void,RequestObject.ReviewHeaderInfo>{
////{bizId}/{userId}/{accessToken}

    public static final String TAG = "GetReviewHeader";

    ServerAnswer serverAnswer;
    Context context;
    IWebservice iWebservice;
    String bizUnqId;
    String userUnqId;
    String accessToken;
    int reqCode;

    public GetReviewHeader(Context ctx,IWebservice iWebservice,String bizUnqId,
                           String userUnqId,int reqCode){
        this.context = ctx;
        this.bizUnqId = bizUnqId;
        this.userUnqId = userUnqId;
        this.accessToken = LoginInfo.getInstance().userAccessToken;
        this.iWebservice = iWebservice;
        this.reqCode = reqCode;
    }

    @Override
    protected RequestObject.ReviewHeaderInfo doInBackground(Void... params) {
        WebserviceGET webserviceGET =
                new WebserviceGET(URLs.GET_REVIEW_HEADER,
                        new ArrayList<>(Arrays.asList(bizUnqId,userUnqId,accessToken)));


        try {
            serverAnswer =
                    webserviceGET.execute(context);

            return new Gson().fromJson(serverAnswer.getResult(true),
                            RequestObject.ReviewHeaderInfo.class);

        } catch (Exception e) {
        }


        return null;
    }

    @Override
    protected void onPostExecute(RequestObject.ReviewHeaderInfo reviewHeaderInfo) {
        try {
            if (serverAnswer == null) {
                iWebservice.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                iWebservice.getResult(reqCode,reviewHeaderInfo);
            else
                iWebservice.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }

}
