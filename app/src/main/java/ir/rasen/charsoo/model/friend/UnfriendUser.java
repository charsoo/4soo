package ir.rasen.charsoo.model.friend;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.ICancelFriendship;

import static android.os.Build.VERSION;
import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class UnfriendUser extends AsyncTask<Void, Void, Boolean> {
    private static final String TAG = "UnfriendUser";

    //private IWebserviceResponse delegate = null;
    private String applicatorUserUniqueId;
    private String requestedUserUniqueId;
    private ServerAnswer serverAnswer;
    private Context context;
    private ICancelFriendship delegate;
    private String accesstoken;

    public UnfriendUser(Context context, String applicatorUserUniqueId, String requestedUserUniqueId, ICancelFriendship delegate,String accesstoken) {
        this.applicatorUserUniqueId = applicatorUserUniqueId;
        this.requestedUserUniqueId = requestedUserUniqueId;
        //this.delegate = delegate;
        this.context = context;
        this.delegate=delegate;
        this.accesstoken=accesstoken;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.UNFRIEND_USER, new ArrayList<>(
                Arrays.asList(String.valueOf(applicatorUserUniqueId),
                        String.valueOf(requestedUserUniqueId),accesstoken)));

        try {
            serverAnswer = webserviceGET.execute_new(context);
            if (serverAnswer.getSuccessStatus()){
                AnswerObjects.BooleanAnswer answer= new Gson().fromJson(serverAnswer.getResult(true),
                        AnswerObjects.BooleanAnswer.class);
                return answer.Result;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        try {
            if (serverAnswer == null) {
                delegate.notifyDeleteFriendFailed(ServerAnswer.getError(context,ServerAnswer.EXECUTION_ERROR,TAG));
            } else if (serverAnswer.getSuccessStatus()) {
                if (result)
                    delegate.notifyDeleteFriend(requestedUserUniqueId);
                else
                    delegate.notifyDeleteFriendFailed("Try Again");
            } else {
                delegate.notifyDeleteFriendFailed(ServerAnswer.getError(context, serverAnswer.getErrorCode(),TAG));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*if (serverAnswer == null) {
            delegate.getError(ServerAnswer.EXECUTION_ERROR);
            return;
        }
        if (serverAnswer.getSuccessStatus()) {
            delegate.getResult(result);
            iCancelFriendship.notifyDeleteFriend(requestedUserUniqueId);
        }
        else
            delegate.getError(serverAnswer.getErrorCode());*/
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
