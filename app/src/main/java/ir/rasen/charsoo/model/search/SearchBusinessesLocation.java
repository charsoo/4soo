package ir.rasen.charsoo.model.search;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.SearchItemBusiness;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebserviceGET;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class SearchBusinessesLocation extends AsyncTask<Void, Void, ArrayList<SearchItemBusiness>> {
    private static final String TAG = "SearchBusinessesLocation";

    private IWebservice delegate = null;

    //private int uniqueId;
    private String searchText;
    private String location_latitude;
    private String location_longitude;
    private String subcategoryUniqueId;
    private ServerAnswer serverAnswer;
    private int beforThisId;
    private int limitataion;
    private Context context;
    private String accesstoken;
    private int reqCode;

    public SearchBusinessesLocation
            (Context context, String searchText,String subcategoryUniqueId,
             String location_latitude,String location_longitude,
             int beforThisId,int limitation,IWebservice delegate,int reqCode,String accesstoken) {
        //this.uniqueId = uniqueId;
        this.searchText = searchText;
        this.location_latitude = location_latitude;
        this.location_longitude = location_longitude;
        this.delegate = delegate;
        this.subcategoryUniqueId = subcategoryUniqueId;
        this.beforThisId = beforThisId;
        this.limitataion = limitation;
        this.context = context;
this.accesstoken=accesstoken;
        this.reqCode = reqCode;
    }

    @Override
    protected ArrayList<SearchItemBusiness> doInBackground(Void... voids) {
        ArrayList<SearchItemBusiness> list = new ArrayList<>();

        WebserviceGET webserviceGET = new WebserviceGET(URLs.SEARCH_BUSINESS_LOCATION,new ArrayList<>(
                Arrays.asList( searchText, subcategoryUniqueId,
                        location_latitude,location_longitude,
                        String.valueOf(beforThisId),String.valueOf(limitataion),accesstoken)));

        try {
            serverAnswer = webserviceGET.executeList(context);

            if (serverAnswer.getSuccessStatus()) {
                JSONArray jsonArray = serverAnswer.getResultList();

                Gson gson= new Gson();
                AnswerObjects.SearchBusinessesLocation[] searchbusinesslocation = gson.fromJson(serverAnswer.getResultList(true), AnswerObjects.SearchBusinessesLocation[].class);

                for (int i = 0; i < jsonArray.length(); i++) {

                    list.add(new SearchItemBusiness(context.getResources(),searchbusinesslocation[i].BusinessId,
                            searchbusinesslocation[i].BusinessProfilePictureId,
                            searchbusinesslocation[i].BusinessUserName,
                            searchbusinesslocation[i].Distance));
                }
                return list;
            }

        } catch (Exception e) {
//            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;

    }

    @Override
    protected void onPostExecute(ArrayList<SearchItemBusiness> result) {


        //if webservice.executeWithNewSolution(); throws exception

        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
