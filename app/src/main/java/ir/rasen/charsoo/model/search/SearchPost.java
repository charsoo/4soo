package ir.rasen.charsoo.model.search;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.SearchItemPost;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class SearchPost extends AsyncTask<Void, Void, ArrayList<SearchItemPost>> {
    private static final String TAG = "SearchPost";

    private IWebservice delegate = null;

    private String searchText;
    private ServerAnswer serverAnswer;
    private int beforeThisId;
    private int limitation;
    private Context context;
    private String accesstoken;
    private int reqCode;

    public SearchPost(Context context,String searchText,int beforeThisId,int limitation,IWebservice delegate,int reqCode,String accesstoken) {
        this.searchText = searchText;
        this.delegate = delegate;
        this.beforeThisId = beforeThisId;
        this.limitation = limitation;
        this.context = context;
        this.accesstoken=accesstoken;
        this.reqCode = reqCode;
    }

    @Override
    protected ArrayList<SearchItemPost> doInBackground(Void... voids) {
        ArrayList<SearchItemPost> list = new ArrayList<>();

        WebserviceGET webserviceGET = new WebserviceGET(URLs.SEARCH_POST,new ArrayList<>(
                Arrays.asList(searchText,String.valueOf(beforeThisId),String.valueOf(limitation),accesstoken)));


        try {
            serverAnswer = webserviceGET.executeList(context);
            if (serverAnswer.getSuccessStatus()) {
                JSONArray jsonArray = serverAnswer.getResultList();


                Gson gson= new Gson();
                AnswerObjects.SearchPost[] searchpost = gson.fromJson(serverAnswer.getResultList(true), AnswerObjects.SearchPost[].class);

                for (int i = 0; i < jsonArray.length(); i++) {

                    Post post =new Post();
//                    post.businessUniqueId
                    post.uniqueId =searchpost[i].PostId;
                    post.pictureUniqueId =searchpost[i].PostPictureId;
                    post.picture="";
                    list.add(new SearchItemPost(post));
                }
                return list;
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<SearchItemPost> result) {

        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
