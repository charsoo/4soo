package ir.rasen.charsoo.model;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.State;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by charsoo on 12/16/2014.
 */
public class GetCountryStates extends AsyncTask<Void, Void, ArrayList<State>> {
    private static final String TAG = "GetCountryStates";
    private IWebservice delegate = null;
    private ServerAnswer serverAnswer;
    private Context context;
    private int reqCode;
    private String accesstoken;
;

    public GetCountryStates(Context context, IWebservice delegate,int reqCode,String accesstoken){
        this.delegate = delegate;
        this.context = context;
        this.reqCode=reqCode;
        this.accesstoken=accesstoken;
    }
    @Override
    protected ArrayList<State> doInBackground(Void... voids) {
        ArrayList<State> statesList = new ArrayList<>();

        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_COUNTRY_STATES,new ArrayList<>(Arrays.asList(accesstoken)));
        try {
//            return null;
            // TODO Fetch States List From Server
            serverAnswer = webserviceGET.executeList(context);
            if (serverAnswer!=null){
                if (serverAnswer.getSuccessStatus()){
                    JSONArray resultList=serverAnswer.getResultList();
                    Gson gson = new Gson();
                    AnswerObjects.State[] states = gson.fromJson(serverAnswer.getResultList(true),
                            AnswerObjects.State[].class);
                    for (int i = 0; i < resultList.length(); i++) {
                        statesList.add(new State(states[i].Id,states[i].Name));
                    }
                    return statesList;
                }
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<State> states) {
//        ArrayList<String> list=new ArrayList<>();
//        list.addAll(Arrays.asList(context.getResources().getStringArray(R.array.states)));
//        delegate.getResult(reqCode,);

        // TODO Fetch From Server
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (!serverAnswer.getSuccessStatus())
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
            else
                delegate.getResult(reqCode,states);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
