package ir.rasen.charsoo.model.business;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ResultStatus;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebServiceDeleteItem;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

/**
 * Created by ASUS on 9/5/2015.
 */
public class GetImageFromGallery extends AsyncTask<Void, Void, Business>{
    private static final String TAG = "GetImageFromGallery";
    private String userid;
    private String accesstoken;
    private String  galid;
    private ServerAnswer serverAnswer;
    private Context context;
    private IWebServiceDeleteItem delegate = null;
    private int reqCode;

    public GetImageFromGallery(Context context,String galid,IWebServiceDeleteItem delegate,String accesstoken){
        this.context=context;
        this.galid=galid;
        this.accesstoken=accesstoken;
        this.delegate=delegate;
    }


    @Override
    protected Business doInBackground(Void... voids) {
        Business business = new Business();
        WebserviceGET webserviceGET = new WebserviceGET(URLs.GALLERY_ID,
                new ArrayList<String>(Arrays.asList(LoginInfo.getInstance().getUserUniqueId(),
                        galid, accesstoken)));

        try {
            serverAnswer = webserviceGET.execute(context);
            if (serverAnswer.getSuccessStatus()){
                Gson gson = new Gson();
                String res=serverAnswer.getResult(true);
                AnswerObjects.ImageFromGallery imageFromGallery = gson.fromJson(res,AnswerObjects.ImageFromGallery.class);
                business.galleryId=imageFromGallery.GalleryItemId;
                business.uniqueId=imageFromGallery.BizId;
                business.isowner=imageFromGallery.IsOwner;
                business.name=imageFromGallery.UserName;
                business.profilePictureUniqueId=imageFromGallery.ImageId;
                business.PublisherUserId=imageFromGallery.PublisherUserId;
                business.LastUpdate=imageFromGallery.LastUpdate;
                business.UserImageId=imageFromGallery.UserImageId;

                return business;
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }



    @Override
    protected void onPostExecute(Business result) {

        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG,0);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result,0);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG,0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
