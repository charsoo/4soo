package ir.rasen.charsoo.model.post;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ResultStatus;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.view.interface_m.IDeletePost;
import ir.rasen.charsoo.model.WebserviceGET;

import static android.os.Build.VERSION;
import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class DeletePost extends AsyncTask<Void, Void, ResultStatus> {
    private static final String TAG = "DeletePost";

//    private IWebserviceResponse delegate = null;
    private String businessUniqueId;
    private String postUniqueId;
    private ServerAnswer serverAnswer;
    private Context context;
    private IDeletePost delegate;
    private String accesstoken;


    public DeletePost(Context context,String businessUniqueId, String postUniqueId,IDeletePost delegate,String accesstoken) {
        this.businessUniqueId = businessUniqueId;
        this.postUniqueId = postUniqueId;
        this.context = context;
//        this.iDeletePost = iDeletePost;
        this.delegate = delegate;
        this.accesstoken=accesstoken;
    }

    @Override
    protected ResultStatus doInBackground(Void... voids) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.DELETE_POST,new ArrayList<>(
                Arrays.asList(String.valueOf(businessUniqueId),String.valueOf(postUniqueId),accesstoken)));

        try {
            serverAnswer = webserviceGET.execute_new(context);
            if (serverAnswer.getSuccessStatus()){
                AnswerObjects.BooleanAnswer answer= new Gson().fromJson(serverAnswer.getResult(true),
                        AnswerObjects.BooleanAnswer.class);
                return ResultStatus.getResultStatus(serverAnswer);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ResultStatus result) {

//        if (serverAnswer.getSuccessStatus())
//            iDeletePost.notifyDeletePost(postUniqueId);

        try {
            if (serverAnswer == null) {
                delegate.notifyDeleteFailed(postUniqueId,ServerAnswer.getError(context,ServerAnswer.EXECUTION_ERROR,TAG));
            } else if (serverAnswer.getSuccessStatus()) {
                    boolean successStatus=serverAnswer.getResult().getBoolean(Params.RESULT);
                    if (successStatus)
                        delegate.notifyDeletePost(postUniqueId);
                    else
                        delegate.notifyDeleteFailed(postUniqueId,"Try Again!");
            } else {
                delegate.notifyDeleteFailed(postUniqueId,ServerAnswer.getError(context,serverAnswer.getErrorCode(), TAG));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //if webservice.executeWithNewSolution(); throws exception
       /* if (serverAnswer == null) {
            delegate.getError(ServerAnswer.EXECUTION_ERROR);
            return;
        }
        if (serverAnswer.getSuccessStatus())
            delegate.getResult(result);
        else
            delegate.getError(serverAnswer.getErrorCode());*/
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
