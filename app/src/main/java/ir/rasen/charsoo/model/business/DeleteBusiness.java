package ir.rasen.charsoo.model.business;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.ResultStatus;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebserviceGET;


/**
 * Created by android on 12/16/2014.
 */
public class DeleteBusiness extends AsyncTask<Void, Void, ResultStatus> {
    private static final String TAG = "DeleteBusiness";

    private IWebservice delegate = null;
    private String businessUniqueId;
    private String userUniqueId;
    private ServerAnswer serverAnswer;
    private Context context;


    public DeleteBusiness(Context context,String userUniqueId, String businessUniqueId) {
        //this.delegate = delegate;
        this.businessUniqueId = businessUniqueId;
        this.userUniqueId = userUniqueId;
        this.context = context;
    }

    @Override
    protected ResultStatus doInBackground(Void... voids) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.BLOCK_BUSINESS, new ArrayList<>(
                Arrays.asList(businessUniqueId, userUniqueId)));

        try {
            //TODO:NABAYAD DELETE DASHTE BASHIM
//            serverAnswer = webserviceGET.execute(context);
            if (serverAnswer.getSuccessStatus()){
                return ResultStatus.getResultStatus(serverAnswer);}
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ResultStatus result) {
        if (serverAnswer != null && serverAnswer.getSuccessStatus())
            ;
        /*if (serverAnswer == null) {
            delegate.getError(ServerAnswer.EXECUTION_ERROR);
            return;
        }
        if (serverAnswer.getSuccessStatus())
            delegate.getResult(result);
        else
            delegate.getError(serverAnswer.getErrorCode());*/
    }
    public void exectueWithNewSolution(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
