package ir.rasen.charsoo.model.business;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.Category;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;


/**
 * Created by android on 12/16/2014.
 */
public class GetBusinessCategories extends AsyncTask<Void, Void, ArrayList<Category>> {
    private static final String TAG = "GetBusinessCategories";
    private IWebservice delegate = null;
    private ServerAnswer serverAnswer;
    private int request;
    private Context context;
    public String accesstoken;

    public GetBusinessCategories(Context context, IWebservice delegate, int request,String accesstoken){
        this.delegate = delegate;
        this.context = context;
        this.request = request;
        this.accesstoken=accesstoken;
    }
    @Override
    protected ArrayList<Category> doInBackground(Void... voids) {
        ArrayList<Category> list = new ArrayList<Category>();

        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_BUSINESS_CATEGORIES,new ArrayList<>(Arrays.asList(accesstoken)));
        try {
            serverAnswer = webserviceGET.executeList(context);
            if (serverAnswer.getSuccessStatus()) {
                JSONArray jsonArray = serverAnswer.getResultList();
                Gson gson = new Gson();
                AnswerObjects.Category[] categories = gson.fromJson(serverAnswer.getResultList(true),
                        AnswerObjects.Category[].class);
                for (int i = 0; i < jsonArray.length(); i++) {

                    //TODO remove test part
                    //list.add(new Category(jsonObject.getString(Params.ID),jsonObject.getString(Params.CATEGORY)));

                    //for the test. getBusinessCategory doesn't return categoryName.uniqueId by the now!
                    list.add(
                            new Category(
                                    categories[i].CategoryId,
                                    categories[i].Category));
                }
                return list;
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<Category> result) {
        try {
            if (serverAnswer == null) {
                delegate.getError(request, ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (!serverAnswer.getSuccessStatus())
                delegate.getError(request, serverAnswer.getErrorCode(),TAG);
            else
                delegate.getResult(request, result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
