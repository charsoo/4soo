package ir.rasen.charsoo.model.friend;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;

import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.ContactEntry;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.model.WebservicePOST;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class TakeContactList extends AsyncTask<Void, Void, ArrayList<ContactEntry>> {
    private static final String TAG = "TakeContactList";

    private IWebservice delegate = null;
    private String userUniqueId;
    private ServerAnswer serverAnswer;
    private Context context;
    private Hashtable<String,ArrayList<ContactEntry>> contactsWithNumber,contactsWithEmail;
    private Hashtable<String,ArrayList<ContactEntry>> addedEmails,addedPhoneNumbers;
    String emails,phoneNumbers;
    private int reqCode;
    private String accesstoken;


    public TakeContactList(Context context, String userUniqueId,Hashtable<String,ArrayList<ContactEntry>> phoneNumberContacts,Hashtable<String,ArrayList<ContactEntry>> emailContacts, IWebservice delegate,int reqCode,String accesstoken) {
        this.userUniqueId = userUniqueId;
        contactsWithNumber=phoneNumberContacts;
        contactsWithEmail=emailContacts;
        this.delegate = delegate;
        this.context = context;
        this.reqCode=reqCode;
        this.accesstoken=accesstoken;
    }

    public RequestObject.TakeContactList createTakeContactList(){
        RequestObject obj = new RequestObject();
        RequestObject.TakeContactList req = obj.new TakeContactList();


        req.UserId = userUniqueId;
        req.Emails = emails;
        req.PhoneNumbers = phoneNumbers;

        return req;
    }
    @Override
    protected ArrayList<ContactEntry> doInBackground(Void... voids) {
        Hashtable<String,ContactEntry> tempSeenResultTable=new Hashtable<>(),tempUnseenResultTable=new Hashtable<>();
        ArrayList<ContactEntry> tempSeenResultList=new ArrayList<>(),tempUnseenResultList=new ArrayList<>();
        
        WebservicePOST webservicePOST = new WebservicePOST(URLs.TAKE_CONTACT_LIST,
                new ArrayList<>(Arrays.asList(accesstoken)));

        try {
            
            parsData();

            webservicePOST.postParam = new Gson().toJson(createTakeContactList());

            serverAnswer = webservicePOST.executeList(context);

//            // TODO: yek Arayeye Sort shode tahvil bedim.
            if (serverAnswer.getSuccessStatus()){
                JSONArray jsonArray=serverAnswer.getResultList();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                    ContactEntry contactEntry=ContactEntry.getContactEntryFromJSON(jsonObject);
                    contactEntry.namesMatchThisContactData=new ArrayList<>();
                    if (contactEntry.type.equals(ContactEntry.ContactType.Email)){
                        for (int j = 0; j < addedEmails.get(contactEntry.contactData).size() ; j++) {
                            contactEntry.namesMatchThisContactData.add(addedEmails.get(contactEntry.contactData).get(j).fullName);
                        }
                    }
                    else
                    {
                        for (int j = 0; j < addedPhoneNumbers.get(contactEntry.contactData).size() ; j++) {
                            contactEntry.namesMatchThisContactData.add(addedPhoneNumbers.get(contactEntry.contactData).get(j).fullName);
                        }
                    }

                    if (contactEntry.isSeenBefore){
                        tempSeenResultTable.put(contactEntry.charsooStringId,contactEntry);
                    }
                    else{
                        tempUnseenResultTable.put(contactEntry.charsooStringId,contactEntry);
                    }
                }

                ArrayList<String> nameList=new ArrayList(tempUnseenResultTable.keySet());
                Collections.sort(nameList);
                for (int i = 0; i < nameList.size(); i++) {
                    tempUnseenResultList.add(tempUnseenResultTable.get(nameList.get(i)));
                }
                nameList=new ArrayList(tempSeenResultTable.keySet());
                Collections.sort(nameList);
                for (int i = 0; i < nameList.size(); i++) {
                    tempSeenResultList.add(tempSeenResultTable.get(nameList.get(i)));
                }
                tempUnseenResultList.addAll(tempSeenResultList);
                return tempUnseenResultList;

            }
        } catch (Exception e) {
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<ContactEntry> result) {

        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void parsData(){
        addedEmails=new Hashtable<>();
        addedPhoneNumbers=new Hashtable<>();
        emails="";
        phoneNumbers="";
        for (String str:contactsWithNumber.keySet())
        {
            for (int i=0;i<contactsWithNumber.get(str).size();i++){
                String contactData=contactsWithNumber.get(str).get(i).contactData;
                if (addedPhoneNumbers.containsKey(contactData)){
                    addedPhoneNumbers.get(contactData).add(contactsWithNumber.get(str).get(i));
                }
                else{
                    addedPhoneNumbers.put(contactData,new ArrayList<>(Arrays.asList(contactsWithNumber.get(str).get(i))));
                    phoneNumbers+=contactData;
                    phoneNumbers+=",";
                }

            }

        }
        for (String str:contactsWithEmail.keySet())
        {
            for (int i = 0; i < contactsWithEmail.get(str).size() ; i++) {
                String contactData=contactsWithEmail.get(str).get(i).contactData;
                if (addedEmails.containsKey(contactData))
                {
                    addedEmails.get(contactData).add(contactsWithEmail.get(str).get(i));
                }
                else{
                    addedEmails.put(contactData,new ArrayList<>(Arrays.asList(contactsWithEmail.get(str).get(i))));
                    emails+=contactData;
                    emails+=",";
                }
            }

        }
        if (emails.length()>0) {
            emails = emails.substring(0, emails.length() - 1);
        }
        if (phoneNumbers.length()>0) {
            phoneNumbers = phoneNumbers.substring(0, phoneNumbers.length() - 1);
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }

}
