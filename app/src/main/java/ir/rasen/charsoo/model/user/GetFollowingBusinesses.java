package ir.rasen.charsoo.model.user;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.BaseAdapterItem;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebserviceGET;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */

public class GetFollowingBusinesses extends AsyncTask<Void, Void, ArrayList<BaseAdapterItem>> {
    private static final String TAG = "GetFollowingBusinesses";
    private String accesstoken;
    private IWebservice delegate = null;
    private String userUniqueId;
    private ServerAnswer serverAnswer;
    private Context context;

    private int reqCode;

    public GetFollowingBusinesses(Context context,String userUniqueId,IWebservice delegate,int reqCode,String accesstoken) {
        this.userUniqueId = userUniqueId;
        this.delegate = delegate;
        this.context = context;
this.accesstoken=accesstoken;
        this.reqCode = reqCode;
    }

    @Override
    protected ArrayList<BaseAdapterItem> doInBackground(Void... voids) {
        ArrayList<BaseAdapterItem> list = new ArrayList();

        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_FOLLOWING_BUSINESSES, new ArrayList<>(
                Arrays.asList(userUniqueId,accesstoken)));

        try {
            serverAnswer = webserviceGET.executeList(context);
            if (serverAnswer.getSuccessStatus()) {
                JSONArray jsonArray = serverAnswer.getResultList();
                Gson gson = new Gson();
                AnswerObjects.BusinessFollowing[] businessFollowing = gson.fromJson(serverAnswer.getResultList(true),AnswerObjects.BusinessFollowing[].class);
                for (int i = 0; i < jsonArray.length(); i++) {
                    list.add(new BaseAdapterItem(context.getResources(),businessFollowing[i].BusinessId,
                            businessFollowing[i].SearchPictureId,
                            businessFollowing[i].BusinessName));
                }
                return list;
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<BaseAdapterItem> result) {

        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
