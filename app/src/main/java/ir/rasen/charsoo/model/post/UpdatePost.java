package ir.rasen.charsoo.model.post;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import java.lang.reflect.AccessibleObject;
import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.controller.helper.Hashtag;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebservicePOST;
import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class UpdatePost extends AsyncTask<Void, Void, Post> {
    private static final String TAG = "UpdatePost";

    private IWebservice delegate = null;
    private Post post;
    private ServerAnswer serverAnswer;
    private Context context;
    private int reqCode;
    private String accesstoken;

    public UpdatePost(Context context,Post post,IWebservice delegate,int reqCode,String accesstoken) {
        this.post = post;
        this.delegate = delegate;
        this.context = context;
        this.reqCode=reqCode;
      this.accesstoken=accesstoken;
    }

    public RequestObject.UpdatePost createUpdatePost(){
        RequestObject obj = new RequestObject();
        RequestObject.UpdatePost req = obj.new UpdatePost();

        req.PostId = post.uniqueId;
        req.Title = post.title;
        req.Description = post.description;
        req.Price = post.price;
        req.Code = post.code;
        req.HashTagList = Hashtag.getStringFromList(post.hashtagList);
        req.Discount = "0";

        return req;
    }
    @Override
    protected Post doInBackground(Void... voids) {
        WebservicePOST webservicePOST = new WebservicePOST(URLs.UPDATE_POST,new ArrayList<>(Arrays.asList(accesstoken)));

        try {
            webservicePOST.postParam = new Gson().toJson(createUpdatePost());

            serverAnswer = webservicePOST.execute(context);
            if (serverAnswer.getSuccessStatus())
                return post;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Post result) {


        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }

}
