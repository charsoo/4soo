package ir.rasen.charsoo.model.review;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Arrays;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

/**
 * Created by Rasen_iman on 9/7/2015.
 */
public class GetOverallReviewStatus extends AsyncTask<Void,Void,ArrayList<RequestObject.RevStatisticInfo>> {
    public static final String TAG = "GetOverallReviewStatus";

    IWebservice iWebservice;
    AnswerObjects.ServerAnswer serverAnswer;
    int reqCode;
    String bizUniqueId;
    Context context;
    String accessToken;

    public GetOverallReviewStatus(Context ctx,String bizUniqueId,IWebservice iWebservice,int reqCode){
        this.context = ctx;
        this.reqCode = reqCode;
        this.bizUniqueId = bizUniqueId;
        this.accessToken = LoginInfo.getInstance().userAccessToken;
        this.iWebservice = iWebservice;
    }

    @Override
    protected ArrayList<RequestObject.RevStatisticInfo> doInBackground(Void... params) {
        WebserviceGET webserviceGET =
                new WebserviceGET(URLs.GET_OVERALL_REVIEW_STATUS,
                        new ArrayList<>(Arrays.asList(bizUniqueId,accessToken)));

        try {
            serverAnswer = webserviceGET.executeP(context);

            RequestObject.RevStatisticInfo[] revStatisticInfos
                    = new Gson().fromJson(serverAnswer.Result,
                    RequestObject.RevStatisticInfo[].class);

            return new ArrayList<>(Arrays.asList(revStatisticInfos));
        } catch (Exception e) {
        }
        return null;
    }


    @Override
    protected void onPostExecute(ArrayList<RequestObject.RevStatisticInfo> revStatisticInfos){
        try {
            if (serverAnswer == null) {
                iWebservice.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.SuccessStatus)
                iWebservice.getResult(reqCode,revStatisticInfos);
            else
                iWebservice.getError(reqCode,Integer.valueOf(serverAnswer.Error.ErrorCode),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }

}