package ir.rasen.charsoo.model.friend;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebserviceGET;

import static android.os.Build.VERSION;
import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class RequestFriendship extends AsyncTask<Void, Void, Boolean> {
    private static final String TAG = "RequestFriendship";

    private IWebservice delegate = null;
    private String applicatorUserUniqueId;
    private String requestedUserUniqueId;
    private ServerAnswer serverAnswer;
    private Context context;
    private int reqCode;
    private String accesstoken;

    public RequestFriendship(Context context,String applicatorUserUniqueId, String requestedUserUniqueId,IWebservice delegate,int reqCode,String accesstoken) {
        this.applicatorUserUniqueId = applicatorUserUniqueId;
        this.requestedUserUniqueId = requestedUserUniqueId;
        this.delegate = delegate;
        this.context = context;
        this.reqCode=reqCode;
        this.accesstoken=accesstoken;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.REQUEST_FRIENDSHIP, new ArrayList<>(
                Arrays.asList(applicatorUserUniqueId,
                        requestedUserUniqueId,accesstoken)));

        try {
            serverAnswer = webserviceGET.execute_new(context);
            if (serverAnswer.getSuccessStatus()){
                return  new Gson().fromJson(serverAnswer.getResult(true), AnswerObjects.BooleanAnswer.class).Result;
            }
               //
               // return serverAnswer.getResult().getBoolean(Params.RESULT);

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {

        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR, TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus()) {
                if (result) {
                    delegate.getResult(reqCode, result);
                } else {
                    delegate.getError(reqCode,ServerAnswer.NONE_DEFINED_ERROR,TAG);
                }

            }else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
