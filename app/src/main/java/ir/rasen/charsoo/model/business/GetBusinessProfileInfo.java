package ir.rasen.charsoo.model.business;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.WorkDays;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.helper.Hashtag;
import ir.rasen.charsoo.controller.helper.Location_M;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebserviceGET;


/**
 * Created by android on 12/16/2014.
 */
public class GetBusinessProfileInfo extends AsyncTask<Void, Void, Business> {
    private static final String TAG = "GetBusinessProfileInfo";
    private IWebservice delegate = null;
    private String businessUniqueId;
    private ServerAnswer serverAnswer;
    private Context context;
    private String accesstoken;
    private int reqCode;

    public GetBusinessProfileInfo(Context context,String businessUniqueId,IWebservice delegate,int reqCode,String accesstoken) {
        this.delegate = delegate;
        this.businessUniqueId = businessUniqueId;
        this.context = context;
        this.reqCode = reqCode;
        this.accesstoken=accesstoken;
    }

    @Override
    protected Business doInBackground(Void... voids) {
        Business business = new Business();
        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_BUSINESS_PROFILE_INFO, new ArrayList<>(
                Arrays.asList(String.valueOf(businessUniqueId),accesstoken)));

        try {

            serverAnswer = webserviceGET.execute(context);

            if (serverAnswer.getSuccessStatus()) {
                Gson gson = new Gson();
                AnswerObjects.BusinessInfo businessInfo = gson.fromJson(serverAnswer.getResult(true)
                        ,AnswerObjects.BusinessInfo.class);
                business.uniqueId = businessUniqueId;
                business.name = businessInfo.Name;
                try{
                    business.profilePictureUniqueId = businessInfo.ProfilePicture;
                }
                catch (Exception e){
                    business.profilePictureUniqueId ="-1";
                }
                business.coverPicture = businessInfo.CoverPicture;
                business.categoryName = businessInfo.Category;
                business.subcategoryName = businessInfo.SubCategory;
                business.description = businessInfo.Description;

                //************************WORK TIME******************************/
                WorkDays workDays = new WorkDays();
                AnswerObjects.WorkingTimes[] workTimes =
                        new Gson().fromJson(businessInfo.WorkItems,AnswerObjects.WorkingTimes[].class);
                ArrayList<WorkDays.DayTime> dayTimes = new ArrayList<>();
                for(int i=0;i<workTimes.length;i++){
                    WorkDays days = new WorkDays();
                    WorkDays.Time openTime = days.new Time(workTimes[i].Oht,workTimes[i].Omt);
                    WorkDays.Time closeTime = days.new Time(workTimes[i].Cht,workTimes[i].Cmt);

                    WorkDays.DayTime dt = days.new DayTime(
                            workTimes[i].Day,
                            openTime,
                            closeTime
                            );
                    dayTimes.add(dt);
                }
                workDays.setDayTimes(dayTimes);

                business.workDays = workDays;
                //************************WORK TIME******************************/


                business.phone = (businessInfo.Phone==null)?"":businessInfo.Phone;
                business.stateName = businessInfo.State;
                business.cityName = businessInfo.City;
                business.address = businessInfo.Address;
                business.location_m = new Location_M(businessInfo.LocationLatitude,
                       businessInfo.LocationLongitude);
                business.email = (businessInfo.Email==null)?"":businessInfo.Email;
                business.webSite = (businessInfo.Website==null)?"":businessInfo.Website;
                business.mobile = (businessInfo.Mobile==null)?"":businessInfo.Mobile;
                business.hashtagList = Hashtag.getListFromString(businessInfo.HashTagList);
                business.IdInstagram = (businessInfo.InstaPage == null) ?"" : businessInfo.InstaPage;

                return business;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Business result) {


        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
