package ir.rasen.charsoo.model.comment;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.Comment;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.view.interface_m.ICommentChange;
import ir.rasen.charsoo.model.WebserviceGET;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class UpdateComment extends AsyncTask<Void, Void, Boolean> {
    private static final String TAG = "UpdateComment";

    private Comment comment;
    private ServerAnswer serverAnswer;
    private ICommentChange iCommentChange = null;
    private Context context;
    private String accesstoken;

    public UpdateComment(Context context, Comment comment, ICommentChange iCommentChange,String accesstoken) {

        this.comment = comment;
        this.iCommentChange = iCommentChange;
        this.context = context;
        this.accesstoken=accesstoken;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.UPDATE_COMMENT, new ArrayList<>(
                Arrays.asList(comment.ownerUniqueId,comment.uniqueId,comment.text,String.valueOf(comment.isUserSendedComment),accesstoken)));

        try {
            serverAnswer = webserviceGET.execute_new(context);
            if (serverAnswer.getSuccessStatus())
            {
                AnswerObjects.BooleanAnswer answer= new Gson().fromJson(serverAnswer.getResult(true),
                        AnswerObjects.BooleanAnswer.class);
                return answer.Result;
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {

        try {
            if(serverAnswer == null)
                iCommentChange.notifyUpdateCommentFailed(comment.uniqueId,ServerAnswer.getError(context,ServerAnswer.EXECUTION_ERROR,TAG));
            else if(serverAnswer.getSuccessStatus()){
                if(result)
                    iCommentChange.notifyUpdateComment(comment);
                else
                    iCommentChange.notifyUpdateCommentFailed(comment.uniqueId,ServerAnswer.getError(context,ServerAnswer.EXECUTION_ERROR,TAG));

            }else
                iCommentChange.notifyUpdateCommentFailed(comment.uniqueId,ServerAnswer.getError(context,serverAnswer.getErrorCode(),TAG));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
