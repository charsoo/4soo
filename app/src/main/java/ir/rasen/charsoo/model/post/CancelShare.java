package ir.rasen.charsoo.model.post;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ResultStatus;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.view.interface_m.IShareCancelShareListener;
import ir.rasen.charsoo.view.interface_m.IUpdateTimeLine;
import ir.rasen.charsoo.model.WebserviceGET;

import static android.os.Build.VERSION;
import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class CancelShare extends AsyncTask<Void, Void, ResultStatus> {
    private static final String TAG = "CancelShare";

//    private IWebserviceResponse delegate = null;
    private IShareCancelShareListener delegate;
    private String userUniqueId;
    private String postUniqueId;
    private ServerAnswer serverAnswer;
    private Context context;
    private IUpdateTimeLine iUpdateTimeLine;
    private String accesstoken;

    public CancelShare(Context context,String userUniqueId, String postUniqueId,IUpdateTimeLine iUpdateTimeLine,IShareCancelShareListener delegate,String accesstoken) {
        this.delegate = delegate;
        this.userUniqueId = userUniqueId;
        this.postUniqueId = postUniqueId;
        this.context = context;
        this.iUpdateTimeLine = iUpdateTimeLine;
        this.accesstoken=accesstoken;
    }

    @Override
    protected ResultStatus doInBackground(Void... voids) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.CANCEL_SHARE, new ArrayList<>(
                Arrays.asList(userUniqueId, postUniqueId,accesstoken)));


        try {

            serverAnswer = webserviceGET.execute_new(context);
            if (serverAnswer.getSuccessStatus()){
                AnswerObjects.BooleanAnswer answer= new Gson().fromJson(serverAnswer.getResult(true),
                        AnswerObjects.BooleanAnswer.class);
                return ResultStatus.getResultStatus(serverAnswer);
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ResultStatus result) {
        try {
            if (iUpdateTimeLine != null && serverAnswer != null && serverAnswer.getSuccessStatus()){
                iUpdateTimeLine.notifyUpdateTimeLineCancelShare(postUniqueId);
            }

            if (serverAnswer == null) {
                delegate.onCancelShareResult(false, postUniqueId, ServerAnswer.getError(context, ServerAnswer.EXECUTION_ERROR, TAG));
            }
            else {
                if (serverAnswer.getSuccessStatus()) {
                    boolean resultStatus = serverAnswer.getResult().getBoolean(Params.RESULT);
                   delegate.onCancelShareResult(resultStatus, postUniqueId, ServerAnswer.getError(context, serverAnswer.getErrorCode(), TAG));
                }
                else
                    delegate.onCancelShareResult(false, postUniqueId, ServerAnswer.getError(context, serverAnswer.getErrorCode(), TAG));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }

}
