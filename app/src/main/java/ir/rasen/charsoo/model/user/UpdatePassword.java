package ir.rasen.charsoo.model.user;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ResultStatus;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class UpdatePassword extends AsyncTask<Void, Void, ResultStatus> {
    private static final String TAG = "UpdatePassword";

    private IWebservice delegate = null;
    public String password;
    private String newPassword;
    private ServerAnswer serverAnswer;
    private Context context;
    private String accesstoken;
    private int reqCode;

    public UpdatePassword(Context context,
                          String password,
                          String newPassword,
                          IWebservice delegate,int reqCode,String accesstoken){
        this.delegate = delegate;
        this.password = password;
        this.newPassword = newPassword;
        this.context = context;
        this.reqCode = reqCode;
        this.accesstoken=accesstoken;
    }

    public RequestObject.UpdatePassword createUpdatePassword(){
        RequestObject obj = new RequestObject();
        RequestObject.UpdatePassword req = obj.new UpdatePassword();
        req.UserId = LoginInfo.getInstance().getUserUniqueId();
        req.PasswordNew = this.newPassword;
        req.Password = this.password;

        return req;
    }

    @Override
    protected ResultStatus doInBackground(Void... voids) {
        RequestObject.UpdatePassword uP = createUpdatePassword();

        WebserviceGET webserviceGET = new WebserviceGET(URLs.UPDATE_PASSWORD,
                new ArrayList<String>(Arrays.asList(LoginInfo.getInstance().getUserUniqueId(),
                        password,newPassword,accesstoken)));

        try {
            serverAnswer = webserviceGET.execute(context);
            if (serverAnswer.getSuccessStatus())
                return ResultStatus.getResultStatus(serverAnswer);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ResultStatus result) {

        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
           execute();
    }
}
