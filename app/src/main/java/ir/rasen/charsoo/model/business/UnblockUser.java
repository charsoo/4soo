package ir.rasen.charsoo.model.business;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IUnblockUserListener;


/**
 * Created by android on 12/16/2014.
 */
public class UnblockUser extends AsyncTask<Void, Void, Boolean> {
    private static final String TAG = "UnblockUser ";

    private IUnblockUserListener delegate = null;
    private String businessUniqueId;
    private String userUniqueId;
    private Context context;
    private ServerAnswer serverAnswer;
    private String accesstoken;

    public UnblockUser(Context context,String businessUniqueId, String userUniqueId,IUnblockUserListener delegate,String accesstoken) {
        this.businessUniqueId = businessUniqueId;
        this.userUniqueId = userUniqueId;
        this.delegate = delegate;
        this.context = context;
        this.accesstoken=accesstoken;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.UNBLOCK_USER, new ArrayList<>(
                Arrays.asList(businessUniqueId,userUniqueId,accesstoken)));

        try {
            serverAnswer = webserviceGET.execute_new(context);

            if (serverAnswer.getSuccessStatus()){
                AnswerObjects.BooleanAnswer answer= new Gson().fromJson(serverAnswer.getResult(true),
                        AnswerObjects.BooleanAnswer.class);

                return answer.Result;

            }


        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return false;
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {

        try {
            if(serverAnswer==null) {
                delegate.unblockFailed(ServerAnswer.EXECUTION_ERROR);
                return;
            }

            if(serverAnswer.getSuccessStatus())
                if(result)
                    delegate.unblockSuccessfull(userUniqueId);
                else
                    delegate.unblockFailed(ServerAnswer.EXECUTION_ERROR);
            else
                delegate.unblockFailed(serverAnswer.getErrorCode());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void exectueWithNewSolution(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
