package ir.rasen.charsoo.model.business;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.SubCategory;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;


/**
 * Created by android on 12/16/2014.
 */
public class GetBusinessSubcategories extends AsyncTask<Void, Void, ArrayList<SubCategory>> {
    private static final String TAG = "GetBusinessSubcategories";
    private IWebservice delegate = null;
    private String categoryUniqeId;
    private ServerAnswer serverAnswer;
    private Context context;
    private int request;
    private String accesstoken;

    public GetBusinessSubcategories(Context context,String categoryUniqeId,IWebservice delegate, int request,String accesstoken) {
        this.categoryUniqeId = categoryUniqeId;
        this.delegate = delegate;
        this.context = context;
        this.request = request;
        this.accesstoken=accesstoken;
    }

    @Override
    protected ArrayList<SubCategory> doInBackground(Void... voids) {
        ArrayList<SubCategory> list = new ArrayList<>();

        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_BUSINESS_SUBCATEGORIES,new ArrayList<>(
                Arrays.asList(categoryUniqeId,accesstoken)));

        try {
            serverAnswer = webserviceGET.executeList(context);
            if (serverAnswer.getSuccessStatus()) {
                JSONArray jsonArray = serverAnswer.getResultList();
                Gson gson = new Gson();
                AnswerObjects.SubCategories[] subCategories = gson.fromJson(serverAnswer.getResultList(true)
                        , AnswerObjects.SubCategories[].class);
                for (int i = 0; i < jsonArray.length(); i++) {

                    list.add(
                            new SubCategory(
                                    subCategories[i].SubCategoryId,
                                    subCategories[i].SubCategory));
                }
                return list;
            }

        } catch (Exception e) {
            e.printStackTrace();
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<SubCategory> result) {


        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null) {
                delegate.getError(request, ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(request, result);
            else
                delegate.getError(request, serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
