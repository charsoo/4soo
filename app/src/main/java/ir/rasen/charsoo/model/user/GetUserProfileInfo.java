package ir.rasen.charsoo.model.user;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.Sex;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.view.fragment.invite.FragmentInviteAppList;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebserviceGET;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class GetUserProfileInfo extends AsyncTask<Void, Void, User> {
    private static final String TAG = "GetUserProfileInfo";
    private IWebservice delegate = null;
    private String userUniqueId;
    private ServerAnswer serverAnswer;
    private Context context;
    private int reqCode;
    private String accesstoken;


    public GetUserProfileInfo(Context context,String userUniqueId, IWebservice delegate,int reqCode,String accesstoken) {
        this.userUniqueId = userUniqueId;
        this.delegate = delegate;
        this.context = context;
        this.reqCode=reqCode;
        this.accesstoken=accesstoken;
    }

    @Override
    protected User doInBackground(Void... voids) {
        User user = new User();

        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_PROFILE_INFO,
                new ArrayList<>(Arrays.asList(userUniqueId,accesstoken)));

        try {
            serverAnswer = webserviceGET.execute(context);

            if (serverAnswer.getSuccessStatus()) {
                Gson gson = new Gson();
                String res=serverAnswer.getResult(true);
                AnswerObjects.UserProfileInfo userProfileInfo = gson.fromJson(res,AnswerObjects.UserProfileInfo.class);
                user.uniqueId = userUniqueId;
                user.name = userProfileInfo.Name;
                user.email = userProfileInfo.Email;
                user.aboutMe = userProfileInfo.AboutMe;
                user.birthDate = userProfileInfo.BirthDate;
                user.profilePictureUniqueId = userProfileInfo.ProfilePictureId;
                user.isEmailConfirmed = userProfileInfo.IsEmailConfirmed;
                user.sex = Sex.getSex(userProfileInfo.Sex);
                return user;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;

        }
        return null;
    }

    @Override
    protected void onPostExecute(User result) {

        //if webserviceGET.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
