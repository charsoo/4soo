package ir.rasen.charsoo.model.review;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.model.WebservicePOST;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

public class UpdateReview
        extends AsyncTask<Void,Void,AnswerObjects.BooleanAnswer>{
    private static final String TAG = "UpdateReview";
    RequestObject.ReviewItem reviewItem;
    private AnswerObjects.ServerAnswer serverAnswer;
    private Context context;
    private String accesstoken;
    public IWebservice iWebservice;

    public int reqCode;
    //UpdateReview/{accessToken}"

    public UpdateReview(Context ctx,RequestObject.ReviewItem reviewItem,IWebservice iw,int reqCode){
        this.context = ctx;
        this.reviewItem = reviewItem;
        this.iWebservice = iw;
        this.reqCode = reqCode;
        this.accesstoken = LoginInfo.getInstance().userAccessToken;
    }

    @Override
    protected AnswerObjects.BooleanAnswer doInBackground(Void... params) {
        WebservicePOST webservicePOST =
                new WebservicePOST(URLs.UPDATE_REVIEW,new ArrayList<>(Arrays.asList(accesstoken)));

        webservicePOST.postParam = new Gson().toJson(reviewItem);

        try {
            serverAnswer = webservicePOST.executeP(context);

            return new Gson().fromJson(serverAnswer.Result,AnswerObjects.BooleanAnswer.class);
        } catch (Exception e) {
        }


        return null;
    }


    @Override
    protected void onPostExecute(AnswerObjects.BooleanAnswer booleanAnswers) {
        try {
            if (serverAnswer == null) {
                iWebservice.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.SuccessStatus)
                iWebservice.getResult(reqCode,booleanAnswers);
            else
                iWebservice.getError(reqCode, Integer.valueOf(serverAnswer.Error.ErrorCode),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
