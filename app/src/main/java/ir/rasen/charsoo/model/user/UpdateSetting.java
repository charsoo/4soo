package ir.rasen.charsoo.model.user;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Permission;
import ir.rasen.charsoo.controller.helper.ResultStatus;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebserviceGET;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class UpdateSetting extends AsyncTask<Void, Void, ResultStatus> {
    private static final String TAG = "UpdateSetting";

    private IWebservice delegate = null;
    private String userUniqueId;
    private Permission permission;
    private ServerAnswer serverAnswer;
    private Context context;
    private String accesstoken;
    private int reqCode;

    public UpdateSetting(Context context,String userUniqueId, Permission permission,IWebservice delegate,int reqCode,String accesstoken) {
        this.userUniqueId = userUniqueId;
        this.permission = permission;
        this.delegate = delegate;
        this.context = context;
this.accesstoken=accesstoken;
        this.reqCode = reqCode;
    }

    @Override
    protected ResultStatus doInBackground(Void... voids) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.UPDATE_SETTING,new ArrayList<>(
                Arrays.asList(userUniqueId,
                        String.valueOf(permission.followedBusiness),
                        String.valueOf(permission.friends),
                        String.valueOf(permission.reviews),
                        accesstoken)));

        try {
            serverAnswer = webserviceGET.execute(context);
            if (serverAnswer.getSuccessStatus())
                return ResultStatus.getResultStatus(serverAnswer);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ResultStatus result) {

        try {
            if (result == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
