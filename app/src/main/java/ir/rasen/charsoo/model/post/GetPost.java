package ir.rasen.charsoo.model.post;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Hashtag;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.Comment;
import ir.rasen.charsoo.controller.object.Post;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebserviceGET;

import static android.os.Build.VERSION;
import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class GetPost extends AsyncTask<Void, Void, Post> {
    private static final String TAG = "GetPosts";
    private IWebservice delegate = null;
    private String postUniqueId;
    private String userUniqueId;
    private Context context;
    private Post.GetPostType getPostType;
    private ServerAnswer serverAnswer;
    private String businessUniqueId;
    private int reqCode;
    private String accesstoken;

    public GetPost(Context context,String userUniqueId,String businessUniqueId,
                   String postUniqueId,Post.GetPostType getPostType, IWebservice delegate,int reqCode,String accesstoken) {
        this.reqCode=reqCode;
        this.postUniqueId = postUniqueId;
        this.delegate = delegate;
        this.userUniqueId = userUniqueId;
        this.context = context;
        this.getPostType = getPostType;
        //businessUniqueId is needed if you want run GetPost for getFromJSONObjectBusiness
        this.businessUniqueId = businessUniqueId;
        this.accesstoken=accesstoken;
    }

    @Override
    protected Post doInBackground(Void... voids) {
        Post post = new Post();
        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_USER_POST, new ArrayList<>(
                Arrays.asList(userUniqueId, postUniqueId,accesstoken)));


        try {
            serverAnswer = webserviceGET.execute(context);
            if (serverAnswer.getSuccessStatus()) {


                JSONObject jsonObject = serverAnswer.getResult();

                Gson gson= new Gson();
                AnswerObjects.GetPost getpost = gson.fromJson(serverAnswer.getResult(true), AnswerObjects.GetPost.class);
                post=getPostFromJSONObjetct(businessUniqueId,getpost);


                return post;
            }

        } catch (Exception e) {

            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Post result) {

        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Post getPostFromJSONObjetct(String businessID,AnswerObjects.GetPost post_) throws Exception{

        Post post = new Post();
        post.uniqueId = post_.PostId;
        post.businessUniqueId = post_.BizID;
        try {
                post.businessProfilePictureUniqueId = post_.BusinessProfilePictureId;
            // post.businessProfilePictureUniqueId = jsonObject.getInt(Params.BUSINESS_PROFILE_PICUTE_ID_INT);
            post.businessIdentifier = post_.BusinessId;
            post.businessUserName = post.businessIdentifier;
        }catch (Exception e1){}
        post.title =post_.Title;
        post.creationDate = Post.setCreationDate(post_);
        post.pictureUniqueId = post_.PostPictureId;
        post.description =post_.Description;
        post.code = post_.Code;
        post.price = post_.Price;

        String comments = post_.Comments;
        JSONArray jsonArrayComments = new JSONArray(comments);


        post.lastThreeComments = Comment.getFromJSONArray(jsonArrayComments);

        post.hashtagList = Hashtag.getListFromString(post_.HashTagList);

        post.isLiked = post_.IsLiked;
        try {
            post.isShared = post_.IsShared;
        } catch (Exception e) {

        }
        post.likeNumber =post_.LikeNumber;
        post.commentNumber = post_.CommentNumber;
        post.shareNumber =post_.ShareNumber;

        return post;
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
