package ir.rasen.charsoo.model.review;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.model.user.Login;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

/**
 * Created by Rasen_iman on 9/7/2015.
 */
public class GetUserReviewByBizId extends AsyncTask<Void,Void,RequestObject.ReviewItem>{
    public static final String TAG = "GetUserReviewByBizId";


    Context context;
    IWebservice iWebservice;
    String userUniqueId;
    String bizUniqueId;
    String accessToken;
    AnswerObjects.ServerAnswer serverAnswer;
    int reqCode;

    public GetUserReviewByBizId(Context ctx,IWebservice iWebservice,String userUniqueId,String bizUniqueId,int reqCode){
        this.userUniqueId = userUniqueId;
        this.bizUniqueId = bizUniqueId;
        this.context = ctx;
        this.iWebservice = iWebservice;
        this.reqCode = reqCode;

        this.accessToken = LoginInfo.getInstance().userAccessToken;
    }

    @Override
    protected RequestObject.ReviewItem doInBackground(Void... params) {
        WebserviceGET webserviceGET =
                new WebserviceGET(
                        URLs.GET_USER_REVIEW_BY_BIZ_ID,
                        new ArrayList<>(Arrays.asList(userUniqueId,bizUniqueId,accessToken)));

        try {
            serverAnswer = webserviceGET.executeP(context);
            return new Gson().fromJson(serverAnswer.Result, RequestObject.ReviewItem.class);

        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    protected void onPostExecute(RequestObject.ReviewItem reviewItem) {
        try {
            if (serverAnswer == null) {
                iWebservice.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.SuccessStatus)
                iWebservice.getResult(reqCode,reviewItem);
            else
                iWebservice.getError(reqCode,Integer.valueOf(serverAnswer.Error.ErrorCode),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
