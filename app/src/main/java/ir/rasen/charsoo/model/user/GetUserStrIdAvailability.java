package ir.rasen.charsoo.model.user;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class GetUserStrIdAvailability extends AsyncTask<Void, Void, Boolean> {
    private static final String TAG = "GetBusinessStrIdAvailability";
    private IWebservice delegate = null;
    private String userStrId;
    private ServerAnswer serverAnswer;
    private Context context;
    private int reqCode;

    public GetUserStrIdAvailability(Context context, String userStrId, IWebservice delegate, int reqCode,String accesstoken) {
        this.userStrId=userStrId;
        this.delegate = delegate;
        this.context = context;
        this.reqCode=reqCode;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.CHECK_USER_IDENTIFIER,new ArrayList<>(
                Arrays.asList(userStrId)));

        try {
            serverAnswer = webserviceGET.execute_new(context);

            if (serverAnswer.getSuccessStatus()){

                return true;
                // TODO: BAD AZ ESLAHE WEB SERVICE AVAILABILITY BAR ASASE PASOKHE SERVER SET SHAVAD
//                return serverAnswer.getResult().getBoolean(Params.IS_STR_ID_AVAILABLE);
            }

        } catch (Exception e) {
//            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        //if webservice.executeWithNewSolution(); throws exception
//        delegate.getResult(reqCode,true);
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR, TAG);
            }
            else if (serverAnswer.getSuccessStatus()) {
                AnswerObjects.BooleanAnswer answer= new Gson().fromJson(serverAnswer.getResult(true),
                        AnswerObjects.BooleanAnswer.class);
//                    boolean successStatus=Boolean.valueOf(serverAnswer.getResult().getBoolean(Params.RESULT));
                    delegate.getResult(reqCode,!answer.Result);
            }
            else {
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
