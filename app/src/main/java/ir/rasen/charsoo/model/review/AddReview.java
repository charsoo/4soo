package ir.rasen.charsoo.model.review;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.RequestObject;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.model.WebservicePOST;
import ir.rasen.charsoo.view.interface_m.IAddReview;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

public class AddReview extends AsyncTask<Void, Void, Boolean>{
    private static final String TAG = "AddReview";
    RequestObject.ReviewItem reviewItem;
    private AnswerObjects.ServerAnswer serverAnswer;
    private Context context;
    private String accesstoken;
    private IAddReview delegate = null;

    public AddReview(Context context,RequestObject.ReviewItem reviewItem,
                     IAddReview delegate,String accesstoken) {
        this.reviewItem = reviewItem;
        this.delegate = delegate;
        this.context = context;
        this.accesstoken=accesstoken;
    }


    @Override
    protected Boolean doInBackground(Void... params) {

        WebservicePOST webservicePost = new WebservicePOST(URLs.REVIEW_BUSINESS + "/" + accesstoken);
        webservicePost.postParam = new Gson().toJson(reviewItem);

        try {
            serverAnswer = webservicePost.executeP(context);
            if (serverAnswer.SuccessStatus){

                return Boolean.valueOf(serverAnswer.Result);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {

        try {
            //if webservice.executeWithNewSolution(); throws exception
            if (serverAnswer == null) {
                delegate.failAddReview(ServerAnswer.EXECUTION_ERROR, TAG);
                return;
            }
            if (serverAnswer.SuccessStatus)
                if(result)
                    delegate.successAddReview(reviewItem);
                else
                    delegate.failAddReview(Integer.valueOf(serverAnswer.Error.ErrorCode),TAG);
            else
                delegate.failAddReview(Integer.valueOf(serverAnswer.Error.ErrorCode),TAG);

        } catch (Exception e) {
        }
    }

    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
