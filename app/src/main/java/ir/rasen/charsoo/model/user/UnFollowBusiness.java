package ir.rasen.charsoo.model.user;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ResultStatus;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.view.interface_m.IUnfollowBusiness;
import ir.rasen.charsoo.model.WebserviceGET;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class UnFollowBusiness extends AsyncTask<Void, Void, ResultStatus> {
    private static final String TAG = "UnFollowBusiness";

    //private IWebserviceResponse delegate = null;
    private String userUniqueId;
    private String businessUniqueId;
    private ServerAnswer serverAnswer;
    private IUnfollowBusiness iUnfollowBusiness;
    private Context context;
    private String accesstoken;

    public UnFollowBusiness(Context context,String userUniqueId, String businessUniqueId,IUnfollowBusiness iUnfollowBusiness,String accesstoken) {
        this.userUniqueId = userUniqueId;
        this.businessUniqueId = businessUniqueId;
        //this.delegate = delegate;
        this.iUnfollowBusiness = iUnfollowBusiness;
        this.context = context;
        this.accesstoken=accesstoken;
    }

    @Override
    protected ResultStatus doInBackground(Void... voids) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.UN_FOLLOW_BUSINESS, new ArrayList<>(
                Arrays.asList(userUniqueId,businessUniqueId,accesstoken)));


        try {
            serverAnswer = webserviceGET.execute_new(context);
//            if (serverAnswer.getSuccessStatus())
//                return ResultStatus.getResultStatus(serverAnswer);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ResultStatus result) {
        try {
            if (serverAnswer==null){
                iUnfollowBusiness.notifyUnfollowBusinessFailed(ServerAnswer.NETWORK_CONNECTION_ERROR,TAG);
            }
            else if (serverAnswer.getSuccessStatus()){
                try {
                    AnswerObjects.BooleanAnswer answer= new Gson().fromJson(serverAnswer.getResult(true),
                            AnswerObjects.BooleanAnswer.class);
//                    boolean successStatus=serverAnswer.getResult().getBoolean(Params.RESULT);
                    if (answer.Result)
                        iUnfollowBusiness.notifyUnfollowBusiness(businessUniqueId);
                    else
                        iUnfollowBusiness.notifyUnfollowBusinessFailed(ServerAnswer.NONE_DEFINED_ERROR, TAG);
                } catch (JSONException e) {
                    e.printStackTrace();
                    iUnfollowBusiness.notifyUnfollowBusinessFailed(ServerAnswer.EXECUTION_ERROR,TAG);
                }
            }
            else
            {
                iUnfollowBusiness.notifyUnfollowBusinessFailed(serverAnswer.getErrorCode(),TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
