package ir.rasen.charsoo.model.business;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebserviceGET;


/**
 * Created by android on 12/16/2014.
 */
public class GetBusinessHomeInfo extends AsyncTask<Void, Void, Business> {
    private static final String TAG = "GetUserHomeInfo";
    private IWebservice delegate = null;
    private String businessUniqueId;
    private String userUniqueId;
    private ServerAnswer serverAnswer;
    private Context context;
    private int reqCode;
    private String accesstoken;

    public GetBusinessHomeInfo(Context context,String businessUniqueId,String userUniqueId,IWebservice delegate,int reqCode,String accesstoken) {
        this.businessUniqueId = businessUniqueId;
        this.delegate = delegate;
        this.userUniqueId = userUniqueId;
        this.context = context;
        this.reqCode=reqCode;
        this.accesstoken=accesstoken;
    }

    @Override
    protected Business doInBackground(Void... voids) {
        Business business = new Business();
        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_BUSINESS_HOME_INFO,new ArrayList<>(
                Arrays.asList(businessUniqueId, userUniqueId,accesstoken)));

        try {
            serverAnswer = webserviceGET.execute(context);

            if (serverAnswer.getSuccessStatus()) {
                Gson gson = new Gson();
                AnswerObjects.Business businessgson = gson.fromJson(serverAnswer.getResult(true)
                        , AnswerObjects.Business.class);
                business.uniqueId = businessUniqueId;
                business.businessIdentifier = businessgson.BusinessUserName;
                business.userUniqueId = businessgson.UserId;
                business.name = businessgson.BusinessName;
                business.profilePictureUniqueId = businessgson.ProfilePictureId;
                business.coverPictureUniqueId = businessgson.CoverPictureId;
                business.categoryName = businessgson.Category;
                business.subcategoryName = businessgson.SubCategory;
                business.description = businessgson.Description;
                business.reviewsNumber = businessgson.ReviewsNumber;
                business.followersNumber = businessgson.FollowersNumber;
                business.isFollowing = businessgson.IsFollowing;
                business.rate = businessgson.Rate;

                return business;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Business result) {
        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
