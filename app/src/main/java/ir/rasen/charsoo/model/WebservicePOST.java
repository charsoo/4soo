package ir.rasen.charsoo.model;

import android.content.Context;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.util.ArrayList;

import ir.rasen.charsoo.controller.helper.NetworkHandler;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.WebservicesHandler;
import ir.rasen.charsoo.controller.object.AnswerObjects;


/**
 * Created by android on 12/1/2014.
 */
public class WebservicePOST {
    HttpClient httpclient;
    public HttpPost httpPost;
    public String postParam;
    public WebservicePOST(String url) {
        httpclient = new DefaultHttpClient();
        httpPost = new HttpPost(url);
    }

    public WebservicePOST(String url, ArrayList<String> paramsList) {
        httpclient = new DefaultHttpClient();
        if (paramsList != null) {
            for (String param : paramsList) {
                url += "/" + param;
            }
        }
        url = url.replaceAll ( " ", "%20" );
        httpPost = new HttpPost(url);
    }


    private HttpResponse run(HttpPost httpPost) throws Exception {
        HttpResponse httpResponse = null;

        StringEntity params = new StringEntity(postParam, "UTF-8");


        params.setChunked(true);

        httpPost.setEntity(params);
        httpPost.setHeader("Content-Type", "application/json");
        httpPost.setHeader("Accept", "application/json");

        try {
            httpResponse = httpclient.execute(httpPost);
        } catch (Exception e) {
            String s = e.getMessage();
        }

        return httpResponse;
    }

    public ServerAnswer execute(Context context) throws Exception {
//        if (!NetworkHandler.isNetworkAvailable(context))
//            return getNetworkConnectionError();

        HttpResponse httpResponse = run(httpPost);

        for (int i = 0; i < Params.RETRY_COUNT_ON_CONNECTION_FAILED; i++) {
            if (httpResponse!=null){
                if (httpResponse.getStatusLine().getStatusCode()==HttpStatus.SC_OK)
                    return ServerAnswer.get(httpResponse);
            }
            httpResponse = run(httpPost);
        }
        return ServerAnswer.get(httpResponse);
    }

    public ServerAnswer execute_new(Context context) throws Exception {
        if (!NetworkHandler.isNetworkAvailable(context))
            return getNetworkConnectionError();

        HttpResponse httpResponse = run(httpPost);

        for (int i = 0; i < Params.RETRY_COUNT_ON_CONNECTION_FAILED; i++) {
            if (httpResponse!=null){
                if (httpResponse.getStatusLine().getStatusCode()==HttpStatus.SC_OK)
                    return ServerAnswer.get_new(httpResponse);
            }
            httpResponse = run(httpPost);
        }
        return ServerAnswer.get_new(httpResponse);
    }

    public ServerAnswer executeList(Context context) throws Exception {
        if (!NetworkHandler.isNetworkAvailable(context))
            return getNetworkConnectionError();

        HttpResponse httpResponse = run(httpPost);
        for (int i = 0; i < Params.RETRY_COUNT_ON_CONNECTION_FAILED; i++) {
            if (httpResponse!=null){
                return ServerAnswer.getList(httpResponse);
            }
            httpResponse = run(httpPost);
        }
        return ServerAnswer.getList(httpResponse);
    }

    public AnswerObjects.ServerAnswer executeP(Context context) throws Exception {

        HttpResponse httpResponse = run(httpPost);

        for (int i = 0; i < Params.RETRY_COUNT_ON_CONNECTION_FAILED; i++) {
            if (httpResponse!=null){
                if (httpResponse.getStatusLine().getStatusCode()==HttpStatus.SC_OK)
                    return ServerAnswer.getP(httpResponse);
            }
            httpResponse = run(httpPost);
        }
        return ServerAnswer.getP(httpResponse);
    }

    public ServerAnswer getNetworkConnectionError(){
        ServerAnswer serverAnswer = new ServerAnswer();
        serverAnswer.setSuccessStatus(false);
        serverAnswer.setResult(null);
        serverAnswer.setErrorCode(ServerAnswer.NETWORK_CONNECTION_ERROR);
        return serverAnswer;
    }

}
