package ir.rasen.charsoo.model.review;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

/**
 * Created by Rasen_iman on 9/8/2015.
 */
public class ReviewActivity extends AsyncTask<Void,Void,AnswerObjects.BooleanAnswer>{
    public static final String TAG = "ReviewActivity";


    public static final int ACTIVITY_HELP_FULL = 0;
    public static final int ACTIVITY_NOT_HELP_FULL = 1;
    public static final int REPORT = 2;

    ServerAnswer serverAnswer;

    Context context;
    IWebservice iWebservice;
    String userUnqId,reviewUnqId,accessToken;
    int activity,reqCode;

    public ReviewActivity(Context ctx,String reviewUnqId,
                          int activity,IWebservice iw,int reqCode){
        this.context = ctx;
        this.userUnqId = LoginInfo.getInstance().getUserUniqueId();
        this.reviewUnqId = reviewUnqId;
        this.activity = activity;
        this.accessToken = LoginInfo.getInstance().userAccessToken;
        this.iWebservice = iw;
        this.reqCode = reqCode;
    }


    @Override
    protected AnswerObjects.BooleanAnswer doInBackground(Void... params) {
        WebserviceGET webserviceGET =
                new WebserviceGET(URLs.REVIEW_ACTIVITY,new ArrayList<>(Arrays.asList(
                        userUnqId,reviewUnqId,String.valueOf(activity),accessToken
                )));

        try {
            serverAnswer = webserviceGET.execute(context);

            return new Gson().fromJson(serverAnswer.getResult(true), AnswerObjects.BooleanAnswer.class);
        } catch (Exception e) {}

        return null;
    }

    @Override
    protected void onPostExecute(AnswerObjects.BooleanAnswer booleanAnswer) {
        try {
            if (serverAnswer == null) {
                iWebservice.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus() && booleanAnswer.Result)
                    iWebservice.getResult(reqCode,true);
            else
                iWebservice.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
