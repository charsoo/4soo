package ir.rasen.charsoo.model.user;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
;import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.User;
import ir.rasen.charsoo.controller.helper.FriendshipRelation;
import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.Permission;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.view.interface_m.IWebservice;
import ir.rasen.charsoo.model.WebserviceGET;

import static android.os.Build.VERSION;
import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class GetUserHomeInfo extends AsyncTask<Void, Void, User> {
    private static final String TAG = "GetUserHomeInfo";
    private IWebservice delegate = null;
    private String visitedUserUniqueId;
    private String visitorUserUniqueId;
    private ServerAnswer serverAnswer;
    private Context context;
    private int reqCode;
    public static String name;
    private String accesstoken;

    public GetUserHomeInfo(Context context,String visitedUserUniqueId, String visitorUserUniqueId, IWebservice delegate,int reqCode,String accesstoken) {
        this.visitedUserUniqueId = visitedUserUniqueId;
        this.visitorUserUniqueId = visitorUserUniqueId;
        this.delegate = delegate;
        this.context = context;
        this.reqCode=reqCode;
        this.accesstoken=accesstoken;
    }

    @Override
    protected User doInBackground(Void... voids) {
        User user = new User();

        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_HOME_INFO, new ArrayList<>(
                Arrays.asList(visitedUserUniqueId,visitorUserUniqueId,accesstoken)));

        try {
            serverAnswer = webserviceGET.execute(context);

            if (serverAnswer.getSuccessStatus()) {
                Gson gson = new Gson();
                AnswerObjects.UserHomeInfo userHomeInfo = gson.fromJson(serverAnswer.getResult(true)
                        ,AnswerObjects.UserHomeInfo.class);


                user.uniqueId = visitedUserUniqueId;
                user.userIdentifier = (userHomeInfo.UserId==null)?"":userHomeInfo.UserId;
                user.name = (userHomeInfo.Name==null)?"":userHomeInfo.Name;
                user.aboutMe = (userHomeInfo.AboutMe==null)?"":userHomeInfo.AboutMe;
                user.profilePictureUniqueId = (userHomeInfo.ProfilePictureId==null)?"":userHomeInfo.ProfilePictureId;
                user.coverPictureUniqueId = (userHomeInfo.CoverPictureId==null)?"":userHomeInfo.CoverPictureId;
                user.friendRequestNumber = userHomeInfo.FriendRequestNumber;
                user.reviewsNumber = userHomeInfo.ReviewsNumber;
                user.followedBusinessesNumber = userHomeInfo.FollowedBusinessNumber;
                user.friendsNumber = userHomeInfo.FriendsNumber;
                user.friendshipRelationStatus =
                        FriendshipRelation.getFromCode(Integer.valueOf(userHomeInfo.FriendshipRelationStatusCode));
                String busi = (userHomeInfo.Businesses==null)?"":userHomeInfo.Businesses;
                JSONArray busiArray;
                if (!busi.equals("null") && !busi.equals("NULL")) {
                    busiArray = new JSONArray(busi);
                    user.businesses = User.getUserBusinesses(busiArray);
                }
                return user;
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(User result) {

        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }

}
