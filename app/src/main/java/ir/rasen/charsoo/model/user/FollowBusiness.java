package ir.rasen.charsoo.model.user;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ResultStatus;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.view.interface_m.IFollowBusiness;
import ir.rasen.charsoo.model.WebserviceGET;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;


/**
 * Created by android on 12/16/2014.
 */
public class FollowBusiness extends AsyncTask<Void, Void, ResultStatus> {
    private static final String TAG = "FollowBusiness";

    private IFollowBusiness iFollowBusiness = null;
    private String userUniqueId;
    private String businessUniqueId;
    private ServerAnswer serverAnswer;
    private Context context;
    private String accesstoken;
    public FollowBusiness(Context context,String userUniqueId, String businessUniqueId,IFollowBusiness iFollowBusiness,String accesstoken) {
        this.userUniqueId = userUniqueId;
        this.businessUniqueId = businessUniqueId;
        this.iFollowBusiness = iFollowBusiness;
        this.context = context;
        this.accesstoken=accesstoken;
    }

    @Override
    protected ResultStatus doInBackground(Void... voids) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.FOLLOW_BUSINESS,new ArrayList<>(
                Arrays.asList(userUniqueId, businessUniqueId,accesstoken)));


        try {
            serverAnswer = webserviceGET.execute_new(context);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ResultStatus result) {
        try {
            if (serverAnswer==null){
                iFollowBusiness.notifyFollowBusinessFailed(ServerAnswer.EXECUTION_ERROR, TAG);
            }
            else if (serverAnswer.getSuccessStatus()){
                try {
                    AnswerObjects.BooleanAnswer booleanAnswer = new Gson().fromJson(serverAnswer.getResult(true),
                            AnswerObjects.BooleanAnswer.class);
//                    boolean resultBln = Boolean.valueOf(serverAnswer.getResult().getBoolean(Params.RESULT));
                    if(booleanAnswer.Result)
                        iFollowBusiness.notifyFollowBusiness(businessUniqueId);
                    else
                        iFollowBusiness.notifyFollowBusinessFailed(ServerAnswer.EXECUTION_ERROR, TAG);

                } catch (JSONException e) {
                    e.printStackTrace();
                    iFollowBusiness.notifyFollowBusinessFailed(ServerAnswer.EXECUTION_ERROR, TAG);
                }
            }
            else
            {
                iFollowBusiness.notifyFollowBusinessFailed(serverAnswer.getErrorCode(), TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
