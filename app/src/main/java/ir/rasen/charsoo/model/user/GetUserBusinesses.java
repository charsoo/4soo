package ir.rasen.charsoo.model.user;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.object.Business;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

/**
 * Created by android on 12/16/2014.
 */

public class GetUserBusinesses extends AsyncTask<Void, Void, ArrayList<Business>> {
    private static final String TAG = "GetUserBusinesses";

    private IWebservice delegate = null;
    private String userUniqueId;
    private ServerAnswer serverAnswer;
    private Context context;
    private int reqCode;
    private String accesstoken;

    public GetUserBusinesses(Context context,String userUniqueId, IWebservice delegate,int reqCode,String accesstoken) {
        this.userUniqueId = userUniqueId;
        this.delegate = delegate;
        this.context = context;
        this.reqCode=reqCode;
        this.accesstoken=accesstoken;
    }

    @Override
    protected ArrayList<Business> doInBackground(Void... voids) {
        ArrayList<Business> list = new ArrayList();

        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_USER_BUSINESSES,
                new ArrayList<>(Arrays.asList(String.valueOf(userUniqueId),accesstoken)));

        try {
            serverAnswer = webserviceGET.executeList(context);
            if (serverAnswer.getSuccessStatus()) {
                JSONArray jsonArray = serverAnswer.getResultList();
                Gson gson = new Gson();
                AnswerObjects.UserBusiness[] userBusinesses = gson.fromJson(serverAnswer.getResultList(true)
                        ,AnswerObjects.UserBusiness[].class);
                for (int i = 0; i < jsonArray.length(); i++) {
                    Business b = new Business();
                    b.name=userBusinesses[i].Name;
                    b.profilePictureUniqueId =userBusinesses[i].PictureId;
                    b.uniqueId =userBusinesses[i].Business_Id;
                    b.businessIdentifier=userBusinesses[i].BusinessId;
                    b.description=userBusinesses[i].Desc;

                    list.add(b);
//                    list.add(new BaseAdapterItem(context.getResources(),jsonObject.getInt(Params.BUSINESS_ID_STRING),
//                            jsonObject.getInt(Params.SEARCH_PICTURE_ID),
//                            jsonObject.getString(Params.BUSINESS_USERNAME_STRING)));
                }
                return list;
            }

        } catch (Exception e) {
//            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<Business> result) {

        //if webservice.executeWithNewSolution(); throws exception
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR,TAG);
                return;
            }
            if (serverAnswer.getSuccessStatus())
                delegate.getResult(reqCode,result);
            else
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
