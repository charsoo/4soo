package ir.rasen.charsoo.model.friend;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION;
import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

/**
 * Created by android on 12/16/2014.
 */
public class AnswerRequestFriendship extends AsyncTask<Void, Void, Void> {
    private static final String TAG = "AnswerRequestFriendship";


    private String applicatorUserUniqueId;
    private String requestedUserUniqueId;
    private String answerNumber;
    private ServerAnswer serverAnswer;
    private Context context;
    IWebservice delegate;
    private String accesstoken;

    private int reqCode;
    public AnswerRequestFriendship(Context context, String requestedUserUniqueId,
                                   String applicatorUserUniqueId,String answerNumber,
                                   IWebservice delegate,int reqCode,String accesstoken) {
        this.applicatorUserUniqueId = applicatorUserUniqueId;
        this.requestedUserUniqueId = requestedUserUniqueId;
        this.answerNumber = answerNumber;
        this.context = context;
        this.delegate=delegate;
        this.accesstoken=accesstoken;
        this.reqCode = reqCode;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.ANSWER_REQUEST_FRIENDSHIP, new ArrayList<>(
                Arrays.asList(requestedUserUniqueId,
                        applicatorUserUniqueId
                        , String.valueOf(answerNumber),accesstoken)));


        try {
            serverAnswer = webserviceGET.execute_new(context);
//            if (serverAnswer.getSuccessStatus())
//                return ResultStatus.getResultStatus(serverAnswer);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            serverAnswer = null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        try {
            if (serverAnswer == null) {
                delegate.getError(reqCode,ServerAnswer.EXECUTION_ERROR, TAG);
            } else if (serverAnswer.getSuccessStatus()) {
                try {
                    boolean results=serverAnswer.getResult().getBoolean(Params.RESULT);
                    AnswerObjects.BooleanAnswer booleanAnswer= new Gson().fromJson(serverAnswer.getResult(true),
                            AnswerObjects.BooleanAnswer.class);
                  //  boolean res=Boolean.valueOf(results);
                    delegate.getResult(reqCode,booleanAnswer.Result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                delegate.getError(reqCode,serverAnswer.getErrorCode(),TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
