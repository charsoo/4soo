package ir.rasen.charsoo.model.app;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import ir.rasen.charsoo.controller.helper.Params;
import ir.rasen.charsoo.controller.helper.ServerAnswer;
import ir.rasen.charsoo.controller.helper.URLs;
import ir.rasen.charsoo.controller.object.AnswerObjects;
import ir.rasen.charsoo.controller.helper.WebservicesHandler;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.controller.object.ServerAppVersion;
import ir.rasen.charsoo.model.WebserviceGET;
import ir.rasen.charsoo.view.interface_m.IWebservice;

import static android.os.Build.VERSION;
import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

/**
 * Created by Rasen_iman on 8/8/2015.
 */
public class GetVersion extends AsyncTask<Void,Void,ServerAppVersion> {
    public final static String TAG = "GetVersion";

    private Context context;
    private int reqCode;
    private IWebservice iWebservice = null;

    public GetVersion(Context ctx,IWebservice delegate,int reqCode){
        this.context = ctx;
        this.reqCode = reqCode;
        this.iWebservice = delegate;
    }



    @Override
    protected ServerAppVersion doInBackground(Void... params) {
        WebserviceGET webserviceGET = new WebserviceGET(URLs.GET_VERSION,null);
        ServerAppVersion serverAppVersion = new ServerAppVersion();

        try {
            ServerAnswer serverAnswer = webserviceGET.execute(context);

            if(serverAnswer.getSuccessStatus()){
                
                String StrString = serverAnswer.getResult(true);
                Gson gson = new Gson();
                AnswerObjects.VersionInfos VersionInfos = gson.fromJson(StrString,AnswerObjects.VersionInfos.class);
                serverAppVersion.minVersion = VersionInfos.Minimum;
                serverAppVersion.maxVersion = VersionInfos.Maxmimum;
                return serverAppVersion;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(ServerAppVersion serverAppVersion) {
        try {
            if(serverAppVersion == null ||
                    serverAppVersion.minVersion==-1 ||
                    serverAppVersion.maxVersion==-1) {
                iWebservice.getError(reqCode, ServerAnswer.EXECUTION_ERROR, TAG);
                return;
            }

            iWebservice.getResult(reqCode,serverAppVersion);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exectueWithNewSolution(){
        if (SDK_INT >= HONEYCOMB)
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        else
            execute();
    }
}
