package ir.rasen.charsoo.avatar;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.helper.LoginInfo;
import ir.rasen.charsoo.view.activity.ActivityLogin;
import ir.rasen.charsoo.view.widgets.material_library.widgets.Dialog;


public class DialogResetDesignedPicture extends Dialog {

    public DialogResetDesignedPicture(final Activity activity) {
        super(activity, activity.getResources().getString(R.string.popup_warning),
                activity.getString(R.string.txt_ResetAvatarHint));

        addCancelButton(R.string.cancel);
        setAcceptText(activity.getString(R.string.txt_ResetAvatar));

        //set onClickListener for the ok button
        setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 MainController.getInstance().resetResultPicture();
            }
        });

    }

}
