package ir.rasen.charsoo.avatar;

/**
 * Created by hossein-pc on 4/28/2015.
 */
public interface DesignFragmentEventListener {
    public void objectClickListener(String objectID, int containerIndex);
    public void updateResultPic();
}
