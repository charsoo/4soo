package ir.rasen.charsoo.avatar;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import java.io.File;

import ir.rasen.charsoo.R;
import ir.rasen.charsoo.controller.object.MyApplication;
import ir.rasen.charsoo.view.interface_m.TryAgainListener;
import ir.rasen.charsoo.view.widgets.charsoo_activity.CharsooActivity;


public class AvatarActivity extends CharsooActivity implements AvatarActivityListener ,TryAgainListener {

    public static final String TAG = "AvatarActivity";

    public String userStrId;
    FragmentDesignPicture fragment;
    Intent shareIntent;
    public static AvatarActivityListener avatarActivityListener;
    Menu menuTemp;
    boolean isActivityLoaded = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avatar);
//        hideWaitDialog();

        setTitle(getString(R.string.txt_AvatarTitle));
//        userStrId = getString(R.string.txt_AvatarLogo);
//        userStrId+="\n";
        userStrId = getIntent().getStringExtra(AvatarParams.userStrID);
//        userStrId+="\n";
//        userStrId+="www.icharsoo.com";
        avatarActivityListener = AvatarActivity.this;

        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                MainController.getInstance(AvatarActivity.this);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                fragment = new FragmentDesignPicture();
                transaction.replace(R.id.sample_content_fragment, fragment);
                transaction.commit();
                Uri shareUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                        + "/"
                        + AvatarParams.charsooPublicSubdirectory
                        + AvatarParams.publicSubdirectoryForSharing
                        + AvatarParams.tempShareFileName));
                shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("image/*");
                shareIntent.putExtra(Intent.EXTRA_STREAM, shareUri);
                isActivityLoaded = true;
            }
        }, 1500);
    }


    @Override
    protected void onDestroy() {
        MainController.destroyInstance();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menuTemp = menu;
        return true;
    }

    @Override
    public void onBackPressed() {
        if (isActivityLoaded)
            super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_tik:
                if (MainController.getInstance().saveResultInPublic())
                    Toast.makeText(AvatarActivity.this, getString(R.string.txt_FileSavedSuccessfully), Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(AvatarActivity.this, getString(R.string.txt_ErrorSavingImageTryAgain), Toast.LENGTH_LONG).show();

                break;
            case R.id.action_share:
                startActivity(Intent.createChooser(shareIntent, getString(R.string.txt_SharePicture)));
                (new Handler()).post(new Runnable() {
                    @Override
                    public void run() {
                        MainController.getInstance().saveResultToShareDirectory();
                    }
                });
                break;
            case R.id.action_refresh:
                (new DialogResetDesignedPicture(AvatarActivity.this)).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void doOnContentLoadingCompletion() {
        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                (findViewById(R.id.avatar_progressbarContainer)).setVisibility(View.GONE);
                getMenuInflater().inflate(R.menu.menu_save_share_refresh, menuTemp);
            }
        }, 2000);
    }

    @Override
    public void doTryAgain() {

    }
}
