package ir.rasen.charsoo.avatar;

import android.app.Activity;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.Ringtone;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import ir.rasen.charsoo.R;


/**
 * Created by hossein-pc on 5/5/2015.
 */
public class FragmentDesignPicture extends Fragment implements DesignFragmentEventListener {
    private static FragmentDesignPicture instance;


    private ImageView resultImageView;
    private static LinearLayout pieceGroupsContainer,piecesContainer;
//    private static ArrayList<MyImageView> pieceGroupsImages;
    private DesignFragmentEventListener eventListener;
    private static String currentCategoryName;
    private static int currentCategoryIndex;
    private int pieceContainerHeight,pieceGroupContainerHeight;
    private MainController mController;
//    private Bitmap piecesGroupIconBackground;
    private AssetManager assetManager;
    private boolean allPiecesShown;
    private boolean currentlyFetchingNewPieceIcon;
    private boolean bb;
    private Bitmap circularIconBackground;

    private Bitmap mostBackwardActiveIconBackground;
    private Activity activity;
    private LinearLayout.LayoutParams pieceGroupIconAvatarParams,piecesIconAvatarParams;

    private Canvas tempCanvas;

    private Hashtable<String,Bitmap> pieceGroupIcons;
    private int topMargin,bottomMargin,leftMargin,rightMargin;
    private static Hashtable<String,int[]> cropBounds;
    private int resultImageViewWidthHeight;
//    private LinearLayout resultImageViewContainer;


    private Uri notification;
    private Ringtone soundNotification;

//    public static synchronized FragmentDesignPicture getInstance(){
//        return instance;
//    }
//
//    public static synchronized FragmentDesignPicture getInstance(final Activity theActivity, View root){
////        if (instance==null)
//            instance=new FragmentDesignPicture(theActivity,root);
////        else
////            instance=new FragmentDesignPicture(root);
//        return instance;
//    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root=inflater.inflate(R.layout.fragment_design,container,false);
//        return super.onCreateView(inflater, container, savedInstanceState);
        pieceGroupsContainer=(LinearLayout) root.findViewById(R.id.inDesFrag_ImgGroupsContainer);
        piecesContainer=(LinearLayout) root.findViewById(R.id.inDesFrag_ImgPiecesContainer);
        resultImageView =(ImageView) root.findViewById(R.id.inDesFrag_resImage);
        activity=getActivity();
//        piecesGroupIconBackground=Bitmap.createBitmap(getCircularIconBackground(AvatarParams.pieceGroupIconWidthHeight, Color.parseColor(AvatarParams.iconBackgroundActiveColor)));
        currentlyFetchingNewPieceIcon=false;
        bb=false;
        mController=MainController.getInstance(activity.getApplicationContext());
        eventListener= this;
        mController.setDesignFragListener(eventListener);
        topMargin=0;
        bottomMargin=9;
        leftMargin=3;
        rightMargin=3;
        assetManager = activity.getAssets();
        circularIconBackground=getCircularIconBackground(AvatarParams.pieceGroupIconWidthHeight,
                Color.parseColor(AvatarParams.charsooDefaultColor));
        /*try {
            notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            soundNotification = RingtoneManager.getRingtone(activity.getApplicationContext(), notification);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        try {
            InputStream iStr = assetManager.open("avatar/active_icon_background" + AvatarParams.fileType);
            mostBackwardActiveIconBackground=changeColorOfNonTransparentPixels(
                    BitmapFactory.decodeStream(iStr),
                    Color.parseColor(AvatarParams.pieceContainerBackgroundColor));
        } catch (IOException e) {
            e.printStackTrace();
        }

//        mController.saveBitmapToPath(mostBackwardActiveIconBackground
//                ,
//                AvatarParams.);
        currentCategoryName= AvatarParams.initialCategoryName;
        pieceGroupsContainer=(LinearLayout) root.findViewById(R.id.inDesFrag_ImgGroupsContainer);
        piecesContainer=(LinearLayout) root.findViewById(R.id.inDesFrag_ImgPiecesContainer);
        piecesContainer.setBackgroundColor(Color.parseColor(AvatarParams.pieceContainerBackgroundColor));

        if (cropBounds==null)
            cropBounds=new Hashtable<>();
//        cropBounds=new int[4];

        pieceGroupsContainer.post(new Runnable() {
            @Override
            public void run() {

                pieceGroupContainerHeight=pieceGroupsContainer.getHeight();
                pieceGroupIconAvatarParams=new LinearLayout.LayoutParams(pieceGroupContainerHeight,pieceGroupContainerHeight);//pieceGroups.getLayoutAvatarParams().height,pieceGroups.getLayoutAvatarParams().height);
                pieceGroupIconAvatarParams.setMargins(leftMargin,topMargin,rightMargin,bottomMargin);

                int scaledHeight=pieceGroupContainerHeight;
                int scaledWidth = (int) Math.floor((double) mostBackwardActiveIconBackground.getWidth() *( (double) scaledHeight / (double) mostBackwardActiveIconBackground.getHeight()));
                mostBackwardActiveIconBackground= Bitmap.createScaledBitmap(mostBackwardActiveIconBackground.copy(Bitmap.Config.ARGB_8888,true), scaledWidth, scaledHeight, true);
                initialPieceGroupIcons();
                AvatarActivity.avatarActivityListener.doOnContentLoadingCompletion();

            }
        });

        piecesContainer.post(new Runnable() {
            @Override
            public void run() {
                pieceContainerHeight=piecesContainer.getHeight();
                updatePiecesIcones(currentCategoryName,currentCategoryIndex);
                /*ArrayList<PicturePiece> picPices=new ArrayList<>(mController.getDefaultPictureList(currentCategoryName));
                for (int i = 0; i < picPices.size(); i++) {
                    MyImageView tempImageView = new MyImageView(activity);
                    tempImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    tempImageView.setLayoutAvatarParams(pieceGroupIconAvatarParams);
                    tempImageView.setClickListener(eventListener);
//                    piecesIconAvatarParams=new LinearLayout.LayoutAvatarParams(pieceContainerHeight,pieceContainerHeight);//pieceGroups.getLayoutAvatarParams().height,pieceGroups.getLayoutAvatarParams().height);
//                    piecesIconAvatarParams.setMargins(leftMargin,topMargin,rightMargin,bottomMargin);
//                    tempImageView.setLayoutAvatarParams(piecesIconAvatarParams);

                    cropBounds.put(picPices.get(i).getID(),getRectToCropBitmap(picPices.get(i).getPicturePiece()));
//                    cropBounds=getRectToCropBitmap(picPices.get(i).getPicturePiece());
                    Bitmap bm=Bitmap.createBitmap(picPices.get(i).getPicturePiece(),
                            cropBounds.get(picPices.get(i).getID())[0],
                            cropBounds.get(picPices.get(i).getID())[1],
                            cropBounds.get(picPices.get(i).getID())[2]-cropBounds.get(picPices.get(i).getID())[0],
                            cropBounds.get(picPices.get(i).getID())[3]-cropBounds.get(picPices.get(i).getID())[1]);
//                    Bitmap bm=Bitmap.createBitmap(picPices.get(i).getPicturePiece(),
//                            cropBounds[0],
//                            cropBounds[1],
//                            cropBounds[2]-cropBounds[0],
//                            cropBounds[3]-cropBounds[1]);
                    tempImageView.setImageBitmap(bm);
                    tempImageView.setObjectIdAndIndex(picPices.get(i).getID(), i);
                    piecesContainer.addView(tempImageView);
                }*/
            }
        });
        resultImageView =(ImageView) root.findViewById(R.id.inDesFrag_resImage);
        /*resultImageViewContainer=(LinearLayout) root.findViewById(R.uniqueId.ll_inFragDesign_ResPicContainer);
        resultImageViewContainer.post(new Runnable() {
            @Override
            public void run() {
                bb=true;
            }
        });*/
        /**/
        resultImageView.post(new Runnable() {
            @Override
            public void run() {
//                for(;!bb;);

                if (resultImageView.getWidth() < resultImageView.getHeight())
                    resultImageViewWidthHeight = resultImageView.getWidth();
                else
                    resultImageViewWidthHeight = resultImageView.getHeight();
                resultImageView.setLayoutParams(new RelativeLayout.LayoutParams(resultImageViewWidthHeight,resultImageViewWidthHeight));
                resultImageView.setImageBitmap(getIfScaledImage(mController.getResultPicture(),
                        mController.getResultPicture().getWidth(),
                        mController.getResultPicture().getHeight(),
                        resultImageViewWidthHeight, resultImageViewWidthHeight));
//                resultImageView.setBackgroundColor(Color.parseColor(AvatarParams.charsooDefaultColor));
            }
        });
        resultImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        return root;
    }

//    private FragmentDesignPicture(View root){
//
//    }

//    private FragmentDesignPicture(final Activity theActivity, View root){
//
//    }




    public void updateLayoutItems(){
//        updatePieceGroupIcones(currentCategoryName,currentCategoryIndex);
//        updatePiecesIcones(currentCategoryName,currentCategoryIndex);
        resultImageView.setImageBitmap(getIfScaledImage(mController.getResultPicture(),
                mController.getResultPicture().getWidth(),
                mController.getResultPicture().getHeight(),
                resultImageViewWidthHeight, resultImageViewWidthHeight));
    }


    @Override
    public void objectClickListener(String objectID,int objectIndex) {
        /*if(soundNotification!=null)
            soundNotification.play();*/
        if (objectID.startsWith(AvatarParams.categoryNamePrefix)){
            updatePieceGroupIcones(objectID, objectIndex);
            updatePiecesIcones(objectID, objectIndex);
        }
        else{
            mController.itemClickManager(objectID);

//            new putPieceOnPicture().execute(objectID);

//            resultImageView.setImageBitmap(mController.getIfScaledImage(mController.getResultPicture(),
//                    mController.getResultPicture().getWidth(),
//                    mController.getResultPicture().getHeight(),
//                    resultImageViewWidthHeight, resultImageViewWidthHeight));
        }
    }

    @Override
    public void updateResultPic() {
        try {
            resultImageView.setImageBitmap(getIfScaledImage(mController.getResultPicture(),
                    mController.getResultPicture().getWidth(),
                    mController.getResultPicture().getHeight(),
                    resultImageViewWidthHeight, resultImageViewWidthHeight));
        }
        catch (Exception e){
            //
        }
    }


    private void updatePieceGroupIcones(String objectID,int objectIndex){
        Bitmap resBitmap,tempBitmap;
        for (int i = 0; i < 2; i++) {
            MyImageView tempImageView = new MyImageView(activity);
            tempImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            tempImageView.setLayoutParams(pieceGroupIconAvatarParams);
            tempImageView.setClickListener(eventListener);
            if (i==1){
                LinearLayout.LayoutParams tempAvatarParams=new LinearLayout.LayoutParams(pieceGroupContainerHeight,pieceGroupContainerHeight);
                tempAvatarParams.setMargins(0,0,0,0);
                tempImageView.setLayoutParams(tempAvatarParams);

                tempBitmap=getOverlayedBitmaps(mostBackwardActiveIconBackground,
                        circularIconBackground);
                resBitmap=getOverlayedBitmaps(tempBitmap,
                        pieceGroupIcons.get(currentCategoryName));
//                tempImageView.setImageBitmap(mController.getIfScaledImage(resBitmap, resBitmap.getWidth(), resBitmap.getHeight(),
//                        pieceGroupContainerHeight - (int) (0.1 * pieceGroupContainerHeight),
//                        pieceGroupContainerHeight - (int) (0.1 * pieceGroupContainerHeight)));
                tempImageView.setImageBitmap(getIfScaledImage(resBitmap, resBitmap.getWidth(), resBitmap.getHeight(),
                        pieceGroupContainerHeight,
                        pieceGroupContainerHeight));
//                tempImageView.setImageBitmap(mController.getIfScaledImage(resBitmap, resBitmap.getWidth(), resBitmap.getHeight(),
//                        pieceGroupContainerHeight - leftMargin - rightMargin - AvatarParams.inDesFragActiveIconMargin_Pixel,
//                        pieceGroupContainerHeight - topMargin - bottomMargin - AvatarParams.inDesFragActiveIconMargin_Pixel));
            }
            else
//                tempImageView.setImageBitmap(mController.getIfScaledImage(pieceGroupIcons.get(currentCategoryName),
//                        pieceGroupIcons.get(currentCategoryName).getWidth(),
//                        pieceGroupIcons.get(currentCategoryName).getHeight(),
//                        pieceGroupContainerHeight - (int) (0.25 * pieceGroupContainerHeight),
//                        pieceGroupContainerHeight - (int) (0.25 * pieceGroupContainerHeight)));
                tempImageView.setImageBitmap(getIfScaledImage(pieceGroupIcons.get(currentCategoryName),
                        pieceGroupIcons.get(currentCategoryName).getWidth(),
                        pieceGroupIcons.get(currentCategoryName).getHeight(),
                        pieceGroupContainerHeight - leftMargin - rightMargin - AvatarParams.inDesFragNonActiveIconMargin_pixel,
                        pieceGroupContainerHeight - topMargin - bottomMargin - AvatarParams.inDesFragNonActiveIconMargin_pixel));
            tempImageView.setObjectIdAndIndex(currentCategoryName, currentCategoryIndex);
            pieceGroupsContainer.removeViewAt(currentCategoryIndex);
            pieceGroupsContainer.addView(tempImageView, currentCategoryIndex);
            currentCategoryName = objectID;
            currentCategoryIndex = objectIndex;
        }
    }

    private void updatePiecesIcones(String categoryName,int objectIndex) {
        piecesContainer.removeAllViews();
        ArrayList<PicturePiece> picPices=new ArrayList<>(mController.getDefaultPictureList(categoryName));
        int size=picPices.size();
        for (int i = 0; i < size; i++) {
            MyImageView tempImageView = new MyImageView(activity);
            tempImageView.setScaleType(ImageView.ScaleType.CENTER);
            tempImageView.setLayoutParams(pieceGroupIconAvatarParams);
            tempImageView.setPadding(0,0,0,0);
            tempImageView.setClickListener(eventListener);
//                    piecesIconAvatarParams=new LinearLayout.LayoutAvatarParams(pieceContainerHeight,pieceContainerHeight);//pieceGroups.getLayoutAvatarParams().height,pieceGroups.getLayoutAvatarParams().height);
//                    piecesIconAvatarParams.setMargins(leftMargin,topMargin,rightMargin,bottomMargin);
//                    tempImageView.setLayoutAvatarParams(piecesIconAvatarParams);
            Bitmap bm;
            if ((i==0)&&(AvatarParams.categoriesWithNoImage.contains(categoryName))) {
                Bitmap tbmp=BitmapFactory.decodeResource(activity.getResources(),R.drawable.avatar_no_image_icon);
                bm=getIfScaledImage(tbmp,tbmp.getWidth(),tbmp.getHeight(),pieceGroupContainerHeight/2,pieceGroupContainerHeight/2);
            }
            else {
                bm=Bitmap.createBitmap(picPices.get(i).getPicturePiece());
                //********************************************************************************************************
                // code with crop capability
                /*if (!cropBounds.containsKey(picPices.get(i).getID())) {
                    cropBounds.put(picPices.get(i).getID(), getRectToCropBitmap(picPices.get(i).getPicturePiece()));
                }
                bm = Bitmap.createBitmap(picPices.get(i).getPicturePiece(),
                        cropBounds.get(picPices.get(i).getID())[0],
                        cropBounds.get(picPices.get(i).getID())[1],
                        cropBounds.get(picPices.get(i).getID())[2] - cropBounds.get(picPices.get(i).getID())[0],
                        cropBounds.get(picPices.get(i).getID())[3] - cropBounds.get(picPices.get(i).getID())[1]);*/
                //********************************************************************************************************
            }
            tempImageView.setImageBitmap(getIfScaledImage(bm, bm.getWidth(), bm.getHeight(),
                    pieceContainerHeight - leftMargin - rightMargin - AvatarParams.inDesFragActiveIconMargin_Pixel,
                    pieceContainerHeight - topMargin - bottomMargin - AvatarParams.inDesFragActiveIconMargin_Pixel));

//            tempImageView.setImageBitmap(mController.getIfScaledImage(bm, bm.getWidth(), bm.getHeight(),
//                    pieceContainerHeight - (int) (0.2 * pieceContainerHeight),
//                    pieceContainerHeight - (int) (0.2 * pieceContainerHeight)));
            tempImageView.setObjectIdAndIndex(picPices.get(i).getID(), i);
            piecesContainer.addView(tempImageView);
        }
//        allPiecesShown=false;
//        for(;allPiecesShown==false;){
//        new getNewPieceToShow().executeWithNewSolution();;
        new getNewPieceToShow().execute();
//        }
    }



    private int[] getRectToCropBitmap(Bitmap bm){
        int[] rect=new int[4];
        rect[0]=bm.getWidth();//left
        rect[1]=bm.getHeight();//top
        rect[2]=0;//right
        rect[3]=0;//bottom
        int color=0;
        for (int i = 0; i < bm.getWidth(); i++) {
            for (int j = 0; j < bm.getHeight(); j++) {
                color=bm.getPixel(i,j);

                //find left Bound
                if ((color != Color.TRANSPARENT) & (i < rect[0])) {
                    rect[0]=i;
                }
                //find Top Bound
                if((color != Color.TRANSPARENT)&(j<rect[1])){
                    rect[1]=j;
                }
                //find right Bound
                if ((color != Color.TRANSPARENT) & (i > rect[2])) {
                    rect[2]=i;
                }
                //find Bottom Bound
                if((color != Color.TRANSPARENT)&(j>rect[3])){
                    rect[3]=j;
                }
            }

        }
        return rect;
    }


    private Bitmap getCircularIconBackground(int widthHeight,int color){
        Bitmap tempBitmap=Bitmap.createBitmap(widthHeight,widthHeight, Bitmap.Config.ARGB_8888);
        Canvas c=new Canvas(tempBitmap);
        c.drawColor(Color.TRANSPARENT);
        Paint p=new Paint();
        p.setStyle(Paint.Style.FILL);
        p.setColor(color);
        c.drawCircle(widthHeight/2,widthHeight/2,(widthHeight/2)-1,p);
        return tempBitmap;
    }



    private class getNewPieceToShow extends AsyncTask<Void,Void,MyImageView> {

        @Override
        protected MyImageView doInBackground(Void... Voids) {
            for (;currentlyFetchingNewPieceIcon;);
            currentlyFetchingNewPieceIcon=true;
            PicturePiece tempPiece=mController.getExtraPicturePiece();
            if (tempPiece!=null){
                MyImageView tempImageView = new MyImageView(activity);
                tempImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                tempImageView.setLayoutParams(pieceGroupIconAvatarParams);
                tempImageView.setClickListener(eventListener);
                Bitmap bm=Bitmap.createBitmap(tempPiece.getPicturePiece());
                // ****************************************************************************************
                // code with crop capability
/*               if (!cropBounds.containsKey(tempPiece.getID())){
                    cropBounds.put(tempPiece.getID(),getRectToCropBitmap(tempPiece.getPicturePiece()));
                }
                Bitmap bm=Bitmap.createBitmap(tempPiece.getPicturePiece(),
                        cropBounds.get(tempPiece.getID())[0],
                        cropBounds.get(tempPiece.getID())[1],
                        cropBounds.get(tempPiece.getID())[2]-cropBounds.get(tempPiece.getID())[0],
                        cropBounds.get(tempPiece.getID())[3]-cropBounds.get(tempPiece.getID())[1]);*/
                //*****************************************************************************************************
               tempImageView.setImageBitmap(getIfScaledImage(bm, bm.getWidth(), bm.getHeight(),
                       pieceContainerHeight - leftMargin - rightMargin - AvatarParams.inDesFragActiveIconMargin_Pixel,
                       pieceContainerHeight - topMargin - bottomMargin - AvatarParams.inDesFragActiveIconMargin_Pixel));
                tempImageView.setObjectIdAndIndex(tempPiece.getID(), piecesContainer.getChildCount());
                return tempImageView;
            }
            return null;
        }

        @Override
        protected void onPostExecute(MyImageView resImageView){
            currentlyFetchingNewPieceIcon=false;
            if (resImageView==null){
                allPiecesShown=true;
            }
            else{
                if (resImageView.getObjectID().startsWith(currentCategoryName.substring(AvatarParams.categoryNameSubStringIndex))) {
                    piecesContainer.addView(resImageView);
                    new getNewPieceToShow().execute();
                }
            }

        }
    }


    private class putPieceOnPicture extends AsyncTask<String,Void,Void>{

        @Override
        protected Void doInBackground(String... strings) {
//            mController.itemClickManager(strings[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void v){
            updateResultPic();
        }
    }


//    ****************************************************************************************************************************
//    ****************************************************************************************************************************
//    ****************************************************************************************************************************
//                                                     Initial Methods

    private void initialPieceGroupIcons(){
        pieceGroupIcons=new Hashtable<>();
        Bitmap resBitmap,tempBitmap;
        int currentIndex=0;
        for (int i = 0; i < AvatarParams.allCategoryNames.size(); i++) {
            try {
                MyImageView tempImageView = new MyImageView(activity);
                tempImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

                tempImageView.setClickListener(eventListener);
                InputStream iStr = assetManager.open("avatar/"+AvatarParams.allCategoryNames.get(i)+"/"+ AvatarParams.allCategoryNames.get(i)+ AvatarParams.fileType);
                pieceGroupIcons.put(AvatarParams.allCategoryNames.get(i),
                        getOverlayedBitmaps(Bitmap.createBitmap(AvatarParams.pieceGroupIconWidthHeight, AvatarParams.pieceGroupIconWidthHeight, Bitmap.Config.ARGB_8888),
                                BitmapFactory.decodeStream(iStr)));

                if (currentCategoryName== AvatarParams.allCategoryNames.get(i)) {
                    currentCategoryIndex=currentIndex;
                    LinearLayout.LayoutParams tempAvatarParams=new LinearLayout.LayoutParams(pieceGroupContainerHeight,pieceGroupContainerHeight);
                    tempAvatarParams.setMargins(0,0,0,0);
                    tempImageView.setLayoutParams(tempAvatarParams);


                    tempBitmap=getOverlayedBitmaps(mostBackwardActiveIconBackground,
                            circularIconBackground);
                    resBitmap=getOverlayedBitmaps(tempBitmap,
                            pieceGroupIcons.get(currentCategoryName));
//                    int scaledHeight =pieceGroupContainerHeight;
//                    int scaledWidth = (int) Math.floor((double) resBitmap.getWidth() *( (double) scaledHeight / (double) resBitmap.getHeight()));
//
//                    tempImageView.setImageBitmap(Bitmap.createScaledBitmap(resBitmap, scaledWidth, scaledHeight, true));
                    tempImageView.setImageBitmap(getIfScaledImage(resBitmap,
                            resBitmap.getWidth(),
                            resBitmap.getHeight(),
                            pieceGroupContainerHeight,
                            pieceGroupContainerHeight));
//                    tempImageView.setImageBitmap(mController.getIfScaledImage(resBitmap,
//                            resBitmap.getWidth(),
//                            resBitmap.getHeight(),
//                            pieceGroupContainerHeight - leftMargin - rightMargin - AvatarParams.inDesFragActiveIconMargin_Pixel,
//                            pieceGroupContainerHeight - topMargin - bottomMargin - AvatarParams.inDesFragActiveIconMargin_Pixel));
//                    tempImageView.setImageBitmap(mController.getIfScaledImage(tempActiveIconBitmap,
//                            tempActiveIconBitmap.getWidth(),
//                            tempActiveIconBitmap.getHeight(),
//                            pieceGroupContainerHeight - (int) (0.1 * pieceGroupContainerHeight),
//                            pieceGroupContainerHeight - (int) (0.1 * pieceGroupContainerHeight)));
                }
                else {
                    tempImageView.setLayoutParams(pieceGroupIconAvatarParams);
                    tempImageView.setImageBitmap(getIfScaledImage(pieceGroupIcons.get(AvatarParams.allCategoryNames.get(i)),
                            pieceGroupIcons.get(AvatarParams.allCategoryNames.get(i)).getWidth(),
                            pieceGroupIcons.get(AvatarParams.allCategoryNames.get(i)).getHeight(),
                            pieceGroupContainerHeight - leftMargin - rightMargin - AvatarParams.inDesFragNonActiveIconMargin_pixel,
                            pieceGroupContainerHeight - topMargin - bottomMargin - AvatarParams.inDesFragNonActiveIconMargin_pixel));

                }
                tempImageView.setObjectIdAndIndex(AvatarParams.allCategoryNames.get(i), currentIndex);
                pieceGroupsContainer.addView(tempImageView);
                currentIndex++;
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


    private Bitmap changeColorOfNonTransparentPixels(Bitmap bmp,int newColor){
        Bitmap result=bmp.copy(Bitmap.Config.ARGB_8888,true);
//        Canvas c=new Canvas(result);
        int x=result.getHeight();
        int y=result.getWidth();
        for (int i = 0; i < result.getWidth(); i++) {
            for (int j = 0; j < result.getHeight(); j++) {
                if (result.getPixel(i,j) != Color.WHITE)
                    result.setPixel(i,j,newColor);
            }

        }
        return result;
    }


    private Bitmap getOverlayedBitmaps(Bitmap baseImage, Bitmap overlayImage){
        Bitmap result=Bitmap.createBitmap(baseImage);
        Canvas c=new Canvas(result);
        Bitmap bm= getIfScaledImage(overlayImage, overlayImage.getWidth(), overlayImage.getHeight(), baseImage.getWidth(), baseImage.getHeight());
        c.drawBitmap(bm,
                (baseImage.getWidth()/2)-(bm.getWidth()/2),
                (baseImage.getHeight()/2)-(bm.getHeight()/2),
                new Paint());
//        if ((overlayImage.getHeight()>baseImage.getHeight())||(overlayImage.getWidth()>baseImage.getWidth())){
//            Bitmap bm= getIfScaledImage(overlayImage, overlayImage.getWidth(), overlayImage.getHeight(), baseImage.getWidth(), baseImage.getHeight());
//            c.drawBitmap(bm,
//                    (baseImage.getWidth()/2)-(bm.getWidth()/2),
//                    (baseImage.getHeight()/2)-(bm.getHeight()/2),
//                    new Paint());
//        }
//        else
//            c.drawBitmap(overlayImage,
//                    (baseImage.getWidth()/2)-(overlayImage.getWidth()/2),
//                    (baseImage.getHeight()/2)-(overlayImage.getHeight()/2),
//                    new Paint());
        return result;
    }

    public Bitmap getIfScaledImage(Bitmap bmp, int originalWidth, int originalHeight, int newWidth, int newHeight){

        if ((originalHeight<newHeight)&&(originalWidth<newWidth))
            return bmp;
        else{
            int scaledWidth,scaledHeight;
            if (originalWidth>originalHeight){
                scaledWidth =newWidth;
                scaledHeight = (int) Math.floor((double) originalHeight *( (double) scaledWidth / (double) originalWidth));
            }
            else{
                scaledHeight=newHeight;
                scaledWidth = (int) Math.floor((double) originalWidth *( (double) scaledHeight / (double) originalHeight));
            }
            try {
                return Bitmap.createScaledBitmap(bmp, scaledWidth, scaledHeight, true);
            }
            catch(Exception e){
//                Toast.makeText(activity.getApplicationContext(), "main controller : getIfScaledImage() e:" + e.getMessage(), Toast.LENGTH_LONG).show();
                return Bitmap.createBitmap(1,1, Bitmap.Config.ARGB_8888);
            }
        }

    }


}
