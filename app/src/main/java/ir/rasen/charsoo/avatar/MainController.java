package ir.rasen.charsoo.avatar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;

import ir.rasen.charsoo.R;


/**
 * Created by Hossien on 4/27/2015.
 */
public class MainController {
    private static MainController instance;
    private static Context thisContext;

    private FileFetcherSaver dSaver;

    private Hashtable<String, PicturePiece> picturePieces;
    private Hashtable<String, ArrayList<PicturePiece>> defaultPicturesToShow;

    private String currentCategoryName;
    private int currentGivenPieceCount;


    private Bitmap lastResultPicture;

    private DesignFragmentEventListener designFragListener;

    private PicturePiece removedHairByHat, removedHatByHair;
    private int resultPictureWidth, resultPictureHeight;
    private ArrayList<PicturePiece> tempRemovedPiecesByVeil;
    private String hat0, hairs0, veil0;

//______________________________________ End of Variables Part _________________________________________________________________________
//_______________________________________________________________________________________________________________________________

    private MainController() {
        hat0 = AvatarParams.catHat.substring(AvatarParams.categoryNameSubStringIndex) + "0";
        hairs0 = AvatarParams.catHairs.substring(AvatarParams.categoryNameSubStringIndex) + "0";
        veil0 = AvatarParams.catVeil.substring(AvatarParams.categoryNameSubStringIndex) + "0";

        tempRemovedPiecesByVeil = new ArrayList<>();
        resultPictureWidth = AvatarParams.resultPictureWidth;
        resultPictureHeight = AvatarParams.resultPictureHeight;
        defaultPicturesToShow = new Hashtable<>();
        dSaver = FileFetcherSaver.getInstance(thisContext);
        lastResultPicture = null;
        currentCategoryName = AvatarParams.initialCategoryName;
        currentGivenPieceCount = 0;
        initialDefaultPicturePieces();
        removedHairByHat = defaultPicturesToShow.get(AvatarParams.catHairs).get(0);
        removedHatByHair = defaultPicturesToShow.get(AvatarParams.catHat).get(0);
    }

    public static synchronized MainController getInstance() {
        if (instance == null) {
            instance = new MainController();
        }
        return instance;
    }

    public static synchronized MainController getInstance(Context c) {
        thisContext = c;
        if (instance == null) {
            instance = new MainController();
        }

        return instance;
    }

    public static synchronized void destroyInstance() {
        try {
            instance.lastResultPicture.recycle();
            for (String str : instance.picturePieces.keySet()) {
                instance.picturePieces.get(str).recyclePiece();
            }
            for (String str : instance.defaultPicturesToShow.keySet()) {
                for (int i = 0; i < instance.defaultPicturesToShow.get(str).size(); i++) {
                    instance.defaultPicturesToShow.get(str).get(i).recyclePiece();
                }
            }
            for (int i = 0; i < instance.tempRemovedPiecesByVeil.size(); i++) {
                instance.tempRemovedPiecesByVeil.get(i).recyclePiece();
            }
            instance.removedHairByHat.recyclePiece();
            instance.removedHatByHair.recyclePiece();
            instance = null;
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    //-----------------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------------------


    public void saveResultToShareDirectory() {
        dSaver.saveResultPicToShareDirectory(getResultPicture());
    }


    public void setUserTextResultPiece(PicturePiece p) {
        putInPicturePieces(AvatarParams.catUserText, p);
        broadcastUpdateResultPic();
        dSaver.saveNewBitmapPiece(p.getPicturePiece(), AvatarParams.catUserText, p.getID());
        Hashtable<String, Hashtable<String, Integer>> tempHashTable = new Hashtable<>();
        tempHashTable.put(p.getID(), new Hashtable<String, Integer>());
        tempHashTable.get(p.getID()).put(AvatarParams.leftIndex, p.getLeftPoitionIndex());
        tempHashTable.get(p.getID()).put(AvatarParams.topIndex, p.getTopPositionIndex());
        dSaver.saveNewPositionIndexes(tempHashTable, AvatarParams.catUserText);
    }

    public void setLogoResultPicture(PicturePiece p) {
        putInPicturePieces(AvatarParams.catCharsooLogo, p);
        defaultPicturesToShow.put(AvatarParams.catCharsooLogo
                , new ArrayList<>(Arrays.asList((p))));
        broadcastUpdateResultPic();
        dSaver.saveNewBitmapPiece(p.getPicturePiece(), AvatarParams.catCharsooLogo, p.getID());
        Hashtable<String, Hashtable<String, Integer>> tempHashTable = new Hashtable<>();
        tempHashTable.put(p.getID(), new Hashtable<String, Integer>());
        tempHashTable.get(p.getID()).put(AvatarParams.leftIndex, p.getLeftPoitionIndex());
        tempHashTable.get(p.getID()).put(AvatarParams.topIndex, p.getTopPositionIndex());
        dSaver.saveNewPositionIndexes(tempHashTable, AvatarParams.catCharsooLogo);
    }

    public void clearUserTextResultPiece() {
        if (picturePieces.containsKey(AvatarParams.catUserText))
            picturePieces.remove(AvatarParams.catUserText);
        drawNewResult();
        broadcastUpdateResultPic();
    }


    public boolean saveResultInPublic() {
        return dSaver.saveResultPicInPublic(getResultPicture());
    }


    public void setDesignFragListener(DesignFragmentEventListener lsr) {
        designFragListener = lsr;
    }


    public int[] getUserTextMaxAllowedDimensions() {
        int[] dim = new int[2];
        dim[0] = AvatarParams.resultPictureWidth - AvatarParams.userTextFrameWidthMargin_PIX;
        dim[1] = AvatarParams.resultPictureHeight - picturePieces.get(AvatarParams.catHuman).getPicturePiece().getHeight() - picturePieces.get(AvatarParams.catHuman).getTopPositionIndex() - AvatarParams.userTextFrameHeightMargin_PIX;
        return dim;
    }


    public int[] getLogoMaxAllowedDimensions() {
        int[] dim = new int[2];
        dim[0] = AvatarParams.resultPictureWidth
                - picturePieces.get(AvatarParams.catHuman).getPicturePiece().getWidth()
                - picturePieces.get(AvatarParams.catHuman).getLeftPoitionIndex()
                - (AvatarParams.logoFrameWidthMargin_PIX / 2);
        dim[1] = dim[0] * 2 / 3;
//        dim[1] = AvatarParams.resultPictureHeight / 2
//                - AvatarParams.logoFrameHeightMargin_PIX;
        return dim;
    }

    public void saveBitmapToPath(Bitmap bmp, String path, String fileName, String fileType) {
        dSaver.saveBitmapToCustomPath(bmp, path, fileName, fileType);
    }


//    ******************************************************************************************
//    ******************************************************************************************


    public ArrayList<PicturePiece> getDefaultPictureList(String categoryName) {
        currentGivenPieceCount = defaultPicturesToShow.get(categoryName).size();
        currentCategoryName = categoryName;
        return defaultPicturesToShow.get(categoryName);
    }

    public PicturePiece getExtraPicturePiece() {
        PicturePiece tempPiece = dSaver.getPicturePiece(currentCategoryName,
                currentCategoryName.substring(AvatarParams.categoryNameSubStringIndex) + Integer.toString(currentGivenPieceCount), false);
        if (tempPiece != null) {
            currentGivenPieceCount++;
        }
        return tempPiece;
    }


    public void itemClickManager(String objectID) {

        if (objectID.startsWith(AvatarParams.categoryNamePrefix)) {
        } else {//means that a picture piece is clicked
            String catID = getCatID(objectID);
            checkForPiecesOverlayCorrectness(objectID, catID);
            putInPicturePieces(catID, dSaver.getPicturePiece(catID, objectID, false));

        }
    }


    private void broadcastUpdateResultPic() {
        if (designFragListener != null)
            designFragListener.updateResultPic();
    }

    public Bitmap getResultPicture() {
        if (lastResultPicture == null) {
//            ArrayList<String> categoryNames =new ArrayList<>(AvatarParams.defaultCategoryOrder.keySet());
            for (int i = 0; i < AvatarParams.allCategoryNames.size(); i++) {
                if (!defaultPicturesToShow.get(AvatarParams.allCategoryNames.get(i)).isEmpty()) {
                    picturePieces.put(AvatarParams.allCategoryNames.get(i), defaultPicturesToShow.get(AvatarParams.allCategoryNames.get(i)).get(0));
                }
            }
            drawNewResult();
        }
        return lastResultPicture;
    }


    public void resetResultPicture() {
        loadDefaultResultPic();
    }


    //-------------------------------------------------------------------------------------------------------------------------------
    //*******************************************************************************************************************************
    //-------------------------------------------------------------------------------------------------------------------------------
    // private Methods


    private void initialDefaultPicturePieces() {

        boolean loadDefaultResultPicture = false;
        picturePieces = dSaver.getAutoSavedPicturePieces();
        if (picturePieces == null) {
            loadDefaultResultPicture = true;
            picturePieces = new Hashtable<>();
        }

        for (int i = 0; i < AvatarParams.allCategoryNames.size(); i++) {
            String categoryName = AvatarParams.allCategoryNames.get(i);
            defaultPicturesToShow.put(categoryName, new ArrayList<PicturePiece>());
            boolean continueFetching = true;
            for (int m = 0; (continueFetching & (m < AvatarParams.defaultPieceCount)); m++) {
                String pictureID = categoryName.substring(AvatarParams.categoryNameSubStringIndex) + Integer.toString(m);
                PicturePiece p = dSaver.getPicturePiece(categoryName, pictureID, true);
                if (p == null)
                    continueFetching = false;
                else
                    defaultPicturesToShow.get(categoryName).add(p);
            }
            if (loadDefaultResultPicture)
                loadDefaultResultPic();
        }
        String[] ss = new String[1];
        ss[0] = ((AvatarActivity) thisContext).userStrId;
//        new getLogoDrawnOnPicture().execute(ss);
        getLogoOnPicture(ss);
        drawNewResult();
    }


    private void loadDefaultResultPic() {
        picturePieces.clear();
        if (defaultPicturesToShow.containsKey(AvatarParams.catUserText))
            defaultPicturesToShow.remove(AvatarParams.catUserText);
        String categoryName;
        for (int i = 0; i < AvatarParams.allCategoryNames.size(); i++) {
            categoryName = AvatarParams.allCategoryNames.get(i);
            if (defaultPicturesToShow.containsKey(categoryName))
                if (!defaultPicturesToShow.get(categoryName).isEmpty())
                    picturePieces.put(categoryName, defaultPicturesToShow.get(categoryName).get(0));
        }
        drawNewResult();

    }


    private void putInPicturePieces(String catID, PicturePiece piece) {
        if (picturePieces.containsKey(catID)) {
            picturePieces.remove(catID);
        }
        picturePieces.put(catID, piece);
        drawNewResult();
        broadcastUpdateResultPic();
    }


    private void drawNewResult() {

        ArrayList<PicturePiece> tempPicurePieces = new ArrayList<>(picturePieces.values());
        Hashtable<Integer, PicturePiece> sortedPieces = new Hashtable<>();
        ArrayList<String> tempPiecesNames = new ArrayList<>();
        for (int i = 0; i < tempPicurePieces.size(); i++) {
            sortedPieces.put(tempPicurePieces.get(i).getCanvasOrder(), tempPicurePieces.get(i));
            tempPiecesNames.add(tempPicurePieces.get(i).getID());
        }
        lastResultPicture = Bitmap.createBitmap(resultPictureWidth, resultPictureHeight, Bitmap.Config.ARGB_8888);
        Canvas drawerCanvas = new Canvas(lastResultPicture);
        drawerCanvas.drawColor(Color.WHITE);
        for (int i = AvatarParams.baseIndex; i < AvatarParams.piecesGroupCount; i++) {
            if (sortedPieces.containsKey(i)) {
                drawerCanvas.drawBitmap(sortedPieces.get(i).getPicturePiece(), sortedPieces.get(i).getLeftPoitionIndex(), sortedPieces.get(i).getTopPositionIndex(), null);
            }
        }
        dSaver.setLastSelectedPieces(tempPiecesNames);
        broadcastUpdateResultPic();
    }

    private String getCatID(String objectID) {
        return AvatarParams.categoryNamePrefix + objectID.replaceAll("\\d", "");
    }

    private void checkForPiecesOverlayCorrectness(String objectID, String catID) {


        // check For Veil

        if (catID.equals(AvatarParams.catVeil)) {
            if (objectID.equals(veil0)) {
                if (!picturePieces.get(catID).getID().equals(veil0)) {
                    for (int i = 0; i < tempRemovedPiecesByVeil.size(); i++) {
                        picturePieces.put(getCatID(tempRemovedPiecesByVeil.get(i).getID()), tempRemovedPiecesByVeil.get(i));
                        //                        putInPicturePieces();
                    }
                    tempRemovedPiecesByVeil.clear();
                    for (int i = 0; i < AvatarParams.categoriesWithOverlayProblemWithVeil.size(); i++) {
                        if (!picturePieces.containsKey(AvatarParams.categoriesWithOverlayProblemWithVeil.get(i))) {
                            picturePieces.put(AvatarParams.categoriesWithOverlayProblemWithVeil.get(i),
                                    defaultPicturesToShow.get(AvatarParams.categoriesWithOverlayProblemWithVeil.get(i)).get(0));
                        }
                    }
//                            drawNewResult();
                }
            } else {
                for (int i = 0; i < AvatarParams.categoriesWithOverlayProblemWithVeil.size(); i++) {
                    if (picturePieces.containsKey(AvatarParams.categoriesWithOverlayProblemWithVeil.get(i))) {
                        tempRemovedPiecesByVeil.add(picturePieces.get(AvatarParams.categoriesWithOverlayProblemWithVeil.get(i)));
                        picturePieces.remove(AvatarParams.categoriesWithOverlayProblemWithVeil.get(i));
                    }
                }
            }
        }

        if ((AvatarParams.categoriesWithOverlayProblemWithVeil.contains(catID)) && (picturePieces.containsKey(AvatarParams.catVeil))) {
            if (!picturePieces.get(AvatarParams.catVeil).getID().equals(AvatarParams.catVeil.substring(AvatarParams.categoryNameSubStringIndex) + "0")) {
                putInPicturePieces(AvatarParams.catVeil, defaultPicturesToShow.get(AvatarParams.catVeil).get(0));
                for (int i = 0; i < tempRemovedPiecesByVeil.size(); i++) {
                    picturePieces.put(getCatID(tempRemovedPiecesByVeil.get(i).getID()), tempRemovedPiecesByVeil.get(i));
                    //                        putInPicturePieces();
                }
                tempRemovedPiecesByVeil.clear();
                for (int i = 0; i < AvatarParams.categoriesWithOverlayProblemWithVeil.size(); i++) {
                    if (!picturePieces.containsKey(AvatarParams.categoriesWithOverlayProblemWithVeil.get(i))) {
                        picturePieces.put(AvatarParams.categoriesWithOverlayProblemWithVeil.get(i),
                                defaultPicturesToShow.get(AvatarParams.categoriesWithOverlayProblemWithVeil.get(i)).get(0));
                    }
                }
//                        drawNewResult();
            }
        }


        // check for hat and hair

        if (catID.equals(AvatarParams.catHairs)) {
            if (!objectID.equals(hairs0)) {
                picturePieces.put(AvatarParams.catHat, defaultPicturesToShow.get(AvatarParams.catHat).get(0));
            }
        } else if (catID.equals(AvatarParams.catHat)) {
            if (objectID.equals(hat0)) {
                if (!picturePieces.get(AvatarParams.catHat).getID().equals(hat0)) {
                    picturePieces.put(AvatarParams.catHairs, removedHairByHat);
                }
            } else {
                if (picturePieces.containsKey(AvatarParams.catHairs)) {
                    removedHairByHat = picturePieces.get(AvatarParams.catHairs);
                    picturePieces.remove(AvatarParams.catHairs);
                }
            }
        }

    }

    public void getLogoOnPicture(String[] strings){
        String text = strings[0];
        int[] dim = getLogoMaxAllowedDimensions();
//            boolean cont = true;
        Bitmap bm = getBitmapWithTextDrawn(text,
                dim[0]
                , dim[1]
                , 20, Color.parseColor(AvatarParams.userTextColor_RGB));

//        PicturePiece pp = new PicturePiece(bm, AvatarParams.catCharsooLogo.substring(AvatarParams.categoryNameSubStringIndex) + Integer.toString(0),
//                AvatarParams.defaultCategoryOrder.get(AvatarParams.catCharsooLogo),
//                AvatarParams.resultPictureWidth - bm.getWidth() - (AvatarParams.logoFrameWidthMargin_PIX),
//                AvatarParams.resultPictureHeight - bm.getHeight() - (AvatarParams.logoFrameHeightMargin_PIX));
        PicturePiece pp = new PicturePiece(bm, AvatarParams.catCharsooLogo.substring(AvatarParams.categoryNameSubStringIndex) + Integer.toString(0),
                AvatarParams.defaultCategoryOrder.get(AvatarParams.catCharsooLogo),
                AvatarParams.resultPictureWidth - bm.getWidth() - (AvatarParams.logoFrameWidthMargin_PIX),
                AvatarParams.logoFrameHeightMargin_PIX);

        setLogoResultPicture(pp);

    }

    private class getLogoDrawnOnPicture extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... strings) {
            int size = AvatarParams.maxTextSizeOnPicture;
            String text = strings[0];
            int[] dim = getLogoMaxAllowedDimensions();
//            boolean cont = true;
            Bitmap bm = getBitmapWithTextDrawn(text,
                    dim[0]
                    , dim[1]
                    , 20, Color.parseColor(AvatarParams.userTextColor_RGB));
//            for (int i = size; ((i >= AvatarParams.minTextSizeOnPicture) && cont == true); i -= 2) {
//                bm = getCroppedBitmap(getBitmapWithTextDrawn(text,
//                        AvatarParams.resultPictureWidth / 2
//                        , AvatarParams.resultPictureHeight / 2
//                        , i, Color.parseColor(AvatarParams.userTextColor_RGB)));
//                if ((bm.getHeight() <= dim[1]) && (bm.getWidth() <= dim[0])) {
//                    cont = false;
//                }
//            }

            return bm;
        }

        @Override
        protected void onPostExecute(Bitmap bm) {

            PicturePiece pp = new PicturePiece(bm, AvatarParams.catCharsooLogo.substring(AvatarParams.categoryNameSubStringIndex) + Integer.toString(0),
                    AvatarParams.defaultCategoryOrder.get(AvatarParams.catCharsooLogo),
                    AvatarParams.resultPictureWidth - bm.getWidth() - (AvatarParams.logoFrameWidthMargin_PIX),
                    AvatarParams.resultPictureHeight - bm.getHeight() - (AvatarParams.logoFrameHeightMargin_PIX));
            setLogoResultPicture(pp);
//            if ((bm.getWidth() > getLogoMaxAllowedDimensions()[0])
//                    || (bm.getHeight() > getLogoMaxAllowedDimensions()[1])) {
////                Toast.makeText(thisContext.getApplicationContext(), thisContext.getString(R.string.txt_TooMuchCharacterTyped), Toast.LENGTH_LONG).show();
////                bm=Bitmap.createBitmap(1,1, Bitmap.Config.ARGB_8888);
//            } else {
////                Bitmap result=userTextBack.copy(Bitmap.Config.ARGB_8888, true);
//                Bitmap result = Bitmap.createBitmap(getLogoMaxAllowedDimensions()[0]
//                        , getLogoMaxAllowedDimensions()[1]
//                        , Bitmap.Config.ARGB_8888);
//                new Canvas(result).drawBitmap(bm, result.getWidth() / 2 - bm.getWidth() / 2, result.getHeight() - bm.getHeight() - 2, new Paint());
//                PicturePiece pp = new PicturePiece(result, AvatarParams.catCharsooLogo.substring(AvatarParams.categoryNameSubStringIndex) + Integer.toString(0),
//                        AvatarParams.defaultCategoryOrder.get(AvatarParams.catCharsooLogo),
//                        0,
//                        AvatarParams.resultPictureHeight - result.getHeight());
//                setLogoResultPicture(pp);
//            }
/*            isProcessingText = false;
            putTextButton.setBackgroundColor(Color.parseColor(AvatarParams.charsooDefaultColor));
            putTextButton.setImageBitmap(BitmapFactory.decodeResource(activity.getResources(), R.drawable.avatar_put_text_button_image));
            putTextButton.setEnabled(true);*/

        }
    }

//    public Bitmap getIfScaledImage(Bitmap bmp, int originalWidth, int originalHeight, int newWidth, int newHeight) {
//
////        if ((new_width==0)||(new_height==0))
////        {
////            new_width=1;
////            new_height=1;
////        }
//        if ((originalHeight < newHeight) && (originalWidth < newWidth))
//            return bmp;
//        else {
//            int scaledWidth, scaledHeight;
//            if (originalWidth > originalHeight) {
//                scaledWidth = newWidth;
//                scaledHeight = (int) Math.floor((double) originalHeight * ((double) scaledWidth / (double) originalWidth));
//            } else {
//                scaledHeight = newHeight;
//                scaledWidth = (int) Math.floor((double) originalWidth * ((double) scaledHeight / (double) originalHeight));
//            }
//            try {
//                return Bitmap.createScaledBitmap(bmp, scaledWidth, scaledHeight, true);
//            } catch (Exception e) {
////                Toast.makeText(activity.getApplicationContext(), "main controller : getIfScaledImage() e:" + e.getMessage(), Toast.LENGTH_LONG).show();
//                return Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
//            }
//        }
//    }

    public Bitmap getBitmapWithTextDrawn(String text, int resultWidth, int resultHeight, int textSize, int textColor) {

//        canvas
        Paint paint = new Paint();
        paint.setColor(textColor); // Text Color
        float scale = thisContext.getResources().getDisplayMetrics().density;
//        paint.setStrokeWidth((int) (textSize*scale)); // Text Size
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)); // Text Overlapping Pattern
        Bitmap tempBitmap = Bitmap.createBitmap(resultWidth
                , resultHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(tempBitmap);
        canvas.drawColor(Color.parseColor("#c5f9ffff"));
//        paint.setShadowLayer(5f, 5f, 5f, Color.TRANSPARENT);
        // some more settings...
        Rect bounds;

        String putText = thisContext.getString(R.string.txt_JoinMyFriendsOnCharsoo);
        int tSize = textSize;
        paint.setFakeBoldText(true);
        do {
            paint.setTextSize((int) (tSize * scale));
            bounds = new Rect();
            paint.getTextBounds(putText, 0, putText.length(), bounds);
            tSize -= 1;
        } while (bounds.height() > (resultHeight / 3) || bounds.width() > resultWidth);
        int x = (tempBitmap.getWidth() - bounds.width()) / 2;
        int y = ((tempBitmap.getHeight() / 3) + bounds.height()) / 2;
        canvas.drawText(putText, x, y, paint);

        putText = text;
        paint.setFakeBoldText(true);
        paint.setColor(thisContext.getResources().getColor(R.color.primaryColor));
        tSize = textSize;
        do {
            paint.setTextSize((int) (tSize * scale));
            bounds = new Rect();
            paint.getTextBounds(putText, 0, putText.length(), bounds);
            tSize -= 1;
        } while (bounds.height() > (resultHeight / 3) || bounds.width() > resultWidth);
        x = (tempBitmap.getWidth() - bounds.width()) / 2;
        y = ((tempBitmap.getHeight() / 3) + bounds.height()) / 2 + (tempBitmap.getHeight() / 3);
        canvas.drawText(putText, x, y, paint);

        putText = "www.icharsoo.com";
        paint.setColor(textColor);
        paint.setFakeBoldText(true);
        tSize = textSize;
        do {
            paint.setTextSize((int) (tSize * scale));
            bounds = new Rect();
            paint.getTextBounds(putText, 0, putText.length(), bounds);
            tSize -= 1;
        } while (bounds.height() > (resultHeight / 3) || bounds.width() > resultWidth);
        x = (tempBitmap.getWidth() - bounds.width()) / 2;
        y = ((tempBitmap.getHeight() / 3) + bounds.height()) / 2 + (2 * tempBitmap.getHeight() / 3);
        canvas.drawText(putText, x, y, paint);
        return tempBitmap;
    }

    public Bitmap getCroppedBitmap(Bitmap pic) {
        int[] cropBounds = getRectToCropBitmap(pic);
        Bitmap cropped = Bitmap.createBitmap(pic,
                cropBounds[0], cropBounds[1],
                cropBounds[2] - cropBounds[0] + 1,
                cropBounds[3] - cropBounds[1] + 1);
        return cropped;
    }

    private int[] getRectToCropBitmap(Bitmap bm) {
        int[] rect = new int[4];
        rect[0] = bm.getWidth();//left
        rect[1] = bm.getHeight();//top
        rect[2] = 0;//right
        rect[3] = 0;//bottom
        int color = 0;
        for (int i = 0; i < bm.getWidth(); i++) {
            for (int j = 0; j < bm.getHeight(); j++) {
                color = bm.getPixel(i, j);

                //find left Bound
                if ((color != Color.TRANSPARENT) & (i < rect[0])) {
                    rect[0] = i;
                }
                //find Top Bound
                if ((color != Color.TRANSPARENT) & (j < rect[1])) {
                    rect[1] = j;
                }
                //find right Bound
                if ((color != Color.TRANSPARENT) & (i > rect[2])) {
                    rect[2] = i;
                }
                //find Bottom Bound
                if ((color != Color.TRANSPARENT) & (j > rect[3])) {
                    rect[3] = j;
                }
            }

        }
        return rect;
    }
}
