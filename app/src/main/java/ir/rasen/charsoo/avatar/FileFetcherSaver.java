package ir.rasen.charsoo.avatar;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;

/**
 * Created by Hossien on 4/26/2015.
 */
public class FileFetcherSaver {
    private static FileFetcherSaver instance;

    private static final String constantAsset="asset";
    private static final String constantLocal="local";
    private static final String constantRemote="remote";

    private String remoteRootDirectory;

    private Hashtable<String,Hashtable<String,Hashtable<String,Integer>>> positionIndexes;

    private String localRootDirectory;
    private Context context;
    private ArrayList<String> lastSelectedPieces;
    private boolean autoSaveInProgress;

    private static final ArrayList<String> categoriesWithNoImageIgnorance = new ArrayList<String>()
    {{
//            add(AvatarParams.catShoes);
            add(AvatarParams.catShirt);
            add(AvatarParams.catPants);
        }};;

    public static synchronized FileFetcherSaver getInstance(Context c){
        if (instance==null)
            instance=new FileFetcherSaver(c);
        return instance;
    }


//    public static synchronized FileFetcherSaver getInstance(){
//        return instance;
//    }


    private FileFetcherSaver(Context c){

        context=c;
        localRootDirectory =context.getFilesDir().getAbsolutePath()+"/";
        lastSelectedPieces=new ArrayList<>();
        autoSaveInProgress=false;
        positionIndexes=new Hashtable<>();
        remoteRootDirectory="http://shikbash.com/charsoo/";
    }




//*******************************************************************************************************************************
//*******************************************************************************************************************************
//*******************************************************************************************************************************
//******************************************* Saving Part


    public boolean saveResultPicToShareDirectory(Bitmap bmp){
        String path=Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+ AvatarParams.charsooPublicSubdirectory;
        File f=new File(path);
        f.mkdirs();
        path+= AvatarParams.publicSubdirectoryForSharing;
        File noMedia=new File(path,".nomedia");
        try {
            noMedia.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean b=noMedia.exists();
        return saveBitmapToCustomPath(bmp,path, AvatarParams.tempShareFileName,"");
    }


    public boolean saveResultPicInPublic(Bitmap pic){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String formattedDate = df.format(c.getTime());
        String path=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+"/"+ AvatarParams.charsooPublicSubdirectory;
        File f=new File(path);
        f.mkdirs();
        path+= AvatarParams.publicImagesSubDirectory;
        if (saveBitmapToCustomPath(pic,path, AvatarParams.publicSavedPicNamesPrefix+formattedDate.toString(), AvatarParams.publicSavedPictureFileType))
        {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT)
                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + path)));
            else
            {
                String[] toBeScanned=new String[]{path};

                MediaScannerConnection.scanFile(context,toBeScanned, null,new MediaScannerConnection.OnScanCompletedListener() {
                    @Override
                    public void onScanCompleted(String s, Uri uri) {

                    }
                });
            }
            return true;
        }
        else
            return false;

    }


    public void setLastSelectedPieces(ArrayList<String> sp){
        lastSelectedPieces.clear();
        lastSelectedPieces.addAll(sp);
        new saveNewSelectedPiecesNames().execute();
    }


    public boolean saveBitmapToCustomPath(Bitmap bmp,String path,String fileName,String fileType){
        String savePath;
        if(path.equals(AvatarParams.appDirectoryFilePathReferrer))
            savePath= localRootDirectory + AvatarParams.noCategoryFilesSubDirectory;
        else
            savePath=path;
        FileOutputStream out = null;
        try {
            File saveDir=new File(savePath);
                saveDir.mkdirs();
            File saveFile=new File(savePath+fileName+fileType);
            if (saveFile.exists())
                saveFile.delete();
            out = new FileOutputStream(saveFile);
            if ((fileType.startsWith(".j"))||(fileType.startsWith(".J")))
                bmp.compress(Bitmap.CompressFormat.JPEG,100,out);
            else
                bmp.compress(Bitmap.CompressFormat.PNG, 100, out);
            // PNG is a lossless format, the compression factor (100) is ignored
            out.flush();
            out.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public void saveNewBitmapPiece(Bitmap bmp, String categoryName,String pictureID){
        saveBitmapToCustomPath(bmp, localRootDirectory + categoryName + "/", pictureID, AvatarParams.fileType);
    }


    public void saveNewPositionIndexes(Hashtable<String,Hashtable<String,Integer>> indexesToSave,String categoryName){
        XmlSerializer serializer= Xml.newSerializer();
        StringWriter writer=new StringWriter();
        ArrayList<String> objects=new ArrayList<>(indexesToSave.keySet());
        try {
            serializer.setOutput(writer);
            serializer.startDocument("UTF-8", true);
            serializer.text("\n");
            serializer.startTag("", categoryName);
            serializer.text("\n");
            for (int i = 0; i < objects.size(); i++) {
                if ((Integer.parseInt(objects.get(i).replaceAll("[\\D]", ""))>4)||(objects.get(i).startsWith(AvatarParams.catUserText.substring(AvatarParams.categoryNameSubStringIndex)))) {
                    serializer.startTag("", objects.get(i));
                    serializer.text("\n");
                    serializer.startTag("", AvatarParams.leftIndex);
                    serializer.text(Integer.toString(indexesToSave.get(objects.get(i)).get(AvatarParams.leftIndex)));
                    serializer.endTag("", AvatarParams.leftIndex);
                    serializer.text("\n");
                    serializer.startTag("", AvatarParams.topIndex);
                    serializer.text(Integer.toString(indexesToSave.get(objects.get(i)).get(AvatarParams.topIndex)));
                    serializer.endTag("", AvatarParams.topIndex);
                    serializer.text("\n");
                    serializer.endTag("", objects.get(i));
                    serializer.text("\n");
                }
            }
            serializer.endTag("", categoryName);
            serializer.endDocument();
            File saveDir=new File(localRootDirectory + AvatarParams.positionIndexesSubdirectory);
            if (!saveDir.exists())
                saveDir.mkdirs();
            File F=new File(localRootDirectory + AvatarParams.positionIndexesSubdirectory+categoryName+".xml");
            if (F.exists())
                F.delete();
            FileOutputStream fos=new FileOutputStream(F);
            fos.write(writer.toString().getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private class saveNewSelectedPiecesNames extends AsyncTask<Void,Void,StringWriter>{

        @Override
        protected StringWriter doInBackground(Void... voids) {
            for(;autoSaveInProgress;);
            autoSaveInProgress=true;
            XmlSerializer serializer= Xml.newSerializer();
            StringWriter writer=new StringWriter();
            ArrayList<String> pieces=new ArrayList<>(lastSelectedPieces);
            try {
                serializer.setOutput(writer);
                serializer.startDocument("UTF-8", true);
                serializer.text("\n");
                for (int i = 0; i < pieces.size(); i++) {
                    serializer.startTag("", pieces.get(i));
                    serializer.endTag("", pieces.get(i));
                    serializer.text("\n");
                }
                serializer.endDocument();
                return writer;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(StringWriter w){
            if (w!=null){
                try {
                    File saveDir=new File(localRootDirectory + AvatarParams.autoSaveSubdirectory);
                    if (!saveDir.exists())
                        saveDir.mkdirs();
                    else
                    {
                        saveDir.delete();
                        saveDir.mkdirs();
                    }
                    File F=new File(localRootDirectory + AvatarParams.autoSaveSubdirectory+ AvatarParams.autoSavedFileName);
                    if (F.exists())
                        F.delete();
                    FileOutputStream fos= null;
                    fos = new FileOutputStream(F);
                    fos.write(w.toString().getBytes());
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            autoSaveInProgress=false;
        }
    }






//*******************************************************************************************************************************
//*******************************************************************************************************************************
//*******************************************************************************************************************************
//******************************************* Fetching Part


    public PicturePiece getPicturePiece(String categoryName, String pictureID,boolean isCalledOnMainThread) { //if there is no image with such uniqueId, returns NULL
        int pieceNumber=Integer.parseInt(pictureID.replaceAll("[\\D]", ""));
        Bitmap bmp=null;

        if (!positionIndexes.containsKey(categoryName))
            positionIndexes.put(categoryName,new Hashtable<String, Hashtable<String, Integer>>());

        if ((pieceNumber==0) && (AvatarParams.categoriesWithNoImage.contains(categoryName))&&(!categoriesWithNoImageIgnorance.contains(categoryName))){ // this categoryName has no image
            positionIndexes.get(categoryName).put(pictureID,
                    new Hashtable<String,Integer>(){{
                        put(AvatarParams.leftIndex,0);
                        put(AvatarParams.topIndex,0);
                    }});
            bmp=Bitmap.createBitmap(1,1, Bitmap.Config.ARGB_8888); // empty Picture
        }
        else{
            // At First Try fetching from asset
            bmp=getBitmapFromAsset(categoryName, pictureID);
            if (!positionIndexes.get(categoryName).containsKey(pictureID))
            {
                Hashtable<String,Hashtable<String,Integer>> newPositions=new Hashtable<>(getNewPositionIndexes(categoryName,constantAsset));
                ArrayList<String> names=new ArrayList<>(newPositions.keySet());
                for (int i = 0; i < names.size(); i++) {
                    positionIndexes.get(categoryName).put(names.get(i),new Hashtable<>(newPositions.get(names.get(i))));
                }
            }


            // then try Local Fetch
            if (bmp==null) {
                bmp = getBitmapFromLocal(categoryName, pictureID);
                if (!positionIndexes.get(categoryName).containsKey(pictureID)) {
                    Hashtable<String, Hashtable<String, Integer>> newPositions = new Hashtable<>(getNewPositionIndexes(categoryName,constantLocal));
                    ArrayList<String> names = new ArrayList<>(newPositions.keySet());
                    for (int i = 0; i < names.size(); i++) {
                        positionIndexes.get(categoryName).put(names.get(i),new Hashtable<>(newPositions.get(names.get(i))));
                    }
                }
            }

            // if Local fetch failed, try remote
            if ((bmp==null) & (isCalledOnMainThread==false)){
                bmp = getBitmapFromRemote(categoryName, pictureID);
                Hashtable<String, Hashtable<String, Integer>> newPositions = new Hashtable<>(getNewPositionIndexes(categoryName,constantRemote));
                ArrayList<String> names = new ArrayList<>(newPositions.keySet());
                for (int i = 0; i < names.size(); i++) {
                    positionIndexes.get(categoryName).put(names.get(i), new Hashtable<>(newPositions.get(names.get(i))));
                }
                // save new fetched position indexes
                saveNewPositionIndexes(positionIndexes.get(categoryName), categoryName);
            }
        }

        if (bmp!=null)
            return makePicturePiece(bmp,categoryName,pictureID);
        else
            return null;
    }


    public Hashtable<String,PicturePiece> getAutoSavedPicturePieces(){
        Hashtable<String,PicturePiece> fetchedPieces=null;
        ArrayList<String> piecesNames=new ArrayList<>();
        XmlPullParserFactory pullParserFactory;
        try {
            pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();

            InputStream in_s = new FileInputStream(new File(localRootDirectory+ AvatarParams.autoSaveSubdirectory+ AvatarParams.autoSavedFileName));
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in_s, null);
            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT){
                String name = null;
                if (eventType==XmlPullParser.START_TAG){
                    piecesNames.add(parser.getName());
                }
                eventType = parser.next();
            }

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (!piecesNames.isEmpty()){
            fetchedPieces=new Hashtable<>();
            String catName="";
            for (int i = 0; i < piecesNames.size(); i++) {
                catName=getCatID(piecesNames.get(i));
                fetchedPieces.put(catName,getPicturePiece(catName,piecesNames.get(i),true));
            }
        }
        return fetchedPieces;
    }





    //-------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------
    // private Methods

    private Bitmap getBitmapFromAsset(String categoryName,String pictureName){
        AssetManager assetManager = context.getAssets();
        Bitmap bmp=null;
        try {
            InputStream iStr = assetManager.open("avatar/"+categoryName + "/" + pictureName + AvatarParams.fileType);
            bmp= BitmapFactory.decodeStream(iStr);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmp;
    }

    private Bitmap getBitmapFromLocal(String categoryName,String pictureName){
        Bitmap bm=null;
        try {
            bm = BitmapFactory.decodeStream(new FileInputStream(new File(localRootDirectory + categoryName + "/" + pictureName + AvatarParams.fileType)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return bm;
    }


    private Bitmap getBitmapFromRemote(String categoryName,String pictureName){
        String path=remoteRootDirectory+categoryName+"/"+pictureName+ AvatarParams.fileType;
        Bitmap bm=null;
        try {
            bm = BitmapFactory.decodeStream((InputStream) new URL(path).getContent());
            saveNewBitmapPiece(bm, categoryName, pictureName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bm;
    }

    private Hashtable<String,Hashtable<String,Integer>> getNewPositionIndexes(String categoryName,String fetchRoot){
        Hashtable<String,Hashtable<String,Integer>> newPositionIndexes=new Hashtable<>();
        switch (fetchRoot){
            case constantAsset:
                AssetManager assetManager = context.getAssets();
                try {
                    InputStream iStr = assetManager.open("avatar/"+AvatarParams.positionIndexesSubdirectory+categoryName + ".xml");
                    newPositionIndexes=getPositionIndexesFromXml(categoryName,iStr,constantAsset);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case constantLocal:
                try {
                    InputStream iStr = new FileInputStream(new File(localRootDirectory+ AvatarParams.positionIndexesSubdirectory+categoryName + ".xml"));
                    newPositionIndexes=getPositionIndexesFromXml(categoryName,iStr,constantLocal);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case constantRemote:
                String path=remoteRootDirectory+ AvatarParams.positionIndexesSubdirectory+categoryName+".xml";
                try {
                    InputStream iStr = (InputStream) new URL(path).getContent();
                    newPositionIndexes=getPositionIndexesFromXml(categoryName,iStr,constantRemote);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
        return newPositionIndexes;
    }


    private PicturePiece makePicturePiece(Bitmap bmp, String categoryName,String pictureID){
        switch (categoryName) {
            case AvatarParams.catBackground:
                return new PicturePiece(bmp, pictureID, AvatarParams.defaultCategoryOrder.get(categoryName),
                        0, 0);
            case AvatarParams.catUserText:
                if (positionIndexes.containsKey(categoryName)) {
                    if (positionIndexes.get(categoryName).containsKey(pictureID))
                        return new PicturePiece(bmp, pictureID, AvatarParams.defaultCategoryOrder.get(categoryName),
                                positionIndexes.get(categoryName).get(pictureID).get(AvatarParams.leftIndex),
                                positionIndexes.get(categoryName).get(pictureID).get(AvatarParams.topIndex));
                    else {
                        return new PicturePiece(bmp, pictureID, AvatarParams.defaultCategoryOrder.get(categoryName),
                                AvatarParams.userTextPositionIndex[0], AvatarParams.userTextPositionIndex[1]);
                    }
                }
            case AvatarParams.catCharsooLogo:
                return new PicturePiece(bmp, pictureID, AvatarParams.defaultCategoryOrder.get(categoryName),
                        AvatarParams.charsooLogoPositionIndex[0], AvatarParams.charsooLogoPositionIndex[1]);
            case AvatarParams.catUserURL:
                return new PicturePiece(bmp, pictureID, AvatarParams.defaultCategoryOrder.get(categoryName),
                        AvatarParams.userURLPositionIndex[0], AvatarParams.userURLPositionIndex[1]);
            case AvatarParams.catReservedItem:
                return new PicturePiece(bmp, pictureID, AvatarParams.defaultCategoryOrder.get(categoryName),
                        AvatarParams.reservedItemPositionIndex[0], AvatarParams.reservedItemPositionIndex[1]);
            case AvatarParams.catHuman:
                return new PicturePiece(bmp, pictureID, AvatarParams.defaultCategoryOrder.get(categoryName),
                        AvatarParams.humanPositionIndex[0], AvatarParams.humanPositionIndex[1]);
            default:
                if (positionIndexes.containsKey(categoryName)) {
                    if (positionIndexes.get(categoryName).containsKey(pictureID))
                        return new PicturePiece(bmp, pictureID, AvatarParams.defaultCategoryOrder.get(categoryName),
                                positionIndexes.get(categoryName).get(pictureID).get(AvatarParams.leftIndex),
                                positionIndexes.get(categoryName).get(pictureID).get(AvatarParams.topIndex));
                    else {
                        // do fetch from remote
                    }
                }
        }
        return null;
    }


    private Hashtable<String,Hashtable<String,Integer>> getPositionIndexesFromXml(String categoryName, InputStream isr,String callerID){
        XmlPullParserFactory pullParserFactory;
        try {
            pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(isr, null);

            String namesPrefix=categoryName.substring(AvatarParams.categoryNameSubStringIndex);
            return parseXML(parser,namesPrefix,callerID);

        } catch (XmlPullParserException e) {

            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new Hashtable<>();
    }

    private Hashtable<String,Hashtable<String,Integer>> parseXML(XmlPullParser parser,String namesPrefix,String callerID) throws XmlPullParserException,IOException
    {
        Hashtable<String,Hashtable<String,Integer>> newPositionIndexes=new Hashtable<>();
        int eventType = parser.getEventType();
        String currentName="";
        while (eventType != XmlPullParser.END_DOCUMENT){
            String name;
            if (eventType==XmlPullParser.START_TAG){
                name = parser.getName();
                if (name.startsWith(namesPrefix)) {
                    currentName=name;
                    newPositionIndexes.put(currentName,new Hashtable<String, Integer>());
                }
                else {
                    switch (callerID) {
                        case constantLocal:
                            if ((name.equals(AvatarParams.leftIndex)) | (name.equals(AvatarParams.topIndex)))
                                newPositionIndexes.get(currentName).put(name, Integer.parseInt(parser.nextText().toString()));
                            break;
                        default:
                            if (name.equals(AvatarParams.leftIndex))
                                newPositionIndexes.get(currentName).put(name, Integer.parseInt(parser.nextText().toString()) + AvatarParams.humanPositionIndex[0]);
                            else if (name.equals(AvatarParams.topIndex))
                                newPositionIndexes.get(currentName).put(name, Integer.parseInt(parser.nextText().toString()) + AvatarParams.humanPositionIndex[1]);
                            break;
                    }
                }
            }
            eventType = parser.next();
        }
        return newPositionIndexes;
    }




    private String getCatID(String objectID){
        return AvatarParams.categoryNamePrefix+objectID.replaceAll("\\d","");
    }

}
