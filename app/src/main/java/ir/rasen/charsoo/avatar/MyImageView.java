package ir.rasen.charsoo.avatar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;
import android.widget.ImageView;


/**
 * Created by Hossien on 4/25/2015.
 */
public class MyImageView extends ImageView {


    String objectID;
    //for categories this uniqueId starts with "cat_". example: cat_hairs
    //for picture pieces this uniqueId ends with piece number. example: hairs23


    int indexInContainer;

    public MyImageView(Context context) {
        super(context);

//        el= new EventListen

        /*this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                MainController.getInstance().itemClickManager("a");

            }
        });*/
    }

    public void setObjectIdAndIndex(String iD,int index){
        objectID=iD;
        indexInContainer=index;
    }

    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);

//        setBackgroundColor(Color.BLACK);

    }

    public void setClickListener(final DesignFragmentEventListener ife){
        this.setOnClickListener(new OnClickListener() {
//            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
//                if (objectID.startsWith(AvatarParams.categoryNamePrefix))
//                    setBackground(new BitmapDrawable(getResources(),getCircularIconBackground(getHeight(), Color.parseColor(AvatarParams.iconBackgroundActiveColor))));
                ife.objectClickListener(objectID,indexInContainer);
            }
        });
    }

    private Bitmap getCircularIconBackground(int widthHeight,int color){
        Bitmap tempBitmap=Bitmap.createBitmap(widthHeight,widthHeight, Bitmap.Config.ARGB_8888);
        Canvas c=new Canvas(tempBitmap);
        c.drawColor(Color.TRANSPARENT);
        Paint p=new Paint();
        p.setStyle(Paint.Style.FILL);
        p.setColor(color);
        c.drawCircle(widthHeight/2,widthHeight/2,(widthHeight/2)-1,p);
        return tempBitmap;
    }

    public String getObjectID(){
        return objectID;
    }
}
