package ir.rasen.charsoo.avatar;

import android.graphics.Bitmap;

/**
 * Created by Hossien on 4/25/2015.
 */
public class PicturePiece {
    public Bitmap piece;
    private String id;

    private int canvasOrder;
    private int leftPos,topPos;

    public void recyclePiece(){
        piece.recycle();
    }

    public PicturePiece(){
        piece=null;
        leftPos=0;
        topPos=0;
        id="NoName"+String.valueOf(0);
        canvasOrder=0;
    }


    public PicturePiece(Bitmap pic,String pieceID,
                        int pieceOrder,int leftPositionIndex,int topPositionIndex){
        piece=Bitmap.createBitmap(pic);

        leftPos=leftPositionIndex;
        topPos=topPositionIndex;



        id=pieceID;

        canvasOrder=pieceOrder;
    }

//  *********************************************************************************

    public Bitmap getPicturePiece(){
        return piece;
    }

    public int getLeftPoitionIndex(){
        return leftPos;
    }

    public int getTopPositionIndex(){
        return topPos;
    }

    public String getID(){
        return id;
    }

    public void setPieceCanvasOrder(int newOrder){
        canvasOrder=newOrder;
    }

    public int getCanvasOrder(){
        return canvasOrder;
    }

}
