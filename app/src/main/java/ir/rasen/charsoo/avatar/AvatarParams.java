package ir.rasen.charsoo.avatar;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by hossein-pc on 4/26/2015.
 */
public class AvatarParams {

    public static final int inDesFragActiveIconMargin_Pixel=4;
    public static final int inDesFragNonActiveIconMargin_pixel=32;

    public static final String userTextColor_RGB="#000000" ;
    public static final String autoSaveSubdirectory="Autosaved/";

    public static final String charsooPublicSubdirectory="charsoo/";
    public static final String publicSubdirectoryForSharing="to_share/";
    public static final String publicImagesSubDirectory="avatar/";
    public static final String publicSavedPicNamesPrefix="Charsooavatar_";
    public static final String tempShareFileName="tempshare.jpeg";
    public static final String publicSavedPictureFileType=".jpeg";

    public static final int picsInAssetCount=5;

    public static final String userStrID="userStrId";

    public static final String activeIconBackgroundFileName="active_icon_back";

    public static final String appDirectoryFilePathReferrer ="inTheLocalAppPath";
    public static final String noCategoryFilesSubDirectory ="No_cat_files/";
    public static final String pieceContainerBackgroundColor="#DDDDDD";
    public static final String charsooDefaultColor="#00bfff";
    public static final String selectedBlueButtonColor="#008bb9";
    public static final int minTextSizeOnPicture=20;
    public static final int maxTextSizeOnPicture =65;
    public static final int userTextFrameWidthMargin_PIX=30;
    public static final int userTextFrameHeightMargin_PIX=0;

    public static final int logoFrameWidthMargin_PIX=20;
    public static final int logoFrameHeightMargin_PIX=60;

    public static final String autoSavedFileName="AutoSaved.xml";

//    public static final int pictureSizeForDrawText=textSizeOnPicture*3;

    public static final String tabDesignFragment="designFragment";
    public static final String tabAddtextFragment="addtextFragment";
    public static final String tabResultFragment="resultFragment";

    public static final String initialCategoryName="cat_background";


    public static final String leftIndex="left_index";
    public static final String topIndex="top_index";

    public static final String iconBackgroundInactiveColor="#eeeeee";
    public static final String iconBackgroundActiveColor="#29b6f6";


    public static final String catBackground="cat_background";
    public static final String catHuman="cat_human";
    public static final String catUserText="cat_usertext";
    public static final String catUserURL="cat_userurl";
    public static final String catCharsooLogo="cat_charsoologo";
    public static final String catHairs="cat_hairs";
    public static final String catHat="cat_hat";
    public static final String catGlasses="cat_glasses";
    public static final String catMustache="cat_mustache";
    public static final String catShirt="cat_shirt";
    public static final String catPants="cat_pants";
    public static final String catShoes="cat_shoes";
    public static final String catHandStuff="cat_handstuff";
    public static final String catEyebrow="cat_eyebrow";
    public static final String catLip="cat_lip";
    public static final String catNose="cat_nose";
    public static final String catEyes="cat_eyes";
    public static final String catVeil="cat_veil";
    public static final String catFaceExtras="cat_faceextras";
    public static final String catReservedItem="cat_reserveditem";
    public static final String catCoat="cat_coat";
    public static final int categoryNameSubStringIndex=4;
    public static final String categoryNamePrefix="cat_";


    public static final int pieceGroupIconWidthHeight=200;
    public static final int resultPictureWidth = 640;
    public static final int resultPictureHeight = 640;
    public static final int maxResultPictureWidth=2048;
    public static final int maxResultPictureHeight=2048;
    public static final int[] humanPositionIndex=new int[]{0,0};
    public static final int[] userTextPositionIndex=new int[]{0,0};
    public static final int[] userURLPositionIndex=new int[]{0,0};
    public static final int[] charsooLogoPositionIndex=new int[]{0,0};
    public static final int[] reservedItemPositionIndex=new int[]{0,0};

    public static final String localMainDirectory="/storage/emulated/0/my_bmp/";
    public static final String positionIndexesSubdirectory= "position_indexes/";
    public static final String fileType=".png";

    public static final int defaultPieceCount=5;
    public static final int piecesGroupCount=21;

    public static final int baseIndex=0;



    //default Orders
    public static final Hashtable<String,Integer> defaultCategoryOrder=new Hashtable<String,Integer>()
    {{
            put(catBackground,0);
            put(catHuman,1);

            put(catLip,2);
            put(catEyebrow,3);
            put(catEyes,4);

            put(catShirt,7);
            put(catShoes,5);
            put(catPants,6);

            put(catCoat,8);
            put(catFaceExtras,9);
            put(catMustache,10);
            put(catNose,11);


            put(catHairs,12);
            put(catHat,13);
            put(catGlasses,14);
            put(catVeil,15);
            put(catHandStuff,16);
            put(catReservedItem,17);
            put(catUserText,18);
            put(catUserURL,19);
            put(catCharsooLogo,20);
    }};

    public static final ArrayList<String> categoriesWithNoImage=new ArrayList<String>()
    {{
//        add(catLip);
        add(catEyebrow);
        add(catShirt);
        add(catPants);
//        add(catShoes);
        add(catCoat);
        add(catMustache);
        add(catFaceExtras);
        add(catGlasses);
        add(catHairs);
        add(catVeil);
        add(catHat);
        add(catHandStuff);
        add(catReservedItem);

            //
//            add(catNose);
    }};

    public static final ArrayList<String> allCategoryNames=new ArrayList<String>()
    {{
            add(catBackground);
            add(catHuman);
            add(catUserText);
            add(catUserURL);
            add(catLip);
            add(catEyebrow);
            add(catEyes);
            add(catShirt);
            add(catPants);
            add(catShoes);
            add(catCoat);
            add(catMustache);
            add(catNose);
            add(catFaceExtras);
            add(catGlasses);
            add(catHairs);
            add(catVeil);
            add(catHat);
            add(catHandStuff);
            add(catReservedItem);
            add(catCharsooLogo);
        }};

    public static final ArrayList<String> categoriesWithOverlayProblemWithVeil= new ArrayList<String>()
    {{
            add(catShirt);
            add(catPants);
            add(catShoes);
            add(catCoat);
            add(catMustache);
            add(catHairs);
            add(catHat);
    }};
    /*public static final ArrayList<String> humanRelatedPieces=new ArrayList<String>()
    {{
            add(catEyebrow);
            add(catShirt);
            add(catPants);
            add(catShoes);
            add(catCoat);
            add(catMustache);
            add(catFaceExtras);
            add(catGlasses);
            add(catHairs);
            add(catVeil);
            add(catHat);
            add(catHandStuff);
            add(catNose);
            add(catEyes);
            add(catLip);
        }};*/




}
